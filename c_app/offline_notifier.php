<?php

/*
 * This script will fetch the notification data for sms,email and gcm notification from redis and send to the user
 * 
 */

//echo "<pre>";
//print_r($_REQUEST);exit();

require_once(dirname(__FILE__).'/protected/vendor/Predis/Autoloader.php'); 
$config = require(dirname(__FILE__) . '/protected/config/cache_config.php');
$config = $config['servers'];

/*
 * Async curl function
 * 
 */
function curl_post_async($url, $params=array()){
    $post_params = array();
    foreach ($params as $key => $val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);
    $parts=parse_url($url);
    $fp = fsockopen($parts['host'],isset($parts['port'])?$parts['port']:80,$errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)){
        $out.= $post_string;
    }
    fwrite($fp, $out);
    fclose($fp);
}

/*
 * This function receive data from redis and send data to call_b2c_offline_notifier
 * 
 */
function get_notifications($redisObj){    
    $rd = "";
    try{        
        global $notifierQ;
        $queue = $notifierQ;
        $rd = $redisObj->rpop($queue);
        if(is_null($rd)){
            var_dump($rd);
            return $rd;
        }
        $rd_array = json_decode($rd,true);
        $param = array();
        foreach($rd_array['data'] as $k=>$v){
            $param["data[".$k."]"] = $v;
        }
        foreach ($rd_array['type'] as $k=>$v){
            $param["type[".$k."]"] = $v;
        }
        //echo "<pre>";
        //print_r($param);exit();
        //$res = call_b2c_offline_notifier($rd_array);
        $res = call_b2c_offline_notifier($param);
    } catch (Exception $ex) {
        print_r($ex->getTraceAsString());
    }
    return $rd;
}

/*
 * This function is to set data in redis (currently for Testing Purpose)
 * 
 */
function  set_notifications($redisObj){
    $rd = "";
    try{
        global $notifierQ;
        $queue = $notifierQ;
        $rd = $redisObj->lpush($queue,time());
    } catch (Exception $ex) {
        print_r($ex->getTraceAsString());
    }
    return $rd;
}

/*
 * This function call the offline notifier API
 * 
 */
function call_b2c_offline_notifier($param=array(),$data=array()){
    global $domain_url;
    $url1 = $domain_url."index.php/api_new/action/api/true/actiontype/call_offline_notifier/?";
    $query_string = http_build_query($data,'','&');
    echo date('Y-m-d H:i:s');echo "  ";
    echo $url = $url1."".$query_string;    
    curl_post_async($url,$param);
}

/*
 *  Making redis connection 
 * 
 */
try{
    Predis\Autoloader::register();
    $redisObj = new Predis\Client($config);
} catch (Exception $ex) {
    print_r($ex->getTraceAsString());
}

//$data = set_notifications($redisObj);
//exit();

/*
 * Executing the script forever
 * 
 */
while(1){
    $data = get_notifications($redisObj);
    if(is_null($data)){
        echo "\n".date('Y-m-d H:i:s')." going to sleep.\n";
        sleep(2);
        continue;
    }
    var_dump($data);        
    //print_r($data);
}





