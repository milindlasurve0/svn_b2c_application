<?php
/* @var $this UserrsController */
/* @var $model Userrs */

$this->breadcrumbs=array(
	'Userrs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Userrs', 'url'=>array('index')),
	array('label'=>'Create Userrs', 'url'=>array('create')),
	array('label'=>'View Userrs', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Userrs', 'url'=>array('admin')),
);
?>

<h1>Update User (<?php echo $model->user_name; ?>)</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'pmodel'=>$pmodel)); ?>