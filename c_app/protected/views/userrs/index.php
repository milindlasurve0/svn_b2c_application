<?php
/* @var $this UserrsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Userrs',
);

$this->menu=array(
	array('label'=>'Create Userrs', 'url'=>array('create')),
	array('label'=>'Manage Userrs', 'url'=>array('admin')),
        array('label'=>'upload dealer', 'url'=>array('upload')),
);
?>

<h1>Userrs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
