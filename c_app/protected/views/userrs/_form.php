<?php
/* @var $this UserrsController */
/* @var $model Userrs */
/* @var $form CActiveForm */

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'userrs-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_number'); ?>
		<?php echo $form->textField($model,'mobile_number',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'mobile_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($pmodel,'name',array('label'=>'Merchant Name')); ?>
		<?php echo $form->textField($pmodel,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($pmodel,'mobile',array('label'=>'Merchant Contact')); ?>
		<?php echo $form->textField($pmodel,'mobile',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'salt'); ?>
		<?php echo $form->textField($model,'salt',array('size'=>60,'maxlength'=>64,'disabled'=>true)); ?>
		<?php echo $form->error($model,'salt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_datetime'); ?>
		<?php echo $form->textField($model,'created_datetime',array('disabled'=>true)); ?>
		<?php echo $form->error($model,'created_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_datetime'); ?>
		<?php echo $form->textField($model,'modified_datetime',array('disabled'=>true)); ?>
		<?php echo $form->error($model,'modified_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
        <?php if(UsersComponent::isAdmin(Yii::app()->user->username)){ ?>
		<?php echo $form->textField($model,'role'); ?>
        <?php }elseif(UsersComponent::isReport(Yii::app()->user->username)){ ?>
		<?php echo $form->textField($model,'role'); ?>
        <?php }else{ ?>
        <?php echo $form->textField($model,'role',array('disabled'=>true)); ?>
        <?php } ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'dealer_name'); ?>
		<?php echo $form->textField($model,'dealer_name',array('disabled'=>false)); ?>
		<?php echo $form->error($model,'dealer_name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
        
</div> 
        
<?php $this->endWidget();
