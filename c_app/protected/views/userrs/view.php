<?php
/* @var $this UserrsController */
/* @var $model Userrs */

$this->breadcrumbs=array(
	'Userrs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Userrs', 'url'=>array('index')),
	array('label'=>'Create Userrs', 'url'=>array('create')),
	array('label'=>'Update Userrs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Userrs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Userrs', 'url'=>array('admin')),
);
?>

<h1>View Userrs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_name',
		'mobile_number',
		'password',
		'salt',
		'created_datetime',
		'modified_datetime',
		'role',
		'status',
                'dealer_name',
	),
)); ?>
