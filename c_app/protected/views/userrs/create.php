<?php
/* @var $this UserrsController */
/* @var $model Userrs */

$this->breadcrumbs=array(
	'Userrs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Userrs', 'url'=>array('index')),
	array('label'=>'Manage Userrs', 'url'=>array('admin')),
);
?>

<h1>Create Userrs</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'pmodel'=>$pmodel)); ?>