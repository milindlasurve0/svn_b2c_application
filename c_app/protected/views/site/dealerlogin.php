<html><head>
<meta charset="utf-8">
<title>Merchant Panel Login</title>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/js/jquery.min.js"></script>
<script>
    function validate(){
        var username = $.trim($("#LoginForm_username").val());
        var password = $.trim($("#LoginForm_password").val());
        if(username==''){
            $("#blank_username").show();
            return false;
        }
        if(password==''){
           $("#blank_password").show();
            return false;
        }    
        return true;
    }
</script>
<style>
@charset "utf-8";
/* CSS Document */
body {
	width:100%;
	font-family:Arial, Helvetica, sans-serif;
	color:#222222;
}
#loginbox {
	display:block;
	width:700px;
	padding:0 10px 10px 10px;
	margin:0 auto;
	border:1px solid #a9a9a9;
}
#redeembox {
	display:block;
	background:#e8e8e8;
	width:600px;
	padding:0 10px 10px 10px;
	margin:0 auto;
	border:1px solid #a9a9a9;
}
#overviewbox {
	display:block;
	width:500px;
	padding:0 10px 10px 10px;
	margin:0 auto;
	border:1px solid #a9a9a9;
}
a{
	text-decoration:none;
}

a.nav:link {
    color: #313131;
}

/* visited link */
a.nav:visited {
    color: #313131;
}

/* mouse over link */
a.nav:hover {
    color: #858585;
}

/* selected link */
a.nav:active {
    color: #313131;
} 
.center {
	margin-left:auto;
	margin-right:auto;
}
h3.login {
	background:#513d96;
	color:#fff;
	font-weight:normal;
	text-transform:uppercase;
	text-align:center;
	padding:8px;
	font-size:16px;
	font-family:Arial, Helvetica, sans-serif;

}
h3.overview {
	background:#999;
	color:#fff;
	font-weight:normal;
	text-transform:uppercase;
	text-align:center;
	padding:8px;
	font-size:16px;
	font-family:Arial, Helvetica, sans-serif;

}
#loginbox p {
	font-size:13px;
	text-align:center;
}
.red {
	color:#F00;
}
.check {
	font-size:12px;
	margin-left:80px;
}
.signup {
	text-transform:uppercase;
	color:#513d96;
	font-weight:bold;
}
a.signup {
	text-decoration:none;
}
.submit-button {
	width:150px;
	height:40px;
	line-height:30px;
	text-align:center;
	border-radius:8px;
	background:#179f87;
	color:#fff;
	font-weight:bold;
}

.button {
	display:inline-block;
	position:relative;
	margin:10px;
	padding:10px 40px;
	text-align:center;
	text-decoration:none;
	font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
	color:#fff;
	width:170px;
	border-radius:10px;
	text-transform:uppercase;
}
.button-redeem {
	display:inline-block;
	position:relative;
	margin:10px;
	padding:10px 40px;
	text-align:center;
	text-decoration:none;
	font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
	color:#fff;
	width:80px;
	border-radius:10px;
	text-transform:uppercase;
}
.green {
	color:#fff;
	background-color:#179f87;
}

.results_txt{
	float:left;
	margin-top:20px;
	padding-left:20px;
	line-height:25px;
}

.result_sty{
	color:#503c95;
	font-weight:bold;
}

ul{
	list-style-type:none;
	display:inline;
}

li{
	display:inline-block;
	padding-left:10px;
    font-family:Arial, Helvetica, sans-serif;
	font-size:14px;

}
.navbar{
 	float:right;
	margin-right:40px;
	margin-top:20px;
 }

h1{
	float:left;
	margin:0;
	padding:10px;
}

header{
	height:70px;
}

.username{
	float:left;
	margin-left:5px;
	margin-right:10px;
	margin-bottom:10px;
	padding-left:10px;
	padding-top:10px;
	height:30px;
	width:97%;
	background-color:#96d3d4;
	font-weight:bold;
	border-radius:8px;
}
.user{
float:left;
margin:0;
}
.date{
float:right;
margin:0px;
padding-right:10px;
}

.mainbody{
	float:left;
	margin-left:300px;
	margin-top:20px;
}

.mainbody_redeem{
	float:left;
	padding-left:20px;
	padding-top:20px;
	line-height:25px;
}
.maintop{
float:left;
width:100%;
margin-left:10px;
margin-right:10px;
}

.topleft{
float:left;
width:50%;
}

.topright{
float:left;
width:50%;
}

.overview_hdtxt{
font-family:Arial, Helvetica, sans-serif;
font-size:13px;
font-weight:bold;
color:#353535;
}

.thum_img{
float:left;
margin-top:20px;
}

.thum_img ul {
padding:0;
margin:0;
float:left;
}

.thum_img ul li{
padding-left:0;
padding-right:5px;
}
.vouchercode{
color:#544099;
font-weight:bold;
font-size:14px;
}
.filterbox{
background-color:#e8e8e8;
width:98%;
border:1px solid #a3a3a3;
float:left;
height:40px;
padding-top:10px;
padding-bottom:10px;
margin-top:20px;
}
.offers {
	display:block;
	float:left;
	//width:98%;
	margin-top:10px;
	height:30px;
	background-color:#e8e8e8;
	border:1px solid #a3a3a3;
}
.offers_body{
	float:left;
	//width:98%;
	border:1px solid #a3a3a3;
}
.offers ul li {
	width:18%;
	height:30px;
	vertical-align:middle;
}
.panel_body{
	float:left;
	border:1px solid #a3a3a3;
}
.panel_body ul li{
	width:18%;
	height:110px;
	vertical-align:middle;
}

.offers ul li{
	width:100px;
}
.offers_body ul li{
	width:100px;
}
.thum_sty{
padding-top:10px;
}    
</style>
</head>

<body>
<header>
<h1><img width="139" height="44" src="/images/pay1logo.jpg"></h1>
</header>
    <form id="login-form" method="post" action="" onsubmit="return validate()">
<section id="loginbox">
<div>
<h3 class="login">Login</h3>
<p align="center">Please fill out the following form with your login credentials: <br><br>
<i>Fields with <span class="red">*</span> are required </i><br><br>
Username <span class="red">*</span> <input type="text" name="username" id="LoginForm_username" type="text"><br>
<span class="red" id="blank_username" style="display: none" name ="blank_username">Username cannot be blank.</span>
<br>
Password <span class="red">*</span> <input type="password" name="password" id="LoginForm_password" type="text" ><br>
 <span class="red" id="blank_password" style="display: none" name ="blank_password">Password cannot be blank.</span>
    <?php if(!empty($error)){ ?><span class="red">Incorrect username or password.</span><?php } ?>
<br>
<span class="check"><input type="checkbox" id="LoginForm_rememberMe" type="checkbox" value="1" name="LoginForm[rememberMe]">Remember me on this computer</span><br><br>
<span class="signup"><!--<a href="http://b2c.pay1.in/index.php/users/signup" class="signup">SIGNUP</a>--></span> <!--a class="button green" href="">LOGIN</a-->
<input type="submit" value="LOGIN" class="submit-button" name="LOGIN">
</p></div>
</section>
</form>
</body></html>