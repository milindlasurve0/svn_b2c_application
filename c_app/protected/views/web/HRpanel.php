<style>
body{
    overflow-x:hidden;
}
</style>
<div class="container">
  <h2>Candidate Job Details</h2>  
  <p>The panel is used to enter  Candidates's Job Title and Job Description (JD) related to their profile on website (Pay1.in).</p>
  <div class="panel-group" style="margin-right: 140px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Job Details</div>
            <div class="panel-body">
                <form role="form" id="web_metatags_form" action="/web/HR_panel" method="post" >
                    <div class="form-group" id="home_page_img_small_div">
                        <label>Job Tittle </label>
                      <input type="text" class="form-control" id="job_tittle" name="job_tittle" placeholder="Job Tittle" required>
                    </div>
                    <div class="form-group" id="home_page_img_link_div">
                        <label>Job Description </label>
                      <textarea class="form-control" rows="5" id="job_description" name="job_description" placeholder="Job Description" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form> 
            </div>
    </div>
  </div>
</div> 
