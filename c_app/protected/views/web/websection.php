<style>
body{
    overflow-x:hidden;
}
</style>
<div class="container">
  <h2>Web Section</h2>  
  <p>The panel is used to upload image and enter text for website (Pay1.in) home and gift page.</p>
  <div class="panel-group" style="margin-right: 140px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Home Page Details</div>
            <div class="panel-body">
                <form role="form" id="web_details_form" action="/web/web_details" method="post" onsubmit="return AddImage(event)" enctype="multipart/form-data" >
                    <div class="form-group row">
                        <div class="col-sm-5">    
                            <label>Image input</label>
                            <input type="file" id="img_file_upload" name="img_file_upload" required>
                            <label id="ImageSizeError" style="display:none;color:red">Too big image to upload.Please upload image having size less than 300 KB.</label>
                            <p class="help-block">Upload image here for website pay1.in </p>
                            
                        </div>
                        <div class="col-sm-3" id="Image_page_div">  
                            <label for="select">Select list (select one):</label>
                            <select class="form-control" id="select_image_page" name="select_image_page">
                              <option selected="selected" value="1">Home page</option>
                              <option value="2">Gift page</option>
                              <option value="3">Category page</option>
                            </select>
                        </div>
                         <div class="col-sm-2" id="Image_sequence_div">  
                            <label for="select">Image Sequence</label>
                            <select class="form-control" id="select_image_sequence" name="select_image_sequence">
                              <option selected="selected" value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                        </div>
                        <div class="col-sm-3" id="Gift_types_div" style="display:none">  
                            <label for="select">Gift Types</label>
                            <select class="form-control" id="select_gift_types" name="select_gift_types">
                              <option selected="selected" value="0">Main Gift</option>
                              <option value="1">Featured Offer</option>
                              <option value="2">Top Offer</option>
                              <option value="3">Popular</option>
                              <option value="4">Recommended For You</option>
                              <option value="5">Offer of the Day</option>
                              <option value="6">Near you</option>
                            </select>
                        </div>
                         <div class="col-sm-3" id="Category_types_div" style="display:none">  
                            <label for="select">Category Types</label>
                            <select class="form-control" id="select_category_types" name="select_category_types">
                              <option selected="selected" value="1">Main Category</option>
                              <option value="2">BEAUTY</option>
                              <option value="7">SERVICE</option>
                              <option value="10">FOOD & BEVERAGES</option>
                              <option value="11">HEALTH & FITNESS</option>
                              <option value="12">FASHION</option>
                              <option value="13">ENTERTAINMENT</option>
                              <option value="14">LEARNING</option>
                              <option value="15">OTHERS</option>
                              <option value="16">ONLINE</option>
                              <option value="17">E-Vouchers</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="home_page_img_big_div">
                      <label>Home Page Image Big Text</label>
                      <input type="text" class="form-control" id="home_page_img_big_text" name="home_page_img_big_text" placeholder="Big Text" >
                    </div>
                    <div class="form-group" id="home_page_img_small_div">
                      <label>Home Page Image Small Text</label>
                      <input type="text" class="form-control" id="home_page_img_small_text" name="home_page_img_small_text" placeholder="Small Text">
                    </div>
                    <div class="form-group" id="home_page_img_link_div">
                      <label>Home Page Image Link</label>
                      <input type="text" class="form-control" id="home_page_img_link" name="home_page_img_link" placeholder="Image Link" >
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form> 
            </div>
    </div>
  </div>
</div> 
      
      

      <script>
            //validation for image upload
            function AddImage(event){
                unsetAddImage();
                var fsize = $('#img_file_upload')[0].files[0].size; //get file size
                if(fsize<=0 || fsize>355555){
                    $('#ImageSizeError').show();
                   event.preventDefault();
                   return false;
                }
                return true;
            }
            //unset the image upload
            function unsetAddImage(){
                $('#ImageSizeError').hide();
            }
            
            $('#select_image_page').change(function(){
                if($('#select_image_page').val() === '2') {
                  
                   $('#Image_sequence_div').hide(); 
                   $('#Gift_types_div').show();
                   $('#Category_types_div').hide();
                   
                   $('#home_page_img_big_div').hide();
                   $('#home_page_img_small_div').hide();
                   $('#home_page_img_link_div').hide();
                } else if($('#select_image_page').val() === '3') {
                   
                   $('#Image_sequence_div').hide(); 
                   $('#Gift_types_div').hide(); 
                   $('#Category_types_div').show(); 
                   
                   $('#home_page_img_big_div').hide();
                   $('#home_page_img_small_div').hide();
                   $('#home_page_img_link_div').hide();
                   
                } else{
                   
                   $('#Image_sequence_div').show(); 
                   $('#Gift_types_div').hide(); 
                   $('#Category_types_div').hide(); 
                   
                   $('#home_page_img_big_div').show();
                   $('#home_page_img_small_div').show();
                   $('#home_page_img_link_div').show();
                }
            });
      </script>    