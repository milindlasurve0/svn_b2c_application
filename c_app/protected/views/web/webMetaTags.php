<style>
body{
    overflow-x:hidden;
}
</style>
<div class="container">
  <h2>Web Meta Tags Details</h2>  
  <p>The panel is used to enter  Meta Tags , Description and Title for different pages on website (Pay1.in).</p>
  <div class="panel-group" style="margin-right: 140px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Meta Tags Details</div>
            <div class="panel-body">
                <form role="form" id="web_metatags_form" action="/web/Web_Meta_Tags" method="post" onsubmit="return AddImage(event)" enctype="multipart/form-data" >
                    <div class="form-group" id="home_page_img_big_div">
                      <label>Page Url</label>
                      <input type="text" class="form-control" id="page_url" name="page_url" placeholder="Page URL" required>
                    </div>
                    <div class="form-group" id="home_page_img_small_div">
                        <label>Page Tittle </label> <small class="text-primary"> <span class="glyphicon glyphicon-asterisk"></span>(60 Characters)</small>
                      <input type="text" class="form-control" id="page_tittle" maxlength="60" name="page_tittle" placeholder="Page Tittle" required>
                    </div>
                    <div class="form-group" id="home_page_img_link_div">
                        <label>Page Meta Description </label> <small class="text-primary"> <span class="glyphicon glyphicon-asterisk"></span>(160 Characters)</small>
                      <textarea class="form-control" rows="2" maxlength="160" id="page_meta_description" name="page_meta_description" placeholder="Page Meta Description" required></textarea>
                    </div>
                    <div class="form-group" id="home_page_img_link_div">
                      <label>Page Meta Tag</label>
                      <input type="text" class="form-control" id="page_meta_tag" name="page_meta_tag" placeholder="Page Meta Tag" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form> 
            </div>
    </div>
  </div>
</div> 
