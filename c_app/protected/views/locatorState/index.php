<?php
/* @var $this LocatorStateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Locator States',
);

$this->menu=array(
	array('label'=>'Create LocatorState', 'url'=>array('create')),
	array('label'=>'Manage LocatorState', 'url'=>array('admin')),
);
?>

<h1>Locator States</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
