<?php
/* @var $this LocatorStateController */
/* @var $model LocatorState */

$this->breadcrumbs=array(
	'Locator States'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LocatorState', 'url'=>array('index')),
	array('label'=>'Create LocatorState', 'url'=>array('create')),
	array('label'=>'View LocatorState', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage LocatorState', 'url'=>array('admin')),
);
?>

<h1>Update LocatorState <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>