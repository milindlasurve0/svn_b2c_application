<?php
/* @var $this LocatorStateController */
/* @var $model LocatorState */

$this->breadcrumbs=array(
	'Locator States'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LocatorState', 'url'=>array('index')),
	array('label'=>'Manage LocatorState', 'url'=>array('admin')),
);
?>

<h1>Create LocatorState</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>