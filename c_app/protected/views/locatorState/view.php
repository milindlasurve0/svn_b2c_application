<?php
/* @var $this LocatorStateController */
/* @var $model LocatorState */

$this->breadcrumbs=array(
	'Locator States'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List LocatorState', 'url'=>array('index')),
	array('label'=>'Create LocatorState', 'url'=>array('create')),
	array('label'=>'Update LocatorState', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LocatorState', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LocatorState', 'url'=>array('admin')),
);
?>

<h1>View LocatorState #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'toShow',
	),
)); ?>
