<?php
/* @var $this DealLocationsController */
/* @var $model DealLocations */

$this->breadcrumbs=array(
	'Deal Locations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DealLocations', 'url'=>array('index')),
	array('label'=>'Manage DealLocations', 'url'=>array('admin')),
);
?>

<h1>Create DealLocations</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>