<?php
/* @var $this DealLocationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Deal Locations',
);

$this->menu=array(
	array('label'=>'Create DealLocations', 'url'=>array('create')),
	array('label'=>'Manage DealLocations', 'url'=>array('admin')),
);
?>

<h1>Deal Locations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
