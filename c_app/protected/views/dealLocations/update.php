<?php
/* @var $this DealLocationsController */
/* @var $model DealLocations */

$this->breadcrumbs=array(
	'Deal Locations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DealLocations', 'url'=>array('index')),
	array('label'=>'Create DealLocations', 'url'=>array('create')),
	array('label'=>'View DealLocations', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DealLocations', 'url'=>array('admin')),
);
?>

<h1>Update DealLocations <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>