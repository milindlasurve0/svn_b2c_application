<?php
/* @var $this DealLocationsController */
/* @var $model DealLocations */

$this->breadcrumbs=array(
	'Deal Locations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DealLocations', 'url'=>array('index')),
	array('label'=>'Create DealLocations', 'url'=>array('create')),
	array('label'=>'Update DealLocations', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DealLocations', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DealLocations', 'url'=>array('admin')),
);
?>

<h1>View DealLocations #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'deal_id',
		'lat',
		'lng',
		'address',
		'city_id',
		'status',
		'updated',
		'created',
	),
)); ?>
