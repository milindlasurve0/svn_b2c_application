<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/new_1'); ?>
<div class="row-fluid">
<div class="span-5 ">


    <div id="content">
        <div class="panel panel-primary">
            <div class="panel-heading">Menu</div>
            <div class="list-group">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Admin', 'url' => array('/panel/admin'), 'visible'=> UsersComponent::isAdmin(Yii::app()->user->username)),
                        array('label' => 'Recharge', 'url' => array('/panel/recharge'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Wallet', 'url' => array('/panel/wallet'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Service', 'url' => array('/panel/services'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'billpayment', 'url' => array('/panel/billpayment'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Complain', 'url' => array('/complains/create'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Deals', 'url' => array('/deals'), 'visible' => UsersComponent::isAdmin()),
                        array('label' => 'voucher', 'url' => array('/panel/create_voucher'), 'visible' => !Yii::app()->user->isGuest),    
                        array('label' => 'users', 'url' => array('/userrs/index'), 'visible' => !Yii::app()->user->isGuest),    
                        //array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                        array('label' => 'Logout ', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                    ),
                    'htmlOptions' => array('class'=>'list-group'),
                    'id' => 'cmenu',
                    'itemCssClass'=>'list-group-item',
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="span-19">
    <div id="content">
<?php echo $content; ?>
    </div><!-- content -->
</div>
    
    <div class="span-5 last">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
    </div>
<?php $this->endContent(); ?>