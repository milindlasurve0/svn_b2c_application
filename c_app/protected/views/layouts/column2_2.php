<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/new_1'); ?>
<div class="row-fluid">
<div class="span-5 ">
    <div id="content">
        <div class="panel panel-primary">
            <div class="panel-heading">Menu</div>
            <div class="list-group">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Admin', 'url' => array('/panel/admin'), 'visible'=> UsersComponent::isAdmin(Yii::app()->user->username)),
                        array('label' => 'Search', 'url' => array('/panel/search'), 'visible'=> UsersComponent::isAdmin(Yii::app()->user->username)) ,
                        array('label' => 'Search', 'url' => array('/panel/search'), 'visible'=> UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'user_wise_Report', 'url' => array('/panel/user_reports'), 'visible'=> UsersComponent::isAdmin(Yii::app()->user->username)),
                        array('label' => 'user_wise_Report', 'url' => array('/panel/user_reports'), 'visible'=> UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'Recharge', 'url' => array('/panel/recharge'), 'visible' => !Yii::app()->user->isGuest && !UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'Wallet', 'url' => array('/panel/wallet'), 'visible' => !Yii::app()->user->isGuest && !UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'Service', 'url' => array('/panel/services'), 'visible' => !Yii::app()->user->isGuest&& !UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'billpayment', 'url' => array('/panel/billpayment'), 'visible' => !Yii::app()->user->isGuest&& !UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'Complain', 'url' => array('/complains/create'), 'visible' => !Yii::app()->user->isGuest && !UsersComponent::isReport(Yii::app()->user->username)),
                        array('label' => 'Deals', 'url' => array('/deals'), 'visible' => UsersComponent::isAdmin()),
                        array('label' => 'voucher', 'url' => array('/panel/create_voucher'), 'visible' => UsersComponent::isAdmin(Yii::app()->user->username)),    
                        array('label' => 'users', 'url' => array('/userrs/index'), 'visible' => UsersComponent::isAdmin(Yii::app()->user->username)),
                        array('label' => 'FreeGift', 'url' => array('/panel/freegift'), 'visible'=> UsersComponent::isAdmin(Yii::app()->user->username)),
                        array('label' => 'Announce FreeGift', 'url' => array('/deals/announcer'), 'visible' => UsersComponent::isAdmin()),
                        //array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)                        
                        array('label' => 'SET Transaction ', 'url' => array('/panel/settransaction'), 'visible' => UsersComponent::isAdmin(Yii::app()->user->username)),
                        array('label' => 'Web Section (Pay1.in)', 'url' => array('/web/web_section'), 'visible' => UsersComponent::isAdmin()),
                        array('label' => 'Web Meta Tag (Pay1.in)', 'url' => array('/web/web_meta_tags'), 'visible' => UsersComponent::isAdmin()),
                        array('label' => 'HR (Pay1.in)', 'url' => array('/web/HR_panel'), 'visible' => UsersComponent::isAdmin()),
                        //array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                        array('label' => 'Logout ', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                        ),
                    'htmlOptions' => array('class'=>'list-group'),
                    'id' => 'cmenu',
                    'itemCssClass'=>'list-group-item',
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="span-24">
    <div id="content">
<?php echo $content; ?>
    </div><!-- content -->
</div>
    </div>
<?php $this->endContent(); ?>
