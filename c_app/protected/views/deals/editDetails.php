<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
    
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="msg_flash flash-' . $key . '">' . $message . "</div>\n";
    }
   
    Yii::app()->clientScript->registerScript(
       'myHideEffect',
       '$(".msg_flash").animate({opacity: 1.0}, 5000).fadeOut("slow");',
       CClientScript::POS_READY
    );
?>
<div class="panel panel-default">
  <!-- Default deal contents -->
  <div class="panel-heading">
      <h4>
          Edit Deal #<?php echo $id   ;?>
          <a class="btn btn-default btn-small right" href="/deals">
              Deals List
          </a>
          <a id="btn_info_preview_<?php echo $id ;?>" data-target="#dealPreview<?php echo $id ;?>" 
             data-toggle="modal" href="/deals/preview/<?php echo $id ;?>" 
             class="btn btn-default btn-small right info_preview_button">
             Preview
          </a>
     </h4> 
  </div>
  <div class="panel-body">

      <?php
      $dealerName = '';
        if(!empty($model->dealer_id)){
            $i=0;
            foreach($dealersList as $dealer){
                if($dealer['id'] == $model->dealer_id){                    
                    break;
                }
                $i++;
            }
            $dealerName = $dealersList[$i]['user_name'];
        }
      ?>
    <form class="form-inline" role="form" id="deal_form" action="/deals/edit" method="POST"  enctype="multipart/form-data">
      <input id="id" name="id" value="<?php echo $model->id ; ?>" type="hidden" />
      <input id="dealer_name" name="dealer_name" value="<?php echo $dealerName; ?>" type="hidden" />
      <div class="form-group" style="padding: 5px;">
        <input id="name" name="name" value="<?php echo $model->name ; ?>" placeholder="Name" type="text" class="form-control" />
      </div>
      <div class="form-group" style="padding: 5px;">        
        <select id="dealer_id" name="dealer_id"   class="form-control" onchange="setDealerName()">
            <option value="">Dealer</option>
             <?php //print_r($dealersList);            
               foreach ($dealersList as $key => $dealer) { ?>
                    <option value="<?php echo $dealer['id']   ;?>" <?php echo $dealer['id']== $model->dealer_id ? "selected" : "" ; ?>><?php echo $dealer['user_name']   ;?></option>
               <?php } ?>
        </select>    
      </div>
    
    <!--Start deal_types table code in deal_location--> 
    <div class="form-group" style="padding: 5px;">
        <select id="type_id" name="type_id"   class="form-control" >
        <option value="">Types</option>
        <?php //print_r($typesList);
        foreach ($typesList as $key => $type) { ?>
                <option value="<?php echo $type['id']   ;?>"  <?php echo $type['id']== $model->type_id ? "selected" : "" ; ?>><?php echo $type['name']   ;?></option>
        <?php   }    ?>
       
        </select>    
    </div>
    <!--End deal_types table code in deal_location-->  
      <div class="form-group" style="padding: 5px;"> 
         <select id="category_id" name="category_id"   class="form-control" >
        <option value="">Category</option>
        <?php //print_r($categoriesList);
        foreach ($categoriesList as $key => $category) {    
          ?>
                <option value="<?php echo $category['id']   ;?>"  <?php echo $category['id']== $model->category_id ? "selected" : "" ; ?>><?php echo $category['name']   ;?></option>
        <?php   }    ?>
       
        </select>    
      </div>
      <div class="form-group" style="padding: 5px;">        
        <select id="status" name="status"  class="form-control" >
        <option value="">Status</option>
        <?php //print_r($categoriesList);
                foreach ($dealStatus as $key => $status) { ?>
                    <option value="<?php echo $key   ;?>" <?php echo $key== $model->status ? "selected" : "" ; ?>><?php echo $status   ;?></option>
        <?php   }    ?>
        </select>    
      </div>
        <div class="form-group" style="padding: 5px;">        
            <select id="Emails" name="Emails"  class="form-control" >
                <option value="">Select Email</option>
                <option value="nivedita@mindsarray.com">Nivedita</option>
                <option value="dhruven@pay1.in">Dhruven</option>
                <option value="manthan@pay1.in">Manthan</option>
                <option value="amrit@pay1.in">Amrit</option>
                <option value="mansi@pay1.in">Mansi</option>
                <option value="niranjan@mindsarray.com">Niranjan</option>
                <option value="shiraz@pay1.in">Shiraz</option>
                <option value="subhadeep@pay1.in">Subhdeep</option>
            </select>    
      </div>
       <div class="form-group" style="padding: 5px;">        
            <select id="reason" name="reason"  class="form-control" >
                <option value="">Select Reason to close</option>
                 <?php
                foreach ($reasons as $row) { ?>
                    <option value="<?php echo $row['id'];?>" <?php echo $row['id']== $model->reason ? "selected" : "" ; ?>><?php echo $row['reason'];?></option>
        <?php   }    ?> 
            </select>    
      </div>
<!--          <div class="form-group">
                <input id="mainimage" name="main_image_url" value="" placeholder="upload image" type="file" style="width: 190px;"/>
          </div>-->
      <div class="form-group" style="padding: 5px;">
           <select id="coupon_type" name="coupon_type"   class="form-control" onchange = "selectCouponType()">
        <?php 
        foreach ($couponTypeList as $key => $ctype) { ?>
                <option value="<?php echo $ctype['id']   ;?>"  <?php echo $ctype['id']== $model->coupon_type ? "selected" : "" ; ?>><?php echo $ctype['name']   ;?></option>
        <?php }    ?>
       
        </select>  
      </div>
      <div class="form-group" id = "coupon_prefix_div" style="padding: 5px;">
         <input id="coupon_prefix" name="coupon_prefix" value="<?php echo $model->coupon_prefix  ; ?>" placeholder="Coupon Name" type="text" class="form-control"  />
      </div>
      <div class="form-group" id = "company_url_div" style="padding: 5px;">
         <input id="company_url_id" name="company_url_name" value="<?php echo $model->company_url  ; ?>" placeholder="Company URL" type="text" class="form-control"  />
      </div>
      <div class="form-group" style="padding: 5px;">
          <input id="deal_url" name="deal_url" placeholder="Gift custom url" value="<?php echo $model->deal_url; ?>" type="text" class="form-control" />
      </div>
        <button type="submit" class="btn btn-success">Save</button>
     </form>
      <div class="list-group">
       <a href="#offers" id="offers" class="expandDiv list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>DETAILS</a>
       <div id="offersDiv" style="display:none;">
           
          <!-- Table -->
      <table class="table">
        <thead> <th>#ID</th><th>Name</th><th>Price (Offer / Actual)</th><th>Stock (Sold/Total)</th><th>Validity (Mode)</th><!--<th>Image</th>--><th>Sequence</th>
          <th>Short Desc</th><th>Long Desc</th><th>Offer Desc</th><th>Status</th></th><th>Min Amount</th></th><th>Per User</th>
      <th>Created</th><th ><button class="btn btn-default btn-small right" data-toggle="modal" data-target="#addOfferModal">
              Add New Offer
            </button></th></thead>
          <tbody>
              <?php

              foreach ($offersList as $key => $offer) { ?>


              <tr><td><?php echo $offer["id"] ;?></td><td><?php echo $offer["name"] ;?></td><td><?php echo $offer["offer_price"]." / ".$offer["actual_price"] ;?></td><td><?php echo  $offer["stock_sold"]." / ".$offer["total_stock"] ; ?></td><td><?php echo $offer["validity"]." ( " ; echo isset($offerValidityModes[$offer["validity_mode"]]) ? $offerValidityModes[$offer["validity_mode"]]." )" : "- )" ;?></td><!--<td style="text-wrap:normal;"><?php echo  $offer["img_url"] ; ?></td>--><td style="text-wrap:normal;"><?php echo  $offer["sequence"] ; ?></td>
              <td>
              <?php
              $this->widget('ext.expander.Expander',array(
                            'content'=>$offer["short_desc"],
                            'config'=>array('slicePoint'=>20, 'expandText'=>'read more', 'userCollapseText'=>'read less', 'preserveWords'=>false)
              ));
              ?>  
                  </td><td><?php 
              $this->widget('ext.expander.Expander',array(
                    'content'=>$offer["long_desc"],
                    'config'=>array('slicePoint'=>20, 'expandText'=>'read more', 'userCollapseText'=>'read less', 'preserveWords'=>false)
                ));
              //echo $offer["short_desc"] ;
              ?></td>
                  <td><?php 
              $this->widget('ext.expander.Expander',array(
                    'content'=>$offer["offer_desc"],
                    'config'=>array('slicePoint'=>20, 'expandText'=>'read more', 'userCollapseText'=>'read less', 'preserveWords'=>false)
                ));
              //echo $offer["short_desc"] ;
              ?></td>
                  <td><?php echo isset($offerStatus[$offer["status"]]) ? $offerStatus[$offer["status"]] : "-" ?></td>
                  <td><?php echo $offer["min_amount"] ;?></td><td><?php echo $offer["max_limit_peruser"] ;?></td>
                  <td><?php echo $offer["created"] ;?></td>
                  <td width="140">
                      <a id="btn_offer_edit_<?php echo $offer["id"] ;?>" class="btn btn-default btn-small offer_edit_button" data-toggle="modal" href="/modals/deals/editOffer/<?php echo $offer["id"] ;?>" data-target="#editOfferModal">Edit</a>
                      <a id="btn_offer_delete_<?php echo $offer["id"] ;?>" href="delete/<?php echo $offer["id"] ;?>" class="btn btn-default btn-small offer_delete_button">Delete</a>
                  </td></tr>
        
            <?php  } ?>
         </tbody>
      </table>
          
    </div>
        
    <!--Start of OFFER INFO View Code-->  
     <a href="#offer_info" id="offer_info" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>OFFER INFO</a>
      <div id="offer_infoDiv" style="display:none">
          <table class="table">
        <thead> <th>#ID</th><th>Time</th><th>Days</th><th>Appointment</th><th>Exclusive M/F</th>
        <th>Dealer Contact</th><th><button class="btn btn-default btn-small right" data-toggle="modal" data-target="#addOfferInfoModal">
              Add OFFER INFO
            </button></th></thead>
        <tbody>
              <?php

              foreach ($offerInfoList as $key => $offer) { ?>

              <tr><td><?php echo $offer["id"] ;?></td><td><?php echo $offer["time"] ;?></td><td><?php echo $offer["day"] ;?></td>
                  <td><?php echo  $offer["appointment"]; ?></td><td><?php echo $offer["exclusive_gender"] ;?></td>
                  <td><?php echo  $offer["dealer_contact"] ; ?></td>
              <td width="140">
              <a id="btn_offer_info_edit_<?php echo $offer["id"] ;?>" class="btn btn-default btn-small offer_info_edit_button open-modal" data-toggle="modal" href="/modals/deals/editOfferInfo/<?php echo $offer["id"] ;?>" data-target="#editOfferInfoModal">Edit</a>    
              <a id="btn_offer_info_delete_<?php echo $offer["id"] ;?>" class="btn btn-default btn-small offer_info_delete_button">Delete</a>
              </td>
              <?php } ?>
          </tbody>
      </table>
      
      </div>
    <!--End of OFFER INFO View Code-->     
        
    <!--Start of IMAGE SECTION of LOGO View Code-->  
        
    <a href="#img_Section" id="img_Section" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>IMAGE SECTION</a>
      <div id="img_SectionDiv" style="display:none">
          
       <table class="table">
        <!--id, deal_id, title, content_txt, sequence, status ,sequence, status -->
        <!--discount ng_desc 	created -->
        <thead> <th>#ID</th><th>Logo Image url</th><th>Image info</th><th>Uploaded Date</th><th >
            <button class="btn btn-default btn-small right" data-toggle="modal" data-target="#addImgSectionModel">
              Add New Logo Image
            </button>
        </th></thead>
          <tbody>
              
              <?php
              foreach ($deal_logo_list as $key => $image_detail) { ?>
              <tr>
                  <td><?php echo $image_detail["id"] ;?></td><td><?php echo $image_detail["image_url"] ;?></td>
                  <td><?php echo $image_detail["image_info"] ;?></td><td><?php echo $image_detail["uploaded_date"] ;?></td>
                  <td width="140">
                      <a id="btn_location_delete_<?php echo $image_detail["id"] ;?>" href="/deals/remove_logo_image/id/<?php echo $image_detail["id"] ;?>/deal_id/<?php echo $id ;?>" class="btn btn-default btn-small location_delete_button">Delete</a>
                  </td>
              </tr>        
            <?php  } ?>
        </tbody>
      </table>
      </div>
    
    <!--End of IMAGE SECTION of LOGO View Code-->     
    
    <a href="#deals" id="deals" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>DEAL SECTIONS</a>
      <div id="dealsDiv" style="display:none">
          
       <table class="table">
        <thead> <th>#ID</th><th>Title</th><th>Sequence</th><th>Status</th><th >
            <button class="btn btn-default btn-small right" data-toggle="modal" data-target="#addInfoModal">
              Add New Deal Section
            </button>
        </th></thead>
          <tbody>
              <?php

              foreach ($infosList as $key => $info) { ?>
              <tr><td><?php echo $info["id"] ;?></td><td><?php echo $info["title"] ;?></td><td><?php echo $info["sequence"] ;?></td><td><?php echo  $info["status"]; ?></td>
                  <td width="140">
                      <a id="btn_info_preview_"<?php echo $info["id"] ;?>" class="btn btn-default btn-small info_preview_button" data-toggle="modal" href="/modals/deals/previewInfoModal/<?php echo $info["id"] ;?>" data-target="#previewInfoModal">Preview</a>
                      <a id="btn_info_edit_<?php echo $info["id"] ;?>" class="btn btn-default btn-small info_edit_button open-modal" data-toggle="modal" data-remote="/modals/deals/editInfo/<?php echo $info["id"] ;?>" data-target="#editInfoModal">Edit</a>
                      <a id="btn_info_delete_<?php echo $info["id"] ;?>" href="previewInfo/<?php echo $info["id"] ;?>" class="btn btn-default btn-small info_delete_button">Delete</a>
                  </td>
              </tr>
        
            <?php  } ?>
         </tbody>
      </table>
          
          
      </div>
      
    <!--Commented LOCATIONS LIST-->  
<!--      <a href="#locations" id="locations" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>LOCATIONS LIST</a>
      <div id="locationsDiv" style="display:none">
          <table class="table">
        deal_id 	name 	offer_price 	actual_price 	discount 	total_stock 	stock_sold 	validity 	validity_mode 	status 	short_desc 	long_desc 	created 
        discount ng_desc 	created 
        <thead> <th>#ID</th><th>Address</th><th>City</th><th>Lattitude</th><th>Longitude</th><th>status</th><th >Created</th><th ><button class="btn btn-default btn-small right" data-toggle="modal" data-target="#addLocationModal">
              Add New Location
            </button></th></thead>
          <tbody>
              <?php

//              foreach ($locationList as $key => $location) { ?>
              <tr><td><?php // echo $location["id"] ;?></td><td><?php // echo $location["address"] ;?></td><td><?php // echo $location["city_name"] ;?></td><td><?php // echo  $location["lat"]; ?></td><td><?php // echo  $location["lng"]; ?></td><td><?php // echo isset($locationStatus[$location["status"]]) ? $locationStatus[$location["status"]] :" - " ;?></td>
                  <td><?php // echo $location["created"] ;?></td>
                  <td width="140">
                      <a id="btn_location_edit_<?php // echo $location["id"] ;?>" class="btn btn-default btn-small location_edit_button" data-toggle="modal" href="/modals/deals/editLocation/<?php // echo $location["id"] ;?>" data-target="#editLocationModal">Edit</a>
                      <a id="btn_location_delete_<?php // echo $location["id"] ;?>" href="delete/<?php // echo $location["id"] ;?>" class="btn btn-default btn-small location_delete_button">Delete</a>
                  </td></tr>
        
            <?php //  } ?>
         </tbody>
      </table>
      
      </div>-->
      
      <a href="#image_gallery" id="image_gallery" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>IMAGE GALLERY</a>
      <div id="image_galleryDiv" style="display:none">
          <table class="table">
        <!--deal_id 	name 	offer_price 	actual_price 	discount 	total_stock 	stock_sold 	validity 	validity_mode 	status 	short_desc 	long_desc 	created -->
        <!--discount ng_desc 	created --> 
        <thead> <th>#ID</th><th>Image url</th><th>Image info</th><th>Uploaded Date</th><th ><button class="btn btn-default btn-small right" data-toggle="modal" data-target="#addImageModal">
              Add New Gallery Image
            </button></th></thead>
         <tbody>
              <?php
              foreach ($deal_image_list as $key => $image_detail) { ?>
              <tr>
                  <td><?php echo $image_detail["id"] ;?></td><td><?php echo $image_detail["image_url"] ;?></td>
                  <td><?php echo $image_detail["image_info"] ;?></td><td><?php echo $image_detail["uploaded_date"] ;?></td>
                  <td width="140">
                      <a id="btn_location_delete_<?php echo $image_detail["id"] ;?>" href="/deals/remove_image/id/<?php echo $image_detail["id"] ;?>/deal_id/<?php echo $id ;?>" class="btn btn-default btn-small location_delete_button">Delete</a>
                  </td>
              </tr>        
            <?php  } ?>
         </tbody>
      </table>
      
      </div>      
    
    <!--Start of SET_ADDRESS_ON_MAP Code-->
      <a href="#set_map" id="set_map" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>LOCATIONS LIST</a>
      <div id="set_mapDiv" style="display:none">
          <table class="table">
              <thead><thead> <th>#ID</th><th>Address</th><th>Area</th><th>City</th><th>Latitude</th><th>Longitude</th><th>Contact</th><th>Status</th><th >Created</th><th ><button class="btn btn-default btn-small center-block" data-toggle="modal" data-target="#addMapModal" onclick="getLocation()">
              Add New Location
            </button></th></thead>
          <tbody>
            <?php
              $map_lat;
              $map_lng;
              $i = 0 ;
            foreach ($mapList as $key => $map) { ?>
            <?php 
              $map_lat = $map["lat"];
              $map_lng = $map["lng"];
              $map_add = $map["address"];
              $map_ID = $map["id"];
              $map_full_add = $map["full_address"];
              $map_shop = $map["shop"];
//            $dealer_contact = $map["dealer_contact"];
               
              $contact_arr  = explode(",",$map["dealer_contact"]);              
              $dealer_contact = implode("<br>", $contact_arr);
              
        // If map full_address value is empty or null
            if (empty($map_full_add) || is_null($map_full_add)){
              $map_full_add = ' ';
            }
        // If map address value is empty or null
            if (empty($map_add) || is_null($map_add)){
              $map_add = ' ';
            }
            ?>
              <tr><td><?php echo $map["id"] ;?></td>
                  <td><?php echo $map["address"] ;?></td>
                  <td><?php echo $map["area_name"] ;?></td>
                  <td><?php echo $map["city_name"] ;?></td>
                  <td><?php echo  $map["lat"]; ?></td><td><?php echo  $map["lng"]; ?></td>
                  <td><?php echo $dealer_contact;?></td><td><?php echo $map["status"];?></td>
                  <td><?php echo $map["created"] ;?></td>
                  <td width="140">
                <!--<button class="btn btn-default btn-small map_edit_button" data-toggle="modal" data-target="#addMapModal" style = "margin-left: 20px;" onclick="editMap('<?php echo $map_lat; ?>','<?php echo $map_lng; ?>','<?php echo $map_add; ?>','<?php echo $map_ID; ?>','<?php echo $map_full_add; ?>')">Edit</button>-->
                <button class="btn btn-default btn-small map_edit_button" data-toggle="modal" data-target="#addMapModal" style = "margin-left: 20px;" onclick="editMap('<?php echo $map_lat; ?>','<?php echo $map_lng; ?>' , '<?php echo $map_ID; ?>', '<?php echo $i ?>')">Edit</button>
                <a id="btn_map_delete_<?php echo $map["id"] ;?>" href="delete/<?php echo $map["id"] ;?>" class="btn btn-default btn-small map_delete_button" style="padding-left: 10px;">Delete</a>
                <input type="hidden"   name="map_shop"    id="map_shop<?php echo $i ?>"    size = "20" value = "<?php echo $map_shop; ?>" />
                <input type="hidden"   name="map_add"    id="map_add<?php echo $i ?>"    size = "20" value = "<?php echo $map_add; ?>" placeholder="Address" />
                <input type="hidden"   name="h_dealer_contact"    id="h_dealer_contact<?php echo $i ?>"    size = "20" value = "<?php echo $dealer_contact; ?>" />
                <input type="hidden"   name="map_full_add" id="map_full_add<?php echo $i ?>" size = "100" value = "<?php echo $map_full_add; ?>" placeholder="Full Address" /><br><br>
            
                  </td>
              </tr>
            <?php  $i++ ;} ?>
                
          </tbody>
          </table>
      
      </div>
    <!--End of SET_ADDRESS_ON_MAP Code-->
    
    <!--Start of IMAGE TEST for Testing Crop Image View Code-->
    <a href="#img_Test" id="img_Test" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>IMAGE CROP</a>
      <div id="img_TestDiv" style="display:none">
          
       <table class="table">
        <thead> 
        <form id ="main_form" action="/deals/addtestImage" method="post"  onsubmit="return AddTestImage(event)" enctype="multipart/form-data">
            <tr><th><input id="location_deal_id" name="test_image_deal_id" value="<?php echo $id   ;?>" type="hidden" ></th>
                <th><input id="test_image_desc" name="test_image_desc" value="<?php echo "Deal_".$id."_IMG" ;?>"  
                           placeholder="Crop Image info" type="hidden" class="form-control"/>
                 <th><input id="upload_img" name="test_image_file" value="" placeholder="Test Image" type="file"  
                 onchange="submitform()"   style="width:120%" ></th>  
                <th><input id="crop_show_id" name="crop_show_id" value="<?php // echo $crop_show = 1 ;?>" type="hidden" ></th>
            <th></th>
            
        </form></tr>
<!--        <tr><th>#ID</th><th>Crop Image url</th><th>Image info</th><th>Uploaded Date</th>
        <th><button id = "crop_button" class="btn btn-default btn-small right" data-toggle="modal" data-target="#addImgTestModel">
              Crop Image
        </button></th></tr>-->
        </thead>
          <tbody>
              
              <?php
              foreach ($deal_test_list as $key => $image_detail) { ?>
              <tr>
                  <td><?php echo $image_detail["id"] ;?></td><td><?php echo $image_detail["image_url"] ;?></td>
                  <td><?php echo $image_detail["image_info"] ;?></td><td><?php echo $image_detail["uploaded_date"] ;?></td>
                  <td width="140">
                    <a id="btn_crop_img_edit_<?php echo $image_detail["id"] ;?>" class="btn btn-default btn-small crop_img_edit_button"  href="/deals/cropimage/<?php echo $image_detail["id"] ;?>" >Edit</a>
                    <a id="btn_crop_img_delete_<?php echo $image_detail["id"] ;?>" class="btn btn-default btn-small crop_img_edit_button" href="/deals/remove_crop_image/id/<?php echo $image_detail["id"] ;?>/deal_id/<?php echo $id ;?>" class="btn btn-default btn-small location_delete_button">Delete</a>
                  </td>
              </tr>        
            <?php  } ?>
        </tbody>
      </table>
      </div>
    <!--End of TEST IMAGE for Crop Image View Code-->     
    
    <!--Start of Dealer Coupon View Code-->
    <div>
        <a href="#dealer_coupon" id="dealer_coupon" class="expandDiv down list-group-item"><span class="right glyphicon glyphicon-chevron-down"></span>DEALER COUPON</a>
        <div id="dealer_couponDiv" style="display:none">
            <form action="/deals/addCoupons" method="post" enctype="multipart/form-data">
                <input id="id" name="id" value="<?php echo $model->id ; ?>" type="hidden" />
              <div style="padding-top:10px">
                <div style="float:left">
                    <input type="file" name="file1" id="file1">
                </div>
                    <div style="float:left">
                      <select id="dealer" name="dealer"   class="form-control" >
                        <option value="">Dealer</option>
                         <?php //print_r($dealersList);            
                          foreach ($dealersList as $key => $dealer) { ?>
                        <option value="<?php echo $dealer['id'];?>" <?php echo $dealer['id']== $model->dealer_id ? "selected" : "" ; ?>><?php echo $dealer['user_name']   ;?></option>
                         <?php } ?>
                      </select>  
                    </div>
                <div style="float:left;padding-left:10px">
                      <input type="submit" value="Submit" name="Submit">
                </div>
              </div>
            </form>
        </div>
    </div>
    <!--End of Dealer Coupon View Code-->
    
    
    <!--Start Preview Modal--> 
        <div class="modal fade" id="dealPreview<?php echo $id ;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" >
<!--                        <div class="modal-body">
                            <?php // echo "Hello Preview Modal";
                            ?>
                        </div>    -->
                    </div>
                </div>
        </div>
  
    
    <!--End Preview Modal--> 
    
    <!--Start of  Offer Info Modal  Code-->
    <div class="modal fade" id="addOfferInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Offer Info Modal Title</h4>
          </div>
          <form class="form-inline" role="form" id="offer_info_form" action="/deals/createOfferInfo" method="POST" >
          <div class="modal-body">
            <input id="offer_info_deal_id" name="offer_info_deal_id" value="<?php echo $id ;?>" placeholder="Name" type="hidden"/>
          <div class="form-group">
            <!--<input id="Start_time" name="Start_time" value=""  type="time" class="form-control"/>-->  
              <input id="Start_time" onkeyup="selectTime('full_time')" name="Start_time" value="10" placeholder="Time" type="text" class="form-control" style="width: 56px;"/>
          </div>
              
           <div class="form-group">
               <select id="Period_1" name="Period_1" value ="AM" class="form-control" onchange= "selectTime('full_time')">
                <option value="AM" selected >
                    AM
                </option>
                <option value="PM" >
                    PM
                </option>
           </select> 
           </div>
            
          <div class="form-group">
               <label for="To" >  To  </label>
          </div>
              
          <div class="form-group">
            <!--<input id="End_time" name="End_time" value="" type="time" class="form-control"/>-->  
            <input id="End_time" name="End_time" onkeyup="selectTime('full_time')" value="10" placeholder="Time" type="text" class="form-control" style="width: 56px;"/>
          </div>
          
            <select id="Period_2" name="Period_2"  class="form-control"  onchange = "selectTime('full_time')" >
                <option value="AM">
                    AM
                </option>
                <option value="PM" selected>
                    PM
                </option>
          </select>
        <input id="full_time" name="full_time" value="10 AM To 10 PM" placeholder="Time" type="text" class="form-control"/>       
        <br>    
          <div class="form-group">
              <input id="Week_days" name="Week_days" value="6 Days in a week (Sunday Closed)" placeholder="Week Days" type="text" 
                     class="form-control" style="width: 250px;"/>
          </div>
           <input id="days" name="days" value="Days" type="button" onclick="default_day()" class="btn btn-primary"/><br> 
         <div class="form-group">
            <input id="Appoinment_Option" name="Appoinment_Option" value="Prior appointment is mandatory" placeholder="Apppoinment Option" type="text" 
                     class="form-control" style="width: 250px;"/>
         </div>
          <input id="days" name="Appoinment" value="Appointment" type="button" onclick="default_appointment()" class="btn btn-primary"/><br> 
         <div class="form-group">
           <input id="Gender" name="Gender" value="Exclusive for men and women" placeholder="Male/Female" type="text" 
                     class="form-control" style="width: 250px;"/>
         </div>
          <input id="gender" name="gender" value="Gender" type="button" onclick="default_gender()" class="btn btn-primary"/><br> 
         <div class="form-group">
             <input id="Dealer_Contact" name="Dealer_Contact" value="" placeholder="Dealer Contact" type="text" class="form-control"/>
         </div> 
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
       </div>
      </div>
    <!--End of  Offer Info Modal  Code-->

    <!--Start of Logo Image Section Modal Code--> 
    <div class="modal fade" id="addImgSectionModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add Logo Image</h4>
          </div>
            <form class="form-inline" role="form" id="location_form" action="/deals/addlogoImage" method="POST" onsubmit="return AddLogoImage(event)" enctype="multipart/form-data">
              <div style="padding-left: 134px">
                  <label id="logoimageSizeError" style="display:none;color:red">Too big image to upload.Please upload image having size less than 50kb.</label>
                  <br/>
                  <label id="logoimageInfoError" style="display:none;color:red">Image info should not be blank.</label>
              </div>
          <div class="modal-body">
              <input id="location_deal_id" name="logo_image_deal_id" value="<?php echo $id   ;?>" type="hidden" />
          
          <div class="form-group">
              <label for="logo_image_file">Upload Image</label>  
            <input id="logo_image_file" name="logo_image_file" value="" placeholder="Logo Image" type="file"  style="width:100%"/>
          </div>
              
          <div class="form-group">
            <label for="logo_image_desc">Image Info</label>
            <input id="logo_image_desc" name="logo_image_desc" value="" placeholder="Logo Image info" type="text" class="form-control"/>            
          </div><br/>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" onclick="unsetAddLogoImage();">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
      </div>
    </div>    
    
    <!--End of Logo Image Section Modal Code--> 
    
    <!--Start of Test Image Modal Code--> 
    <div class="modal fade" id="addImgTestModel"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Crop Image</h4>
          </div>
            <form class="form-inline" role="form" id="location_form" action= "/deals/addcropImage" method="POST"  enctype="multipart/form-data">
              <div style="padding-left: 134px">
                  <label id="testimageSizeError" style="display:none;color:red">Too big image to upload.Please upload image having size less than 50kb.</label>
                  <br/>
                  <label id="testimageInfoError" style="display:none;color:red">Image info should not be blank.</label>
              </div>
          <div class="modal-body">
              <input id="location_deal_id" name="crop_image_deal_id" value="<?php echo $id   ;?>" type="hidden" />
          
            <?php
              
              $test_url = "";
              $test_desc = "";
              $test_img_id = "";
              foreach ($deal_test_list as $key => $image_detail) { 
                  $test_url = isset($image_detail['image_url']) ? $image_detail['image_url'] : " ";
                  $test_desc = isset($image_detail['image_info']) ? $image_detail['image_info'] : " ";
                  $test_img_id = isset($image_detail['id']) ? $image_detail['id'] : " ";
            ?>
              
<!--
                  <td><?php // echo $image_detail["id"] ;?></td><td><?php // echo $image_detail["image_url"] ;?></td>
                  <td><?php // echo $image_detail["image_info"] ;?></td><td><?php // echo $image_detail["uploaded_date"] ;?></td>
               </tr>        -->
            <?php } ?>
      
        <div class="form-group">
            <div id="pholder">
             <div id="pbox">
              <div id="pbody">
               <div class="jc_coords">
                   
            <img id="cropbox" src="<?php echo $test_url  ;?>" width ="550" height ="450" />
            <input type="hidden" id="crop_image_url" name="orginal_image_url" value = "<?php echo $test_url ;?>"/>
            <input type="hidden" id="crop_image_info" name="orginal_image_desc" value = "<?php echo $test_desc ;?>"/>
            <input type="hidden" id="crop_image_id" name="crop_image_id" value = "<?php echo $test_img_id ;?>"/>
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                
              </div>
              </div>
             </div>
            </div>
            
            
          </div> 
        </div>    
        <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" onclick="unsetAddTestImage();">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
       </div>
    </div>    
    
    <!--End of Test Image Modal Code--> 
    
    <!--Start of DETAILS addOfferModal code -->
    <div class="modal fade" id="addOfferModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel" >DETAILS MODAL</h4>
          </div>
          <form class="form-inline" role="form" id="offer_form" action="/deals/createOffer" method="POST">
          <div class="modal-body">
            
              <input id="offer_deal_id" name="offer_deal_id" value="<?php echo $id   ;?>" placeholder="Name" type="hidden" />
          <div class="form-group">
            <input id="offer_name" name="offer_name" value="" placeholder="Name" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="offer_offer_price" name="offer_offer_price" value="" placeholder="Offer Price" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="offer_actual_price" name="offer_actual_price" value="" placeholder="Actual Price" type="text" class="form-control"/>
         </div>
          <div class="form-group">
            <input id="offer_total_stock" name="offer_total_stock" value="" placeholder="Total Stock" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="offer_validity" name="offer_validity" value="" placeholder="Validity" type="text" class="form-control"/>
         </div>
          <div class="form-group">
<!--            <input id="offer_validity_mode" name="offer_validity_mode" value="" placeholder="Validity Mode" type="text" class="form-control"/>-->
        <select id="offer_validity_mode" name="offer_validity_mode"  class="form-control" >
            <option value="">Validity Mode</option>
            <?php //print_r($categoriesList);
                    foreach ($offerValidityModes as $key => $mode) { ?>
                        <option value="<?php echo $key   ;?>"><?php echo $mode   ;?></option>
            <?php   }    ?>
        </select>
         </div>
          <div class="form-group">
            <input id="offer_sequence" name="offer_sequence" value="" placeholder="Sequence" type="text" class="form-control"/>
          </div>
          <!--<div class="form-group">
            <input id="offer_img_url" name="offer_img_url" value="" placeholder="Image Url" type="text" class="form-control"/>
          </div>-->
          <div class="form-group">
<!--            <input id="offer_status" name="offer_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="offer_status" name="offer_status"  class="form-control" >
                <option value="">Status</option>
                <?php //print_r($categoriesList);
                        foreach ($offerStatus as $key => $status) { ?>
                            <option value="<?php echo $key   ;?>"><?php echo $status   ;?></option>
                <?php   }    ?>
            </select>
          </div>
          <div class="form-group" id="offer_min_amount">
            <input id="offer_min_amount" name="offer_min_amount" value="" placeholder="min amount" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="offer_max_limit_peruser" name="offer_max_limit_peruser" value="" placeholder="per user" type="text" class="form-control"/>
          </div>
           <div class="form-group">
            <select id="offer_global_flag" name="offer_global_flag"  class="form-control" >
                <option value="" >
                   Global flag
                </option>
                <option value="0" selected>
                    With Location
                </option>
                <option value="1" >
                    Without Location
                </option>
            </select>
          </div>
          <div class="form-group">
            <input id="offer_order_flag" name="offer_order_flag" value="" placeholder="order flag" type="text" class="form-control"/>
          </div>
            <div class="form-group" >
           <?php
              if($couponType[0]['coupon_type'] ==0 ){    
                 $coupon_expiry_date = strtotime("+7 day"); 
                 $coupon_expiry_date = date('Y-m-d', $coupon_expiry_date);
                 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'coupon_expiry_date',    
                'value'=> isset($coupon_expiry_date)?$coupon_expiry_date:"",
                'options'=>array(        
                    'showButtonPanel'=>true,
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'style'=>'',
                    'placeholder'=>'Coupon expiry date',
                    'class'=>'form-control'
                ),
            ));
                  } ?>
          </div>

<!--          <div class="form-group" >
            <input id="offer_category" name="offer_category" value="" placeholder="Category" type="text" class="form-control"/>
          </div>-->
          <div class="form-group" style="width:80%">
            <input id="offer_short_desc" name="offer_short_desc" value="" placeholder="Short Description" type="text" class="form-control" style="width:100%"/>
          </div>
              <br/>
          <div class="form-group" style="width:80%">
            <textarea id="offer_long_desc" name="offer_long_desc" placeholder="Long Description" class="form-control" style="width:100%"></textarea>
            </div>
              <br />
              <div class="form-group" style="width:80%">
                <input id="offer_short_desc" name="offer_of_desc" value="" placeholder="Offer Description" type="text" class="form-control" style="width:100%"/>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
      </div>
    </div>
    <!--End of DETAILS addOfferModal code -->
    
   <!--  Comment addLocationModal   
   <div class="modal fade" id="addLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content" style="width:80%">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add-Location</h4>
          </div>
          <form class="form-inline" role="form" id="location_form" action="/deals/createLocation" method="POST">
          <div class="modal-body">
            
              <input id="location_deal_id" name="location_deal_id" value="<?php echo $id   ;?>" type="hidden" />
          
          <div class="form-group" style="width:80%">
            <input id="location_address" name="location_address" value="" placeholder="Address" type="text" class="form-control" style="width:100%"/>
          </div>
              <div class="form-group">
            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>
            <select id="location_state" name="location_state"  class="form-control location_city_search" >
                <option value="">State</option>
                <?php //print_r($categoriesList);
//                        foreach ($statesList as $key => $value) { ?>
                            <option value="<?php // echo $value['id']   ;?>"><?php // echo $value['name']   ;?></option>
                <?php //   }    ?>
            </select>
          </div>
          <div class="form-group">
            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>
            <select id="location_city" name="location_city"  class="form-control location_city_fill" >
                <option value="">City</option>                
            </select>
          </div><br/>
          
          <input id="location_name" name="location_lat" value="" placeholder="Latitude" type="text" class="form-control"/>
          <input id="location_location_price" name="location_lng" value="" placeholder="Longitude" type="text" class="form-control"/>
         
          <div class="form-group">
            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>
            <select id="location_status" name="location_status"  class="form-control" >
                <option value="">Status</option>
                <?php //print_r($categoriesList);
//                        foreach ($locationStatus as $key => $status) { ?>
                            <option value="<?php // echo $key   ;?>"><?php // echo $status   ;?></option>
                <?php //   }    ?>
            </select>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
      </div>
    </div>
   End of Comment addLocationModal   -->
    <!---------- -->
    <div class="modal fade" id="addImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add Image</h4>
          </div>
            <form class="form-inline" role="form" id="location_form" action="/deals/addImage" method="POST" onsubmit="return AddImage(event)" enctype="multipart/form-data">
              <div style="padding-left: 134px">
                  <label id="imageSizeError" style="display:none;color:red">Too big image to upload.Please upload image having size less than 50kb.</label>
                  <br/>
                  <label id="imageInfoError" style="display:none;color:red">Image info should not be blank.</label>
              </div>
          <div class="modal-body">
              <input id="location_deal_id" name="image_deal_id" value="<?php echo $id   ;?>" type="hidden" />
          
          <div class="form-group">
              <label for="image_file">Upload Image</label>  
            <input id="image_file" name="image_file" value="" placeholder="Image" type="file"  style="width:100%"/>
          </div>
              
          <div class="form-group">
            <label for="image_desc">Image info</label>
            <input id="image_desc" name="image_desc" value="" placeholder="Image info" type="text" class="form-control"/>            
          </div><br/>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" onclick="unsetAddImage();">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
      </div>
    </div>    
    
    <!--Start of Map Modal Code-->
    <div class="modal fade" id="addMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog"  style = "width:800px; height:800px;">
           <div class="modal-content" style = "width:800px; height:800p">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 class="modal-title" id="myModalLabel">Show Address On Map</h4>
            </div>
          
            <form class="form-inline" role="form" id="map_form" action="/deals/SetMap" method="POST">
            <div class="modal-body">
                <input id="map_deal_location_id" name="map_deal_location_id" type="hidden" />
                <input id="map_deal_id" name="map_deal_id" value="<?php echo $id   ;?>" type="hidden" />
            <div class="form-group">
              <div class="padding10">
                <label for="lat">Latitude</label>
                <input id="lat" name="lat" placeholder="lat coordinate" type="text" class="form-control"/>
              </div>
            </div>
            <div class="form-group">
              <div class="padding10">
                <label for="lng">Longitude</label>
                <input id="lng" name="lng" placeholder="long coordinate" type="text" class="form-control"/>
              </div>
            </div>
            <div class="form-group">
              <div class="padding10">
                  <button type="button" class="btn btn-primary" name="Submit" onclick="initialize()">Show Location </button>
              </div>
            </div>
    
    <!--Start of Show_Address_On_Map_Code--> 
            <div id="latlongmap" style="height:500px; width:400px;float:left;">
            </div>
            <div class="form-group" style="float:left;margin-left: 0px;width: 317px;padding-left: 2‒;padding-left: 60px;padding-top: 16px;">
            <div class="padding10">
        <!--Script used for dynamic textbox and get its value at same page -->
        <script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
            <label>Shop Details</label><br>
             <textarea rows="4" cols="20" name="shop" id="shop" form="map_form" placeholder="Shop Details (Optional)"></textarea><br>
             <label>Landmark Area</label><br>
             <textarea rows="4" cols="20" name="near_loc" id="near_loc" form="map_form" placeholder="Landmark Area (Optional)"></textarea><br><br>
             <label>Address Detail</label><br>
             <textarea rows="5" cols="25" name="gadres" id="gadres" form="map_form" readonly placeholder="Full Address"></textarea><br>
             <button type="button" name="Submit1 "class="btn btn-primary" onclick= "codeAddress()" 
             style="padding-left: 30px; padding-right: 30px; border-left-width: 12px; border-right-width: 12px;">Show Address</button>
             <!--    <input type="text" name="dealer_contact" id="dealer_contact"  size = "20" form="map_form" placeholder="Dealer contact's" />-->
            <label>Dealer Contacts</label><br>
             <div id="dealer_contact_div" name="dealer_contact_div">
                <input type="text" name="dealer_contact" id="dealer_contact"  size = "20" form="map_form" placeholder="Dealer contact's" />         
                <input type="button" id="btAdd" value="+" class="bt">
                <input type="button" id="btRemove" value="-" class="bt">
                <p id="demo_add" name="demo_add"></p>
             </div>
      
            <input type="hidden"   name="city"     id="city"     size = "20" placeholder="City" />
            <input type="hidden"   name="country"  id="country"  size = "20" placeholder="Country" />
            <input type="hidden"   name="pin_code" id="pin_code" size = "20" placeholder="Pin code" />
            <input type="hidden"   name="state"    id="state"    size = "20" placeholder="State" />
            <input type="hidden"   name="route"    id="route"    size = "20" placeholder="Route" />
            <input type="hidden"   name="loc_area" id="loc_area" size = "20" placeholder="Local Area" />
            <input type="hidden"   name="extra"     id="extra"     size = "20" placeholder="Area" />
            </div>
            </div>
                   
            <div class="form-group">
             <h3 class="titleh3"></h3>
             <span id="latlngspan" class="coordinatetxt"></span>
            </div>
            <div class="form-group">
             <h3 class="titleh3"></h3>
             <span id="mlat" class="coordinatetxt"></span>
            </div>

    <!--End of Show_Address_On_Map_Code--> 
          </div> 
          <div class="modal-footer">
            <div style="clear:both">     
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onclick="GetTextValue()">Save changes</button>
                <!--<button type="submit" class="btn btn-primary" onclick="initialize()">Save changes</button>-->
            </div>
          </div>
        </form>
        </div>    
        </div>
      </div>
  
    <!--End of Map Modal Code-->
    
   <!-- Modal -->
    <div class="modal fade" id="addInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <form class="form-inline" role="form" id="info_form" action="/deals/createInfo" method="POST">
          <div class="modal-body">
              
          <input id="info_deal_id" name="info_deal_id" value="<?php echo $id   ;?>" placeholder="Name" type="hidden" />
          <div class="form-group">
            <input id="info_title" name="info_title" value="" placeholder="Title" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="info_sequence" name="info_sequence" value="" placeholder="Sequence" type="text" class="form-control"/>
          </div>
          
          <div class="form-group">
            <select id="info_status" name="info_status"  class="form-control" >
                <option value="">Status</option>
                <?php //print_r($categoriesList);
                        foreach ($infoStatus as $key => $status) { ?>
                            <option value="<?php echo $key   ;?>"><?php echo $status   ;?></option>
                <?php   }    ?>
            </select>
          </div>
          <br/>

          <div class="form-group">
          <?php 
          
              $this->widget('ext.editMe.widgets.ExtEditMe', array(
                        'name'=>'info_context_txt',
                        'bodyId'=>'info_context_txt',
//                        "defaultValue"=>'<p>HEllo</p>',
                        'filebrowserImageBrowseUrl' => '../../kcfinder/browse.php?type=files',
                        'filebrowserImageUploadUrl'=>'../../kcfinder/upload.php?type=files',
                        'htmlOptions'=>array('option'=>'value','width'=>'900px'),
                        'width'=>'560',
                        'height'=>'180',
                ));
          
          ?>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        </div>
      </div>
    </div>
    
    
    <!-- Modal -->
    <div class="modal fade" id="editOfferModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
<!--          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>           -->
        </div>
      </div>
    </div>
    
    <!--Start of Edit Offer Info Modal Code-->
    <div class="modal fade" id="editOfferInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
<!--      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>           -->
        </div>
      </div>
    </div>
    <!--End of Edit Offer Info Modal Code-->
    
<!--Commented Location Modal-->    
<!--    
     Modal 
    <div class="modal fade" id="editLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>           
        </div>
      </div>
    </div>-->
<!--Commented Location Modal-->    

<!--    Start Map Modal 
    <div class="modal fade" id="editMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Map Modal title</h4>
          </div>
          <div class="modal-body">
              <input id="map_deal_id" name="map_deal_id" value="<?php echo $id   ;?>" type="hidden" />  
              <?php 
//              $map_lat = $map["lat"];
//              $map_lng = $map["lng"];
//              
//             echo "Lat - ".$map_lat ." & ". "Long - ".$map_lng;
             
            ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>        
        </div>
      </div>
    </div>
    End Map Modal-->
 
<!-- Add Deals-->
      
    <!-- Modal -->
    <div class="modal fade" id="editInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
<!--          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>           -->
        </div>
      </div>
    </div>
       
  </div>

  
</div>

<script>
    $(function(){
        
        $(".expandDiv").on("click",function(event){
            //alert("HEllo");
            //alert(this.id)
            //$( ".expandDiv" ).removeClass( "active" ); 
            $( this ).toggleClass( "active" ); 
            $("#"+this.id+"Div").toggle("slow");
        });
        /*$("#offer_form").on("submit",function(event){
            //alert("offer_form");
            event.preventDefault();
        });*/
        
        //setTimeout( function(){
            
            //$('#addOfferModal').modal("show");
            var hash  = document.location.hash;
            if(hash == "#offers"){
                $("#offers").toggleClass( "active" ); 
                $("#offersDiv").toggle("slow");
            }else if(hash == "#deals"){
                $( "#deals" ).toggleClass( "active" ); 
                $("#dealsDiv").toggle("slow");
            }else if(hash == "#locations"){
                $( "#locations" ).toggleClass( "active" ); 
                $("#locationsDiv").toggle("slow");
            }
            
            jQuery.noConflict();
            $('#addOfferModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
            $('#editOfferModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
            $('#addOfferInfoModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
            $('#editOfferInfoModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
/*  Commented #addLocationModal ,#editLocationModal #editMapModal
          $('#addLocationModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
            $('#editLocationModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });          
            $('#editMapModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
*/            
            $('#addInfoModal')
            .on('hidden.bs.modal', function () {//.bs.modal
                $(this).removeData('bs.modal');
            });
            $('#editInfoModal')
            .on('hidden.bs.modal', function (e) {//.bs.modal
                $(this).removeData('bs.modal');
            });
            
            function init(){
                $(".location_city_search").on("change",function(){
                    var state_id = this.value;
                    if(state_id == ""){
                        $(".location_city_fill").html("<option value=\"\">City</option>");
                        return;
                    }

                    $(".location_city_fill").html("<option value=\"\">Loading...</option>");
                    $.ajax({
                        url:"<?php echo  Yii::app()->params['GLOBALS']['_URLS']["API"]["GET_Cities_By_StateId"] ;?>",
                        method:"GET",
                        dataType:"JSON",
                        data:"state_id="+state_id
                    }).done(function(data){
                        var htmlStr = "";
                        $.each(data,function(i,v){
                            htmlStr = htmlStr + "<option value=\""+v.id+"\">"+v.name+"</option>";
                        });
                        $(".location_city_fill").html(htmlStr);
                    });
                });
            }
            init();
            loadScript();
            
    });
    function setDealerName(){
       var name = $('#dealer_id').find(":selected").text();
       $('#dealer_name').val(name);
    }
    function AddImage(ev){
        unsetAddImage();
        var fsize = $('#image_file')[0].files[0].size; //get file size
        var imageInfo = $('#image_desc').val();
       if(fsize<=0 || fsize>51200){
            $('#imageSizeError').show();
           ev.preventDefault();
        }
        if(imageInfo==''){
           $('#imageInfoError').show();
           ev.preventDefault();
        }
    }
    function unsetAddImage(){
       $('#imageSizeError').hide();
       $('#imageInfoError').hide();
    }

    
    function AddLogoImage(ev){
        unsetAddLogoImage();
        var fsize = $('#logo_image_file')[0].files[0].size; //get file size
        var logoimageInfo = $('#logo_image_desc').val();
        if(fsize<=0 || fsize>51200){
            $('#logoimageSizeError').show();
           ev.preventDefault();
        }
        if(logoimageInfo==''){
           $('#logoimageInfoError').show();
           ev.preventDefault();
           //return false;
        }
       //return true;
    }
    function unsetAddLogoImage(){
       $('#logoimageSizeError').hide();
       $('#logoimageInfoError').hide();
    }
    
    function AddTestImage(ev){
        unsetAddTestImage();
        var fsize = $('#upload_img')[0].files[0].size; //get file size
        var testimageInfo = $('#test_image_desc').val();
//        if(fsize<=0 || fsize>51200){
//            $('#testimageSizeError').show();
//           ev.preventDefault();
//        }
//        if(testimageInfo==''){
//           $('#testimageInfoError').show();
//           ev.preventDefault();
//        }
    }

    function unsetAddTestImage(){
       $('#testimageSizeError').hide();
       $('#testimageInfoError').hide();
    }
    
// Start Get_Current_location Code
        var x = document.getElementById("demo");
        var vmap;
        var geocoder;
        var marker;
        var infowindow;
        var full_address;
        var lat ;
        var lng;
    
    function getLocation(){
        if (navigator.geolocation) {
          navigator.geolocation.watchPosition(showPosition);
        }else{ 
          x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position){
       document.getElementById('lat').value= position.coords.latitude;
       document.getElementById('lng').value= position.coords.longitude;
       setTimeout(function () {initialize();}, 50);
    }

    function showError(error){
        switch(error.code){
            case error.PERMISSION_DENIED:
               x.innerHTML = "User denied the request for Geolocation.";
               break;
            case error.POSITION_UNAVAILABLE:
               x.innerHTML = "Location information is unavailable.";
               break;
            case error.TIMEOUT:
               x.innerHTML = "The request to get user location timed out.";
               break;
            case error.UNKNOWN_ERROR:
               x.innerHTML = "An unknown error occurred.";
               break;
        }
    }
    // End Get_Current_location Code
    
   // Start Edit Map Code
      function editMap(map_lat,map_lng,map_ID, i){
       document.getElementById('demo_add').innerHTML="";
       document.getElementById('map_deal_location_id').value= map_ID; 
       document.getElementById('lat').value= map_lat;
       document.getElementById('lng').value= map_lng;
       
//      document.getElementById('near_loc').value= map_add;  //To Show previous near_loc value 
//      document.getElementById('gadres').value= map_full_add; //To Show previous full_location value
       document.getElementById('shop').value= document.getElementById('map_shop'+i).value;  //To Show previous near_loc value 
       document.getElementById('near_loc').value= document.getElementById('map_add'+i).value;  //To Show previous near_loc value 
       document.getElementById('gadres').value= document.getElementById('map_full_add'+i).value; //To Show previous full_location value
//       document.getElementById('dealer_contact').value= document.getElementById('h_dealer_contact'+i).value;
       var cont = document.getElementById('h_dealer_contact'+i).value;
       var contacts_arr = [];
       contacts_arr  =  cont.split("<br>");
       document.getElementById('dealer_contact').value = contacts_arr[0];
       var table="";
       for(var j=1;j<contacts_arr.length;j++){
           table +="<input tyep='text' class='input' name='contacts[]' id='tb"+j+"' value='"+contacts_arr[j]+"'>"
       }
       document.getElementById('demo_add').innerHTML=table;
//       document.getElementById('dealer_contact').value = contacts_arr[0]; //will Show first Element in dealer_contact textbox
//       editDealerContact(contacts_arr);                   //function will show rest of the contacts in dynamic textbox 
//       editDealerContact( );  
       setTimeout(function () {initialize();}, 300);   //To Refresh the Map
        
        Address();                                      //Calling Address Function to Show Address by Lat_long
    } 
    // End Edit Map Code
     
    //Start of Show_Location_On_Map Code
    function initialize(){
            var lat =  document.getElementById("lat").value;
            var lng =  document.getElementById("lng").value;
            var latlng = new google.maps.LatLng(lat,lng);	
          
            var myOptions = {
             zoom: 15,
             center: latlng,
             panControl: true,
             scaleControl: true,
             overviewMapControl: true,
             overviewMapControlOptions: { opened: false },
             mapTypeId: google.maps.MapTypeId.HYBRID
            };
        
            map = new google.maps.Map(document.getElementById("latlongmap"),myOptions);
            geocoder = new google.maps.Geocoder();
            marker = new google.maps.Marker({
             position: latlng,
             map: map
            });

            map.streetViewControl = false;
            infowindow = new google.maps.InfoWindow({
             content: "(lat, lng)"
            });
            Address(); 
        //Add Map Event Listener Functoin
            google.maps.event.addListener(map, 'click', function(event){
              marker.setPosition(event.latLng);
              var yeri = event.latLng;
           // var latlongi = "(" + yeri.lat().toFixed(6) + ", " +yeri.lng().toFixed(6) + ")";
           // infowindow.setContent(latlongi);
              document.getElementById('lat').value = yeri.lat().toFixed(6);
              document.getElementById('lng').value = yeri.lng().toFixed(6);
           // document.getElementById('latlngspan').innerHTML =  latlongi;
              
            // document.getElementById('near_loc').value = " ";  //To Blank near_loc textbox 
            // document.getElementById('gadres').value = " ";    //To Blank full_address textbox 
            Address();  //Calling Address function to popup Address on Every Map Event Click
            });
            
        }
    //End of Show_Location_On_Map Code
    
    
    //Start of Show_Address_On_Map Code
	function codeAddress(){
    
        //Checking Shop textbox value is Blank/Not otherwise adding comma (,) to it
          shop = " ";
        if(document.getElementById('shop').value != "" || document.getElementById('shop').value != 'undefined' 
                || document.getElementById('shop').value.length != 0 ){
           shop = document.getElementById('shop').value + " ";
        } 
        //Checking Near Location value is Blank/Not otherwise adding comma (,) to it
          near_loc = " ";
        if(document.getElementById('near_loc').value != "" || document.getElementById('near_loc').value != 'undefined' 
                || document.getElementById('near_loc').value.length != 0 ){
           near_loc = document.getElementById('near_loc').value + " ";
        }    
        //Checking Route value is Blank/Not otherwise adding comma (,) to it    
           route = " ";
        if(document.getElementById('route').value !== " " || document.getElementById('route').value !== undefined){
              route = document.getElementById('route').value + " " ;
        } 
        
//        full_address = near_loc + route +
        full_address = shop + near_loc +        
                           document.getElementById('loc_area').value + " , " +
//                           document.getElementById('extra').value + " , " +
                           document.getElementById('city').value ;
//                           document.getElementById('state').value + " , " + 
//                           document.getElementById('country').value + " , " + 
//                           document.getElementById('pin_code').value;
                   
          document.getElementById('gadres').value = full_address ;
            var address = document.getElementById("gadres").value;
            if (address == ''){
                alert("Address can not be empty!");
	        return;
            }
            infowindow.setContent(near_loc);
            if (infowindow){
                     infowindow.close();
            }
            infowindow.open(map, marker);
            
        /*    geocoder.geocode({'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK){
                 var lat1 = results[0].geometry.location.lat().toFixed(6);
                 var lng1 = results[0].geometry.location.lng().toFixed(6);
                 map.setCenter(results[0].geometry.location);
                 document.getElementById('lat').value = lat1;
                 document.getElementById('lng').value = lng1;
                 var latlong = "("+lat1+","+lng1+")";
                 marker.setPosition(results[0].geometry.location);
                 map.setZoom(15);
                 infowindow.setContent(near_loc);
                 if (infowindow) {
                     infowindow.close();
                 }
                 google.maps.event.addListener(marker, 'click', function() {
                     infowindow.open(map, marker);
                 });

                 infowindow.open(map, marker);
                     
                }
                Address();
            });
        */
           
        }
            
    //End of Show_Address_On_Map Code
     
    //Start of City_Area_Country_Code
        function httpGet(theUrl){
            var xmlHttp = null;
            xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", theUrl, false );
            xmlHttp.send( null );
            return xmlHttp.responseText;
        }

        function Address(){
            var lat_lng = document.getElementById('lat').value+','+document.getElementById('lng').value;
            var data =JSON.parse(httpGet("https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat_lng+"&sensor=true"));
            
            /* Initializing the variables */
            var route    = '';
            var loc_area = '';
            var city     = '';
            var state    = '';
            var country  = '';
            var extra      = '';
            var street_number = '';
            var pincode = '';
            
            for (var i=0; i<data.results[0].address_components.length; i++){ 
                txt = data.results[0].address_components[i];
                if(txt.types[0] == "sublocality_level_1"){
                    loc_area = txt;
                }
                else if(txt.types[0] == "sublocality"){
                    loc_area = txt;
                }else if(txt.types[0] == "locality"){
                    city  =txt;
                }else if(txt.types[0] == "administrative_area_level_1"){
                    state = txt;
           	 	}
           	 	else if(txt.types[0] == "administrative_area_level_2"){
                    extra = txt;
                }
                else if(txt.types[0] == "country"){
                    country  = txt;
                }else if(txt.types[0] == "street_number"){
                   street_number  = txt;
                }else if(txt.types[0] == "route"){
                    route  = txt;
                }else if(txt.types[0] == "postal_code"){
                    pincode  = txt;
                }
            }
            
//            console.log(loc_area.long_name+' - '+city.long_name+' - '+state.long_name+' - '+extra.long_name+' - '
//                       +country.long_name+' - '+street_number.long_name+' - '+route.long_name+' - '+pincode.long_name);
            
            
            if(typeof loc_area.long_name == 'undefined'){
            	loc_area = city;
            	if(extra.long_name)
                city = extra;
            }
            
            document.getElementById('route').value= " ";
            if (typeof route.long_name !== 'undefined' && route.long_name !== 'Unnamed Road'){
                document.getElementById('route').value= route.long_name;
            }
            document.getElementById('loc_area').value= " ";
            if (typeof loc_area.long_name !== 'undefined'){
                document.getElementById('loc_area').value=  loc_area.long_name; 
            }
            document.getElementById('extra').value= " ";                
            if (typeof extra.long_name !== 'undefined' && extra.long_name !== ''){
                document.getElementById('extra').value=  extra.long_name;
            }
            document.getElementById('city').value= " ";                
            if (typeof city.long_name !== 'undefined' && city.long_name !== ''){
                document.getElementById('city').value=  city.long_name;
            }
            
            document.getElementById('state').value= state.long_name;
            document.getElementById('country').value=country.long_name;
            document.getElementById('pin_code').value= pincode.long_name;
        }
    //End of City_Area_Country_Code 
    
    //Start Loading_Google_Map_Code
        function loadScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?sensor=false&' +
                   'callback=initialize';
            document.body.appendChild(script);
        }
    /*End Loading_Google_Map_Code*/
    
    /*Start of Offer_Info Code Set default_day ,default_appointment ,default_gender and 
    * function for Selecting Dropdown as selectPeriod_1, selectPeriod_2 and
    * finally show the time in textbox using selectTime function
     * @returns {slected value in final textbox}     */
    
    function default_day(){
     document.getElementById('Week_days').value='6 Days in a week (Sunday Closed)';
    }
    function default_appointment(){
     document.getElementById('Appoinment_Option').value='Prior appointment is mandatory';
    }
    function default_gender(){
     document.getElementById('Gender').value='Exclusive for men and women';    
    }
    function selectTime(id){
        //alert($("#Start_time").val());
        $('#'+id).val( $("#Start_time").val()+" "+$("#Period_1").val()+" To "+$("#End_time").val()+" "+$("#Period_2").val());
    }
     
    /*End of Offer_Info Code*/
    
    /*Start of Image_Crop Code using updateCoords , checkCoords ,  preview , and
    * submitform function to submit the form in which image is uploaded
    * @returns {show the preview & Submit the uploaded image form}     */
    
    jQuery(function(){
       jQuery('#cropbox').Jcrop({
                   aspectRatio: 2,
//                   setSelect: [0,0,450,400],
//                   allowResize: true,
                   onSelect: updateCoords
           });

   });
      
    function updateCoords(c)
    {
            jQuery('#x').val(c.x);
            jQuery('#y').val(c.y);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
    };

    function checkCoords()
    {
            if (parseInt(jQuery('#w').val())>0) return true;
            alert('Please select a crop region then press submit.');
            return false;
    };
 
    function preview(input) {
        if (input.files && input.files[0]) {
            var freader = new FileReader();
            freader.onload = function (e) {
                $("#preview").show();
                $('#preview').attr('src', e.target.result);
            }
            freader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#upload_img").change(function(){
        preview(this);
        
    });
  
    function submitform()
    {
     document.getElementById("main_form").submit();
    }
    /*End of Image_Crop Code*/
    
    /*Start of Add_Dealer_Contact Code using for dynamic addition of Textbox on clicking '+' button and delete on clicking '-' button
    * function will POST all the dynamic textbox value to createMapLocation of DealsComponent
    * @returns {show the textbox & add its value to Dealer_contact}     */
    $(document).ready(function() {

        var iCnt = 0;

        // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
        var container = $('#demo_add');
       
        $('#btAdd').click(function() {
            if (iCnt <= 19) {

                iCnt = container.children('input').length + 1;

                // ADD TEXTBOX.
                $(container).append('<input type=text class="input" name ="contacts[]" id=tb' + iCnt + ' ' +
                            'value=""  placeholder = "Dealer Contact ' + iCnt + '"/>');

                if (iCnt == 1) {        

                    var divSubmit = $(document.createElement('div'));
//                    $(divSubmit).append('<input type=button class="bt" onclick="GetTextValue()"' + 
//                            'id=btSubmit value=Submit />');
                }

               // $('#demo_add').after(container, divSubmit);   // ADD BOTH THE DIV ELEMENTS TO THE "dealer_contact_div" CONTAINER.
            }
            else {      // AFTER REACHING THE SPECIFIED LIMIT, DISABLE THE "ADD" BUTTON. (20 IS THE LIMIT WE HAVE SET)
                
                $(container).append('<label>Reached the limit</label>'); 
                $('#btAdd').attr('class', 'bt-disable'); 
                $('#btAdd').attr('disabled', 'disabled');

            }
            //var demo_addHtml =  $('#demo_add').html();
           // $('#demo_add').append(container);
        });

        $('#btRemove').click(function() {   // REMOVE ELEMENTS ONE PER CLICK.
            if (iCnt != 0) { $('#tb' + iCnt).remove(); iCnt = iCnt - 1; }
        
            if (iCnt == 0) { $(container).empty(); 
                $(container).remove(); 
                $('#btAdd').removeAttr('disabled'); 
                $('#btAdd').attr('class', 'bt') 
            }
        });
    });

    // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
    var divValue, values = '';

    function GetTextValue() {
        $(divValue).empty(); 
        $(divValue).remove(); values = '';

        $('.input').each(function() {
            divValue = $(document.createElement('div')).css({
                padding:'5px', width:'200px'
            });
            values += this.value + '<br />'
        });
       
    }
    /*End of Add_Dealer_Contact  Code*/
    
    /*Start of edit_Dealer_Contact Code using to generate dynamic Textbox based on number of dealer_contact in database
    * function will edit all the dynamic textbox value and POST to updateDealLocationMapList of DealsComponent
    * @returns {show the textbox & allow to edit its value to Dealer_contact}     */
    
//    function editDealerContact(contacts_arr) {
  /*  function editDealerContact( ) {  
        cont =  document.getElementById('dealer_contact').value;
        var contacts_arr = [];
        contacts_arr  =  cont.split(",");
        $(document).ready(function() {
        
         var iCnt = 0;
        // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
       
        var container = $(document.createElement('div')).html('');
        for(var i = 1 ; i < contacts_arr.length ; i++){ 
             iCnt = iCnt + 1;
             
             // ADD TEXTBOX.
             $(container).append('<input type=text class="input" name ="contacts[]" id=editTb' + iCnt + ' ' +
                                'value="'+contacts_arr[i]+'"  placeholder = "Dealer Contact ' + iCnt + '"/>');
                if (iCnt == 1) {
                    var divSubmit = $(document.createElement('div'));
                }
                $('#dealer_contact_div').after(container, divSubmit);   // ADD BOTH THE DIV ELEMENTS TO THE "dealer_contact_div" CONTAINER.
        }
        
    });
    $('#btRemove').click(function() {   // REMOVE ELEMENTS ONE PER CLICK.
            if (iCnt != 0) { $('#editTb' + iCnt).remove(); iCnt = iCnt - 1; }
        
            if (iCnt == 0) { $(container).empty(); 
        
                $(container).remove(); 
                $('#btSubmit').remove(); 
                $('#btAdd').removeAttr('disabled'); 
                $('#btAdd').attr('class', 'bt') 
                
            }
    });
    
    }*/
    /*End of Edit_Dealer_Contact Code*/
    
    /*Start of selectCouponType Code using to hide & show coupon_prefix_div based on Selection of Counpon type
    * function will hide div on selectiing System type and Dealer type Coupon or show on selection of other Coupon type Prefix and Fix 
    * @returns {coupon prefix text on show or NULL value on hide}     */
    function selectCouponType(){
        if($('#coupon_type').val() == "0" ||$('#coupon_type').val() == "3") {
            $('#coupon_prefix_div').hide();
          } else {
            $('#coupon_prefix_div').show();
      }
    }
    /*End of selectCouponType Code*/
    
    $(document).ready(function(){
       selectCouponType(); 
    });
</script>