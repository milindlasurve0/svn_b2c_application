<style type="text/css">
body {
	margin: 0px;
	padding:0px;
}

img{
	width:100%;
}

p{
	margin:0;
}

ul{
	margin:0 0 20px 0;
	padding-left:20px;
}

li{
	font-size:16px;
	margin:0 0 2px 0;
	font-family:"Encode Sans Normal";
}
.head_content{
	float:left;
	margin-left:10px;
	color:#6D6F71;
}
.top{
	float:left;
	margin-top:20px;
	font-family:"Encode Sans Normal";
	color:#513D98;
}
.top1{
	text-align:center;
	margin-top:20px;
	font-family:"Encode Sans Normal";
	color:#513D98;
        position: absolute;
        top:113px;
        left:215px;
        background-color: beige;
}
.address{
	font-family:"Encode Sans Normal";
	font-size:18px;
	float:left;
	clear:left;
	color:#000000;
	padding-bottom:10px;
}
.content{
	font-family:"Encode Sans Normal";
	float:left;
	clear:left;
	margin-left:10px;
	margin-top:5px;
	color:#6D6F71;
}
h1{
	font-size:24px;
	font-family:"Encode Sans Normal";
	margin:0;
	padding:0px 0 5px 0;
	color:#513D98;
}
h2{
	font-size:18px;
	font-family:"Encode Sans Normal";
	margin:0;
	padding:0px 0 5px 0;
	color:#513D98;
}
hr{
	border:0;
	clear:both;
	width:100%;
	height:1px; 
	background: #6D6F71;
	margin-top:0px;
}
</style>
</head>
<body >
    <div style="overflow: auto;" id="mainContent" name="mainContent">
        <div id="dealPreview" name="dealPreview" style="display:block">
        <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Deal Preview</h4>
         </div>
    <?php 
    $offer='';
    $image='';
    $offerDesc='';
    $deal = '';
    $address='';
    $dataArray= array();
    $locationUrl = '';
    foreach($dealsList as $row){
        $deal_id = $row['id'];
        $image = $row['img_url'];
        $offer = $row['ofname'];
        $offerDesc = $row['offer_desc'];
        $deal = $row['dealname'];
        $address = $row['address'];
        $area    = $row['area'];
        $city    = $row['city'];
        $state   = $row['state'];
        $lat = $row['latitude'];
        $lng = $row['longitude'];
        $logo_url = $row['logo_url'];
        $brandDesc = $row['long_desc'];
        $locationUrl = "https://maps.googleapis.com/maps/api/staticmap?center=$lat,$lng&zoom=13&size=200x150&maptype=roadmap&markers=color:red%7Clabel:A%7C$lat,$lng&format=gif";
        break;
    }
    $content = "";
    foreach($context as $record){
        $content = $content.$record['content_txt'];
    }    
    ?>
        <div class="top" style="width:100%;text-align: center"><h1><b><?php echo $deal; ?></b></h1></div>
        <div style="clear:both">
            <div class="top1" style="z-index:2"><h1><?php echo $offer; ?></h1></div>
            <div  style="position:absolute;"><img src="<?php echo $image; ?>" /></div>
            <div style="position:absolute;padding-top: 156px;padding-left: 37px;"><img src="<?php echo $logo_url; ?>" style="height:100px;width:100px;z-index: 2;border-radius: 23px;"></div>
        </div>
       <br />
       <div style="clear:both;position: relative;padding-top: 247px">
            <div style="clear:both;text-align:left"><b><?php echo $offerDesc; ?></b></div>
            <br />
            <hr>
            <div class="head_content">
                <div><b>Offer Details</b></div><br />
                <div><?php echo $offerDetails['time'];?></div>
                <div><?php echo $offerDetails['day'];?></div>
                <div><?php echo $offerDetails['appointment'];?></div>
                <div><?php echo $offerDetails['exclusive_gender'];?></div>
                <br />
                <div id="serviceList" class="collapse">
                <div class="row featurette">        
                    <div style="padding-left:15px"><?php echo $content;?></div>
                </div>
                </div>
                <div class="press-title">
                <div class="text" data-toggle="collapse" data-target="#serviceList">
                <span style="padding-left: 480px;" id="servicesButton" data-toggle="tooltip " data-original-title="Click Me!">
                    <strong>More</strong><span class="servicedrop glyphicon glyphicon-chevron-down"></span>
                 <!--More<span class="servicedrop glyphicon glyphicon-chevron-down"></span>-->
                </span>
                </div>
                </div>
                <br />    
            </div>
      
        <hr>
        <div class="head_content" style="width:200px">
        
        <div><h1><font color="black">Address :</font></h1></div>
        <div class="address" style="padding-bottom: 0px;"><h2 style="padding-bottom: 0px;"><?php echo $address.',' ; ?></h2></div>
        <div class="area"><h2 style="padding-bottom: 0px;"><?php echo $area.','; ?></h2></div>
        <!--<div class="city"style="clear:both"><h2><?php // echo $city; ?></h2></div>-->
        <div class="city"><h2><?php echo $city; ?></h2></div>
        <!--<div class="state"><h2><?php // echo $state; ?></h2></div>-->    <!-- will Show the State name -->
        <!--<div class="Country"><h2><?php // echo "India"; ?></h2></div>--> <!-- will Show the Country -->
        </div>
        
        <!--<div style="float:right;padding-top:20px" ><a target="_blank" href='panel/getDealMap?id=<?php // echo $deal_id;?>'><image src="../../../images/marker.png" height=30px width=30px /></a></div>-->
        <div style="float:right;padding-top:20px;width:200px" ><a target="_blank" href='panel/getDealMap?id=<?php echo $deal_id;?>'><img src="<?php echo $locationUrl; ?>" style="width: 200px;height: 160px    ;"/></a></div>
        <hr>
        <div style="clear:both">
        <?php if(count($gallery)>0){ ?>
         <div style="float: left;"> 
            <?php 
              //        print_r($gallery);
                        for($i=0;$i<count($gallery);$i++){ ?>
                               <?php // echo $gallery[$i]['image_url'] ;?>
                                <img  src="<?php echo $gallery[$i]['image_url'] ;?>" 
                                     style="width: 105px; height: 105px; border-radius: 25px; padding-top: 4px;padding-bottom: 4px;padding-left: 2px;padding-right: 2px;">

                                   
                        
             <?php   }    ?>

       </div> <?php   }    ?>
        </hr>
        <div class="content" >
            <p><b>Brand Description</b></p>
            <div style="padding-top:5px"><?php echo $brandDesc;?></div>
        </div>
       </div>   
         </div> 
         </div>
        </div>
</body> 

<script>
$('#serviceList').on('shown.bs.collapse', function() {
    $(".servicedrop").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
  });

$('#serviceList').on('hidden.bs.collapse', function() {
    $(".servicedrop").addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
  });
</script>