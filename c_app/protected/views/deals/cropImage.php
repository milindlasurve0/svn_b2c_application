<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

    <!--Start of Test Image Modal Code-->
    <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Crop Image #1</h4>
    </div>
    
<!--<div class="modal fade" id="addImgTestModel"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--    <div class="modal fade" id=" "  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Crop Image</h4>
          </div>-->
    <form class="form-inline" role="form" id="location_form" action= "/deals/addcropImage" method="POST" onsubmit="return checkCoords()" enctype="multipart/form-data">
     <font color="red" ><strong><?php echo (count($deal_test_list)==0) ? "Too big image to upload.Please upload image having size less than 50kb." : " " ;  ?></strong></font>  
        <div style="padding-left: 134px">
                  <label id="testimageSizeError" style="display:none;color:red">Too big image to upload.Please upload image having size less than 50kb.</label>
                  <br/>
                  <label id="testimageInfoError" style="display:none;color:red">Image info should not be blank.</label>
        </div>
        <div class="modal-body">
            <input id="location_deal_id" name="crop_image_deal_id" value="<?php echo $id   ;?>" type="hidden" />
          
            <?php
              $test_url = "";
              $test_desc = "";
              $test_img_id = "";
              foreach ($deal_test_list as $key => $image_detail) { 
                  $test_url = isset($image_detail['image_url']) ? $image_detail['image_url'] : " ";
                  $test_desc = isset($image_detail['image_info']) ? $image_detail['image_info'] : " ";
                  $test_img_id = isset($image_detail['id']) ? $image_detail['id'] : " ";
            ?>
              
<!--              <tr><td><?php //  echo "<br>Crop Check- ".$crop_check; ;?></td>
                  <td><?php // echo $image_detail["id"] ;?></td><td><?php // echo $image_detail["image_url"] ;?></td>
                  <td><?php // echo $image_detail["image_info"] ;?></td><td><?php // echo $image_detail["uploaded_date"] ;?></td>
               </tr>        -->
            <?php } ?>
              
        <div class="form-group">
            <link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.min.css" type="text/css" />
            <script src="http://edge1u.tapmodo.com/global/js/jquery.min.js"></script>
            <script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
           
            <div id="pholder">
             <div id="pbox">
              <div id="pbody">
               <div class="jc_coords">
                
            <img id="cropbox" src="<?php echo $test_url  ;?>" />
            <input type="hidden" id="crop_image_url" name="orginal_image_url" value = "<?php echo $test_url ;?>"/>
            <input type="hidden" id="crop_image_info" name="orginal_image_desc" value = "<?php echo $test_desc ;?>"/>
            <input type="hidden" id="crop_image_id" name="crop_image_id" value = "<?php echo $test_img_id ;?>"/>
                <input type="hidden" id="x" name="x"  />
                <input type="hidden" id="y" name="y" />
                <input type="text"   id="w" name="w" />
                <input type="text"   id="h" name="h" />
                
              </div>
              </div>
             </div>
            </div>
            
            
          </div> 
        </div>    
        <div class="modal-footer">
            <a id="btn_crop" class="btn btn-default" href="/deals/editDetails/<?php echo $id ;?>" >Close</a>    
            <button type="submit" class="btn btn-primary">Save Crop</button>
        </div>
        </form>
      
      
    <!--End of Test Image Modal Code--> 

<script>
    jQuery(function(){
           jQuery('#cropbox').Jcrop({         //cropbox is the ID of image control
                   aspectRatio: 2,           //Set aspect Ratio 2 for Rectangular Shape        
                   setSelect: [0,0,900,450], //Set Cropbox Width and Height
                   allowResize: false,       //Disallow to Resize the cropbox
                   onSelect: updateCoords
           });
   });

    //will update the cordinates x , y, w and h 
    function updateCoords(c)
    {
            jQuery('#x').val(c.x);
            jQuery('#y').val(c.y);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
    };

    function checkCoords()
    {
            if (parseInt(jQuery('#w').val())>0) return true;
            alert('Please upload image & select a crop region then press Save Crop.');
            return false;
    };
 
    // preview function will display the selected image with id upload_img
    function preview(input) {
        if (input.files && input.files[0]) {
            var freader = new FileReader();
            freader.onload = function (e) {
                $("#preview").show();
                $('#preview').attr('src', e.target.result);
            }
            freader.readAsDataURL(input.files[0]);
        }
    }
    // will call preview function to display the selected img on change.
    $("#upload_img").change(function(){
        preview(this);
        
    });
    //will submit the form with id main_form.
    function submitform()
    {
     document.getElementById("main_form").submit();
    }
    
   //will click the button with id crop_button.
    $(document).ready(function(){
     if($('#x').val() == "NaN") {
            $('#pholder').hide();
      } else {
            $('#pholder').show();
      }
    });
</script>