<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script>
    function savedealtype(ElemObj,dealid){
        $.ajax({
                url:"<?php echo  Yii::app()->params['GLOBALS']['_URLS']["CONTROLLER"]["add_Deal_Type"] ;?>",
                method:"POST",
                dataType:"text",
                data:"id="+dealid+"&typeid="+ElemObj.value
            }).done(function(responce){
                $('#deal_type_save'+dealid).html(responce);
                $('#deal_type_save'+dealid).show(1000);
                $('#deal_type_save'+dealid).hide(1000);
            },dealid);
    }
    function showdeals(type){
        if(type=='open'){
            $('#open').addClass('active');
             $('#close').removeClass('active');
        }
        else{
            $('#close').addClass('active');
             $('#open').removeClass('active');
        }
    }
 </script>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="msg_flash flash-' . $key . '">' . $message . "</div>\n";
    }
   
    Yii::app()->clientScript->registerScript(
       'myHideEffect',
       '$(".msg_flash").animate({opacity: 1.0}, 3000).fadeOut("slow");',
       CClientScript::POS_READY
    );
    if(!empty($_REQUEST) && isset($_REQUEST['type'])){
        if(isset($_REQUEST['type']) && $_REQUEST['type']=='open'){
            $open = "class='active'";
            $close="class=''";
        }
        else{
             $open = "class=''";
             $close="class='active'";
        }
    }
    else{
        $open = "class='active'";
        $close="class=''";
    }

?>
<div id="content">

<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <?php
    
        $count_open = 0 ;
        $count_close = 0 ;
     
    for($a = 0 ;$a<count($dealsList);$a++){
      if($dealsList[$a]['status']==1){
       $count_open++;
    }
    else if($dealsList[$a]['status']==2){
       $count_close++;
    }
    } 
    ?>
    
    <li <?php echo $open ; ?> name="open" id="open" onclick="showdeals('open')"><a href="/index.php/deals/show_stockDeal?type=open">Open Deals <?php echo ($count_open != 0) ? "(".$count_open.")" : " " ; ?></a></li>
    <li <?php echo $close ; ?> name="close" id="close" onclick="showdeals('close')"><a href="/index.php/deals/show_stockDeal?type=stockout">Close Deals <?php echo ($count_close != 0) ? "(".$count_close.")" : " " ; ?></a></li>
</ul>
    </div>

<div class="panel panel-default">
  <!-- Default deal contents -->
  <div class="panel-heading"><h4>Deals List</h4></div>
  
  <div class="panel-body">
<!--    <p></p>-->
    <form class="form-inline" role="form" id="deal_form" action="/deals/create" method="POST"  enctype="multipart/form-data">
<!--    <form class="form-inline" role="form" id="deal_form" action="deals/create" enctype="multipart/form-data" method="POST">-->
      <div class="form-group" id="deal_name" >
        <input id="name" name="name" value="" placeholder="Name" type="text" class="form-control" style="width: 630px;"/>
      </div>
<!--      <div class="form-group">
        <textarea id="summary" name="summary" value="" placeholder="Summary" type="text" class="form-control" style="width: 490px; height: 35px;"></textarea>
      </div>-->
        <div class="form-group">        
        <select id="dealer_id" name="dealer_id"   class="form-control" >
            <option value="">Dealer</option>
             <?php //print_r($dealersList);            
               foreach ($dealersList as $key => $dealer) { ?>
                    <option value="<?php echo $dealer['id']   ;?>"><?php echo $dealer['user_name']   ;?></option>
               <?php } ?>
        </select>    
      </div>
        <div class="form-group">        
        <select id="category_id" name="category_id"   class="form-control" >
        <option value="">Category</option>
         <?php //print_r($categoriesList);
                foreach ($categoriesList as $key => $category) { ?>
                    <option value="<?php echo $category['id']   ;?>"><?php echo $category['name']   ;?></option>
        <?php   }    ?>
       
        </select>    
      </div>
      <div class="form-group">        
<!--        <input id="status" value="" placeholder="Status" type="text" class="form-control"/>-->
        <select id="status" name="status"  class="form-control" >
        <option value="">Status</option>
        <?php //print_r($categoriesList);
                foreach ($dealStatus as $key => $status) { ?>
                    <option value="<?php echo $key   ;?>"><?php echo $status   ;?></option>
        <?php   }    ?>
        </select>    
      </div>
      <div class="form-group">
        <input id="mainimage" name="main_image_url" value="" placeholder="upload image" type="file" />
      </div>
        <button type="submit" class="btn btn-default">Add Deal</button>
    </form>
    <!--    <form role="form">
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
      </div>
    </from>-->
  </div>
  <!-- Table -->
  <table class="table">
    <thead> <th>#ID</th><th>Name</th><th>Category</th><th>Dealer</th><th>Total / Used </th><th>Status</th><th width="140">Action</th></thead>
  <tbody>
<?php     $i=0; 
      foreach ($dealsList as $key => $deal) {
      	
      	
?>
      <tr >
          <td><?php echo $deal["id"] ;?></td>
          <td><?php echo $deal["name"] ;  if(!empty($deal['stock_flag'])){ echo " <span style='color:red'>".$deal['stock_flag']."</span>";} ?>
                <br /><?php echo "(".$deal["offer_name"].")" ;?></td>
          <td><?php echo $deal["category_name"] ;?></td>
          <td><?php echo $deal["user_profile_name"]."(".$deal["user_name"].")" ;?></td>
          <td><?php echo $deal["total_stock"]." / ".$deal["stock_sold"] ;?> <br /><?php echo $deal["off_validity"]; ?> </td>
          <td><?php echo isset($dealStatus[$deal["status"]]) ? $dealStatus[$deal["status"]] : "-" ;?></td>
          <td width="140">
              <a id="btn_edit_<?php echo $deal["id"] ;?>" href="/deals/editDetails/<?php echo $deal["id"] ;?>" class="btn btn-default btn-small edit_button">Edit</a>
              <a id="btn_delete_<?php echo $deal["id"] ;?>" href="/deals/delete/<?php echo $deal["id"] ;?>" class="btn btn-default btn-small delete_button">Delete</a>
              <a id="btn_info_preview_<?php echo $deal["id"] ;?>" data-target="#dealPreview<?php echo $deal["id"] ;?>" data-toggle="modal" href="/deals/preview/<?php echo $deal["id"] ;?>" class="btn btn-default btn-small info_preview_button">Preview</a>
              <a id="btn_info_pdf_<?php echo $deal["id"] ;?>" target="_blank"  href="/deals/downloadPdf/?id=<?php echo $deal["id"] ;?>" class="btn btn-default btn-small info_pdf_button">Pdf</a>
          </td>
          <div class="modal fade" id="dealPreview<?php echo $deal["id"] ;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" >             
                </div>
            </div>
        </div>
      </tr>
      <tr>
        <td colspan="7" style="border:0px;height: 22px;display: none;background-color: aliceblue;text-align: center" id="deal_type_save<?php echo $deal["id"] ;?>">
                    
        </td>
      </tr>
      <tr>
          <td colspan="7" style="border:0px">
              <div style="text-align: center;">
                    <?php foreach($dealsTypes as $row){?>
                  <input type="radio" value="<?php echo $row['id']; ?>" onclick="savedealtype(this,<?php echo $deal["id"] ;?>)" name="dealTypes<?php echo $i; ?>" id="<?php echo $row['name'].$i; ?>" <?php if($row['id']==$deal['type_id']){ echo "checked"; }?>><span style="margin-right: 10px"><?php echo $row['name']; ?></span>
                    <?php } ?>
              </div>
          </td>
      </tr>
      
  <?php $i++; } ?>
  </tbody>
  </table>
</div>
 
