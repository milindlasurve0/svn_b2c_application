<script>
    function get_offers(ElemObj){
             //   $("#deals").on("change",function(){ 
                    var deals_id = ElemObj.value;
                    if(deals_id == ""){
                        $("#offers").html("<option value=\"\">Offers</option>");
                        return;
                    }

                    $("#offers").html("<option value=\"\">Loading...</option>");
                    $.ajax({
                        url:"<?php echo  Yii::app()->params['GLOBALS']['_URLS']["API"]["Get_deal_offers"] ;?>",
                        method:"GET",
                        dataType:"JSON",
                        data:"deals_id="+deals_id
                    }).done(function(data){
                        var baseUrl = '<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL'];?>';
                       var htmlStr = "";
                        if(data.length>0){ 
                            //baseUrl= 'http://b2c.pay1.in/';
                            baseUrl= baseUrl+data[0].img_url;
                        $('#deal_image').html('<img src="'+baseUrl+'" height="300px" width="300px">');
                        $.each(data,function(i,v){
                            htmlStr = htmlStr + "<option value=\""+v.id+"\">"+v.name+"</option>";
                        });
                    }
                    else{
                        $('#deal_image').html('');
                    }
                    
                        $(".offers_fill").html(htmlStr);
                    });
                //});
            }
        
      function check_criteria(ElemObj){
        if(ElemObj.value=='specific_user'){
             $('#specific_user_details').show();
             $('#km_wise_details').hide();
        }
        else if(ElemObj.value=='Km'){
            $('#km_wise_details').show();
            $('#specific_user_details').hide();
        }
        else{
            $('#km_wise_details').hide();
            $('#specific_user_details').hide();
        }
      }
      function setPromotionView(ElemObj){
          if(ElemObj.value==0){
              //non-promotional notification
              $('#DescriptionView').show();
              $('#deal_offer').hide();
              $('#deal_image').hide();
              $('#km_wise_details').hide();
              $("#criteria option[value='Km']").remove();
          }
          else if(ElemObj.value==1){
              //promotional notification
              $('#DescriptionView').hide();
              $('#image_desc').val('');
              $('#deal_offer').show();
              $('#deal_image').show();
              $("#criteria").append($("<option></option>").attr("value", "Km").text("Km wise")); ;
          }
      }
 </script>
<?php
if(isset($message) && !empty($message)){
    
    echo $message;
}
?>
<form enctype="multipart/form-data" method="POST" action="" id="location_form" >
    <!-- hidden element-->
    <fieldset>
        <legend>Announce Free-Gift</legend>
    </fieldset>
    <div style="clear:both;">
        <div style="float:left;"><span><input type="radio" name="viewNotify" value="1" checked onclick="setPromotionView(this)"> </span>Promotional Notification</div>
        <div style="float:left;"><span style="padding-left:20px"><input type="radio" name="viewNotify" value="0" onclick="setPromotionView(this)" > </span>Non-Promotional Notification</div>
    </div>
    <div id="deal_offer" stye="display:block">
    <div style="clear: both;width:500px;padding-top: 15px;">
        <div style="float:left;width:190px">
<!--            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="deals" name="deals"  class="form-control" onchange="get_offers(this)" >
                <option value="">Select Deals</option>
                <?php //print_r($categoriesList);
                        foreach ($deals as $key => $value) { ?>
                            <option value="<?php echo $value['id']   ;?>"><?php echo $value['name']   ;?></option>
                <?php   }    ?>
            </select>
          </div>
         <div style="float:right;width:190px">
<!--            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="offers" name="offers"  class="form-control offers_fill" >
                <option value="">Select Offers</option>                
            </select>
          </div><br/>
          
     </div>
    </div>
    <div style="clear: both;width:500px;padding-top:5px">
        <div style="float:left;width:190px">
<!--            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="criteria" name="criteria"  class="form-control criteria_specific" onchange="check_criteria(this)" >
                <option value="">Select Criteria</option>
                <option value="All">All Users</option>
                <option value="specific_user">Specific Users</option>
                <option value="Km">Km wise</option>
            </select>
          </div>
        <div style="float:right;width:190px">
            <span id="specific_user_details" style="display:none">
                <input id="mobile" name="mobile" value="" placeholder="Users Mobile Number" type="text" class="form-control"/>
            </span>
            <span id="km_wise_details" style="display:none">
                <select id="km_wise" name="km_wise"  class="form-control" >
                   <?php for($i=10;$i<=100;$i=$i+10){ ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                   <?php } ?>
                </select>
            </span>
          </div><br/>
    </div>
    <div style="clear: both;width:500px;padding-top:5px">
        <div style="float:left;width:190px">
            <label> Notification </label>&nbsp;<input type="checkbox" checked="checked" style="vertical-align:middle" name="Notification" id="Notification" value="Notification" />
        </div>
        <div style="float:left;width:190px">
            <label> SMS </label>&nbsp;<input type="checkbox" style="vertical-align:middle" name="SMS" id="SMS" value="SMS" />
        </div>
    </div>
    <div style="clear: both;padding-top: 15px">
        <div  id="deal_image" name="deal_image" style="float: left">
        </div>             
          <div style="width: 50%;float: left;display:none" id="DescriptionView">
            <label for="image_desc">Description</label>
                <textarea class="form-control" name="image_desc" id="image_desc"></textarea>
                
          </div>
    </div>
    <div style="clear: both">
        <button style="float: left;margin-top: 9px;" class="btn btn-primary" type="submit" id="Announce_submit" value="Announce" >Send</button>
    </div>
</form>

