<?php
/* @var $this UsersController */
/* @var $model Users */

$auth_user = array(
	'Users'=>array('index'),
	'Create',
);

$general = array(
	'Create',
);


$this->breadcrumbs= (Yii::app()->user->isGuest) ? $general : $auth_user;

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('site/login'), 'visible'=>!Yii::app()->user->isGuest),
	array('label'=>'Manage Users', 'url'=>array('admin'),  'visible'=>!Yii::app()->user->isGuest),
);

$formhead = (!Yii::app()->user->isGuest)?"Create Users":"Registration Form";
?>

<h1><?php echo $formhead; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>