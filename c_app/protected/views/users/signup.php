<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

?>
<center><h2>Registration Form</h2></center>
<?php
    //print_r($data);    
?>
<div style="color:red;text-align: center">
    <?php echo $error = isset($data['error'])?"<h3>".$data['error']."</h3>":""?>
</div>
<form method="POST">
    <div class="control-group">
        <label class="control-label"><b>Customer Name : </b></label>
        <input class="form-control" type="text" name="name" placeholder="Enter name">
    </div><br/>
    <div class="control-group">
        <label class="control-label"><b>Mobile Number : </b></label>
        <input class="form-control" type="text" name="mobile_number" placeholder="Enter mobile name">
    </div><br/>
    <div class="control-group">
        <label class="control-label"><b>gender : </b></label>
        <select class="form-control" name="gender">
            <option value="m">Male</option>
            <option value="f">Female</option>
        </select>
    </div><br/>
    <div class="control-group">
        <label class="control-label"><b>Date of birth : </b></label>
        <input class="form-control" type="text" name="date_of_birth" placeholder="date of birth in Y-M-D format">
        <input type="hidden" name="action" value="next">
    </div><br/>
    <div class="control" style="text-align: center">
        <button name="next" class="btn btn-success btn-lg" >Continue</button>
    </div>
</form>