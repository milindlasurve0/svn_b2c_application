<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

?>
<?php
   // print_r($data);
?>
<div style="color:red;text-align: center">
    <?php echo $error = isset($data['error'])?"<h3>".$data['error']."</h3>":""?>
    <?php echo $link = (isset($data['status']) && $data['status']=='success')?CHtml::link('signup', $this->createAbsoluteUrl('site/index')):""; ?>
</div>
<form method="POST">
    <?php $mobile = Yii::app()->user->getState('mobile_number');
    if(is_null($mobile)){?>
    <div class="control-group">
        <label class="control-label"><b>Mobile Number : </b></label>
        <input class="form-control" type="text" name="mobile_number" placeholder="Enter mobile name">
    </div><br/>
    <?php }else{ ?>
        <input type="hidden" name="mobile_number" value="<?php echo $mobile;?>">
    <?php } ?>
    <div class="control-group">
        <label class="control-label"><b>OTP : </b></label>
        <input class="form-control" type="text" name="code" placeholder="Enter OTP received">
    </div><br/>
    <div class="control-group">
        <label class="control-label"><b>Password : </b></label>
        <input class="form-control" type="text" name="password" placeholder="Enter Password">
    </div><br/>
    <div class="control-group">
        <label class="control-label"><b>Confirm Password : </b></label>
        <input class="form-control" type="text" name="confirm_password" placeholder="Confirm Password">
        <input type="hidden" name="action" value="finish">
    </div><br/>
    <div class="control" style="text-align: center">
        <button name="next" class="btn btn-success btn-lg" >Continue</button>
    </div>
</form>