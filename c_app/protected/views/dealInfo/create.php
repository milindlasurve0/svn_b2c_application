<?php
/* @var $this DealInfoController */
/* @var $model DealInfo */

$this->breadcrumbs=array(
	'Deal Infos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DealInfo', 'url'=>array('index')),
	array('label'=>'Manage DealInfo', 'url'=>array('admin')),
);
?>

<h1>Create DealInfo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>