<?php
/* @var $this DealInfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Deal Infos',
);

$this->menu=array(
	array('label'=>'Create DealInfo', 'url'=>array('create')),
	array('label'=>'Manage DealInfo', 'url'=>array('admin')),
);
?>

<h1>Deal Infos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
