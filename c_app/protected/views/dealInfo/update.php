<?php
/* @var $this DealInfoController */
/* @var $model DealInfo */

$this->breadcrumbs=array(
	'Deal Infos'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DealInfo', 'url'=>array('index')),
	array('label'=>'Create DealInfo', 'url'=>array('create')),
	array('label'=>'View DealInfo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DealInfo', 'url'=>array('admin')),
);
?>

<h1>Update DealInfo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>