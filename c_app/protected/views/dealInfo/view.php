<?php
/* @var $this DealInfoController */
/* @var $model DealInfo */

$this->breadcrumbs=array(
	'Deal Infos'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List DealInfo', 'url'=>array('index')),
	array('label'=>'Create DealInfo', 'url'=>array('create')),
	array('label'=>'Update DealInfo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DealInfo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DealInfo', 'url'=>array('admin')),
);
?>

<h1>View DealInfo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'deal_id',
		'title',
		'content_txt',
		'sequence',
		'status',
	),
)); ?>
