<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"> Edit Offer Info #1</h4>
          </div>
          <form class="form-inline" role="form" id="offer_info_form" action="/index.php/deals/editOfferInfo" method="POST">
          <div class="modal-body">
            
              <input id="offer_info_deal_id" name="offer_info_deal_id" value="<?php echo $model->deal_id   ;?>"  type="hidden" />
              <input id="offer_info_id" name="offer_info_id" value="<?php echo $model->id   ;?>"  type="hidden" />
          <div class="form-group">
            <input id="offer_info_time" name="offer_info_time" value="<?php echo $model->time   ;?>" 
                   placeholder="Time" type="text" class="form-control" style="width: 250px;"/>
          </div>
            <input id="time" name="time" value="Time" onclick="default_time()" type="button" class="btn btn-primary"/><br>
          <div class="form-group">
            <input id="offer_info_day" name="offer_info_day" value="<?php echo $model->day   ;?>" 
                   placeholder="Day" type="text" class="form-control" style="width: 250px;"/>
          </div>
           <input id="days" name="days" onclick="default_day()" value="Days" type="button" class="btn btn-primary"/><br>   
          <div class="form-group">
            <input id="offer_info_appointment" name="offer_info_appointment" value="<?php echo $model->appointment ;?>" 
                   placeholder="Appointment" type="text" class="form-control" style="width: 250px;"/>
          </div>
           <input id="Appoinment" name="Appoinment" onclick="default_appointment()"  value="Appointment" type="button" class="btn btn-primary"/><br> 
          <div class="form-group">
            <input id="offer_info_gender" name="offer_info_gender" value="<?php echo $model->exclusive_gender ;?>" 
                   placeholder="Gender" type="text" class="form-control" style="width: 250px;"/>
          </div>
            <input id="gender" name="gender"  onclick="default_gender()"  value="Gender" type="button" class="btn btn-primary"/><br>
           <div class="form-group">
            <input id="offer_info_contact" name="offer_info_contact" value="<?php echo $model->dealer_contact   ;?>" 
                   placeholder="Dealer Contact" type="text" class="form-control" style="width: 250px;"/>
          </div>          
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
          </form>

<script>
    
    function default_time(){
     document.getElementById('offer_info_time').value='10 AM To 10 PM';
    }
    
    function default_day(){
     document.getElementById('offer_info_day').value='6 Days in a week (Sunday Closed)';
    }
    
    function default_appointment(){
     document.getElementById('offer_info_appointment').value='Prior appointment is mandatory';
    }
    
    function default_gender(){
     document.getElementById('offer_info_gender').value='Exclusive for men and women';    
    }
    
</script>    