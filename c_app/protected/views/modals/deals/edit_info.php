
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <form class="form-inline" role="form" id="info_form" action="/deals/editInfo" method="POST">
          <div class="modal-body">
              
          <input id="edit_info_deal_id" name="edit_info_deal_id" value="<?php echo $model->deal_id   ;?>" placeholder="Name" type="hidden" />
          <input id="edit_info_id" name="edit_info_id" value="<?php echo $model->id   ;?>" placeholder="Name" type="hidden" />
          <div class="form-group">
            <input id="edit_info_title" name="edit_info_title" value="<?php echo $model->title   ;?>" placeholder="title" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="edit_info_sequence" name="edit_info_sequence" value="<?php echo $model->sequence   ;?>" placeholder="Sequence" type="text" class="form-control"/>
          </div>
          
          <div class="form-group">
<!--            <input id="info_status" name="info_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="edit_info_status" name="edit_info_status"  class="form-control" >
                <option value="">Status</option>
                <?php //print_r($categoriesList);
                        foreach ($infoStatus as $key => $status) { ?>
                            <option value="<?php echo $key   ;?>"  <?php echo $model->status == $key ? "  selected "  : "" ;?> >  <?php echo $status   ;?></option>
                <?php   }    ?>
            </select>
          </div>
          <br/>
<!--          <div class="form-group">
            <textarea id="info_context_txt" name="info_context_txt" value="" placeholder="HTML" type="text" class="form-control"></textarea>
          </div>-->
          
          <div class="form-group">
          <?php 
          
              $this->widget('ext.editMe.widgets.ExtEditMe', array(
                        'name'=>'edit_info_context_txt',
                        'bodyId'=>'edit_info_context_txt',
                        "value"=>$model->content_txt,

                        'filebrowserImageBrowseUrl' => '/kcfinder/browse.php?type=files',
                        'filebrowserImageUploadUrl'=>'/kcfinder/upload.php?type=files',
                        'htmlOptions'=>array(
                            'option'=>'value',
                            'width'=>'900px'
                        ),
                        'width'=>'560',
                        'height'=>'180',
              ));
          
          ?>
          </div>
          

          
            
<!--          <button id="add_info" type="submit" class="btn btn-default">Add info</button>-->
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
        
