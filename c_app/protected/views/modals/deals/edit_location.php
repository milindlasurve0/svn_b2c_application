<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<!--
<div class="modal fade" id="addLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">-->
<script>
function test(){
           // alert("HEllo");
                    var state_id = $("#edit_location_state").val();
                    if(state_id == ""){
                        $(".location_city_fill").html("<option value=\"\">City</option>");
                        return;
                    }

                    $(".location_city_fill").html("<option value=\"\">Loading...</option>");
                    $.ajax({
                        url:"<?php echo  Yii::app()->params['GLOBALS']['_URLS']["API"]["GET_Cities_By_StateId"] ;?>",
                        method:"GET",
                        dataType:"JSON",
                        data:"state_id="+state_id
                    }).done(function(data){
                       // alert(data.toSource());

                        var htmlStr = "";
                        $.each(data,function(i,v){
                            htmlStr = htmlStr + "<option value=\""+v.id+"\">"+v.name+"</option>";
                        });
                        $(".location_city_fill").html(htmlStr);
                    });
     }
       
</script>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add Location</h4>
          </div>
          <form class="form-inline" role="form" id="location_form" action="/deals/editLocation" method="POST">
          <div class="modal-body">
              
              <input id="edit_location_id" name="edit_location_id" value="<?php echo $info[0]['id']   ;?>"  type="hidden" />
              <input id="edit_location_deal_id" name="edit_location_deal_id" value="<?php echo $info[0]['deal_id']   ;?>" type="hidden" />
          
          <div class="form-group" style="width:80%">
            <input id="edit_location_address" name="edit_location_address" value="<?php echo $info[0]['address']   ;?>" placeholder="Address" type="text" class="form-control" style="width:100%"/>
          </div>
              <div class="form-group">
<!--            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="edit_location_state" name="edit_location_state"  class="location_city_search form-control" onchange="test()">
                <option value="">State</option>
                <?php //print_r($categoriesList);
                        foreach ($statesList as $key => $value) { ?>
                            <option value="<?php echo $value['id']   ;?>"   <?php echo $info[0]['state_id'] == $value['id'] ? "  selected "  : "" ;?> ><?php echo $value['name']   ;?></option>
                <?php   }    ?>
            </select>
          </div>
          <div class="form-group">
<!--            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="edit_location_city" name="edit_location_city"  class="form-control location_city_fill" >
                <option value="">City</option>  
                <option value="<?php echo $info[0]['city_id']   ;?>" selected><?php echo $info[0]['city_name']   ;?></option>
            </select>
          </div><br/>
          
            <input id="edit_location_lat" name="edit_location_lat" value="<?php echo $info[0]['lat']   ;?>" placeholder="Latitude" type="text" class="form-control"/>
            <input id="edit_location_lng" name="edit_location_lng" value="<?php echo $info[0]['lng']   ;?>" placeholder="Longitude" type="text" class="form-control"/>
          
          <div class="form-group">
<!--            <input id="location_status" name="location_status" value="" placeholder="Status" type="text" class="form-control"/>-->
            <select id="edit_location_status" name="edit_location_status"  class="form-control" >
                <option value="">Status</option>
                <?php //print_r($categoriesList);
                        foreach ($locationStatus as $key => $status) { ?>
                            <option value="<?php echo $key   ;?>"  <?php echo $info[0]['status'] == $key ? "  selected "  : "" ;?> ><?php echo $status   ;?></option>
                <?php   }    ?>
            </select>
          </div>
          
              
<!--          <div class="form-group" >
            <input id="location_category" name="location_category" value="" placeholder="Category" type="text" class="form-control"/>
          </div>-->
          
<!--          <button id="add_location" type="submit" class="btn btn-default">Add location</button>-->
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>
<!--        </div>
      </div>
    </div>-->
   
         