<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--<div class="modal-content">-->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Offer Edit #1</h4>
          </div>
          <form class="form-inline" role="form" id="offer_form" action="/index.php/deals/editOffer" method="POST">
          <div class="modal-body">
            
              <input id="offer_deal_id" name="offer_deal_id" value="<?php echo $model->deal_id   ;?>" type="hidden" />
              <input id="offer_deal_id" name="offer_id" value="<?php echo $model->id   ;?>"  type="hidden" />
          <div class="form-group">
            <input id="offer_name" name="offer_name" value="<?php echo $model->name   ;?>" placeholder="Name" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="offer_offer_price" name="offer_offer_price" value="<?php echo $model->offer_price   ;?>" placeholder="Offer Price" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <input id="offer_actual_price" name="offer_actual_price" value="<?php echo $model->actual_price   ;?>" placeholder="Actual Price" type="text" class="form-control"/>
         </div>
          <div class="form-group">
            <input id="offer_total_stock" name="offer_total_stock" value="<?php echo $model->total_stock   ;?>" placeholder="Total Stock" type="text" class="form-control"/>
          </div>
          
              
<!--          <div class="form-group">
            <input id="offer_stock_sold" name="offer_stock_sold" value="<?php echo $model->stock_sold   ;?>" placeholder="Stock Sold" type="text" class="form-control"/>
          </div>          -->
          <div class="form-group">
            <input id="offer_validity" name="offer_validity" value="<?php echo $model->validity   ;?>" placeholder="Validity" type="text" class="form-control"/>
         </div>
          <div class="form-group">
        <select id="offer_validity_mode" name="offer_validity_mode"  class="form-control" >
            <option value="">Validity Mode</option>
            <?php //print_r($categoriesList);
                    foreach ($offerValidityModes as $key => $mode) { ?>
                        <option value="<?php echo $key   ;?>"   
                                <?php echo $model->validity_mode == $key ? "  selected "  : "" ;?>
                                
                                ><?php echo $mode   ;?></option>
            <?php   }    ?>
        </select>
         </div>
        <div class="form-group">
            <input id="offer_sequence" name="offer_sequence" value="<?php echo $model->sequence   ;?>" placeholder="Sequence" type="text" class="form-control"/>
        </div>
          <div class="form-group">
            <select id="offer_status" name="offer_status"  class="form-control" >
                <option value="">Status</option>
                <?php //print_r($categoriesList);
                        foreach ($offerStatus as $key => $status) { ?>
                            <option value="<?php echo $key   ;?>"   <?php echo $model->status == $key ? "  selected "  : "" ;?>  ><?php echo $status   ;?></option>
                <?php   }    ?>
            </select>
          </div>
          
          <!--<div class="form-group">
            <input id="offer_img_url" name="offer_img_url" value="<?php echo $model->img_url   ;?>" placeholder="Image Url" type="text" class="form-control"/>
          </div>-->
          
          <div class="form-group">
            <input id="offer_min_amount" name="offer_min_amount" value="<?php echo $model->min_amount; ?>" placeholder="min amount" type="text" class="form-control"/>
          </div>
           <div class="form-group">
            <input id="offer_max_limit_peruser" name="offer_max_limit_peruser" value="<?php echo $model->max_limit_peruser; ?>" placeholder="per user" type="text" class="form-control"/>
          </div>
              <div class="form-group">
            <select id="offer_global_flag" name="offer_global_flag"  class="form-control" >
                <option value="" >
                   Global flag
                </option>
                <option value="0" <?php if($model->global_flag==0){ echo 'selected';} ?> >
                    With Location
                </option>
                <option value="1" <?php if($model->global_flag==1){ echo 'selected';} ?>>
                    Without Location
                </option>
            </select>
          </div>
          <div class="form-group">
            <input id="offer_order_flag" name="offer_order_flag" value="<?php echo $model->order_flag; ?>" placeholder="order flag" type="text" class="form-control"/>
          </div>
         <div class="form-group" >
           <?php
             if($model->coupon_expiry_date){
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'Coupon expiry date',    
                'value'=> $model->coupon_expiry_date,
                'options'=>array(        
                    'showButtonPanel'=>true,
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'style'=>'',
                    'placeholder'=>'Coupon expiry date',
                    'class'=>'form-control'
                ),
            ));
             } ?>
          </div>
<!--          <div class="form-group" >
            <input id="offer_category" name="offer_category" value="" placeholder="Category" type="text" class="form-control"/>
          </div>-->
          <div class="form-group" style="width:80%">
            <input id="offer_short_desc" name="offer_short_desc" value="<?php echo $model->short_desc;?>" placeholder="Short Description" type="text" class="form-control" style="width:100%"/>
          </div>
              <br/>
          <div class="form-group" style="width:80%">
            <textarea id="offer_long_desc" name="offer_long_desc" placeholder="Long Description" class="form-control" style="width:100%"><?php echo $model->long_desc   ;?></textarea>
            </div>
              <br/>
              <div class="form-group" style="width:80%">
                <input id="offer_short_desc" name="offer_of_desc" value="<?php echo $model->offer_desc;?>" placeholder="Offer Description" type="text" class="form-control" style="width:100%"/>
              </div>
            

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
            </form>