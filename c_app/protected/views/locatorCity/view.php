<?php
/* @var $this LocatorCityController */
/* @var $model LocatorCity */

$this->breadcrumbs=array(
	'Locator Cities'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List LocatorCity', 'url'=>array('index')),
	array('label'=>'Create LocatorCity', 'url'=>array('create')),
	array('label'=>'Update LocatorCity', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LocatorCity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LocatorCity', 'url'=>array('admin')),
);
?>

<h1>View LocatorCity #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'state_id',
		'name',
		'toShow',
	),
)); ?>
