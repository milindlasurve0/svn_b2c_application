<?php
/* @var $this LocatorCityController */
/* @var $model LocatorCity */

$this->breadcrumbs=array(
	'Locator Cities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LocatorCity', 'url'=>array('index')),
	array('label'=>'Manage LocatorCity', 'url'=>array('admin')),
);
?>

<h1>Create LocatorCity</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>