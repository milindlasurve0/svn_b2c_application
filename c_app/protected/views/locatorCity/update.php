<?php
/* @var $this LocatorCityController */
/* @var $model LocatorCity */

$this->breadcrumbs=array(
	'Locator Cities'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LocatorCity', 'url'=>array('index')),
	array('label'=>'Create LocatorCity', 'url'=>array('create')),
	array('label'=>'View LocatorCity', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage LocatorCity', 'url'=>array('admin')),
);
?>

<h1>Update LocatorCity <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>