<?php
/* @var $this LocatorCityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Locator Cities',
);

$this->menu=array(
	array('label'=>'Create LocatorCity', 'url'=>array('create')),
	array('label'=>'Manage LocatorCity', 'url'=>array('admin')),
);
?>

<h1>Locator Cities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
