<script>
    function searchvalidate(){
      var vaucher = $("#voucher").val();
      var custMobNum =  $("#custMobNum").val();
      $("#searcherror").hide();
      if(vaucher=='' && custMobNum==''){
          $("#searcherror").show();
          return false;
      }
      return true;
    }
    function redeem_Freebie(coupon,offerId,dealId,start){
          $.ajax({
                url:"<?php echo  Yii::app()->params['GLOBALS']['_URLS']["CONTROLLER"]["redeem_Coupon"] ;?>",
                method:"POST",
                dataType:"text",
                data:"code="+coupon+'&offerId='+offerId+'&dealId='+dealId+'&mobile='
            }).done(function(responce){
                if(responce.indexOf('_')!==-1){
                    var messageArray = responce.split('_');
                    var message = messageArray[0];
                    var status = messageArray[1];
                    var Elem  = "#active_"+start;
                    var Elem2  = "#inactive_"+start;
                    var status_id = "#status_"+start;
                    if(status=='success'){
                        alert(message);
                       // $(ElemObj).html("<b style='color:red'>Redeemed</b>");
                       $(Elem2).show();
                       $(Elem).hide();
                       if(message.indexOf('redeemed')){
                             $(status_id).html("<span style='font-weight:bold;color:#544099'>REDEEMED</span>");
                        }
                        else if(message.indexOf('expired')){
                             $(status_id).html("<span style='font-weight:bold;color:#333333'>EXPIRED</span>");
                        }
                    }
                    else{
                         alert(message);                   
                        
                    }
                }
                
            },start);
    }
</script>
<div class="maintop">
    <div class="mainbody_redeem">
        <div><strong><?php echo $dealerData[0]['dealer_name']; ?></strong></div>
        <div class="vouchercode">Voucher Code: Report of Free Gifts</div>
    </div>
<form action="" method="GET" id="searchFreeGift" name="searchFreeGift" class="form-inline" role="form" onsubmit="return searchvalidate()">
    <div class="filterbox" style="text-align: center;height:43px">
        <ul>
        <li>REDEEM FREE GIFT</li>
        <li><input type="text" name="voucher" id="voucher" placeholder="Enter voucher code"  class="vouchercode" size="20" placeholder="Enter voucher code for free gift" ></li>
        <li>OR</li>
        <li><input type="text" class="vouchercode" name="custMobNum" id="custMobNum" size="25" placeholder="Enter Pay1 registered mobile number"></li>
        <li><input type="submit" name="search_freegift" id="search_freegift" value="Search" name="submit"  /></li>
        </ul>
        <p align="center" class="vouchercode" id='searcherror' style="display:none;color:red">Please enter 'voucher code' or 'mobile number' to search</p>
   </div>
</form>
 </div>
<!-- </div>-->
<div style="clear:both;margin-left: 10px;padding-left: 10px">
<?php 
if(count($freegift_data)<=0 && $display===true){ ?>
    <div style="text-align:center;padding-top: 20px;width:100%">No Data</div>
<?php }
else{
if(isset($display) && $display===true){?>
<div class="offers" style="clear:both;padding-left: 10px;padding-top: 5px">
 <div  style="width:20%;height: 110px;float:left">Free Gift</div>
<div  style="width:20%;height: 110px;float:left">Grabbed Date</div>
<div  style="width:20%;height: 110px;float:left">Expiry Date</div>
<div  style="width:20%;height: 110px;float:left">Status</div>
<div  style="width:20%;height: 110px;float:left">Redemption</div>
</ul>
</div>

<?php
    $i=1;

    foreach($freegift_data as $result){
        $image = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$result['img_url'];
        $coupon = $result['code'];
        $offer_id = $result['offer_id'];
        $deal_id = $result['deal_id'];
        $couponId = $result['id'];
        $offer = $result['name'];
        $expirydate = $result['exDate'];
        $redeemDate = $result['redeemDate'];
        $status =  Yii::app()->params['GLOBALS']['_DEAL_COUPON_STATUS'][$result['status']];
        switch ($result['status']){
            case 1 :
              $color = "color:#146d13";
              break;
            case 2 :
                $color = "color:#544099";
                break;
            case 3 :
                $color = "color:#333333";
                break;
        }
    ?>
<div class="panel_body" style="vertical-align:middle;padding-top: 10px;padding-left: 10px">
<div  style="width:20%;height: 110px;float:left">    
    <img  src="<?php echo $image; ?>" title="<?php echo $offer; ?>" class='image'>
    </div>
    <div  style="width:20%;height: 110px;float:left"><?php echo $redeemDate; ?></div>
    <div  style="width:20%;height: 110px;float:left"><?php echo $expirydate; ?></div>
    <div  style="width:20%;height: 110px;float:left" id='status_<?php echo $i; ?>'><span style="font-weight:bold;<?php echo $color; ?>" ><?php echo $status; ?></span></div>
    <div  style="width:20%;height: 110px;float:left">
        <div class="button-redeem" id="inactive_<?php echo $i; ?>" style="display: <?php echo ($result['status']!=1) ? "block" : "none" ?>;background-color:#5D5D5D;color:#CCCCCC">
            Inactive
            </div>
        <div class="button-redeem green" onclick="redeem_Freebie('<?php echo $couponId; ?>','<?php echo $offer_id; ?>','<?php echo $deal_id; ?>','<?php echo $i; ?>')" id="active_<?php echo $i; ?>" style="cursor:pointer;display: <?php echo ($result['status']!=1) ? "none" : "block" ?>">
            Redeem
        </div>
</div>
    </div>
<?php
$i++;
    }
    
}  }
?>
 </div>
