<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchant Panel Dashboard</title>
<!--link rel="stylesheet" type="text/css" href="merchant_style.css"-->
<div class="maintop">
<div class="topleft">
<strong><?php echo $dealerData[0]['dealer_name']; ?></strong><br/><br/>
<div>Free Gifts:</div>
<div class="thum_img" style="overflow: auto; width: 590px; height: 188px;">
<ul>
    <?php
foreach($offerData as  $row){    
    $image = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$row['img_url'];
    $offer = $row['offer_name'];
    $offerid = $row['offerid'];
    ?>
<li><img  src="<?php echo $image; ?>" title="<?php echo $offer; ?>" class='image'></li>
<?php } ?>
</ul>
</div>

</div>
<div class="topright"><section id="overviewbox">
<div>
<h3 class="overview">OVERVIEW OF GIFTS</h3>
<span class="overview_hdtxt">Grabbed today :<?php echo isset($reportData[0]['redeemtoday']) ? $reportData[0]['redeemtoday'] : 0; ?> </span><br/>
<span class="overview_hdtxt">Grabbed yesterday : <?php echo isset($reportData[0]['redeemyday']) ? $reportData[0]['redeemyday'] : 0; ?></span><br/>
<span class="overview_hdtxt">Total Grabbed : <?php echo isset($reportData[0]['redeemtotal']) ? $reportData[0]['redeemtotal'] : 0; ?> </span><br/>
<br/>
<span class="overview_hdtxt">Redeemed today : <?php echo isset($reportData[0]['claimtoday']) ? $reportData[0]['claimtoday'] : 0; ?></span><br/>
<span class="overview_hdtxt">Redeemed yesterday : <?php echo isset($reportData[0]['claimyday']) ? $reportData[0]['claimyday'] : 0; ?></span><br/>
<span class="overview_hdtxt">Total Redeemed : <?php echo isset($reportData[0]['claimtotal']) ?  $reportData[0]['claimtotal'] : 0; ?></span><br/>
</div>
</section></div>
</div>

<form action="redeem" method="GET" id="searchFreeGift" name="searchFreeGift" class="form-inline" role="form">
  <div class="mainbody">
        <section id="redeembox">
        <div>
        <h3 class="login">REDEEM FREE GIFT</h3>
        <p align="center">
        <input type="text" name='voucher' id="voucher" placeholder="Enter voucher code"  class="vouchercode" size="36" placeholder="Enter voucher code for free gift" ><br>
          </p><p align="center">OR</p>
        <p align="center"><input type="text" class="vouchercode" name="custMobNum" id="custMobNum" size="36" placeholder="Enter Pay1 registered mobile number"><br><br>
        <!--a class="button green" href="#">Search</a-->
         <input type="submit" name="search_freegift" id="search_freegift" value="Search" name="submit"  />
        </p></div>
        </section>
    </div>
</body>
</html>