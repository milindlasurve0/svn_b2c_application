<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
        $(function() {
            $( "#start_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
            $( "#end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
        });
function validate(){
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    if($.trim(start_date).length < 1){
        alert("Please select start date");
        return false;
    }
    if($.trim(end_date).length < 1){
        alert("Please select end date");
        return false;
    } 
    var oneDay = 24*60*60*1000;	// hours*minutes*seconds*milliseconds
    var firstDate = new Date(start_date);
    var secondDate = new Date(end_date);

    var diffDays = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay));
    if(diffDays>30){
        alert("Please select 30 days date range");
        return false;
    }
    return true;
}

</script>
 <form action="" method="GET" id="searchForm" name="searchForm" role="form" class="form-search" style="text-align: center" onsubmit="return validate()">
<div class="mainbody_redeem" style="text-align:left">
<div ><strong><?php echo $dealerData[0]['dealer_name']; ?></strong></div>
<div class="vouchercode">Voucher Code: Report of Free Gifts</div>
</div>
<div class="filterbox">
<ul>
<li>Filter by</li>
<li><select name="offers">
        <option value="">All</option>
        <?php foreach($offers as $row){ ?>
        <option value="<?php echo $row['offerid']; ?>"><?php echo $row['offer_name']; ?></option>
        <?php } ?>
    </select>
</li>
<li>Between</li>
<li><input type="text" id="start_date" name="start_date" ></li>
<li>Till</li>
<li><input type="text"  id="end_date" name="end_date"></li>
<li><input type="submit" value="search" class="submit-button" name="search"></li>
</ul>
</div>
<?php
if(isset($reportData)){
    ?>

<?php
    if(empty($reportData)){
        ?>
<div class="results_txt"  style="text-align:center;padding-top: 20px;width:100%">No Data </div>
        <?php
    }
    else{
?>
<div class="results_txt" ><span class="result_sty">Results</span><br >Total : <?php echo count($reportData); ?> Record(s)<!--br>Viewing 1 to 20 (50 Total)--></div>
<div style="width:60%;padding-left: 242px" >
<div class="offers" style="clear: both" >
<ul>
<li>Offers</li>
<li>Date</li>
<li>Grabbed</li>
<li>Redeemed</li>
<li>Expiry</li>
<li>Total Gifts</li>
</ul>
</div>
 <?php foreach ($reportData as $row){ ?>
<div class="offers_body" style="float:left"><ul>
<li><?php echo $row['offer']; ?></li>
<li><?php echo $row['date']; ?></li>
<li><?php echo $row['grabbed']; ?></li>
<li><?php echo $row['redeem']; ?></li>
<li><?php echo $row['expired']; ?></li>
<li><?php echo $row['total']; ?></li>
</ul></div>
<?php } ?>
</div>
  <?php  } }?>
 </form>