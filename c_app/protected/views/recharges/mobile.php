<?php
/* @var $this RechargeController */
/* @var $model Recharge */

?>
<form name = "mobile_recharge" method = "POST" action = "<?php echo Yii::app()->baseUrl . '/index.php/transactions'; ?>">
    <div class="control-group">
        <label for="mobile" class="control-label" >Mobile Number</label>
        <input type="text" name="mobile" class="form-control" placeholder="Mobile Number"/>
    </div><br/>
    <div class="control-group">
        <div class="controls">
            <label for="operator" class="control-label">Operator</label>                        
            <select name="operator" class="span5">
                <?php
                foreach (TelecomComponent::getOperator() as $id => $name) {
                    echo "<option value='$id'>$name</option>";
                }
                ?>
            </select>
        </div>
    </div><br/>
    <div class="control-group">
        <div class="controls">
            <label for="amount" class="control-label" >Amount</label>
            <input type="text" name="amount" class="form-control" placeholder="amount" />
        </div>
    </div><br/>
    <div class="control-group">
        <div class="controls">
            <label class="radio">
                <input type="radio" name="payment" id="optionsRadios1" value="wallet" checked=""> Pay by Wallet</label>
            <label class="radio">
                <input type="radio" name="payment" id="optionsRadios1" value="online" > Pay Online
            </label>
            <button name="recharge" class="btn" onclick="javascript:alert('rr')" >Continue</button>
        </div>
    </div>

</form>