<script>
function validate(){
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    if($.trim(start_date).length < 1){
        alert("Please select start date");
        return false;
    }
    if($.trim(end_date).length < 1){
        alert("Please select end date");
        return false;
    }    
    return true;
}

</script>

<div style="border: 1px solid;	border-radius: 25px;">
    <form action="" method="POST" id="searchForm" name="searchForm" role="form" class="form-search" style="text-align: center" onsubmit="return validate()">
    <fieldset>
    <legend>Search</legend>
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'start_date',    
                'value'=> isset($start_date)?$start_date:"",
                'options'=>array(        
                    'showButtonPanel'=>true,
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'style'=>'',
                    'placeholder'=>'Start Date'
                ),
            ));?>
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'end_date',    
                'value'=>isset($end_date)?$end_date:"",
                'options'=>array(        
                    'showButtonPanel'=>true,
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'style'=>'',
                    'placeholder'=>'End Date'
                ),
            ));
            ?>   
        <input type="submit" name="search" value="search" class="btn">
    </fieldset>
</form><br/>
</div>
<hr/>
<?php $url_date_string = "sd=".$start_date."&ed=".$end_date;?>
<ul>
    <li>
        Total New Users : <a target="_blank" href="detail_reports?type=new_user&<?php echo $url_date_string;?>"><?php echo $total_new_users;?></a>
    </li>
    <hr/>
    <li>
        Total Recharge [<a target="_blank" href="detail_reports?type=total_recharge&<?php echo $url_date_string;?>"><?php echo $recharge_total_count; ?></a>]: <?php echo $recharge_total_amount; ?>
        <ul>
            <li>
                Wallet [<a target="_blank" href="detail_reports?type=wallet_recharge&<?php echo $url_date_string;?>"><?php echo $recharge_wallet_count; ?></a>] : <?php echo $recharge_wallet_amount; ?>
            </li>
            <li>
                Online [<a target="_blank" href="detail_reports?type=online_recharge&<?php echo $url_date_string;?>"><?php echo $recharge_online_count; ?></a>] : <?php echo $recharge_online_amount; ?>
            </li>
        </ul>
    </li>
    <hr/>
    <li>
        Total Billing [<a target="_blank" href="detail_reports?type=total_billing&<?php echo $url_date_string;?>"><?php echo $billing_total_count; ?></a>] : <?php echo $billing_total_amount; ?>
        <ul>
            <li>
                Wallet [<a target="_blank" href="detail_reports?type=wallet_billing&<?php echo $url_date_string;?>"><?php echo $billing_wallet_count; ?></a>] : <?php echo $billing_wallet_amount; ?>       
            </li>
            <li>
                Online [<a target="_blank" href="detail_reports?type=online_billing&<?php echo $url_date_string;?>"><?php echo $billing_online_count; ?></a>] : <?php echo $billing_online_amount; ?>
            </li>
        </ul>
    </li>
    <hr/>
    <li>
        Total Top Up [<a target="_blank" href="detail_reports?type=total_refill&<?php echo $url_date_string;?>"><?php echo $refill_total_count; ?></a>] : <?php echo $refill_total_amount; ?>
        <ul>
            <li>
                Retailer [<a target="_blank" href="detail_reports?type=wallet_refill&<?php echo $url_date_string;?>"><?php echo $refill_wallet_count; ?></a>] : <?php echo $refill_wallet_amount; ?>
            </li>
            <li>
                Online [<a target="_blank" href="detail_reports?type=online_refill&<?php echo $url_date_string;?>"><?php echo $refill_online_count; ?></a>] : <?php echo $refill_online_amount; ?>
            </li>
            <li>
                Voucher [<a target="_blank" href="detail_reports?type=voucher_refill&<?php echo $url_date_string;?>"><?php echo $refill_voucher_count; ?></a>] : <?php echo $refill_voucher_amount; ?>
            </li>
        </ul>
    </li>
    <hr/>
    <li>
        Total Deal Sold [<a target="_blank" href="detail_reports?type=total_deal&<?php echo $url_date_string;?>"><?php echo $deal_total_count; ?></a>] : <?php echo $deal_total_amount; ?>
        <ul>
            <li>
                Wallet [<a target="_blank" href="detail_reports?type=wallet_deal&<?php echo $url_date_string;?>"><?php echo $deal_wallet_count; ?></a>] : <?php echo $deal_wallet_amount; ?>
            </li>
            <li>
                Online [<a target="_blank" href="detail_reports?type=online_deal&<?php echo $url_date_string;?>"><?php echo $deal_online_count; ?></a>] : <?php echo $deal_online_amount; ?>
            </li>
        </ul>
    </li>
    <hr/>
    <li>
        Direct Transaction [<a target="_blank" href="detail_reports?type=total_directTransaction&<?php echo $url_date_string;?>"><?php echo $Direct_Transaction; ?></a>] : <?php echo $Direct_Transaction_amount; ?>
    </li>
    <hr />
    <li>
        Total Repeated Users : <a target="_blank" href="detail_reports?type=user_repeated&<?php echo $url_date_string;?>"><?php echo $repeated_users_count; ?></a>
    </li>
    <hr/>
    <li>
        Total User Registered (Till <?php echo GeneralComponent::getFormatedDate();?>) : <b><a target="_blank" href="detail_reports?type=n_user&<?php echo $url_date_string;?>"><?php echo ReportingComponent::get_totalUsers();?></a></b>
    </li>
    <hr/>
    <li>
        <b><a target="_blank" href="genuine_non_genuine_users"><?php echo 'Genuine & Non Genuine Users';?></a></b>
    </li>
    <hr/>
    <li>
        <b><a target="_blank" href="miss_call_users"><?php echo 'Miss Call Users';?></a></b>
    </li>
    <hr/>
    <li>
        Dealer Map (Till <?php echo GeneralComponent::getFormatedDate();?>) : <b><a target="_blank" href="map_reports?type=dealer"><?php echo $dealerMapCount;?></a></b>
    </li>
   <li>
        User Map (Till <?php echo GeneralComponent::getFormatedDate();?>) : <b><a target="_blank" href="map_reports?type=user"><?php echo $userMapCount;?></a></b>
    </li>
    <li>
        Retailer Map (Till <?php echo GeneralComponent::getFormatedDate();?>) : <b><a target="_blank" href="map_reports?type=retailer"><?php echo $retailerMapCount;?></a></b>
    </li>
    <hr/>
    <li><font color="blue">Upload Distributor Extra Incentive List</font>
    <div id="dealer_couponDiv">
    <form action="/panel/UploadExtraIncentiveList" method="post" enctype="multipart/form-data">
            <div style="padding-top:10px">
                <div style="float:left">
                    <input type="file" name="file1" id="file1">
                </div>
                <div style="float:left;padding-left:10px">
                      <input type="submit" value="Submit" name="Submit">
                </div>
            </div>
    </form>
    </div>
    </li>   
    
</ul>

    
