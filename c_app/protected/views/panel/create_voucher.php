<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript">

    $(function() {
        $('#count').keydown(function(e) {
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 16) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                    e.preventDefault();
                }
            }
        });
        $('#amount').keydown(function(e) {            
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 16) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || (key == 190) || (key == 110))) {
                    e.preventDefault();
                }
            }
        });
       
     });
     function display_promo(obj){
         if(obj.value==1)
         $("#promo_voucher_details").show();
     else
         $("#promo_voucher_details").hide();
     }
     function showvoucher(id1,id2){
         $("#"+id1).show();
         $("#"+id2).hide();
     }
//     $("vreport").validate({
//        rules: {
//          mobile_number: {
//            require_from_group: [1, ".vreport-group"]
//          },
//          vcode: {
//            require_from_group: [1, ".vreport-group"]
//          },
//          start_date: {
//            require_from_group: [1, ".vreport-group"]
//          },
//          end_date: {
//            require_from_group: [1, ".vreport-group"]
//          }
//        }
//      });

    function validate(){
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        if(!($.trim(start_date).length <1) && ($.trim(end_date).length < 1)){
            if($.trim(start_date).length < 1){
            
            }
            if($.trim(end_date).length < 1){
                alert("Please select end date");
                return false;
            }    
            
        }else if($("#mobile_number").val()=="" && $("#vcode").val()=="" && $.trim(start_date).length <1 && $.trim(end_date).length < 1 ){
            alert("Please select atleats one search criteria");
            return false;
        }
        return true;
    }
    
    
</script>     
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10-dev/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
     $(document).ready(function() {
       $('#voucher_report').dataTable();
    });
</script>
<?php
    
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="msg_flash flash-' . $key . '">' . $message . "</div>\n";
    }
   
    Yii::app()->clientScript->registerScript(
       'myHideEffect',
       '$(".msg_flash").animate({opacity: 1.0}, 1000).fadeOut("slow");',
       CClientScript::POS_READY
    );

?>
<ul class="nav nav-tabs">
    <li <?php if(isset($_REQUEST['vreport_submit'])){ ?> class="active" <?php } ?>><a href="#report" data-toggle="tab" onclick="showvoucher('report','createvoucher')">Reports</a></li>
    <li <?php if(isset($_REQUEST['submit'])){ ?> class="active" <?php } ?>><a href="#createvoucher" onclick="showvoucher('createvoucher','report')" data-toggle="tab">Create Voucher</a></li>
</ul>
<div id="report" style="display:<?php if(isset($_REQUEST['vreport_submit'])){ echo ""; }else{ echo "none"; } ?>;border-bottom-width: 25px;padding-bottom: 25px;padding-top: 25px;width:100%" >
    <form action="" method="GET" id="vreport" name="vreport" class="form-inline" role="form" onsubmit="return validate()">
        <select name="v_type" class="form-control" id="v_type" >
                    <?php
                        foreach (Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'] as $id => $name) {
                            echo "<option value='$id'>".ucfirst(strtolower($name))."</option>";
                        }
                    ?>
         </select>
        <input type="number" class="form-control vreport-group" maxlength="10" name="mobile_number" id="mobile_number" placeholder="Pay1 Mobile No.">
        <input type="text" class="form-control vreport-group" name="vcode" id="vcode" placeholder="Voucher Code">
       <br />
       <br />
         <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'start_date',    
                'value'=> isset($start_date)?$start_date:"",
                'options'=>array(        
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                     'class'=>'form-control vreport-group',
                    'placeholder'=>'Start Date'
                ),
            ));?>
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'end_date',    
                'value'=>isset($end_date)?$end_date:"",
                'options'=>array(        
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'class'=>'form-control vreport-group',
                    'placeholder'=>'End Date'
                ),
            ));
            ?>   
            
         <button class="btn btn-success " name="vreport_submit" id="vreport_submit" value="Search">Search</button>
         
    </form>
    <br>
    <br>
    <?php if(isset($status) && !empty($status) && isset($_REQUEST['vreport_submit'])){
        if($status=='failure'){ echo $description; }
        else { 
            $data_col = array('User Mobile','Voucher','Voucher Created On','Voucher Used On','Amount','Status');
            $data_count = count($description);
            $data_title = "Voucher Details Report";
            /*$this->widget('ext.htmltableui.htmlTableUi',array(
                        'ajaxUrl'=>'site/handleHtmlTable',
                        'arProvider'=>'',    
                        'collapsed'=>false,
                        'columns'=>$data_col,
                        'cssFile'=>'',
                        'editable'=>false,
                        'enableSort'=>true,
                        'exportUrl'=>'site/exportTable',
                        'extra'=>'',
                        'footer'=>'Total rows: '.($data_count).' ',
                        'formTitle'=>'Form Title',
                        'rows'=>$description,
                        'sortColumn'=>1,
                        'sortOrder'=>'desc',
                        'subtitle'=>'',
                        'title'=>$data_title,
                    ));*/
            
              ?>
                <table class="table table-striped table-bordered" id="voucher_report" name="voucher_report" style="width: 80%;">
                   <thead>
                       <?php foreach ($data_col as $col){ ?>
                        <th style="text-align:center;cursor: pointer"><?php echo $col; ?></th>
                       <?php } ?>
                   </thead>
                   <tbody>
                       <?php foreach ($description as $row){ ?>
                       <tr>
                            <?php foreach ($data_col as $col){ ?>
                           <td style="text-align:center"><?php echo $row[$col]; ?></td>
                            <?php } ?>
                       </tr>
                       <?php } ?>
                   </tbody>
                </table>
             
  
    <?php  } } ?>
</div>
<div id="createvoucher" style="display: <?php if(isset($_REQUEST['submit'])){ echo ""; }else{ echo "none"; } ?>" >
    <h2 >Create Voucher</h2>
    <form name="vcform" id="vcform" method="get" onsubmit="return validate_voucher_cform();">
        <div class="control-group">
            <label class="control-label">Client Name</label>
            <input type="text" id="client" name="client" class="form-control">
        </div><br/>
        <div class="control-group">
            <label class="control-label">Amount of each voucher</label>
            <input type="text" name="amount" id="amount" class="form-control">
        </div><br/>
        <div class="control-group">
            <label class="control-label">Number of vouchers</label>
            <input type="text" name="count" id="count" class="form-control">
        </div><br/>
        <div class="control-group">
            <div class="controls">
                <label for="voucher_type" class="control-label">Voucher_type</label>                        
                <select name="voucher_type" class="form-control" id="operator" onchange="display_promo(this)">
                    <?php
                        foreach (Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'] as $id => $name) {
                            echo "<option value='$id'>".ucfirst(strtolower($name))."</option>";
                        }
                    ?>
                </select>
            </div>
        </div><br/>
        <div id="promo_voucher_details" style="display:block">
            <div class="control-group">
                <label class="control-label">Total balance</label>
                <input type="text" name="balance" id="balance" class="form-control">
            </div><br/>
            <div class="control-group">
                <label class="control-label">validity</label>
                <input type="date" name="validity" id="validity" class="form-control">
            </div><br/>
            <div class="control-group">
                <label class="control-label">code</label>
                <input type="text" name="code" id="code" class="form-control">
            </div><br/>
             <div class="control-group">
                <label class="control-label">Total stock</label>
                <input type="text" name="quantity" id="quantity" class="form-control">
            </div><br/>
            <div class="control-group">
                <label class="control-label">Max per user</label>
                <input type="text" name="max_per_person" id="max_per_person" class="form-control">
            </div><br/>
        </div>
        <div class="control-group">
            <label class="control-label">Check to generate voucher with dry feature</label>
            <input type="checkbox" value="1" name="is_dry" id="is_dry"/>
        </div><br/>    
        <div>
            <button name="submit" class="btn btn-success">Submit</button>
        </div>
    </form>
</div>
