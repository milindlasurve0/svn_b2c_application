<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mobile Payment gateway</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="b2c.pay1.loc/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="b2c.pay1.loc/bootstrap/css/bootstrap-theme.min.css">
<script src="b2c.pay1.loc/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style type="text/css">
body {
margin: 0px;
padding:0px;
}

h1{
font-size:30px;
font-family:"Encode Sans Normal";
margin:0;
text-align:center;
/*padding:10px 0 0 0;*/
padding: 7px 5px;
color:#414142;
}

h2{
font-size:14px;
font-family:"Encode Sans Narrow Medium";
margin:0;
padding:10px 0 5px 0;
color:#513D98;
clear:left;
}

ul{
margin:0 0 10px 0;
padding-left:20px;
font-size:20px;
}

/*
li{
font-size:20px;
background: url(images/arrow.png) no-repeat right center;
background-position:95% 50%;
margin:0;
font-family:"Encode Sans Normal";
color:#503C97;
list-style-type:none;
border-bottom-width: 1px;
line-height:50px;
border-bottom-style: solid;
padding: 0 0 5px 0; 
}*/

/*.discount_title{
font-family:"Encode Sans Normal";
font-size:20px;
text-align:center;
font-style: normal;
}*/

.btn_style{
	background-color:#503e98;
	color:#FFFFFF;
	font-size:14px;
	width:165px;
	height:40px;
	box-shadow: 0 0px 0 #39286b, 0 1px 0 #39286b, 0 1px 1px rgba(0,0,0,0.3);
        cursor:pointer;
}

.card_tab li{
font-size:20px;
display:block;	
background: url(/images/arrow.png) no-repeat right center;
background-position:95% 50%;
margin:0;
font-family:"Encode Sans Normal";
color:#503C97;
list-style-type:none;
border-bottom-width: 1px;
line-height:40px;
border-bottom-style: solid;
padding: 0 0 5px 0;
cursor:pointer;
}	

.top_border{
border-top-style: solid;
border-top-width: 1px;
}
/*
.text_font{
font-family:"Encode Sans Normal";
}*/

button, input, select, textarea {
  font-family : inherit;
  font-size   : 100%;
  padding-left: 1px;
  border-style: solid;
  border-color: #503C97;
  border-width: 1px;
  font-family: "Encode Sans Normal";
  padding: 7px 5px;
}
.img_space{
  padding-left:2px;
  padding-right:2px;
}
.desc_text{
  padding-left:20px;
  font-family: "Encode Sans Normal";
  font-size:12px;
}
.note_text{
  font-family:"Encode Sans Normal";
  font-size:12px;
}

label{
  font-family:"Encode Sans Normal";
  font-size:12px;
}
.color_purple{
 color:#503E95;
}

.color_grey{
 color:#414041;
}

.uline{
text-decoration:underline;
color:#414041;	
}

</style>
<script src='http://b2c.pay1.loc/bootstrap/js/jquery.min.js' ></script>
<script  type="text/coffeescript" src='http://b2c.pay1.loc/bootstrap/js/jquery.CC.coffee' ></script>
<script src='http://b2c.pay1.loc/bootstrap/js/jquery.CC.js'></script>
<script>
    function selectCard(type){
        var card = "";
        if(type=='credit'){
            card = 'c_';
            
        }else if(type=='debit'){
             card = 'd_';
        }
        var val = $("#"+card+"CardType").val(); 
        if($.isNumeric(val)){
            $("#"+card+"name").val($("#"+card+"name_on_card"+val).val());
            $("#store_card_token").val($("#"+card+"card_token"+val).val());
            console.log(card+"cnum "+$("#"+card+"card_no"+val).val());
            $("#"+card+"num").val($("#"+card+"card_no"+val).val());
            $("#"+card+"name").attr("disabled","disabled");
            $("#"+card+"num").attr("disabled","disabled");
            $("."+card+"hideExpiry").css('display','block');
            $("#"+card+"expmon").css('display','none');
            $("#"+card+"expyr").css('display','none');
            $("#span_storecard").css('display','none');
        }else{
            $("#"+card+"name").val('');
            $("#store_card_token").val('');
            $("#"+card+"num").val('');
            $("#"+card+"name").removeAttr("disabled");
            $("#"+card+"num").removeAttr("disabled");
            $("#"+card+"expmon").css('display','block');
            $("#"+card+"expyr").css('display','block');
            $("."+card+"hideExpiry").css('display','none');
            $("#span_storecard").css('display','block');
        }
    }    
    
    
    function checkCard(key){
        var card = "";
        var errorFlag=0;
          if($("#cardtype").val()=='credit'){
               card = 'c';
          }else if($("#cardtype").val()=='debit'){
               card = 'd';
          } // console.log('card '+card);
            if(card!='' ){
                if($("#"+card+"_cvv").val()=='' && $("#"+card+"_cvv").length!=3){
                    //console.log('_carddetailserror '+card);
                     $("#"+card+"_carddetailserror").show();
                     errorFlag=1;
                    // console.log('test');
                }else{
                       $("#ccnum").val($("#"+card+"_num").val());
                        $("#ccname").val($("#"+card+"_name").val());
                        $("#ccvv").val($("#"+card+"_cvv").val()); //console.log(card+'_cvv '+$("#"+card+"_cvv").val());
                        $("#ccexpmon").val($("#"+card+"_expmon").val());
                        $("#ccexpyr").val($("#"+card+"_expyr").val());
                         var cardToken =  $("#store_card_token").val();
                         var val = $("#"+card+"_CardType").val(); 
                        if(cardToken =='' && typeof($("#"+card+"_card_token0").val())!=="undefined" && !isNaN(val)){
                            $("#store_card_token").val($("#"+card+"_card_token0").val());
                        }
                        if( $("#store_card_token").val()!=='' && errorFlag==0){
                             console.log('cardToken '+cardToken+' val '+$("#ccvv").val()+' store_card_token '+$("#store_card_token").val());
                                $("#"+card+"_loading").css('display','block');
                                $("#postpayu").submit();
                             
                        }else{
                        
                            if($("#"+card+"_expmon").val()==''){
                                $("#"+card+"_carddetailserror").show();
                                errorFlag=1;
                            }
                            if($("#"+card+"_expyr").val()==''){
                                $("#"+card+"_carddetailserror").show();
                                errorFlag=1;
                            }
                            if(errorFlag===0){
                                
                                $("#"+card+"_num").validateCreditCard(function(result){
                               //$("#emailerror").hide();
                                   $("#"+card+"_carddetailserror").hide();
                                  if(result.luhn_valid){
                                       var cardName = $("#"+card+"_CardType").val();
                                        $("#bankcode").val(cardName);
                                        //console.log('test');
                                        //console.log('pg '+$("#pg").val()+' bankcode '+$("#bankcode").val()+' ccnum '+$("#ccnum").val()+' ccname '+$("#ccname").val()+' ccvv '+$("#ccvv").val()+' $("#ccexpmon").val() '+$("#ccexpmon").val()+' $("#ccexpyr").val() '+$("#ccexpyr").val()+' phone '+$("#phone").val()+' email '+$("#emailid").val()+' firstname '+$("#firstname").val()+' productinfo '+$("#productinfo").val()+' amount '+$("#amount").val()+' txnid '+$("#txnid").val()+' key '+$("#key").val());
                                        //+' ccnum '+$("#ccnum").val()+' ccname '+$("#ccname").val()+' ccvv '+$("#ccvv").val()+' $("#ccexpmon").val() '+$("#ccexpmon").val()+' $("#ccexpyr").val() '+$("#ccexpyr").val()+' phone '+$("#phone").val()+' email '+$("#emailid").val()+' firstname '+$("#firstname").val()+' productinfo '+$("#productinfo").val()+' amount '+$("#amount").val()+' txnid '+$("#txnid").val()+' key '+$("#key").val()
                                        $("#"+card+"_loading").css('display','block');
                                        $("#postpayu").submit();
                                   }
                                   else{
                                       $("#"+card+"_carddetailserror").show();
                                       event.preventDefault();
                                   }
                               });
                            }
                    }
                }
        }
    }
    
    function paybyNetBank(){
      var netbanking_select = $("#netbanking_select").val();
       $("#bankcode").val(netbanking_select);
        if(netbanking_select!=''){
            $("#netbanking_error").hide();
            $("#postpayu").submit();
        }
        else{
            $("#netbanking_error").show();
            event.preventDefault();
        }
    }
    function selectCardType(cardType,cardName){
        switch (cardType){
            case 'c_' :
                if(cardName=='visa' || cardName=='mastercard'){
                    $("#bankcode").val('CC');
                }
                else if(cardName=='amex'){
                    $("#bankcode").val('AMEX');
                }
                else if(cardName=='diners'){
                    $("#bankcode").val('DINR');
                }
            break;
            case 'd_' :
                if(cardName=='visa'){
                   $("#bankcode").val('VISA'); 
                }
                else if(cardName=='mastercard'){
                    $("#bankcode").val('MAST');
                }
                else if(cardName=='maestro'){
                    $("#bankcode").val('MAES');
                }
              break;

        }
        return  $("#bankcode").val();
    }
        
    function validate_card(type){
         $('#'+type+'num').validateCreditCard(function(result){ 
             //console.log(result);
            if(result.luhn_valid){
                $("#"+type+"carddetailserror").hide();
                //console.log(result.card_type.name);
                var dropdown = ''+type+'CardType';
                //console.log(selectCardType(type,result.card_type.name));
               $('select[name="'+dropdown+'"]').find('option[value="'+selectCardType(type,result.card_type.name)+'"]').attr("selected",true);
            }else{
                $("#"+type+"carddetailserror").show();
            }
         });
    }
   
    $(function() {
    $('#mobilenum,#c_num,#c_cvv,#d_num,#d_cvv').keydown(function(e) {
        
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 16) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                    e.preventDefault();
                }
            }
        });
        
       $('#c_name,#d_name').keypress(function(e)
      {
          var code = e.keyCode ? e.keyCode:e.which; // Get the key code.
          
          var pressedKey = String.fromCharCode(code); // Find the key pressed.
          if(!pressedKey.match(/[a-zA-Z ]/g) && code!=8) // Check if it's a alphabet char or not.
          {
              e.preventDefault(); // If it is not then prevent the event from happening.
         
          }
      });
    });
    
    function paybyPayu(){
            var email =  $("#emailid").val();
           if(email!='' && validateEmail(email)==false){
                $("#emailerror").show();
                return false;
                
            }
            else{
                $("#emailerror").hide();
                $("#postpayu").submit();   
            }
        
    }
    
    function validateEmail(Email) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(Email)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    /*function luhnChk() {
        var numbID = document.getElementById('ccnum');
        if(numbID && numbID.value!=''){
            var numb = numbID.value;
            var len = numb.length;
            var mul = 0;
            var prodArr = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]];
            var sum = 0;

            while (len--) {
                sum += prodArr[mul][parseInt(numb.charAt(len), 10)];
                mul ^= 1;
            }
            
            var flag = (sum % 10 === 0 && sum > 0);
            if(flag==true){
                return flag;
            }
            else{
                alert('Pls check your card number');
                return flag;
            }
        }
        else {
            return true;
        }
    }
    */
   function enablePayment(payMethod){
       $("#emailerror").hide();
       $("#carddetails_submit").hide();
       $("#paymentMethod").show(500);
       $("#paymentOption").hide(500);
       $("#payudetails").hide(500);
       $("#carddetails").hide(500);
       $("#d_carddetails").hide(500);
       $("#netbankdetails").hide(500);
        switch (payMethod){
            case 'creditcard':
                 $("#cardtype").val('credit');
                 $("#pg").val('CC');
                 $("#carddetails").show(500);
                  $("#CCCardType_p").show();
                  $("#carddetails_submit").show();
                break;
            case 'debitcard':
                $("#debitCardType_p").show();
                $("#cardtype").val('debit');
                $("#pg").val('DC');
                $("#d_carddetails").show(500);
                $("#carddetails_submit").show();
                break;
            case 'netbank':
                $("#cardtype").val('');
                $("#pg").val('NB');
                $("#netbankdetails").show(500);
                break;
            case 'payu':
                $("#cardtype").val('');
                $("#pg").val('-');
                $("#payudetails").show(500);
                break;
        }
    }
    
    function select_netbanking(ElemObj){
        var bankcode = ElemObj.value;
         $("#bankcode").val(bankcode);
    }
    function displayPayOption(){
        $("#emailerror").hide();
        $("#paymentMethod").hide(500);
        $("#paymentOption").show(500);
    }
    function addlabel(){
        if( document.getElementById("store_card").checked){
            $("#card_label_p").show(500);
            $("#store_card").val(1);
        }else{
            $("#card_label_p").hide();
            $("#store_card").val(0);
        }
    }
    function d_addlabel(){
        if( document.getElementById("d_store_card").checked){
            $("#d_card_label_p").show(500);
            $("#d_store_card").val(1);
        }else{
            $("#d_card_label_p").hide();
            $("#d_store_card").val(0);
        }
    }
    
    function payumoney(){
        $("#pg").val('Wallet')
        $("#bankcode").val('payuw')
        $("#postpayu").submit();
    }
</script>
</head>

<body>
    <?php //var_dump($UserCardDetails);
    if($UserCardDetails['status']==1){ $i=0;
        foreach($UserCardDetails['user_cards'] as $row){
            if($row['is_expired']==0){
                $card_mode = $row['card_mode']=='CC' ? 'c_' : 'd_';
           ?>
    <input type="hidden" id="<?php echo $card_mode."name_on_card".$i; ?>" name="<?php echo $card_mode."name_on_card".$i; ?>" value="<?php echo $row['name_on_card']; ?>">
    <input type="hidden" id="<?php echo $card_mode."card_token".$i; ?>" name="<?php echo $card_mode."card_token".$i; ?>" value="<?php echo $row['card_token']; ?>">
    <input type="hidden" id="<?php echo $card_mode."card_no".$i; ?>" name="<?php echo $card_mode."card_no".$i; ?>" value="<?php echo $row['card_no']; ?>">
    
    <?php
           $i++;
            }
        }
    }
    ?>
<form id="postpayu" name="postpayu" method="post" action="<?php echo $B2C_SUBMIT_URL; ?>" >
<div id="paymentOption">
 <hr />   
<h1>Rs.<?php echo $amount; ?></h1>
<p align="center" class="note_text">Transaction ID : <?php echo $txnid; ?></p>

 <div class="container">
    <div class="jumbotron">
     <p class="desc_text">Select your prefered payment method
      <ul class="card_tab">
        <a href="#carddetails" style="text-decoration: none;"><li class="top_border" onclick="enablePayment('creditcard');">Credit Card</li></a>
        <a href="#d_carddetails" style="text-decoration: none;"><li onclick="enablePayment('debitcard');" >Debit Card</li></a>
        <a href="#netbankdetails" style="text-decoration: none;"><li onclick="enablePayment('netbank');">Net Banking</li></a>
        <a href="#payudetails" style="text-decoration: none;"><li  onclick="payumoney('payu');">PayU</li></a>
      </ul>
     </p>
    </div>
 </div>
</div>
<div id="paymentMethod" style="display:none">
<p align="left" style="padding-left:20px"><input type="button" value="Back" onclick="displayPayOption()" class="btn_style"/></p>
<input type="hidden" name="key" id ="key" value="<?php echo $key; ?>" />
<!--input type="hidden" name="salt" value="<?php //echo $salt; ?>" /-->
<input type="hidden" name="email" id ="email" value="<?php echo $email; ?>" />
<input type="hidden" name="user_credentials" id ="user_credentials" value="<?php echo $user_credentials; ?>" />
<input type="hidden" name="hash" id ="hash" value="<?php echo $hash; ?>" />
<input type="hidden" name="txnid" id ="txnid" value="<?php echo $txnid; ?>" />
<input type="hidden" name="phone" id ="phone" value="<?php echo $phone; ?>" />
<input type="hidden" name="pg" id ="pg" value="CC" />

<input type="hidden" name="bankcode" id ="bankcode" id="bankcode" value="CC" />
<input type="hidden" name="cardtype" id ="cardtype"  id="cardtype" value="" >
<input type="hidden" name="amount" id ="amount"  value="<?php echo $amount; ?>" />
<input type="hidden" name="firstname" id ="firstname" value="<?php echo $firstname; ?>" />
<input type="hidden" name="surl" id ="surl" value="<?php echo $surl; ?>" />
<input type="hidden" name="curl" id ="curl" value="<?php echo $curl; ?>" />
<input type="hidden" name="furl" id ="furl" value="<?php echo $furl; ?>" />
<input type="hidden" name="productinfo" id ="productinfo" value="<?php echo $productinfo; ?>" />

<input type="hidden" name="ccnum" id ="ccnum" value="" />
<input type="hidden" name="ccname" id ="ccname" value="" />
<input type="hidden" name="ccvv" id ="ccvv" value="" />
<input type="hidden" name="ccexpmon" id ="ccexpmon" value="" />
<input type="hidden" name="ccexpyr" id ="ccexpyr" value="" />
<input class="form-control" type="hidden" name="store_card_token" id ="store_card_token" value="">
<hr />
<div>
<h1>Rs.<?php echo $amount; ?></h1>
<p align="center" class="note_text">Transaction ID : <?php echo $txnid; ?></p>
<hr />
<p align="center" ><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/visa.png" class="img_space"/><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/master.png" class="img_space" /><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/netbanking.png" class="img_space"/></p>

<!--PayU-->
<div class="container" style="display:none" id="payudetails">
    <div class="jumbotron" style="padding-bottom : 400px; padding-top: 10px">
        <div class="col-sm-10 col-md-offset-4">
            <div class="row">
                <div class="col-md-6 ">
                  <h4><p class="text-center">Discount : 2%</p></h4>
                  <h4><p class="text-center">Enter details to get discount</p></h4>
                  <span id="emailerror" style="display:none;color:red"><p align="center"><label>Please give valid email id</label></p></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <input class="form-control" type="text" id="emailid" name='emailid' value="<?php echo $email; ?>"/>
                    <input class="form-control" type="text" id="mobilenum" maxlength="15" value="<?php echo $phone; ?>"/>
                    <br/><button type="button" id="paybyPayuid" onclick="paybyPayu();" class="btn btn-primary btn-block" >Pay using PayUMoney</button>  
                    <p align="center" class="note_text">Or, cancel the <span class="uline"><a href="<?php  echo $curl; ?>" >transaction.</a></span></p>
                </div>
            </div>
        </div>
    </div>    
</div>
</div>

<!--Credit Card Details-->
    <div class="container" style="display:none" id="carddetails">
        <div class="jumbotron" style="padding-bottom : 400px; padding-top: 10px">
            <div class="col-sm-10 col-md-offset-4">
              <div class="row">
                <div class="col-md-6">
                   <label>Enter card details</label>
                   <span id="c_carddetailserror" style="display:none;color:red"><p align="center"><label>Please enter valid card details</label></p></span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                   <p align="center"style="display:none" id="CCCardType_p">
                   <select class="form-control" id="c_CardType" name="c_CardType" onchange="selectCard('credit')">
                    <option value="">Select Card Types</option>
                     <?php 
                        $nameOnCard = "";
                        $cardno = "";
                    if($UserCardDetails['status']==1){
                        $j=0;
                        foreach($UserCardDetails['user_cards'] as $row){
                            if($row['is_expired']==0 && $row['card_mode']=='CC'){
                                if($j==0){
                                    $nameOnCard = $row['name_on_card'];
                                    $cardno = $row['card_no'];
                                }
                           ?>
                          <option value="<?php echo $j; ?>" <?php if($j==0){ echo "selected"; } ?>><?php echo $row['name_on_card']; ?></option>
                           <?php
                           $j++;
                            }
                        }
                    }
                    ?>
                        <option value="CC">Visa/Master</option>
                        <option value="AMEX">AMEX cards</option>
                        <option value="DINR">Diners</option>
                    </select>
                    </p>
                </div>
              </div>
        <div class="row">
          <div class="col-md-6">
            <!--<div class="input-group">-->
            <input type="text" class="form-control" placeholder="Card Number" <?php if($j>0){ ?> disabled="disabled" <?php } ?> autocomplete="off" length='16' name="c_num" onblur="validate_card('c_')" value="<?php echo $cardno; ?>" id="c_num" />
             <input type="text" class="form-control" placeholder="Name on card" name="c_name" <?php if($j>0){ ?> disabled="disabled" <?php } ?> value="<?php echo $nameOnCard; ?>"  id="c_name" />
             <input type="text" class="form-control" name="c_expiry" placeholder="Expiry" value="Expiry" readonly> 
            <!--</div>-->
          </div>
        </div>
                
        <div class="row">    
                <?php $cdisplay="none"; if($j>0) $cdisplay="block"; ?>
            <div class="col-md-3">
                <span class="c_hideExpiry" style="font-size: 13px; width: 224px; float: left; font-weight: bold; line-height: 28px; padding: 5px 0px;display: <?php echo $cdisplay; ?>"><img src="https://test.payu.in/images/disable_select_year.jpg" style="clear: none; margin-left: 5px"> </span>
                <select class="form-control" id="c_expmon" name="c_expmon" <?php if($j>0){ ?>  style="display:none" <?php } ?> >
                    <option value="">MM</option>
                    <option value="01">Jan (1)</option>
                    <option value="02">Feb (2)</option>
                    <option value="03">Mar (3)</option>
                    <option value="04">Apr (4)</option>
                    <option value="05">May (5)</option>
                    <option value="06">Jun (6)</option>
                    <option value="07">Jul (7)</option>
                    <option value="08">Aug (8)</option>
                    <option value="09">Sep (9)</option>
                    <option value="10">Oct (10)</option>
                    <option value="11">Nov (11)</option>
                    <option value="12">Dec (12)</option>
                </select>
            </div>
            <div class="col-md-3">
                <span class="c_hideExpiry" style="font-size: 13px; width: 224px; float: left; font-weight: bold; line-height: 28px; padding: 5px 0px;display: <?php echo $cdisplay; ?>"><img src="https://test.payu.in/images/disable_select_year.jpg" style="clear: none; margin-left: 5px"> </span>
                <select class="form-control" id="c_expyr" name="c_expyr" <?php if($j>0){ ?>  style="display:none" <?php } ?> >
                    <option value="">YY</option>
                    <?php
                    $year = date('Y');
                    for($i=$year;$i< $year+30;$i++){
                        echo "<option value='".$i."'>$i</option>";
                    }
                     ?>
                </select>    
            </div>
            </div>
                
            <div class="row">
              <div class="col-md-6">
              <!--<div class="input-group">-->
              <input type="text" class="form-control" placeholder="CVV" value="" length='3' name="c_cvv" id="c_cvv" />  
              <!--</div>-->
              </div>
            </div>
        <div class="row" style="display:none" id="carddetails_submit" name="carddetails_submit">
            <div class="col-md-6">
             <span id='span_storecard' name='span_storecard'>
             <input type="checkbox" onclick="addlabel();" id="store_card" name="store_card" value="0"/>Save card for faster checkouts. <span class="color_purple">Learn More</span></label></p>
             </span>
             <p align="center" id="card_label_p" name="card_label_p" style="display:none"><label> Add Label <input class="form-control" type="text" id="card_name"/></label></p>
             <button type="button" class="btn btn-primary btn-block"  onclick="checkCard('<?php echo $key; ?>');">Pay Now</button> 
             <img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/loading.gif" style="display:none;width: 82px" id="c_loading" name="c_loading">
             <p align="center"><span class="note_text">Or, cancel the <span class="uline"><a href="<?php  echo $curl; ?>" >transaction.</a></span></span></p> 
            </div>
        </div>        
            </div>
        </div>
    </div>   

<!-- Debit Card-->
    <div class="container" style="display:none" id="d_carddetails">
        <div class="jumbotron" style="padding-bottom : 400px; padding-top: 10px">
            <div class="col-sm-10 col-md-offset-4">
              <div class="row">
                <div class="col-md-6">
                   <label>Enter card details</label>
                   <span id="d_carddetailserror" style="display:none;color:red"><p align="center"><label>Please enter valid card details</label></p></span>
                </div>
              </div>
            <div class="row">
                <div class="col-md-6">
                    <p align="center"style="display:none" id="debitCardType_p">
                  <select class="form-control" id="d_CardType" name="d_CardType" onclick="selectCard('debit')">
                    <option value="">Select Debit Card Types</option>
                     <?php 
                     $nameOnCard ="";
                     $cardno = "";
                    if($UserCardDetails['status']==1){$k=0;
                        foreach($UserCardDetails['user_cards'] as $row){
                            if($row['is_expired']==0 && $row['card_mode']=='DC'){
                                if($k==0){
                                    $nameOnCard = $row['name_on_card'];
                                    $cardno = $row['card_no'];
                                }
                           ?>
                           <option value="<?php echo $k; ?>" <?php if($k==0){ echo "selected"; } ?>><?php echo $row['name_on_card']; ?></option>
                           <?php
                           $k++;
                            }
                        }
                    }
                    ?>
                    <option value="VISA">Visa debit cards (all banks)</option>
                    <option value="MAST">MasterCard Debit Cards (All Banks)</option>
                    <option value="SMAE">SBI Maestro card</option>
                    <option value="MAES">Other Maestro card</option>
                    <option value="CITD">CITI Debit Cards</option>
                 </select>
                </p>        
                </div>
             </div>   
             <div class="row">
                <div class="col-md-6">
                    <!--<div class="input-group">-->
                    <input type="text" class="form-control" placeholder="Card Number" length='16' name="d_num" onblur="validate_card('d_')" value="<?php echo $cardno; ?>" id="d_num"/>
                    <input type="text" class="form-control" placeholder="Name on card" name="d_name" value="<?php echo $nameOnCard; ?>"  id="d_name" value=""/>
                    <input type="text" class="form-control" placeholder="Expiry" value="Expiry" readonly>
                    <!--</div>-->
                </div>
             </div>
                <div class="row">    
                  <div class="col-md-3">
                    <span class="d_hideExpiry" style="font-size: 13px; width: 224px; float: left; font-weight: bold; line-height: 28px; padding: 5px 0px;display: <?php echo $cdisplay; ?>"><img src="https://test.payu.in/images/disable_select_year.jpg" style="clear: none; margin-left: 5px"> </span>
                    <select id="d_expmon" name="d_expmon" <?php if($k>0){ ?>  style="display:none" <?php } ?> class="form-control">
                     <option value="">MM</option>
                     <option value="01">Jan (1)</option>
                     <option value="02">Feb (2)</option>
                     <option value="03">Mar (3)</option>
                     <option value="04">Apr (4)</option>
                     <option value="05">May (5)</option>
                     <option value="06">Jun (6)</option>
                     <option value="07">Jul (7)</option>
                     <option value="08">Aug (8)</option>
                     <option value="09">Sep (9)</option>
                     <option value="10">Oct (10)</option>
                     <option value="11">Nov (11)</option>
                     <option value="12">Dec (12)</option>
                     </select>
                  </div>
                  <div class="col-md-3">
                      <span class="d_hideExpiry" style="font-size: 13px; width: 224px; float: left; font-weight: bold; line-height: 28px; padding: 5px 0px;display: <?php echo $cdisplay; ?>"><img src="https://test.payu.in/images/disable_select_year.jpg" style="clear: none; margin-left: 5px"> </span>
                     <select id="d_expyr" name="d_expyr" class="form-control" <?php if($k>0){ ?>  style="display:none" <?php } ?>> 
                         <option value="">YY</option>
                         <?php
                         $year = date('Y');
                         for($i=$year;$i< $year+30;$i++){
                             echo "<option value='".$i."'>$i</option>";
                         }

                         ?>
                     </select>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="CVV" value="" length='3' name="d_cvv" id="d_cvv" />  
                  </div>
                </div>

                <!--<div style="display:none" id="carddetails_submit" name="carddetails_submit"></div>-->   
             <div class="row">
                <div class="col-md-6">
                <input type="checkbox" onclick="d_addlabel();" id="d_store_card" name="d_store_card" value="0"/>Save card for faster checkouts. <span class="color_purple">Learn More</span>
                <p align="center" id="d_card_label_p" name="d_card_label_p" style="display:none"><label> Add Label <input class="form-control" type="text" id="d_card_name"/></label></p>
                <button type="button" class="btn btn-primary btn-block" onclick="checkCard('<?php echo $key; ?>');">Pay Now</button>
                <img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/loading.gif" style="display:none;width: 82px" id="d_loading" name="d_loading">
                <p align="center"><span class="note_text">Or, cancel the <span class="uline"><a href="<?php  echo $curl; ?>" >transaction.</a></span></span></p>
                </div>
             </div>
            </div>
        </div>
    </div>

<!--Net Banking-->
<div style="display:none" id="netbankdetails">
<p align="center" class="note_text">Transaction ID : 100023181</p>

<div class="container">
    <div class="jumbotron" style="padding-bottom : 400px; padding-top: 10px">
      <div class="col-sm-10 col-md-offset-4">
         <div class="row">
            <div class="col-md-6">
              <h2 class="discount_title"><span class="color_grey">Pay by </span>Netbanking</h2>       
            </div>
         </div>   
         <div class="row">
           <div class="col-md-6">
             <span id="netbanking_error" style="display:none;color:red"><p align="center"><label>Please select bank</label></p></span>
        <select  class="form-control" onchange="select_netbanking(this);" name="netbanking_select" id="netbanking_select" class="required">
            <option gateway_status="1" selected="selected" value="">Select Your Bank</option>
            <option value="ALLB" gateway_status="1">Allahabad Bank NetBanking</option>
            <option value="AXIB" gateway_status="1">AXIS Bank NetBanking</option>
            <option value="BOIB" gateway_status="1">Bank of India</option>
            <option value="BOMB" gateway_status="1">Bank of Maharashtra</option>
            <option value="CABB" gateway_status="1">Canara Bank</option>
            <optgroup style="font-weight: bold; text-align: left; font-style: normal;" label="---------------------"></optgroup>
            <option value="AXIB" gateway_status="1">AXIS Bank NetBanking</option>
            <option value="ALLB" gateway_status="1">Allahabad Bank NetBanking</option>
            <option value="BOIB" gateway_status="1">Bank of India</option>
            <option value="BOMB" gateway_status="1">Bank of Maharashtra</option>
            <option value="CABB" gateway_status="1">Canara Bank</option>
            <option value="CSBN" gateway_status="1">Catholic Syrian Bank</option>
            <option value="CBIB" gateway_status="1">Central Bank Of India</option>
            <option value="CUBB" gateway_status="0" style="color: grey;">City Union Bank</option>
            <option value="CRPB" gateway_status="1">Corporation Bank</option>
            <option value="DCBCORP" gateway_status="1">DCB Corporate Netbanking </option>
            <option value="DSHB" gateway_status="1">Deutsche Bank</option>
            <option value="DCBB" gateway_status="1">Development Credit Bank</option>
            <option value="DLSB" gateway_status="1">Dhanlaxmi Bank</option>
            <option value="FEDB" gateway_status="1">Federal Bank</option>
            <option value="HDFB" gateway_status="1">HDFC Bank</option>
            <option value="ICIB" gateway_status="1">ICICI Netbanking</option>
            <option value="INGB" gateway_status="1">ING Vysya Bank</option>
            <option value="INDB" gateway_status="1">Indian Bank </option>
            <option value="INOB" gateway_status="1">Indian Overseas Bank</option>   
            <option value="INIB" gateway_status="1">IndusInd Bank</option>
            <option value="IDBB" gateway_status="1">Industrial Development Bank of India</option>
            <option value="JAKB" gateway_status="1">Jammu and Kashmir Bank</option>
            <option value="KRKB" gateway_status="1">Karnataka Bank</option>
            <option value="KRVB" gateway_status="1">Karur Vysya </option>
            <option value="162B" gateway_status="1">Kotak Mahindra Bank</option>
            <option value="PNBCORP" gateway_status="0" style="color: grey;">Punajb National Bank(Corp)</option>
            <option value="PNBB" gateway_status="1">Punjab National Bank - Retail Banking</option>
            <option value="SRSWT" gateway_status="0" style="color: grey;">Saraswat Bank</option>
            <option value="SOIB" gateway_status="1">South Indian Bank</option>
            <option value="SBBJB" gateway_status="0" style="color: grey;">State Bank of Bikaner and Jaipur</option>
            <option value="SBHB" gateway_status="0" style="color: grey;">State Bank of Hyderabad</option>
            <option value="SBIB" gateway_status="0" style="color: grey;">State Bank of India</option>
            <option value="SBMB" gateway_status="1">State Bank of Mysore</option>
            <option value="SBPB" gateway_status="1">State Bank of Patiala</option>
            <option value="SBTB" gateway_status="1">State Bank of Travancore</option>
            <option value="UBIB" gateway_status="1">Union Bank of India</option>
            <option value="UNIB" gateway_status="1">United Bank Of India</option>
            <option value="VJYB" gateway_status="1">Vijaya Bank</option>
            <option value="YESB" gateway_status="1">Yes Bank</option>									
        </select>   
            </div>
         </div>
        <div class="row">
          <div class="col-md-6">
            <span class="note_text"><span class="color_purple">Note :</span> In the next step you will be redirected to your bank
                    website to verify your account.</span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
           <button type="button" id="payNetbank" class="btn btn-primary btn-block" onclick="paybyNetBank();">Pay Now</button>  
              <p align="center"><span class="note_text">Or, cancel the <span class="uline"><a href="<?php  echo $curl; ?>" >transaction.</a></span></span></p>      
          </div>
        </div>     
      </div>
    </div>    
</div>
</div>
 <div><p align="center"><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/verified_by_visa-01.png" class="img_space"/><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/mastercard-securecode.png" class="img_space" /><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/pci.png" /><img src="<?php echo Yii::app()->params['GLOBALS']['LIVE_BASE_URL']; ?>images/verisign.png" /></p></div>
</form>
</body>
</html>