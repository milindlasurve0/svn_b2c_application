<div style="border: 1px solid;	border-radius: 25px;">
    <h4 align="center">Genuine & Non-Genuine Users Report</h4>
    <h6 align="center">(Last 2 months Report)</h6>
</div>

<form  method="post">
<div style="float:right ; color:blue">
    <select id="user_type" name="user_type"  onchange = "this.form.submit()" >
        <option value="1" <?php if($user_type==1) echo "selected"; ?>>All Users</option>
        <option value="2" <?php if($user_type==2) echo "selected"; ?>>Genuine Users</option>
        <option value="3" <?php if($user_type==3) echo "selected"; ?>>Non-Genuine Users</option>
    </select>
</div>
</form>

<hr/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		
		<title>DataTables Bootstrap 3 example</title>

		<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="dataTables.bootstrap.css">

		<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10-dev/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#User_Table').dataTable();
                        });
		</script>
    </head>
    <body>
    <div class="container">
    <table class="table table-striped table-bordered" id="User_Table" style="width: 1001px;">
       <thead>
       <th style="width: 130px; height: 40px;">User Mobile</th><th>Register date</th><th>Frequency</th><th>Last login</th><th>Recharge Amount</th><th>Gift Grep</th><th>Gift Redeem</th>
       </thead>
    <tbody>
     <?php if (!empty($Auser)){ ?>
       <?php foreach($Auser as $key => $user){ ?>    
        <tr>
        <td><a target="_blank" href="search?search_submit=Search&wallet_user_name=<?php echo $user['mobile'] ;?>"><?php echo $user['mobile'] ;?></a></td>
        <td><?php echo $user['created'] ;?></td>
        <td><?php echo $user['frequency'] ;?></td> 
        <td><?php echo $user['updated'] ;?></td> 
        <td><?php echo $user['recharge_amt'] ;?></td>
        <td><?php echo $user['total_gifts'] ;?></td>
        <td><?php echo $user['total_redeem'] ;?></td>
        </tr>
      <?php } ?>  
    <?php }else{ ?>
        <tr><td>Empty.</td></tr>
    <?php } ?>
    </tbody>
    </table>
    </div>
    </body>
    </html>    
    <hr/>

