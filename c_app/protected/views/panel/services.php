<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#mobile" data-toggle="tab">Mobile</a></li>
    <li><a href="#dth" data-toggle="tab">DTH</a></li>
    <li><a href="#data" data-toggle="tab">Data Card</a></li>
</ul>

<h2>Delegate Recharge</h2>

<script type="text/javascript">

    $(function() {
        $('#mobiletxt,#amounttxt').keydown(function(e) {
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 16) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                    e.preventDefault();
                }
            }
        });

        $('#mobile #mobiletxt').keydown(function() {
            if ($('#mobile #mobiletxt').val().length == 10) {
                $.ajax({url: "<?php echo Yii::app()->baseUrl . '/index.php/api/findoperator.do?mobile='; ?>" + $('#mobile #mobiletxt').val(), success: function(result) {
                        var res = JSON.parse(result);
                        $('#mobile #operator option[value="' + res['operator'] + '"]').attr("selected", "selected");
                        $('#mobile #circle').val(res['circle'])
                        //console.log(res['operator']+" : "+res['circle']);
                    }})
            }
            ;
        });

        $('#data #mobiletxt').keydown(function() {
            if ($('#data #mobiletxt').val().length == 10) {
                $.ajax({url: "<?php echo Yii::app()->baseUrl . '/index.php/api/findoperator.do?mobile='; ?>" + $('#data #mobiletxt').val(), success: function(result) {
                        var res = JSON.parse(result);
                        $('#data #circle').val(res['circle'])
                        //console.log(res['operator']+" : "+res['circle']);
                    }})
            }
            ;
        });
    });


</script>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="mobile">
        <div><br/>
            <form name = "mobile_recharge" method = "POST" action = "<?php echo Yii::app()->baseUrl . '/index.php/transactions/service'; ?>">
                <div class="control-group">
                    <label for="mobile" class="control-label" >Mobile Number</label>
                    <input type="text" id="mobiletxt" name="mobile_number"  maxlength="10" class="form-control" placeholder="Enter Mobile Number"/>
                </div>
                <br/>
                <div class="control-group">
                    <div class="controls">
                        <label for="operator" class="control-label">Operator</label>                        
                        <select name="operator" class="form-control" id="operator">
                            <option value="0">Select Your Operator</option>
                            <?php
                            foreach (TelecomComponent::getOperator() as $id => $name) {
                                echo "<option value='$id'>$name</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" name="circle" id="circle" />
                        <input type="hidden" name="flag" value="1" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">
                        <label for="stv" >Special Recharge</label>
                        <input type="checkbox" name="stv" />
                    </div><br/>
                    <div class="controls">
                        <label for="amount" class="control-label" >Amount</label>
                        <input type="text" name="amount" id="amounttxt" class="form-control" maxlength="4" placeholder="amount" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">
                        <input type="hidden" name="circle" id="circle" />
                        <input type="hidden" name="flag" value="1" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">
                        <button name="recharge" class="btn btn-success" >Continue</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- ------------------------------ -->

    <div class="tab-pane" id="dth">
        <div><br/>
            <form name = "data_recharge" method = "POST" action = "<?php echo Yii::app()->baseUrl . '/index.php/transactions/service'; ?>">
                <div class="control-group">
                    <label for="mobile" class="control-label" >DTH Subscriber Number</label>
                    <input type="text" id="subscriber_id" name="subscriber_id" class="form-control" placeholder="Enter Subscriber ID"/>
                </div>
                <br/>
                <div class="control-group">
                    <div class="controls">
                        <label for="operator" class="control-label">Operator</label>                        
                        <select name="operator" class="form-control" id="operator">
                            <option value="0">Select Your Operator</option>
                            <?php
                            foreach (TelecomComponent::getdthOperator() as $id => $name) {
                                echo "<option value='$id'>$name</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" name="flag" value="2" />
                    </div>
                </div><br/>
                <div class="control-group">        
                    <div class="controls">
                        <label for="amount" class="control-label" >Amount</label>
                        <input type="text" name="amount" id="amounttxt" class="form-control" maxlength="4" placeholder="amount" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">
                        <input type="hidden" name="circle" id="circle" />
                        <input type="hidden" name="flag" value="1" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">
                        <button name="recharge" class="btn btn-success" >Continue</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- ------------------------------ -->

    <div class="tab-pane" id="data">     
        <div><br/>
            <form name = "data_recharge" method = "POST" action = "<?php echo Yii::app()->baseUrl . '/index.php/transactions/service'; ?>">
                <div class="control-group">
                    <label for="mobile" class="control-label" >Data Card Number</label>
                    <input type="text" id="mobiletxt" name="mobile_number" maxlength="10" class="form-control" placeholder="Enter Data Card Number"/>
                </div>
                <br/>
                <div class="control-group">
                    <div class="controls">
                        <label for="operator" class="control-label">Operator</label>                        
                        <select name="operator" class="form-control" id="operator">
                            <option value="0">Select Your Operator</option>
                            <?php
                            foreach (TelecomComponent::getdataOperator() as $id => $name) {
                                echo "<option value='$id'>$name</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" name="circle" id="circle" />
                        <input type="hidden" name="flag" value="3" />
                    </div>
                </div><br/>
                <div class="control-group">        
                    <div class="controls">
                        <label for="amount" class="control-label" >Amount</label>
                        <input type="text" name="amount" id="amounttxt" class="form-control" maxlength="4" placeholder="amount" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">                        
                        <input type="hidden" name="circle" id="circle" />
                        <input type="hidden" name="flag" value="1" />
                    </div>
                </div><br/>
                <div class="control-group">
                    <div class="controls">
                        <button name="recharge" class="btn btn-success" >Continue</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
