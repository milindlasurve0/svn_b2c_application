<?php
/* @var $this TransactionsController */
/* @var $model Transactions */
?>
<!--
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#refill">Wallet Refill</button>
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#transfer">Money Transfer</button>
-->
<script type="text/javascript">    
    $(function() {
        $('#amounttxt').keydown(function(e) {
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 16) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                    e.preventDefault();
                }
            }
        });
        
        $('#ispromocode').change(function(e){
           if(this.checked) {
               $("#promo_coupon").show();
               $("#amount").hide();
            }else{
                $("#promo_coupon").hide();
                $("#amount").show();
            }
        });
        
    });
</script>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#wallet_refill" data-toggle="tab">Wallet Refill</a></li>
    <li><a href="#wallet_transfer" data-toggle="tab">Wallet Transfer</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="wallet_refill">
        <br/><form>
            <div class="control-group">
                    <label for="ispromocode" class="control-label" >Promocode</label>
                    <input type="checkbox" id="ispromocode" name="ispromocode">
                    <div class="hint">( * Check if u have promo code.)</div>
            </div>
            <div class="control-group" id="promo_coupon" style="display: none" ><br/>
                    <label for="promo_coupon" class="control-label" >Promo code</label>
                    <input type="text" id="promo_coupon_txt" name="promo_coupon" class="form-control" placeholder="Promo Coupon Code"/>
            </div>
            <div class="control-group" id="amount" ><br/>
                    <label for="amount" class="control-label" >Amount</label>
                    <input type="text" id="amounttxt" name="amount" class="form-control" maxlength="4" placeholder="Refill Amount"/>
            </div><br/>
            <div class="control-group">
                    <div class="controls">
                        <button name="refill" class="btn btn-success" >NEXT</button>
                    </div>
            </div>
        </form>          
    </div>
    
    <div class="tab-pane" id="wallet_transfer">
        <br/><form method = "POST" action = "<?php echo Yii::app()->baseUrl . '/index.php/transactions/payment'; ?>">
            <div class="control-group">
                    <label for="mobile" class="control-label" >Mobile Number</label>
                    <input type="text" id="mobiletxt" name="mobile_number" maxlength="10" class="form-control" placeholder="Pay1 Registered Mobile Number"/>
            </div>
            <div class="control-group" id="amount" ><br/>
                    <label for="amount" class="control-label" >Amount</label>
                    <input type="text" id="amounttxt" name="amount" class="form-control" maxlength="4" placeholder="Refill Amount"/>
            </div><br/>
            <div class="control-group">
                    <div class="controls">
                        <button name="refill" class="btn btn-success" >Transfer</button>
                    </div>
            </div>
        </form>
    </div>
</div>