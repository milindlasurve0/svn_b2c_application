<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form action="" method="post"  class="form-inline" style="padding-top: 50px" role="form">

    <input class="form-control" type="text" name="trans_id" id="trans_id" placeholder="Enter transaction id" />
    
    <button  id="failedTransaction" class="btn btn-success" name="failedTransaction" value="submit">Submit</button>
</form><hr>

    <?php if(Yii::app()->user->hasFlash('Transaction')): ?>
 
<div class="flash-success">
    <?php echo Yii::app()->user->getFlash('Transaction'); ?>
</div>
 <?php
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$(".flash-success").animate({opacity: 1.0}, 3000).fadeOut("slow");',
   CClientScript::POS_READY
);
?>
<?php endif; ?>