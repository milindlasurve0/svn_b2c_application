<div style="border: 1px solid;	border-radius: 25px;">
    <h4 align="center">Miss Calls Users Report</h4>
</div>


<div style="border: 1px solid;	border-radius: 25px;">
    <form action="" method="POST" id="searchForm" name="searchForm" role="form" class="form-search" style="text-align: center" onsubmit="return validate()">
    <fieldset>
    <legend>Search</legend>
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'start_date',    
                'value'=> isset($start_date)?$start_date:"",
                'options'=>array(        
                    'showButtonPanel'=>true,
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'style'=>'',
                    'placeholder'=>'Start Date'
                ),
            ));?>
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'end_date',    
                'value'=>isset($end_date)?$end_date:"",
                'options'=>array(        
                    'showButtonPanel'=>true,
                    'dateFormat'=>'yy-mm-dd',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                ),
                'htmlOptions'=>array(
                    'style'=>'',
                    'placeholder'=>'End Date'
                ),
            ));
        
            ?>
      
      <input type="submit" name="search_submit" id="search_submit" value="Search" class="btn">
    </fieldset>
    </form><br/>
    </div>
    <hr/>
    
    <?php 
    $a = 0 ;
    $b = 0 ;
    for($i = 0 ; $i < count($TotalAppInstall) ; $i++){
       $App_Mrk[$a++]   =  (int)$TotalAppInstall[$i]['App_mrk'];
       $App_Store[$b++] =  (int)$TotalAppInstall[$i]['App_Store'];
    }
    $a = 0 ;
    $b = 0 ;
    for($i = 0 ; $i < count($TotalMissedCall) ; $i++){
       $via_Poster[$a++] =  (int)$TotalMissedCall[$i]['via_Poster'];
       $via_Sms[$b++]    =  (int)$TotalMissedCall[$i]['via_Sms'];
    }
    
    if(!isset($via_Sms) || !isset($via_Poster)){
       $via_Poster[] = array(0,0,0,0,0,0,0);
       $via_Sms[]    = array(0,0,0,0,0,0,0);   
    }
    if(!isset($App_Mrk) || !isset($App_Store)){
       $App_Mrk[] = array(0,0,0,0,0,0,0);
       $App_Store[]    = array(0,0,0,0,0,0,0);   
    }
    
    $a = 0;
    for($i = 6 ; $i >= 0 ; $i--){
        $days[$a++] =  date('Y-m-d', strtotime($start_date .' -'.$i.'day')).'<br>';
    }
     $xAxis = $days;
     $this->Widget('ext.yii-highcharts.highcharts.HighchartsWidget', array(
      'options'=>array(
      'title' => array('text' => 'Missed Call Users'),
      'subtitle' => array('text' => 'Last 7 days Usage'),
      'xAxis' => array(
          'title' => array('text' => 'Date'),
          'categories' => $xAxis
      ),
      'yAxis' => array(
         'title' => array('text' => 'Number')
      ),
      'series' => array(
         array('name' => 'Total Marketing Msg Usage', 'data' => $TotalMsgUsage), 
         array('name' => 'Miss Call via_sms', 'data' => $via_Sms),
         array('name' => 'Miss Call via_poster', 'data' => $via_Poster),
         array('name' => 'App Install via_marketing', 'data' => $App_Mrk),
         array('name' => 'App Install via_playstore', 'data' => $App_Store) 
      )
     )
    ));
    ?>
    <hr/>
    <li><b>Total Miss Call Users      ( via SMS 02267242255 )</b>
        <ul>
            <li>Old Users : <font color="blue"><?php  echo $SmsMissCallusers[0]['sms_old_user']; ?></font></li>  
            <li>New Users     : <font color="blue"><?php  echo $SmsMissCallusers[0]['sms_new_user']; ?></font></li>
            
        </ul>    
    </li>        
    <hr/>
    <li><b>Total Miss Call Users   ( via Poster 02267242211 )</b>
        <ul>
            <li>Old Users  : <font color="blue"><?php echo $PosterMissCallusers[0]['poster_old_user']; ?></font></li>
            <li>New Users    : <font color="blue"><?php echo $PosterMissCallusers[0]['poster_new_user']; ?></font></li>
            
        </ul>    
    </li>
    <hr/>
    <li><b>Total App Installs</b> 
        <ul> 
            <li>Via Sms  : <font color="blue"><?php echo $SignupUsers[0]['sms_signup_user']; ?></font></li>
            <li>Via Poster : <font color="blue"><?php  echo $SignupUsers[0]['poster_signup_user']; ?></font></li>
            <li>Via Play Store  : <font color="blue"><?php  echo $SignupUsers[0]['other_signup_user']; ?></font></li>
        </ul>
    </li>
    <hr/>
    <li><b>Total Loyalty Points Given Users After Miss Call</b>
        <ul> 
            <li>to New : <font color="blue"><?php echo $LoyaltyPointUsers[0]['via_miss_call']; ?>  </font>   
                (App Installed Users :  <font color="blue"><?php echo $LoyaltyPointAppUsers[0]['appUser_via_miss_call']; ?></font>)</li>
            <li>to Old : <font color="blue"><?php echo $LoyaltyPointUsers[0]['via_recharge']; ?>   </font>    
                (App Installed Users : <font color="blue"><?php echo $LoyaltyPointAppUsers[0]['appUser_via_recharge']; ?></font>)</li>
        </ul>
    </li>
    <hr/>
    <li><b>Gifts</b>
        <ul>
            <li>Gift Grep       :  <font color="blue"><?php if(empty($total_gift_grep)){echo 0;}else{echo $total_gift_grep; } ?></font></li>
            <li>Gift Redeem     :  <font color="blue"><?php if(empty($total_gift_redeem)){echo 0;}else{echo $total_gift_redeem; } ?></font></li>
        
    </ul></li>
    <hr/>

<script>

function validate(){
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    if($.trim(start_date).length < 1){
        alert("Please select start date");
        return false;
    }
    
    if($.trim(end_date).length < 1){
        alert("Please select end date");
        return false;
    }
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var firstDate  = new Date(start_date);
    var secondDate = new Date(end_date);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
    
    if(diffDays > 7){
        $('#start_date').val("");
        $('#end_date').val("");
        alert("Sorry !! U can't select date range more than 7 days");
        return false;
    }
    return true;
}

</script>
