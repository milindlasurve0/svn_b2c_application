<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--
<div class="row">  
    <div >
        <a href="<?php echo Yii::app()->baseUrl.'/index.php/recharges/mobile';?>" class="btn btn-primary" role="button">Button</a>
    </div>
</div>
-->

<!-- Button trigger modal -->
<!--
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mobile_recharge">MOBILE</button>
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#dth_recharge">DTH</button>
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#sch_recharge">SCHEDULE</button>
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#delegate_recharge">DELEGATE</button>
-->
<!-- Mobile Recharge -->
<div class="modal fade" id="mobile_recharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Mobile Recharge</h4>
            </div>
            <form name="mobile_recharge" method="POST" action="<?php echo Yii::app()->baseUrl.'/index.php/transactions';?>">
            <div class="modal-body">                
                    <div class="control-group">
                        <label for="mobile" class="control-label" >Mobile Number</label>
                        <input type="text" name="mobile" class="form-control" placeholder="Mobile Number"/>
                    </div><br/>
                    <div class="control-group">
                        <div class="controls">
                        <label for="operator" class="control-label">Operator</label>                        
                        <select name="operator" class="span5">
                            <?php foreach(TelecomComponent::getOperator() as $id=>$name){ 
                                echo "<option value='$id'>$name</option>";
                             } ?>
                        </select>
                        </div>
                    </div><br/>
                    <div class="control-group">
                        <div class="controls">
                        <label for="amount" class="control-label" >Amount</label>
                        <input type="text" name="amount" class="form-control" placeholder="amount"/>
                        </div>
                    </div><br/>
                    <div class="control-group">
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="payment" id="optionsRadios1" value="wallet" checked=""> Pay by Wallet</label>
                            <label class="radio">
                                <input type="radio" name="payment" id="optionsRadios1" value="online" > Pay Online
                            </label>
                            <button name="recharge" class="btn" onclick="javascript:alert('rr')" >Continue</button>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>                
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
                </form>
        </div>
    </div>
</div>

<!-- DTH Recharge -->
<div class="modal fade" id="dth_recharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#delegate_recharge" data-toggle="tab">Delegate Recharge</a></li>
    <li><a href="#schedule_recharge" data-toggle="tab">Schedule Recharge</a></li>
</ul>



