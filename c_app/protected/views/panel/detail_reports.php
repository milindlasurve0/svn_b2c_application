<?php 
      /*  if(isset($message) && !empty($message)){
            echo $message;
        }*/

$this->widget('ext.htmltableui.htmlTableUi',array(
    'ajaxUrl'=>'site/handleHtmlTable',
    'arProvider'=>'',    
    'collapsed'=>false,
    'columns'=>$data_col,
    'cssFile'=>'',
    'editable'=>false,
    'enableSort'=>true,
    'exportUrl'=>'site/exportTable',
    'extra'=>'',
    'footer'=>'Total rows: '.($data_count).' ',
    'formTitle'=>'Form Title',
    'rows'=>$detail_data,
    'sortColumn'=>1,
    'sortOrder'=>'desc',
    'subtitle'=>'',
    'title'=>$data_title,
));
?>
