
<h1>payment</h1>
<script type="text/javascript">
    $(document).ready(function() {
        $("input:radio").click(function() {
            $("#balance-msg").alert('close')
            //alert('ok');
        });

        //$("input:radio:first").prop("checked", true).trigger("click");

    });
</script>

<div>
    <?php // print_r($form_data);?>
</div>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Your current Wallet balance is : Rs. <?php  echo $wallet['account_balance']; ?></strong>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span-3"><?php echo $form_data['service']['label'];?></div><div class="span-1">:</div>
        <div class="span-5-last"><?php echo $form_data['service']['value']; ?></div> 
    </div>
    
    
    <?php if(isset($form_data['billpayment'])){?>
    <div class="row-fluid">
        <div class="span-3">Amount</div><div class="span-1">:</div><div class="span-5-last"><?php echo $form_data['base_amount']; ?></div>
    </div>
    <div class="row-fluid">
        <div class="span-3">Service Charges</div><div class="span-1">:</div>
        <div class="span-5-last"><?php echo $form_data['service_charge']; ?></div>
    </div>
    <div class="row-fluid">
        <div class="span-3">Service Tax</div><div class="span-1">:</div>
        <div class="span-5-last"><?php echo $form_data['service_tax']; ?></div>
    </div>
    <?php }; ?>
    <div class="row-fluid" style="font-weight: bolder">
        <div class="span-3">Total Amount</div><div class="span-1">:</div>
        <div class="span-5-last"><?php echo $form_data['amount']; ?></div>
    </div>
    
    <?php if(isset($form_data['stv'])){ ?>
    <div class="row-fluid">
        <div class="span-3">Special Recharge</div><div class="span-1">:</div><div class="span-5-last">Yes</div>
    </div>    
    <?php };?>
</div>
<form method="POST" action="process_payment">
<div class="control-group">
    <?php foreach ($form_data as $name=>$value){        
        echo "<input type='hidden' name='$name' value='$value'>";
    } ?>
    <div class="controls">
        <label class="radio">
            <input type="radio" name="paymentopt" id="optionsRadios1" value="wallet" checked="checked"> Pay by Wallet
        </label>
        <label class="radio">
            <input type="radio" name="paymentopt" id="optionsRadios1" value="online" > Pay Online
        </label>
        <!--<input type="hidden" name="paymentopt" id="optionsRadios1" value="wallet" checked="checked">-->
        <br/>
        <button name="payment" class="btn btn-success" >Make Payment</button>
    </div>
</div>
</form>