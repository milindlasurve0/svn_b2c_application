<?php

/**
 * This is the model class for table "offer_info".
 *
 * The followings are the available columns in table 'offer_info':
 * @property string $id
 * @property string $offer_id
 * @property string $deal_id
 * @property string $title
 * @property string $content_txt
 * @property integer $sequence
 * @property integer $status
 */
class OfferInfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offer_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, offer_id, deal_id, title, content_txt, sequence, status', 'required'),
			array('sequence, status', 'numerical', 'integerOnly'=>true),
			array('id, offer_id, deal_id', 'length', 'max'=>20),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, offer_id, deal_id, title, content_txt, sequence, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'offer_id' => 'Offer',
			'deal_id' => 'Deal',
			'title' => 'Title',
			'content_txt' => 'Content Txt',
			'sequence' => 'Sequence',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('offer_id',$this->offer_id,true);
		$criteria->compare('deal_id',$this->deal_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content_txt',$this->content_txt,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OfferInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
