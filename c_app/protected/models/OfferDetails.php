<?php

/**
 * This is the model class for table "offer_details".
 *
 * The followings are the available columns in table 'offer_details':
 * @property integer $id
 * @property integer $deal_id 
 * @property string $time
 * @property string $day
 * @property string $appointment
 * @property string $exclusive_gender
 * @property string $dealer_contact
 */
class OfferDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offer_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deal_id ', 'length', 'max'=>20),
			array('time , day , appointment , exclusive_gender ', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, deal_id , time , day , appointment, exclusive_gender, dealer_contact', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                        'deal_id' => 'Deal ID', 
			'time' => 'Time',
			'day' => 'Day',
			'appointment' => 'Appointment',
			'exclusive_gender' => 'Exclusive Gender',
			'dealer_contact' => 'Dealer Contact',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
                $criteria->compare('deal_id',$this->deal_id,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('appointment',$this->appointment,true);
		$criteria->compare('exclusive_gender',$this->exclusive_gender,true);
		$criteria->compare('dealer_contact',$this->dealer_contact);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OfferDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
