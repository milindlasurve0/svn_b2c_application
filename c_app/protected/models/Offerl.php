<?php

/**
 * This is the model class for table "deals".
 *
 * The followings are the available columns in table 'deals':
 * @property integer $id
 * @property string $name
 * @property string $category_id
 * @property string $dealer_id
 * @property string $status
 * @property string $created
 */
class Offer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
        //id 	deal_id 	name 	offer_price 	actual_price 	discount 	total_stock 	stock_sold 	validity 	validity_mode 	status 	short_desc 	long_desc 
		return array(
			array('deal_id, name, offer_price, actual_price,discount,total_stock,stock_sold,validity,validity_mode,status,short_desc,long_desc', 'required'),
			array('deal_id, offer_price, actual_price , total_stock , stock_sold , status', 'numerical'),
			array('name', 'length', 'max'=>255),
            array('category_id', 'length', 'max'=>5),
            array('dealer_id', 'length', 'max'=>20),
            array('status', 'length', 'max'=>4),
			array('created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, category_id, dealer_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
            //@TODO add relationship with Category , Dealer
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            
			'id' => 'ID',
			'name' => 'Name',
			'category_id' => 'Category',
			'dealer_id' => 'Dealer',
			'gender' => 'Gender',
			'status' => 'Status',
			'created' => 'Created'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        //name, category_id, dealer_id, status
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('dealer_id',$this->dealer_id,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
