<?php

/**
 * This is the model class for table "offers".
 *
 * The followings are the min_amountavailable columns in table 'offers':
 * @property string $id
 * @property string $image_url
 * @property string $image_flag
 * @property integer $image_sequence
 * @property integer $gift_type
 * @property integer $category_type
 * @property integer $big_text
 * @property integer $small_text
 * @property string $total_stock
 * @property string $img_link  
 * @property string $created
 */
class Web_Section extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'web_section';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            //id  image_url  image_flag image_sequence  $gift_type $category_type big_text  small_text  img_link   created
			array('image_flag, image_sequence, created', 'required'),
			array('image_url, big_text, small_text, img_link', 'length', 'max'=>255),
			array('id, image_url, image_flag, big_text, small_text, img_link, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        // 	short_desc 	long_desc 	created 	update
		return array(
			'id' => 'ID',
			'image_url' => 'Image url',
			'image_flag' => 'Image flag',
                        'image_sequence' => 'Image Sequence',
                        'gift_type' => 'Gift type',
                        'category_type' => 'Category type',
                    	'big_text' => 'Big Text',
			'small_text' => 'Small Text',
			'img_link' => 'Image Link',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('image_url',$this->image_url,true);
		$criteria->compare('image_flag',$this->image_flag,true);
                $criteria->compare('image_sequence',$this->image_sequence,true);
                $criteria->compare('gift_type',$this->gift_type,true);
                $criteria->compare('category_type',$this->category_type,true);
            	$criteria->compare('big_text',$this->big_text);
		$criteria->compare('small_text',$this->small_text);
		$criteria->compare('img_link',$this->img_link);
		$criteria->compare('created',$this->created,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
