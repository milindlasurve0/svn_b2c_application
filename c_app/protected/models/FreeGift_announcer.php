<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FreeGift_announcer extends CActiveRecord
{
    //public $image;
    // ... other attributes
    public function tableName()
    {
            return 'FreeGift_announcer';
    }
 
    public function rules()
    {
        return array(
           array('image ,Details','required'),
            array('image', 'file', 'types'=>'jpg, gif, png'),
            array('Details', 'length','min'=>3, 'max'=>12),
            array('id, image, details, users_id, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
        );
    }
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
            );
    }
        
    public function search()
    {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id,true);
            $criteria->compare('image',$this->image,true);
            $criteria->compare('details',$this->details,true);
            $criteria->compare('users_id',$this->users_id,true);
            $criteria->compare('created_datetime',$this->created_datetime,true);
            $criteria->compare('updated_datetime',$this->updated_datetime);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}