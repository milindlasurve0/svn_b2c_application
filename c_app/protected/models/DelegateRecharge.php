<?php

/**
 * This is the model class for table "delegate_recharge".
 *
 * The followings are the available columns in table 'delegate_recharge':
 * @property string $id
 * @property string $name
 * @property integer $operator_id
 * @property string $mobile_number
 * @property string $subscriber_id
 * @property double $recharge_amount
 * @property integer $stv
 * @property integer $status
 * @property string $users_id
 * @property integer $global_missed_numbers_id
 * @property integer $recharge_flag
 * @property integer $confirmation
 * @property string $created
 *
 * The followings are the available model relations:
 * @property GlobalMissedNumbers $globalMissedNumbers
 * @property Users $users
 */
class DelegateRecharge extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'delegate_recharge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, operator_id, subscriber_id, stv, users_id, global_missed_numbers_id, recharge_flag, created', 'safe'),
			array('operator_id, stv, status, global_missed_numbers_id, recharge_flag, confirmation', 'numerical', 'integerOnly'=>true),
			array('recharge_amount', 'numerical'),
			array('name, subscriber_id', 'length', 'max'=>50),
			array('mobile_number, users_id', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, operator_id, mobile_number, subscriber_id, recharge_amount, stv, status, users_id, global_missed_numbers_id, recharge_flag, confirmation, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'globalMissedNumbers' => array(self::BELONGS_TO, 'GlobalMissedNumbers', 'global_missed_numbers_id'),
			'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'operator_id' => 'Operator',
			'mobile_number' => 'Mobile Number',
			'subscriber_id' => 'Subscriber',
			'recharge_amount' => 'Recharge Amount',
			'stv' => 'Stv',
			'status' => 'Status',
			'users_id' => 'Users',
			'global_missed_numbers_id' => 'Global Missed Numbers',
			'recharge_flag' => 'Recharge Flag',
			'confirmation' => 'Confirmation',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('operator_id',$this->operator_id);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('subscriber_id',$this->subscriber_id,true);
		$criteria->compare('recharge_amount',$this->recharge_amount);
		$criteria->compare('stv',$this->stv);
		$criteria->compare('status',$this->status);
		$criteria->compare('users_id',$this->users_id,true);
		$criteria->compare('global_missed_numbers_id',$this->global_missed_numbers_id);
		$criteria->compare('recharge_flag',$this->recharge_flag);
		$criteria->compare('confirmation',$this->confirmation);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DelegateRecharge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
