<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property string $user_name
 * @property string $mobile_number
 * @property string $password
 * @property string $created_datetime
 * @property string $modified_datetime
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property DelegateRecharge[] $delegateRecharges
 * @property ScheduleRecharge[] $scheduleRecharges
 * @property GlobalMissedNumbers[] $globalMissedNumbers
 * @property StoreAppCategories[] $storeAppCategories
 * @property Transactions[] $transactions
 * @property Wallets[] $wallets
 */
class Users extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_name', 'unique', 'className' => 'Users', 'attributeName' => 'user_name', 'message' => "Mobile number already exists"),
            array('user_name', 'required', 'message' => 'User name cannot be blank ', 'on' => 'signin'),
            array('user_name, password', 'length', 'max' => 50, 'message' => '1103 : ', 'on' => 'signin'),
            array('mobile_number', 'required', 'message' => 'Mobile number cannot be blank'),
            array('mobile_number', 'numerical', 'integerOnly' => true, 'message' => 'Invalid mobile number format'),
            array('mobile_number', 'length', 'max' => 10, 'message' => 'Mobile number cannot be less than 10 digit'),
            array('password', 'required', 'message' => '1102 : Password cannot be blank'),
            // array('modified_datetime', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_name, mobile_number, password, created_datetime, modified_datetime, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'delegateRecharges' => array(self::HAS_MANY, 'DelegateRecharge', 'users_id'),
            'scheduleRecharges' => array(self::HAS_MANY, 'ScheduleRecharge', 'users_id'),
            'globalMissedNumbers' => array(self::MANY_MANY, 'GlobalMissedNumbers', 'users_has_global_missed_numbers(users_id, global_missed_numbers_id)'),
            'storeAppCategories' => array(self::MANY_MANY, 'StoreAppCategories', 'users_has_store_app_categories(users_id, store_app_categories_category_id)'),
            'transactions' => array(self::MANY_MANY, 'Transactions', 'users_has_transactions(users_id, transactions_trans_id)'),
            'wallets' => array(self::HAS_MANY, 'Wallets', 'users_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_name' => 'User Name',
            'mobile_number' => 'Mobile Number',
            'password' => 'Password',
            'created_datetime' => 'Created Datetime',
            'modified_datetime' => 'Modified Datetime',
            'status' => 'Status',
            'role' => 'Role',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_name', $this->user_name, true);
        $criteria->compare('mobile_number', $this->mobile_number, true);
        //$criteria->compare('password', $this->password, true);
        $criteria->compare('created_datetime', $this->created_datetime, true);
        $criteria->compare('modified_datetime', $this->modified_datetime, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    //--hash password
    public function hashPassword($password) {
        return md5($password);
    }

    //---validate password
    public function validatePassword($password) {
        return $this->hashPassword($password) === $this->password;
    }
    
    //
    public function validateApiPassword($password) {
        return $password == $this->password;
    }   
    

    //----generate salt
    public function generateSalt() {
        return uniqid('', true);
    }

    //---
    public function beforeValidate() {
        $this->salt = $this->generateSalt();
        return parent::beforeValidate();
    }

    //---
    public function beforeSave() {
        $this->password = $this->hashPassword($this->password);
        return parent::beforeSave();
    }

}
