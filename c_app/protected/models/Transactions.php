<?php

/**
 * This is the model class for table "transactions".
 *
 * The followings are the available columns in table 'transactions':
 * @property string $trans_id
 * @property string $trans_datetime
 * @property string $trans_category
 * @property string $users_id
 * @property string $track_id
 * @property string $transaction_mode
 * @property integer $trans_type
 * @property double $opening_bal
 * @property double $transaction_amount
 * @property double $closing_bal
 * @property string $response
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Users[] $users
 * @property WalletsHasTransactions[] $walletsHasTransactions
 */
class Transactions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transactions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trans_datetime, users_id, trans_type, response', 'required'),
			array('trans_type, status', 'numerical', 'integerOnly'=>true),
			array('opening_bal, transaction_amount, closing_bal', 'numerical'),
			array('trans_category, transaction_mode', 'length', 'max'=>50),
			array('users_id', 'length', 'max'=>20),
			array('track_id', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('trans_id, trans_datetime, trans_category, users_id, track_id, transaction_mode, trans_type, opening_bal, transaction_amount, closing_bal, response, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::MANY_MANY, 'Users', 'users_has_transactions(transactions_trans_id, users_id)'),
			'walletsHasTransactions' => array(self::HAS_MANY, 'WalletsHasTransactions', 'transactions_trans_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trans_id' => 'Trans',
			'trans_datetime' => 'Trans Datetime',
			'trans_category' => 'Trans Category',
			'users_id' => 'Users',
			'track_id' => 'Track',
			'transaction_mode' => 'Transaction Mode',
			'trans_type' => 'Trans Type',
			'opening_bal' => 'Opening Bal',
			'transaction_amount' => 'Transaction Amount',
			'closing_bal' => 'Closing Bal',
			'response' => 'Response',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trans_id',$this->trans_id,true);
		$criteria->compare('trans_datetime',$this->trans_datetime,true);
		$criteria->compare('trans_category',$this->trans_category,true);
		$criteria->compare('users_id',$this->users_id,true);
		$criteria->compare('track_id',$this->track_id,true);
		$criteria->compare('transaction_mode',$this->transaction_mode,true);
		$criteria->compare('trans_type',$this->trans_type);
		$criteria->compare('opening_bal',$this->opening_bal);
		$criteria->compare('transaction_amount',$this->transaction_amount);
		$criteria->compare('closing_bal',$this->closing_bal);
		$criteria->compare('response',$this->response,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transactions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
