<?php

/**
 * This is the model class for table "user_profile".
 *
 * The followings are the available columns in table 'user_profile':
 * @property integer $id
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $gender
 * @property string $date_of_birth
 * @property string $user_id
 * @property string $gcm_reg_id
 * @property string $uuid
 * @property double $longitude
 * @property double $latitude
 * @property string $location_src
 * @property string $device_type
 * @property string $created
 * @property string $updated
 */
class UserProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, mobile, email, gender, date_of_birth', 'safe'),
			//array('longitude, latitude', 'numerical'),
                        array('longitude, latitude', 'safe'),
			array('name, mobile, email', 'length', 'max'=>100),
			array('gender', 'length', 'max'=>10),
			array('user_id', 'length', 'max'=>20),
			array('gcm_reg_id, uuid, location_src, device_type', 'length', 'max'=>255),
			array('created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, mobile,image,fb_data,email, api_version, gender, date_of_birth,latitude_longitude, user_id, gcm_reg_id, uuid, longitude, latitude, location_src, device_type, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'mobile' => 'Mobile',
			'email' => 'Email',
			'gender' => 'Gender',
			'date_of_birth' => 'Date Of Birth',
			'user_id' => 'User',
			'gcm_reg_id' => 'Gcm Reg',
			'uuid' => 'Uuid',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'location_src' => 'Location Src',
			'device_type' => 'Device Type',
			'created' => 'Created',
			'updated' => 'Updated',
                        'latitude_longitude ' => 'Latitude_longitude',
                        'api_version'=> 'Api_version',
                        'image'=>'image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('date_of_birth',$this->date_of_birth,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('gcm_reg_id',$this->gcm_reg_id,true);
		$criteria->compare('uuid',$this->uuid,true);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('location_src',$this->location_src,true);
		$criteria->compare('device_type',$this->device_type,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
                $criteria->compare('latitude_longitude',$this->latitude_longitude,true);
                $criteria->compare('api_version',$this->api_version,true);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
