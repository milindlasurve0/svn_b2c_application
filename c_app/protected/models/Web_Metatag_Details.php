<?php

/**
 * This is the model class for table "offers".
 *
 * The followings are the min_amountavailable columns in table 'offers':
 * @property string $id
 * @property string $page_url
 * @property string $page_tittle
 * @property integer $page_meta_description
 * @property integer $page_meta_tag
 * @property string $created
 */
class Web_Metatag_Details extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'web_metatag_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            //id  page_url  page_tittle page_meta_description  page_meta_tag created 
			array('page_url, page_tittle, page_meta_description , page_meta_tag , created', 'required'),
			array('page_url, page_tittle, page_meta_description , page_meta_tag', 'length', 'max'=>255),
			array('id, page_url, page_tittle, page_meta_description , page_meta_tag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        // 	short_desc 	long_desc 	created 	update
		return array(
			'id' => 'ID',
			'page_url' => 'Page url',
			'page_tittle' => 'Page Tittle',
                        'page_meta_description' => 'Page Meta Description',
                        'page_meta_tag' => 'Page Meta Tag',
                        'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('page_url',$this->page_url,true);
		$criteria->compare('page_tittle',$this->page_tittle,true);
                $criteria->compare('page_meta_description',$this->page_meta_description,true);
                $criteria->compare('page_meta_tag',$this->page_meta_tag,true);
                $criteria->compare('created',$this->created,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
