<?php

/**
 * This is the model class for table "deal_transaction".
 *
 * The followings are the available columns in table 'deal_transaction':
 * @property string $id
 * @property string $transaction_id
 * @property string $users_id
 * @property string $deal_id
 * @property string $offer_id
 * @property string $coupon_id
 * @property string $quantity
 */
class DealTransaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deal_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transaction_id, users_id, deal_id, offer_id, coupon_id, quantity', 'required'),
			array('transaction_id, users_id, deal_id, offer_id, coupon_id', 'length', 'max'=>20),
			array('quantity', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, transaction_id, users_id, deal_id, offer_id, coupon_id, quantity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'transaction_id' => 'Transaction',
			'users_id' => 'Users',
			'deal_id' => 'Deal',
			'offer_id' => 'Offer',
			'coupon_id' => 'Coupon',
			'quantity' => 'Quantity',
            'amount'=>'Amount',    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('transaction_id',$this->transaction_id,true);
		$criteria->compare('users_id',$this->users_id,true);
		$criteria->compare('deal_id',$this->deal_id,true);
		$criteria->compare('offer_id',$this->offer_id,true);
		$criteria->compare('coupon_id',$this->coupon_id,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('amount',$this->amount,true);
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DealTransaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
