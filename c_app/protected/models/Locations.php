<?php

/**
 * This is the model class for table "deal_locations".
 *
 * The followings are the available columns in table 'deal_locations':
 * @property string $id
 * @property integer $deal_id
 * @property double $lat
 * @property double $lng
 * @property string $address
 * @property integer $city_id
 * @property integer $status
 * @property string $updated
 * @property string $created
 *
 * The followings are the available model relations:
 * @property LocatorCity $city
 */
class Locations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deal_locations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deal_id, lat, lng, status, updated, created', 'required'),
			array('deal_id, city_id, status', 'numerical', 'integerOnly'=>true),
			array('lat, lng', 'numerical'),
			array('address', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, deal_id, lat, lng, address, city_id, status, updated, created,lat_lng', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'LocatorCity', 'city_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'deal_id' => 'Deal',
			'lat' => 'Lat',
			'lng' => 'Lng',
			'address' => 'Address',
			'city_id' => 'City',
			'status' => 'Status',
			'updated' => 'Updated',
			'created' => 'Created',
                        'lat_lng' => 'lat_lng',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('deal_id',$this->deal_id);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lng',$this->lng);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('created',$this->created,true);
                $criteria->compare('lat_lng',$this->lat_lng,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DealLocations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
