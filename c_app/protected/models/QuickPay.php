<?php

/**
 * This is the model class for table "quick_pay".
 *
 * The followings are the available columns in table 'quick_pay':
 * @property string $id
 * @property string $name
 * @property string $mobile_number
 * @property string $subscriber_id
 * @property double $amount
 * @property string $datetime
 * @property string $modified_time
 * @property integer $operator_id
 * @property string $additional_detail1
 * @property string $additional_detail2
 * @property integer $payment_flag
 */
class QuickPay extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quick_pay';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,transaction_flag, mobile_number, subscriber_id, datetime, modified_time, operator_id, additional_detail1, additional_detail2, payment_flag', 'safe'),
			array('operator_id, payment_flag', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('name, additional_detail1', 'length', 'max'=>100),
			array('mobile_number', 'length', 'max'=>12),
			array('subscriber_id', 'length', 'max'=>20),
			array('additional_detail2', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, mobile_number, subscriber_id, amount, datetime, modified_time, operator_id, additional_detail1, additional_detail2, payment_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                        'users_id' => 'User ID',    
			'name' => 'Name',
			'mobile_number' => 'Mobile Number',
			'subscriber_id' => 'Subscriber',
                        'stv' => 'STV',    
			'amount' => 'Amount',
			'datetime' => 'Datetime',
			'modified_time' => 'Modified Time',
			'operator_id' => 'Operator',
			'additional_detail1' => 'Additional Detail1',
			'additional_detail2' => 'Additional Detail2',
			'payment_flag' => 'Payment Flag',
                        'delegate_id' => 'delegate Id',
                        'transaction_flag'=>'Transaction Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
        $criteria->compare('users_id',$this->users_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('subscriber_id',$this->subscriber_id,true);
        $criteria->compare('stv',$this->stv,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('modified_time',$this->modified_time,true);
		$criteria->compare('operator_id',$this->operator_id);
		$criteria->compare('additional_detail1',$this->additional_detail1,true);
		$criteria->compare('additional_detail2',$this->additional_detail2,true);
		$criteria->compare('payment_flag',$this->payment_flag);
		$criteria->compare('delegate_id',$this->delegate_id);
                $criteria->compare('transaction_flag',$this->transaction_flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuickPay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
