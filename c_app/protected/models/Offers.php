<?php

/**
 * This is the model class for table "offers".
 *
 * The followings are the min_amountavailable columns in table 'offers':
 * @property string $id
 * @property string $deal_id
 * @property string $name
 * @property integer $offer_price
 * @property integer $actual_price
 * @property integer $discount
 * @property string $total_stock 
 * @property string $stock_sold
 * @property string $validity
 * @property integer $validity_mode
 * @property integer $gift_coin
 * @property integer $status
 * @property string $coupon_expiry_date
 */
class Offers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            //id 	deal_id 	name 	offer_price 	actual_price 	discount 	total_stock 	stock_sold 	validity 	validity_mode 	status 	short_desc 	long_desc 	created 	update
			array('deal_id, name, offer_price, actual_price, discount, validity_mode, status , created , updated', 'required'),
			array('validity_mode, status,global_flag,order_flag', 'numerical', 'integerOnly'=>true),
                        array('offer_price, actual_price, discount, status', 'numerical','integerOnly'=>false),
			array('deal_id, total_stock', 'length', 'max'=>20),
			array('name', 'length', 'max'=>100),
                        array('coupon_expiry_date', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, deal_id, name, offer_price, actual_price, offer_desc, discount, total_stock, validity, '
                            . ' global_flag, order_flag, validity_mode, status , created , updated ,coupon_expiry_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        // 	short_desc 	long_desc 	created 	update
		return array(
			'id' => 'ID',
			'deal_id' => 'Deal',
			'name' => 'Name',
			'offer_price' => 'Offer Price',
			'actual_price' => 'Actual Price',
			'discount' => 'Discount',
			'total_stock' => 'Total Stock',
                       'stock_sold' => 'Stock Sold',
			'validity' => 'Validity',
			'validity_mode' => 'Validity Mode',
                        'max_quantity' => 'Max Quantity',   
                        'status' => 'Status',
                        'short_desc' => 'Short Description',
                        'long_desc' => 'Long Description',
                        'created' => 'Created',
                        'updated' => 'Updated',
                        'img_url' => 'Image Url',
                        'sequence' => 'Sequence',
                        'global_flag' => 'Global flag',
                        'order_flag' => 'Order flag',
                    'coupon_expiry_date'=> 'Coupon expiry date',
                        'offer_desc'=> 'offer desc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('deal_id',$this->deal_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('offer_price',$this->offer_price);
		$criteria->compare('actual_price',$this->actual_price);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('total_stock',$this->total_stock,true);
                $criteria->compare('stock_sold',$this->stock_sold,true);
		$criteria->compare('validity',$this->validity,true);
		$criteria->compare('validity_mode',$this->validity_mode);
		$criteria->compare('max_quantity',$this->max_quantity);
		$criteria->compare('status',$this->status);
                $criteria->compare('img_url',$this->img_url);
		$criteria->compare('sequence',$this->sequence);
                $criteria->compare('global_flag',$this->global_flag);
                $criteria->compare('order_flag',$this->order_flag);
                $criteria->compare('coupon_expiry_date',$this->coupon_expiry_date);
                $criteria->compare('offer_desc',$this->offer_desc);
               
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
