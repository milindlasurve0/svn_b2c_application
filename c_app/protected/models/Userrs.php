<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property string $user_name
 * @property string $mobile_number
 * @property string $password
 * @property string $salt
 * @property string $created_datetime
 * @property string $modified_datetime
 * @property integer $role
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property DelegateRecharge[] $delegateRecharges
 * @property ScheduleRecharge[] $scheduleRecharges
 * @property UserRole $role0
 * @property Wallets[] $wallets
 */
class Userrs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('salt, created_datetime', 'required'),
			array('role, status', 'numerical', 'integerOnly'=>true),
			array('user_name, password,dealer_name', 'length', 'max'=>50),
			array('mobile_number', 'length', 'max'=>20),
			array('salt', 'length', 'max'=>64),
			array('modified_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_name, mobile_number, password, salt, created_datetime, modified_datetime, role, status,dealer_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'delegateRecharges' => array(self::HAS_MANY, 'DelegateRecharge', 'users_id'),
			'scheduleRecharges' => array(self::HAS_MANY, 'ScheduleRecharge', 'users_id'),
			'role0' => array(self::BELONGS_TO, 'UserRole', 'role'),
			'wallets' => array(self::HAS_MANY, 'Wallets', 'users_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_name' => 'User Name',
			'mobile_number' => 'Mobile Number',
			'password' => 'Password',
			'salt' => 'Salt',
			'created_datetime' => 'Created Datetime',
			'modified_datetime' => 'Modified Datetime',
			'role' => '1=admin, 2= general, 3=dealer',
			'status' => 'Status',
                        'dealer_name'=> 'Dealer Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('modified_datetime',$this->modified_datetime,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('status',$this->status);
                $criteria->compare('dealer_name',$this->dealer_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Userrs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
