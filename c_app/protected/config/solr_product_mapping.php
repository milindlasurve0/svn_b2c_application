<?php
// this contains the application parameters that can be maintained via GUI

return array(
        //mobile
        1 => array("product"=>"MOBILE","operator_id"=>1,"flag"=>1),
        2 => array("product"=>"MOBILE","operator_id"=>2,"flag"=>1),
        3 => array("product"=>"MOBILE","operator_id"=>4,"flag"=>1),
        4 => array("product"=>"MOBILE","operator_id"=>8,"flag"=>1),
        5 => array("product"=>"MOBILE","operator_id"=>9,"flag"=>1),
        6 => array("product"=>"MOBILE","operator_id"=>10,"flag"=>1),
        7 => array("product"=>"MOBILE","operator_id"=>12,"flag"=>1),
        8 => array("product"=>"MOBILE","operator_id"=>13,"flag"=>1),
        9 => array("product"=>"MOBILE","operator_id"=>16,"flag"=>1),
        10 => array("product"=>"MOBILE","operator_id"=>17,"flag"=>1),
        11 => array("product"=>"MOBILE","operator_id"=>18,"flag"=>1),
        12 => array("product"=>"MOBILE","operator_id"=>5,"flag"=>1),
        15 => array("product"=>"MOBILE","operator_id"=>21,"flag"=>1),
        30 => array("product"=>"MOBILE","operator_id"=>6,"flag"=>1),
        
        //billing
        36 => array("product"=>"BILLING","operator_id"=>16,"flag"=>4),
        37 => array("product"=>"BILLING","operator_id"=>9,"flag"=>4),
        38 => array("product"=>"BILLING","operator_id"=>4,"flag"=>4),
        39 => array("product"=>"BILLING","operator_id"=>8,"flag"=>4),
        40 => array("product"=>"BILLING","operator_id"=>17,"flag"=>4),
        41 => array("product"=>"BILLING","operator_id"=>21,"flag"=>4),
        42 => array("product"=>"BILLING","operator_id"=>2,"flag"=>4),
        43 => array("product"=>"BILLING","operator_id"=>12,"flag"=>4),
        
        //dth
        16 => array("product"=>"DTH","operator_id"=>1,"flag"=>2),
        17 => array("product"=>"DTH","operator_id"=>2,"flag"=>2),
        18 => array("product"=>"DTH","operator_id"=>3,"flag"=>2),
        19 => array("product"=>"DTH","operator_id"=>4,"flag"=>2),
        20 => array("product"=>"DTH","operator_id"=>5,"flag"=>2),
        21 => array("product"=>"DTH","operator_id"=>6,"flag"=>2),
        
        //stv
        27 => array("product"=>"MOBILE","operator_id"=>16,"stv"=>1,"flag"=>1),
        28 => array("product"=>"MOBILE","operator_id"=>5,"stv"=>1,"flag"=>1),
        29 => array("product"=>"MOBILE","operator_id"=>18,"stv"=>1,"flag"=>1),
        31 => array("product"=>"MOBILE","operator_id"=>6,"stv"=>1,"flag"=>1),
        34 => array("product"=>"MOBILE","operator_id"=>4,"stv"=>1,"flag"=>1),
);

