<?php

// this contains the application parameters that can be maintained via GUI

return array(
        'MOBILE' => array(
                '1' => array(
                        'operator' => 'Aircel',
                        'opr_code' => 'AC',
                        'flexi' => array('pay1' => '1'),
                        'voucher' => array('pay1' => '')
                ),
                '2' => array(
                        'operator' => 'Airtel',
                        'opr_code' => 'AT',
                        'flexi' => array('pay1' => '2'),
                        'voucher' => array('pay1' => '')
                ),
                '3' => array(
                        'operator' => 'BSNL',
                        'opr_code' => 'CG',
                        'flexi' => array('pay1' => '3'),
                        'voucher' => array('pay1' => '34')
                ),
                '4' => array(
                        'operator' => 'Idea',
                        'opr_code' => 'ID',
                        'flexi' => array('pay1' => '4'),
                        'voucher' => array('pay1' => '')
                ),
                '5' => array(
                        'operator' => 'Loop/BPL',
                        'opr_code' => 'LM',
                        'flexi' => array('pay1' => '5'),
                        'voucher' => array('pay1' => '')
                ),
                '6' => array(
                        'operator' => 'MTS',
                        'opr_code' => 'MT',
                        'flexi' => array('pay1' => '6'),
                        'voucher' => array('pay1' => '')
                ),
                '7' => array(
                        'operator' => 'Reliance CDMA',
                        'opr_code' => 'RC',
                        'flexi' => array('pay1' => '7'),
                        'voucher' => array('pay1' => '')
                ),
                '8' => array(
                        'operator' => 'Reliance GSM',
                        'opr_code' => 'RG',
                        'flexi' => array('pay1' => '8'),
                        'voucher' => array('pay1' => '')
                ),
                '9' => array(
                        'operator' => 'Tata Docomo',
                        'opr_code' => 'TD',
                        'flexi' => array('pay1' => '9'),
                        'voucher' => array('pay1' => '27')
                ),
                '10' => array(
                        'operator' => 'Tata Indicom',
                        'opr_code' => 'TI',
                        'flexi' => array('pay1' => '10'),
                        'voucher' => array('pay1' => '27')
                ),
                '11' => array(
                        'opr_code' => 'UN',
                        'flexi' => array('pay1' => '11'),
                        'operator' => 'Uninor',
                        'voucher' => array('pay1' => '29')
                ),
                '12' => array(
                        'operator' => 'Videocon',
                        'opr_code' => 'DC',
                        'flexi' => array('pay1' => '12'),
                        'voucher' => array('pay1' => '28')
                ),
                '13' => array(
                        'operator' => 'Virgin CDMA',
                        'opr_code' => 'VC',
                        'flexi' => array('pay1' => '13'),
                        'voucher' => array('pay1' => '')
                ),
                '14' => array(
                        'operator' => 'Virgin GSM',
                        'opr_code' => 'VG',
                        'flexi' => array('pay1' => '14'),
                        'voucher' => array('pay1' => '')
                ),
                '15' => array(
                        'operator' => 'Vodafone',
                        'opr_code' => 'VF',
                        'flexi' => array('pay1' => '15'),
                        'voucher' => array('pay1' => '')
                ),
                '30' => array(
                        'operator' => 'MTNL',
                        'opr_code' => 'MT',
                        'flexi' => array('pay1' => '30'),
                        'voucher' => array('pay1' => '31')
                )
        ),
        'DTH' => array(
                '16' => array(
                        'operator' => 'Airtel DTH',
                        'flexi' => array('pay1' => '16')
                ),
                '17' => array(
                        'operator' => 'Big TV DTH',
                        'flexi' => array('pay1' => '17')
                ),
                '18' => array(
                        'operator' => 'Dish TV DTH',
                        'flexi' => array('pay1' => '18')
                ),
                '19' => array(
                        'operator' => 'Sun TV DTH',
                        'flexi' => array('pay1' => '19')
                ),
                '20' => array(
                        'operator' => 'Tata Sky DTH',
                        'flexi' => array('pay1' => '20')
                ),
                '21' => array(
                        'operator' => 'Videocon DTH',
                        'flexi' => array('pay1' => '21')
                )
        ),
        'DATA' => array(
                '44' => array(
                        'operator' => 'Aircel',
                        'flexi' => array('pay1' => '1')
                ),
                '45' => array(
                        'operator' => 'Airtel',
                        'flexi' => array('pay1' => '2')
                ),
                '46' => array(
                        'operator' => 'BSNL',
                        'flexi' => array('pay1' => '3')
                ),
                '47' => array(
                        'operator' => 'Idea',
                        'flexi' => array('pay1' => '4')
                ),
                '48' => array(
                        'operator' => 'MTNL',
                        'flexi' => array('pay1' => '30')
                ),
                '49' => array(
                        'operator' => 'MTS',
                        'flexi' => array('pay1' => '6')
                ),
                '50' => array(
                        'operator' => 'Reliance',
                        'flexi' => array('pay1' => '7')
                ),
                '51' => array(
                        'operator' => 'Tata',
                        'flexi' => array('pay1' => '10')
                ),
                '52' => array(
                        'operator' => 'Vodafone',
                        'flexi' => array('pay1' => '15')
                )
        ),
        'POSTPAID' => array(
                '36' => array(
                        'operator' => 'Docomo Postpaid',
                        'flexi' => array('pay1' => '36')
                ),
                '37' => array(
                        'operator' => 'Loop Mobile PostPaid',
                        'flexi' => array('pay1' => '37')
                ),
                '38' => array(
                        'operator' => 'Cellone PostPaid',
                        'flexi' => array('pay1' => '38')
                ),
                '39' => array(
                        'operator' => 'IDEA Postpaid',
                        'flexi' => array('pay1' => '39')
                ),
                '40' => array(
                        'operator' => 'Tata TeleServices PostPaid',
                        'flexi' => array('pay1' => '40')
                ),
                '41' => array(
                        'operator' => 'Vodafone Postpaid',
                        'flexi' => array('pay1' => '41')
                ),
                '42' => array(
                        'operator' => 'Airtel Postpaid',
                        'flexi' => array('pay1' => '42')
                ),
                '43' => array(
                        'operator' => 'Reliance Postpaid',
                        'flexi' => array('pay1' => '43')
                )
        )
);

