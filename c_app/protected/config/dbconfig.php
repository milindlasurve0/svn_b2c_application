<?php

/*
 * This is a part of main.php. It contain config specific to database
 * 
 */

include_once 'setting.php';

return array(
        'connectionString' => $db_con_string,
        'emulatePrepare' => true,
        'username' => $db_user,
        'password' => $db_password,
        'charset' => 'utf8',
);
