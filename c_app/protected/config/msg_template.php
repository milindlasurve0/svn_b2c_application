<?php

// this contains the application parameters that can be maintained via GUI

return array(
    
'OTP_PASSWORD'=>"Your One Time Password (OTP) for Pay1 app registration is : <OTP>.",
    
'NEW_OTP_PASSWORD'=>"Your One Time Password (OTP) for Pay1 app registration is : <OTP>. This is also your temporary password. Save it for your next login. Kindly set a permanent password.",

'FORGOT_PASSWORD' =>  "You have requested for a new password. Your new Password is: <PASSWD>. Kindly ignore the message if you haven't requested one.",
        
'SUCCESS_REGISTRATION'=>"Congratulations!! You have successfully signed-up with Pay1. You can now topup your account at nearby Pay1 store or by using online payment.",
        
'WALLET_REFILLED_RETAILER_NONUSER' => "Retailer <SHOP_NAME> have transferred Rs <AMOUNT> in your Pay1 account. Kindly register today & do recharges with www.pay1.in\n Download Pay1 App: <APP_URL>",
        
'WALLET_REFILLED_ONLINE' => "You have successfully updated your Pay1 balance by Rs <AMOUNT> successfully. Your current Pay1 wallet balance is Rs <BALANCE>",
    
'WALLET_REFILLED_ONLINE_NEW' => "You have successfully updated your Pay1 balance by Rs <AMOUNT>. Your current Pay1 wallet balance is Rs <BALANCE>",
        
'WALLET_REFILLED_VOUCHER' => "Your Pay1 account has been successfully updated by Pay1 voucher. Your current Pay1 balance is Rs. <BALANCE>.",
    
'WALLET_REFILLED_VOUCHER_NEW' => "Your Pay1 account has been successfully updated by Pay1 voucher. Your current Pay1 balance is Rs. <BALANCE>.You have <LOYALTY_BALANCE> coins in your Pay1 account.",
        
'WALLET_REFILLED_RETAILER' => "Retailer <SHOP_NAME> have transferred Rs <AMOUNT> in your Pay1 account. Your current Pay1 balance is Rs <BALANCE>.\nKindly pay the same to your retailer.",
    
'WALLET_REFILLED_RETAILER_NEW' => "Retailer <SHOP_NAME> have transferred Rs <AMOUNT> in your Pay1 account. Your current Pay1 balance is Rs <BALANCE>.\nKindly pay the same to your retailer.You have <LOYALTY_BALANCE> coins in your Pay1 account.",
        
'WALLET_TOPUP_REVERSED' => "We have successfully taken back Rs <AMOUNT> from your account. As our merchant have previously refilled your Pay1 account by mistake. Sorry for inconvenience caused. For any help give a missed call to <MISSCALL_NO>",

'RECHARGE_SUCCESS' => "Your request is accepted. Your Pay1 TXN ID: <TXN_ID>; <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT>. You will receive your recharge shortly. Your current Pay1 balance is Rs <BALANCE>.For Support - Give a missed call to 02267242266",

'RECHARGE_SUCCESS_NEW' =>"Your request is accepted. Your Pay1 TXN ID: <TXN_ID>; <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT>. You have been awarded <LOYALTY_POINTS> Gift Coins & now your current balance is <LOYALTY_BALANCE> Gift Coins. For Support - Give a missed call to 02267242266",
        
'RECHARGE_FAILURE' => "Your recharge request of <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> has been failed due to <ERRMSG>. The recharge amount has been credited back to your Pay1 balance. Your current Pay1 balance is Rs <BALANCE>.",
    
'RECHARGE_FAILURE_NEW' => "Your recharge request of <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> has been failed due to <ERRMSG>. The recharge amount has been credited back to your Pay1 balance. Your current Pay1 balance is Rs <BALANCE>. Your awarded Pay1 coins have been withdrawn. You have <LOYALTY_BALANCE> coins in your Pay1 account.",

'BILLPAYMENT_SUCCESS' => "Your request is accepted. Your Pay1 TXN ID: <TXN_ID>; <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> (Exclusive of Service Charge Rs <SERVICE_CHARGE>). Your bill will get updated within 24 hours. Your current Pay1 balance is Rs <BALANCE>.",
        
'BILLPAYMENT_SUCCESS_NEW' => "Your request is accepted. Your Pay1 TXN ID: <TXN_ID>; <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> (Exclusive of Service Charge Rs <SERVICE_CHARGE>). Your bill will get updated within 24 hours. Your current Pay1 balance is Rs <BALANCE>. You have <LOYALTY_BALANCE> coins in your Pay1 account.",
    
'BILLPAYMENT_FAILURE' => "Your bill payment request of <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> has been failed due to <ERRMSG>. The bill amount with service charge has been credited back to your Pay1 balance. Your current Pay1 balance is Rs <BALANCE>.",
    
 'BILLPAYMENT_FAILURE_NEW' => "Your bill payment request of <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> has been failed due to <ERRMSG>. The bill amount with service charge has been credited back to your Pay1 balance. Your current Pay1 balance is Rs <BALANCE>. Your awarded Pay1 coins have been withdrawn. You have <LOYALTY_BALANCE> coins in your Pay1 account.",   

'DEAL_SUCCESS' => "Your purchase of Pay1 deal of Rs <AMOUNT> is successful. Your current wallet balance is Rs <BALANCE>. Your Pay1 TXN id is: <TXN_ID>",
        
'DEAL_DETAILS' => "Your voucher code for <DEAL_NAME> is <VOUCHER> of TXN ID <TXN_ID>. Your deal is valid until <EXPIRY_DATE>.\nKindly show this voucher code to your deal merchant",

'MISSCALL_SUCCESS_USER' => "Your wallet has been deducted for the recharge of <UMOBILE>; Operator: <OPERATOR>; Amt: <AMOUNT>  via missed call. Your current Pay1 balance is Rs <BALANCE>",
    
'MISSCALL_SUCCESS_USER_NEW' => "Your wallet has been deducted for the recharge of <UMOBILE>; Operator: <OPERATOR>; Amt: <AMOUNT>  via missed call. Your current Pay1 balance is Rs <BALANCE>. You have <LOYALTY_BALANCE> coins in your Pay1 account.",
        
'MISSCALL_FAILURE_USER_NOBAL' => "<MOBILE> is trying for recharge via misscall. Kindly update your Pay1 wallet for the recharges to happen. Your current Pay1 balance is Rs <BALANCE>",
    
'MISSCALL_FAILURE_USER_NOBAL_NEW' => "<MOBILE> is trying for recharge via misscall. Kindly update your Pay1 wallet for the recharges to happen. Your current Pay1 balance is Rs <BALANCE>. You have <LOYALTY_BALANCE> coins in your Pay1 account.",
    
 'MISSCALL_FAILURE_USER' => "<UMOBILE> tried for recharge via missed call service but failed due to <ERRMSG>. Your current Pay1 balance is Rs <BALANCE>",
    
 'MISSCALL_FAILURE_USER_NEW' => "<UMOBILE> tried for recharge via missed call service but failed due to <ERRMSG>. Your current Pay1 balance is Rs <BALANCE>. The rewarded coins have been withdrawn. You have <LOYALTY_BALANCE> coins in your Pay1 account.",

'MISSCALL_SUCCESS_NON_USER' => "Your request is accepted via missed call for recharge of <OPERATOR> and amount <AMOUNT>. You will receive your recharge shortly.",

'MISSCALL_FAILURE_NON_USER_NOBAL' => "We cannot proceed your recharge as your Pay1 user does not have sufficient balance in his/her account",
        
'MISSCALL_FAILURE_NON_USER' => "Your missed call recharge request of <UMOBILE>; Operator: <OPERATOR>; Amt: <AMOUNT> failed due to <ERRMSG>",

'MISSCALL_FAILURE_NON_USER_RETRY' => "You cannot do the same recharge within 1 hour of <OPERATOR>; amount <AMOUNT> on your mobile",

'CHANGE_PASSWORD_SUCCESS' => "Your Pay1 password has been changed successfully.",

'COMPLAINT_MISSCALL_SUCCESS' => "We have successfully registered complaint for your last transaction with TXN id <TXN_ID>. Your complaint number for the same is <COMP_NO>",
        
'COMPLAINT_MISSCALL_FAILURE' => "We will get in touch with you soon",
        
'COMPLAINT_RESOLVED_SUCCESS' => "Your complaint for TXN id <TXN_ID> is resolved successfully. Your transaction was successful",
        
'COMPLAINT_RESOLVED_FAILURE' => "Your complaint for TXN id <TXN_ID> is resolved successfully. Your transaction failed due to <ERRSMG>",

'CHANGE_EMAIL_ID_SUCCESS' => "Your email id has been updated successfully.",
    
'CHANGE_PROFILE_SUCCESS'=>"You have successfully updated your profile",
    
'CHANGE_PROFILE_SUCCESS_FB'=>"We have successfully fetched the data from facebook",

'LINK_VIA_MISSED_CALL'=>'Hi Thanks for choosing Pay1. To download the new Pay1 app from Play Store, Check out the link below:  Android : <A_LINK>',
        
'FREEBIE_SUCCESS' => "You have successfully grabbed a gift from <FB_NAME>. Your gift code is <CODE>. Please redeem your gift  on or before <EXPIRY>.",
    
'FREEBIE_SUCCESS_NEW' => "You have successfully grabbed a gift from <FB_NAME>. Your gift code is <CODE>. Please redeem your gift on or before <EXPIRY>. You have <LOYALTY_BALANCE> gift coins remaining in your Pay1 account.",
    
'FREEBIE_FAILURE' => "Due to failure of your transaction you are not eligible for your free gift.",

'FREEBIE_BEFORE_EXPIRE_NOTIFY' =>  "Your gift(s) from <OFFER> will expire after two days.\r\n Please redeem your gift today. Ignore if redeemed.",

'FREEBIE_EXPIRE_NOTIFY' =>   "Your gift(s) from <OFFER> has expired today.\r\n Ignore if redeemed.",
    
'FREEBIE_ANNOUNCEMENT'=> "Hey buddy, Grab the new gift from <OFFER> today. Check out the Pay1 Application now.",
    
'FREEBIE_ANNOUNCEMENT_NEW'=> "Hey buddy, Grab the new gift from <OFFER> today. Check out the Pay1 Application now.  You have <LOYALTY_BALANCE> gift coins in your Pay1 account.",
    
'B2C_BY_RETAILER_RECHARGE_SUCC'=>"Wow! <LOYALTY_COINS>  more gift coins, you have got now <LOYALTY_BALANCE> Pay1 gift coins. A free coffee is waiting for you. Check out the new Pay1 app to enjoy it. https://goo.gl/z09JU1",
    
'B2C_BY_RETAILER_RECHARGE_FAIL'=>"Oops Something went wrong! <LOYALTY_COINS> Gift coins reversed due to last txn failure. You've got total <LOYALTY_BALANCE> Pay1 gift coins.",

'NEW_MISS_CALL_USER_MSG'=>"Awesome! You've <AMOUNT> Pay1 gift coins. Grab exciting gifts using these coins. Simply Download Pay1 App https://goo.gl/z09JU1 and get additional 100 Coins FREE. For more info give a missed call on 022-67242266",    
    
'NON_B2B_N_B2C_USER'=>"Wonderful! Now simply recharge from Pay1 and start earning Gift coins. Download Pay1 app now and get your first 100 gift coins FREE! https://goo.gl/z09JU1 For more info give a missed call on 022-67242266",  

'B2C_EXISTING_USER'=>"You've got <LOYALITY_POINTS> Gift coins in ur PAY1 account. To get gifts, download PAY1 app & register ur mobile no. Choose Gift from the app! https://goo.gl/z09JU1", 
   
'APP_EXISTING_USER'=>"Wow! We missed you too. Keep recharging from Pay1, invite your friends to PAY1 family and keep sharing the fun. https://goo.gl/z09JU1",   
    
'CASHBACK_SUCCESS'=>"Cashback of Rs. <AMOUNT> will be credited in your Pay1 wallet within 24hrs of this transaction.",
    
'CASHBACK_INVALID'=>"Invalid code",
    
'CASHBACK_EXPIRED'=>"Expired, seems you're late!",
    
 'CASHBACK_USED'=>"Seems you've already applied.",
    
  'OTP_PASSWORD'=>"OTP for Pay1 login is <OTP>.",
    
   'USER_AUTH_OTP' =>"Please Enter OTP received on your mobile.",
    
    "INVALID_OTP" =>"Invalid otp.",
    
    "EXCEPTION"=>'Something went wrong.',
    
    'VALID_PARAM'=>'Please provide valid parameter',
    
    'STRONG_PASS'=>"Your password should be strong.\n Use at least 6 characters. Your password must contain \n At least 1 alphabet, 1 number and 1 special character( !, @ , # , $ , _ )",
    
    'MAX_WALLET_BALANCE_ALLOWED'=>'Wallet Amount Exceeding Max Allowed Amount',
    
    'EXISTING_USER'=>'User already registered with this Mobile Number',
);
