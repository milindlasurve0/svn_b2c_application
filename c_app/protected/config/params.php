<?php

// this contains the application parameters that can be maintained via GUI
return array(
        // this is displayed in the header section
        'title'=>'B2C Application',
        // this is used in error pages
        'adminEmail'=>'webmaster@example.com',
        // the copyright information displayed in the footer section
        'copyrightInfo'=>'Copyright &copy; 2014 by MindsArray.com',

    'vendorApi' => require(dirname(__FILE__) . '/vendorapis.php'),
    'productMapping' => require(dirname(__FILE__) . '/product_mapping.php'),
    'solrMapping' => require(dirname(__FILE__) . '/solr_product_mapping.php'),
    'accounting_cost' => array('service_charge_percent'=>'2','service_tax_percent'=>'14'),
    'whitelisted_IP'=>array('54.91.210.231','127.0.0.1','54.224.1.54','107.23.19.91','54.235.195.140','182.74.214.237','182.74.214.234','183.87.34.26','122.170.110.229','107.21.186.83','174.129.181.27','23.21.220.6','49.248.17.244','182.74.214.236','54.243.132.70','220.227.219.197','14.192.27.170','14.192.27.171','14.192.27.172'),
    'Internal_IP'=>array('182.74.214.234','182.74.214.235','182.74.214.236','182.74.214.237','220.227.219.197','127.0.0.1','14.192.27.170','14.192.27.171','14.192.27.172'),
    'cloud_IP'=>array('54.224.1.54','107.23.19.91','54.235.195.140','54.243.132.70'),
    'message_template'=>require(dirname(__FILE__) . '/msg_template.php'),
    'notification_template'=>require(dirname(__FILE__) . '/notification_template.php'),
    'err_template'=>require(dirname(__FILE__) . '/err_message.php'),
    'email_template'=>require(dirname(__FILE__) . '/email_template.php'),
    'GLOBALS'=>require(dirname(__FILE__) . '/constants.php')
);