<?php

/*
This is a part of main.php. It contain config specific to cache/redis
 */
include_once 'setting.php';

return $cache_conf = array(
        'class' => 'ext.redis.CRedisCache',
        'servers' => array(
                array(
                        'host' => $redis_host,
                        'port' => $redis_port,
                        //'password'=>$redis_password,
                        'database'=>$redis_database,
                ),
        ),
);
