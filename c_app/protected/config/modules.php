<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => 'password',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters' => array('127.0.0.1', '::1'),
        ),
);
