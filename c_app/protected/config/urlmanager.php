<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(strpos($_SERVER['HTTP_HOST'],'b2c.pay1')===0 || strpos($_SERVER['HTTP_HOST'],'cdev.pay1')===0){
    $url= 'site/login';
}else if(strpos($_SERVER['HTTP_HOST'],'bb.merchant')==0 || strpos($_SERVER['HTTP_HOST'],'bp.pay1')===0){
    $url='site/dealerlogin';
}

return array(
        'urlFormat' => 'path',
        'rules' => array(
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'posts/<tag:.*?>' => 'post/index',
                'deals/edit/' => 'deals/edit/',
                'deals/editDetails/<id:\d+>' => 'deals/editDetails/',
                'deals/createLocation' => 'deals/createLocation',
                'deals' => 'deals/index',
                'deals/preview/<id:\d+>' => 'deals/preview/',
                'deals/cropImage/<id:\d+>' => 'deals/cropImage/', 
                'deals/editDetails/panel/getDealMap/'=>'panel/getDealMap/',
                
                //MODALS
                //modals/deals/offerEdit/7
                'modals/deals/editOffer/<id:\d+>' => 'modals/deals/editOffer/',
                'modals/deals/editOfferInfo/<id:\d+>' => 'modals/deals/editOfferInfo/',
                'modals/deals/editInfo/<id:\d+>' => 'modals/deals/editInfo/',
                'modals/deals/editLocation/<id:\d+>' => 'modals/deals/editLocation/',
                'modals/deals/editMap/<id:\d+>' => 'modals/deals/editMap/',
                'api_new/action/<api:\w+>/<actionType:\w+>'=>'api_new/action/',
            
            
                // REST patterns
                
                array('api/list', 'pattern' => 'api/<model:\w+>', 'verb' => 'GET'),
                array('api/test', 'pattern' => 'api/<model:\w+>/<newval:\d+>', 'verb' => 'GET'),
                array('api/view', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => 'GET'),
                array('api/update', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => 'PUT'), // Update
                array('api/delete', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => 'DELETE'),
                //array('users/create/api/true', 'pattern'=>'api/<model:\w+>/<apiname:signup.do>', 'verb'=>'POST'),
//user api
                //array('users/create/api/true', 'pattern' => 'api/<apiname:signup.do>', 'verb' => 'POST'),
                array('site/login/api/true', 'pattern' => 'api/<apiname:signin.do>', 'verb' => 'GET'),
                array('site/logout/api/true', 'pattern' => 'api/<apiname:signout.do>', 'verb' => 'GET'),
                //wallet api
                array('wallets/action/api/true/actiontype/recharge/flag/1', 'pattern' => 'api/<apiname:mobilerecharge.do>', 'verb' => 'GET'),
                array('wallets/action/api/true/actiontype/recharge/flag/2', 'pattern' => 'api/<apiname:dthrecharge.do>', 'verb' => 'GET'),
                array('wallets/action/api/true/actiontype/moneytransfer/flag/1', 'pattern' => 'api/<apiname:wallet2wallettransfer.do>', 'verb' => 'GET'),
                array('wallets/action/api/true/actiontype/moneytransfer/flag/2', 'pattern' => 'api/<apiname:wallet2banktransfer.do>', 'verb' => 'GET'),
                //array('apis/action/api/true/actiontype/moneytransfer/flag/2', 'pattern'=>'api/<apiname:wallet2banktransfer.do>', 'verb'=>'GET'),
//array('apis/action/api/true/actiontype/moneytransfer/flag/1', 'pattern'=>'api/<apiname:wallet2wallettransfer.do>', 'verb'=>'GET'),
//------payment for apps
                array('wallets/action/api/true/actiontype/pay4app/flag/1', 'pattern' => 'api/<apiname:paybywallet.do>', 'verb' => 'GET'),
                array('wallets/action/api/true/actiontype/getapplist/flag/1', 'pattern' => 'api/<apiname:getapplist.do>', 'verb' => 'GET'),
                //array('wallets/action/api/true/actiontype/entertainment/flag/1', 'pattern'=>'api/<apiname:entertainment.do>', 'verb'=>'GET'),
                array('wallets/action/api/true/actiontype/delegate_recharge', 'pattern' => 'api/<apiname:missedcall2recharge.do>', 'verb' => 'GET'),
                //test api
                array('users/test/api/true', 'pattern' => 'api/<apiname:testapi.do>', 'verb' => 'GET'),
                array('apis/action/api/true/actiontype/apiauth', 'pattern' => 'api/<apiname:testauth.do>', 'verb' => 'GET'),
                array('apis/action/api/true/actiontype/get_operator', 'pattern' => 'api/<apiname:findoperator.do>', 'verb' => 'GET'),
                //array('users/testcall/api/true', 'pattern'=>'api/<model:\w+>/<apiname:getuserlist.do>', 'verb'=>'POST'),
                array('api/create', 'pattern' => 'api/<model:\w+>', 'verb' => 'POST'), // Create
               '<controller:\w+>/<action:\w+>' => '<controller>/<action>',                
                '' => $url,
        ),
);
