<?php
return array(
        'E008' => 'Invalid Mobile Number / Wrong operator',
        'E009' => 'Invalid Subscriber Id / Wrong operator',
        'E010' => 'Invalid Amount',
        'E012' => 'Two transactions within a time interval',
        'E013' => 'Operator generator error. Uncatched failure from operator',
        'E016' => 'Operator is down',
        'E018' => 'Complaint already taken',
        'else' => 'Technical glitch. Please try after some time',
);
