<?php

return array(
        'WALLET_REFILLED_RETAILER'=>"Your account has been successfully refilled with Rs <AMOUNT>, Your current Pay1 balance is <BALANCE>",
        'WALLET_TOPUP_REVERSED'=>"We have successfully taken back Rs <AMOUNT> from your account. As we have previously refilled your Pay1 account by mistake. Sorry for inconvenience caused. Your current Pay1 balance is Rs <BALANCE>",	
        'WALLET_TOPUP_REVERSED_NEW'=>"We have successfully taken back Rs <AMOUNT> from your account. As we have previously refilled your Pay1 account by mistake. Sorry for inconvenience caused. Your current Pay1 balance is Rs <BALANCE>. You have <LOYALTY_BALANCE> coins in your Pay1 account.",	
        'FAILED_TRANSACTION' => "Your recharge request of <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> has been failed due to <ERRMSG>. The recharge amount has been credited back to your Pay1 balance. Your current Pay1 balance is Rs <BALANCE>.",
        'FAILED_TRANSACTION_NEW' => "Your recharge request of <NUMBER>; Operator: <OPERATOR>; Amt: <AMOUNT> has been failed due to <ERRMSG>. The recharge amount has been credited back to your Pay1 balance. Your current Pay1 balance is Rs <BALANCE>. Your awarded Pay1 coins have been withdrawn. You have <LOYALTY_BALANCE> coins in your Pay1 account.",
        'MISSCALL_SUCCESS_USER' => "Your wallet has been deducted for recharge of <UMOBILE>; Operator: <OPERATOR>; Amt: <AMOUNT>; txn ID: <TXN_ID> via missed call. Your current Pay1 balance is Rs <BALANCE>",
        'MISSCALL_SUCCESS_USER_NEW' => "Your wallet has been deducted for recharge of <UMOBILE>; Operator: <OPERATOR>; Amt: <AMOUNT>; txn ID: <TXN_ID> via missed call. Your current Pay1 balance is Rs <BALANCE>. You have <LOYALTY_BALANCE> coins in your Pay1 account.",
        'MISSCALL_FAILURE_USER_NOBAL' => "<MOBILE> is trying for recharge via missed call. Kindly update your Pay1 wallet for the recharges to happen",
        'MISSCALL_FAILURE_USER' => "<UMOBILE> tried for recharge via missed call but failed due to <ERRMSG>",
        'MISSCALL_FAILURE_USER_NEW' => "<UMOBILE> tried for recharge via missed call but failed due to <ERRMSG>Your awarded Pay1 coins have been withdrawn. You have <LOYALTY_BALANCE> coins in your Pay1 account.",
        'FREEBIE_FAILURE' => "Due to failure of your transaction you are not eligible for your free gift.",
        'INSUFFICIENT_COINS'=>"Insufficient balance You need <AMOUNT> coins to pocket this gift.",
        'NEW_USER'=>"You've earned <COIN> Gift Coins for signing up with pay1.",
        'B2C_BY_RETAILER_RECHARGE_SUCC'=>"Wow! <LOYALTY_COINS>  more gift coins, you have got now <LOYALTY_BALANCE> Pay1 gift coins. A free coffee is waiting for you.",
        'B2C_BY_RETAILER_RECHARGE_FAIL'=>"Oops Something went wrong! <LOYALTY_COINS> Gift coins reversed due to last txn failure. Please try again.",
   //'FREEBIE_SUCCESS' => "You have successfully grabbed your Free gift <FB_NAME>. Your Free gift code is <CODE>. Please use your free gift on or before <EXPIRY>.",
    );