    <?php

include_once 'setting.php';

return array(
        'pay1' => array(
                'Recharge_URL' => 'https://'.$pay1_recharge_host.'/apis/receiveApi?partner_id=ACCOUNT_ID&operation_type=1&operator=OP_CODE&number=NUMBER&amount=AMT&trans_id=TRANS_ID&special=STV&hash_code=HASH&mobile=MOBILE',
                'Complain_URL'=>'https://'.$pay1_recharge_host.'/apis/receiveApi?partner_id=ACCOUNT_ID&operation_type=4&trans_id=TRANS_ID&hash_code=HASH',
                'KEY' => $pay1_key,
                'ACCOUNT_ID' => $pay1_account_id,
                'CHECK_STATUS_URL' => 'https://'.$pay1_recharge_host.'/apis/receiveApi?partner_id=ACCOUNT_ID&operation_type=3&trans_id=TRANS_ID&hash_code=HASH',
        ),
        'smstadka'=>array(
                'URL'=>'http://www.smstadka.com/redis/insertInQsms?mobile=MOBILE&sms=MESSAGE&root=ROUTE',
                'ROUTE'=>'tata',
        ),
        'payment_gateway'=> array(
            'payu_india'=>array(
                    'KEY'=> $b2c_payu_key,
                    'SALT'=> $b2c_payu_salt,
                    'SUBMIT_URL'=>$b2c_payu_url,
                    //'B2C_SUBMIT_URL'=>$b2c_submit_url,
                    'SUCCESS_URL'=>'https://'.$pay_response_host.'/index.php/api_new/action/api/true/actiontype/payu_update/page/success',
                    'FAILURE_URL'=>'https://'.$pay_response_host.'/index.php/api_new/action/api/true/actiontype/payu_update/page/failure',
                    'TIMEOUT_URL'=>'https://'.$pay_response_host.'/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout',
                    'CANCEL_URL'=>'https://'.$pay_response_host.'/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel',
                    'SERVICE_URL'=>$b2c_payu_service_url,
                    'PAYMENT_URL'=>$pay1_Payment_Url,
            ),
        ),
        'google_notif'=>array(
                'GCM_URL'=>'https://android.googleapis.com/gcm/send',
                'GOOGLE_API_KEY'=>$GOOGLE_API_KEY,
        ),
);
