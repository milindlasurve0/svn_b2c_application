<?php
return array();
/*return array(
      'EMAIL_TEMPLATE_HEADER'=>"<html xmlns='http://www.w3.org/1999/xhtml'><head>
                                <title>Pay1 Emailer</title>
                                <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                                <meta http-equiv='pragma' content='no-cache'>
                                <meta http-equiv='cache-control' content='no-cache'>
                                <meta http-equiv='expires' content='0'>
                                <style type='text/css'></style></head>
                                <body background='#fff'>
                                <br/><br/>
                                <table align='center' border='0' cellpadding='0' cellspacing='0' height='auto' style='max-width:600px;font-family:Arial, Helvetica, sans-serif; font-size:14px;border:1px solid #d6d6d6;border-radius:2px;'>
                                    <tbody><tr>
                                        <td>
                                            <div style='background-image:url('http://b2c.pay1.in/images/bg1.png');max-width:600px;border-radius:2px;'>
                                                    <table align='center' style='background:#503c97; width:100%;' cellpadding='10px'>
                                                        <tbody><tr>
                                                            <td>
                                                                <img src='http://b2c.pay1.in/images/logo.png' style='height:35px;'>
                                                            </td>
                                                            <td style='vertical-align: bottom;padding-bottom: 15px;'>
                                                                <a href='http://pay1.in/' target='_blank'><img alt='' src='http://b2c.pay1.in/images/link.png' style='height:12px;' align='right'></a>
                                                            </td>
                                                        </tr> 
                                                    </tbody></table>
                                                    <table align='center' width='100%' style='font-familyArial, Helvetica, sans-serif; padding:45px;'>
                                                        <tbody><tr>
                                                            <td>Dear User,<br/><br/>",
        
        'EMAIL_TEMPLATE_FOOTER'=>"<br/><br/><p style='margin:2px'>Regards</p>
                                            <p style='margin:0;padding-bottom:5px;'><b>Team Pay1</b></p>	
                                            <p style='margin:0;padding-bottom:50px;'>Support - Email: listen@pay1.in</p>
                                            </td><td>
                                        </td></tr>
                                    </tbody></table>
                                    <table align='center' style='background: #fff; width:100%;color:#aaa;font-size:10px;font-familyArial, Helvetica, sans-serif;'>
                                        <tbody><tr>
                                            <p align='center'>
                                            <a href='http://shop.pay1.in/tandc' style='color:inherit' target='_blank'> Terms &amp; Conditions</a> | 
                                            <a href='http://shop.pay1.in/cnrp' style='color:inherit' target='_blank'> Cancellation &amp; Return Policy</a> | 
                                            <a href='http://shop.pay1.in/privacy' style='color:inherit' target='_blank'>Privacy Policy</a> | 
                                            <a href='http://shop.pay1.in/faq' style='color:inherit' target='_blank'>FAQ</a>
                                            <br>
                                            726, Raheja's Metroplex (IJMIMA), Link Road, Malad (W), Mumbai - 400 064. India</p>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                        </div>			
                                    </td>
                                </tr>
                            </tbody></table>
                            </body></html>",
        
      'SUCCESS_REGISTRATION'=>"Congratulations!! Your registration with Pay1 is successful.<br/>
                                Your registered Pay1 Mobile no is <MOBILE>.<br/><br/>
                                Thank you for choosing Pay1.<br/><br/>
                                You can top-up your Pay1 account at a nearby Pay1 outlet or using online payment options like net 
                                banking, credit card etc.<br/>
                                <!-- <br/>To locate a near by Pay1 outlet (Presently only available in Mumbai) <a href=''>Click here</a>.<br/><br/>
                                For online topup, you can <a href=''>click here</a>--><br/><br/>
                                Pay1 also offers you exciting deals and offers near you. So don’t forget to grab vouchers for your 
                                next meal out or a fantastic hair cut.<br/><!--<br/>
                                <a href=''>Checkout Pay1 Deals now</a>.-->",
              
	  'WALLET_REFILLED_ONLINE' => "(Pay1 Mobile No.: <MOBILE>)<br/><br/>
                                    Your Pay1 topup request of Rs <AMOUNT> is successful. Your updated Pay1 Balance is Rs <BALANCE>.<br/><br/>
                                    In your <Credit Card/Bank> statement this entry will be visible with the name as “PAYU”.<br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone.<br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
      'WALLET_REFILLED_VOUCHER' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                    Your Pay1 top-up of Rs <AMOUNT> is successful. Your updated Pay1 Balance is Rs <BALANCE>.<br/><br/>
                                    Voucher Serial no. : <TXN_ID><br/>
                                    Voucher MRP: Rs 500/-<br/>
                                    Top-up Amount : Rs 525/-<br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone. <br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
      'WALLET_REFILLED_RETAILER' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                    Your Pay1 top-up of Rs <AMOUNT> is successful. Your updated Pay1 Balance is Rs <BALANCE>.<br/><br/>
                                    Pay1 Merchant Name : <SHOP_NAME><br/>
                                    Pay1 Merchant No. : <RETAILER_MOBILE><br/>
                                    Date : <DATE><br/>
                                    Time : <TIME> Hours<br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone. <br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
      'RECHARGE_SUCCESS' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                    Your Pay1 recharge request of Rs <AMOUNT> of <OPERATOR0> is successfully submitted to the <br/>
                                    operator. You will get it (the benefit) shortly. Your Current Pay1 Balance is Rs. <BALANCE>.<br/><br/>
                                    Mobile/Subscriber ID : <NUMBER><br/>
                                    Operator : <OPERATOR><br/>
                                    Amount : <AMOUNT><br/>
                                    Transaction ID : <TXN_ID><br/>
                                    Date : <DATE><br/>
                                    Time : <TIME> Hours<br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone.<br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
	  
      'RECHARGE_FAILURE' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                Your Pay1 recharge request with the Transaction ID <TXN_ID> of Rs <AMOUNT> of <OPERATOR> 
                                is failed due to <ERRMSG>. The amount has been credited back to your Pay1 Balance/Wallet. Your
                                Current Pay1 Balance is Rs. <BALANCE>.<br/><br/>
                                Mobile/Subscriber ID : <NUMBER><br/>
                                Operator : <OPERATOR><br/>
                                Amount : <AMOUNT><br/>
                                Transaction ID : <TXN_ID><br/>
                                Recharge Status : Failed<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                Pay1 new cool mobile app is also available for your phone. <br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'BILLPAYMENT_SUCCESS0' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                Your Pay1 Bill Payment request of Rs <AMOUNT> of <OPERATOR0> is successfully submitted to the <br/>
                                <OPERATOR>. The request will take minimum 24 hours to reflect in your account. Your Current Pay1 
                                Balance is Rs. <AMOUNT>.<br/><br/>
                                Mobile No. : <MOBILE><br/>
                                Operator : <OPERATOR><br/>
                                Amount : <AMOUNT><br/>
                                Service Charge : <S_AMOUNT><br/>
                                Transaction ID : <TXN_ID><br/>
                                Date : <DATE><br/>
                                Time : <TIME> Hours<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>                                
                                Pay1 new cool mobile app is also available for your phone. <br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
	  
	  'BILLPAYMENT_SUCCESS' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                Your Pay1 Bill Payment request of Rs <AMOUNT> of <OPERATOR0> is successfully submitted to <br/>
                                the Biller. The request will take minimum 24 hours to reflect in your account. Your Current Pay1 <br/>
                                Balance is Rs. <BALANCE>.<br/><br/>
                                Account No. : <NUMBER><br/>
                                Biller : <OPERATOR><br/>
                                Amount : <AMOUNT><br/>
                                Service Charge : <S_AMOUNT><br/>
                                Transaction ID : <TXN_ID><br/>
                                Date : <DATE><br/>
                                Time : <TIME> Hours<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>                                
                                Pay1 new cool mobile app is also available for your phone. <br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'BILLPAYMENT_FAILURE' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                Your Pay1 Bill Payment request with the Transaction ID <TXN_ID> of Rs <AMOUNT> of <OPERATOR0><br/>
                                Name> is/has been failed due to <ERRMSG>. The amount has been credited back to your Pay1 <br/>
                                Balance/Wallet. Your Current Pay1 Balance is Rs. <AMOUNT>.<br/><br/>
                                Account No. : <NUMBER><br/>
                                Biller : <OPERATOR><br/>
                                Amount :<AMOUNT><br/>
                                Status : Failed<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                Pay1 new cool mobile app is also available for your phone. <br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
	  
	  'DEAL_SUCCESS'        => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                Your Pay1 Deal of Rs <AMOUNT> is / has been successfully purchased. Your Current Pay1 Balance<br/>
                                is Rs <BALANCE>.<br/><br/>
                                Merchant Name :<DEALER_NAME><br/>
                                Amount : <AMOUNT0><br/>
                                Date : <DATE><br/>
                                Time : <TIME> Hours<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                Pay1 new cool mobile app is also available for your phone. <br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'DEAL_DETAILS'        => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                You have bought a Pay1 Deal of <DEALER_NAME> of Rs. <AMOUNT0>. Enjoy with Love!!<br/><br/>
                                Merchant Name : <DEALER_NAME><br/>
                                Deal : <OFFER_NAME><br/>
                                Amount : <AMOUNT><br/>
                                Voucher Code : <VCODE><br/>
                                Valid Till : <EXPIRY><br/><br/>
                                Deal Description : <Offer Description>includes Location, Contact No.<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                Pay1 new cool mobile app is also available for your phone.<br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>
                                Like us our Facebook page: www.facebook.com/Pay1<br/><br/>",
	
	  'MISSCALL_SUCCESS_USER' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                Your Pay1 recharge request via Missed Call Service of Rs <AMOUNT> of <OPERATOR> for Mobile <br/>
                                no. <MOBILE>is successfully submitted to the operator. Rs.<AMOUNT> has been deducted from Pay1<br/>
                                Wallet. Your Current Pay1 Balance is Rs. <BALANCE>.<br/><br/>
                                Mobile No. : <UMOBILE><br/>
                                Operator : <OPERATOR><br/>
                                Amount : <AMOUNT><br/>
                                Transaction ID : <TXN_ID><br/>
                                Date : <DATE><br/>
                                Time : <TIME> Hours<br/><br/>
                                You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                Pay1 new cool mobile app is also available for your phone.<br/>
                                <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'MISSCALL_FAILURE_USER_NOBAL' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                        Your Pay1 Missed Call registered no. <MOBILE> is trying to recharge for Rs. <AMOUNT>via Missed <br/>
                                        Call service. Instantly refill your Pay1 Wallet to avoid the inconvenience. Your Current Pay1<br/>
                                        Balance is Rs. <BALANCE>.<br/><br/>
                                        Mobile No. : <MOBILE><br/>
                                        Amount : <AMOUNT><br/>
                                        Requested Date : <DATE><br/>
                                        Requested Time : <TIME> Hours<br/><br/>                                        
                                        You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                        Pay1 new cool mobile app is also available for your phone.<br/>
                                        <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'MISSCALL_FAILURE_USER'    => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                    Your Pay1 recharge request via Missed Call Service of Rs <Amount> of <OPERATOR> for Mobile no.<br/>
                                    <MOBILE>is/has been failed due to <ERRMSG>. The amount has been credited back to your Pay1 <br/>
                                    Wallet. Your Current Pay1 Balance is Rs. <AMOUNT>.<br/><br/>
                                    Mobile No. : <MOBILE><br/>
                                    Operator : <OPERATOR><br/>
                                    Amount : <AMOUNT><br/>
                                    Recharge Status : Failed<br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone. <br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
	
      'CHANGE_PASSWORD_SUCCESS' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                    Your Pay1 password has been changed successfully.<br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone. <br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
      'COMPLAINT_MISSCALL_SUCCESS' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                        Your Pay1 complaint request of your last transaction <TXN_ID> has been successfully registered.<br/><br/>
                                        Transaction ID : <TXN_ID><br/>
                                        Complaint No. : <COMP_NO><br/><br/>
                                        You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                        Pay1 new cool mobile app is also available for your phone. <br/>
                                        <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'COMPLAINT_MISSCALL_FAILURE' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                        Your Pay1 complaint request via Missed Call Service cannot be possible at the moment due to <br/>
                                        <ERRMSG>. Sorry for the inconvenience caused. Please try again after some time else write us at <br/>
                                        listen@pay1.in<br/><br/>
                                        You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                        Pay1 new cool mobile app is also available for your phone.<br/>
                                        <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'COMPLAINT_RESOLVED_SUCCESS' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                        Your Pay1 complaint request for transaction <TXN_ID> has been successfully resolved. The <br/>
                                        Transaction was successful.<br/><br/>
                                        Transaction ID : <TXN_ID><br/>
                                        Complaint No. : <COMP_NO><br/>
                                        Status : Successful<br/><br/>
                                        Incase you have any query regarding the same please write us at listen@pay1.in<br/><br/>
                                        You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                        Pay1 new cool mobile app is also available for your phone. <br/>
                                        <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
        
	  'COMPLAINT_RESOLVED_FAILURE' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                        Your Pay1 complaint request for transaction <TXN_ID> has been successfully resolved. The <br/>
                                        Transaction was unsuccessful due to <ERRMSG>.<br/><br/>                                        
                                        Transaction ID : <TXN_ID><br/>
                                        Complaint No. : <COMP_NO><br/>
                                        Status : Failed<br/><br/>
                                        Incase you have any query regarding the same please write us at listen@pay1.in<br/><br/>
                                        You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                        Pay1 new cool mobile app is also available for your phone. <br/>
                                        <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
	  
	  'CHANGE_EMAIL_ID_SUCCESS' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                                    Your Pay1 registered e-mail ID has been changed/updated successfully to <EMAIL>. From now-<br/>
                                    onwards you will receive all the alerts & notifications on <EMAIL><br/><br/>
                                    You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                                    Pay1 new cool mobile app is also available for your phone.<br/>
                                    <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",
      
	  'FORGOT_PASSWORD' => "(Pay1 Mobile No: <MOBILE>)<br/><br/>
                            Your New Pay1 Temporary Password is <PASSWD>. Kindly change your password once logged in.<br/><br/>
                            You can now recharge your mobile, grab some exciting deals, pay your bills.<br/><br/>
                            Pay1 new cool mobile app is also available for your phone. <br/>
                            <a href='<DOWNLOAD_URL>'>Download now</a>.<br/><br/>",        
      
);
 
 */
