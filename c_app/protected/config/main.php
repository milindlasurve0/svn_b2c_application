<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'defaultController' => 'users',
    'modules'=>require(dirname(__FILE__) . '/modules.php'),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'identityCookie' => array('name' => 'b2c.pay1.in'),
            'allowAutoLogin' => TRUE,
            'authTimeout'=>(60*60*24*7),
        ),
        'session' => array(
                'timeout' => (60*60*24*7),
                'cookieParams' => array(
                    'lifetime' => (60*60*24*7),
                     'httponly' => true,
                 ),
        ),            
        'cache' => require(dirname(__FILE__) . '/cache_config.php'),
            
        'curl' => array(
            'class' => 'ext.Curl',
        ),
            
        'db' => require(dirname(__FILE__) . '/dbconfig.php'),
            
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
            
        'urlManager' => require(dirname(__FILE__) . '/urlmanager.php'),
            
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'rotateByCopy' => 1,
                    'logPath' => '/var/log/apps/',
                    'logFile' => 'b2c_application_' . date('Ymd') . '.log'
                ),
            // uncomment the following to show log messages on web pages

            /* 		array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
        'ePdf' => array(
        'class'         => 'ext.yii-pdf.EYiiPdf',
        'params'        => array(
            'mpdf'     => array(
                'librarySourcePath' => 'application.vendor.MPDF56.*',
                'constants'         => array(
                    '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                ),
                'class'=>'mpdf',
                )
            )
        ),
        'request'=>array(
            //'enableCsrfValidation'=>true,
            'enableCookieValidation'=>true,
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => require(dirname(__FILE__) . '/params.php'),
    
);
