<?php

// this contains the application parameters that can be maintained via GUI
//$baseUrl = "http://www.b2capp.local/index.php/";//Yii::app()->baseUrl."/index.php/";
//$baseUrl = "http://cdev.pay1.in/index.php/";
include_once 'setting.php';

$baseUrl = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://'.$_SERVER['SERVER_NAME'].'/' : 'http://'.$_SERVER['SERVER_NAME'].'/index.php/';
$apiSubPart = "api_new/action/api/true/actiontype/";
return array(
       
    //'URL' => 'http://localhost/stub/teststuff.php?partner_id=ACCOUNT_ID&operation_type=1&operator=OP_CODE&number=NUMBER&amount=AMT&trans_id=TRANS_ID&special=STV&hash_code=HASH',
    '_ROLE_ADMIN' => 1,
    '_ROLE_GENERAL' => 2,
    '_ROLE_DEALER' => 3,
    '_ROLE_REPORT' => 4,
    '_DEAL_STATUS' => array(
        1=>"OPEN",
        2=>"CLOSED",
        3=>"EXPIRED"        
    ),
    '_OFFER_VALIDITY_MODES' => array(
        1=>"Stock",
        2=>"Expiry Date", 
        3=>'Both'
    ),
    '_DEAL_OFFER_STATUS' => array(
        1=>"OPEN",
        2=>"CLOSED",
        3=>"EXPIRED"        
    ),
    '_DEAL_LOCATION_STATUS' => array(
        1=>"OPEN",
        2=>"CLOSED"        
    ),
    '_DEAL_INFO_STATUS' => array(
        1=>"SHOW",
        2=>"HIDE",
        3=>"CLOSE"        
    ),
    '_DEAL_COUPON_STATUS' => array(
        1=>"ACTIVE",
        2=>"REDEEMED",
        3=>"EXPIRED"        
    ),
    '_BASE_URL'=>$baseUrl,
    '_URLS' => array(
        "API"=>array(
            "GET_Cities_By_StateId"=>$baseUrl.$apiSubPart."Cities_By_StateId/",
            "Get_deal_offers"=>$baseUrl.$apiSubPart."Get_deal_offers/",
            "Get_Payu_Token"=>$baseUrl.$apiSubPart."Get_Payu_Token/"
        ),
        "CONTROLLER"=>array(
            "createOffer"=>$baseUrl."deals/createOffer",
            "createLocation"=>$baseUrl."deals/createLocation",
            "blockUser"=>$baseUrl."Panel/blockUser",
            "unblockUser"=>$baseUrl."Panel/unblockUser",
            "redeem_Freebie"=>$baseUrl."Panel/redeem_Freebie",
            "redeem_Coupon"=>$baseUrl."Panel/redeem_Coupon",
            "add_Deal_Type"=>$baseUrl."deals/addDealType",
        ),
        "MODAL"=>array(
            "editOffer"=>$baseUrl."modals/deals/editOffer",
            
        ),
                
    ),
    '_VOUCHER_TYPE' => array(
     // 0 => "DISCOUNT",
      1 => "PROMO",
      2 => "WALLET",     
    ),
    '_VOUCHER_STATUS' => array(
      0 => "OPEN",
      1 => "INPROCESS",
      2 => "CLOSED",     
    ),
    '_TXN_STATUS'=>array(
       0 => "Received",
       1 => "Inprocess",
       2 => "Complete",
       3 => "Failed",
       4 => "Refunded",
       5 => "Pulledback",
       7 => "refund to bank",
    ),    
    '_VOUCHER_DRY_STATE'=>array(
        0=>FALSE,
        1=>TRUE,
     ),
    'MAX_WALLET_BALANCE_ALLOWED'=>5000,    
    '_IMG_DIR'=>   'images/',
    '_WEB_IMG_DIR'=>   'web_images/',
    'NEW_WIDTH'=> 450,
    'LIVE_BASE_URL' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://'.$_SERVER['SERVER_NAME'].'/' : 'http://'.$_SERVER['SERVER_NAME'].'/', //stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://'.$_SERVER['SERVER_NAME'].'/' : 'http://'.$_SERVER['SERVER_NAME'].'/',
    'WALLET_API_KEY'=>'Daugi4b2oo',
    'API_BASE_URL'=>$baseUrl.$apiSubPart,
    '_DAILY_TRANSACTION_LIMIT' => 10,
    '_DAILY_BILLINGTRANSACTION_LIMIT' => 5,
    '_DAILY_AMOUNT_LIMIT' => 10000,
    '_MONTHLY_RECHARGE_LIMIT' => 30,
    '_MONTHLY_BILLING_LIMIT' => 10,
    'B2C_SUPPORT_MISSED_CALL_NUMBER' => '02267242266',
    '_NOTIFIER_Q'  => $notifierQ,    
    'redis_default_database'=>$redis_database,
    'SMS_VENDOR_FLAG'=> 1, //1-smstadka | 2-247sms
    'NEW_WIDTH'=>450,
    '_Loyalty_Points'=> array('1'=>1, //for mobile
                        '2'=>1,       //for dth
                        '3'=>1,       //for data
                        '4'=>1,       //for postpaid
                        '5'=>1,       //for billing
                        '6'=>1,),    //for voucher deal
    '_Offer_Reviews'=> array('1'=>'IT\'S BAD',
                             '2'=>'POOR',
                             '3'=>'JUST OK!',
                             '4'=>'LIKE IT!',
                             '5'=>'LOVE IT!'),
    '_New_User_LoyaltyPoints'=>100,
    '_New_User_Title'=>'CONGRATULATIONS',
//    'xoxo_voucher_user'=>"test@perk.com",
//    'xoxo_voucher_password'=>"Qt26dghd",
    'xoxo_voucher_user'=>"Pay1.in", //live credential
    'xoxo_voucher_password'=>"3jhg3iug5u34",
);
