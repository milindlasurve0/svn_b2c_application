<?php

class VendorComponent {

    public static function callPay1($api_param = array()) {
        Yii::app()->session['apiCalled'] = "";
        $pay1_Api_detail = Yii::app()->params['vendorApi']['pay1'];
        $api_param['hash'] = sha1($pay1_Api_detail['ACCOUNT_ID'].$api_param['trans_id'].$pay1_Api_detail['KEY']);
        $api = $pay1_Api_detail['Recharge_URL'];
        $pay1_product_detail = GeneralComponent::getProductDetails($api_param['product'],$api_param['operator']);
        if($pay1_product_detail===0){
            return array('status' => 'failure', 'errCode' => '243', 'description' =>'Invalid operator');
        }
        $api_param['operator'] = ( $api_param['stv']==0)? $pay1_product_detail['flexi']['pay1']:(!isset($pay1_product_detail['voucher']['pay1']) || (isset($pay1_product_detail['voucher']['pay1'])) && $pay1_product_detail['voucher']['pay1']=='')?$pay1_product_detail['flexi']['pay1']:$pay1_product_detail['voucher']['pay1'];
        $api = GeneralComponent::replaceword('ACCOUNT_ID', $pay1_Api_detail['ACCOUNT_ID'], $api);
        $api = GeneralComponent::replaceword('OP_CODE', $api_param['operator'], $api);
        $api = GeneralComponent::replaceword('NUMBER', $api_param['number'], $api);
        $api = GeneralComponent::replaceword('AMT', $api_param['amount'], $api);
        $api = GeneralComponent::replaceword('TRANS_ID', $api_param['trans_id'], $api);
        $api = GeneralComponent::replaceword('HASH', $api_param['hash'], $api);
        $api = GeneralComponent::replaceword('STV', $api_param['stv'], $api);
        $api = GeneralComponent::replaceword('MOBILE', $api_param['mobile'], $api);
        Yii::app()->session['apiCalled'] = $api;
        return GeneralComponent::curl($api);
        //return true;
    }
    
    public static function callsmstadka($api_param = array()) {
        Yii::app()->session['apiCalled'] = "";
        $pay1_Api_detail = Yii::app()->params['vendorApi']['smstadka'];
        $api = $pay1_Api_detail['URL'];
        $api = GeneralComponent::replaceword('ROUTE', $pay1_Api_detail['ROUTE'], $api);
        $api = GeneralComponent::replaceword('MOBILE', $api_param['msisdn'], $api);
        $api = GeneralComponent::replaceword('MESSAGE', $api_param['message'], $api);
        Yii::app()->session['apiCalled'] = $api;
        return GeneralComponent::curl($api);
    }    
    
    public static function callPay1_complain($api_param = array()) {
        Yii::app()->session['apiCalled'] = "";
        $pay1_Api_detail = Yii::app()->params['vendorApi']['pay1'];
        $api_param['hash'] = sha1($pay1_Api_detail['ACCOUNT_ID'].$api_param['trans_id'].$pay1_Api_detail['KEY']);
        $api = $pay1_Api_detail['Complain_URL'];
        $api = GeneralComponent::replaceword('ACCOUNT_ID', $pay1_Api_detail['ACCOUNT_ID'], $api);
        $api = GeneralComponent::replaceword('TRANS_ID', $api_param['trans_id'], $api);
        $api = GeneralComponent::replaceword('HASH', $api_param['hash'], $api);
        Yii::app()->session['apiCalled'] = $api;
        return GeneralComponent::curl($api);
    }

    public static function get_circle_by_mobile($data){
        $circle_api = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getMobileDetails&mobile=".$data['umobile'];
        return $response = trim(trim(trim(GeneralComponent::curl($circle_api),"("),";"),")");
    }
    
    public static function get_pay1_transaction_status($api_param){
        $pay1_Api_detail = Yii::app()->params['vendorApi']['pay1'];
        $api = $pay1_Api_detail['CHECK_STATUS_URL'];
        $api = GeneralComponent::replaceword('ACCOUNT_ID', $pay1_Api_detail['ACCOUNT_ID'], $api);        
        $api_param['hash'] = sha1($pay1_Api_detail['ACCOUNT_ID'].$api_param['trans_id'].$pay1_Api_detail['KEY']);
        $api = GeneralComponent::replaceword('TRANS_ID', $api_param['trans_id'], $api);
        $api = GeneralComponent::replaceword('HASH', $api_param['hash'], $api);
        return GeneralComponent::curl($api);
    }
}
