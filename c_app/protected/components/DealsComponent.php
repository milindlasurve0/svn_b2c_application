<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Deals Component
 *
 * @author Vinay Rathore
 */

class DealsComponent {
   
    public static function getDealsList($dealerId = null,$offerCond = null) {
       $date = GeneralComponent::getFormatedDate();
       $dealerIdQryPart = empty($dealerId) ? "1" : " dealer_id = $dealerId";
       $offerCondPart = !empty($offerCond) ? "  AND (`offers`.validity_mode = 2 AND `offers`.validity > '$date') OR (offers.total_stock <  stock_sold AND `offers`.validity_mode = 1) " : "";
       $query="SELECT deals.id,deals.img_url, deals.name, deals.category_id, deals.dealer_id, deals.dealer_id, deals.created, deals.status, categories.name AS category_name, users.user_name AS user_name, user_profile.name AS user_profile_name, min(offers.validity) AS deal_validity
                                                            FROM 
                                                                        `deals` 
                                                            LEFT JOIN   `categories`    ON `deals`.category_id = `categories`.id
                                                            LEFT JOIN   `users`         ON  `deals`.dealer_id = `users`.id
                                                            LEFT JOIN   `user_profile`  ON  `users`.id = `user_profile`.user_id
                                                            LEFT JOIN   `offers`  ON  (`deals`.id = `offers`.deal_id  AND `offers`.status = 1  $offerCondPart )                                                            
                                                            WHERE deals.status=1  AND                                                   
                                                            $dealerIdQryPart 
                                                            group by deals.id ORDER BY deals.created DESC";
       try{
            $dealsList = Yii::app()->db->createCommand($query)->queryAll();
       }catch(Exception $ex){
           return array();
       }
        return isset($dealsList) ? $dealsList : array();
    }
    
    public static function getDealsList2($dealerId = null,$offerCond = null,$showcond = null) {
       $date = GeneralComponent::getFormatedDate();
      // $showcond = (!empty($showcond) && $showcond=='Open') ? "AND ((`offers`.validity_mode = 2 AND `offers`.status = 1) OR (`offers`.validity_mode = 1 AND offers.total_stock >  stock_sold)) AND deals.status=1 " : " AND ((`offers`.validity_mode = 2 AND `offers`.status != 1) OR (`offers`.validity_mode = 1 AND offers.total_stock <=  stock_sold) OR deals.status!=1 ) ";
        $showcond = (!empty($showcond) && $showcond=='Open') ? " AND deals.status=1 " : " AND deals.status!=1 ";
       if(empty($showcond)){
           $showcond = "";
       }
       $dealerIdQryPart = empty($dealerId) ? "1" : " AND dealer_id = $dealerId";
      // $offerCondPart = empty($offerCond) ? " AND `offers`.status = 1  AND `offers`.validity_mode = 2" : " AND offers.total_stock <  stock_sold AND `offers`.validity_mode = 1";
       $query="SELECT deals.id,deals.img_url,deal_url, deals.name, offers.name as offer_name,offers.total_stock,offers.stock_sold,offers.validity as off_validity,
                      deals.category_id, deals.dealer_id, deals.created, deals.status, categories.name AS category_name, 
                      users.dealer_name AS user_name, user_profile.name AS user_profile_name, min(offers.validity) AS deal_validity,
                        (CASE WHEN `offers`.validity_mode = 2 AND offers.validity < '$date' THEN 'Expire' 
                              WHEN `offers`.validity_mode = 1 AND offers.total_stock <=  stock_sold AND offers.total_stock!=-1 Then 'Stock out' 
                              WHEN offers.status =3 THEN 'Expire'
                              ELSE '' END ) as stock_flag,deals.type_id
                        FROM 
                                    `deals` 
                        LEFT JOIN   `categories`    ON `deals`.category_id = `categories`.id
                        LEFT JOIN   `users`         ON  `deals`.dealer_id = `users`.id
                        LEFT JOIN   `user_profile`  ON  `users`.id = `user_profile`.user_id
                        LEFT JOIN   `offers`  ON  (`deals`.id = `offers`.deal_id AND offers.sequence=1)
                        WHERE                                                            
                        $dealerIdQryPart $showcond
                        group by deals.id ORDER BY deals.created DESC";
       try{
            $dealsList = Yii::app()->db->createCommand($query)->queryAll();
       }catch(Exception $ex){
           return array();
       }
        return isset($dealsList) ? $dealsList : array();
    }
    
    public static function getAllDeals() {
        $query="SELECT d.id, d.name,d.img_url "
                . " FROM deals as d INNER JOIN"
                . " offers as off ON (off.deal_id = d.id) "
                . " WHERE d.status =1 AND off.status=1 AND off.sequence=1"
                . " ORDER BY d.name";
        try{ 
            $dealsList = Yii::app()->db->createCommand($query)->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($dealsList) ? $dealsList : array();
    }
    
    public static function getDealCategoriesList() {
        try{
            $list = Yii::app()->db->createCommand("SELECT categories.id,categories.name,parent.id as parent_id, parent.name as parent_name 
                                                FROM `categories` , `categories` as parent 
                                                WHERE categories.parent_id = parent.id AND categories.id != 1 AND categories.isActive = 1 AND categories.order != 0
                                                ORDER BY categories.name;")->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    
    /*Selecting types ID and Name from deal_types table */
    public static function getDealTypesList() {
        try{
            $list = Yii::app()->db->createCommand("SELECT deal_types.id,deal_types.name 
                                                FROM deal_types 
                                                ORDER BY deal_types.name;")->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    /*End the DealTypesList function
      return :content array list or blank array     */ 
    
    /*Selecting Coupon types ID and Name from deal_coupon_types table */
    public static function getDealCouponTypesList() {
        try{
            $list = Yii::app()->db->createCommand("SELECT deal_coupon_types.id,deal_coupon_types.name 
                                                FROM deal_coupon_types 
                                                ORDER BY deal_coupon_types.id;")->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    /*End the DealCouponTypesList function
      return :content array list or blank array */ 
    
    /*Selecting Coupon types ID and Name from deals table */
    public static function getDealCouponTypes($dealId) {
        try{
            $list = Yii::app()->db->createCommand("SELECT `coupon_type` FROM deals WHERE  `id` = $dealId")->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    /*End the DealCouponTypes function
      return : content array list or blank array */
     
    /*Selecting Gift Voucher ID and Name from Gift Voucher Name table */
    public static function getDealGiftVoucher($dealId) {
        try{
            $list = Yii::app()->db->createCommand("SELECT by_voucher FROM  `deals` WHERE  id = $dealId")->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    /*End the getgiftVoucherList function
      return : content array list or blank array */
    
    public static function getDealOffersList($dealId,$offerId=null,$validOffer = null) {
        $dealIdQry = "1";
        $offerIdQry = "";
        
        if(!empty($offerId)){
            $offerIdQry = " AND id = $offerId";
        }
        if(!empty($dealId)){
            $dealIdQry = " `deal_id` = $dealId ";
        }
         if(!empty($validOffer)){
            $offerIdQry = " AND status = 1 AND mail_status !=2";
        }
        
        try{
             $list = Yii::app()->db->createCommand("SELECT *
                                                FROM `offers`
                                                WHERE 
                                                     $dealIdQry
                                                     $offerIdQry
                                                ORDER BY id desc;")->queryAll();
        }catch(Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    
    public static function getDealName($dealId) {
        if(!empty($dealId)){
            $dealIdQry = " `id` = $dealId ";
        }
        try{
            $list = Yii::app()->db->createCommand("SELECT name
                                                FROM `deals`
                                                WHERE 
                                                     $dealIdQry
                                                ORDER BY id desc;")->queryAll();
        }  catch (Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    public static function getDealOffersInfoList($dealId) {
        $dealIdQry = "1";
        
        if(!empty($dealId)){
            $dealIdQry = " `deal_id` = $dealId ";
        }
        
        try{
            $list = Yii::app()->db->createCommand("SELECT *
                                                FROM `offer_details`
                                                WHERE 
                                                     $dealIdQry
                                                ORDER BY id desc;")->queryAll();
        }catch(Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    
    public static function getDealLocationsList($dealId,$locationId=null) {
        $dealIdQry = "1";
        $locationIdQry = "";
        
        if(!empty($locationId)){
            $locationIdQry = " AND `deal_locations`.id = $locationId";
        }
        if(!empty($dealId)){
            $dealIdQry = " `deal_id` = $dealId ";
        }
        
        try{
            $list = Yii::app()->db->createCommand("SELECT  `deal_locations`.id,`deal_locations`.deal_id, `deal_locations`.lat, `deal_locations`.lng,`deal_locations`.address, `deal_locations`.status, `deal_locations`.updated, `deal_locations`.created,
                                                       `locator_city`.id as city_id , `locator_city`.name as city_name,
                                                       `locator_state`.id as state_id , `locator_state`.name as state_name,`deal_locations`.full_address
                                                FROM 
                                                            `deal_locations`
                                                LEFT JOIN   `locator_city`  ON `deal_locations`.city_id = `locator_city`.id
                                                LEFT JOIN   `locator_state` ON `locator_state`.id = `locator_city`.state_id
                                                WHERE 
                                                     $dealIdQry
                                                     $locationIdQry
                                                ORDER BY id desc;")->queryAll();
        }catch(Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    
    public static function getDealMapList($dealId,$locationId=null) {
        $dealIdQry = "1";
        $locationIdQry = "";
        
        if(!empty($mapId)){
            $locationIdQry = " AND `deal_locations`.id = $locationId";
        }
        if(!empty($dealId)){
            $dealIdQry = " `deal_id` = $dealId ";
        }
        try{
            $list = Yii::app()->db->createCommand("SELECT  `deal_locations`.*,
                                                       `locator_area`.id as area_id , `locator_area`.name as area_name,
                                                       `locator_city`.id as city_id , `locator_city`.name as city_name,
                                                       `locator_state`.id as state_id , `locator_state`.name as state_name
                                                FROM 
                                                            `deal_locations`
                                                LEFT JOIN   `locator_area`  ON `deal_locations`.area_id = `locator_area`.id         
                                                LEFT JOIN   `locator_city`  ON `deal_locations`.city_id = `locator_city`.id
                                                LEFT JOIN   `locator_state` ON `locator_state`.id = `locator_city`.state_id
                                                WHERE 
                                                     $dealIdQry
                                                     $locationIdQry
                                                ORDER BY id desc;")->queryAll();
             }catch(Exception $ex){
                return array();
            }
        return isset($list) ? $list : array();
    }
    
    public static function getDealLocationMapList($locationId) {
        $locationIdQry = "`deal_locations`.id = $locationId";
        try{
            $list = Yii::app()->db->createCommand("SELECT  `deal_locations`.*,
                                                       `locator_area`.id as area_id , `locator_area`.name as area_name,
                                                       `locator_city`.id as city_id , `locator_city`.name as city_name,
                                                       `locator_state`.id as state_id , `locator_state`.name as state_name
                                                FROM 
                                                            `deal_locations`
                                                LEFT JOIN   `locator_area`  ON `deal_locations`.area_id = `locator_area`.id         
                                                LEFT JOIN   `locator_city`  ON `deal_locations`.city_id = `locator_city`.id
                                                LEFT JOIN   `locator_state` ON `locator_state`.id = `locator_city`.state_id
                                                WHERE 
                                                   $locationIdQry
                                                ORDER BY id desc;")->queryAll();
         }catch(Exception $ex){
            return array();
        }
        return isset($list) ? $list : array();
    }
    
    public static function updateDealLocationMapList($data,$deal_location_data) {
        $result = array();
       try {
           
           $state_id        =   $deal_location_data[0]['state_id'];
           $city_id         =   $deal_location_data[0]['city_id'];
           $deal_locationID =   $deal_location_data[0]['id'];
           $new_state_name  = $data['state'];
           $new_city_name   = $data['city'];
           $new_area_name   = $data['loc_area'] ;
           $new_shop = $data['shop'];
           $new_address = $data['near_loc'];
           $new_full_address = $data['gadres'];
//           $dealer_contact = $data['dealer_contact'];
           if(isset($data['contacts']) && !empty($data['contacts'])){
            $ful_contacts = $data['dealer_contact'].','.implode(",",$data['contacts']);//Collect all the Dealer Contacts
            $dealer_contact = $ful_contacts;
            }else{
            $dealer_contact = $data['dealer_contact'];
            }
           $lat_lng = substr( $data['lat'], 0, strpos( $data['lat'] , '.' ) +2 )."_".substr( $data['lng'], 0, strpos( $data['lng'] , '.' ) +2 );
           
        /*   if($data['near_loc'] == ' '){
            $new_address = $data['route'];
           }else{
            $new_address = $data['near_loc']." , ".$data['route']; 
           }
        */ 
        //If New_State_Name and Old_State_Name are NOT same   
            if($deal_location_data[0]['state_name'] != $data['state']){
               $getStateID = Yii::app()->db->createCommand("SELECT id from locator_state WHERE name = '$new_state_name'")->queryAll();
                //If getStateID is empty i.e. New_Sate_Name is not Available in locator_state Table then Insret into that table    
                    if(count($getStateID)==0){
                        $new_state_insert_qry = Yii::app()->db->createCommand("INSERT into locator_state (name,toShow) values ('$new_state_name',1); ")->execute();
                        $New_StateID = Yii::app()->db->getLastInsertID();//getting new state_id of last insert New_State_Name
                        $result['$New_StateID'] = $New_StateID;
                    }else{
                        $New_StateID = $getStateID[0]['id'];
                        $result['$New_StateID'] = $New_StateID;
                    }  
            }//If New_City_Name and Old_City_Name are NOT same     
           if($deal_location_data[0]['city_name'] != $data['city']){
                $getCityID = Yii::app()->db->createCommand("SELECT id from locator_city WHERE state_id = $state_id AND name = '$new_city_name' ")->queryAll();
            //If getCityID is empty i.e. New_City_Name is not Available in locator_city Table then Insret into that table    
                if(count($getCityID)==0){
                    $new_city_insert_qry = Yii::app()->db->createCommand("INSERT into locator_city (state_id,name,toShow) values ($state_id,'$new_city_name',1); ")->execute();
                    $New_CityID = Yii::app()->db->getLastInsertID();//getting new area_id of last insert New_Area_Name
                    $result['$New_CityID'] = $New_CityID;
                }else{
                    $New_CityID = $getCityID[0]['id'];    
                    $result['$New_CityID'] = $New_CityID;
                }
            //Update City_ID in deal_location Table corresponding to it deal_location Id     
                $cityID_update_qry = Yii::app()->db->createCommand("UPDATE deal_locations SET city_id = $New_CityID , "
                              . "lat = ".$data['lat']." , lng = ".$data['lng'].", shop = '$new_shop' ,dealer_contact='$dealer_contact' WHERE id = $deal_locationID")->execute();
            }//If New_Area_Name and Old_Area_Name are NOT same   
            if($deal_location_data[0]['area_name'] != $data['loc_area']){
                $getAreaID = Yii::app()->db->createCommand("SELECT id from locator_area WHERE city_id = $city_id AND name = '$new_area_name' ")->queryAll();
            //If getAreaID is empty i.e. New_Area_Name is not Available in locator_area Table then Insret into that table    
               if(count($getAreaID)==0){
                    $new_area_insert_qry = Yii::app()->db->createCommand("INSERT into locator_area (city_id,name,toShow) values ($city_id,'$new_area_name',1); ")->execute();
                    $New_AreaID = Yii::app()->db->getLastInsertID();//getting new area_id of last insert New_Area_Name
                    $result['$New_AreaID'] = $New_AreaID;
                }else{
                    $New_AreaID = $getAreaID[0]['id'];//getting new area_id
                    $result['$New_AreaID'] = $New_AreaID;
                }
           //Update Area_ID in deal_location Table corresponding to it deal_location Id     
               $areaID_update_qry = Yii::app()->db->createCommand("UPDATE deal_locations SET area_id = $New_AreaID , "
                        . "lat = ".$data['lat']." , lng = ".$data['lng'].", shop = '$new_shop' ,dealer_contact='$dealer_contact' WHERE id = $deal_locationID AND city_id = $city_id")->execute();
            }//If New_Address and Old_Address are NOT same  
           if($deal_location_data[0]['address'] != $new_address){
            $address_update_qry = Yii::app()->db->createCommand("UPDATE deal_locations SET address = '$new_address' ,"
                      . "lat = ".$data['lat']." , lng = ".$data['lng'].", shop = '$new_shop' ,dealer_contact='$dealer_contact' WHERE id = $deal_locationID ")->execute();
            }
           /**If New_lat and Old_lat are NOT same  OR */ 
           /**If New_Full Address and Old Full Address are NOT same */ 
           if(($deal_location_data[0]['full_address'] != $data['gadres']) || ($deal_location_data[0]['lat'] != $data['lat'])){
             $update_lat_lng = Yii::app()->db->createCommand("UPDATE deal_locations SET lat = ".$data['lat']." , lng = ".$data['lng']." , address = '$new_address' , full_address = '$new_full_address', lat_lng = '".$lat_lng."',shop = '$new_shop' ,dealer_contact='$dealer_contact' WHERE id = $deal_locationID ")->execute();
        }
        if(($deal_location_data[0]['shop'] != $new_shop) ){
             $update_shop = Yii::app()->db->createCommand("UPDATE deal_locations SET shop = '$new_shop' WHERE id = $deal_locationID ")->execute();
        }
        if(($deal_location_data[0]['dealer_contact'] != $dealer_contact) ){
             $update_dealer_contact = Yii::app()->db->createCommand("UPDATE deal_locations SET dealer_contact='$dealer_contact' WHERE id = $deal_locationID ")->execute();
        } 
        $query = "SELECT deal_id FROM deal_locations WHERE id=".$deal_locationID;
        $data = Yii::app()->db->createCommand($query)->queryAll();
        if(isset($data[0]['deal_id']) && !empty($data[0]['deal_id'])){
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$data[0]['deal_id']));
            $offer->updated = date("Y-m-d H:i:s");
            $offer->save();
        }
        $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Info updated successfully .');
           
        }catch (Exception $ex) {
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' =>$message);
        }
        return $result;    

    }
    public static function getDealInfosList($dealId,$infoId=null) {
        $dealIdQry = "1";
        $infoIdQry = "";
        
        if(!empty($infoId)){
           $infoIdQry = " AND id = $infoId";
        }
        if(!empty($dealId)){
            $dealIdQry = " `deal_id` = $dealId ";
        }
        try{
            $list = Yii::app()->db->createCommand("SELECT  `deal_info`.id,`deal_info`.deal_id,`deal_info`.title, `deal_info`.content_txt, `deal_info`.sequence ,`deal_info`.status                                                      
                                                FROM   `deal_info`
                                                WHERE 
                                                     $dealIdQry
                                                     $infoIdQry
                                                ORDER BY id;")->queryAll();
           }catch(Exception $ex){
             return array();
            }
        return isset($list) ? $list : array();
    }
    
    public static function createDeal($data) {
        try {
            $data['by_voucher']=0;
            $query = "SELECT dealer from dealvoucher_api as d LEFT JOIN users as u ON (u.id=d.dealer) WHERE u.role=3 AND d.dealer= ".$data['dealer_id'];
            $dealerForVoucher = Yii::app()->db->createCommand($query)->queryRow();
            if(isset($dealerForVoucher['dealer']) && !empty($dealerForVoucher['dealer'])){
                $data['by_voucher']=1;
            }
            $deal = new Deal();
            $deal->name = $data['name'];
            $deal->by_voucher = $data['by_voucher'];
            $deal->category_id = $data['category_id'];
            $deal->dealer_id = $data['dealer_id'];
            $deal->status = $data['status'];
            $deal->created = date("Y-m-d H:i:s");
            //$deal->updated = date("Y-m-d H:i:s");
            $deal->img_url = isset($data['img_url'])?$data['img_url']:$deal->img_url;
            if(!$deal->save()){
                throw new Exception("Deal creation error .",422); 
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Deal created successfully .','model'=>$deal);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
    public static function createOffer($data) {
            $result = array();
        try {
            $offer = new Offers();
            
            $offer->deal_id = $data['offer_deal_id'];
            $offer->name = $data['offer_name'];
            $offer->offer_price = $data['offer_offer_price'];
            $offer->actual_price = $data['offer_actual_price'];
            try{
                if($data['offer_actual_price'] < 1){
                    $offer->discount = 0;
                }else{
                    $offer->discount = ceil((($data['offer_actual_price'] - $data['offer_offer_price'])/$data['offer_actual_price'])*100);
                }
            }catch(Exception $e){
                $offer->discount = 0;
            }
            $offer->total_stock   = $data['offer_total_stock'];
           // $offer->isGenuineUser   = $data['offer_IsGenuineUser'];
            $offer->sequence      = $data['offer_sequence'];
            $offer->img_url       = isset($data['offer_img_url']) ? $data['offer_img_url'] : "" ;
            $offer->stock_sold    = 0;
            $offer->validity      = $data['offer_validity'];
            $offer->validity_mode = $data['offer_validity_mode'];
            $offer->status        = $data['offer_status'];
            $offer->short_desc    = $data['offer_short_desc'];
            $offer->long_desc     = $data['offer_long_desc'];
            $offer->offer_desc    = $data['offer_of_desc']; 
            $offer->created       = date("Y-m-d H:i:s");
            $offer->updated       = date("Y-m-d H:i:s");
            $offer->min_amount    = isset($data['offer_min_amount']) ? $data['offer_min_amount'] : 0 ; //Offer min amount
//            $offer->amount        = isset($data['offer_amount']) ? $data['offer_amount'] : 0 ; //Offer Amount
            $offer->max_limit_peruser  = $data['offer_max_limit_peruser'];
            $offer->global_flag        = ($data['offer_global_flag']=='') ? 0 :$data['offer_global_flag'];
            $offer->order_flag         = $data['offer_order_flag'];
            $offer->coupon_expiry_date = isset($data['coupon_expiry_date']) ? $data['coupon_expiry_date'] : $data['offer_validity'] ; //Coupon Expiry Date
        
            if(!$offer->save()){
                //$errror = $offer->getErrors();
                throw new Exception("Offer creation error .",422); 
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Offer created successfully .','model'=>$offer);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
    /*
     * This function is used to CREATE new Offer Info Details using OfferDeatils.php in offer_details table 
     * @return : success or failure description (based on method)
    */
    public static function createOfferInfo($data) {
        $result = array();
        try {
            $offer_details = new OfferDetails();
            
            $offer_details->deal_id = $data['offer_info_deal_id'];
            $offer_details->time = $data['full_time'];
            $offer_details->day = $data['Week_days'];
            $offer_details->appointment = $data['Appoinment_Option'];
            $offer_details->exclusive_gender = $data['Gender'];
            $offer_details->dealer_contact = $data['Dealer_Contact'];
            if(!$offer_details->save()){
                throw new Exception("Offer Info creation error .",422); 
            }
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['offer_info_deal_id']));
            $offer->updated = GeneralComponent::getFormatedDate();
            $offer->save();
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Offer Info Details created successfully .','model'=>$offer_details);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' =>$message);
        }   
        return $result;
    }
    /*End of create Offer Info */    
    
    public static function editOffer($data) {
        $result = array();
        try {
            $offer = Offers::model()->findByPk($data['offer_id']);
            if(is_null($offer)) {
                throw new Exception("Offer not fount .",422); 
            } else {
                $offer->deal_id = $data['offer_deal_id'];
                $offer->name = $data['offer_name'];
                $offer->offer_price = $data['offer_offer_price'];
                $offer->actual_price = $data['offer_actual_price'];
                try{
                    if($data['offer_actual_price'] < 1){
                        $offer->discount = 0;
                    }else{
                        $offer->discount = ceil((($data['offer_actual_price'] - $data['offer_offer_price'])/$data['offer_actual_price'])*100);
                    }
                }catch(Exception $e){
                    $offer->discount = 0;
                }
                $offer->total_stock    = $data['offer_total_stock'];
               // $offer->isGenuineUser  = $data['offer_IsGenuineUser'];
                $offer->sequence       = $data['offer_sequence'];
                $offer->img_url        = isset($data['offer_img_url']) ? $data['offer_img_url'] : "" ;
//              $offer->stock_sold     = $data['offer_stock_sold'];
                $offer->validity       = $data['offer_validity'];
                $offer->validity_mode  = $data['offer_validity_mode'];
                $offer->status         = $data['offer_status'];
                $offer->short_desc     = $data['offer_short_desc'];
                $offer->long_desc      = $data['offer_long_desc'];// 
                $offer->offer_desc     = $data['offer_of_desc'];
                $offer->updated        = date("Y-m-d H:i:s");
                $offer->min_amount     = isset($data['offer_min_amount']) ? $data['offer_min_amount'] : 0 ; //Offer min amount
//                $offer->amount         = isset($data['offer_amount']) ? $data['offer_amount'] : 0 ; //Offer Amount
                $offer->max_limit_peruser  = $data['offer_max_limit_peruser'];
                $offer->global_flag        = ($data['offer_global_flag']=='') ? 0 :$data['offer_global_flag'];
                $offer->order_flag         = $data['offer_order_flag'];
                $couponType                = DealsComponent::getDealCouponTypes($data['offer_deal_id']);  //getting system generated coupon type which value = 0 
                $offer->coupon_expiry_date = ($couponType[0]['coupon_type']==0) ? $data['Coupon_expiry_date'] : $data['offer_validity'] ; //Edit Coupon Expiry Date
                if(!$offer->save()){
                    $errror = $offer->getErrors();                    
                    throw new Exception(json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Offer updated successfully .','model'=>$offer);
           }            
            
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
    /*
     * This function is used to SAVE edit Offer Info Details using OfferDeatils.php in offer_details table 
     * @return : success or error description (based on method)
    */
    public static function editOfferInfo($data) {
        $result = array();
        try {
            $offer_details = OfferDetails::model()->findByPk($data['offer_info_id']);
            if(is_null($offer_details)) {
                throw new Exception("Offer Info not fount .",422); 
            } else {
                $offer_details->deal_id = $data['offer_info_deal_id'];
                $offer_details->time = $data['offer_info_time'];
                $offer_details->day = $data['offer_info_day'];
                $offer_details->appointment = $data['offer_info_appointment'];
                $offer_details->exclusive_gender = $data['offer_info_gender'];
                $offer_details->dealer_contact = $data['offer_info_contact'];
                if(!$offer_details->save()){
                    $errror = $offer_details->getErrors();                    
                    throw new Exception(json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Offer Info Details updated successfully .','model'=>$offer_details);
           }            
            
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    /*End of Edit Offer Info */ 
    
    public static function createLocation($data) {
        //deal_id,lat,lng,address,city_id,status,updated,created
        $result = array();
        try {
                $info = new Locations();
                $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['location_deal_id']));
                $offer->updated = date("Y-m-d H:i:s");
                if(!$offer->save()){
                    $errror = $offer->getErrors();
                    throw new Exception("Info Update error :".json_encode($errror),422); 
                }
                $info->deal_id = $data['location_deal_id'];
                $info->lat = $data['location_lat'];
                $info->lng = $data['location_lng'];
                $info->address = GeneralComponent::replaceword("&nbsp;", " ", $data['location_address']);
                $info->city_id = $data['location_city'];
                $info->status = $data['location_status'];

                $info->created = date("Y-m-d H:i:s");
                $info->updated = date("Y-m-d H:i:s");
                $info->lat_lng = substr( $data['location_lat'], 0, strpos( $data['location_lat'] , '.' ) +2 )."_".substr( $data['location_lng'], 0, strpos( $data['location_lng'] , '.' ) +2 );
                if(!$info->save()){
                    $errror = $info->getErrors();
                    throw new Exception("Info creation error :".json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Info updated successfully .','model'=>$info);
           
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
    public static function createMapLocation($data){
        $result = array();
        try {
                $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['map_deal_id']));
                $offer->updated = date("Y-m-d H:i:s");
        //                if(!$offer->save()){
        //                    $errror = $offer->getErrors();
        //                    throw new Exception("Info Update error :".json_encode($errror),422); 
        //                }
                $deal_id = $data['map_deal_id'];
                $created = date("Y-m-d H:i:s");
                $updated = date("Y-m-d H:i:s");
                $connection = Yii::app()->db;
                $lat = $data['lat'];
                $lng = $data['lng'];
                $shop = $data['shop'];  
                $near_loc = $data['near_loc'];
                $area =  $data['loc_area'];
                $full_address =  $data['gadres'];
                $city =  $data['city'];
                $state = $data['state'];
                $country = $data['country'];
                $pincode = $data['pin_code'];
                
                if(isset($data['contacts']) && !empty($data['contacts'])){
                $ful_contacts = $data['dealer_contact'].','.implode(",",$data['contacts']);//Collect all the Dealer Contacts
                $dealer_contact = $ful_contacts;
                }else{
                $dealer_contact = $data['dealer_contact'];
                }
//                if($near_loc == ' ' || empty($near_loc)){
//                  $address = $route;  //Full Address with route when near location is not available;   
//                }else{
//                  $address = $near_loc." , ".$route;//Full Address with near_location and route;
//                }
               
                $address = $near_loc;//Full Address with near_location and route; 
                
                
                $lat_lng = substr( $lat, 0, strpos( $lat , '.' ) +2 )."_".substr( $lng, 0, strpos( $lng , '.' ) +2 );

        //Start State_Query_Code
                $state_qry = "Select id , name  from locator_state where name like '".$state."' ";
                $result_s = $connection->createCommand($state_qry)->queryAll();
                if(!empty($result_s)){
                $stateID = $result_s[0]['id'];
                }
                else{
            /* @var $state_insert_qry type */
                $state_insert_qry = "Insert into locator_state (name , toShow) values ('$state' , 1);";
                $result_state = $connection->createCommand($state_insert_qry)->execute();
                /*Get last insert State ID */
                $stateID = $connection->getLastInsertID();
                }    
                $result['stateID'] = $stateID;
                $result['stateName'] = $state;
        //End State_Query_Code

        //Start City_Query_Code        
                $city_qry = "Select id , name from locator_city where name like '".$city."' AND state_id = $stateID;";
                $result_c = $connection->createCommand($city_qry)->queryAll();
        
        //If City_ID is not available then insert city in locator_city otherwise select area from locator_area    
                if(!empty($result_c[0]['id'])){
                $cityID = $result_c[0]['id'];
                $result['cityID'] = $result_c[0]['id'];
                $result['cityName'] = $result_c[0]['name'];
                    
                $area_qry = "Select id , name from locator_area where name like '".$area."' AND city_id = $cityID;";
                $result_a = $connection->createCommand($area_qry)->queryAll();
                }else{
                $city_insert_qry = "Insert into locator_city (state_id , name , toShow) values ($stateID , '$city' , 1);";
                $result_city = $connection->createCommand($city_insert_qry)->execute();
                /*Get last insert State ID */
                $cityID = $connection->getLastInsertID();
                
                }
        //End City_Query_Code        
        
        //Start Area City_Query_Code            
            //If Area_ID is not available then insert area in locator_area otherwise insert area in deal_locations       
                if(empty($result_a[0]['id'])){
                  $loc_area_insert_qry = "Insert into locator_area (city_id , name , toShow) values ($cityID , '$area' , 1);";
                  $result_area = $connection->createCommand($loc_area_insert_qry)->execute();
                 /*Get last insert State ID */
                  $areaID = $connection->getLastInsertID();
                
                }else{
                $areaID = $result_a[0]['id'];    
                $deal_insert_qry = "Insert into deal_locations (deal_id , lat , lng , shop , address , full_address , area_id , city_id , status , updated , created , lat_lng,dealer_contact )"
                        . " values ( $deal_id , $lat , $lng , '$shop' , '$address' , '$full_address', $areaID , $cityID , 1 ,'$updated', '$created', '$lat_lng','$dealer_contact' );";   
                $result_deal = $connection->createCommand($deal_insert_qry)->execute();
                $result['areaID'] = $result_a[0]['id'];
                $result['areaName'] = $result_a[0]['name'];
                }
                $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['map_deal_id']));
                $offer->updated = date("Y-m-d H:i:s");
                $offer->save();
        //End Area_Query_Code
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Map Info located successfully .');
        }catch (Exception $ex) {
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
    public static function editLocation($data) {
        $result = array();
        try {
            $info = Locations::model()->findByPk($data['edit_location_id']);
            if(is_null($info)) {
                throw new Exception("Offer not fount .",422); 
            } else {
                $info->deal_id = $data['edit_location_deal_id'];
                $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['edit_location_deal_id']));
                $offer->updated = date("Y-m-d H:i:s");
                if(!$offer->save()){
                    $errror = $offer->getErrors();
                    throw new Exception("Info Update error :".json_encode($errror),422); 
                }
                $info->lat = $data['edit_location_lat'];
                $info->lng = $data['edit_location_lng'];
                $info->address = GeneralComponent::replaceword("&nbsp;", " ", $data['edit_location_address']);
                $info->city_id = $data['edit_location_city'];
                $info->status = $data['edit_location_status'];
                $info->lat_lng = substr( $data['edit_location_lat'], 0, strpos( $data['edit_location_lat'] , '.' ) +2 )."_".substr( $data['edit_location_lng'], 0, strpos( $data['edit_location_lng'] , '.' ) +2 );
                $info->created = date("Y-m-d H:i:s");
                $info->updated = date("Y-m-d H:i:s");

                if(!$info->save()){
                    $errror = $info->getErrors();                    
                    throw new Exception(json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Location updated successfully .','model'=>$info);
           }            
            
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    public static function createInfo($data) {
        //deal_id,lat,lng,address,city_id,status,updated,created
        $result = array();
        try {
            $info = new DealInfo();
            
            $info->deal_id = $data['info_deal_id'];
            $info->title = GeneralComponent::replaceword("&nbsp;", " ", $data['info_title']);
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['info_deal_id']));
            if(empty($offer))
            {
            echo '<br><strong>-----    ----------         Sorry !!          ----------     -----  <br>';
            echo '<strong>Offer Not Available ,Please add Offer before adding New Deal Section.</strong>';die;
            }
            $offer->updated = date("Y-m-d H:i:s");
            if(!$offer->save()){
                $errror = $offer->getErrors();
                throw new Exception("Info Update error :".json_encode($errror),422); 
            }
            $info->sequence = $data['info_sequence'];
            $info->status = $data['info_status'];
            $info->content_txt = GeneralComponent::replaceword("&nbsp;", " ", $data['info_context_txt']);
            	 
            //$info->created = date("Y-m-d H:i:s");
            //$info->updated = date("Y-m-d H:i:s");
            
            if(!$info->save()){
                $errror = $info->getErrors();
                throw new Exception("Info creation error :".json_encode($errror),422); 
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Info created successfully .','model'=>$info);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
    public static function editInfo($data) {
        //deal_id,lat,lng,address,city_id,status,updated,created
        $result = array();
        try {
            $info = DealInfo::model()->findByPk($data['edit_info_id']);
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['edit_info_deal_id']));
            $offer->updated = date("Y-m-d H:i:s");
            if(!$offer->save()){
                $errror = $offer->getErrors();
                throw new Exception("Info Update error :".json_encode($errror),422); 
            }
            $info->deal_id = $data['edit_info_deal_id'];
            $info->title = GeneralComponent::replaceword("&nbsp;", " ", $data['edit_info_title']);
            $info->sequence = $data['edit_info_sequence'];
            $info->status = $data['edit_info_status'];
            $info->content_txt = GeneralComponent::replaceword("&nbsp;", " ", $data['edit_info_context_txt']);
            	 
            //$info->created = date("Y-m-d H:i:s");
            //$info->updated = date("Y-m-d H:i:s");
            
            if(!$info->save()){
                $errror = $info->getErrors();
                throw new Exception("Info Update error :".json_encode($errror),422); 
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Info created successfully .','model'=>$info);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }
    
   /*
    public static function editImageCrop($data) {
       
        $result = array();
        try {
            $crop_img = DealsImageTest::model()->findByPk($data['crop_img_id']);
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['deal_id']));
            $offer->updated = date("Y-m-d H:i:s");
            if(!$offer->save()){
                $errror = $offer->getErrors();
                throw new Exception("Crop Image Update error :".json_encode($errror),422); 
            }
            $crop_img->deal_id = $data['deal_id'];
            $crop_img->image_url = $data['image_url'];
            $crop_img->image_info = $data['image_info'];
            $crop_img->uploaded_date = $data['uploaded_date'];
            $crop_img->status = $data['status'];
            	 
            //$info->created = date("Y-m-d H:i:s");
            //$info->updated = date("Y-m-d H:i:s");
            
            if(!$crop_img->save()){
                $errror = $crop_img->getErrors();
                throw new Exception("Crop_img Update error :".json_encode($errror),422); 
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Crop Image updated successfully .','model'=>$crop_img);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
    }
    */
    public static function getDeals($data = null){
        try{
            $maxdistance = 5;
            $data['latitude'] = ($data['latitude']==="null" || $data['latitude']=="")?1:$data['latitude'];
            $data['longitude'] = ($data['longitude']==="null" || $data['longitude']=="")?1:$data['longitude'];
            $lat = deg2rad($data['latitude']);
            $lng = deg2rad($data['longitude']);
            $dod_cond = "";
            if(isset($data['dod']) && $data['dod']==TRUE ){
                $dod_cond =" AND d.order=0 ";
            }
            $eradius = 6371;
            if(isset($data['distance']) && $data['distance']>0){
                $dod_cond .=" AND (acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius)<=".$data['distance'];
            }
            
            //$deallist_qry = "SELECT d.id, d.name as dealname, df.content_txt, c.name as category,c.id as category_id,dl.lat as latitude,dl.lng as longitude,"
            $deallist_qry = "SELECT d.id,of.id as offer_id,area.name as area,d.deal_url, d.name as dealname, "
                                . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url , of.offer_price, c.name as category,c.id as category_id,"
                                //. "group_concat(dl.lat separator ',') as latitude,group_concat(dl.lng separator ',') as longitude,"
                                . "dl.lat as latitude, dl.lng as longitude,"
                                . "(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance, "
                                . "of.actual_price, of.discount,of.total_stock,of.offer_desc,of.name as offer_name,"
                                . "CASE WHEN d.deal_status = 0 THEN of.total_stock ELSE (SELECT SUM(total_stock) from offers where deal_id = d.id) END as total_stock,"
                                . "CASE WHEN d.deal_status = 0 THEN of.stock_sold ELSE (SELECT SUM(stock_sold) from offers where deal_id = d.id) END as stock_sold,"
                                . " (CASE WHEN length(dm.image_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',dm.image_url) 
                                    ELSE (
                                    CASE d.category_id WHEN 2 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_beauty.png' )
                                    WHEN 12 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fashion.png' )
                                    WHEN 7 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_services.png' )
                                    WHEN 15 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_other.png' )
                                    WHEN 14 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_learning.png' )
                                    WHEN 11 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fitness.png' )
                                    WHEN 13 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_entertainment.png' )
                                    WHEN 16 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_online.png' )
                                    WHEN 10 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_food.png')  ELSE '' END ) END) as logo_url"
                                . " FROM deals as d "
                                . "LEFT JOIN offers as of ON d.id = of.deal_id "
                                . " LEFT JOIN deals_image_section as dm ON (d.id=dm.deal_id)"
                                . "LEFT JOIN categories as c ON d.category_id = c.id "
                                . "LEFT JOIN deal_locations dl ON d.id=dl.deal_id "
                                . "LEFT JOIN locator_area area ON area.id=dl.area_id "
                                . "WHERE of.sequence = '1' AND d.status=1 AND (total_stock > stock_sold || total_stock=-1) " 
                                . $dod_cond
                                . " GROUP BY 1 "
                                . "ORDER BY d.order asc, distance asc";
            $deals = Yii::app()->db->createCommand($deallist_qry)->queryAll();
            if(count($deals) < 1){ $deals['id']=0; }
            $stock_details = array();
            unset($deals['id']);
            $result = array('status'=>'success','errCode'=>0,'description'=>GeneralComponent::removeSlashes(array_merge($deals,$stock_details)));
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    public static function getDealWebImg($data){ 
      try{
          $id = isset($data['id'])?$data['id']:0;
          $base_url = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'];
          $dealImg_qry = "SELECT concat('".$base_url."',d.img_url) as S_img , 
                                 concat('".$base_url."',d.L_img_url) as L_img , 
                                 concat('".$base_url."',di.image_url) as logo_url 
                          FROM `deals` as d
                          LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id )
                          WHERE d.id = '".$id."'";
          $dealImgUrl = Yii::app()->db->createCommand($dealImg_qry)->queryAll();
          $result = array('status'=>'success','errCode'=>0,
                  'description'=>array('image_list'=>$dealImgUrl));
      } catch (Exception $ex) {
          $message = Yii::app()->params['message_template']['EXCEPTION'];
          $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
      return $result;
    }
    
    public static function getSimilarWebDeal($data){ 
        try{
            $id = isset($data['id'])?$data['id']:0;
            $base_url = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'];
            $SimilarDeal_qry = "SELECT id,name,category_id,
                            concat('".$base_url."',img_url) as S_img
                            from deals where category_id = (SELECT category_id FROM `deals` WHERE id = '".$id."') "
                          . "AND id != '".$id."' AND status=1  LIMIT 0,3";
            
            $SimilarDeal = Yii::app()->db->createCommand($SimilarDeal_qry)->queryAll();
            $result = array('status'=>'success','errCode'=>0,
                    'description'=>array('SimilarDeal'=>$SimilarDeal));
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
          }
        return $result;
     }
        
    public static function getDetailedDeal($data){
        try{
            $id = isset($data['id'])?$data['id']:0;$maxdistance = 5;
            $data['latitude'] = ($data['latitude']==="null" || $data['latitude'] == "")?1:$data['latitude'];
            $data['longitude'] = ($data['longitude']==="null" || $data['longitude'] == "")?1:$data['longitude'];
            $lat = deg2rad($data['latitude']);
            $lng = deg2rad($data['longitude']);
            $eradius = 6371;
            $maxdistance = 5;
            //$dealsinfo_qry = "select * from deal_info where deal_id='".$id."' and status !=0";
            $connection = Yii::app()->db;
            //$connection->createCommand("SET SESSION group_concat_max_len = 1000000");
            //$dealsinfo_qry = "select REPLACE(group_concat(content_txt,'|<>|'),'|<>|,','') as content_txt from deal_info where deal_id='".$id."' and status !=0 group by deal_id";
            $dealsinfo_qry = "select content_txt from deal_info where deal_id='".$id."' and status !=0 order by sequence asc";
            $dealsinfo1 = $connection->createCommand($dealsinfo_qry)->queryAll();
            $content = "";
            foreach ($dealsinfo1 as $k=>$v){
                $content .= html_entity_decode($v['content_txt']);
            }
            $dealsinfo = array(array('content_txt'=>$content));
            $dealsoffer_qry = "select * from offers where deal_id='".$id."' and status !=0 and sequence =1";
            $dealsoffer = Yii::app()->db->createCommand($dealsoffer_qry)->queryAll();
            $deal_image_list = self::get_gallery_list($id);
            foreach ($deal_image_list as $k=>$v){
                unset($deal_image_list[$k]['deal_id'],$deal_image_list[$k]['uploaded_date'],$deal_image_list[$k]['status']);
            }
            $dealslocation_qry = "select dl.deal_id, dl.lat, dl.lng, dl.address, lc.name as city,ls.name as state,dl.full_address, "
                            ."(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance "
                            . "FROM deal_locations as dl "
                            . "LEFT JOIN locator_city as lc ON dl.city_id = lc.id "
                            . "LEFT JOIN locator_state as ls ON lc.state_id=ls.id "
                            . "WHERE dl.deal_id='".$id."' and dl.status !=0 "
                                //. "AND (acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) <= $maxdistance "
                                    . "ORDER BY distance asc";
            $dealslocation = Yii::app()->db->createCommand($dealslocation_qry)->queryAll();
            $result = array('status'=>'success','errCode'=>0,
                    'description'=>array('deal_detail'=>  (GeneralComponent::replaceword("&nbsp;", " ", GeneralComponent::removeSlashes($dealsinfo))),
                                        'offer_detail'=>  (GeneralComponent::replaceword("&nbsp;", " ",GeneralComponent::removeSlashes($dealsoffer))),
                                        'image_list'=>$deal_image_list,
                                        'location_detail'=>  GeneralComponent::replaceword("&nbsp", "&nbsp;",GeneralComponent::removeSlashes($dealslocation))));
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    public static function getformateddealdetails($str){ 
        foreach($str as $k=>$v){ 
            if(is_array($v)){ //if element is an array

                return self::getformateddealdetails($v);
            }
            else {
               // if(!strpos($str,"\'"))
                $str[$k]= html_entity_decode(htmlentities(str_replace("\"","'",(trim($v)))));
                }
            }
            return $str;
        }
   
    public static function getClaimedFreegift($data=null){
        try{
            $userid="";
            if(isset($data['user_mobile'])){
                $profileObj = new UserProfile();
                $profileObj = $profileObj->findByAttributes(array('mobile'=>$data['user_mobile']));
                $userid = $profileObj['user_id'];
            }
            else if(isset(Yii::app()->user->id)){
                $userid = Yii::app()->user->id;
            }

                if(!empty($userid)){
    //            $query="SELECT group_concat(DISTINCT offers.id) as offer
    //                    FROM coupons AS c
    //                    LEFT JOIN offers ON c.offer_id = offers.id
    //                    WHERE c.users_id =$userid";
                $query="SELECT group_concat( offers.id) as offer
                        FROM coupons AS c
                        LEFT JOIN offers ON c.offer_id = offers.id
                        WHERE c.users_id =$userid";
                $connection = Yii::app()->db;
                $freeGift = $connection->createCommand($query)->queryAll();
                if(count($freeGift)>0 && !empty($freeGift[0]['offer'])){
                    return $freeGift[0]['offer'];
                }
            }
            else{
                return "";
            }
            return "";
        }catch(Exception $ex){
            return "";
        }
    }
    
    public static function getExpiredFreegift($data=null){
        try{
            $userid="";
            if(isset($data['user_mobile'])){
                $profileObj = new UserProfile();
                $profileObj = $profileObj->findByAttributes(array('mobile'=>$data['user_mobile']));
                $userid = $profileObj['user_id'];
            }
            else if(isset(Yii::app()->user->id)){
                $userid = Yii::app()->user->id;
            }
            $updatedTime = "";
             if(isset($data['updatedTime'])){
                 if(empty($data['updatedTime'])){
                     $data['updatedTime'] = '2014-01-01';
                 }
                $updatedTime= " AND of.updated > '".$data['updatedTime']."'";
             }
            if(isset($userid) && !empty($userid)){
    //         $query="SELECT group_concat(DISTINCT of.id) as offer
    //                    FROM offers AS of LEFT JOIN
    //                    deals as d on (d.id=of.deal_id)
    //                    WHERE (of.status = 3 OR of.stock_sold>=of.total_stock OR  of.total_stock=-1 OR d.status=2) $updatedTime ";
             $query="SELECT group_concat( of.id) as offer
                        FROM offers AS of LEFT JOIN
                        deals as d on (d.id=of.deal_id)
                        WHERE (of.status = 3 OR of.stock_sold>=of.total_stock OR  of.total_stock=-1 OR d.status=2) $updatedTime ";
                $connection = Yii::app()->db;
                $freeGift = $connection->createCommand($query)->queryAll();
                if(count($freeGift)>0 && !empty($freeGift[0]['offer'])){
                    return $freeGift[0]['offer'];
                }
            }
            return "";
        }
        catch(Exception $ex){
            return "";
        }
     }
    
    public static function checkInkArray($val,$arr){
        $i=0;
        foreach($arr as $value){
            if($value['id']==$val){
                return $i;
            }
            $i++;
        }
        return false;
    }
    
    public static function getupdatedDeal_bk($data){
        if(isset($data['latitude']) && isset($data['longitude']) && isset($data['updatedOffers'])){
        $connection = Yii::app()->db;
        $eradius = 6371;
        $amount = isset($data['amount']) ? trim($data['amount']):1;
        $data['mobile'] = isset($data['mobile']) ?trim($data['mobile']):1;
         if(!isset($data['mobile'])){
            $userid = Yii::app()->user->id;
            $prf = UsersComponent::getuserProfile($userid, 0);
            $data['mobile'] = $prf['mobile'];
         }
        $maxdistance = 5;
        $data['latitude'] = ($data['latitude']==="null" || $data['latitude']=="") ? 1:$data['latitude'];
        $data['longitude'] = ($data['longitude']==="null" || $data['longitude']=="") ? 1:$data['longitude'];
        $lat = deg2rad($data['latitude']);
        $lng = deg2rad($data['longitude']);
         $dod_cond = "";
            if(isset($data['dod']) && $data['dod']==TRUE ){
                $dod_cond =" AND d.order=0 ";
            }
            
            $deallist_qry = "SELECT of.id,of.name,of.updated,of.total_stock,of.stock_sold,of.status,of.min_amount,dl.full_address,"
                            . "dl.lat as latitude, dl.lng as longitude,of.global_flag,of.order_flag,"
                            . "(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance"
                            . "  FROM offers as of "
                            . " LEFT JOIN deal_locations as dl ON ( of.deal_id = dl.deal_id ) "
                            . " GROUP BY 1 "
                            . " ORDER BY CAST((distance/5) AS UNSIGNED) asc,"
                            . " (CASE WHEN of.min_amount<=$amount THEN -1 ELSE 0 END) asc,of.min_amount desc";
                $deals = Yii::app()->db->createCommand($deallist_qry)->queryAll();
                $dealData = array();
                $dealArray = array();
                
                $requestData = array();
                $updatedOffers = json_decode($data['updatedOffers'],true);
                if(!empty($updatedOffers)){
                    $requestData = $updatedOffers;
                }
                $j=0;
                if(count($deals) > 0){
                foreach($deals as $row){
                   $dealData[$j] = $row;
                    $key = self::checkInkArray($row['id'],$requestData);
                    //$key = array_search($row['id'],$requestData);
                   if($key!==false){
                       if($dealData[$j]['status']==3 || ($dealData[$j]['total_stock']<= $dealData[$j]['stock_sold'])){
                           $dealData[$j]['updated'] ='Expired';
                       }
                       else if($dealData[$j]['updated']>$requestData[$key]['updated']){
                           $dealData[$j]['updated'] ='Updated';
                       }
                       else if($dealData[$j]['updated']==$requestData[$key]['updated']){
                           $dealData[$j]['updated'] ='Same';
                       }
                       
                   }
                   else{
                       $dealData[$j]['updated'] = 'New';
                   }
                   $j++;
                }
            }
             $userClaimedGift = self::getClaimedFreegift();
             $dealArray['offer_details'] = $dealData;
             $dealArray['claimedGifts'] = $userClaimedGift;
             
            return $dealArray;
        }
       else{
           return "";
       }
    }
    
    public static function getupdatedDeal($data){
        if(isset($data['latitude']) && isset($data['longitude'])){
        $connection = Yii::app()->db;
        $eradius = 6371;
        $orderbyCond = '';
        $start = 0;
        $end =  $data['displayLimit'];
        $limitcond = "";
        if(!isset($data['next'])){
            $data['next']=0;
        }
        $orderbyCond = " ,FLOOR(distance/5) ASC";
        if(isset($data['amount'])){
            $amount = $data['amount'];
            $orderbyCond .= " ,(CASE WHEN of.min_amount<=$amount THEN -1 ELSE 0 END) asc,of.min_amount desc";
            
        }
        else{
             $orderbyCond .= " ,of.min_amount desc";
        }
        if(isset($data['next']) && !empty($data['next']) && $data['next']!=-1){
            $start = ($data['next'] * 20);
        }
        
        $limitcond =  "LIMIT $start,$end ";
        
        if(isset($data['next']) && $data['next']==-1){
            $limitcond = "";
        }
        $maxdistance = 5;
        $data['latitude'] = ($data['latitude']==="null" || $data['latitude']=="") ? 1:$data['latitude'];
        $data['longitude'] = ($data['longitude']==="null" || $data['longitude']=="") ? 1:$data['longitude'];
        $lat = deg2rad($data['latitude']);
        $lng = deg2rad($data['longitude']);
        
         $dod_cond = "";
            if(isset($data['dod']) && $data['dod']==TRUE ){
                $dod_cond =" AND d.order=0 ";
            }
            $updatedTime = $data['updatedTime'];
            $deallist_qry = "SELECT d.id as deal_id,of.id,of.min_amount,"
                            . "dl.lat as latitude, dl.lng as longitude,of.global_flag,"
                            . "of.order_flag,(CASE WHEN of.order_flag =0
                                THEN 44444444
                                ELSE of.order_flag
                                END
                                ) AS flag, "
                            . "(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance"
                            . " FROM deals AS d
                                LEFT JOIN offers AS of ON ( d.id = of.deal_id )
                                INNER JOIN deal_locations AS dl ON ( d.id = dl.deal_id )
                                WHERE of.status =1
                                AND (total_stock > stock_sold || total_stock=-1)
                                AND of.sequence != '0'
                                AND d.status =1
                                
                                GROUP BY 1"
                            . " ORDER BY flag ASC"
                            . $orderbyCond
                            ." $limitcond";
            //AND of.offer_price =0
             try{
                $offers = Yii::app()->db->createCommand($deallist_qry)->queryAll();
             }catch(Exception $ex){
                 return '';
             }
                $dealArray = array();
                
                if(count($offers) > 0){
                    foreach($offers as $row){
                        $offers_id[]=$row['id'];
                    }
                    $id = implode(",",array_unique($offers_id));
                    $dealArray = $id;
                    return $dealArray ;
                }            
                else{
                    return '';
                }
           
        }
       else{
           return "";
       }
    }
    
    public static function getAllDealdetails($data){
         $updatedTime = "";
         $cond = "";
         if(isset($data['synk_flag']) && $data['synk_flag']=1){
            $off_id = $data['offer_details'];
            if(!empty($data['offer_details'])){
                $cond = " AND of.id IN ($off_id)";
            }
        }
         if(isset($data['updatedTime'])){
             if(empty($data['updatedTime'])){
                 $data['updatedTime'] = '2014-01-01 00:00:00';
             }
            $updatedTime= " AND of.updated > '".$data['updatedTime']."'";
         }
            //========================getDeals ======================================
            $connection = Yii::app()->db;
            $deallist_qry = "SELECT d.id,of.offer_desc,of.actual_price,of.offer_price, d.name as dealname,d.by_voucher,of_d.dealer_contact,dl.full_address, "
                                . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) 
                                    ELSE d.img_url END) as img_url , (CASE WHEN length(dm.image_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',dm.image_url) 
                                ELSE (
                                CASE d.category_id WHEN 2 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_beauty.png' )
                                WHEN 12 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fashion.png' )
                                WHEN 7 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_services.png' )
                                WHEN 15 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_other.png' )
                                WHEN 14 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_learning.png' )
                                WHEN 11 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fitness.png' )
                                WHEN 13 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_entertainment.png' )
                                WHEN 16 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_online.png' )
                                WHEN 10 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_food.png')  ELSE '' END ) END) as logo_url ,"
                                . " c.name as category,c.id as category_id,"
                                . "dl.lat as latitude, dl.lng as longitude,of.min_amount, la.name as area,dl.address, lc.name as city,ls.name as state,"
                                . "of.id as ofid, of.name as ofname,of.validity as validity, of.offer_desc as short"
                                . " FROM deals as d "
                                . "LEFT JOIN offers as of ON d.id = of.deal_id "
                                . "LEFT JOIN categories as c ON d.category_id = c.id "
                                . "LEFT JOIN deals_image_section as dm ON dm.deal_id=d.id "
                                . "LEFT JOIN deal_locations dl ON d.id=dl.deal_id "
                                . " LEFT JOIN offer_details as of_d ON of_d.deal_id =d.id "
                                . " LEFT JOIN locator_area as la ON la.id=dl.area_id "
                                . "LEFT JOIN locator_city as lc ON dl.city_id = lc.id "
                                . "LEFT JOIN locator_state as ls ON lc.state_id=ls.id "
                                . "WHERE d.status=1"
                                . " AND of.sequence = '1' AND (of.total_stock > of.stock_sold || of.total_stock=-1) AND of.status=1 $updatedTime  $cond "
                                    . " ORDER BY d.id asc";
            try{
                $deals = Yii::app()->db->createCommand($deallist_qry)->queryAll();
            }catch(Exception $ex){
                return array();
            }
            
            if(count($deals) < 1){ $deals['id']=0; }
            $dealsids = array(); //content_text related array
            $i=0;
            $dealsArray = array();
            $dealsArray2 = array();
            if(count($deals)>0){
                foreach($deals as $rows){
                    if(!empty($rows)){
                    $id = $rows['id'];
                    if(!in_array($rows['id'], $dealsids)){
                        $dealsids[] = $rows['id'];
                        $dealsArray[$id]['id'] = $rows['id'];
                        $dealsArray[$id]['name'] = $rows['dealname'];
                        $dealsArray[$id]['i_url'] = $rows['img_url'];
                        $dealsArray[$id]['logo_url'] = $rows['logo_url'];
                        $dealsArray[$id]['cat'] = $rows['category'];
                        $dealsArray[$id]['cat_id'] = $rows['category_id'];
                        $dealsArray[$id]['min'] = $rows['min_amount'];
                        $dealsArray[$id]['price'] = $rows['actual_price'];
                        $dealsArray[$id]['offer_price'] = $rows['offer_price'];
                        $dealsArray[$id]['of_id'] = $rows['ofid'];
                        $dealsArray[$id]['by_voucher'] = $rows['by_voucher'];
                        $dealsArray[$id]['of_name'] = $rows['ofname'];
                        $dealsArray[$id]['valid'] = $rows['validity'];
                        $dealsArray[$id]['desc'] = $rows['short'];
                        $dealsArray[$id]['offer_desc'] = $rows['offer_desc'];
                        
                        $dealsArray[$id]['dealer_contact'] = empty($rows['dealer_contact']) ? "" : $rows['dealer_contact'];
                        $i++;
                    }

                    $locs = array();
                    if(!empty($rows['address'])){
                        $locs['lat'] = $rows['latitude'];
                        $locs['lng'] = $rows['longitude'];
                        $locs['addr'] = $rows['address'];
                        $locs['city'] = $rows['city'];
                        $locs['state'] = $rows['state'];
                        $locs['area'] = $rows['area'];
                        $locs['full_address'] = $rows['full_address'];
                        $locs['dealer_contact'] = $rows['dealer_contact'];
                        $dealsArray[$id]['locs'][] = $locs;
                    }
                    else{
                        $dealsArray[$id]['locs'] = array();
                    }
                    
                   }
               }
               foreach($dealsArray as $row){
                   $dealsArray2[]=$row;
               }
        }
        return $dealsArray2;
    }
    
    public static function getAllDealData($data){
         $data['mobile'] = isset($data['mobile']) ?trim($data['mobile']):1;
         
         if(!isset($data['mobile'])){
            $userid = Yii::app()->user->id;
            $prf = UsersComponent::getuserProfile($userid, 0);
            $data['mobile'] = $prf['mobile'];
         }
         try{            
            $dealsArray = array();
            $maxdistance = 5;
            $data['latitude'] = ($data['latitude']==="null" || $data['latitude']=="") ? 1:$data['latitude'];
            $data['longitude'] = ($data['longitude']==="null" || $data['longitude']=="") ? 1:$data['longitude'];
            $lat = deg2rad($data['latitude']);
            $lng = deg2rad($data['longitude']);
            $connection = Yii::app()->db;
            $dod_cond = "";
            if(isset($data['dod']) && $data['dod']==TRUE ){
                $dod_cond =" AND d.order=0 ";
            }
            $eradius = 6371;
            
            $deallist_qry = "SELECT d.id as deal_id, of.id as offer_id, d.name as dealname,of.offer_desc,of.created,of.updated,of.mail_status,d.status as deal_status,dl.dealer_contact,dl.full_address,"
                                . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) 
                                    ELSE d.img_url END) as img_url , (CASE WHEN length(dm.image_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',dm.image_url) 
                                ELSE (
                                CASE d.category_id WHEN 2 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_beauty.png' )
                                WHEN 12 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fashion.png' )
                                WHEN 7 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_services.png' )
                                WHEN 15 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_other.png' )
                                WHEN 14 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_learning.png' )
                                WHEN 11 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fitness.png' )
                                WHEN 13 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_entertainment.png' )
                                WHEN 16 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_online.png' )
                                WHEN 10 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_food.png')  ELSE '' END ) END) as logo_url ,"
                               . " of.offer_price, c.name as category,c.id as category_id,"
                                . "dl.lat as latitude, dl.lng as longitude, dl.address,la.name as area, lc.name as city, ls.name as state,dl.status as dl_status,"
                                . "(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance, "
                                . "of.actual_price, of.discount,of.total_stock,of.min_amount, of.total_stock as total_stock, of.stock_sold as stock_sold, "
                                . "of.name,of.offer_price,of.discount,of.validity,of.validity_mode,of.max_quantity,of.status as offer_status,of.offer_desc as 'short_desc',of.long_desc,of.sequence ,"
                                . "of.max_limit_peruser,of.global_flag,of.order_flag "
                                    . "FROM deals as d "
                                    . "LEFT JOIN offers as of ON d.id = of.deal_id "
                                    . " LEFT JOIN offer_details as of_d ON (of_d.deal_id = d.id) "
                                    . " LEFT JOIN deals_image_section as dm ON dm.deal_id = d.id "
                                    . "LEFT JOIN categories as c ON d.category_id = c.id "
                                    . "LEFT JOIN deal_locations dl ON d.id=dl.deal_id "
                                    . " LEFT JOIN locator_area as la ON la.id=dl.area_id "
                                    . "LEFT JOIN locator_city as lc ON dl.city_id = lc.id "
                                    . "LEFT JOIN locator_state as ls ON lc.state_id=ls.id "
                                    . "WHERE d.status=1 AND of.sequence=1 "                                
                                        . $dod_cond
                                        . " GROUP BY 1,2 "
                                        . " ORDER BY d.order asc,d.id asc, distance asc, of.actual_price asc";
         
            $deals = Yii::app()->db->createCommand($deallist_qry)->queryAll();
            $mcond = " ";
            if(strlen($data['mobile'])>1){
                $mcond = "as dt inner join users as u on u.id=dt.users_id where user_name='".$data['mobile']."' ";
            }
            $total_deal_qry = "SELECT  `deal_id`,`offer_id`,count(1) as cnt  FROM `deal_transaction` $mcond                                 
                                group by `deal_id`,`offer_id` ";
            $total_deal = Yii::app()->db->createCommand($total_deal_qry)->queryAll();
            $total_deal1 = array();
            foreach($total_deal as $k=>$v){
                $total_deal1[$data['mobile']][$v['deal_id']][$v['offer_id']] = $v['cnt']; 
            }
            if(count($deals) < 1){ $deals['id']=0; }
            $stock_details = array();          
            $dealslocationAll = array(); //location related array
            $dealsofferAll = array(); //offer related array
            $dealsinfoAll = array(); //content_text related array
            $i=0;
            $deal_ids = array();
            $deal_location_array = array();
            foreach ($deals as $k=>$row){
                if($row['dl_status'] == 1 && ($row['latitude'] != "" || strtoupper($row['latitude']) != 'NULL' )){
                    if(!isset($deal_location_array[$row['deal_id']])){
                        $deal_location_array[$row['deal_id']] = 1;
                    }else{
                        $deal_location_array[$row['deal_id']] += 1;
                    }
                }
            }
            
            foreach ($deals as $k=>$row){                
                $dealsoffer = array();
                $increment_FLAG = FALSE;
                $dealsArrayKey = array('id','dealname','img_url','logo_url','offer_price','category','category_id','latitude','longitude','distance',
                        'actual_price','discount','total_stock','min_amount','stock_sold','location','location_count','content_txt',
                        'offer_detail','location_detail','image_list','dealer_contact');
                
                $offersArrayKey = array('id','deal_id','name','offer_price','offer_desc','actual_price','discount','total_stock','stock_sold','validity',
                        'validity_mode','max_quantity','status','short_desc','long_desc','created','updated','mail_status','img_url','sequence',
                        'min_amount','max_limit_peruser','global_flag','order_flag','allow');
                //$row['offer_price'] == 0 &&
                if( $row['deal_status'] == 1 && $row['offer_status'] == 1 && !in_array($row['deal_id'],$deal_ids)){
                    foreach($dealsArrayKey as $k){                        
                        if($k == 'id'){
                            $dealsArray[$i][$k] = $row['deal_id'];
                        }
                        if(in_array($k,  array_keys($row))){
                            $dealsArray[$i][$k] = $row[$k];
                        }
                    }
                    $increment_FLAG = TRUE;
                    array_push($deal_ids, $row['deal_id']);
                }
                if($increment_FLAG){
                    $dealsArray[$i]['location'] = array('deal_id'=>$row['deal_id'],'lat'=>$row['latitude'],'lng'=>$row['longitude'],'address'=>$row['address'],'city'=>$row['city'],'state'=>$row['state'],'distance'=>$row['distance'],'area'=>$row['area'],'full_address'=>$row['full_address']);                
                    $dealsArray[$i]['location_detail'] = array($dealsArray[$i]['location']);
                    $current_cnt = isset($total_deal1[$data['mobile']][$row['deal_id']][$row['offer_id']])?$total_deal1[$data['mobile']][$row['deal_id']][$row['offer_id']]:0;
                    if((intval($row['max_limit_peruser']) > intval($current_cnt)) || $row['max_limit_peruser'] == '-1'){
                        $dealsArray[$i]['allow'] = 1;
                    }else{
                        $dealsArray[$i]['allow'] = 0;
                    }
                    $dealsArray[$i]['content_txt'] = '';
                    $dealsArray[$i]['image_list'] = array();
                    $dealsArray[$i]['location_count'] = isset($deal_location_array[$row['deal_id']])?$deal_location_array[$row['deal_id']]:1;
                    foreach($offersArrayKey as $k){                        
                        if($k == 'id'){
                            $dealsArray[$i]['offer_detail'][$k] = $row['offer_id'];
                        }
                        if($k == 'allow'){
                            $dealsArray[$i]['offer_detail'][$k] = $dealsArray[$i]['allow'];
                            unset($dealsArray[$i]['allow']);
                        }
                        if(in_array($k,  array_keys($row))){
                            $dealsArray[$i]['offer_detail'][$k] = $row[$k];
                        }
                    }                
                    unset($row);
                    $i++;
                }
            }
            
             $result = array('status'=>'success','errCode'=>0,'description'=>GeneralComponent::removeSlashes($dealsArray));        
         } catch (Exception $ex) {
             $message = Yii::app()->params['message_template']['EXCEPTION'];
             $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
         }
         return $result;
    }

    public static function getDealOffers($data){
            $id = isset($data['deals_id'])?$data['deals_id']:0;
            $dealsinfo_qry = "select off.id,off.name,off.offer_price,off.actual_price,off.total_stock,off.stock_sold,off.validity,off.validity_mode,off.status,off.mail_status,"
                            . "off.max_limit_peruser ,d.img_url"
                            . "  from offers as off INNER JOIN deals as d ON off.deal_id=d.id"
                            . " where off.deal_id='".$id."' and off.status !=0 and off.sequence =1";
           try{
                $dealsinfo = Yii::app()->db->createCommand($dealsinfo_qry)->queryAll();
           }catch(Exception $ex){
               return array();
           }
           return $dealsinfo;
    }
    
    public static function getUsersforannoucement($data=null){
        $condition = 1;
        $displayimg = 0;
        $message = isset($data['image_desc']) ? $data['image_desc'] :'';//message text from textbox
        if(isset($data['viewNotify']) && $data['viewNotify']==1){ //viewNotify=0 for Non-Promotional Notification and 1 for Promotional
            $displayimg = 1;
        }
        $NotifyImage = "";
        $dealName = "";
        if(isset($data['deals']) && !empty($data['deals'])){
            $image_query = "select img_url,name FROM deals where id=".$data['deals']." LIMIT 0,1";
            $image = Yii::app()->db->createCommand($image_query)->queryAll();
            $NotifyImage =  Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$image[0]['img_url'];
             $latlng_query = "select lat_lng, SUBSTRING( lat_lng, 1, INSTR( `lat_lng` , '_' ) -1 ) AS lat1, SUBSTRING( lat_lng, INSTR( `lat_lng` , '_' ) +1 ) AS lng1 FROM deal_locations WHERE deal_id =".$data['deals'];
             $latlng = Yii::app()->db->createCommand($latlng_query)->queryAll();
             $dealName = $image[0]['name'];
        }
               
        if(isset($data['criteria']) && !empty($data['criteria'])){
            $offerid = (isset($data['offers']) && !empty($data['offers'])) ? $data['offers'] : 0;
            if($data['criteria']=='Km' && (isset($data['deals'])) && !empty($data['deals'])){
                $area = $data['km_wise'];
                $eradius = 6371;
                $union = '';
                $query = '';
                $i=0;
                $kmcondition = 0.01*$data['km_wise'];
                foreach($latlng as $row){
                    //apply new formula 
                    if($i>0){$union = ' UNION ';}
                    $lat = $row['lat1'];
                    $lng = $row['lng1'];
                    $condition="";
                    if(!empty($lat)){
                        $condition = " AND ABS( ABS( $lat + $lng ) - ABS( SUBSTRING( up.latitude_longitude, 1, INSTR( up.`latitude_longitude` , '_' ) -1 ) + SUBSTRING( up.latitude_longitude, INSTR( up.`latitude_longitude` , '_' ) +1 ) ) ) <=$kmcondition";
                    }
                    //$condition = "(acos(sin($lat)*sin(radians(latitude)) + cos($lat)*cos(radians(latitude))*cos(radians(longitude)- $lng)) * $eradius) <=$area ";
                    
                    $query .= " $union "
                            . " SELECT up.user_id,up.mobile "
                            . " FROM user_profile as up INNER JOIN users as u"
                            . " ON (u.id=up.user_id) "
                            . " WHERE u.role =2 OR u.role =1 $condition ";//u.role=2 AND u.id=150
                    $i++;
                }
             }
            else if($data['criteria']=='specific_user'){
                $condition = "";
                $mobile = trim($data['mobile']);
                $condition = " u.mobile_number IN ( $mobile )";
                 $query = "select u.id as user_id,u.mobile_number as mobile FROM users as u"
                    . " INNER JOIN user_profile as up ON u.id=up.user_id "
                    . " where $condition AND (
                        u.role =2 
                        ) "; //AND (u.id=150 OR u.id=50) //u.role =2 OR u.role =1
            }
            else if($data['criteria']=='All'){
                $query=" SELECT up.user_id,u.mobile_number as mobile"
                        . " FROM user_profile as up INNER JOIN users as u"
                        . " ON (u.id=up.user_id) "
                        . " WHERE u.role =2 ";//. " WHERE u.role=2 AND u.id=131";
            }
            try{
                $result = Yii::app()->db->createCommand($query)->queryAll();
            }catch(Exception $ex){
               return false;
           }
            $user_data = array();
            if(count($result)>0){
                if(empty($message)){
                        $title = 'Get gift in every recharge';
                    }
                    else{
                        $title = 'Pay1 Notification';
                    }
                //send announcement                
                if(isset($data['Notification']) && ($data['Notification']=='Notification')){
                    foreach($result as $row){
                        $user_data[]=$row['user_id'];
                        $userid = $row['user_id'];
                    }
                    $userids = array_chunk($user_data,900);
                    foreach($userids as $record){
                       $responce =  GeneralComponent::sendNotification($record,$title,$message,'notify',1,0,$NotifyImage,$offerid,$dealName,$displayimg); 
                    }
                }
                $user_data = array();
                if(isset($data['SMS']) && ($data['SMS']=='SMS')){
                    if(isset($data['deals']) && !empty($data['deals'])){
                        $content = Yii::app()->params['message_template']['FREEBIE_ANNOUNCEMENT'];
                        $message = GeneralComponent::replaceword('<'.'OFFER'.'>', $dealName, $content);
                    }
                    foreach($result as $row){
                        $user_data[]=$row['mobile'];
                    }
                    
                    $mobile = implode(',',array_unique($user_data));
                    GeneralComponent::sendSMS($mobile,$message);
                }   
                return 'Announcement sent successfully';
            }
            else{
                return 'There is no user with this criteria';
            }
            
        }
        else{
            return false;
        }
    }
    
    public static function getDealView($dealid){
         try{
            $deals_qry = "select * from deal where id='".$id."'";
            $deals = Yii::app()->db->createCommand($deals_qry)->queryAll();
            $dealsinfo_qry = "select * from deal_info where deal_id='".$id."' and status !=0";
            $dealsinfo = Yii::app()->db->createCommand($dealsinfo_qry)->queryAll();
            $dealsoffer_qry = "select * from offers where deal_id='".$id."' and status !=0 and sequence =1";
            $dealsoffer = Yii::app()->db->createCommand($dealsoffer_qry)->queryAll();   
         }  catch (Exception $ex){
             $deals = array();
             $dealsinfo = array();
             $dealsoffer = array();
         }
    }

    public static function add_in_image_gallery($data){
        $deal_image_gallery = new DealsImageGallery();
        if(isset($data['deal_id'])){
          $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['deal_id']));
          
          /*Checking, If Offer is not available u can't add image gallery */
          if(empty($offer))
          {
          echo '<br><strong>-----    ----------         Sorry !!          ----------     -----  <br>';
          echo '<strong>Offer Not Available ,Please add Offer before adding New Image Gallery.</strong>';die;
          }
//          $offer->updated = date("Y-m-d H:i:s");
//            if(!$offer->save()){
//                $errror = $offer->getErrors();
//                throw new Exception("Info Update error :".json_encode($errror),422); 
//            } 
        }
        
        $data['status']= 0;
            foreach ($data as $k=>$v){
          $deal_image_gallery->$k = $v;
        }
        if(!$deal_image_gallery->save()){
           return FALSE;
        }
        return TRUE;
    }
    /*
     * This function is used to ADD new Logo Image using DealsImageSection.php in deals_image_section.php 
     * @return : success or failure description (based on method)
    */
    public static function add_in_image_section($data){
        $deal_image_section = new DealsImageSection();
        if(isset($data['deal_id'])){
          $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['deal_id']));
          
          /*Checking, If Offer is not available u can't add image gallery */
          if(empty($offer))
          {
          echo '<br><strong>-----    ----------         Sorry !!          ----------     -----  <br>';
          echo '<strong>Offer Not Available ,Please add Offer before adding New Image Section Logo.</strong>';die;
          }
//          $offer->updated = date("Y-m-d H:i:s");
//            if(!$offer->save()){
//                $errror = $offer->getErrors();
//                throw new Exception("Info Update error :".json_encode($errror),422); 
//            } 
        }
        
        $data['status']= 0;
            foreach ($data as $k=>$v){
          $deal_image_section->$k = $v;
        }
        if(!$deal_image_section->save()){
           return FALSE;
        }
        return TRUE;
    }
    /*End of Add Image Section function  */   
    
    /* For TESTING IMAGE CROP */
    public static function add_in_image_test($data){
        $deal_image_test = new DealsImageTest();
        if(isset($data['deal_id'])){
          $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['deal_id']));
          
          /*Checking, If Offer is not available u can't add image gallery */
          if(empty($offer))
          {
          echo '<br><strong>-----    ----------         Sorry !!          ----------     -----  <br>';
          echo '<strong>Offer Not Available ,Please add Offer before adding New Image for Crop .</strong>';die;
          }
//          $offer->updated = date("Y-m-d H:i:s");
//            if(!$offer->save()){
//                $errror = $offer->getErrors();
//                throw new Exception("Info Update error :".json_encode($errror),422); 
//            } 
        }
        
        $data['status']= 0;
            foreach ($data as $k=>$v){
          $deal_image_test->$k = $v;
        }
        if(!$deal_image_test->save()){
           return FALSE;
        }
        return TRUE;
    }

    /*End of For Testing Croping Image Code */  
    
    /* For Update Crop IMAGE in Deals Table for Preview */
    public static function add_crop_in_deal($data){
        $connection = Yii::app()->db;
         $targetFile = '';
         $new_file_name="";
        $tmp = explode(".", $data['L_img_url']);
        $ext = end($tmp);
        $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'Deal_'.$data['deal_id'].".".$ext;
        $data['img_url'] = $new_file_name;
        $originalFile = $data['L_img_url'];
        $newWidth = Yii::app()->params['GLOBALS']['NEW_WIDTH'];
        $targetFile =  $new_file_name ;
        $info = getimagesize($originalFile);
        $mime = $info['mime'];
        switch ($mime) {
            case 'image/jpeg':
                    $image_create_func = 'imagecreatefromjpeg';
                    $image_save_func = 'imagejpeg';
                    break;
            case 'image/png':
                    $image_create_func = 'imagecreatefrompng';
                    $image_save_func = 'imagepng';
                    break;
            case 'image/gif':
                    $image_create_func = 'imagecreatefromgif';
                    $image_save_func = 'imagegif';
                    break;
            default: 
                    throw Exception('Unknown image type.');
        }

        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);
        /* find the “desired height” of this thumbnail, relative to the desired width  */
        $newHeight = floor(($height / $width) * $newWidth);
        /* create a new, “virtual” image */
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
         /* copy source image at a resized size */
        $t = imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        if (file_exists($targetFile)) {
                unlink($targetFile);
        }
         /* create the physical thumbnail image to its destination */
        $t1 = imagejpeg($tmp, $targetFile,75);
        $tt = $image_save_func($tmp, "$targetFile");
        list($width, $height, $type, $attr) = getimagesize($targetFile);
        /*End Resizing the Original Image Code*/

        $query = "update deals SET img_url ='".$targetFile."',L_img_url='".$originalFile."' where id=". $data['deal_id'];
        $qry_res = $connection->createCommand($query)->execute();  
        if(!$qry_res){
           return FALSE;
        }
        return TRUE;
    }

    /*End of For Testing Croping Image Code */  
    
    public static function get_gallery_list($id){
        $img_gallery = DealsImageGallery::model()->findAllByAttributes(array('deal_id'=>$id));
        if(!$img_gallery){
            return array();
        }else{
            foreach($img_gallery as $k=>$v){
                $imagedetail = $v->getAttributes();
                if(isset($imagedetail['image_url']) && strlen($imagedetail['image_url']) > 0){
                    $imagedetail['image_url'] = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$imagedetail['image_url'];
                }
                $img_data[] = $imagedetail;
            }
        }        
        return $img_data;
    }
    
    public static function get_logo_list($id){
        $img_section = DealsImageSection::model()->findAllByAttributes(array('deal_id'=>$id));
        if(!$img_section){
            return array();
        }else{
            foreach($img_section as $k=>$v){
                $imagedetail = $v->getAttributes();
                if(isset($imagedetail['image_url']) && strlen($imagedetail['image_url']) > 0){
                    $imagedetail['image_url'] = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$imagedetail['image_url'];
                }
                $img_data[] = $imagedetail;
            }
        }        
        return $img_data;
    }
    
    public static function get_test_list($id){
        $img_test = DealsImageTest::model()->findAllByAttributes(array('deal_id'=>$id));
        if(!$img_test){
            return array();
        }else{
            foreach($img_test as $k=>$v){
                $imagedetail = $v->getAttributes();
                if(isset($imagedetail['image_url']) && strlen($imagedetail['image_url']) > 0){
                    $imagedetail['image_url'] = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$imagedetail['image_url'];
                }
                $img_data[] = $imagedetail;
            }
        }        
        return $img_data;
    }
    
    
    public static function remove_from_image_gallery($id,$deal_id=null){
        $img_gallery = DealsImageGallery::model()->findByAttributes(array('id'=>$id));
//        if(!empty($deal_id)){
//            $offer = Offers::model()->findByAttributes(array('deal_id'=>$deal_id));
//            $offer->updated = date("Y-m-d H:i:s");
//            if(!$offer->save()){
//                $errror = $offer->getErrors();
//                throw new Exception("Info Update error :".json_encode($errror),422); 
//            }
//        }
        
        if($img_gallery){
            $img_path = $img_gallery->image_url;
            if(!$img_gallery->delete()){
                return FALSE;
            }else{
                unlink($img_path);
            }
        }
        return TRUE;
    }
    
    /*
     * This function is used to Remove Logo Image from Deal Image Section Table
     * @return : unlink image or FALSE (based on method)
    */
    public static function remove_from_image_section($id,$deal_id=null){
        $img_section = DealsImageSection::model()->findByAttributes(array('id'=>$id));
        //if(!empty($deal_id)){
//            $offer = Offers::model()->findByAttributes(array('deal_id'=>$deal_id));
//            $offer->updated = date("Y-m-d H:i:s");
//            if(!$offer->save()){
//                $errror = $offer->getErrors();
//                throw new Exception("Info Update error :".json_encode($errror),422); 
//            }
        //}
        
        if($img_section){
            $img_path = $img_section->image_url;
            if(!$img_section->delete()){
                return FALSE;
            }else{
                //unlink($img_path);
            }
        }
        return TRUE;
    }
    /*End of remove image section function */    
   
    /*
     * This function is used to Remove Cropped Image from deal_image_test Table 
     * @return : unlink image or FALSE (based on method)
    */
    public static function remove_from_image_test($id,$deal_id=null){
        $img_test = DealsImageTest::model()->findByAttributes(array('id'=>$id));
        if(!empty($deal_id)){
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$deal_id));
//            $offer->updated = date("Y-m-d H:i:s");
//            if(!$offer->save()){
//                $errror = $offer->getErrors();
//                throw new Exception("Info Update error :".json_encode($errror),422); 
//            }
        }
        
        if($img_test){
            $img_path = $img_test->image_url;
            if(!$img_test->delete()){
                return FALSE;
            }else{
                unlink($img_path);
            }
        }
        return TRUE;
    }
    /*End of remove image test function */    

    
    public static function get_my_deals($data=null){

        $data = !empty($data) ? $data : $_REQUEST;
        try{
            //$data = $_REQUEST;
                $result = self::userLikes();
                $offer_likes = array();
                if(count($result)>0){
                    $offer_likes = $result[0]['offer_id'];
                }
                 if(!empty($offer_likes) && (isset($data['api_version']) && $data['api_version']==3)){
                    $column = " CASE WHEN ofr.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
                }
                else{
                     $column = " 0 as mylikes";
                }
           // $currentDate = GeneralComponent::getFormatedDate('Y-m-d');
            // $freebie_cond =(isset($data['freebie']) && $data['freebie']==='true')? " and ofr.offer_price = 0 and dl.expiry_date >= $currentDate ":""; 
           // $freebie_cond =(isset($data['freebie']) && $data['freebie']==='true')? " and ofr.offer_price = 0 ":""; 
                $freebie_cond = "";
            $qry = "SELECT dl.id,ofr.offer_desc, d.by_voucher,dl.transaction_id,dl.users_id,dl.deal_id,dl.offer_id,dl.quantity,dl.amount,d.name as deal_name,ofr.name as offer_name,CASE WHEN of_d.dealer_contact is null THEN '' ELSE of_d.dealer_contact END as dealer_contact,
                ofr.discount,txn.trans_datetime,txn.transaction_mode,ofr.offer_desc as 'short_desc',ofr.long_desc,date(dl.expiry_date) as expiry,
                ofr.actual_price,ofr.offer_price,ofr.discount,ofr.total_stock,ofr.stock_sold,cpn.code,cpn.pin,cpn.status,ofr.min_amount,
                (CASE WHEN cpn.status = 1 THEN 'Active' ELSE (CASE WHEN cpn.status=2 THEN 'Redeem' ELSE (CASE WHEN cpn.status=3 THEN 'Expired' ELSE 'Active' END) END) END) as coupon_status,
                (CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) 
                    ELSE d.img_url END) as img_url , (CASE WHEN length(dm.image_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',dm.image_url) 
                ELSE (
                CASE d.category_id WHEN 2 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_beauty.png' )
                WHEN 12 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fashion.png' )
                WHEN 7 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_services.png' )
                WHEN 15 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_other.png' )
                WHEN 14 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_learning.png' )
                WHEN 11 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fitness.png' )
                WHEN 13 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_entertainment.png' )
                WHEN 16 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_online.png' )
                WHEN 10 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_food.png')  ELSE '' END ) END) as logo_url ,
                 $column
                     FROM `deal_transaction` as dl 
                     LEFT JOIN deals d ON d.id = dl.deal_id
                     LEFT JOIN deals_image_section as dm ON dm.deal_id = d.id 
                     LEFT JOIN offers ofr ON ofr.id = dl.offer_id 
                     LEFT JOIN offer_details as of_d ON of_d.deal_id = d.id
                     LEFT JOIN transactions txn ON txn.trans_id = dl.transaction_id
                     LEFT JOIN coupons cpn ON cpn.id = dl.coupon_id
                     LEFT JOIN users u ON d.dealer_id = u.id
                         WHERE dl.users_id='".Yii::app()->user->id."' and cpn.createdon>'2015-03-01' and txn.status in ('0','1','2') $freebie_cond 
                         ORDER BY dl.transaction_id desc";
            $result = Yii::app()->db->createCommand($qry)->queryAll();
            $i=0;
            $offers = array();
//            if(isset($data['api_version']) && $data['api_version']==3){
//                foreach ($result as $row){
//                    $offers[$i]=$row;
//                    if(array_search($row['offer_id'], $offer_likes)!==false){
//                        $offers[$i]['mylikes']='1';
//                    }
//                    else{
//                         $offers[$i]['mylikes']='0';
//                    }
//                    $i++;
//                }
//            }else{
//                 $offers = $result;
//            }
           $offers = $result;
            $result = array('status'=>'success','errCode'=>0,'description'=>$offers);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        } 
        return $result;
    } 
    /*
     public static function get_my_deals(){
        try{
            $data = $_REQUEST;
           // $currentDate = GeneralComponent::getFormatedDate('Y-m-d');
            // $freebie_cond =(isset($data['freebie']) && $data['freebie']==='true')? " and ofr.offer_price = 0 and dl.expiry_date >= $currentDate ":""; 
            $freebie_cond =(isset($data['freebie']) && $data['freebie']==='true')? " and ofr.offer_price = 0 ":""; 
            $qry = "SELECT dl.id, dl.transaction_id,dl.users_id,dl.deal_id,dl.offer_id,dl.quantity,dl.amount,d.name as deal_name,ofr.name as offer_name,
                ofr.discount,txn.trans_datetime,txn.transaction_mode,ofr.short_desc,ofr.long_desc,date(dl.expiry_date) as expiry,
                ofr.actual_price,ofr.offer_price,ofr.discount,ofr.total_stock,ofr.stock_sold,cpn.code,cpn.status,
                (CASE WHEN cpn.status = 1 THEN 'Active' ELSE (CASE WHEN cpn.status=2 THEN 'Redeem' ELSE (CASE WHEN cpn.status=3 THEN 'Expired' ELSE 'Active' END) END) END) as coupon_status,
                (CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url
                     FROM `deal_transaction` as dl 
                     LEFT JOIN deals d ON d.id = dl.deal_id
                     LEFT JOIN offers ofr ON ofr.id = dl.offer_id 
                     LEFT JOIN transactions txn ON txn.trans_id = dl.transaction_id
                     LEFT JOIN coupons cpn ON cpn.id = dl.coupon_id
                     LEFT JOIN users u ON d.dealer_id = u.id
                         WHERE dl.users_id='".Yii::app()->user->id."' and txn.status in ('0','1','2') $freebie_cond 
                         ORDER BY dl.transaction_id desc";
            $data = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status'=>'success','errCode'=>0,'description'=>$data);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        } 
        return $result;
    } 
    */
    public static function getDealTransaction_expiry($txn_id){
        $query = "SELECT distinct expiry_date FROM deal_transaction where transaction_id= $txn_id";
        $data = Yii::app()->db->createCommand($query)->queryRow();
        return $data;
    }
    /*
    public static function pay_deal_by_wallet($data){
        try{
            $walletdetail = WalletComponent::checkWalletBal();
            if($data['amount'] < $walletdetail[0]['balance']){
                throw new Exception("Insufficent balance",211);
            }
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            $data['userid'] = Yii::app()->user->id;
            $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $data_rev['userid'] . " FOR UPDATE");
            $walletdetail = $walletdata->queryAll();
            $walletObj = $walletdetail[0];
            
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $result;
    }
    */
    
    public static function allow_freebie($connection,$data){
        $userid =isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
        $offerID = $data['offer_id'];
        $dealID = $data['deal_id'];
        $isFreeBeeUsed=false;
        $offeTab = $connection->createCommand("SELECT max_limit_peruser,total_stock FROM `offers` where id=$offerID")->queryAll();
        $totalfreebee=$connection->createCommand("SELECT count(id) as cnt FROM `deal_transaction` "
                 . "where users_id=$userid and deal_id=$dealID and offer_id=$offerID")->queryAll();
        if(count($totalfreebee)>0){
            if($offeTab[0]['total_stock']==-1){
                $isFreeBeeUsed=false;
            }
            elseif($offeTab[0]['max_limit_peruser']==-1){
                    $isFreeBeeUsed=false;
            }
            elseif ($totalfreebee >0 && $offeTab[0]['max_limit_peruser'] <= $totalfreebee[0]['cnt']) {
                    $isFreeBeeUsed=true;
            }
        }
        return $isFreeBeeUsed;
    }
    
    public static function delete_freebie($connection,$data){
         $ref_trans_id = isset($data['trans_id']) ? $data['trans_id'] : 0 ;
         $UserData = UsersComponent::newUserProfile(array('user_id'=>$data['users_id']));
         if(isset($UserData->api_version) && $UserData->api_version==3){
             return false;
         }
        if($ref_trans_id!==0){
            try{
                $deal_transaction_query = "SELECT transaction_id FROM `deal_transaction`
                            WHERE `ref_trans_id` = $ref_trans_id";
                $result = $connection->createCommand($deal_transaction_query)->queryAll();
                if(count($result) < 1){
                    return false;
                }
                //delete from transaction table
                $query = "DELETE FROM  `transactions`
                            WHERE `trans_id` =".$result[0]['transaction_id'];
                $connection->createCommand($query)->execute();
                //delete particulatr transaction from deal_transaction table
                $query2 = "DELETE FROM  `deal_transaction`
                            WHERE `transaction_id` =".$result[0]['transaction_id'];
                $connection->createCommand($query2)->execute();
            }
            catch(Exception $ex){
                return false;
                // $ex->getMessage();
            }
            return true;            
        }
        else{
            return false;
        }
    }
    
    //if freegift going to expire tomorrow send sms and notification to customer
    public static function notify_freeGift_expire(){
        $date = GeneralComponent::getFormatedDate('Y-m-d');
        $connection = Yii::app()->db;
        try{
            $query = "SELECT d.`deal_id` , d.offer_id, d.coupon_id, d.users_id, `name` , u.mobile_number
                       FROM `deal_transaction` AS d
                       LEFT JOIN deals AS dl ON d.deal_id = dl.id
                       LEFT JOIN coupons AS cp ON cp.id = d.coupon_id
                       LEFT JOIN users AS u ON u.id = d.users_id
                       WHERE DATEDIFF(`expiry_date`,'".$date."')=2 AND cp.status !=3 "
                   . " order by u.mobile_number";
           $result = $connection->createCommand($query)->queryAll();
           if(count($result) > 0){
               foreach($result as $row){
                   $users[$row['mobile_number']][$row['name']] =$row['name'];
                   $coupon_id[] = $row['coupon_id'];
               }
               foreach($users as $key=>$val){
                  $mobile = $key;
                  $i=1;
                  $offerName="";
                  $content = Yii::app()->params['message_template']['FREEBIE_BEFORE_EXPIRE_NOTIFY'];
                    foreach($val as $k2=>$v2){
                       // $offerName .= "\r\n ".$i.". ".$k2;
                        $offerName .= $k2." , ";
                       $i++;
                        
                    }
                    $offerName =  trim($offerName,', ');
                   $content = GeneralComponent::replaceword('<'.'OFFER'.'>', $offerName, $content);
                   //send SMS
                   GeneralComponent::sendSMS($mobile, urlencode($content));
                   $nlog_data = array('mobile'=>$mobile,'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>'sms',
                            'msg_content'=>  addslashes($content),'status'=>1);
                    GeneralComponent::log_notifications($nlog_data);
               }
             }
       }
       catch(Exception $ex){
           $transaction->rollback();
            $sender = "support@pay1.in";
            $to = "swapnil@mindsarray.com";
            $subject = "exception occure in notify_freeGift_expire query ";
            $message = json_encode($ex->getTraceAsString());
            GeneralComponent::sendMail($to, $subject, $message,$sender);
       } 
    }
    
    //expire freegift and send mail to customer
    public static function freeGift_expired(){
        $date = new DateTime('- 1 day');
        $date = $date->format('Y-m-d');
        $currentDateTime =GeneralComponent::getFormatedDate();;
        $content = Yii::app()->params['message_template']['FREEBIE_EXPIRE_NOTIFY'];
        $connection = Yii::app()->db;
        $query = "SELECT d.`deal_id` , d.deal_id, d.coupon_id, d.users_id, `name` , u.mobile_number
                    FROM `deal_transaction` AS d
                    LEFT JOIN deals AS dl ON d.deal_id = dl.id
                    LEFT JOIN coupons AS cp ON cp.id = d.coupon_id
                    LEFT JOIN users AS u ON u.id = d.users_id
                    WHERE DATEDIFF(`expiry_date`,'".$date."')=0 AND cp.status !=3 "
                . " order by u.mobile_number";
        $result = $connection->createCommand($query)->queryAll();
        if(count($result) > 0){
              foreach($result as $row){
                  $users[$row['mobile_number']][$row['name']] =$row['name'];
                  $coupon_id[] = $row['coupon_id'];
              } 
             try{
                    $update_query = "UPDATE `coupons` SET status = 3,confirmation_date='$currentDateTime' WHERE id IN (".implode(",",$coupon_id).")";
                    $connection->createCommand($update_query)->execute();
                }
                catch(Exception $ex){
                     $transaction->rollback();
                     $sender = "support@pay1.in";
                     $to = "swapnil@mindsarray.com";
                     $subject = "exception occure in notify_freeGift_expire query ";
                     $message = json_encode($ex->getTraceAsString());
                     GeneralComponent::sendMail($to, $subject, $message,$sender);
                }
                $user_ids = array();
                $offer = array();
                foreach($users as $key=>$val){
                  $mobile = $key;
                  $i=1;
                  $offerName="";
                  $content = Yii::app()->params['message_template']['FREEBIE_EXPIRE_NOTIFY'];
                    foreach($val as $k2=>$v2){
                        //$offerName .= "\r\n ".$i.". ".$k2;
                        $offerName .= $k2." , ";
                       $i++;
                        
                    }
                    $offerName = trim($offerName,', ');
                   $content = GeneralComponent::replaceword('<'.'OFFER'.'>', $offerName, $content);
                   //send SMS
                   GeneralComponent::sendSMS($mobile, urlencode($content));
                   $nlog_data = array('mobile'=>$mobile,'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>'sms',
                            'msg_content'=>  addslashes($content),'status'=>1);
                    GeneralComponent::log_notifications($nlog_data);
               }
            }
    }
    
    public static function notify_offer_expire_validity(){
        $date = new DateTime('+ 3 day');
        $date = $date->format('Y-m-d');
        $connection = Yii::app()->db;
        //notify email before 3 day expiry of deal
        $query = "SELECT d.name AS deal, off.name AS offer,DATE_FORMAT( off.validity, '%Y-%m-%d' ) as validity
                    FROM `offers` AS off
                    INNER JOIN deals AS d ON d.id = off.deal_id
                    WHERE DATEDIFF( DATE_FORMAT( validity, '%Y-%m-%d' ) , '".$date."' )=0 AND off.status =1 "
                . " AND validity_mode in(2,3)"
                . " Group By 1,2";
        $result = $connection->createCommand($query)->queryAll();
        if(count($result) > 0){
               $sender = "support@pay1.in";
               $to = "tadka@mindsarray.com,swapnil@mindsarray.com,upload@pay1.in,giftsupport@pay1.in";
               $subject = "Deals going to expire on $date";
               $message ="Following deals are going to expire on $date .<br><br>";
               $i=1;
               foreach($result as $row){
                   $message .="$i. Deal '".$row['deal']."' having offer '".$row['offer']."' .<br>";
                   $i++;
               }
               GeneralComponent::sendMail($to, $subject, $message,$sender);
        }
        
    }
    
    public static function notify_offer_expire_stock(){
        $connection = Yii::app()->db;
        //notify email when 80% of stock get used
        $query ="SELECT d.name AS deal, off.name AS offer, d.id,round( (
                `stock_sold` /  `total_stock`
                ) *100) as stock_used,off.id as offer_id,
                off.total_stock,off.stock_sold
                FROM `offers` AS off
                INNER JOIN deals AS d ON d.id = off.deal_id
                WHERE round( (
                `stock_sold` /  `total_stock`
                ) *100) >=90
                AND validity_mode in (1,3)  AND off.mail_status=0
                AND off.status =1
                 order by stock_used desc";
        $result = $connection->createCommand($query)->queryAll();
        if(count($result) > 0){
                $transaction = $connection->beginTransaction();
                try{
                    $sender = "support@pay1.in";
                    $to = "tadka@mindsarray.com,swapnil@mindsarray.com,upload@pay1.in,giftsupport@pay1.in";
                    $subject = "Deals with 90+% of stock used";
                    $message ="Following deals stock are 90+% used .<br><br>";
                    $offer_ids = array();
                    $i=1;
                    foreach($result as $row){
                        $message .="$i. Deal '".$row['deal']."' having offer '".$row['offer']."' is used upto ".$row['stock_sold']."/".$row['total_stock']." (".$row['stock_used']."%) .<br>";
                        $i++;
                        $offer_ids[] = $row['offer_id'];

                    }
                    if(is_array($offer_ids)){
                        $offer_ids = implode(",",$offer_ids);
                    }

                    $updateOffer = "UPDATE offers SET mail_status=1 WHERE id in (".$offer_ids.")";
                    if($connection->createCommand($updateOffer)->execute()){
                        $transaction->commit();
                        GeneralComponent::sendMail($to, $subject, $message,$sender);
                    }
                    else{
                        $sender = "support@pay1.in";
                         $to = "swapnil@mindsarray.com";
                         $subject = "update query fails for 95+% of stock used";
                         $message ="error occured in notify_deal_expire_stock API while updating offer table .<br><br>";
                         GeneralComponent::sendMail($to, $subject, $message,$sender);
                    }
                }
                catch(Exception $ex){
                     $transaction->rollback();
                     $sender = "support@pay1.in";
                     $to = "swapnil@mindsarray.com";
                     $subject = "exception occure in notify_deal_expire_stock query ";
                     $message = json_encode($ex->getTraceAsString());
                     GeneralComponent::sendMail($to, $subject, $message,$sender);
                }
        }
    }
    
    public static function expire_offer_stock(){ 
        $connection = Yii::app()->db;
        //notify email when 80% of stock get used
        $query ="SELECT d.name AS deal, off.name AS offer, d.id as deal_id,round( (
                `stock_sold` /  `total_stock`
                ) *100, 2 ) as stock_used,off.id as offer_id,
                off.total_stock,off.stock_sold
                FROM `offers` AS off
                INNER JOIN deals AS d ON d.id = off.deal_id
                WHERE round( (
                `total_stock` / `stock_sold`
                ) *100) =100
                AND validity_mode in (1,3) AND off.mail_status=1
                AND off.status =1
                order by stock_used desc";
        $result = $connection->createCommand($query)->queryAll();
        if(count($result) > 0){
             $transaction = $connection->beginTransaction();
            try{
               $sender = "support@pay1.in";
               $to = "tadka@mindsarray.com,swapnil@mindsarray.com,upload@pay1.in,giftsupport@pay1.in";
               $subject = "Deals with 100% of stock used";
               $message ="Following deals stock are 100% used .<br>";
               $i = 1;
               $deal_id = array();
               foreach($result as $row){
                   $message .="$i. Deal '".$row['deal']."' having offer '".$row['offer']."' is used upto ".$row['stock_sold']." / ".$row['total_stock']." (100%) .<br><br>";
                   $i++;
                   $offer_ids[] = $row['offer_id'];
                   $deal_id[] = $row['deal_id'];
               }
               if(is_array($offer_ids)){
                   $offer_ids = implode(",",array_unique($offer_ids));
               }
                if(is_array($deal_id)){
                   $deal_id = implode(",",array_unique($deal_id));
               }
               $currentDate = GeneralComponent::getFormatedDate();
               $updateOffer = "UPDATE offers SET mail_status=2,updated='$currentDate',status = 3 WHERE id in (".$offer_ids.")";
               //$updateDeal = "UPDATE deals SET updated=$currentDate WHERE id in (".$deal_id.")";
               if($connection->createCommand($updateOffer)->execute() ){
                   $transaction->commit();
                   GeneralComponent::sendMail($to, $subject, $message,$sender);
               }
               else{
                   $sender = "support@pay1.in";
                    $to = "swapnil@mindsarray.com";
                    $subject = "update query fails for 100% of stock used";
                    $message ="error occured in expire_deal_stock API while updating offer table .<br>";
                    GeneralComponent::sendMail($to, $subject, $message,$sender);
               }
            }
            catch(Exception $ex){
                    $transaction->rollback();
                     $sender = "support@pay1.in";
                     $to = "swapnil@mindsarray.com";
                     $subject = "exception occure in expire_deal_stock query ";
                     $message = json_encode($ex->getTraceAsString());
                     GeneralComponent::sendMail($to, $subject, $message,$sender);
            }
               
        }
        
    }
    //expire offer and send notification mail
    public static function expire_offer(){ 
        $connection = Yii::app()->db;
        $date = GeneralComponent::getFormatedDate('Y-m-d');
       // $date = '2014-11-23';
        $query = "SELECT d.name AS deal, off.name AS offer,off.id,d.id as deal_id 
                 FROM `offers` AS off
                 INNER JOIN deals AS d ON d.id = off.deal_id
                 WHERE (DATEDIFF(DATE_FORMAT( validity, '%Y-%m-%d' ) ,'".$date."')=0 OR stock_sold=total_stock) AND off.status =1 AND off.sequence=1 ";
                //. " AND validity_mode in(2,3)";//AND validity_mode in(2,3)
        $deal_transaction = $connection->createCommand($query)->queryAll();
        
        if(count($deal_transaction) > 0){
            $offer_id = array();
            $deal_id = array();
            foreach($deal_transaction as $row){
                $offer_id[] = $row['id'];
                $deal_id[] = $row['deal_id'];
            }
            $offer_id  = implode(",",array_unique($offer_id));
            $deal_id = implode(",",array_unique($deal_id));
            $transaction = $connection->beginTransaction();
            $currentDate = GeneralComponent::getFormatedDate();
            try{
                $expire_deal = "UPDATE offers SET status = 3,updated='$currentDate' where id in ($offer_id)";
              /*  if(!empty($deal_id)){
                    $update_deal = "UPDATE deals SET updated=$currentDate WHERE id in (".$deal_id.")";
                }*/
                if(!$connection->createCommand($expire_deal)->execute() ){
                    $sender = "support@pay1.in";
                    $to = "swapnil@mindsarray.com";
                    $subject = "Updation of deals get failed";
                    $message ="Please check expire_deal api";
                   GeneralComponent::sendMail($to, $subject, $message,$sender);
                }
                else{
                    $transaction->commit();
                    //following deal expired today
                    $sender = "support@pay1.in";
                    $to = "tadka@mindsarray.com,swapnil@mindsarray.com,upload@pay1.in,giftsupport@pay1.in";
                    $subject = "Deals which are expired Today";
                    $message ="Following deals are expired Today .<br><br>";
                    $i=1;
                    foreach($deal_transaction as $row){
                        $message .="$i. Deal '".$row['deal']."' having offer '".$row['offer']."' .<br>";
                        $i++;
                        
                    }
                    GeneralComponent::sendMail($to, $subject, $message,$sender);
                }
                    
            }
            catch (Exception $ex){
                $transaction->rollback();
                 $sender = "support@pay1.in";
                 $to = "swapnil@mindsarray.com";
                 $subject = "exception occure in expire_deal query ";
                 $message = json_encode($ex->getTraceAsString());
                 GeneralComponent::sendMail($to, $subject, $message,$sender);
            }
        }
    }
 
    public static function FreeGiftAnnouncement($data = null){
        $data['offer_id'] = isset($data['offer_id']) ? $data['offer_id'] : 0;
        $query="SELECT d.name AS deal, off.name
        FROM deals AS d
        INNER JOIN offers AS off ON d.id = off.deal_id
        LEFT JOIN deal_locations AS dl ON dl.deal_id = d.id
        LEFT JOIN user_profile 
        WHERE off.id = ".$data['offer_id']." ";
    }
    
    public static function getPreview($id){
         $connection = Yii::app()->db;
         $deallist_qry = "SELECT d.id, d.name as dealname, "
                                . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url , c.name as category,c.id as category_id,"
                                ."(CASE WHEN length(dm.image_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',dm.image_url) ELSE  dm.image_url END) as logo_url,"
                                . "dl.lat as latitude, dl.lng as longitude,of.min_amount, dl.address,la.name as area,lc.name as city,ls.name as state,"
                                . "of.id as ofid, of.name as ofname,of.validity as validity, of.offer_desc as short,of.long_desc,of.offer_desc "
                                . " FROM deals as d "
                                . "LEFT JOIN offers as of ON d.id = of.deal_id "
                                . "LEFT JOIN categories as c ON d.category_id = c.id "
                                . "LEFT JOIN deal_locations dl ON d.id=dl.deal_id "
                                . "LEFT JOIN locator_area as la ON dl.area_id = la.id "
                                . "LEFT JOIN locator_city as lc ON dl.city_id = lc.id "
                                . "LEFT JOIN locator_state as ls ON lc.state_id=ls.id "
                                . "LEFT JOIN deals_image_section as dm ON dm.deal_id=d.id "
                                . "WHERE d.id=$id "
                                . " AND of.sequence = '1'"
                                    . " ORDER BY d.id asc";
         try{
            $deals = Yii::app()->db->createCommand($deallist_qry)->queryAll();
         }  catch (Exception $ex){
             return array();
         }
         return isset($deals) ? $deals : array();
    }
    
    public static function getDealContext($id){
        $connection = Yii::app()->db;
        $dealsinfo_qry = "select content_txt from deal_info where deal_id='".$id."' and status !=0 order by sequence asc";
        try{ 
            $context = $connection->createCommand($dealsinfo_qry)->queryAll();
        }  catch (Exception $ex){
            return array();
        }
         return isset($context) ? $context : array();
    }
    //get reasons list for closing deals (in edit_deals)
    public static function get_reason_list(){
        $connection = Yii::app()->db;
        $dealsreason_qry = "select * from deal_reason ";
         $reasons = $connection->createCommand($dealsreason_qry)->queryAll();
         return isset($reasons) ? $reasons : array();
    }
    
    public static function getUserGiftCounts(){
        $connection = Yii::app()->db;
        $userid = Yii::app()->user->id;
        $query="SELECT ul.offer_id,ul.reviews,up.image,count(coupon_id) as total_gifts,SUM(case WHEN c.status=2 THEN 1 ELSE 0 END) as total_redeem"
                . " FROM deal_transaction as d INNER JOIN coupons as c ON (c.id=d.coupon_id) LEFT JOIN user_likes as ul"
                . " ON (ul.users_id=c.users_id) "
                . " LEFT JOIN user_profile as up ON (up.user_id=ul.users_id) WHERE d.users_id = $userid";
        try{
            $result = $connection->createCommand($query)->queryAll();
        }catch(Exception $ex){
            return array();
        }
        return isset($result) ? $result : array();
    }
    
   public static function getAllOfferCategory($data){
       $category = '';
       $type = isset($data['type']) ? $data['type']: 0;
       $next = isset($data['next']) ? $data['next']: 0;
       $connection = Yii::app()->db;
       
       $start = 0;
       $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
       $limitcond = "";
      if(!empty($next) && $next!=-1){
           $start = ($next * 10);
       }

       $limitcond =  "LIMIT $start,$end ";
        
       if($next==-1){
            $limitcond = "";
       }
      // $offer_likes = self::userLikes();
//        if(count($offer_likes)>0){
//            $offer_likes = explode(',',$offer_likes[0]['offer_id']);
//        }
       try{
            if(empty($type)){
               $query = "SELECT c.name,c.id,(CASE WHEN c.id=17 THEN 1 ELSE 0 END) as flag, SUBSTRING_INDEX( GROUP_CONCAT( 
                        CASE WHEN off.sequence =1 AND off.status =1 THEN off.id END ORDER BY updated DESC,(CASE WHEN order_flag =0 OR order_flag IS NULL THEN 11111 ELSE order_flag END ) ASC) ,  ',', 10 ) AS off_id
                        FROM deals AS d
                        INNER JOIN offers AS off ON ( d.id = off.deal_id ) 
                        INNER JOIN categories AS c ON ( c.id = d.category_id ) 
                        where c.id!=1 AND d.status=1 AND off.status=1 AND c.order!=0
                        GROUP BY c.order";
                $result = $connection->createCommand($query)->queryAll();
                $offers = array();
                if(count($result) > 0){
                    foreach($result as $row){
                        $offers[] = array('special'=>$row['flag'],'name'=>$row['name'],'id'=>$row['id'],'details'=>$row['off_id']);//,'mylikes'=>  implode(',',array_intersect($offer_likes, explode(',',$row['off_id'])))
                    }
                }
           }
           else{
               $query = "SELECT c.name,c.id,off.id as off_id "
                       . " FROM deals AS d "
                       . " INNER JOIN offers AS off ON ( d.id = off.deal_id)"
                       . " INNER JOIN categories AS c ON (c.id = d.category_id )  "
                       . " WHERE c.id=$type AND off.sequence =1 AND off.status =1 AND d.status=1"
                       . " ORDER  by updated DESC,(CASE WHEN order_flag =0 OR order_flag IS NULL THEN 11111 ELSE order_flag END ) ASC "
                       . " $limitcond ";
               //AND off.sequence=1 AND off.status =1
                $result = $connection->createCommand($query)->queryAll();
                $offers = array();
                if(count($result) > 0){
                    $offerids = array();
                    foreach($result as $row){
                        $offerids[] = $row['off_id'];
                    }
                    $offers[] = array('name'=>($result[0]['name']),'id'=>$result[0]['id'],'details'=>implode(',',$offerids));//,'mylikes'=>  implode(',',array_intersect($offer_likes,$offerids))
                }
           }
       }catch(Exception $ex){
           $offers = array();
       }
       
//       $gifts['offer_details']=$offers;
//       $gifts['offer_likes']= (isset($offer_likes[0]['offer_id']) && !empty($offer_likes[0]['offer_id'])) ? $offer_likes[0]['offer_id'] : "";
       
       return json_encode($offers);
   }
   
   
   public static function getAllofferTypes($data,$requiredtypes=null){
       $condition = 1;
       if(isset($requiredtypes) && is_array($requiredtypes)){
           $ids = implode(',', $requiredtypes);
           $condition = " dt.id IN ($ids) ";
       } 
       $connection = Yii::app()->db;
        $offer_likes = self::userLikes();
       $type = isset($data['type']) ? $data['type']: 0;
       $next = isset($data['next']) ? $data['next']: 0;
       $connection = Yii::app()->db;
       
       $start = 0;
       $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
       $limitcond = "";
      if(!empty($next) && $next!=-1){
           $start = ($next * 10);
       }

       $limitcond =  "LIMIT $start,$end ";
        
       if($next==-1){
            $limitcond = "";
       }
       $offers = array();
       try{
            if(empty($type)){
                $query = "SELECT dt.name,CASE WHEN dt.id=1 THEN 1 ELSE 0 END as flag, dt.id, SUBSTRING_INDEX( GROUP_CONCAT( 
                         CASE WHEN off.sequence =1 AND off.status =1 THEN off.id END ORDER BY updated DESC,(CASE WHEN order_flag =0 OR order_flag IS NULL THEN 11111 ELSE order_flag END ) ASC  ) ,  ',', 10 ) AS off_id
                         FROM deals AS d
                         INNER JOIN offers AS off ON ( d.id=off.deal_id ) 
                         INNER JOIN deal_types AS dt ON ( dt.id=d.type_id )
                         where $condition AND d.status=1 AND off.status=1  AND dt.order!=0 AND off.sequence =1 
                         GROUP BY dt.order";
                 $result = $connection->createCommand($query)->queryAll();

                 if(count($result) > 0){
                     foreach($result as $row){
                         $offers[] = array('featured'=>$row['flag'],'name'=>($row['name']),'id'=>$row['id'],'details'=>$row['off_id']);//,'mylikes'=>  implode(',',array_intersect($offer_likes, explode(',',$row['off_id'])))
                     }
                 }
            }
            else{
                 $query = "SELECT dt.name,dt.id,off.id as off_id "
                        . " FROM deals AS d "
                        . " INNER JOIN offers AS off ON ( d.id=off.deal_id )"
                        . " INNER JOIN deal_types AS dt ON ( dt.id=d.type_id ) "
                        . " WHERE dt.id=$type AND off.sequence =1 AND off.status =1 AND d.status=1 "
                        . " ORDER  by updated DESC,(CASE WHEN order_flag =0 OR order_flag IS NULL THEN 11111 ELSE order_flag END ) ASC"
                        . " $limitcond ";
                //AND off.sequence=1 AND off.status =1
                 $result = $connection->createCommand($query)->queryAll();
                 if(count($result) > 0){
                     $offerids = array();
                     foreach($result as $row){
                         $offerids[] = $row['off_id'];
                     }
                     $offers[] = array('name'=>($result[0]['name']),'id'=>$result[0]['id'],'details'=>implode(',',$offerids));//,'mylikes'=>  implode(',',array_intersect($offer_likes,$offerids))
                 }
            } 
       }catch(Exception $ex){
           $offers = array();
       }
       $gifts['offer_details']=$offers;
       $gifts['offer_likes']= (isset($offer_likes[0]['offer_id']) && !empty($offer_likes[0]['offer_id'])) ? $offer_likes[0]['offer_id'] : "";
       if(empty($offers)){
           return json_encode(array());
       }
       
       return json_encode($gifts);
   }
   
   public static function userLikes(){
       $userid = Yii::app()->user->id;
       if(!empty($userid)){
            $query = "SELECT offer_id FROM user_likes WHERE users_id=$userid";
            try{
                $result = Yii::app()->db->createCommand($query)->queryAll();
            }catch(Exception $ex){
                return array();
            }
            if(count($result)>0){
                return $result;
            }
       }
       return array();
   }
   
   public static function userReviews(){
       $userid = Yii::app()->user->id;
       if(!empty($userid)){
           $reviews = array();
            $query = "SELECT reviews FROM user_likes WHERE users_id=$userid";
            try{
                $result = Yii::app()->db->createCommand($query)->queryAll();
            }  catch (Exception $ex){
                $result = array();
            }
            if(count($result)>0){
                $Review = $result[0]['reviews'];
                if(!empty($Review)){
                    $ReviewArray = explode(',', $Review);
                    foreach ($ReviewArray as $row){
                        $Arr = explode('_', $row);
                        $reviews[$Arr[0]] = $Arr[1];
                        
                    }
                    return $reviews;
                }
            }
       }
       return array();
   }


   public static function myLikes($data){
       try{
       if(isset($data['id']) && !empty($data['id']) && is_numeric($data['id'])){
           $connection = Yii::app()->db;
           $off_id = $data['id'];
           $date = GeneralComponent::getFormatedDate();
           $userid = Yii::app()->user->id;
           //check offerid in user_likes table
           $query = "SELECT offer_id FROM user_likes WHERE users_id=$userid";
           $result = self::userLikes();
           $myLikeAdd = 0;
            if(count($result)>0){
               $row = $result[0];
               $offers_id = array();
               if(!empty($row['offer_id'])){
                    $offers_id = explode(',', $row['offer_id']);
               }
               $flag = false;
               $flag = array_search($off_id,$offers_id);
               
               if($flag===false){
                   
                   $offers_id = empty($offers_id) ? array($off_id) : array_merge($offers_id,array($off_id));
                   $myLikeAdd = 1;
               }
               else{
                   unset($offers_id[$flag]);
                   $myLikeAdd = 0;
               }
               $offers = implode(',', $offers_id);
              $query2 = "UPDATE user_likes SET offer_id='$offers' WHERE users_id= $userid";
               if($connection->createCommand($query2)->execute()){
//                   $query3 = "UPDATE offers SET updated='$date' WHERE id= $off_id";
//                   $connection->createCommand($query3)->execute();
                   return array('status' => 'success', 'errCode' => '0', 'description' =>array('msg'=>'Mylikes saved','status'=>$myLikeAdd));
               }
               else{
                   return array('status' => 'error', 'errCode' => '0', 'description' =>'There is an error while saving the record');
               }
           }
           else{
              $query="INSERT INTO user_likes (users_id,offer_id) VALUES($userid,'$off_id')";
               if($connection->createCommand($query)->execute()){
                  
                   $myLikeAdd = 1;
//                   $query3 = "UPDATE offers SET updated='$date' WHERE id= $off_id";
//                   $connection->createCommand($query3)->execute();
                   return array('status' => 'success', 'errCode' => '0', 'description' =>array('msg'=>'Mylikes saved','status'=>$myLikeAdd));
               }
               else{
                   return array('status' => 'error', 'errCode' => '0', 'description' =>'There is an error while saving the record');
               }
           }
           //insert offer id in 
           
       }else{
           return array('status' => 'error', 'errCode' => '0', 'description' =>'Please provide parameter id with offer id value');
       }
       }
       catch (Exception $ex) {
           $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        } 
   }
   
   public static function  offerDetails ($data){
       try{
           $d_userid = isset($data['user_id']) ? $data['user_id']: "";
           $userid = isset(Yii::app()->user->id) ? Yii::app()->user->id : $d_userid;
           if(empty($userid) && !isset($data['request_via'])){
               throw new Exception("UNAUTHORIZE ACCESS",201);
           }
           $result = self::userLikes();
            $offer_likes = array();
            if(count($result)>0){
                $offer_likes = $result[0]['offer_id'];
            }
             if(!empty($offer_likes)){
                $column = " CASE WHEN id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
            }
            else{
                 $column = " 0 as mylikes";
            }
            
            $result2 = self::userReviews();
            
            $id = isset($data['id'])?$data['id']:0;$maxdistance = 5;
            $data['latitude'] = ($data['latitude']==="null" || $data['latitude'] == "")?1:$data['latitude'];
            $data['longitude'] = ($data['longitude']==="null" || $data['longitude'] == "")?1:$data['longitude'];
            $lat = deg2rad($data['latitude']);
            $lng = deg2rad($data['longitude']);
            $eradius = 6371;
            $maxdistance = 5;
            $pocketGift = "0";
            $coupon = array();
            $connection = Yii::app()->db;
            $dealsDetails = $connection->createCommand("SELECT by_voucher FROM deals where id=$id")->queryRow();
            $by_voucher = isset($dealsDetails['by_voucher']) ? $dealsDetails['by_voucher'] : 0;
            if(!empty($userid)){
                $query="SELECT status FROM coupons WHERE deal_id=$id AND users_id=$userid";
                $coupon = $connection->createCommand($query)->queryAll();
            }
            $dealsinfo_qry = "select content_txt from deal_info where deal_id='".$id."' and status !=0 order by sequence asc";
            $dealsinfo1 = $connection->createCommand($dealsinfo_qry)->queryAll();
            $content = "";
            foreach ($dealsinfo1 as $k=>$v){
                $content .= html_entity_decode($v['content_txt']);
            }
            $dealsinfo = array(array('content_txt'=>$content));
            $dealsoffer_qry = "select id,deal_id,name,offer_price,actual_price,discount,total_stock,stock_sold,validity,$by_voucher as 'by_voucher',"
                    . "validity_mode,max_quantity,status,dealer_contact,offer_desc as 'short_desc',offer_desc,long_desc,created,updated,"
                    . "mail_status,img_url,sequence,min_amount,max_limit_peruser,global_flag,order_flag,coupon_expiry_date,$column from offers where deal_id='".$id."' and status !=0 and sequence =1";
            $dealsoffer = Yii::app()->db->createCommand($dealsoffer_qry)->queryAll();
            if(count($dealsoffer)>0){
                $brand_desc = isset($dealsoffer[0]['long_desc']) ? $dealsoffer[0]['long_desc'] : "";
                $offerArray = array();
               // print_r($result2);
                $result3 =  array_keys($result2);
                $i=0;
                foreach ($dealsoffer as $row){
                    $offerArray[$i] = $row;
                    $flag = array_search($row['id'],$result3);
                    if($flag!==false){
                        $offerArray[$i]['review']= $result2[$result3[$flag]];
                    }else{
                        $offerArray[$i]['review']= 0;
                    }
                    $offerArray[$i]['off_avg'] = self::get_OffAvgReview($row['id']);;
                    $i++;
                }
                $deal_image_list = self::get_gallery_list($id);
                $deal_image_gallery = array();
                foreach ($deal_image_list as $k=>$v){
                   // unset($deal_image_list[$k]['deal_id'],$deal_image_list[$k]['uploaded_date'],$deal_image_list[$k]['status']);
                     $deal_image_gallery[] =  $v['image_url'];
                }

                $dealslocation_qry = "select dealer_contact, dl.deal_id, dl.lat, dl.lng, dl.address, lc.name as city,ls.name as state,la.name as area,dl.full_address, "
                            ."(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance "
                            . "FROM deal_locations as dl "
                            . " LEFT JOIN locator_area as la ON (la.id=dl.area_id)"
                            . " LEFT JOIN locator_city as lc ON dl.city_id = lc.id "
                            . " LEFT JOIN locator_state as ls ON lc.state_id=ls.id "
                            . "WHERE dl.deal_id='".$id."' and dl.status !=0 "
                                //. "AND (acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) <= $maxdistance "
                                    . "ORDER BY distance asc";
                $dealslocation = Yii::app()->db->createCommand($dealslocation_qry)->queryAll();
                $deals_q = "SELECT company_url FROM deals WHERE id=$id";
                $deals_result = Yii::app()->db->createCommand($deals_q)->queryRow();
                $companyUrl = (isset($deals_result['company_url']) && !empty($deals_result['company_url'])) ? $deals_result['company_url'] : "";
                $off_avg = 0;
                if(isset($dealsoffer[0]['id'])){
                    $off_avg = self::get_OffAvgReview($dealsoffer[0]['id']);
                }
                $offer_info = self::getOfferdetails($id);
                $offInfo = "<ul>";
                $offInfo.= !empty($offer_info['time']) ? "<li>".$offer_info['time']."</li>" : "";
                $offInfo.= !empty($offer_info['day']) ? "<li>".$offer_info['day']."</li>" : "";
                $offInfo.= !empty($offer_info['appointment']) ? "<li>".$offer_info['appointment']."</li>" : "";
                $offInfo.= !empty($offer_info['exclusive_gender']) ? "<li>".$offer_info['exclusive_gender']."</li>" : "";
                $offInfo.= !empty($offer_info['dealer_contact']) && empty($dealslocation) ? "<li>".$offer_info['dealer_contact']."</li>" : "";
                 $offInfo.= "</ul>";
                 
                  if(isset($coupon[0]['status']) && count($coupon)>=$dealsoffer[0]['max_limit_peruser'] && $dealsoffer[0]['max_limit_peruser']!=-1){
                        $pocketGift = $coupon[0]['status'];
                    }
            
                $offer_like = isset($offerArray[0]['id']) ? self::checkuser_likestatus($offerArray[0]['id']):0;
                $result = array('status'=>'success','errCode'=>0,
                        'description'=>array('deal_detail'=>  (GeneralComponent::replaceword("&nbsp;", " ", GeneralComponent::removeSlashes($dealsinfo))),
                                         'offer_detail'=>  (GeneralComponent::replaceword("&nbsp;", " ",GeneralComponent::removeSlashes($offerArray))),
                                         'location_detail'=>  !empty($dealslocation) ? GeneralComponent::replaceword("&nbsp", "&nbsp;",GeneralComponent::removeSlashes($dealslocation)) : array(),
                                         'image_list'=>$deal_image_gallery,
                                         'brand_description'=>$brand_desc,
                                         'offer_like'=>$offer_like,
                                         'offer_info'=>$offer_info,
                                         'off_avg'=>"$off_avg",
                                         'pocketGift'=>$pocketGift,
                                         'offer_info_html'=>$offInfo,
                                         'company_url'=>$companyUrl
                                       ));
            }else{
                throw new Exception("Deal not exist with this id");
            }
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
   }
   
   public static function checkuser_likestatus($offerid){
       try{
       $result = self::userLikes();
       }catch(Exception $ex){
           return 0;
       }
       if(count($result)>0){
           $offers = explode(',', $result[0]['offer_id']);
           $flag = array_search($offerid,$offers);
           if($flag===false){
               return '0';
           }else{
               return '1';
           }
       }
       return '0';
   }
   //store user review related to offer in user_likes table
   public static function setOfferReview($data){
       $userid = Yii::app()->user->id;
       $offerId = (isset($data['id'])) ? $data['id']:0;
       $reviewPt = (isset($data['review'])) ? $data['review']:0;
       if(empty($offerId)){
           return array('status'=>'failure','errCode'=>0,'description'=>'Please enter offer id of gift');
       }
       if(!empty($reviewPt)){
           switch($reviewPt){
               case 1:
                   $reviewPt=5;
                   break;
               case 2:
                   $reviewPt=4;
                   break;
                case 4:
                   $reviewPt=2;
                   break;
               case 5:
                   $reviewPt=1;
                   break;
           }
       }else{
            return array('status'=>'failure','errCode'=>0,'description'=>'Please enter review number');
       }
       $review =$reviewPt;
       $user_review = $offerId."_".$review;
       if(!empty($offerId) && !empty($review)){
           $query = "SELECT reviews FROM user_likes WHERE users_id=$userid";
           $result = Yii::app()->db->createCommand($query)->queryAll();
           if(count($result)>0 ){
                if(!empty($result[0]['reviews'])){
                    $review_pts = explode(',',$result[0]['reviews']);
                    $i=0;
                    $review_flag = false;
                    foreach($review_pts as $row){
                       if(strpos($row,$offerId."_")!==false){
                           $review_flag = true;
                           break;
                       }
                       $i++;
                    }
                    if($review_flag){ //if review with same offer id already +nt
                        unset($review_pts[$i]);
                        $review_pts = array_merge($review_pts,array("$user_review"));
                        $review_pts = implode(',', $review_pts);
                        $query2 = "UPDATE user_likes SET reviews='$review_pts' WHERE users_id= $userid";
                    }
                    else{
                        $review_pts = array_merge($review_pts,array("$user_review"));
                         $review_pts = implode(',', $review_pts);
                        $query2 = "UPDATE user_likes SET reviews='$review_pts' WHERE users_id= $userid";
                    }
                }
                else{
                    //review is blank
                     $query2 = "UPDATE user_likes SET reviews='$user_review' WHERE users_id= $userid";
                }
           }
            else{ //new entry of user 
                $query2 = "INSERT INTO user_likes (users_id,reviews) VALUES($userid,'$user_review')";
            }
            if(Yii::app()->db->createCommand($query2)->execute()){
                     return array('status' => 'success', 'errCode' => '0', 'description' =>'User Reviews Saved Successfully');
               }
               else{
                   return array('status' => 'error', 'errCode' => '0', 'description' =>'Error while saving user review');
               }
            }
       return array('status'=>'failure','errCode'=>0,'description'=>'Please enter valid offer id or review number');
   }
   
   public static function getDealTypes(){
       $query="SELECT id,name FROM deal_types ";
       $result=Yii::app()->db->createCommand($query)->queryAll();
       return !empty($result) ? $result : array();
   }
   
   public static function updateDealType($data){
       $typeid = $data['typeid'];
       $id = $data['id'];
       $query="UPDATE deals SET type_id=$typeid WHERE id=$id";
       if(Yii::app()->db->createCommand($query)->execute()!==false){
           return 'Deal type saved successfully';
       }
       else{
            return 'error while saving deal type';
       }
       
   }
   //get average review of offer
   public static function get_OffAvgReview($offerid){
        $query="SELECT `reviews` FROM `user_likes` ";
        $result=Yii::app()->db->createCommand($query)->queryAll();
        $reviewPts =0;
        $j=0;
        if(count($result)>0){
            
            $reviewPts = 0;
            foreach($result as $record){
                $reviewsArr = explode(',', $record['reviews']);
                foreach ($reviewsArr as $row){
                    $reviews = explode('_', $row);
                    if($reviews[0]==$offerid){
                         $reviewPts+= (int)$reviews[1];
                        $j++;
                    }
                } 
            }
        }
        if(!empty($reviewPts) && $j>5){
            return round($reviewPts/$j);
        }
        return 0;
   }
   
   public static function getOfferdetails($dealid){
       $dealsOfferDetails = "SELECT * FROM offer_details WHERE deal_id=".$dealid;
        $offer_info = Yii::app()->db->createCommand($dealsOfferDetails)->queryAll();
        if(count($offer_info)<=0){
            $offer_info = array('time'=>'10 AM TO 10 PM','day'=>'6 Days in Week (Sunday closed)','appointment'=>'Prior Appointment is mandatory','exclusive_gender'=>'Exclusive for men and women');
        }
        else{
            $offer_info = array('time'=>$offer_info[0]['time'],'day'=>$offer_info[0]['day'],'appointment'=>$offer_info[0]['appointment'],'exclusive_gender'=>$offer_info[0]['exclusive_gender'],'dealer_contact'=>$offer_info[0]['dealer_contact']);
        }
        return $offer_info;
   }
   
   public static function getAllGifts_WEB($data){
    try{
       $type = (isset($data['type'])&&!empty($data['type'])) ? $data['type']: 'gift';
       $condition =1;
       $typesId=0;
       if($type=='gift'){
           $deal_type_q = "SELECT *,LOWER(REPLACE(REPLACE(name,' ',''),'&','')) as url FROM deal_types dt WHERE dt.order!=0 ORDER by dt.order ";
           $id='type_id';
       }elseif ($type=='category') {
           $deal_type_q = "SELECT *,LOWER(REPLACE(REPLACE(name,' ',''),'&','')) as url FROM categories dt WHERE dt.order!=0 ORDER by dt.order ";
            $id='category_id';
       }
        
        $result = self::userLikes();
        $offer_likes = array();
        if(count($result)>0){
            $offer_likes = $result[0]['offer_id'];
        }
        if(!empty($offer_likes)){
            $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
        }
        else{
            $column = " 0 as mylikes";
        }
       
       $deal_types = Yii::app()->db->createCommand($deal_type_q)->queryAll();
       $i=0;
       $query = "";
       $allgifts = array();
       if(count($deal_types)>0){
        foreach($deal_types as $types){
           $typesId = $types['id'];
           if($type=='category'){
                $condition = " d.category_id =$typesId AND d.category_id !=1";
           }
           else{
               $condition = " d.type_id =$typesId ";
              }
           if($i>0){
              $query .= " UNION ALL "; 
           }
           $query .= "SELECT of.id as offer_id,d.by_voucher,of.offer_price,d.type_id,d.category_id,of.name AS offer, d.name AS deal,$column,  (CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url, (CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',di.image_url) END as logo
                        FROM deals AS d
                        INNER JOIN offers AS of ON ( d.id = of.deal_id ) 
                        LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id ) 
                        WHERE $condition
                        AND of.sequence =1 AND d.status=1 AND of.status=1 "
                   . "";
           
           $i++;
       }
       $result= Yii::app()->db->createCommand($query)->queryAll();
       $deals = array();
       $j=0;
       foreach($deal_types as $types){
           $k=0;
            foreach($result as $row){
                if($types['id']==$row[$id]){
                    $allgifts[$j]['name'] = $types['name'];
                    $allgifts[$j]['id'] = $types['id'];
                    $allgifts[$j]['url'] = $types['url'];
                    $allgifts[$j]['details'][$k]=$row;
                    $k++;
                }
            }
            $j++;
       }
      }
       $categories_q = "SELECT id,name,LOWER(REPLACE(REPLACE(name,' ',''),'&','')) as url,(CASE WHEN length(c.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',c.img_url) ELSE  c.img_url END) as img_url FROM categories c WHERE c.isActive=1 AND c.id !=1 AND c.order!=0 ORDER by c.order ";
       
       $categories= Yii::app()->db->createCommand($categories_q)->queryAll();
       $deals['data'] = $allgifts;
       if(count($categories)>0){
       $deals['category'] = $categories;
       }else{
       $deals['category'] = array();
       }
    } catch(Exception $ex){
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
            }
       $responce = array('status'=>'success','errCode'=>0,'description'=>$deals);
       return $responce;
   }
   
   public static function getGiftsByType_WEB($data){
       Yii::app()->user->id = (isset($data['user_id']) && !empty($data['user_id'])) ? $data['user_id'] : Yii::app()->user->id;
        $type = isset($data['type']) ? $data['type']: 0;
        $type_id = isset($data['type_id']) ? $data['type_id']: 0;
        $next = isset($data['next']) ? $data['next']: 0;
        $connection = Yii::app()->db;
        $start = 0;
        $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
        $condition = 1;
        $limitcond = "";
        $column = "";
        if(!empty($next) && $next!=-1){
            $start = ($next * 10);
        }

        $limitcond =  "LIMIT $start,$end ";
        $currentDate = date('Y-m-d h:i:s');
        if($next==-1){
            $limitcond = "";
        }
       //END FOR pagination
        try{
         if(empty($type) || empty($type_id)){
           throw new Exception("Please pass parameter type as (gift or category) and type_id in number");
         }else{
           $offer_likes = array();  
           $result = self::userLikes();
           if(count($result)>0){
                $offer_likes = $result[0]['offer_id'];
           }
           if(!empty($offer_likes)){
                $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
           }else{
                 $column = " 0 as mylikes";
           }
        
           $condition = 1;
           if($type=='gift'){
               $joinCondition = " LEFT JOIN deal_types as j ON (j.id=d.type_id AND j.order!=0) ";
               $condition = " d.type_id =$type_id ";
           }elseif ($type=='category'){
                $joinCondition = " LEFT JOIN categories as j ON (j.id=d.category_id  AND j.order!=0) ";
                $condition = " d.category_id =$type_id AND d.category_id !=1 ";
            }else{
                throw new Exception("Please pass parameter type as gift or category only");
            }
            
           }
            $query = "SELECT la.name as area,d.deal_url,d.by_voucher,of.offer_price,of.offer_desc as offer_desc,of.deal_id, "
                    . "url,of.id,of.name AS offer, "
                    . "d.name AS deal, $column,"
                    . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',"
                    . "d.img_url) ELSE  d.img_url END) as img_url, "
                    . "(CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',"
                    . "d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,"
                    . "CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',
                        di.image_url) END as logo
                            FROM deals AS d
                            INNER JOIN offers AS of ON ( d.id = of.deal_id ) 
                            LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id )
                            LEFT JOIN deal_locations AS dl ON ( dl.deal_id = d.id )
                            LEFT JOIN locator_area AS la ON ( dl.area_id = la.id )
                            $joinCondition
                            WHERE $condition AND d.status=1 AND of.status=1  AND (((validity_mode=1 OR validity_mode=3) AND (total_stock > stock_sold OR total_stock=-1)) OR (validity_mode=2 AND validity > '$currentDate' ))
                            AND of.sequence =1 group by deal_id $limitcond ";
            //file_put_contents("/tmp/gifts.log", " |".  $query." \n", FILE_APPEND | LOCK_EX);
            $result = Yii::app()->db->createCommand($query)->queryAll();
            
            $dealsArray = array();
            $dealsids = array();
         $dealsArray2 = array();	
	foreach($result as $rows){
                    if(!empty($rows)){
                    $id = $rows['deal_id'];
                    if(!in_array($rows['deal_id'], $dealsids)){
                        $dealsids[] = $rows['deal_id'];
                        $dealsArray[$id]['id'] = $rows['id'];
                        $dealsArray[$id]['area'] = $rows['area'];
                        $dealsArray[$id]['by_voucher'] = $rows['by_voucher'];
                        $dealsArray[$id]['offer_price'] = $rows['offer_price'];
                        $dealsArray[$id]['offer_desc'] = $rows['offer_desc'];
                        $dealsArray[$id]['deal_id'] = $rows['deal_id'];
                        $dealsArray[$id]['url'] = $rows['url'];
                        $dealsArray[$id]['offer'] = $rows['offer'];
                        $dealsArray[$id]['mylikes'] = $rows['mylikes'];
                        $dealsArray[$id]['img_url'] = $rows['img_url'];
                        $dealsArray[$id]['L_img_url'] = $rows['L_img_url'];
                        $dealsArray[$id]['min_amount'] = $rows['min_amount'];
                        $dealsArray[$id]['logo'] = $rows['logo'];
                        $dealsArray[$id]['deal'] = $rows['deal'];
                        $dealsArray[$id]['deal_url'] = $rows['deal_url'];
                    }

                   
                   }
               }
               foreach($dealsArray as $row){
                   $dealsArray2[]=$row;
               }
               
            $responce = array('status'=>'success','errCode'=>0,'description'=>$dealsArray2);
        }
            catch(Exception $ex){
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
            }
        return $responce;
       }
   
    public static function getMyGifts_WEB($data){
       Yii::app()->user->id = (isset($data['user_id']) && !empty($data['user_id'])) ? $data['user_id'] : Yii::app()->user->id;
        $result = self::userLikes();
        $offer_likes = array();
        if(count($result)>0){
            $offer_likes = $result[0]['offer_id'];
        }
        if(!empty($offer_likes)){
            $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
        }
        else{
            $column = " 0 as mylikes";
        }
        $type = isset($data['type']) ? $data['type']: 0;
        $type_id = isset($data['type_id']) ? $data['type_id']: 0;
        $next = isset($data['next']) ? $data['next']: 0;
        $connection = Yii::app()->db;
        $start = 0;
        $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
        $condition = 1;
        $limitcond = "";
        if(!empty($next) && $next!=-1){
            $start = ($next * 10);
        }
        $user_id = Yii::app()->user->id;
        $limitcond =  "LIMIT $start,$end ";
        $currentDate = date('Y-m-d h:i:s');
        if($next==-1){
            $limitcond = "";
        }
       //END FOR pagination
        try{
            
            $query = "SELECT d.deal_url,dt.transaction_id,c.status,$column, (CASE WHEN c.status = 1 THEN 'Active' ELSE (CASE WHEN c.status=2 THEN 'Redeem' ELSE (CASE WHEN c.status=3 THEN 'Expired' ELSE 'Active' END) END) END) as coupon_status,"
                    . "'Andheri' as area,d.by_voucher,of.offer_price,of.deal_id, LOWER(REPLACE(REPLACE(j.name,' ',''),'&','')) as url,"
                    . "of.id,of.name AS offer, of.offer_desc,  d.name AS deal,(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url, (CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',di.image_url) END as logo
                            FROM deal_transaction as dt 
                    INNER JOIN coupons as c ON (dt.coupon_id = c.id )
                    LEFT JOIN deals AS d ON (dt.deal_id=d.id)
                    LEFT JOIN offers AS of ON ( dt.offer_id = of.id ) 
                    LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id )

                    LEFT JOIN categories as j ON (j.id=d.category_id  AND j.order!=0)
                    WHERE   c.users_id=$user_id";
            
          file_put_contents("/var/log/apps/txn_failure.log", "| ".Yii::app()->user->getId()." |". $query." \n", FILE_APPEND | LOCK_EX);
            $result = Yii::app()->db->createCommand($query)->queryAll();
            $responce = array('status'=>'success','errCode'=>0,'description'=>$result);
        }
            catch(Exception $ex){
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
            }
        return $responce;
       }
        
       
       public static function getMyLikes_WEB($data){
       Yii::app()->user->id = (isset($data['user_id']) && !empty($data['user_id'])) ? $data['user_id'] : Yii::app()->user->id;
        $result = self::userLikes();
        $offer_likes = array();
        if(count($result)>0){
            $offer_likes = $result[0]['offer_id'];
        }
        if(!empty($offer_likes)){
            $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
        }
        else{
            $column = " 0 as mylikes";
        }
        $type = isset($data['type']) ? $data['type']: 0;
        $type_id = isset($data['type_id']) ? $data['type_id']: 0;
        $next = isset($data['next']) ? $data['next']: 0;
        $connection = Yii::app()->db;
        $start = 0;
        $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
        $condition = 1;
        $limitcond = "";
        if(!empty($next) && $next!=-1){
            $start = ($next * 10);
        }
        $user_id = Yii::app()->user->id;
        $limitcond =  "LIMIT $start,$end ";
        $currentDate = date('Y-m-d h:i:s');
        if($next==-1){
            $limitcond = "";
        }
       //END FOR pagination
        try{
            if(empty($offer_likes)){
                return $responce =  array('status'=>'failure','errCode'=>0,'description'=>"No offers");
            }
            $query = "SELECT $column,"
                    . "'Andheri' as area,of.deal_id, d.deal_url,LOWER(REPLACE(REPLACE(j.name,' ',''),'&','')) as url,"
                    . "of.id,of.offer_price,d.by_voucher,of.name AS offer,of.offer_desc, d.name AS deal,(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url, (CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',di.image_url) END as logo
                            FROM deals AS d 
                            LEFT JOIN offers AS of ON ( d.id = of.deal_id ) 
                            LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id ) 
                            LEFT JOIN categories as j ON (j.id=d.category_id AND j.order!=0) WHERE of.id in ($offer_likes)";
                  
            $result = Yii::app()->db->createCommand($query)->queryAll();
            $responce = array('status'=>'success','errCode'=>0,'description'=>$result);
        }
            catch(Exception $ex){
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
            }
        return $responce;
       }
       
    public static function getGiftCoinDetails_WEB($data){
        $type = isset($data['type']) ? $data['type']: 0;
        $type_id = isset($data['type_id']) ? $data['type_id']: 0;
        $next = isset($data['next']) ? $data['next']: 0;
        $first_amount = isset($data['first_amt']) ? $data['first_amt']: 0;
        $last_amount = isset($data['last_amt']) ? $data['last_amt']: 100;
        $connection = Yii::app()->db;
        $start = 0;
        $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
        $condition = 1;
        $limitcond = "";
        $column = "";
        if(!empty($next) && $next!=-1){
            $start = ($next * 10);
        }

        $limitcond =  "LIMIT $start,$end ";
        
        if($next==-1){
            $limitcond = "";
        }
       //END FOR pagination
        try{
         if(empty($type) || empty($type_id)){
           throw new Exception("Please pass parameter type as (gift or category) and type_id in number");
         }else{
           $offer_likes = array();  
           $result = self::userLikes();
           if(count($result)>0){
                $offer_likes = $result[0]['offer_id'];
           }
           if(!empty($offer_likes)){
                $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
           }else{
                 $column = " 0 as mylikes";
           }
        
           $condition = 1;
           if($type=='gift'){
               $joinCondition = " LEFT JOIN deal_types as j ON (j.id=d.type_id AND j.order!=0) ";
               $condition = " d.type_id =$type_id ";
           }elseif ($type=='category'){
                $joinCondition = " LEFT JOIN categories as j ON (j.id=d.category_id  AND j.order!=0) ";
                $condition = " d.category_id =$type_id AND d.category_id !=1 ";
            }else{
                throw new Exception("Please pass parameter type as gift or category only");
            }
            
           }
            $query = "SELECT la.name as area, LOWER(REPLACE(REPLACE(j.name,' ',''),'&','')) as url,of.id,of.name AS offer,of.offer_desc, d.name AS deal, $column,(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url, (CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',di.image_url) END as logo
                            FROM     deals AS d
                            INNER JOIN offers AS of ON ( d.id = of.deal_id ) 
                            LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id )
                            LEFT JOIN deal_locations AS dl ON ( dl.deal_id = d.id )
                            LEFT JOIN locator_area AS la ON ( dl.area_id = la.id )
                            $joinCondition
                            WHERE $condition AND d.status=1 AND of.status=1 AND (of.min_amount >=$first_amount AND of.min_amount <=$last_amount)
                            AND of.sequence =1 $limitcond ";
            
            $result = Yii::app()->db->createCommand($query)->queryAll();
            $responce = array('status'=>'success','errCode'=>0,'description'=>$result);
        }
            catch(Exception $ex){
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
            }
        return $responce;
       }    
       
   public static function getGiftDetails_WEB($data){
       try{
        $offerId = isset($data['id']) ? $data['id'] :0;
        $result =array();
        $offer_likes = array();
            if(count($result)>0){
                $offer_likes = $result[0]['offer_id'];
            }
             if(!empty($offer_likes)){
                $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
            }
            else{
                 $column = " 0 as mylikes";
            }


        $off_avg = self::get_OffAvgReview($offerId);
        $query = "SELECT of.id as offer_id,$column,d.id,of.min_amount,of.name,of.offer_desc,of.long_desc,d.name as deal,"
                . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) 
                    ELSE d.img_url END) as img_url ,(CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.L_img_url) 
                    ELSE d.L_img_url END) as L_img_url , 
                    (CASE WHEN length(dm.image_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',dm.image_url) 
                ELSE (
                CASE d.category_id WHEN 2 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_beauty.png' )
                WHEN 12 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fashion.png' )
                WHEN 7 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_services.png' )
                WHEN 15 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_other.png' )
                WHEN 14 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_learning.png' )
                WHEN 11 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_fitness.png' )
                WHEN 13 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_entertainment.png' )
                WHEN 16 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_online.png' )
                WHEN 10 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."','images/logo_food.png')  ELSE '' END ) END) as logo_url "
                . " FROM offers as of LEFT JOIN deals as d ON (d.id=of.deal_id)"
                . " LEFT JOIN deals_image_section as dm ON (d.id=dm.deal_id) "
                . " LEFT JOIN offer_details as of_d ON (of_d.deal_id=d.id) WHERE of.id=$offerId AND d.status=1 AND of.status=1 AND of.sequence=1";
        $offer = Yii::app()->db->createCommand($query)->queryRow();
        if(isset($offer['id'])){
            $dealid=  $offer['id'];
            $dealsinfo_qry = "select content_txt from deal_info where deal_id='".$dealid."' and status !=0 order by sequence asc";
            $dealsinfo1 = Yii::app()->db->createCommand($dealsinfo_qry)->queryAll();
            $offerdetails_Q = "SELECT time,day,appointment,exclusive_gender FROM offer_details WHERE deal_id=$dealid";
            $offerdetails = Yii::app()->db->createCommand($offerdetails_Q)->queryRow();
            if(!isset($offerdetails['time'])){
                $offerdetails = array();
            }
            $content = "";
            foreach ($dealsinfo1 as $k=>$v){
                $content .= html_entity_decode($v['content_txt']);
            }
            $dealsinfo = array(array('content_txt'=>$content));

            $dealslocation_qry = "select dealer_contact, dl.deal_id, dl.lat, dl.lng, dl.address, lc.name as city,ls.name as state,la.name as area,dl.full_address "
                            . "FROM deal_locations as dl "
                            . " LEFT JOIN locator_area as la ON (la.id=dl.area_id)"
                            . " LEFT JOIN locator_city as lc ON dl.city_id = lc.id "
                            . " LEFT JOIN locator_state as ls ON lc.state_id=ls.id "
                            . "WHERE dl.deal_id='".$dealid."' and dl.status !=0 ";

            $dealslocation = Yii::app()->db->createCommand($dealslocation_qry)->queryAll();
            $deal_image_list = self::get_gallery_list($dealid);
            $deal_image_gallery = array();
            foreach ($deal_image_list as $k=>$v){
               // unset($deal_image_list[$k]['deal_id'],$deal_image_list[$k]['uploaded_date'],$deal_image_list[$k]['status']);
                 $deal_image_gallery[] =  $v['image_url'];
            }
            $result = array('status'=>'success','errCode'=>0,
                    'description'=>array('deal_detail'=>  (GeneralComponent::replaceword("&nbsp;", " ", GeneralComponent::removeSlashes($dealsinfo))),
                                         'offer'=>  (GeneralComponent::replaceword("&nbsp;", " ",GeneralComponent::removeSlashes($offer))),
                                         'location_detail'=>  GeneralComponent::replaceword("&nbsp", "&nbsp;",GeneralComponent::removeSlashes($dealslocation)),
                                         'image_list'=>$deal_image_gallery,
                                         'off_avg'=>"$off_avg",
                                         'offer_details'=>$offerdetails),
                                       );

        }else{
             $result = array('status'=>'success','errCode'=>0,'description'=>'No offer exist with this offer id');
         }
        }
        catch(Exception $ex){
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
   
   public static function getAllCategory_WEB($data=null){
        try{
            $AllCategory = array();
         if (isset($data['type']) && $data['type']=='category') {
            $categories_q = "SELECT LOWER(REPLACE(REPLACE(c.name,' ',''),'&','')) as url,id,name,(CASE WHEN length(c.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',c.img_url) ELSE  c.img_url END) as img_url FROM categories c WHERE c.isActive=1 AND c.id !=1 AND c.order!=0 ORDER by c.order ";
            $categories= Yii::app()->db->createCommand($categories_q)->queryAll();
            $AllCategory = $categories;
         }
         elseif (isset($data['type']) && $data['type']=='gifts'){
             $deal_type_q = "SELECT  LOWER(REPLACE(REPLACE(dt.name,' ',''),'&','')) as url,id,name FROM deal_types dt WHERE dt.order!=0 ORDER by dt.order ";
             $deal_type= Yii::app()->db->createCommand($deal_type_q)->queryAll();
             $AllCategory = $deal_type;
         }
         else{  
              $query = "SELECT LOWER(REPLACE(REPLACE(c.name,' ',''),'&','')) as url,c.name,c.id,off.name as offer, SUBSTRING_INDEX( GROUP_CONCAT( 
                    CASE WHEN off.sequence =1 AND off.status =1 THEN off.id END ORDER BY off.id ) ,  ',', 10 ) AS off_id
                    FROM offers AS off
                    LEFT JOIN deals AS d ON ( off.deal_id = d.id ) 
                    LEFT JOIN categories AS c ON ( d.category_id = c.id ) 
                    where  c.id!=1 AND c.order!=0 AND d.status=1 AND off.status=1 AND off.sequence=1
                    GROUP BY c.id";
                $result = Yii::app()->db->createCommand($query)->queryAll();

                $result1 = self::userLikes();
                $offer_likes = array();
                if(count($result1)>0){
                    $offer_likes = $result1[0]['offer_id'];
                }
                 if(!empty($offer_likes)){
                    $column = " CASE WHEN of.id IN ($offer_likes) THEN '1' ELSE '0' END as mylikes ";
                }
                else{
                     $column = " 0 as mylikes";
                }
                $AllOffer_Q = "SELECT of.id,of.name AS offer, d.name AS deal, $column,(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url, (CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',di.image_url) END as logo
                        FROM deals AS d
                        INNER JOIN offers AS of ON ( d.id = of.deal_id ) 
                        LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id ) 
                        WHERE d.category_id !=1 AND d.status=1 AND of.status=1 AND of.total_stock > of.stock_sold 
                        AND of.sequence =1  ";
                $AllOffer = Yii::app()->db->createCommand($AllOffer_Q)->queryAll();

                $i=0;
                foreach ($result as $row){
                    $AllCategory[$i]['category'] = $row['name'];
                    $AllCategory[$i]['category_id'] = $row['id'];
                    $AllCategory[$i]['url'] = $row['url'];
                    $offers = explode(',',$row['off_id']);
                    foreach($offers as $off){
                        $j=0;
                        foreach($AllOffer as $offer_row){

                            if($off==$offer_row['id']){
                                $AllCategory[$i]['category_details'][] = $offer_row;

                            } 
                        }
                        $j++;
                    }
                    $i++;
                 }
            }
             $responce = array('status'=>'success','errCode'=>0,'description'=>$AllCategory);
        }
        catch(Exception $ex){
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $responce;
    }
 
   public static function getGiftInfo_WEB($data=null){
       $data = array();
       if(isset($data['type']) && $data['type']=='category'){
           $type = 'categories';
           $query = "SELECT id, name,url
                        FROM categories t
                        INNER JOIN (
                        SELECT DISTINCT category_id AS c_id
                        FROM  `deals` 
                        WHERE STATUS =1
                        ) AS d
                        WHERE t.order !=0
                        AND d.c_id = t.id
                        ORDER BY t.order";
           $result = Yii::app()->db->createCommand($query)->queryAll();
           $data['categories'] = $result;
       }elseif(isset($data['type']) && $data['type']=='gift'){
           $query = "SELECT id, name, url
                        FROM deal_types t
                        INNER JOIN (

                        SELECT DISTINCT type_id
                        FROM  `deals` 
                        WHERE STATUS =1
                        ) AS d
                        WHERE t.order !=0
                        AND d.type_id = t.id
                        ORDER BY t.order";
           $result = Yii::app()->db->createCommand($query)->queryAll();
           $data['gift'] = $result;
       }else{
           $query = "SELECT id, name,url
                        FROM categories t
                        INNER JOIN (
                        SELECT DISTINCT category_id AS c_id
                        FROM  `deals` 
                        WHERE STATUS =1
                        ) AS d
                        WHERE t.order !=0
                        AND d.c_id = t.id
                        ORDER BY t.order";
           $result = Yii::app()->db->createCommand($query)->queryAll();
            $data['categories'] = $result;
           $query1 = "SELECT id, name,url
                        FROM deal_types t
                        INNER JOIN (

                        SELECT DISTINCT type_id
                        FROM  `deals` 
                        WHERE STATUS =1
                        ) AS d
                        WHERE t.order !=0
                        AND d.type_id = t.id
                        ORDER BY t.order";
           $result1 = Yii::app()->db->createCommand($query1)->queryAll();
           $data['gift'] = $result1;
       }
       return array('status'=>'success','errocode'=>0,'description'=>$data);
    }
    
    public static function myLikesCount($data=null){
        $user_gifts = self::getUserGiftCounts();
        $user_data = array();
        if(count($user_gifts)>0){
            $user_data['total_gifts']= isset($user_gifts[0]['total_gifts']) ? $user_gifts[0]['total_gifts']: 0;
            $user_data['total_redeem']= isset($user_gifts[0]['total_redeem']) ? $user_gifts[0]['total_redeem']: 0;
            $user_data['total_mylikes']= isset($user_gifts[0]['offer_id']) ? count(explode(",",$user_gifts[0]['offer_id'])): 0;
            $user_data['total_myreviews']= isset($user_gifts[0]['reviews']) ? count(explode(",",$user_gifts[0]['reviews'])): 0;
        }
        return $user_data;
    }
  
    public static function getPayuToken($data){
         $payu_detail = OnlineComponent::payu_getconfig();
       // $service_api_data = array('var1' => $data['var1'],'var2' => $data['var2'],'var3' => $data['var3'],'var4' => $data['var4'],'var5' => $data['var5'],'var6' => $data['var6'],'var7' => $data['var7'],'var8' => $data['var8'], 'command' => $data['service_type'], 'key' => $payu_detail['KEY']);
        $service_api_data = array('var1' => $_REQUEST['var1'],'command' => $data['service_type'], 'key' => $payu_detail['KEY']);
                $hash = hash("sha512", $payu_detail['KEY'] . "|".$service_api_data['command']."|" . $_REQUEST['var1'] . "|" . $payu_detail['SALT']);
                $service_api_data = array_merge($service_api_data, array('hash' => $hash));
        $result = unserialize(GeneralComponent::curl($payu_detail['SERVICE_URL'], $service_api_data, 'post'));
        var_dump($result);
    }
    
    public static function getuser_card($data){
        try{
        if(isset($data['api'])) unset($data['api']);
        if(isset($data['actiontype'])) unset($data['actiontype']);
        if(isset($data['request_via'])) unset($data['request_via']);
        $message = Yii::app()->params['message_template']['VALID_PARAM'];
        if(!isset($data['command']) || !isset($data['var1']))
            throw new Exception($message, 580);
        
        $payu_detail = OnlineComponent::payu_getconfig();
        $service_api_data = $data;
        $hash = hash("sha512", $payu_detail['KEY'] . "|".$data['command']."|" . $data['var1'] . "|" . $payu_detail['SALT']);
        $service_api_data = array_merge($service_api_data, array('hash' => $hash,'key' => $payu_detail['KEY']));
        $result = unserialize(GeneralComponent::curl($payu_detail['SERVICE_URL'], $service_api_data, 'post'));
        //var_dump($result);
        return $result;
        }
        catch(Exception $ex){
            return array('status'=>$ex->getCode(),'msg'=>$ex->getMessage());
        }
    }
    
    public static function Coupon_code_CSV($data){
       $max = $data['total_code'];
       $list = array();
       $prefix = isset($data['prefix']) ? $data['prefix']: "";
       $j = 0;
       for($i = 0 ; $i < $max ; $i++ ){
           $suffix =  GeneralComponent::generate_unique_number(8 -(strlen($prefix)),FALSE);
          $list[$j++] = $prefix.$suffix;
       }
       $file = fopen("/tmp/coupon.csv","w");
       foreach ($list as $line){
         fputcsv($file,explode(',',$line));
       }
       return fclose($file);
   }
   
   public static function renameImg(){
       
       $path=Yii::app()->params['GLOBALS']['_IMG_DIR'];
       $query="SELECT id,img_url,L_img_url FROM deals where img_url is not null";
       $result = Yii::app()->db->createCommand($query)->queryAll();
       foreach($result as $row){
          // var_dump(file_exists($row['img_url']));//exit;
           if(!empty($row['img_url']) && file_exists($row['img_url'])){
               $ext = explode('.', $row['img_url']);
           if(copy($row['img_url'], 'images/Deal_'.$row['id'].".".end($ext))){
               //rename($path.$row['img_url'], $path.'Deal_'.$row['id'].end($ext));
               Yii::app()->db->createCommand("UPDATE deals SET img_url='images/".'Deal_'.$row['id'].".".end($ext)."' where id=".$row['id'])->execute();
      
           }
         }
       }
   }
   
   public static function giftsearch($data){

        $type = isset($data['type']) ? $data['type']: 0;  
//        $next = isset($data['next']) ? $data['next']: 0;
        $data['latitude'] = ($data['latitude']==="null" || $data['latitude'] == "")?1:$data['latitude'];
        $data['longitude'] = ($data['longitude']==="null" || $data['longitude'] == "")?1:$data['longitude'];
        $lat = deg2rad($data['latitude']);
        $lng = deg2rad($data['longitude']);
        $eradius = 6371;
        $connection = Yii::app()->db;
        $condition = 1;
        
//        $start = 0;
//        $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
//        $limitcond = "";
//        if(!empty($next) && $next!=-1){
//            $start = ($next * 10);
//        }
//        $limitcond =  "LIMIT $start,$end ";
//        if($next==-1){
//            $limitcond = "";
//        }
        
        if(isset($data['name']) && !empty($data['name'])){
            $name = strtolower(trim($data['name']));
            if($type=='brand'){
               $condition = " LOWER(d.name) LIKE '%$name%' ";
            }elseif ($type=='area'){
               $condition = " LOWER(la.name) LIKE '%$name%' ";
            }
            $query = "SELECT d.id as deal_id , la.name as area,of.deal_id, dl.lat, dl.lng,of.offer_desc, LOWER(REPLACE(REPLACE(d.name,' ',''),'&','')) as url,"
                    . "of.id,of.name AS offer, d.name AS deal,"
                    ."(acos(sin($lat)*sin(radians(dl.lat)) + cos($lat)*cos(radians(dl.lat))*cos(radians(dl.lng)- $lng)) * $eradius) As distance, "
                    . "(CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',"
                    . "d.img_url) ELSE  d.img_url END) as img_url, "
                    . "(CASE WHEN length(d.L_img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',"
                    . "d.L_img_url) ELSE  d.L_img_url END) as L_img_url, of.min_amount,"
                    . "CASE WHEN ISNULL(di.image_url) THEN '' ELSE concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',
                        di.image_url) END as logo
                     FROM     deals AS d
                     INNER JOIN offers AS of ON ( d.id = of.deal_id ) 
                     LEFT JOIN deals_image_section AS di ON ( di.deal_id = d.id )
                     LEFT JOIN deal_locations AS dl ON ( dl.deal_id = d.id )
                     LEFT JOIN locator_area AS la ON ( dl.area_id = la.id )
                     WHERE d.status=1 AND of.status=1 
                     AND of.sequence =1 AND $condition ORDER BY distance asc";
            //file_put_contents("/tmp/search_gift_web.log", "| ".Yii::app()->user->getId()." |". $query." \n", FILE_APPEND | LOCK_EX);   
           try{
            $deals = Yii::app()->db->createCommand($query)->queryAll();
           }
            catch(Exception $ex){
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $responce =  array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
            return $response;
        }
            
         if(count($deals) < 1){ $deals['id']=0; }
            $dealsids = array(); //content_text related array
            $i=0;
            $dealsArray = array();
            $dealsArray2 = array();
            if(count($deals)>0){
                foreach($deals as $rows){
                    if(!empty($rows)){
                    $id = $rows['id'];
                    if(!in_array($rows['id'], $dealsids)){
                        $dealsids[] = $rows['id'];
                        $dealsArray[$id]['id'] = $rows['deal_id'];
                        $dealsArray[$id]['area'] = $rows['area'];
                        $dealsArray[$id]['deal_id'] = $rows['deal_id'];
                        $dealsArray[$id]['lat'] = $rows['lat'];
                        $dealsArray[$id]['lng'] = $rows['lng'];
                        $dealsArray[$id]['offer_desc'] = $rows['offer_desc'];
                        $dealsArray[$id]['url'] = $rows['url'];
                        $dealsArray[$id]['ofid'] = $rows['id'];
                        $dealsArray[$id]['offer'] = $rows['offer'];
                        $dealsArray[$id]['deal'] = $rows['deal'];
                        $dealsArray[$id]['distance'] = $rows['distance'];
                        $dealsArray[$id]['img_url'] = $rows['img_url'];
                        $dealsArray[$id]['L_img_url'] = $rows['L_img_url'];
                        $dealsArray[$id]['min_amount'] = $rows['min_amount'];
                        $dealsArray[$id]['logo'] = $rows['logo'];
                        $i++;
                    }
                   }
                }
               foreach($dealsArray as $row){
                   $dealsArray2[]=$row;
               }
            }
            
             if(count($dealsArray2) > 0){
                 $response = array('status'=>'success','errCode'=>0,'description'=>$dealsArray2);
             }else{
                 $response =  array('status'=>'failure','errCode'=>'241','description'=>'No gift found with searching criteria');
             }

        }else{
            $response =  array('status'=>'failure','errCode'=>'241','description'=>'Please provide brand name to search.');
        }
        return $response;
            
    }
 }//End of DealsComponent Class