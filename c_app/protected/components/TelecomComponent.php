<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author nandan
 */
class TelecomComponent {
    
    public static function getOperator($opr_code=null){
        $cond = ($opr_code != null)?"AND lower(opr_code)='$cond'":"";
        $operator_result = Yii::app()->db->createCommand("SELECT topr.id as id, pr.name as operator, product_id from telecom_operators as topr, products as pr where topr.product_id=pr.id $cond group by product_id order by 2")->queryAll();
        $operator_list = array();
        foreach($operator_result as $k=>$v){ $operator_list[$v['id']]=$v['operator'];}
        return $operator_list;
    }
    
    public static function getCircle($msc_code,$operatorcode){
        if(strlen(trim($msc_code)) < 1 || strlen(trim($operator)) < 1) return False;
        $circle_array = Yii::app()->db->createCommand("SELECT distinct t_circle.area_code as circle_code"
                                                    ."FROM telecom_circles as t_circle, telecom_codes as t_code "
                                                    ."WHERE t_circle.area_code=t_code.area "
                                                    ."AND t_code.operator='AT' AND t_code.number='7602'")->queryAll();
        return $circle_array[0]['circle_code'];
    }
    
    public static function getOpbynumber($number){ 
        $operator_result = Yii::app()->db->createCommand("SELECT t_opr.id AS operator, tcircle.id AS circle "
                                    . "FROM  `telecom_codes` AS tcode,  `telecom_circles` AS tcircle,  `telecom_operators` AS t_opr "
                                    . "WHERE tcode.operator = t_opr.opr_code AND tcode.area = tcircle.area_code "
                                    . "AND tcode.number='".substr($number,0,4)."'")->queryAll();
        $nodata = array("operator"=>"null","circle"=>"null");
        return $result = isset($operator_result[0])?$operator_result[0]:$nodata;
    }
    
    public static function getdataOperator($opr_code=null){
        $cond = ($opr_code != null)?"AND lower(opr_code)='$cond'":"";
        $operator_result = Yii::app()->db->createCommand("SELECT topr.id as id, pr.name as operator, product_id from telecom_operators as topr, products as pr where topr.data_product_id=pr.id $cond group by data_product_id order by 2")->queryAll();
        $operator_list = array();
        foreach($operator_result as $k=>$v){ $operator_list[$v['id']]=$v['operator'];}
        return $operator_list;
    }
    
    public static function getdthOperator(){
        $operator_result = Yii::app()->db->createCommand("SELECT dth.id,prod.name as operator FROM `products` as prod, dth_operators as dth WHERE dth.product_id=prod.id and service_id='2'")->queryAll();
        $operator_list = array();
        foreach($operator_result as $k=>$v){ $operator_list[$v['id']]=$v['operator'];}
        return $operator_list;
    }
    
    public static function getPostpaidOperator(){
        $operator_result = Yii::app()->db->createCommand("SELECT bill.id,prod.name as operator FROM `products` as prod, billing_operators as bill WHERE bill.product_id=prod.id and service_id='4'")->queryAll();
        $operator_list = array();
        foreach($operator_result as $k=>$v){ $operator_list[$v['id']]=$v['operator'];}
        return $operator_list;
    }
    
    public static function getOperatorByTransaction($trans_id,$category){
        if(in_array($category, array('recharge','billing'))){
            $cond = "";
            if($category == "recharge"){
                $col = ",(CASE WHEN c.recharge_flag = 1 THEN 'MOBILE' ELSE 
                            (CASE WHEN c.recharge_flag = 2 THEN 'DTH' ELSE 
                                (CASE WHEN c.recharge_flag = 3 THEN 'DATA' ELSE 'OTHER'
                                END)
                            END)
                        END) as product,
                        (CASE WHEN c.recharge_flag = 1 THEN mobile_number ELSE 
                            (CASE WHEN c.recharge_flag = 2 THEN subscriber_id ELSE 
                                (CASE WHEN c.recharge_flag = 3 THEN mobile_number ELSE subscriber_id
                                END)
                            END)
                        END) as number";
            }
            if($category == "billing"){
                $col = ",(CASE WHEN c.payment_flag = 1 THEN 'POSTPAID' ELSE 
                            (CASE WHEN c.payment_flag = 2 THEN 'BILLING' ELSE 'OTHER'
                            END)
                        END) as product,
                        (CASE WHEN c.payment_flag = 1 THEN mobile_number ELSE 
                            (CASE WHEN c.payment_flag = 2 THEN subscriber_id ELSE subscriber_id
                            END)
                        END) as number";
            }
            
            $qry = "select c.operator_id $col FROM transactions as txn , ".$category."_transaction as c where c.transaction_id = txn.trans_id AND c.transaction_id = '$trans_id'";
            $opr_result = Yii::app()->db->createCommand($qry)->queryAll();
            if(count($opr_result) < 1){
                $opr = FALSE;
            }else{
                $opr = $opr_result[0];
            }
        }else{
            $opr = FALSE;
        }
        return $opr;
    }
    
}
