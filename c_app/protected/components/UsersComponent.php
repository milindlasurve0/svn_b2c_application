<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author nandan
 */
class UsersComponent {
    
    public static function getAdminusers() {
        $res = Yii::app()->db->createCommand("SELECT user_name from users where role=1")->queryAll();
        $result = array();
        foreach ($res as $k => $v) {
            array_push($result, $v['user_name']);
        }
        return $result;
    }
    
    public static function getReportusers() {
        $res = Yii::app()->db->createCommand("SELECT user_name from users where role=4")->queryAll();
        $result = array();
        foreach ($res as $k => $v) {
            array_push($result, $v['user_name']);
        }
        return $result;
    }
    
    public static function getUsersListByType($type_id) {
        $res = Yii::app()->db->createCommand("SELECT users.id , users.user_name, users.role , user_profile.name as user_profile_name  from users LEFT JOIN user_profile ON `users`.id = `user_profile`.user_id  where 1  AND role=$type_id")->queryAll();
        return $res;
    }
    
    public static function isAdmin($uid=null) {
        //return (in_array($uid, self::getAdminusers())) ? True : False;
        if(Yii::app()->params['GLOBALS']['_ROLE_ADMIN'] == Yii::app()->user->getState('role')/*Yii::app()->user->role*/){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function isVoucher($uid=null){
        if(Yii::app()->params['GLOBALS']['_ROLE_VOUCHER'] == Yii::app()->user->getState('role')/*Yii::app()->user->role*/){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function isReport($uid=null) {
        if(Yii::app()->params['GLOBALS']['_ROLE_REPORT'] == Yii::app()->user->getState('role')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function isVcreator($uid=null) {
        if(Yii::app()->params['GLOBALS']['_ROLE_VOUCHER'] == Yii::app()->user->getState('role')){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public static function BlackListedUsers($uid = null){
        $user_cond = is_null($uid)?"":" AND users_id=$uid AND device_id!='null'";
        //$device_id = self::getDeviceId($uid);
        //$user_cond .= !empty($device_id) ? " OR device_id=$device_id " : "";
        $res = Yii::app()->db->createCommand("SELECT users_id from black_listed_user where 1=1 ".$user_cond)->queryAll();
        $result = array();
        foreach ($res as $k => $v) {
            array_push($result, $v['users_id']);
        }
        return $result;
    }
    
    public static function getDeviceId($id){
        if(!empty($id)){
        $res = Yii::app()->db->createCommand("SELECT uuid FROM user_profile where user_id = $id")->queryAll();
        //return (count($res) > 0) ? True : False;
        return (isset($res[0]) && !empty($res[0]['uuid']))?$res[0]['uuid']:FALSE;
        }
        return false;
    }
    
    public static function isBlackListed($uid){
        if(count(self::BlackListedUsers($uid)) > 0){
            return TRUE;
        }
        return False;
    }
    
    public static function isUserBlackListed($uid){
        if(count(self::CheckBlackListedUsers($uid)) > 0){
            return TRUE;
        }
        return False;
    }
    
    public static function CheckBlackListedUsers($uid = null){
//        $user_cond = is_null($uid)?"":" AND users_id=$uid ";
//        $res = Yii::app()->db->createCommand("SELECT users_id from black_listed_user where 1=1 ".$user_cond)->queryAll();
//        $result = array();
//        foreach ($res as $k => $v) {
//            array_push($result, $v['users_id']);
//        }
//        return $result;
         $user_cond = is_null($uid)?"":" AND users_id=$uid ";
        $device_id = self::getDeviceId($uid);
        $user_cond .= !empty($device_id) ? " OR device_id='$device_id' " : "";
        $res = Yii::app()->db->createCommand("SELECT users_id from black_listed_user where 1=1 ".$user_cond)->queryAll();
        $result = array();
        foreach ($res as $k => $v) {
            array_push($result, $v['users_id']);
        }
        return $result;
    }

    public static function isDealer() {        
        if(Yii::app()->params['GLOBALS']['_ROLE_DEALER'] == Yii::app()->user->getState('role')/*Yii::app()->user->role*/){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function is_incomplete_signup($username) {
        $res = Yii::app()->db->createCommand("SELECT id from users where user_name='$username' and (password='' or password is null)")->queryAll();
        //return (count($res) > 0) ? True : False;
        return (isset($res[0]))?$res[0]['id']:FALSE;
    }
    
    public static function isUser($username) {
        $res = Yii::app()->db->createCommand("SELECT user_name from users where user_name='$username'")->queryAll();
        return (count($res) > 0) ? True : False;
    }

    public static function getIdByName($username) {
        $res = Yii::app()->db->createCommand("SELECT id from users where user_name='$username'")->queryAll();
        return (count($res) > 0) ? $res[0]['id'] : 0;
    }

    public static function createUser($data) {
        $user_otp = "";
        $recent_transaction = array();
        try {
           if(isset($data['miss_call'])){ unset($data['miss_call']); }
           if(isset($data['email'])){ unset($data['email']); }
            $newAppDownload=false;
            $data['userid'] = 0;
            $incompleteflag = FALSE;
            $incompleteflag_ck = FALSE;
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            $mandatory_fields = array('mobile_number'=>'Mobile Number');
//            $missing_params = array_diff(array_keys($mandatory_fields), array_keys($data));
//            if (count($missing_params) > 0) {
//                throw new Exception('Missing parameters : ' . implode(",", $missing_params), 209);
//            }
//            $emptylist = GeneralComponent::getEmptyElement(array_keys($mandatory_fields), $data);
//            if (count($emptylist) > 0) {
//                throw new Exception('field(s) (' . implode(',', $emptylist) . ') cannot be empty.', 210);
//            }            
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            if(!isset($data['mobile_number']) || empty($data['mobile_number']) || !is_numeric($data['mobile_number']))
                throw new Exception($message, 580);
            self::setRedisCreateUser($data['mobile_number']);  //Set Redis in b2cUserList New Created User
            if (self::isUser($data['mobile_number'])) {
                $data['userid'] = self::getIdByName($data['mobile_number']);    //Getting user_id of mobile_num
                $UserData = self::newUserProfile(array('user_id'=>$data['userid']));
                if((isset($data['miss_call'])&&($data['miss_call'] == TRUE)) && (isset($data['mobile_number']))){
                    $user_type = $UserData['user_type'];
                    if($user_type == 0){                                        //check User_type is 0 for app
                       $data['isApp'] = 1;                                      //set isApp as 1 for App downloaded user 
                      // self::sendExistingUserMsg($data);                        //Send msg to App downloaded existing user
                    }else{
                      // self::sendExistingUserMsg($data);                        //Send msg to other existing user
                    }
                    throw new Exception('User with '.$data['miss_call'].'Missed called number is already registered ', 202);                    
                }
                $userApi = $UserData->api_version;         
                //update profile
                //self::updateProfile($data);
                $incomplete_flag_check = self::is_incomplete_signup($data['mobile_number']);
                if($incomplete_flag_check === FALSE){    
                    $incompleteflag = TRUE;
                    $incompleteflag_ck = TRUE;
                    $data['userid'] = isset($data['userid'])?$data['userid']:0;
                    throw new Exception('User already registered with this Mobile Number', 202);                    
                }else{  
                    $incompleteflag = TRUE;
                    $data['userid'] = $incomplete_flag_check;
                    $userType = $UserData->user_type;
                    if(($userType==3 || $userType==2) && !isset($data['miss_call'])){                                           //Check if User created by Poster miss call number
                       $newAppDownload=TRUE;
                    }   
                }
                 self::updateProfile($data);                                    //update profile
            }
            if($incompleteflag==FALSE){
                $userObj = new Users;
                $connection->createCommand("INSERT INTO users (user_name,mobile_number,salt,created_datetime) VALUES('" . $data['mobile_number'] . "',"
                        . "'" . $data['mobile_number'] . "','" . $userObj->generateSalt(). "','" . GeneralComponent::getFormatedDate() . "')")->execute();
                $data['userid'] = $connection->lastInsertID;
                if(isset($data['miss_call'])){
                    self::InsertWalletTransaction($connection,$data); 
                }else{
                $connection->createCommand("INSERT INTO wallets (users_id) VALUES('" . $data['userid'] . "')")->execute();
                }
                self::createProfile($connection, $data);
                self::manageTmpWallet($connection, $data);
                if(isset($data['api_version']) && $data['api_version']==3){
                    $newAppDownload=true;
                }
            }
            if(!isset($data['miss_call'])){                    
            $user_otp = self::sendOTP($data);                   //Send OTP to only create user case
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => "user created successfully",'api_version'=>$data['api_version']);
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            if($ex->getCode()!=580){
                self::updateProfile($data);
                $message = Yii::app()->params['message_template']['EXCEPTION'];
            }else{
                $message = $ex->getMessage();
            }
            
//             if($newAppDownload){ 
//                WalletComponent::refill_loyalty_wallet(array('userid'=>$data['userid'],'api_version'=>$data['api_version']));
//             }
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }

        $userid = self::getIdByName($data['mobile_number']);
        try{
            $data['mobile'] = $data['mobile_number'];
            ServiceComponent::set_quickpay_via_solr($data);                
        } catch (Exception $ex) {
            $error = $ex->getTraceAsString();
        }
        $wallet_info = isset($data['mobile_number'])?WalletComponent::getWalletDetail($userid):"";
        $quicklist = ServiceComponent::get_quickpaylist($userid, '6'); 
        $recent_transaction = ($quicklist['status'] === "failed") ? $recent_transaction:$quicklist['description'];
        $wallet_amount = isset($wallet_info['account_balance'])?$wallet_info['account_balance']:0;
        $wallet_loyalty_points = isset($wallet_info['loyalty_points'])?$wallet_info['loyalty_points']:0;
        $loyalty_points = 0;    
        $support_number = Yii::app()->params['GLOBALS']['B2C_SUPPORT_MISSED_CALL_NUMBER'];
        //if($incompleteflag === FALSE && $incompleteflag_ck === FALSE){
        $sid = NULL;
        $sname = NULL;
        if(!isset($data['miss_call'])){
          if($incompleteflag_ck === FALSE){
            //self::updatePassword(array('mobile_number'=>$data['mobile_number'],'password'=>$user_otp,'confirm_password'=>$user_otp,'code'=>$user_otp));
            $passwd = md5($user_otp);
            self::activateUser($data,$user_otp);
          }else{
            $passwd = self::getuser_password($data);//'SID'=>  session_id()
            self::activateUser($data,$passwd,FALSE);
          }
        }else{
            $passwd = '';
            self::activateUser($data,$passwd,FALSE);
        }
        $sid = session_id();
        $sname = session_name();
        $errorMsg = GeneralComponent::get_errorMsg_list();
        $Deals_offer = array();
        $user_data = array();
        if(isset($data['api_version']) && $data['api_version']==3){
            $Deals_offer=  json_decode(DealsComponent::getAllofferTypes(array(),array(2)),true);//2=> Top offers
            $user_data['total_gifts']= isset($user_gifts[0]['total_gifts']) ? $user_gifts[0]['total_gifts']: 0;
            $user_data['total_redeem']= isset($user_gifts[0]['total_redeem']) ? $user_gifts[0]['total_redeem']: 0;
            $user_data['total_mylikes']= isset($user_gifts[0]['gifts']) ? count(explode(",",$user_gifts[0]['gifts'])): 0;
            $user_data['total_myreviews']= isset($user_gifts[0]['reviews']) ? count(explode(",",$user_gifts[0]['reviews'])): 0;
            $user_data['OfferReview'] = Yii::app()->params['GLOBALS']['_Offer_Reviews'];
            if($newAppDownload){
                $user_data['newApp'] = "true";
            }
            else{
                $user_data['newApp'] = "false";
            }

            if($newAppDownload){
                $opening_bal = 0;
                $connection = Yii::app()->db;
                $loyalty_points = Yii::app()->params['GLOBALS']['_New_User_LoyaltyPoints'];    
                $trans_amount   = $loyalty_points;
                $closing_bal    = $loyalty_points;
              if(isset($wallet_loyalty_points) && ($wallet_loyalty_points > 0)){
                $opening_bal    = $wallet_loyalty_points;
                $trans_amount   = $loyalty_points;
                $loyalty_points = $loyalty_points + $wallet_loyalty_points;    
                $closing_bal    = $loyalty_points;
              }
                $user_data['msg_title'] = Yii::app()->params['GLOBALS']['_New_User_Title'];
                $content = Yii::app()->params['notification_template']['NEW_USER'];
                $message = GeneralComponent::replaceword('<'.'COIN'.'>', $loyalty_points, $content);
                $user_data['msg_body'] =$message;
                WalletComponent::refill_loyalty_wallet(array('loyalty_points'=>$loyalty_points,'userid'=>$data['userid']));
                
                $Trans_data = array('opening_bal'=>$opening_bal,'transaction_amount'=>$trans_amount,'closing_bal'=>$closing_bal,
                'trans_category'=>'signup','users_id'=>$data['userid'],'trans_type'=>'1','transaction_mode'=>'bonus',
                'track_id'=>Yii::app()->session['Transaction_trackid'],'trans_datetime'=>GeneralComponent::getFormatedDate(),
                     'recharge_flag'=>3,'status'=>2,'loyalty_points'=>$loyalty_points,'free_points'=>1);
                TransactionComponent::newTransaction($connection, $Trans_data);
                $Trans_data['transaction_id'] = $connection->lastInsertID;
                TransactionComponent::updateTransaction($connection,$Trans_data['transaction_id'],array('ref_trans_id'=>$Trans_data['transaction_id']));
                
            }else{
                $loyalty_points = isset($wallet_info['loyalty_points'])? $wallet_info['loyalty_points']:0;
                $user_data['msg_title'] = "";
                $user_data['msg_body'] = "";
            }
            $user_data['loyalty_points'] = $loyalty_points;
            
        }
        Yii::app()->user->logout();
        if(isset($data['request_via']) && $data['request_via'] === "web"){
           $result['description'] = array_merge(array('otp'=>md5($data['mobile_number']).$passwd,'ErrorMsg'=>$errorMsg,'Deals_offer'=>$Deals_offer,'loyalty_points'=>$loyalty_points),$user_data);
           return $result = $result;
        }
//        $result['description'] = array_merge(array('otp'=>md5($data['mobile_number']).$passwd,'balance'=>$wallet_amount,
//            'loyalty_points'=>$loyalty_points,'recent_pay1_transactions'=>$recent_transaction,
//            'support_number'=>$support_number,'ErrorMsg'=>$errorMsg,
//            'Deals_offer'=>$Deals_offer,'OfferReview'=>Yii::app()->params['GLOBALS']['_Offer_Reviews']),$user_data);
        if(isset($data['api_version']) && $data['api_version']==3){
            $result['description'] = array_merge(array('otp'=>md5($data['mobile_number']).$passwd,'balance'=>$wallet_amount,
            'recent_pay1_transactions'=>$recent_transaction,
            'support_number'=>$support_number,'ErrorMsg'=>$errorMsg,'user_val'=>"",'user_val_name'=>"",
            'Deals_offer'=>$Deals_offer,'OfferReview'=>Yii::app()->params['GLOBALS']['_Offer_Reviews']),$user_data);
        }elseif(isset($data['miss_call'])){
             $result['description'] = array_merge(array('loyalty_points'=>$wallet_loyalty_points,
            'recent_pay1_transactions'=>$recent_transaction,
            'support_number'=>$support_number,'ErrorMsg'=>$errorMsg,'session_id'=>$sid,'session_name'=>$sname,
            'Deals_offer'=>$Deals_offer,'OfferReview'=>Yii::app()->params['GLOBALS']['_Offer_Reviews']),$user_data);
        }else{
            $result['description'] = array_merge(array('otp'=>md5($data['mobile_number']).$passwd,'balance'=>$wallet_amount,
            'recent_pay1_transactions'=>$recent_transaction,
            'support_number'=>$support_number,'ErrorMsg'=>$errorMsg,'session_id'=>$sid,'session_name'=>$sname,
            'Deals_offer'=>$Deals_offer,'OfferReview'=>Yii::app()->params['GLOBALS']['_Offer_Reviews']),$user_data);
        }
        
      //  $result['description'] = array('otp'=>md5($data['mobile_number']).$passwd,'balance'=>$wallet_amount,'recent_pay1_transactions'=>$recent_transaction,'support_number'=>$support_number);
        return $result;
    }
    
     /**
	 * Create InsertWalletTransaction()
	 *
	 * @static
	 * @access	public
	 * @param	mobile_number isnew api_version email
         * @descp       create user if not exist 
	 * @return	if new user then success with userinfo else failure
	 */
    public static function create_Newuser($data){
        if(isset($data['mobile_number']) && isset($data['api_version']) && filter_var($data['mobile_number'], FILTER_VALIDATE_FLOAT) && filter_var($data['api_version'], FILTER_VALIDATE_INT) && $data['api_version']==4 && strlen((string)$data['mobile_number'])==10){
            //check user exist ?
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            try{
                $param = array('mobile_number'=>$data['mobile_number']);
                $userData = self::getUserByCol($param);
                if(empty($userData) ){
                    //newUser so register and sendotp
                    self::pre_register($connection,$data);
                    
                }elseif(isset($userData['password']) && empty($userData['password']) ){
                    //existing user without password
                    
                }else if(isset($userData['password']) && !empty ($userData['password'])){
                    //existing user with password
                    $message = Yii::app()->params['message_template']['EXISTING_USER'];
                    throw new Exception($message, 202);
                }
                $message = Yii::app()->params['message_template']['USER_AUTH_OTP'];
                $user_otp = self::sendOTP($data);
                $transaction->commit();
                $result = array('status' => 'success', 'errCode' => '0', 'description' => $message);
                
            }catch(Exception $ex){
                $transaction->rollback();
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $message = $ex->getMessage();
                $status = 'failure';
                if($ex->getCode()==202){
                    $message = $ex->getMessage();
                    $status = 'success';
                }
                $result = array('status' => $status, 'errCode' =>$ex->getCode().'', 'description' => $message);
                
            }
            
        }else{
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            $result = array('status' => 'failure', 'errCode' =>'291', 'description' => $message);
        }
        return $result;
    }

    
    /**
	 * Create InsertWalletTransaction()
	 *
	 * @static
	 * @access	public
	 * @param	$connection & array $data
         * @descp       insert entries into transaction & wallet table after mapping loyality points on the basis of amount 
         *              and flag if amount exist 
	 * @return	nothing
	 */
    public static function InsertWalletTransaction($connection,$data){
        $date = GeneralComponent::getFormatedDate();
        if(isset($data['amount'])&&($data['amount'] > 0)){  	    //Checking for amount availablity and should not be zero  
                $amount = $data['amount'];
                $servicePoints  = $data['flag'];                     // Assing Flag as a service points 	 
                $loyalty_points = ceil($amount*$servicePoints);      // loyality points mapping on the basis of amount & service points 
                $connection->createCommand("INSERT INTO wallets (loyalty_points , users_id, created_datetime) VALUES($loyalty_points, '" . $data['userid'] . "','".$date."')")->execute();
                $Trans_data = array('opening_bal'=>0,'transaction_amount'=>$data['amount'],'closing_bal'=>$data['amount'],
                'trans_category'=>'pay1shop','users_id'=>$data['userid'],'trans_type'=>'1','transaction_mode'=>'loyaltypoints','response_id'=>'',
                'track_id'=>'','trans_datetime'=>$date,
                     'recharge_flag'=>3,'status'=>2,'loyalty_points'=>$loyalty_points,'free_points'=>1);
                TransactionComponent::newTransaction($connection, $Trans_data);
                $data['transaction_id'] = $connection->lastInsertID;
                $data['trans_id'] = isset($data['trans_id']) ? $data['trans_id'] : $connection->lastInsertID;
                TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['trans_id']));
                //self::sendMissCallMsg($data);                        //Send Msg to only new B2C user 
            }else{
                $data['amount'] = 0 ;   
                $servicePoints = $data['flag'];                      // Assing Flag as a service points 	 
                $loyalty_points = ceil($data['amount']*$servicePoints); 
                $connection->createCommand("INSERT INTO wallets (loyalty_points , users_id, created_datetime) VALUES('" . $data['amount'] . "' , '" . $data['userid'] . "','".$date."')")->execute();         
                //ssself::sendNonUserMsg($data);                         //Send Msg to both New B2B and B2C user
            }
            unset($data['amount']);
    }
    //End of function InsertWalletTransaction()	
    
    public static function activateUser($data,$password,$eflag=TRUE){
        if($eflag === TRUE){
            $userObj = new Users();
            $userObj = $userObj->findByAttributes(array('user_name'=>$data['mobile_number']));
            if($userObj){
                $userObj->password = $password;
                $userObj->save();
            }
            $password = md5($password);
        }
         //$ldata = array('username'=>$data['mobile_number'],'password'=>$password);
         $login_model = new LoginForm();
            $login_model->setAttributes(array('username' => $data['mobile_number'], 'password' => $password,'rememberMe'=>true));
            if ($login_model->validate() && $login_model->login()) {
            }
            return true;
        // return self::login($ldata);
    }
    
    public static function isTempWallet($mobile) {
        $res = Yii::app()->db->createCommand("SELECT * from temp_wallets where mobile='$mobile'")->queryAll();
        return (count($res) > 0) ? True : False;
    }

    
    public static function callcreateProfile($connection,$data) {
        self::createProfile($connection, $data);
    }
    
    private static function createProfile($connection, $data) {
        $lat_lng = '';
        if(!isset($data['user_type'])){   
           $data['user_type'] = 0;
        }   
        if(isset($data['latitude']) && isset($data['longitude'])){
            $lat_lng = substr( $data['latitude'], 0, strpos( $data['latitude'] , '.' ) +2 )."_".substr( $data['longitude'], 0, strpos( $data['longitude'] , '.' ) +2 );
        }
        $connection->createCommand("INSERT INTO user_profile (name,mobile,gender,date_of_birth,email,user_id,gcm_reg_id,uuid,longitude,"
                . "latitude,location_src,device_type,user_type,created,api_version,latitude_longitude) VALUES('" . GeneralComponent::set_defaultBlank($data,'name') . "','" . $data['mobile_number'] . "','" . GeneralComponent::set_defaultBlank($data,'gender') . "',"
                . "'" . GeneralComponent::set_defaultBlank($data,'date_of_birth') . "','".GeneralComponent::set_defaultBlank($data,'email')."','" . $data['userid'] . "','" . GeneralComponent::set_defaultBlank($data, 'gcm_reg_id') . "',"
                . "'" . GeneralComponent::set_defaultBlank($data, 'uuid') . "','" . GeneralComponent::set_defaultBlank($data, 'longitude') . "',"
                . "'" . GeneralComponent::set_defaultBlank($data, 'latitude') . "','" . GeneralComponent::set_defaultBlank($data, 'location_src') . "',"
                . "'" . GeneralComponent::set_defaultBlank($data, 'device_type') . "','" . $data['user_type'] . "','".GeneralComponent::getFormatedDate()."','".GeneralComponent::set_defaultBlank($data, 'api_version')."','$lat_lng')")->execute();
    }

    private static function manageTmpWallet($connection, $data) {
        if (self::isTempWallet($data['mobile_number'])) {
            $tmpwalletdata = WalletComponent::getTempWalletDetail($data['mobile_number']);
            foreach ($tmpwalletdata as $tmpwallet) {
                $data['transaction_id'] = $tmpwallet['trans_id'];
                $data['amount'] = $tmpwallet['amount'];
                $data['retailer_name'] = $tmpwallet['retailer_shop'];
                $data['retailer_mobile'] = $tmpwallet['retailer_mobile'];
                $data['retailer_id'] = $tmpwallet['retailer_id'];
                $data['lat'] = $tmpwallet['lat'];
                $data['lng'] = $tmpwallet['lng'];
                WalletComponent::redeem_temp_wallet($connection, $data);
                $connection->createCommand("UPDATE transactions SET users_id='" . $data['userid'] . "' WHERE trans_id='" . $data['transaction_id'] . "' ")->execute();
            }
        }
    }
    
    private static function updateProfile($data) {
        try {
            $userId = isset($data['userid']) ? $data['userid'] : Yii::app()->user->getId();
            $uprofile = new UserProfile();
            $uprofile = $uprofile::model()->findByAttributes(array('user_id' => $userId));
            $oldemail = $uprofile->email;
            if (isset($data['name']) && $data['name'] != "")$uprofile->name =filter_var($data['name'], FILTER_SANITIZE_STRING);
            if (isset($data['mobile']) && $data['mobile'] != "")$uprofile->mobile = filter_var($data['mobile'], FILTER_SANITIZE_NUMBER_FLOAT);
            if (isset($data['email']) && $data['email'] != "")$uprofile->email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
            if (isset($data['gender']) && $data['gender'] != "")$uprofile->gender = filter_var($data['gender'], FILTER_SANITIZE_STRING);
            if (isset($data['date_of_birth']) && $data['date_of_birth'] != "")$uprofile->date_of_birth = $data['date_of_birth'];
            if (isset($data['latitude']) && $data['latitude'] != "")$uprofile->latitude = $data['latitude'];
            if (isset($data['longitude']) && $data['longitude'] != "")$uprofile->longitude = $data['longitude'];
            if ((isset($data['longitude']) && $data['longitude'] != "") && (isset($data['latitude']) && $data['latitude'] != ""))
                $uprofile->latitude_longitude= substr( $data['latitude'], 0, strpos( $data['latitude'] , '.' ) +2 )."_".substr( $data['longitude'], 0, strpos( $data['longitude'] , '.' ) +2 );
            if (isset($data['api_version']) && $data['api_version'] != "")$uprofile->api_version = filter_var($data['api_version'], FILTER_SANITIZE_NUMBER_FLOAT);;
            $uprofile->updated = GeneralComponent::getFormatedDate();
            $uprofile->save();
            if($oldemail != $uprofile->email){
                GeneralComponent::notifier(array('CHANGE_EMAIL_ID_SUCCESS'),array('user_id'=>Yii::app()->user->getId()));
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => 'user profile updated successfully');
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return json_encode($result);
    }

    public static function sendOTP($data) {
        $userOTP = new UserOtp();
        $userOTP->mobile = $data['mobile_number'];
        $otp = GeneralComponent::generate_OTP(4);
        $userOTP->otp = $otp;
        $userOTP->generatedate=  GeneralComponent::getFormatedDate();
        $userOTP->save();
        $sms_msg = GeneralComponent::replaceword('<OTP>',$otp,Yii::app()->params['message_template']['OTP_PASSWORD']);
        if(isset($data['isnew']) && $data['isnew']=="1" && $data['actiontype']=="create_user"){
            $sms_msg = GeneralComponent::replaceword('<OTP>',$otp,Yii::app()->params['message_template']['NEW_OTP_PASSWORD']);
        }
        if(isset($data['request_via']) && $data['request_via']=='web' && $data['actiontype']=="login"){
            $sms_msg = GeneralComponent::replaceword('<OTP>',$otp,Yii::app()->params['message_template']['OTP_REAUTH']);
        }
        $api_param = array('msisdn' => $data['mobile_number'], 'message' => $sms_msg);
        //VendorComponent::callsmstadka($api_param);  
        GeneralComponent::sendSMS($data['mobile_number'],$sms_msg);
        return $otp;
    }
    /**
	 * Create sendMissCallMsg()
	 *
	 * @static
	 * @access	public
	 * @param	array $data (amount and mobile number)
         * @descp       sending registeration msg to the miss call user mobile number  
	 */
    public static function sendMissCallMsg($data) {
        $notify_success_var = array();
        $notify_data        = array();
        $notify_data = array('mobile'=>$data['mobile_number'],'amount'=>$data['amount']);
        $content = Yii::app()->params['message_template']['NEW_MISS_CALL_USER_MSG'];
        $content = GeneralComponent::replaceword('<'.strtoupper('amount').'>', $data['amount'], $content);
        GeneralComponent::sendSMS($data['mobile_number'],$content);    
    }//End of function sendMissCallMsg()	
    
    /**
	 * Create sendExistingUserMsg()
	 *
	 * @static
	 * @access	public
	 * @param	array $data (account_balance, loyalty_points and mobile number)
         * @descp       sending msg to the already registered miss call user mobile number  
    */
    public static function sendExistingUserMsg($data) {
       $notify_success_var = array();
       $notify_data        = array();
       $user_id = $data['userid'];
       $content = Yii::app()->params['message_template']['B2C_EXISTING_USER'];
       if(isset($data['isApp']) && $data['isApp']=="1" && $data['actiontype']=="Create_MissCall_user"){
          $content = Yii::app()->params['message_template']['APP_EXISTING_USER'];   
       }
       $res = Yii::app()->db->createCommand("SELECT account_balance , loyalty_points from wallets where users_id = $user_id")->queryAll();
       if(count($res)>0){
          $loyality_points = $res[0]['loyalty_points'];
          $notify_data = array('mobile'=>$data['mobile_number'],'loyality_points'=>$loyality_points);
          $content = GeneralComponent::replaceword('<'.strtoupper('loyality_points').'>', $loyality_points, $content);
          GeneralComponent::sendSMS($data['mobile_number'],$content);    
        return TRUE;
       }
    }//End of function sendExistingUserMsg()
    
    /**
	 * Create sendNonUserMsg()
	 *
	 * @static
	 * @access	public
	 * @param	array $data (mobile number)
         * @descp       sending msg to both B2B & B2C new user user mobile number  
    */
    public static function sendNonUserMsg($data) {
        $notify_success_var = array();
        $notify_data        = array();
        $notify_data = array('mobile'=>$data['mobile_number']);
//        $notify_success_var = array("NON_B2B_N_B2C_USER");
//        GeneralComponent::notifier($notify_success_var,$notify_data);
        $content = Yii::app()->params['message_template']['NON_B2B_N_B2C_USER'];
       // $content = GeneralComponent::replaceword('<'.strtoupper('loyality_points').'>', $loyality_points, $content);
         GeneralComponent::sendSMS($data['mobile_number'],$content);
        
    }//End of function sendNonUserMsg()
    
    public static function verifyOTP($data,$returnObj=FALSE){
        $userOTP = new UserOtp();
        //$userOTP = $userOTP->findByAttributes(array('mobile'=>$data['mobile_number'],'otp'=>$data['code'],'status'=>'0'));
            $userOTP = $userOTP->findByAttributes(array('mobile'=>$data['mobile_number']),array('limit'=>1,'order'=>'id DESC'));        
        if(is_null($userOTP)){
            return FALSE;
        }
        $userData = $userOTP->attributes;
        if($userData['otp']!==$data['code'])
            return FALSE;
        if($userData['status'] == 0){
            if($returnObj){
                return $userOTP;
            }
            return TRUE;
        }elseif($userData['status'] == 1){
            $timediff = strtotime(GeneralComponent::getFormatedDate()) - strtotime($userData['generatedate']);
            if($timediff < 900){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        return FALSE;
    }
    
    public static function strongpass($str){
        $re = "/^                    # start of line
            (?=(?:.*[a-zA-Z]){1,})   # 1 or more lower case letters
            (?=(?:.*\\d){1,})      # 1 or more digits
            (?=(?:.*[!@#$\_]){1,})            # 1 or more special characters
            (.{6,})             # length 6 or more
            $
            /mx"; 
       $result = preg_match_all($re, $str, $matches);
       return $result;
    }


    public static function updatePassword($data){
            if(!(isset($data['password']) && !empty($data['password']) && self::strongpass($data['password']) && strlen($data['password'])>=6)){
                $stronpass = Yii::app()->params['message_template']['STRONG_PASS'];
               return $result = json_encode(array('status'=>'failure','errCode'=> '291','description'=>$stronpass));
            }
            if(isset($data['vtype']) && $data['vtype']==1){
                return self::updatePassword_new($data);
            }
        try{
            $mandatory_fields = array('mobile_number'=>'Mobile Number', 'password'=>'new Password', 'confirm_password'=>'Confirm Password');
            $emptylist = GeneralComponent::getEmptyElement(array_keys($mandatory_fields), $data);
            if (count($emptylist) > 0) {
                throw new Exception('field(s) (' . implode(',', $emptylist) . ') cannot be empty.', 210);
            }
            if($data['password']!=$data['confirm_password']){
                throw new Exception("confirm password mismatch",212);
            }
            if(is_null(Yii::app()->user->getId())){
                if(!self::verifyOTP($data)){
                    throw new Exception("Invalid OTP",213);
                }
                $optobj = self::verifyOTP($data,TRUE);
                if(is_object($optobj)){
                $optobj->status=1;
                $optobj->useddate=  GeneralComponent::getFormatedDate();
                $optobj->save();
                }
            }
            $userObj = new Users();
            $userObj = $userObj->findByAttributes(array('mobile_number'=>$data['mobile_number'],'user_name'=>$data['mobile_number']));
            if(is_null($userObj)){
                throw new Exception("User does not exist",214);
            }
            if(!is_null(Yii::app()->user->getId())&& $userObj->password != md5($data['current_password']) && $data['code'] == ""){
                $hash = $data['mobile_number']."-".date('Y-m-d');
                if(Yii::app()->cache->hexists($hash,1)){
                    if(Yii::app()->cache->hget($hash,1)<=5){
                        Yii::app()->cache->hincrby($hash,1,1);
                    }else{ 
                           // file_put_contents("/tmp/check_block_user", " STEP3 " . json_encode($data) . "\n", FILE_APPEND | LOCK_EX);
                            //block user and send mail to team
                            self::limituserLogin($data);
                    } 
                
                }
                throw new Exception("Incorrect current password",224);
            }else{
                $hash = $data['mobile_number']."-".date('Y-m-d');
                 if(Yii::app()->cache->hexists($hash,1)){
                     Yii::app()->cache->hdel($hash,1);
            }
            }
            $old_passwd = $userObj->password;
            $userObj->password= $data['password'];
            $userObj->save();
            if($old_passwd == "" || $old_passwd == NULL){
                GeneralComponent::notifier(array('SUCCESS_REGISTRATION'),array('user_id'=>$userObj->id));
            }elseif($old_passwd !== $userObj->password){
                GeneralComponent::notifier(array('CHANGE_PASSWORD_SUCCESS'),array('user_id'=>$userObj->id));
            }
            $result = array('status'=>'success','errCode'=>'0','description'=>'Password set successfully');
        } catch (Exception $ex) {
            $msg = 'something went wrong';
            if($ex->getCode()==212 || $ex->getCode()==213 || $ex->getCode()==214 || $ex->getCode()==219 || $ex->getCode()==224)
            $msg = $ex->getMessage();
            $result = array('status'=>'failure','errCode'=> $ex->getCode(),'description'=>$msg);
        }
        return json_encode($result);
    }
    
    public static function updatePassword_new($data){
        try{
            $mandatory_fields = array('mobile_number'=>'Mobile Number', 'password'=>'new Password', 'confirm_password'=>'Confirm Password');
            $emptylist = GeneralComponent::getEmptyElement(array_keys($mandatory_fields), $data);
            if (count($emptylist) > 0) {
                throw new Exception('field(s) (' . implode(',', $emptylist) . ') cannot be empty.', 210);
            }
            if($data['password']!=$data['confirm_password']){
                throw new Exception("confirm password mismatch",212);
            }
            //if(is_null(Yii::app()->user->getId())){
            
            if(!self::verifyOTP($data)){
                $hash = $data['mobile_number']."-".date('Y-m-d');
                if(Yii::app()->cache->hexists($hash,1)){
                    if(Yii::app()->cache->hget($hash,1)<=5){
                        Yii::app()->cache->hincrby($hash,1,1);
                    }else{ 
                        //file_put_contents("/tmp/check_block_user", " STEP2 " . json_encode($data) . "\n", FILE_APPEND | LOCK_EX);
                        //block user and send mail to team
                            self::limituserLogin($data);
                    } 
                
               }
                    
                throw new Exception("Invalid OTP",213);
            }else{
                $hash = $data['mobile_number']."-".date('Y-m-d');
                 if(Yii::app()->cache->hexists($hash,1)){
                     Yii::app()->cache->hdel($hash,1);
            }
            }
            $optobj = self::verifyOTP($data,TRUE);
            if(is_object($optobj)){
            $optobj->status=1;
            $optobj->useddate=  GeneralComponent::getFormatedDate();
            $optobj->save();
            }
            
            //}
            $userObj = new Users();
            $userObj = $userObj->findByAttributes(array('mobile_number'=>$data['mobile_number'],'user_name'=>$data['mobile_number']));
            if(is_null($userObj)){
                throw new Exception("User does not exist",214);
            }            
            $old_passwd = $userObj->password;
            $userObj->password= $data['password'];
            $userObj->save();
            if($old_passwd == "" || $old_passwd == NULL){
                GeneralComponent::notifier(array('SUCCESS_REGISTRATION'),array('user_id'=>$userObj->id));
            }elseif($old_passwd !== $userObj->password){
                GeneralComponent::notifier(array('CHANGE_PASSWORD_SUCCESS'),array('user_id'=>$userObj->id));
            }
            $api_version  = isset($data['api_version']) ? $data['api_version']: 0;
            $lgdata = array('username'=>$data['mobile_number'],'password'=>$data['password'],'api_version'=>$api_version);
            return $result = self::login($lgdata);
            /*if(is_null(Yii::app()->user->getId())){
                $lgdata = array('username'=>$data['mobile_number'],'password'=>$data['password']);
                return $result = self::login($lgdata);
            }else{
                $result = array('status'=>'success','errCode'=>'0','description'=>'Password set successfully');
            }*/
        } catch (Exception $ex) {
            $msg = 'something went wrong';
            if($ex->getCode()==212 || $ex->getCode()==213 || $ex->getCode()==214 || $ex->getCode()==219 || $ex->getCode()==224)
                $msg = $ex->getMessage ();
            $result = array('status'=>'failure','errCode'=> $ex->getCode().'','description'=>$msg);
        }
        return json_encode($result);
    }
    
    public static function chekgenuinedevice($data){
        $deviceid = isset($data['uuid']) ? $data['uuid'] : "";
        $blacklistedUser = self::isUserBlackListed($data['id']);
        if(!empty($deviceid) && !$blacklistedUser){
             $query="SELECT DATE( created_datetime ) as us_dt FROM users WHERE id=".$data['id'];
              $result = Yii::app()->db->createCommand($query)->queryAll();
              $date = $result[0]['us_dt'];
             
            $query="SELECT id FROM user_profile WHERE uuid='".$deviceid."'";
            $result = Yii::app()->db->createCommand($query)->queryAll();
            $pg_data= array('user'=>$data['username']);
             if((strtotime($date)-strtotime('2016-06-01'))>0 && count($result)>3){
                
                 GeneralComponent::sendMail("sunilc@mindsarray.com,chaitra@pay1.in,krunal@pay1.in,nandan@mindsarray.com,swapnil@mindsarray.com,ashish@mindsarray.com", "Blocked user since using Pay1 App for multiple user", json_encode($pg_data));
                 $blockdata = array('userid'=>$data['id'],'device_id'=>$deviceid);
                self::blockUser($blockdata);
             }
            else if((strtotime($date)-strtotime('2016-06-01'))<0 && count($result)>20){
                GeneralComponent::sendMail("sunilc@mindsarray.com,chaitra@pay1.in,krunal@pay1.in,nandan@mindsarray.com,swapnil@mindsarray.com,ashish@mindsarray.com", "Blocked user since using Pay1 App for multiple user", json_encode($pg_data));
                $blockdata = array('userid'=>$data['id'],'device_id'=>$deviceid);
                self::blockUser($blockdata);
            }
        }
    }
    
    
    public static function login($data,$rememberflag=FALSE){
        try{
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            if(!isset($data['username']) || !isset($data['password'])  )
                throw new Exception($message, 580);
        if (strlen(trim($data['username'])) > 0 && strlen(trim($data['password'])) > 0) {
            $login_model = new LoginForm();
            $login_model->setAttributes(array('username' => $data['username'], 'password' => md5($data['password']),'rememberMe'=>$rememberflag));
            if ($login_model->validate() && $login_model->login()) {
                 $hash = $data['username']."-".date('Y-m-d');
                 if(Yii::app()->cache->hexists($hash,1)){
                     Yii::app()->cache->hdel($hash,1);
                 }
                $data['through_login'] = 1;
                $userdetail = Users::model()->findByAttributes(array('user_name' => $data['username']))->attributes;
                $userwallet = Wallets::model()->findByAttributes(array('users_id' => $userdetail['id']))->attributes;
                $data['id']= $userdetail['id'];
                $data['user_id']= $userdetail['id'];
                self::edituserProfile($data);
                
                    $userprofile = UserProfile::model()->findByAttributes(array('user_id' => $userdetail['id']))->attributes;                    
                
                unset($userprofile['id']); 
                $userid = self::getIdByName($data['username']);
                $quicklist = ServiceComponent::get_quickpaylist($userid, '6');            
                $recent_transaction = ($quicklist['status'] === "failed")?array():$quicklist['description'];
                $user_gifts = DealsComponent::getUserGiftCounts();
                $user_data = array_merge($userprofile,
                array('wallet_balance'=>$userwallet['account_balance'],'loyalty_points'=>$userwallet['loyalty_points'],'user_role'=>$userdetail['role']),Yii::app()->params['accounting_cost']);
                $user_data['support_number'] = Yii::app()->params['GLOBALS']['B2C_SUPPORT_MISSED_CALL_NUMBER'];
                $user_data['ErrorMsg'] = GeneralComponent::get_errorMsg_list();
                if(isset($data['api_version']) && $data['api_version']==3){
                    $user_data['user_val']=session_id();
                    $user_data['user_val_name']= session_name();
                }else{
                    $user_data['session_id']=session_id();
                    $user_data['session_name']=  session_name();
                }
                
                if(isset($userprofile['api_version']) && $userprofile['api_version']=='3' && !empty($user_gifts)){ //for new apps
                    $user_data['total_gifts']= isset($user_gifts[0]['total_gifts']) ? $user_gifts[0]['total_gifts']: 0;
                    $user_data['total_redeem']= isset($user_gifts[0]['total_redeem']) ? $user_gifts[0]['total_redeem']: 0;
                    $user_data['total_mylikes']= isset($user_gifts[0]['offer_id']) ? count(explode(",",$user_gifts[0]['offer_id'])): 0;
                    $user_data['total_myreviews']= isset($user_gifts[0]['reviews']) ? count(explode(",",$user_gifts[0]['reviews'])): 0;
                    $user_data['profile_image']= isset($user_gifts[0]['image']) ? $user_gifts[0]['image']: "";
                    $user_data['Deals_offer']=  json_decode(DealsComponent::getAllofferTypes(array(),array(2)),true);//2=> Top offers,2=> Featured offers
                    $user_data['OfferReview'] = Yii::app()->params['GLOBALS']['_Offer_Reviews'];
                    $user_data['fb_data'] =  isset($userprofile['fb_data']) ? json_decode(stripslashes($userprofile['fb_data'])): "";
                    if(!strpos($user_data['profile_image'],"facebook") && !empty($user_data['profile_image'])){
                   // if(empty($user_data['fb_data'] )){
                        $user_data['profile_image'] = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$user_data['profile_image'];
                        $user_data['image'] = $user_data['profile_image'];
                    }else if(!strpos($user_data['profile_image'],"facebook") && empty($user_data['profile_image'])){
                        $user_data['profile_image'] = Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."images/user.jpg";
                    }
                    else{
                        $user_data['image'] = $user_data['profile_image'];
                    }
                }
                $res = array('status'=>'success','errCode'=>'','description'=>$user_data);
                $res['description']['recent_pay1_transactions'] = $recent_transaction; 
            }else{
                if(isset($data['username']) && !empty($data['username'])){
                   //set user in redis for failure attempt of login within 10 minutes 5 times
                    $hash = $data['username']."-".date('Y-m-d');
                    if(Yii::app()->cache->hexists($hash,1)){
                        if(Yii::app()->cache->hget($hash,1)<5){
                            Yii::app()->cache->hincrby($hash,1,1);
                        }else{ 

                            file_put_contents("/var/log/apps/check_block_user".date('Ymd').".log", " STEP1 " . json_encode($data) . "\n", FILE_APPEND | LOCK_EX);
                                //block user and send mail to team
                                self::limituserLogin($data);
                        }

                    }else{
                        Yii::app()->cache->hset($hash,1,1);
                        Yii::app()->cache->expire($hash,600);//user use  wrong password within10 min
                    } 
                }
                $res = array('status'=>'failure','errCode'=>'205','description'=>'Incorrect username or password');
                }
        } else {
            $res = array('status'=>'failure','errCode'=>'221','description'=>'Missing parameters');
        }
        } 
        catch (Exception $ex) {
            $error = array('status'=>'failed','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
            file_put_contents("/var/log/apps/login_error".date('Ymd').".log", " STEP1 " . json_encode($error) . "\n", FILE_APPEND | LOCK_EX);
            $userprofile = array();
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            if($ex->getCode()==580)
                $message = $ex->getMessage();
            $res = array('status'=>'failed','errCode'=>$ex->getCode().'','description'=>$message);
        }
        return json_encode($res);
    }
    
    public static function createCaptcha($data){
        try{
            self::captchaImage($data);
            return array('status'=>'success','errCode'=>'221','description'=>'Missing parameters','captcha'=>'Pay1captcha.jpeg');
        }
        catch(Exception $e){
            return array('status'=>'failure','errCode'=>'221','description'=>'Missing parameters','captcha'=>'0');
        }
    }
    
    public static function captchaImage($data){
        //Let's generate a totally random string using md5 
        $md5_hash = md5(rand(0,999)); 
        //We don't need a 32 character long string so we trim it down to 5 
        $security_code = substr($md5_hash, 15, 4); 

        //Set the session to store the security code
        $hash = $data['username']."_captcha";
        Yii::app()->cache->hset($hash,1,$security_code);
        Yii::app()->cache->expire($hash,5*60);//expire after 24 hours
       // $_SESSION["security_code"] = $security_code;

        //Set the image width and height 
        $width = 80; 
        $height = 30;  

        //Create the image resource 
        $image = ImageCreate($width, $height);  

        //We are making three colors, white, black and gray 
        $white = ImageColorAllocate($image, 255, 255, 255); 
        $black = ImageColorAllocate($image, 0, 0, 0); 
        $grey = ImageColorAllocate($image, 204, 204, 204); 

        //Make the background black 
        ImageFill($image, 0, 0, $black); 

        //Add randomly generated string in white to the image
        ImageString($image, 3, 30, 8, $security_code, $white); 

        //Throw in some lines to make it a little bit harder for any bots to break 
        ImageRectangle($image,0,0,$width-1,$height-1,$grey); 
        //imageline($image, 0, $height/2, $width, $height/2, $grey); 
        imageline($image, $width/2, 0, $width/2, $height, $grey); 

        //Tell the browser what kind of file is come in 
        header("Content-Type: image/jpeg"); 

        //Output the newly created image in jpeg format 
        ImageJpeg($image,Yii::app()->params['GLOBALS']['_IMG_DIR'].'Pay1captcha.jpeg'); 

        //Free up resources
        ImageDestroy($image); 
    }
    
    public static function failedLoginByAPP($data){
        if(isset($data['username']) && !empty($data['username'])){
            self::limituserLogin($data);
        }else{
            return array('status'=>'failure','errCode'=>0,'description'=>'Please provide username parameter to block user');
        }
    }
    
    public static function limituserLogin($data){
        if(isset($data['username']) && !empty($data['username'])){
            try{
                $Result = Yii::app()->db->createCommand("SELECT id FROM users WHERE user_name=".$data['username'])->queryRow();
                file_put_contents("/var/log/apps/check_block_user".date('Ymd').".log", " STEP5 " . "SELECT id FROM users WHERE user_name=".$data['username'] . "\n", FILE_APPEND | LOCK_EX);
                //send mail
                $pg_data = array('user'=>$data['username']);
               
                
                if(isset($Result['id']) && !Yii::app()->db->createCommand("SELECT * FROM black_listed_user WHERE users_id=".$Result['id'])->execute()){
                     file_put_contents("/var/log/apps/check_block_user".date('Ymd').".log", " STEP5 " . "SELECT * FROM black_listed_user WHERE users_id=".$Result['id']. "\n", FILE_APPEND | LOCK_EX);
                    $hash2 = 'failedLoginBlock';
                    if(!Yii::app()->cache->hexists($hash2,$data['username'])){
                        Yii::app()->cache->hset($hash2,$data['username'],time());
                    }
                    //block user
                    $data['userid']=$Result['id'];
                    if(self::blockUser($data)){
                        GeneralComponent::sendMail("sunilc@mindsarray.com,chaitra@pay1.in,krunal@pay1.in,nandan@mindsarray.com,swapnil@mindsarray.com,sanjeev@mindsarray.com", "Blocked user since trying to do login more than limit", json_encode($pg_data));
                        return array('status'=>'success','errCode'=>'','description'=>'user blocked successfully for 1 hr');
                    }
                }else{
                    return array('status'=>'failure','errCode'=>0,'description'=>'Either user not exist in system or already blocked');
                }
            }catch(Exception $ex){
                GeneralComponent::sendMail("swapnil@mindsarray.com,sanjeev@mindsarray.com", "ERROR while Blocking user ", json_encode($ex));
                return array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>'There is some problem while blocking user');
            }
        }
    }
    public static function logout(){
        try{            
            Yii::app()->user->logout();
            $res = array('status'=>'success','errCode'=>'','description'=>'user successfully logout');
        } catch (Exception $ex) {
            $res = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($res);
    }
    
    public static function addnew_user($data){
        try{
            $transaction = Yii::app()->db->beginTransaction();
            $user = new Users();
            $userdetail = array('user_name'=>$data['mobile_number'],'mobile_number'=>$data['mobile_number'],'password'=>$data['password'],
                    'role'=>$data['role'],'created'=> GeneralComponent::getFormatedDate());
            $user->setAttributes($userdetail);
            if(!$user->save()){
                throw new Exception($user->errors(),215);
            }
            $user_profile = new UserProfile();
            $profile_detail = array('name'=>$data['name'],'mobile_number'=>$data['mobile_number'],'users_id'=>$user->id,'created'=>  GeneralComponent::getFormatedDate());
            $user_profile->setAttributes($profile_detail);
            if(!$user_profile->save()){
                throw new Exception($user_profile->errors(),216);
            }
            $user_wallet = new Wallets();
            $wallet_detail = array('users_id'=>$user->id,'account_balance'=>0);
            if(!$user_wallet->save()){
                throw new Exception($user_wallet->errors(),219);
            }
            $transaction->commit();
            $result = array('status'=>'success','errCode'=>'0','description'=>'user created successfully');
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status'=>'failed','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($result);
    }
    
    public static function edituserProfile($data){
        try{     
            Yii::app()->user->id = isset(Yii::app()->user->id) ? Yii::app()->user->id : $data['user_id'];
            $profileObj = new UserProfile();
            $f_url = isset($data['f_url']) ? $data['f_url'] : "";
            $code = isset($data['code']) ? $data['code'] : "";
            $fb_data = isset($data['fb_data']) ? $data['fb_data'] : 0;
            $throughLogin  = isset($data['through_login']) ? 1 : 0;
            $profileObj = $profileObj->findByAttributes(array('user_id'=>Yii::app()->user->id));
            if(is_null($profileObj)){
                throw new Exception("User profile does not exist",221);
            }
            $field_list = array('name','email','gender','date_of_birth','gcm_reg_id','uuid','device_type','location_src','manufacturer','os_info','updated');
            $data['updated'] = GeneralComponent::getFormatedDate();
            $email_flag = FALSE;
            $profile_flag = FALSE;
            $userObj = Users::model()->findByAttributes(array('id'=>Yii::app()->user->id))->attributes;
            if(isset($data['email']) && $data['email'] != ""){
                if(!isset($data['password']) && !isset($data['api_version'])){
                    throw new Exception("Missing password", 238);
                }
                $email_flag = TRUE;
                if(!isset($data['api_version'])){
                    if(isset($data['password']) && $userObj['password'] != md5($data['password'])){
                        throw new Exception("Invalid password", 239);
                    }
                }
            }
            foreach($field_list as $var_name){
                if(isset($data[$var_name]) && trim($data[$var_name]) !=''){
                    
                    $profileObj->setAttribute($var_name, $data[$var_name]);
                }
            }
            if(!empty($data['longitude'])){
                $profileObj->setAttribute('longitude', $data['longitude']);
            }
            if(!empty($data['latitude'])){
                $profileObj->setAttribute('latitude', $data['latitude']);
            }
            if(!empty($data['longitude']) && !empty($data['latitude'])){
                $profileObj->setAttribute('latitude_longitude', substr( $data['latitude'], 0, strpos( $data['latitude'] , '.' ) +2 )."_".substr( $data['longitude'], 0, strpos( $data['longitude'] , '.' ) +2 ));
            }
            if(isset($data['api_version']) && !empty($data['api_version'])){
                $profileObj->setAttribute('api_version', $data['api_version']);
            }
            if($fb_data==2 && isset($data['api_version']) && $data['api_version']==3 && !empty($data['code'])){
                $image = self::getImageUrlFromBase64Code($data);
                $profileObj->setAttribute('image', $image);
                 
            }else if(isset($data['f_url']) && !empty($data['f_url']) && $fb_data==1 && isset($data['api_version']) && $data['api_version']==3){
                $email_flag = FALSE;
                $profile_flag = TRUE;
                $profileObj->setAttribute('image', $data['f_url']);
                
            }
           $profileObj->setAttribute('fb_data',$fb_data);
            if(!$profileObj->save()){
                throw new Exception("Profile cannot be updated", 225);
            }else{
                if( isset($data['api_version']) && $data['api_version']==3 && $profile_flag && !$throughLogin){
                   // GeneralComponent::notifier(array('CHANGE_PROFILE_SUCCESS_FB'),array('user_id'=>$userObj['id']));
                }else if(isset($data['api_version']) && $data['api_version']==3 && !$throughLogin){
                    //GeneralComponent::notifier(array('CHANGE_PROFILE_SUCCESS'),array('user_id'=>$userObj['id']));
                }
            }
            if($email_flag && !isset($data['api_version'])){
                GeneralComponent::notifier(array('CHANGE_EMAIL_ID_SUCCESS'),array('user_id'=>$userObj['id']));
            }
           
            $query="SELECT * FROM user_profile WHERE user_id=".Yii::app()->user->id;
            $result = Yii::app()->db->createCommand($query)->queryAll();
            $profileData = array();
            if(count($result)>0){
                $profileData = $result[0];
                if(!empty($result[0]['image']) && !strpos($result[0]['image'],"facebook")){
                    $profileData['image'] = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$result[0]['image'];
                }
            }
            $result = array('status'=>'success','errCode'=>'0','description'=>'Profile updated successfully','profileData'=>$profileData);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($result);
    }
    
    public static function getuserProfile($user_id=0,$txn_id=0){
        try{
            $apiFlag = TRUE;
            if($user_id != 0){
                Yii::app()->user->id =  is_null(Yii::app()->user->getId())?$user_id:Yii::app()->user->getId();
                $apiFlag = FALSE;
            }
            if($txn_id > 0){
                $transObj = new Transactions();
                $transObj = $transObj->findByAttributes(array('trans_id'=>$txn_id));
                Yii::app()->user->id =$transObj->users_id;
                $apiFlag = FALSE;
            }
            $profileObj = new UserProfile();
            $profileObj = $profileObj->findByAttributes(array('user_id'=>Yii::app()->user->id));
            if(is_null($profileObj)){
                throw new Exception("User profile does not exist",221);
            }
            if($apiFlag){
                $result = array('status'=>'success','errCode'=>0,'description'=>$profileObj->attributes);
            }else{
                $result = $profileObj->attributes;
            }
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $result;
    }
    
    public static function forgotPassword($data){
        if(isset($data['vtype'])){
            return self::forgotPassword_new($data);
        }
        try{
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            if(!isset($data['user_name']) || empty($data['user_name']))
                throw new Exception($message, 580);
            $userObj = new Users();
            $userObj = $userObj->findByAttributes(array('user_name'=>$data['user_name']));
            if(is_null($userObj)){
                throw new Exception("User does not exist",214);
            }
            $uprofile = new UserProfile();
            $uprofile = $uprofile::model()->findByAttributes(array('user_id' => $userObj->id));
            $uprofile->email = "not empty";
            if($uprofile->email == "" || $uprofile->email == NULL){
                //throw new Exception("We have received your request successfully. Soon you will receive a call from us.",241);
                $result = array('status'=>'success','errCode'=>0,'description'=>"We have received your request successfully. Soon you will receive a call from us.");
            }else{
                $password = GeneralComponent::generate_OTP(5);
                $userObj->password = $password;
                $userObj->save();
                GeneralComponent::notifier(array('FORGOT_PASSWORD'),array('password'=>$password,'email'=>$uprofile->email,'mobile'=>$data['user_name']));            
                $result = array('status'=>'success','errCode'=>0,'description'=>"Password sent successfully on your registered mobile number",'otp'=>md5($data['user_name']).md5($password));
            }
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage(),'otp'=>'');
        }
        return $result;
    }
    
    /*
     * This is to support backward compatiblity for old users
     * 
     */
    
    public static function forgotPassword_new($data){
        try{
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            if(!isset($data['user_name']) || empty($data['user_name']))
                throw new Exception($message, 580);
            $data['mobile_number'] = $data['user_name'];
            $userObj = new Users();
            $userObj = $userObj->findByAttributes(array('user_name'=>$data['user_name']));
            if(is_null($userObj)){
                throw new Exception("User does not exist",214);
            }
            $user_otp = self::sendOTP($data);
            $result = array('status'=>'success','errCode'=>0,'description'=>"OTP send successfully",'otp'=>md5($data['user_name']).md5($user_otp));
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage(), 'otp'=>'');
        }
        return $result;
    }
    
   /*
    *    Possible Values Of Type Of Search
    *    $typeOfSearch => "byUserId" ,"byUserMobile" ,"byUserName" , "byWalletId"
    *    Default Value => "byUserId"
    *    Values of $input would be UserId OR UserMobile OR UserName OR WalletId
    */
    public static function getFullUserDetails($input , $typeOfSearch= "byUserId"){
        $searchCondition = "";
        $flag=0;
        if ($typeOfSearch == "bytxnId") {
            $flag=1;
            $searchCondition = "`transactions`.`trans_id` = $input";
        } else if ($typeOfSearch == "byUserMobile") {
            $flag=1;
            $searchCondition = "(rt.`mobile_number` = '$input' OR bt.mobile_number = '$input')";
        } else if ($typeOfSearch == "byUserName") {
            $searchCondition = "`users`.`user_name` = '$input'";
        } else if ($typeOfSearch == "byWalletId") {
            $flag=1;
            $searchCondition = "`wallets`.`wallet_id` = '$input'";
        }else if ($typeOfSearch == "byUserSubscription") {
            $flag=1;
            $searchCondition = "(rt.`subscriber_id` = '$input')";
        }else if ($typeOfSearch == "byPay1Voucher_no") {
            $flag=1;
            $searchCondition = "(vchr.`voucher_code` = '$input')";
        }else if ($typeOfSearch == "byDealCoupon_no") {
            $flag=1;
            $searchCondition = "(cpn.code = '$input')";
        }
        else {
            return(array("status" => "failure","description" => "Wrong input .","data" => array()));
        }
        try {
            $tables = '';
            if($flag==1){
                $tables = "LEFT JOIN `transactions` ON `users`.`id` = `transactions`.`users_id` 
                    LEFT JOIN `billing_transaction` as bt ON bt.transaction_id  = `transactions`.`trans_id` 
                    LEFT JOIN `deal_transaction` as dt ON dt.transaction_id  = `transactions`.`trans_id` 
                    LEFT JOIN `recharge_transaction` as rt ON rt.transaction_id  = `transactions`.`trans_id` 
                    LEFT JOIN `voucher` as vchr ON vchr.txnid  = `transactions`.`trans_id` 
                    LEFT JOIN `coupons` as cpn ON cpn.users_id = `users`.`id` 
                   ";
            }
            $qry = "SELECT users.id,(CASE WHEN is_genuine=0 THEN 'non genuine user' ELSE 'genuine user' END) as userType, users.user_name, users.mobile_number,users.created_datetime, 
                users.role, user_profile.id as 'user_profile_id',user_profile.name, user_profile.mobile, user_profile.email,
                user_profile.gender,user_profile.date_of_birth, user_profile.user_id, user_profile.gcm_reg_id, user_profile.uuid,
                user_profile.longitude, user_profile.latitude, user_profile.location_src, user_profile.manufacturer, 
                user_profile.os_info, user_profile.device_type, user_profile.created as 'user_profile_created', user_role.role_name, 
                wallets.wallet_id as 'wallet_user_id', wallets.account_balance, wallets.loyalty_points, user_profile.updated as 'user_last_login' 
                    FROM `users`
                    LEFT JOIN `user_profile` ON `users`.`id` = `user_profile`.`user_id`
                    LEFT JOIN `user_role`    ON `users`.`role` = `user_role`.`id`
                    LEFT JOIN `wallets`      ON `users`.`id` = `wallets`.`users_id` 
                    $tables
                     WHERE  $searchCondition  group by 1
                ";
           $list = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array("status" => "success","description" => "","data" => $list);            
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage(), "data" => array());
        }
        return $result;
    }
    
    public static function getuser_password($data){        
            $userObj = new Users();
            $userObj = $userObj->findByAttributes(array('mobile_number'=>$data['mobile_number'],'user_name'=>$data['mobile_number']));
            if(is_null($userObj)){
                return "";
            }
            return $userObj->password;
    }
    
    public static function updatedUserlat_lng($data){
        $latitude = isset($data['latitude']) && !empty($data['latitude']) ? $data['latitude'] : "";
        $longitude = isset($data['longitude']) && !empty($data['longitude']) ? $data['longitude'] : "";
        $lat_lng = substr($latitude, 0, strpos($latitude , '.' ) +2 )."_".substr( $longitude, 0, strpos( $longitude , '.' ) +2 );
        if(isset(Yii::app()->user->id) && $latitude!="" && $longitude!=""){
            $userid = Yii::app()->user->id;
            $qry = "update user_profile set latitude=$latitude,longitude=$longitude, latitude_longitude='$lat_lng' WHERE user_id = $userid";
            
            try{
                Yii::app()->db->createCommand($qry)->execute();
                $result = array("status" => "success","description" => "User Lat, Lng updated successfully");
                
            } catch (Exception $ex) {
                $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
            }
            return $result;
        }
    }
    
    public static function getUserDetails($id=null) {
        $cond = (isset($id) && !empty($id)) ? " id = $id" : " 1 ";
        $query = "SELECT `id`, `user_name`, `mobile_number`, `password`, `salt`, `created_datetime`, "
                . "`modified_datetime`, `role`, `status`,dealer_name FROM `users`"
                . " where $cond ";
         $UserList = Yii::app()->db->createCommand($query)->queryAll();
        return isset($UserList) ? $UserList : array();
    }
    
    public static function setUserFeedback($data){
        $user_id = Yii::app()->user->id;
        $feedback = trim($data['feedback']);
        $query = "SELECT user_id FROM users_feedback WHERE user_id=$user_id";
        $result= Yii::app()->db->createCommand($query)->queryAll();
        if(count($result)>0){
            $query1 = "UPDATE users_feedback SET feedback = '$feedback' WHERE user_id=$user_id";
            
        }
        else{
             $query1 = "INSERT INTO  users_feedback VALUES ($user_id,  '$feedback')";
        }
        if(Yii::app()->db->createCommand($query1)->execute()){
                return array('status' => 'success', 'errCode' => '0', 'description' =>'Feedback saved');
            }
        return array('status' => 'error', 'errCode' => '0', 'description' =>'error while saving feedback');
    }
    
    public static function newUserProfile($data){
        $column = key($data);
        $profileObj = new UserProfile();
         return $profileObj->findByAttributes(array($column=>$data[$column]));
    }
    
    public static function getUserLoyatyBalance($userId){
        $query="SELECT * FROM `wallets` WHERE users_id= $userId";
        return $result = Yii::app()->db->createCommand($query)->queryRow();
    }
    
    public static function getImageUrlFromBase64Code($data = null) {
        if(!isset($data['code']) || empty($data['code'])){
           throw new Exception("Please pass Image base-64 code as parameter"); 
        }
        $userid = isset($data['id']) ? $data['id']:Yii::app()->user->id;
        $base_64_code = ($data['code'] == null) ? '': $data['code'];
        $data = str_replace('data:image/png;base64,', '', $base_64_code);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data); // base64 decoded image data
        
           if(!isset($data) || empty($data)){
           throw new Exception("Invalid base-64 code , please pass Valid base-64 code as parameter"); 
        }
        if (!$img = @imagecreatefromstring($data)) {
           throw new Exception("Invalid base-64 code , please pass Valid base-64 code as parameter"); 
        }else{
        $source_img = imagecreatefromstring($data);
        }
//            $rotated_img = imagerotate($source_img, 0, 0); // rotate with angle 90 here
        $file = 'images/User_id_'.$userid.'.png';
//            $imageSave = imagejpeg($rotated_img, $file, 10);
        $imageSave = imagejpeg($source_img, $file, 100);
        imagedestroy($source_img);
        return $file;
        //Yii::app()->db->createCommand("UPDATE user_profile SET image = '$file' WHERE user_id ='" . $userid . "'")->execute();
       //      $user_image_url = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$file;
            // $result  = array('status' => 'success', 'errCode' => '0', 'description' =>array('user_image_url'=>$user_image_url));
//        } catch (Exception $ex) {
////        $result =  array('status' => 'error', 'errCode' => '0', 'description' =>'Error while Updating User Profile Image ');
//        $result =  array('status' => 'failure', 'errCode' =>$ex->getCode(), 'description' =>$ex->getMessage());
//     }
//        return $result;
    }
    
    public static function setRedisCreateUser($mob_num){
        if(!(Yii::app()->cache->hexists('b2cUserList', $mob_num))){ 
            Yii::app()->cache->hset('b2cUserList',$mob_num, 1);
        }
    }
    
    /*
     * set All b2c user in redis
     */
    public static function setRedisAllUsers(){
       $query = "select user_name FROM users ";
       $result = Yii::app()->db->createCommand($query)->queryAll();
        if(count($result)>0){
            foreach ($result as $row){
                if(!(Yii::app()->cache->hexists('b2cUserList', $row['user_name']) && !empty($row['user_name']))){
                    Yii::app()->cache->hset('b2cUserList', $row['user_name'],1);
                }
            }
        }
    }
    
    public static function getRedisCreateUser($mob_num=NULL){
             $missUser = Yii::app()->cache->hgetall('b2cUserList');
    }
    

    public static function rpushRedisUser($mob_num=NULL){
        
        $myarray = array('trans_id'=>90109,'area'=>150,'flag'=>1,'product_id'=>150,
                    'mobile'=>8115093740,'param'=>'postpaid','amount'=>300,'timestamp'=>date('Y-m-d H:i:s'));
        
//       $myarray = array('trans_id' => 19109, 'amount'=> 150, 'mobile' => 8422054700 , 'flag'=>1);
//       yii::app()->cache->rpush("B2B_shop_failure_txn_Q", $result); 
        yii::app()->cache->rpush("B2B_shop_success_txn_Q", json_encode($myarray)); 
    }
    
    public static function popb2bTxnSucc($data=NULL){
        $notify_success_var = array();
        $notify_data        = array();
        $connection = Yii::app()->db;
        file_put_contents("/var/log/apps/popb2bTxnSucc".date('Ymd').".log", "|  : ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        if(isset($data['amount']) && isset($data['mobile']) && isset($data['trans_id'])){
            $transaction = $connection->beginTransaction();
            try{
                $data['userid']  = self::getIdByName($data['mobile']);
                $user_id = $data['userid'];
                $servicePoints   = 1;

                $amount = $data['amount'];
                $trans_loyalty_points = ceil($amount*$servicePoints);
                $wall_loyalty_points = $connection->createCommand("SELECT loyalty_points from wallets  WHERE users_id='$user_id' FOR UPDATE")->queryAll();
                $closing_bal = $wall_loyalty_points[0]['loyalty_points'] + $amount;
                $Trans_data = array('opening_bal'=>$wall_loyalty_points[0]['loyalty_points'],'transaction_amount'=>$amount,'closing_bal'=>$closing_bal,
                    'trans_category'=>'pay1shop','users_id'=>$data['userid'],'trans_type'=>'1','transaction_mode'=>'loyaltypoints','response_id'=>$data['trans_id'],
                    'track_id'=>'','trans_datetime'=>GeneralComponent::getFormatedDate(),
                         'recharge_flag'=>0,'status'=>2,'loyalty_points'=>$trans_loyalty_points,'free_points'=>1);
                TransactionComponent::newTransaction($connection, $Trans_data);
                $data['transaction_id'] = $connection->lastInsertID;
                TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['transaction_id']));

                $query  = "UPDATE wallets as w "
                        . "SET w.loyalty_points = CEIL(w.loyalty_points + ($trans_loyalty_points)) "
                        . "WHERE w.users_id = $user_id";
                $connection->createCommand($query)->execute();
                $transaction->commit();

                $res = Yii::app()->db->createCommand("SELECT account_balance , loyalty_points from wallets where users_id = $user_id")->queryAll();
                $total_points = $res[0]['loyalty_points'];
                $notify_data = array('mobile'=>$data['mobile'],'LOYALTY_COINS'=>$trans_loyalty_points,'LOYALTY_BALANCE'=>$total_points);
                $content = Yii::app()->params['message_template']['B2C_BY_RETAILER_RECHARGE_SUCC'];
                $notification = Yii::app()->params['notification_template']['B2C_BY_RETAILER_RECHARGE_SUCC'];
                $api_param = GeneralComponent::send_message($notify_data,$content,$notification);
            }
            catch(Exception $e){
                $transaction->rollback();
            }
        }    
    }
    
    public static function popb2bTxnFail($data=NULL){
        $notify_success_var = array();
        $notify_data        = array();
        $connection = Yii::app()->db;
        file_put_contents("/var/log/apps/popb2bTxnFail".date('Ymd').".log", "|  : Entry ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        usleep(10);
        
        if(isset($data['amount']) && isset($data['trans_id'])){ 
            $FailTxn = $connection->createCommand("SELECT trans_id FROM  `transactions` WHERE  `response_id` =".$data['trans_id']." AND trans_category='pay1shop'" )->queryRow();
            if(isset($FailTxn['trans_id']) && !empty($FailTxn['trans_id'])){
                $data['userid']  = self::getIdByName($data['mobile']);
                $user_id = $data['userid'];
                $trans_id = $data['trans_id'];
                $servicePoints   = 1;
                $amount          = $data['amount'];
                $trans_loyalty_points = ceil($amount*$servicePoints);
                try{
                    $transaction = $connection->beginTransaction();
                    $query  = "UPDATE transactions as t "
                            . "SET t.status = 3 "
                            . "WHERE t.response_id = ".$data['trans_id'];
                    if($connection->createCommand($query)->execute()){
                        //$connection->createCommand($query)->execute();
                        file_put_contents("/var/log/apps/popb2bTxnFail".date('Ymd').".log", "|  : query ".  $query."\n", FILE_APPEND | LOCK_EX);
                        $wall_loyalty_points = $connection->createCommand("SELECT loyalty_points from wallets  WHERE users_id='$user_id' FOR UPDATE")->queryAll();
                        $closing_bal = $wall_loyalty_points[0]['loyalty_points'] - $amount;
                        $Trans_data = array('opening_bal'=>$wall_loyalty_points[0]['loyalty_points'],'transaction_amount'=>$amount,'closing_bal'=>$closing_bal,
                            'trans_category'=>'pay1shop','users_id'=>$data['userid'],'trans_type'=>'0','transaction_mode'=>'loyaltypoints','response_id'=>$data['trans_id'],
                            'track_id'=>'','trans_datetime'=>GeneralComponent::getFormatedDate(),
                            'updated_response'=>'failure from b2b','recharge_flag'=>0,'status'=>4,'loyalty_points'=>-$trans_loyalty_points,'free_points'=>1,'ref_trans_id'=>$FailTxn['trans_id']);
                        TransactionComponent::newTransaction($connection, $Trans_data);
                        //$data['transaction_id'] = $connection->lastInsertID;
                        //TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['trans_id']));

                        $query  = "UPDATE wallets as w "
                                . "SET w.loyalty_points = CEIL(w.loyalty_points - ($trans_loyalty_points)) "
                                . "WHERE w.users_id = $user_id";
                        $connection->createCommand($query)->execute();
                        file_put_contents("/var/log/apps/popb2bTxnFail".date('Ymd').".log", "|  : update wallet ".  $query."\n", FILE_APPEND | LOCK_EX);
                        $transaction->commit();

                        $res = Yii::app()->db->createCommand("SELECT account_balance , loyalty_points from wallets where users_id = $user_id")->queryAll();
                        $total_points = $res[0]['loyalty_points'];
                        $notify_data = array('mobile'=>$data['mobile'],'LOYALTY_COINS'=>$trans_loyalty_points,'LOYALTY_BALANCE'=>$total_points);
                        $content = Yii::app()->params['message_template']['B2C_BY_RETAILER_RECHARGE_FAIL'];
                        $notification = Yii::app()->params['notification_template']['B2C_BY_RETAILER_RECHARGE_FAIL'];
                        GeneralComponent::send_message($notify_data,$content,$notification);
                    }

                }
                catch (Exception $ex){
                    $transaction->rollback();
                    file_put_contents("/var/log/apps/popb2bTxnFail".date('Ymd').".log", "|  : exception ". json_encode($data)."\n", FILE_APPEND | LOCK_EX);
                }
            }
            
        }    
    }
    
    /*
     * This function use to add user in block list
     * 
     */

    public static function blockUser($data) {
        $userid = $data['userid'];
        $device_id = isset($data['device_id'])? $data['device_id']:0;
        if(!empty($device_id)){
        try {
            Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')")->execute();
            Yii::app()->db->createCommand("DELETE FROM permanent_unblock_user WHERE users_id = $userid")->execute();
            Yii::app()->db->createCommand("UPDATE user_profile SET is_genuine=0 WHERE user_id = $userid")->execute();
            return true;
        } catch (Exception $ex) {
            return false;
        }
        }
    }
    
    public static function unblockUser($data) {
        //file_put_contents("/tmp/failedlogin".date('Ymd').".log", "| unblockUser1 ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        $userid =  0;
        if(isset($data['id'])){
            $userid = $data['id'];
        }else if(isset($data['userid'])){
            $userid = $data['userid'];
        }
        //file_put_contents("/tmp/failedlogin".date('Ymd').".log", "| unblockUser ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        $device_id = isset($data['device_id'])? $data['device_id']:0;
        if(!empty($userid)){
            try {
                if(isset($data['userid'])){
                    Yii::app()->db->createCommand("INSERT INTO permanent_unblock_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')")->execute();
                    
                }
                Yii::app()->db->createCommand("DELETE FROM black_listed_user WHERE users_id = $userid")->execute();
                Yii::app()->db->createCommand("UPDATE user_profile SET is_genuine=1 WHERE user_id = $userid")->execute();
                return true;
            } catch (Exception $ex) {
                return false;
            }
        }
    }
    
    public static function getuserinfo(){
        $user_id = Yii::app()->user->id;
        $result =  self::getuserProfile();
        unset($result['id']);
        unset($result['user_id']);
        return $result;
    }
    
    public static function unblockuser_cron(){
        $hash2 = 'failedLoginBlock';
        $keys = Yii::app()->cache->hkeys($hash2);
        //file_put_contents("/var/log/apps/failedlogin".date('Ymd').".log", "| unblockuser_cron ".  json_encode($keys)."\n", FILE_APPEND | LOCK_EX);
        $time = time();
        $userid = '';
        foreach($keys as $val){
            if(($time-Yii::app()->cache->hget($hash2,$val))>=3600){
                //file_put_contents("/var/log/apps/failedlogin".date('Ymd').".log", "| unblockuser_cron step2 time ".$time.' settime '.  Yii::app()->cache->hget($hash2,$val)."\n", FILE_APPEND | LOCK_EX);
                //unblocked user after 1 hour
                $user = $val;
                $query = "SELECT id FROM users where user_name=".$user;
                //file_put_contents("/var/log/apps/failedlogin".date('Ymd').".log", "| unblockuser_cron step3 ".  $query."\n", FILE_APPEND | LOCK_EX);
                $userid = Yii::app()->db->createCommand($query)->queryRow();
                //file_put_contents("/tmp/failedlogin".date('Ymd').".log", "| unblockuser_cron step4 ". json_encode($userid)."\n", FILE_APPEND | LOCK_EX);
                if(isset($userid['id']) && !empty($userid['id'])){
                    //file_put_contents("/var/log/apps/failedlogin".date('Ymd').".log", "| unblockuser_cron step5 ". json_encode($userid)."\n", FILE_APPEND | LOCK_EX);
                    self::unblockUser($userid);
                }
                Yii::app()->cache->hdel($hash2,$val);
            }
        }
    }
    
    public static function voucher_creator() {
        $res = Yii::app()->db->createCommand("SELECT user_name from users where role=4")->queryAll();
         $result = array();
        foreach ($res as $k => $v) {
            array_push($result, $v['user_name']);
        }
        return $result;
    }
    
    public static function getUserByCol($data){
         $column = key($data);
         $profileObj = new Users();
         return $profileObj->findByAttributes(array($column=>$data[$column]));
    }
    
    private static function setWallet($id){
        $wallet = new Wallets();
        $wallet->users_id = $id;
        $wallet->created_datetime=GeneralComponent::getFormatedDate();
        $wallet->save();
    }
    
    private static function pre_register($connection,$data){
        $sql = "INSERT INTO users (user_name,mobile_number,created_datetime) VALUES (:user_name,:mobile_number,:created_datetime)";
        $command=$connection->createCommand($sql);
        $command->bindParam(":user_name",$data['mobile_number'],PDO::PARAM_STR);
        $command->bindParam(":mobile_number",$data['mobile_number'],PDO::PARAM_STR);
        $command->bindParam(":created_datetime",GeneralComponent::getFormatedDate());

        $command->execute();
       
        $data['userid']=$connection->lastInsertID;
        self::setWallet($data['userid']);
        self::createProfile($connection, $data);
        self::manageTmpWallet($connection, $data);
    }
    
 }
