<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    private $_username;
    private $_role;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $is_api = (isset($_REQUEST['api']) && $_REQUEST['api'] === 'true') ? 1 : 0;
        $users = Users::model()->findByAttributes(array('user_name' => $this->username));
        if ($users === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($is_api === 1) {
            if (!$users->validateApiPassword($this->password)) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } else {
                $this->errorCode = self::ERROR_NONE;
                $this->_id = $users->id;
                $this->_role = $users->role;
                $this->setState('username', $this->username);
                $this->setState('role', $this->role);
            }
        } else if (!$users->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $users->id;
            $this->_role = $users->role;
            $this->setState('username', $this->username);
            $this->setState('role', $this->role);
            //$this->setState('role', $users->role);
           // Yii::app()->request->cookies['b2c'] = new CHttpCookie('b2c', "{'username': $this->username,'role':$this->role}");
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

    public function getUsername() {
        return $this->_username;
    }
    public function getRole() {
        return $this->_role;
    }

}
