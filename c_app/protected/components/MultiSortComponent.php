<?php
class MultiSortComponent
{
    var $key;
    var $i;
    var $url='http://54.235.193.96:8983/solr/pay1/select?';
    private function runsort ($myarray, $key_to_sort, $type_of_sort = '')
    {
        $this->key = $key_to_sort;
       
        if ($type_of_sort == 'desc')
            usort($myarray, array($this, 'myreverse_compare'));
        else
            usort($myarray, array($this, 'mycompare'));
           
        return $myarray;
    } 
     private function mycompare($x, $y)
    {
        if ( $x[$this->key] == $y[$this->key] )
            return 0;
        else if ( $x[$this->key] < $y[$this->key] )
            return -1;
        else
            return 1;
    }
   
    //for descending order
    private function myreverse_compare($x, $y){
        if ( $x[$this->key] == $y[$this->key] )
            return 0;
        else if ( $x[$this->key] > $y[$this->key] )
            return -1;
        else
            return 1;
    }
    
    public function MultiSortComponent($data){
        $mobile = trim($data['mobile']);
        $urlData = 'q=*:*&fq=+mobile:'.$mobile.'&fq=-param:[%22NULL%22+TO+*]&fq=+status:1&fl=param,ampunt,product_id,frequency&group=true&group.field=amount&wt=json&rows=800';
        $this->url = $this->url.$urlData;
       // http_build_query
    }
    public function get_solrResult($col = 'frequency',$limit=3){
        $columns = array('frequency','amount','param');
        $ord_arr = array('asc','desc');
        if (in_array($col, $columns)){
            $reqUrl = $this->url;
             try{ //curl frequest to fetch data from solr
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $reqUrl,
                ));
                // Send the request & save response to $resp
                $resp = curl_exec($curl);
                // Close request to clear up some resources
                 curl_close($curl);
              }
              catch(Exception $ex){      
                  echo $ex->getMessage();
              }

                $records = array();
                $records_row = array();
                $result = json_decode($resp,true);
                $resultArray=$result['grouped']['amount']['groups'];

               if(!empty($resultArray)){
                    foreach($resultArray as $rows){
                         $records[]=array('amount'=>$rows['groupValue'],'frequency'=>$rows['doclist']['numFound'],'productID'=>$rows['doclist']['docs'][0]['product_id'],'param'=>isset($rows['doclist']['docs'][0]['param'])?$rows['doclist']['docs'][0]['param']:"");
                     }

                    $reqArr = $this->runsort($records,$col,'desc'); //sort array
                    $limit = (count($reqArr)<$limit) ? count($reqArr) : $limit; 
                     for($i=0;$i<$limit;$i++){
                         $records_row[]=$reqArr[$i];
                     }
                     $result = array('status'=>'success','errCode'=>0,'description'=>
                                $records_row);
                }
                else{
                     $result = array('status'=>'failure','errCode'=>249,'description'=>'no data found');
                }

        }
        else{
            $result = array('status'=>'failure','errCode'=>250,'description'=>'please provide correct column name parameter');
        }
            return $result;
    }
    
    public function Distributor_Extra_Incentive($data,$dist_id=null){
//      $data['month'] = '04';                               
        $ch     = array();
        $resp   = array();
        $reqUrl = array();
        $result = array();
        $month  = isset($data['month'])?trim($data['month']):date("m");
        $year   =  date("Y");
        $days   =  cal_days_in_month(CAL_GREGORIAN, $month, $year);    // return no. of days in a month
        $start_date     = $year.'-'.$month.'-'.'01';                   // start date of month
        $end_date       = $year.'-'.$month.'-'.$days;                  // end date of month
        $distributor_id = isset($data['distributor_id'])?trim($data['distributor_id']):$dist_id;
        $i = 0 ;
        $reqUrl[$i++] = 'http://54.235.193.96:8983/solr/pay1/select?q=date%3A%5B'.$start_date.'+TO+'.$end_date.'%5D+AND'
                  . '+-status%3A%5B2+TO+3%5D+AND++distributor_id%3A'.$distributor_id.'+AND+product_id%3A2'
                  . '&rows=0&wt=json&indent=true&stats=true&stats.field=amount';
        $reqUrl[$i++] = 'http://54.235.193.96:8983/solr/pay1/select?q=date%3A%5B'.$start_date.'+TO+'.$end_date.'%5D+AND'
                  . '+-status%3A%5B2+TO+3%5D+AND++distributor_id%3A'.$distributor_id.'+AND+product_id%3A4'
                  . '&rows=0&wt=json&indent=true&stats=true&stats.field=amount';
        $reqUrl[$i++] = 'http://54.235.193.96:8983/solr/pay1/select?q=date%3A%5B'.$start_date.'+TO+'.$end_date.'%5D+AND'
                . '+-status%3A%5B2+TO+3%5D+AND++distributor_id%3A'.$distributor_id.'+AND+product_id%3A%5B36+TO+43%5D+OR+product_id%3A%5B45+'
                . 'TO+*%5D&rows=0&wt=json&indent=true&stats=true&stats.field=amount';
        try{ 
            for($j = 0 ; $j < $i ; $j++){
             $ch[$j] = curl_init();
             curl_setopt($ch[$j], CURLOPT_URL, $reqUrl[$j]);
             curl_setopt($ch[$j], CURLOPT_RETURNTRANSFER, 1); 
             $resp[$j] = curl_exec($ch[$j]);                   //Send the request & save response to $resp
             curl_close($ch[$j]);                              //Close request to clear up some resources
            }      
        }catch(Exception $ex){      
           echo $ex->getMessage();
        }
        
        for($i = 0 ; $i < $j ; $i++){
            $result[$i] = json_decode($resp[$i],true);
        }
        
        for($j = 0 ; $j < $i ; $j++){
            $res[$j] = $result[$j]['stats']['stats_fields']['amount']['sum'];
        }
        return $res;
    }
}
