<?php

class TransactionComponent {
    
    public static function RechargeTransaction($connection, $data_rev){
        $connection->createCommand("INSERT INTO recharge_transaction "
                . "(`transaction_id`,`" . $data_rev['recharge_column'] . "`,`amount`,`operator_id`,`recharge_type`,`device_type`,`type`,`profile_id`,`status`,`recharge_flag`,`datetime`) "
                . "values('" . $data_rev['transaction_id'] . "','" . $data_rev[$data_rev['recharge_column']] . "','" . $data_rev['amount'] . "','" . $data_rev['operator_id'] . "','"
                . "" . $data_rev['recharge_type'] . "','" . $data_rev['device_type'] . "','" . $data_rev['type'] . "',"
                . "'" . $data_rev['profile_id'] . "','','" . $data_rev['flag']. "','" . GeneralComponent::getFormatedDate() . "')")->execute();
    }
    
    public static function BillingTransaction($connection, $data_rev){
        $data_rev['additional_detail1'] = isset($data_rev['additional_detail1']) ? $data_rev['additional_detail1'] : "";

        $connection->createCommand("INSERT INTO `billing_transaction` (`transaction_id`, `mobile_number`, `subscriber_id`, `amount`, `service_charge`, `service_tax`, `operator_id`,`additional_detail1`, `payment_flag`,`datetime`)"
        . " VALUES ('" . $data_rev['transaction_id'] . "', '" . $data_rev['mobile_number'] . "', '" . $data_rev['subscriber_id'] . "', '" . $data_rev['base_amount'] . "',"
        . " '" . $data_rev['service_charge'] . "','" . $data_rev['service_tax'] . "','" . $data_rev['operator_id'] . "', "
                . "'" . $data_rev['additional_detail1'] . "', '" . $data_rev['payment_flag']. "', '" . GeneralComponent::getFormatedDate() . "')")->execute();
    }

    public static function DealTransaction($connection, $data_rev){
        $result = $connection->createCommand("SELECT * FROM deals WHERE id='".$data_rev['deal_id']."'")->queryAll();
        $data_rev['deal_detail']=$result[0];        
        $coupon_code = self::Generate_DealCoupon($connection, $data_rev);
        $data_rev['coupon_id'] = $connection->lastInsertID;
        $column = ""; $colvals ="";
        $cols_list = array('transaction_id','users_id','deal_id','offer_id','coupon_id','quantity','amount');
        foreach ($cols_list as $cols){
            $column .= (strlen($column) >0)?",`".$cols."`":"`".$cols."`";
            $colvals .= (strlen($colvals) >0)?",'".$data_rev[$cols]."'":"'".$data_rev[$cols]."'";
        }
        $connection->createCommand("INSERT INTO `deal_transaction` ($column) VALUES($colvals)")->execute();
        return $coupon_code;
    }
    
    public static function Generate_DealCoupon($connection, $data_rev){
        $column = ""; $colvals ="";
        $data_rev['createdon'] = GeneralComponent::getFormatedDate(); 
        $data_rev['status'] = 1;
        $code_prefix = trim($data_rev['deal_detail']['coupon_prefix']);
        $prefix = ($code_prefix === "NULL")?"":$code_prefix;
        $i = 0;
        while($i<1){
            try{            
                $data_rev['code'] = strtoupper($prefix).GeneralComponent::generate_unique_number(14-(strlen($prefix)));
                $cols_list = array('users_id','deal_id','offer_id','createdon','code','status');
                foreach ($cols_list as $cols){
                    $column .= (strlen($column) >0)?",`".$cols."`":"`".$cols."`";
                    $colvals .= (strlen($colvals) >0)?",'".$data_rev[$cols]."'":"'".$data_rev[$cols]."'";
                }
                $connection->createCommand("INSERT INTO `coupons` ($column) VALUES($colvals)")->execute();
                $i++;
            } catch (Exception $ex) {
                $i = 0;
            }        
        }
        return $data_rev['code'];
    }

    public static function newTransaction($connection, $data_rev){
        $tablename = "transactions";
        $data_rev['ip_address'] = CHttpRequest::getUserHostAddress();
        if(!isset($data_rev['trans_datetime'])){
            $data_rev['trans_datetime'] = GeneralComponent::getFormatedDate();
        }
        self::addData($connection, $tablename, $data_rev);
    }
    
    public static function newRefillTransaction($connection, $data_rev){
        $tablename = "refill_transaction";
        if(!isset($data_rev['datetime'])){
            $data_rev['datetime'] = GeneralComponent::getFormatedDate();
        }
        self::addData($connection, $tablename, $data_rev);
    }
    
    public static function newTempRefillTransaction($connection, $data_rev){
        $tablename = "temp_wallets";
        self::addData($connection, $tablename, $data_rev);
    }

    public static function addData($connection,$tablename, $data_rev){
        $qry_col = implode("`,`",array_keys($data_rev));
        $col_val = implode("','",array_map('mysql_escape_string',array_values($data_rev)));
        $connection->createCommand("INSERT into $tablename (`".$qry_col."`)  VALUES('".$col_val."')")->execute();
    }
    
    public static function updateTransaction($connection,$trans_id,$data=array()){
        $tablename = "transactions";
        if(count($data)>0){
            $update_data = "";
            foreach ($data as $col=>$val){
                $update_data .= (strlen($update_data)>0)?", `$col`='$val'":"`$col`='$val'";
            }
            $connection->createCommand("UPDATE $tablename SET $update_data WHERE trans_id='$trans_id'")->execute();
        }
    }

    public static function check_trans_by_client_req($data){
        try{
            if(!isset($data['client_req_id'])){ throw new Exception("Missing parameter",209); }
            $transObj = Transactions::model()->findAllByAttributes(array('response_id'=>$data['client_req_id']));
            if(count($transObj)<1){ throw new Exception("transaction not found",226); }
            $transdata = $transObj[0]->attributes;
            $result = array('status'=>'success','errCode'=>'','description'=>$transdata['trans_id']);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($result);
    }
    
    public static function getTransactionlog_old($type=null,$mode=null,$page=0,$limit=10){
        try{            
            $offset = ($page > 0)?$page*$limit:$page;
            $condition_param = array('users_id'=>Yii::app()->user->id);
            if(!is_null($type)){
                if(!in_array($type,array('0','1'))){
                    throw new Exception("Invalid transaction type",222);
                }
                $condition_param = array_merge($condition_param, array('trans_type'=>$type));                
            }
            if(!is_null($mode)){
                if(!in_array($mode,array('wallet','online'))){
                    throw new Exception("Invalid transaction mode",223);
                }
                $condition_param = array_merge($condition_param, array('transaction_mode'=>$mode));                
            }
            $transObj = Transactions::model()->findAllByAttributes(
                    $condition_param,
                    array('limit'=>$limit,'offset'=>$offset,'order'=>'trans_datetime desc'));
            $transdetail = array();
            foreach ($transObj as $transaction=>$transval){            
                $transdetail[] = $transval->getAttributes(array('trans_category','trans_datetime','transaction_amount','closing_bal'));
            }
            $result = array('status'=>'success','errCode'=>'','description'=>$transdetail);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($result);
    }
    
    public static function getTransactionlog($type=null,$mode=null,$page=0,$limit=10){
        return self::getPurchaselog($page,$limit,$mode);
    }
    
    public static function getPurchaselog($page=0,$limit=10,$mode=NULL){
        try{
            $refundstatus = "";
            if($mode == "wallet" && $mode != null){
                $refundstatus = ",'4'";
            }
            $cond = "";
            if($mode != NULL){
                $cond = " AND txn.transaction_mode = '".  strtolower($mode)."'";
            }
            $qry = "SELECT t1.trans_id as transaction_id, t1.trans_datetime, t1.transaction_amount, t1.trans_category,t1.status,t1.transaction_mode,t1.number,
                t1.closing_bal,prod.name as operator_name,d.name as deal_name,ofr.name as offer_name
                FROM (
                SELECT txn.trans_id, txn.trans_datetime, txn.transaction_amount,txn.status,txn.transaction_mode,txn.closing_bal,
                dt.deal_id,dt.offer_id,
                (CASE  WHEN txn.status = '4' THEN concat(txn.trans_category,' (refund)') ELSE txn.trans_category END) as trans_category,
                (CASE 
                WHEN trans_category='recharge' 
                THEN 
                    (CASE 
                    WHEN rt.recharge_flag=2 
                    THEN rt.subscriber_id  
                    ELSE rt.mobile_number 
                    END) 
                ELSE 
                    (CASE 
                    WHEN trans_category='billing' 
                    THEN 
                        (CASE 
                        WHEN bt.payment_flag=2 
                        THEN bt.subscriber_id 
                        ELSE bt.mobile_number 
                        END) 
                    ELSE 
                        (CASE
                        WHEN trans_category='refill'
                        THEN u.mobile_number
                        ELSE 
                            (CASE
                            WHEN trans_category='deal'
                            THEN 'N.A' 
                            ELSE 'N.A'
                            END) 	 
                        END) 
                    END)
                END) as number,
                (CASE 
                WHEN trans_category='recharge' 
                THEN 
                    (CASE
                    WHEN rt.recharge_flag = 1
                    THEN topr.product_id
                    ELSE
                        (CASE
                        WHEN rt.recharge_flag = 2
                        THEN dopr.product_id
                        ELSE
                            (CASE
                            WHEN rt.recharge_flag = 3
                            THEN topr.data_product_id
                            ELSE 0
                            END)
                        END)
                    END)
                ELSE 
                    (CASE
                    WHEN trans_category='billing' and payment_flag = 1
                    THEN topr1.postpaid
                    ELSE 0
                    END)
                END) as product
                FROM `transactions` as txn 
                    LEFT JOIN billing_transaction as bt ON txn.trans_id=bt.transaction_id 
                    LEFT JOIN recharge_transaction as rt ON txn.trans_id = rt.transaction_id
                    LEFT JOIN deal_transaction as dt ON txn.trans_id = dt.transaction_id
                    LEFT JOIN users as u ON txn.users_id = u.id
                    LEFT JOIN telecom_operators as topr ON rt.operator_id=topr.id 
                    LEFT JOIN dth_operators as dopr ON bt.operator_id=dopr.id
                    LEFT JOIN telecom_operators as topr1 ON bt.operator_id=topr1.id 
                        WHERE txn.status IN ('0','1','2','3' ".$refundstatus.") AND
                        txn.users_id=".Yii::app()->user->id." ".$cond."
                        ORDER BY 1 desc
                        LIMIT ".($page*$limit).",". $limit."
                        ) as t1 
                LEFT JOIN products as prod ON prod.id=t1.product  
                LEFT JOIN deals as d ON d.id=t1.deal_id
                LEFT JOIN offers as ofr ON ofr.id=t1.offer_id
                ORDER BY 1 DESC";
            $data = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$data);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $result;
    }
    
    public static function get_Deal_Transaction_details($trans_id){
        $qry = "SELECT dl.id, dl.transaction_id,dl.users_id,dl.deal_id,dl.offer_id,dl.quantity,dl.amount,d.name as deal_name,ofr.name as offer_name,
                ofr.discount,txn.trans_datetime,txn.transaction_mode,ofr.short_desc,ofr.long_desc,ofr.validity as expiry,
                ofr.actual_price,ofr.offer_price,ofr.discount,ofr.total_stock,ofr.stock_sold,cpn.code,
                (CASE WHEN cpn.status = 1 THEN 'UNUSED' ELSE (CASE WHEN cpn.status=2 THEN 'USED' ELSE (CASE WHEN cpn.status=3 THEN 'EXPIRED' ELSE 'UNUSED' END) END) END) as coupon_status,
                (CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url
                     FROM `deal_transaction` as dl 
                     LEFT JOIN deals d ON d.id = dl.deal_id 
                     LEFT JOIN offers ofr ON ofr.id = dl.offer_id 
                     LEFT JOIN transactions txn ON txn.trans_id = dl.transaction_id
                     LEFT JOIN coupons cpn ON cpn.id = dl.coupon_id
                         WHERE dl.transaction_id='".$trans_id."'";
        
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        return isset($data[0])?$data[0]:null;
    }
}
