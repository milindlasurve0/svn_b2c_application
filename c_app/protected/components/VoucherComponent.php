<?php

Class VoucherComponent 
{
	public static function generate_VCODE($data){
        try{
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            $mandatory_fields = array('client'=>'Client Name', 'amount'=>'Amount', 'count'=>'Number of voucher','voucher_type' => 'voucher_type');
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            $missing_params = array_diff(array_keys($mandatory_fields), array_keys($data));
            if (count($missing_params) > 0) {
                throw new Exception('Missing parameters : ' . implode(",", $missing_params), 209);
            }
            foreach($data as $k=>$val){
                 if($k=='amount' && !is_numeric($val)){
                     throw new Exception($message, 580);
                }
            }
            $emptylist = GeneralComponent::getEmptyElement(array_keys($mandatory_fields), $data);
            if (count($emptylist) > 0) {
                throw new Exception('field(s) (' . implode(',', $emptylist) . ') cannot be empty.', 210);
            }
            $voucher_table = "voucher";
            $voucher_gen_detail_table = "voucher_generation_details";
            $voucher_gen_detail = array('amount'=> $data['amount'],
                        'generated_date'=>  GeneralComponent::getFormatedDate(),
                        'voucher_client'=> $data['client'],
                        'voucher_type'=> $data['voucher_type'],
                        'quantity'=>$data['count']);
            TransactionComponent::addData($connection,$voucher_gen_detail_table, $voucher_gen_detail);
            $voucher_detail_id = $connection->lastInsertID;
            $vcode_length = isset($data['vlen'])?$data['vlen']:14;
            $only_numeric = isset($data['only_numeric'])?$data['only_numeric']:FALSE;
            if($data['voucher_type']==2){
                for($i=0;$i<$data['count'];$i++){
                    $voucher_data = array('voucher_code'=> GeneralComponent::generate_unique_number(6,FALSE),
                            'amount'=> $data['amount'],
                            'created_date'=>  GeneralComponent::getFormatedDate(),
                            'voucher_client'=> $data['client'],
                            'is_dry'=> isset($data['is_dry'])?$data['is_dry']:0,
                            'voucher_detail_id'=>$voucher_detail_id);
                    try{
                        TransactionComponent::addData($connection,$voucher_table, $voucher_data);
                    } catch (Exception $ex) {
                        $i--;
                    }                
                }
            }else if($data['voucher_type']==1){
                $voucher_table = 'promo_voucher';
                    $voucher_data = array('code'=> $data['code'],
                            'amount'=> $data['amount'],
                            'voucher_client'=>$voucher_detail_id,
                            'total_amount'=> $data['balance'],
                            'validity'=> $data['validity'],
                            'max_per_person'=> $data['max_per_person'],
                            'quantity'=> $data['quantity']);
                    try{
                        TransactionComponent::addData($connection,$voucher_table, $voucher_data);
                    } catch (Exception $ex) {
                        //$i--;
                    }      
            }
            
            
            $transaction->commit();
            $result = array('status'=>'success','errCode'=>0,'description'=>array('voucher_detail_id'=>$voucher_detail_id));
        } catch (Exception $ex) {
            $transaction->rollback();    
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $result;
    }
    
    public static function getVoucher_detail($data){
        try{
            
                if(!isset($data['vcode']) && !ctype_alnum($data['vcode'])){
                    $message = Yii::app()->params['message_template']['VALID_PARAM'];
                    throw new Exception($message, 580);
                }
            
            $vcode = isset($data['vcode'])?strtoupper($data['vcode']):0;
            $vqry = "SELECT v.serial_id, v.voucher_code, v.amount, v.created_date, v.status, vd.max_user_per_voucher,"
                    . "v.is_dry, vd.voucher_type, vd.expiry_in_days "
                    . "FROM voucher as v "
                    . "LEFT JOIN voucher_generation_details as vd ON v.voucher_detail_id=vd.id "
                    . "WHERE v.voucher_code = '".  strtoupper(trim($vcode))."'";
            $vdata = Yii::app()->db->createCommand($vqry)->queryAll();
            $data['userid'] = Yii::app()->user->getId();
                if(empty($data['userid'])){
                   throw new Exception('UNAUTHORIZE ACCESS',201);
                }
            if($vdata){
                $vdata = $vdata[0];
                $data['vdetail'] = $vdata;
                if(self::is_voucher_expired($data)){
                    throw new Exception('Voucher Expired',231);
                }
                if(self::is_voucher_used($data)){
                    throw new Exception('Voucher Already used',234);
                }
                if(Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'][$data['vdetail']['voucher_type']] == "DISCOUNT"){
                    $resultdata = self::verify_discount_voucher($data);
                }
                if(Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'][$data['vdetail']['voucher_type']] == "PROMO"){
                    $resultdata = self::verify_promo_voucher($data);
                }
                if(Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'][$data['vdetail']['voucher_type']] == "WALLET"){
                    $resultdata = self::verify_wallet_topup($data);
                }
            }else{
                
                $updatedAt =  GeneralComponent::getFormatedDate();
                $query = "INSERT INTO voucher_trail (code,user_id,updatedAt) VALUES ('".$data['vcode']."','".$data['userid']."','".$updatedAt."') ";
                //file_put_contents("/tmp/voucher_".date('Ymd').".log", "| step1 ".$query."\n", FILE_APPEND | LOCK_EX);
                Yii::app()->db->createCommand($query)->execute();
                if($data['vcode']=='P100' || $data['vcode']=='p100'){
                    throw new Exception('Oops! Max limit reached, 20000 users have already used it today. Stay connected for more!',233);
                }
                if(strtoupper($data['vcode'])=='CONIPHN6'){
                    throw new Exception('Yayy ! Entry accepted for the iphone6 contest. 3 winners will be declared on 05 September, 2015. Stay updated on https://www.facebook.com/pay1app',233);
                }
                throw new Exception('Invalid Voucher',232);
            }
        } catch (Exception $ex) {
            $resultdata = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $resultdata;
    }
    
    public static function verify_promo_voucher($data){
        
    }
    
    public static function verify_discount_voucher($data){
        
    }
    
    public static function verify_wallet_topup($data){        
        if($data['vdetail']['is_dry']){
            throw new Exception('Voucher is  Inactive',233);
        }else{
            $result = array('serial_id'=>$data['vdetail']['serial_id'],
                    'amount'=>$data['vdetail']['amount'],
                    'type'=>$data['vdetail']['voucher_type']);
            return array('status'=>'success','errCode'=>0,'description'=>$result);
        }        
    }
    
    public static function is_voucher_expired($data){
        $expiry_days = $data['vdetail']['expiry_in_days'];
        if($expiry_days > 0){
            $voucher_created_time = date($data['vdetail']['created_date']);
            if(strtotime(date('Y-m-d H:i:s')) >= strtotime($voucher_created_time.'+ $expiry_days days')){
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public static function is_voucher_used($data){
        $status = $data['vdetail']['status'];
        if($status > 0){
            return TRUE;
        }
        return FALSE;
    }
    
    public static function get_deal_couponCode($data){
        $user_id = Yii::app()->user->id;
        $coupon_qry = "SELECT * FROM coupons WHERE users_id='$user_id' AND deal_id='".(int)$data['deal_id']."' AND offer_id='".(int)$data['offer']."'";
        $vdata = Yii::app()->db->createCommand($coupon_qry)->queryAll();
        if($vdata){
            $result = array('status'=>'success','errCode'=>0,'description'=>array('deal_coupon_code'=>$vdata[0]['code']));
        }else{
            $result = array('status'=>'failure','errCode'=>230,'description'=>'No deals Coupon Found');
        }
        return $result;
    }
    
    public static function get_coupondetails($data){
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : $data['user_id'];
        $txn_id = $data['txn_id'];
        $coupon_qry = "SELECT d.name as deal, la.name as area,c.id,c.code,c.pin,c.createdon,c.status,dt.expiry_date "
                . "FROM deal_transaction as dt INNER JOIN  coupons as c ON (dt.coupon_id=c.id AND dt.transaction_id = $txn_id) "
                . " LEFT JOIN deal_locations AS dl ON ( dl.deal_id = dt.deal_id )
                    LEFT JOIN locator_area AS la ON ( dl.area_id = la.id ) 
                    LEFT JOIN deals AS d ON ( d.id = dt.deal_id ) "
                . "WHERE dt.users_id='$user_id' ";
        //file_put_contents("/tmp/get_coupondetails", "| : ".$coupon_qry."\n", FILE_APPEND | LOCK_EX);
        $vdata = Yii::app()->db->createCommand($coupon_qry)->queryAll();
        if($vdata){
            $result = array('status'=>'success','errCode'=>0,'description'=>$vdata);
        }else{
            $result = array('status'=>'failure','errCode'=>230,'description'=>'No deals Coupon Found');
        }
        return $result;
    }
    
    public static function redeem_voucher($data){
        try{
            $data['vcode'] = isset($data['vcode'])?strtoupper($data['vcode']):0;
            $vdetail = self::getVoucher_detail($data);
            if($vdetail['errCode'] == 0){
                $vdetail['vcode'] = $data['vcode'];
                if(Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'][$vdetail['description']['type']] == "DISCOUNT"){
                        $resultdata = self::redeem_discount_voucher($vdetail);
                    }
                    if(Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'][$vdetail['description']['type']] == "PROMO"){
                        $resultdata = self::redeem_promo_voucher($vdetail);
                    }
                    if(Yii::app()->params['GLOBALS']['_VOUCHER_TYPE'][$vdetail['description']['type']] == "WALLET"){
                        if(is_null(Yii::app()->user->getId())){
                            $vdetail['mobile_number'] = $data['mobile_number'];
                            $vdetail['transaction_mode'] = 'voucher';
                            $tmp_result = WalletComponent::refillwallet($vdetail);
                            if($tmp_result['errCode']== 0){
                                $resultdata = array('status'=>'success','errCode'=>0,'description'=>'voucher redeem successfully');
                            }else{
                                throw new Exception($tmp_result['description'],$tmp_result['errCode']);
                            }
                        }else{
                            $resultdata = self::redeem_wallet_topup($vdetail);
                        }                    
                    }
            }else{
                $resultdata = $vdetail;
            }    
        } catch (Exception $ex) {
            $resultdata = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }        
        return $resultdata;
    }

    public static function redeem_discount_voucher($data) {
        
    }

    public static function redeem_promo_voucher($data) {
        
    }

    public static function redeem_wallet_topup($data) {
        try{
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            if(is_null(Yii::app()->user->getId())){
                $uObj = Users::model()->findAllByAttributes(array('user_name'=>$data['mobile_number']));            
            }else{
                $uObj = Users::model()->findAllByAttributes(array('id'=>Yii::app()->user->id));
            }
            $u_detail = $uObj[0]->attributes;
            $voucher_redeem_data = array('trans_category' =>'refill','transaction_mode'=>'voucher','users_id'=>$u_detail['id'],'trans_type'=>1,
                    'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>6,'transaction_amount'=>$data['description']['amount']);
            $walletdetail = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $u_detail['id'] . " FOR UPDATE")->queryAll();
            if($walletdetail){
                $walletObj = $walletdetail[0];
                $voucher_redeem_data['opening_bal'] = $walletObj['account_balance'];
                $voucher_redeem_data['closing_bal'] = $voucher_redeem_data['opening_bal'] + $voucher_redeem_data['transaction_amount'];
                if(Yii::app()->params['GLOBALS']['MAX_WALLET_BALANCE_ALLOWED'] < $voucher_redeem_data['closing_bal']){
                    throw new Exception("Wallet Amount Exceeding Max Allowed Amount",229);
                }
                TransactionComponent::newTransaction($connection, $voucher_redeem_data);
                $vdata['txnid'] = $connection->lastInsertID;
                $vdata['status'] = 3;
                self::update_voucher_status($connection, $data['vcode'], $vdata);
                $refill_data = array('transaction_id'=>$vdata['txnid'],'amount'=>$voucher_redeem_data['transaction_amount'],'userid'=>$u_detail['id']);
                WalletComponent::refill_user_wallet($connection, $refill_data);
                $result = array('status'=>'success','errCode'=>0,'description'=>'successfully redeem');
                $transaction->commit();
            }else{
                throw new Exception('user wallet issue',235);
            }
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $result;
    }

    public static function update_voucher_status($connection, $vcode, $data){
        $tablename = "voucher";
        if(count($data)>0){
            $update_data = "";
            foreach ($data as $col=>$val){
                $update_data .= (strlen($update_data)>0)?", `$col`='$val'":"`$col`='$val'";
            }
            $connection->createCommand("UPDATE $tablename SET $update_data WHERE voucher_code='$vcode'")->execute();
        }
    }
    
    public static function check_usercashback($data){
        if(!isset($data['code']) || empty($data['code']) || !ctype_alnum($data['code'])){
             return array('status'=>'failure','errCode'=>274,'description'=>'please send code to verify.');
        }
        if(!isset($data['recharge_amount']) || empty($data['recharge_amount']) || !is_numeric($data['recharge_amount'])){
             return array('status'=>'failure','errCode'=>274,'description'=>'please send recharge amount to verify.');
        }
        $code = strtolower($data['code']);
        $recharge_amount = $data['recharge_amount'];
        $current_date = date('Y-m-d');
        $user_id = (isset( $data['user_id']) && !empty( $data['user_id'])) ? $data['user_id'] : Yii::app()->user->id;
        if(empty($user_id)){
            return array('status'=>'failure','errCode'=>201,'description'=>'UNAUTHORIZE ACCESS');
        }
        $connection = Yii::app()->db;
        $query = "SELECT code,amount,total_amount,validity,quantity,min_amount,max_limit,percent FROM promo_voucher where LOWER(code)='$code'";
        $vouchers = $connection->createCommand($query)->queryRow();
        $uuid = $connection->createCommand("SELECT uuid FROM user_profile where user_id = $user_id AND is_genuine=1")->queryRow();
        if(isset($uuid['uuid'])){
            $uuid = $uuid['uuid'];
        }else{
             $msg = Yii::app()->params['message_template']['CASHBACK_INVALID'];
            return array('status'=>'failure','errCode'=>274,'description'=>$msg);
        }
        if(!empty($vouchers['percent'])){
          $voucheramount = ceil($data['recharge_amount']*0.01*$vouchers['percent']); 
          if($voucheramount>100)
              $voucheramount = 100;
        }else{
            $voucheramount = $vouchers['amount'];
        }
        
        
        $used_code = $connection->createCommand("SELECT * FROM user_cashback where  code='$code' AND is_used!=2")->queryAll();
        
        $sumamount = $connection->createCommand("SELECT sum(amount) as sum_amount FROM user_cashback where  code='$code'  AND is_used!=2")->queryRow();
        
        $query = "SELECT user_id,uuid,code,trans_id,is_used FROM user_cashback where code='$code' AND (user_id='$user_id' OR uuid='$uuid') AND is_used!=2";
        $result  = $connection->createCommand($query)->queryAll();
         
        if(!isset($vouchers['code'])){
            $msg = Yii::app()->params['message_template']['CASHBACK_INVALID'];
            return array('status'=>'failure','errCode'=>274,'description'=>$msg);
        }else if($vouchers['validity']<$current_date){
            $msg = Yii::app()->params['message_template']['CASHBACK_EXPIRED'];
            return array('status'=>'failure','errCode'=>274,'description'=>$msg);
        }
        else if($recharge_amount < $vouchers['min_amount']){
            return array('status'=>'failure','errCode'=>274,'description'=>'Amount should be greater than or equal to '.$vouchers['min_amount']);
        }else if( !empty($sumamount['sum_amount']) && $sumamount['sum_amount']>($vouchers['total_amount']-$voucheramount)){
             $msg = Yii::app()->params['message_template']['CASHBACK_EXPIRED'];
            return array('status'=>'failure','errCode'=>274,'description'=>$msg);
        }
        else if(empty($vouchers['percent']) && $vouchers['quantity']<count($used_code)){
            $msg = Yii::app()->params['message_template']['CASHBACK_EXPIRED'];
            return array('status'=>'failure','errCode'=>274,'description'=>$msg);
        }
        
        
        if(count($result)>0){
            $msg = Yii::app()->params['message_template']['CASHBACK_USED'];
            
            return array('status'=>'failure','errCode'=>274,'description'=>$msg);
        }else{
            $var = 'amount';
            $msg = Yii::app()->params['message_template']['CASHBACK_SUCCESS'];
            $msg = GeneralComponent::replaceword('<'.strtoupper($var).'>',$voucheramount, $msg);
          //  $msg = "Cashback of Rs. ".$vouchers['amount']." will be credited in your Pay1 wallet within 24hrs of this transaction.";
           return array('status'=>'success','errCode'=>0,'description'=>$msg,'code'=>$vouchers['code']);
        }
    }
    
    public static function user_cashback($data){
        //add data to user_cashback table
        try{
            //file_put_contents("/tmp/user_cashback".date('Ymd').".log", "| step1 ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
            $user_id = (isset( $data['user_id']) && !empty( $data['user_id'])) ? $data['user_id'] : Yii::app()->user->id;
            $code  = $data['code'];
            $connection = Yii::app()->db;
            $uuid = $connection->createCommand("SELECT uuid FROM user_profile where user_id = $user_id")->queryRow();
            //file_put_contents("/tmp/user_cashback".date('Ymd').".log", "| step2 "." SELECT uuid FROM user_profile where user_id = $user_id ".  json_encode($uuid)."\n", FILE_APPEND | LOCK_EX);
            $uuid = $uuid['uuid'];
            $recharge_amount = isset($data['recharge_amount']) ? $data['recharge_amount'] : 0;
            $voucher = $connection->createCommand("SELECT amount,percent FROM promo_voucher where code = '$code'")->queryRow();
            //file_put_contents("/tmp/user_cashback".date('Ymd').".log", "| step3 ".  json_encode($voucher)."\n", FILE_APPEND | LOCK_EX);
            if(isset($voucher['percent']) && !empty($voucher['percent']) && !empty($recharge_amount)){
                $amount = ceil($recharge_amount*0.01*$voucher['percent']);
                if($amount>100)
                    $amount = 100;
                        
            }else{
                $amount = $voucher['amount'];
            }
            
            $transaction_id = $data['transaction_id'];
            $query = "INSERT INTO user_cashback (user_id,uuid,code,trans_id,is_used,amount) values ('$user_id','$uuid','$code','$transaction_id',0,'$amount')";
            //file_put_contents("/tmp/user_cashback".date('Ymd').".log", "| step4 ".  $query."\n", FILE_APPEND | LOCK_EX);
            $connection->createCommand($query)->execute();
        }
        catch (Exception $ex){
             //file_put_contents("/var/log/apps/user_cashback_exception".date('Ymd').".log", "| query ". json_encode($ex)."\n", FILE_APPEND | LOCK_EX);
        }
        
    }
    
    public static function cashback(){
        try{
            $current_date = GeneralComponent::getFormatedDate();
            $connection = Yii::app()->db;
            $query = "SELECT t.trans_id,t.`users_id` , uc.code, uc.amount
                        FROM promo_voucher AS pro
                        LEFT JOIN  `user_cashback` AS uc ON ( pro.code = uc.code ) 
                        LEFT JOIN transactions AS t ON ( uc.trans_id = t.trans_id ) 
                        WHERE uc.`is_used` =0 AND t.status =2 AND t.users_id=uc.user_id AND (TIME_TO_SEC(TIMEDIFF('$current_date',trans_datetime))/60) > 15";
           // file_put_contents("/tmp/user_cashback_query".date('Ymd').".log", "| query ".  $query."\n", FILE_APPEND | LOCK_EX);
            $codes =  $connection->createCommand($query)->queryAll();
            if(count($codes)>0){
                foreach ($codes as $row){

                    $walletdata = $connection->createCommand("SELECT account_balance FROM wallets WHERE users_id =" . $row['users_id'] . " FOR UPDATE")->queryRow();;
                    $balance = $walletdata['account_balance'];

                    $update = "UPDATE wallets SET account_balance=account_balance+".$row['amount']." where users_id=".$row['users_id'];
                    $connection->createCommand($update)->execute();
                    //insert in transaction table
                     $data = array('opening_bal'=>$walletdata['account_balance'],'transaction_amount'=>$row['amount'],'closing_bal'=>$walletdata['account_balance']+$row['amount'],
                                'trans_category'=>'refill','users_id'=>$row['users_id'],'trans_type'=>'1','transaction_mode'=> 'voucher'
                               ,'status'=>1,'trans_datetime'=>GeneralComponent::getFormatedDate(),'recharge_flag'=>'0','ref_trans_id'=>$row['trans_id'],'status'=>'2');
                    //file_put_contents("/tmp/user_cashback_query".date('Ymd').".log", "| query ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
                    TransactionComponent::newTransaction($connection, $data);
                    $connection->createCommand("UPDATE user_cashback SET is_used=1 where user_id=".$row['users_id']." AND code= '".$row['code']."' AND is_used!=2")->queryAll();
                }
            }
        }
        catch (Exception $ex){
            file_put_contents("/tmp/user_cashback_query".date('Ymd').".log", "| query ".  json_encode($ex)."\n", FILE_APPEND | LOCK_EX);
        }
        
    }
    
    public static function revert_cashback($connection,$data){
        //file_put_contents("/var/log/apps/revert_cashback".date('Ymd').".log", "| step1 ".json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        $users_id = isset($data['user_id']) ? $data['user_id'] : "";
         $trans_id = isset($data['txn_id']) ? $data['txn_id'] : "";
         if(!empty($users_id) && !empty($trans_id)){
             $result =  $connection->createCommand("SELECT uc.amount,uc.is_used  FROM promo_voucher as pr LEFT JOIN user_cashback as uc ON (pr.code=uc.code) WHERE uc.trans_id = $trans_id")->queryRow();
                if(isset($result['is_used']) && $result['is_used']==0){
                    // $query = "DELETE FROM user_cashback WHERE trans_id = $trans_id ";
                    $query = "UPDATE user_cashback SET is_used=2 WHERE trans_id = $trans_id ";
                     $connection->createCommand($query)->execute();
                     return true;
                }
                if(isset($result['amount'])){
                    try{
                        //$query = "DELETE FROM user_cashback WHERE trans_id = $trans_id ";
                         $query = "UPDATE user_cashback SET is_used=2 WHERE trans_id = $trans_id ";
                        if($connection->createCommand($query)->execute()){
                            $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id='$users_id' FOR UPDATE")->queryRow();

                            file_put_contents("/var/log/apps/revert_cashback".date('Ymd').".log", "| query ".json_encode($data)." ". $query."\n", FILE_APPEND | LOCK_EX);

                            $tqry = "UPDATE transactions SET status ='3' where ref_trans_id=$trans_id AND transaction_mode='voucher' AND trans_category='refill' AND users_id='$users_id'";                
                            $connection->createCommand($tqry)->execute();
                            $rdata = array('opening_bal'=>$walletdata['account_balance'],'transaction_amount'=>$result['amount'],'closing_bal'=>($walletdata['account_balance']-$result['amount']),
                                       'trans_category'=>'refill','users_id'=>$users_id,'trans_type'=>'0','transaction_mode'=>'voucher','trans_datetime'=>GeneralComponent::getFormatedDate(),
                                       'status'=>4,'ref_trans_id'=>$trans_id);
                           TransactionComponent::newTransaction($connection, $rdata);
                          //refund amount to wallet
                          $connection->createCommand("UPDATE wallets SET account_balance = account_balance - " . $result['amount'] . " WHERE users_id='" . $users_id . "'")->execute();
                        }
                   }
                   catch(Exception $ex){

                        file_put_contents("/var/log/apps/revert_cashback".date('Ymd').".log", "| error ". json_encode($ex)."\n", FILE_APPEND | LOCK_EX);
                    }
                }
         }
        
        
    }
    
    public static function voucher_report($data){
        $condition = "";
        if(isset($data['vcode']) && !empty($data['vcode']) && $data['v_type']==2){
            $condition = "AND v.voucher_code='".trim($data['vcode'])."'";
        }else if(isset($data['vcode']) && !empty($data['vcode']) && $data['v_type']==1){
            $condition = "AND v.code='".trim($data['vcode'])."'";
        }
        if(isset($data['start_date']) && !empty($data['start_date']) && isset($data['end_date']) && !empty($data['end_date'])){
            $condition = "AND trans.trans_date BETWEEN '".trim($data['start_date'])."' AND '".trim($data['end_date'])."'";
        }
        if(isset($data['mobile_number']) && !empty($data['mobile_number'])){
            $condition = "AND u.mobile_number=".trim($data['mobile_number']);
        }
        if($data['v_type']==1){
            $query = "SELECT u.mobile_number 'User Mobile',v.code 'Voucher',vg.generated_date 'Voucher Created On',uc.amount 'Amount',trans.trans_date 'Voucher Used On',
                        (CASE WHEN uc.is_used=0 THEN 'Unused' WHEN uc.is_used=1 THEN 'Used' WHEN uc.is_used=2 THEN 'Failed' END) 'Status'
                        FROM `voucher_generation_details` as vg 
                        LEFT JOIN promo_voucher as v ON (vg.id = v.voucher_client)
                        LEFT JOIN user_cashback as uc ON (v.code = uc.code)
                        INNER JOIN transactions as trans ON (uc.trans_id = trans.trans_id)
                        INNER JOIN users as u ON (u.id=uc.user_id)
                        where 1 $condition ";
        }else if($data['v_type']==2){
            $query = "SELECT u.mobile_number 'User Mobile', v.voucher_code 'Voucher', vg.generated_date 'Voucher Created On', trans.trans_date 'Voucher Used On', v.amount 'Amount',
                        (CASE WHEN v.status=0 THEN 'Unused' WHEN v.status=3 THEN 'Used' END) 'Status'
                        FROM  `voucher_generation_details` AS vg
                        LEFT JOIN voucher AS v ON ( vg.id = v.voucher_detail_id ) 
                        LEFT JOIN transactions AS trans ON ( v.txnid = trans.trans_id ) 
                        LEFT JOIN users AS u ON ( u.id = trans.users_id ) 
                        WHERE  voucher_code IS NOT NULL $condition";
        }
        
        $result = Yii::app()->db->createCommand($query)->queryAll();
        if(count($result)>0){
            return array('status'=>'success','errCode'=>0,'description'=>$result);
        }
        return array('status'=>'failure','errCode'=>0,'description'=>'No voucher code with this details');
    }
    public static function download_vouchercode($code){
        $connection = Yii::app()->db;
        $sql = "SELECT voucher_code 
	FROM  `voucher` 
	WHERE  `voucher_client` LIKE  '$code'";
        $command=$connection->createCommand($sql);
        $rows=$command->queryAll();
        if(!empty($rows)){
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=data.csv');
            // create a file pointer connected to the output stream
            $handle = fopen('php://output', 'w');
            foreach($rows as $result){
                fputcsv($handle,$result);
            }
            exit;
            
        }
    }
}