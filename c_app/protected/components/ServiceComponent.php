<?php

class ServiceComponent {
    /*
     * This function is used to make a recharge by missed call
     * @return : array
     */

    public static function delegateRecharge($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $dflag = TRUE;
        try{
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            if(!isset( $data['origincode']) || !isset($data['msisdn']) || !is_numeric( $data['origincode']) || !is_numeric($data['msisdn']))
                 throw new Exception($message, 580);
              $delObj = new DelegateRecharge();
            $delegateObj = $delObj->findBySql("SELECT *,dr.id as 'delegate_id' FROM `delegate_recharge` AS dr, global_missed_numbers AS gmn "
                    . "WHERE dr.`global_missed_numbers_id`=gmn.id AND dr.`mobile_number`='" . $data['msisdn'] . "' "
                    . "AND gmn.missed_number='" . $data['origincode'] . "' AND gmn.status='0' AND gmn.deleted='0' AND dr.status=1");
            $addition_detail = array('device_type' => 'missed_call', 'type' => 'flexi',
                    'profile_id' => '', 'recharge' => '', 'payment' => '', 'paymentopt' => 'wallet', 'payaddQuickpaymentopt' => 'wallet');
            if (!$delegateObj) {
                $dflag = FALSE;
                $data['delegate_id'] = 0;
            } else {
                $data['flag'] = $delegateObj->recharge_flag;
                $data['delegate_id'] = $delegateObj->id;
            }
            self::log_missed_call_history($data);
            if (!$dflag) {
                return json_encode(array('status' => 'failure', 'errCode' => '236', 'description' => 'number is not registed with delegate recharge.'));
            }
            if ($delegateObj->confirmation == '1') {
                //call notification url
                return json_encode(array('status' => 'success', 'errCode' => '', 'description' => 'waiting for confirmation'));
            }
            $data = array_merge($data, $delegateObj->getAttributes(), $addition_detail);
            $data['amount'] = $data['recharge_amount'];
            $data['operator_id'] = $data['operator_id'];
            $wall_key = 'misscall_wall_' . $data['origincode'] . "_" . $data['msisdn'];
            if (yii::app()->cache->exists($wall_key)) {
                $errmsg = "repeated request with in 1 hour";
                if (yii::app()->cache->llen($wall_key) <= 1) {
                    yii::app()->cache->rpush($wall_key, "send msg");
                    GeneralComponent::notifier(array('MISSCALL_FAILURE_USER', 'MISSCALL_FAILURE_NON_USER'), array('mobile' => $data['msisdn'], 'errmsg' => $errmsg, 'user_id' => $data['users_id'], 'amount' => $data['amount']));
                }
                return json_encode(array('status' => 'failure', 'errCode' => '237', 'description' => $errmsg));
            }
            unset($data['id'], $data['status'], $data['recharge_amount']);
            Yii::app()->user->id = $data['users_id'];
            $userApi =  Yii::app()->db->createCommand("SELECT api_version FROM user_profile where user_id=".$data['users_id'])->queryRow();
            $data['api_version'] = (isset($userApi['api_version']) && !empty($userApi['api_version'])) ? $userApi['api_version']:0;
            $result = WalletComponent::PaymentTranction($data);
            $resultdata = json_decode($result, TRUE);
            //print_r($resultdata);exit();
            if ($resultdata['status'] == "success") {
                $data['balance'] = isset($resultdata['description']['closing_balance']) ? $resultdata['description']['closing_balance'] : 0;
                $data['operator'] = isset($resultdata['description']['operator_name']) ? $resultdata['description']['operator_name'] : "";
                $data['loyalty_balance'] = isset($resultdata['description']['loyalty_balance']) ? $resultdata['description']['loyalty_balance'] : 0;
                yii::app()->cache->rpush($wall_key, "1");
                yii::app()->cache->expire($wall_key, 3600);
                $ndetail = array('txn_id' => $resultdata['description']['transaction_id'], 'operator_id' => $data['operator_id'], 'balance' => $data['balance'],
                        'operator' => $data['operator'], 'mobile' => $data['msisdn'], 'amount' => $data['amount'], 'user_id' => $data['users_id'],'loyalty_balance'=>$data['loyalty_balance']);
                if(isset($data['api_version']) && $data['api_version'] == 3 && isset($data['loyalty_balance'])){
                    GeneralComponent::notifier(array('MISSCALL_SUCCESS_USER_NEW'), $ndetail);
                }else{
                    GeneralComponent::notifier(array('MISSCALL_SUCCESS_USER'), $ndetail);
                }
            }elseif ($resultdata['status'] == "failure") {
                 $userArr = array('MISSCALL_FAILURE_USER', 'MISSCALL_FAILURE_NON_USER');
                 $user_nobalArr =  array('MISSCALL_FAILURE_USER_NOBAL', 'MISSCALL_FAILURE_NON_USER_NOBAL');
                 if(isset($data['api_version']) && $data['api_version'] == 3 ){
                     $userArr = array('MISSCALL_FAILURE_USER_NEW', 'MISSCALL_FAILURE_NON_USER');
                     $user_nobalArr =  array('MISSCALL_FAILURE_USER_NOBAL_NEW', 'MISSCALL_FAILURE_NON_USER_NOBAL');
                 }
                if ($resultdata['errCode'] == '211') {
                    GeneralComponent::notifier($user_nobalArr, array('mobile' => $data['msisdn'], 'errmsg' => $resultdata['description'], 'user_id' => $data['users_id'], 'amount' => $data['amount']));
                } else {
                    GeneralComponent::notifier($userArr, array('mobile' => $data['msisdn'], 'errmsg' => $resultdata['description'], 'user_id' => $data['users_id'], 'amount' => $data['amount']));
                }
            }
            return $result;
        }
        catch (Exception $ex){
            return json_encode(array('status' => 'failure', 'errCode' => $ex->getCode(),'description' => $ex->getMessage()));
        }
        //return WalletComponent::rechargeTranction($_REQUEST);        
    }

    /*
     * This function is use to get the confirmation for missed call recharge
     * @return : array
     */

    public static function confirmdelegateRecharge($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $delObj = new DelegateRecharge();
        $delegateObj = $delObj->findBySql("SELECT * FROM `delegate_recharge` AS dr, global_missed_numbers AS gmn "
                . "WHERE dr.`global_missed_numbers_id`=gmn.id AND dr.id='" . $data['id'] . "' AND gmn.status='0' AND gmn.deleted='0'");

        $addition_detail = array('device_type' => 'missed_call', 'type' => 'flexi',
                'profile_id' => '', 'recharge' => '', 'payment' => '', 'paymentopt' => 'wallet');
        $data['flag'] = $delObj->recharge_flag;
        $data = array_merge($_REQUEST, $delegateObj->getAttributes(), $addition_detail);
        $data['amount'] = $data['recharge_amount'];
        $data['operator'] = $data['operator_id'];
        unset($data['id'], $data['status'], $data['recharge_amount']);
        Yii::app()->user->id = $data['users_id'];
        return WalletComponent::PaymentTranction($data);
    }

    /*
     * This function is used to set delegate Recharge
     * @return : array
     */

    public static function setDelegateRecharge($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        try {
            $msg = "";
            $delObj = new DelegateRecharge();

            $data1 = $delObj->findBySql("SELECT * FROM `delegate_recharge` WHERE mobile_number='" . $data['mobile_number'] . "' AND operator_id = " . $data['operator'] . " AND recharge_amount = " . $data['amount'] . " AND status=0 AND users_id = " . Yii::app()->user->getId());
            if ($data1)
                throw new Exception("Already exists.", 401);

            $delegateObj = new DelegateRecharge();
            $delegateObj->operator_id = $data['operator'];
            $delegateObj->mobile_number = isset($data['mobile_number']) ? $data['mobile_number'] : 0;
            $delegateObj->subscriber_id = isset($data['subscriber_id']) ? $data['subscriber_id'] : 0;
            $delegateObj->stv = isset($data['stv']) ? 1 : 0;
            $delegateObj->status = 1;
            $delegateObj->recharge_amount = $data['amount'];
            $delegateObj->users_id = Yii::app()->user->getId();
            $delegateObj->created = GeneralComponent::getFormatedDate();
            $missed_data = ServiceComponent::getDelegateMissed($data['mobile_number']);
            if (count($missed_data) < 1) {
                throw new Exception("No Missed Call Number found.", 241);
            }
            $delegateObj->global_missed_numbers_id = $missed_data['id'];
            $delegateObj->recharge_flag = $data['flag'];
            $delegateObj->confirmation = isset($data['confirmation']) ? $data['confirmation'] : 0;
            if (!$delegateObj->save()) {
                throw new Exception(json_encode($delegateObj->errors), 228);
            }
            $msg = GeneralComponent::get_missedcallNumberById($missed_data['id']);
            $result = array('status' => 'success', 'errCode' => 0, 'description' => $msg);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
    }

    /*
     * This function is used to calculate service cost
     * @return : array
     */

    public static function calculateServiceCost($amount) {
        $chargeDetails = Yii::app()->params['accounting_cost'];
        $service_charge_amount = $amount * ($chargeDetails['service_charge_percent'] / 100);
        $service_tax_amount = $service_charge_amount * ($chargeDetails['service_tax_percent'] / 100);
        return array('base_amount' => $amount, 'amount' => ($amount + $service_charge_amount + $service_tax_amount), 'service_charge' => $service_charge_amount, 'service_tax' => $service_tax_amount);
    }

    /*
     * This function is used to get list of missed call recharge set
     * @return : array
     */

    public static function getDelegateMissed($mobile) {
        $missed_numList = Yii::app()->db->createCommand("SELECT id, missed_number from global_missed_numbers "
                        . "WHERE id NOT IN (SELECT global_missed_numbers_id FROM `delegate_recharge` WHERE mobile_number ='" . $mobile . "' )")->queryAll();
        return isset($missed_numList[0]) ? $missed_numList[0] : array();
    }

    /*
     * This function is used to save complain and forward them to define team
     * @return : array
     */

    public static function manageComplain($data = NULL) {
        $data = is_null($data) ? $_REQUEST : $data;
        try {
            $complain = new Complains();
            if (isset($data['resolve'])) {
                $complain = $complain->findByAttributes(array('transaction_id' => $data['transaction_id'], 'status' => 0));
                if (is_null($complain)) {
                    throw new Exception("No complain open for this transaction", 421);
                }
                $complain->status = 1;
            } else {
                $complain = $complain->findByAttributes(array('transaction_id' => $data['transaction_id'], 'status' => 0));
                if (!is_null($complain)) {
                    throw new Exception("complain already open for this transaction", 422);
                }
                $complain = new Complains();
                $complain->transaction_id = $data['transaction_id'];
                $complain->users_id = isset($data['users_id']) ? $data['users_id'] : Yii::app()->user->getId();
                $complain->complain_msg = $data['complain_msg'];
                $complain->created = GeneralComponent::getFormatedDate();
            }
            $complain->save();
            $param['trans_id'] = $data['transaction_id'];
            $param['complain_msg'] = $data['complain_msg'];
            VendorComponent::callPay1_complain($param);
            $result = array('status' => 'success', 'errCode' => '', 'description' => $complain->getPrimaryKey());
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return json_encode($result);
    }

    /*
     * This function is used to manage complain via missed call and open a complain against their last txn and 
     * alse used to provide data for payu refund.
     * @return : array 
     */
     //Complain Number       02267242266   
    public static function manage_missedcall_complain($data) {
        if (isset($data['msisdn'])) {
            $data['delegate_id'] = 0;
            $log_complain = self::log_missed_call_history($data);

            /* $qry = "SELECT txn.trans_id as transaction_id,txn.users_id, txn.trans_category, users. 
              FROM transactions as txn
              INNER JOIN users as u ON txn.users_id = u.id
              WHERE
              u.user_name='".$data['msisdn']."' order by txn.trans_id desc limit 1";
             */
            $qry = "SELECT txn.trans_id as transaction_id,txn.users_id, txn.trans_category, txn.trans_date 
                    FROM users as u
                        LEFT JOIN transactions as txn ON txn.users_id = u.id
                        WHERE
                            u.user_name='" . $data['msisdn'] . "' order by txn.trans_id desc limit 1";

            $data['complain_id'] = $log_complain['description'];
            $result = Yii::app()->db->createCommand($qry)->queryAll();
            $body = "User number: " . $data['msisdn'];

            if (count($result) > 0) {//user exists
                $param = $result[0];
                if (!empty($param['transaction_id'])) {//transaction exists
                    if (isset($param['type']) && $param['type'] == "refundcall") {
                        return self::pg_refund($param);
                    }
                    $param['trans_id'] = $param['transaction_id'];
                    $param['complain_msg'] = "Complain via missed call";
                     $resp = self::manageComplain($param);
                    $result =  VendorComponent::callPay1_complain($param);
                    $sub = 'Customer complaint via misscall';
                    $body .= "<br/>Transaction ID: " . $param['trans_id'];
                    //GeneralComponent::notifier(array('COMPLAINT_MISSCALL_SUCCESS'), $data);
                } else {//no transaction exists
                    $sub = 'Customer complaint via misscall - no txn';
                    $body .= "<br/>No transaction exists";
                    //GeneralComponent::notifier(array('COMPLAINT_MISSCALL_FAILURE'), $data);
                }
                GeneralComponent::sendMail('listen@pay1.in', 'Customer complaint via misscall', $body);
                $message = "Your request has been logged successfully. Our support executive will contact you soon.";
                GeneralComponent::sendSMS($data['msisdn'], $message);
            } else {//user does not exists
                $message = "This mobile number is not registered with us. Kindly give missed call from your registered number to raise the complaint.";
                GeneralComponent::sendSMS($data['msisdn'], $message);
                //$sub  = 'Customer complaint via misscall - non user';
                //$body .= "<br/>User does not exists";
            }
        }
    }

    /*
     * This function will call online pg web service to refund txn amount
     * @return : array
     */

    public static function pg_refund($data = array()) {
        $data['txn_id'] = $data['transaction_id'];
        $logger = isset($data['loggerObj']) ? $data['loggerObj'] : "";
        $track_id = isset($data['request_track_id']) ? $data['request_track_id'] : "";
        is_object($logger) ? $logger->info(($data['request_track_id']) . "last Transaction id fetched : " . $data['txn_id']) : "";
        return json_encode(OnlineComponent::call_payu_webservices($data));
    }

    /*
     * This function log the missed call request
     * @return : void
     */

    public static function log_missed_call_history($data) {
        try{
            $missCHObj = new MissedCallHistory();
            $missCHObj->missed_number = $data['origincode'];
            $missCHObj->mobile_number = $data['msisdn'];
            $missCHObj->intime = GeneralComponent::getFormatedDate();
            $missCHObj->delegate_id = $data['delegate_id'];
            $missCHObj->save();
        }catch(Exception $ex){
            
        }
    }

    /*
     * This function is use to get detail by quick pay id 
     * @return : array
     */

    public static function manage_quickpay($data) {
        $id = isset($data['id']) ? $data['id'] : null;
        $model = QuickPay::model();
        if (!is_null($id)) {
            $model = QuickPay::model()->findByPk($id);
        }
        return $model->getAttributes();
    }

    /*
     * This function is use to add transaction in quick pay to make it 1 click
     * @return : array
     */

    public static function addQuickpay($data = null) {
        try {
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            //$data = self::set_dataforQuickpay($data);
            $data['payment_flag'] = $data['flag'];
            
            $quickpay = new QuickPay();
            $quickpaykeys = array_keys($quickpay->getAttributes());
            file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step2 ".json_encode($data)."\n", FILE_APPEND | LOCK_EX);
            file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step3 quickpaykeys ".json_encode($quickpaykeys)."\n", FILE_APPEND | LOCK_EX);
            foreach ($data as $key => $val) {
                if ($key == 'amount' && $data['flag'] > 3) {
                    $val = isset($data['base_amount']) ? $data['base_amount'] : $val;
                    $val = 0;
                }
                if (in_array($key, $quickpaykeys)) {
                    file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step1 KEY ".$quickpay->$key." ".$val."\n", FILE_APPEND | LOCK_EX);
                    $quickpay->$key = $val;
                }
            }
            $quickpay->users_id = isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
            $quickpay->datetime = GeneralComponent::getFormatedDate();
            $quickpay->transaction_flag = 1;
            $successmsg = array('msg' => 'Quickpay added successfully', 'missed_number' => null);
            if (isset($data['delegate_flag']) && $data['delegate_flag'] === '1') {
                $data['operator'] = $data['operator_id'];
                $create_delegate = self::setDelegateRecharge($data);
                if ($create_delegate['errCode'] > 0) {
                    throw new Exception($create_delegate['description'], $create_delegate['errCode']);
                } else {
                    $delegate_detail = DelegateRecharge::model()->findByAttributes(
                                    array('users_id' => $quickpay->users_id, 'mobile_number' => $data['mobile_number'],
                                            'global_missed_numbers_id' => GeneralComponent::get_globalMissedCallumberId($create_delegate['description'])))->attributes;
                    $quickpay->delegate_id = $delegate_detail['id'];
                    $successmsg['missed_number'] = $create_delegate['description'];
                }
            }
            if ($quickpay->save()) {
                $result = array('status' => 'success', 'errCode' => '0', 'description' => $successmsg);
            } else {
                throw new Exception("error in quick pay object", 227);
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
    }

    /*
     * This function is use to get quick pay list by user id 
     * @return : array
     */

    public static function get_quickpaylist($uid = null, $limit = null) {
        try {
            $userid = is_null($uid) ? Yii::app()->user->id : $uid;
            $limit_cond = is_null($limit) ? "" : " limit 0," . $limit;
            $qry = "select t1.id, t1.flag, t1.name, t1.amount,t1.users_id,t1.stv,t1.operator_id,t1.product,t1.number,t1.delegate_flag,
                t1.missed_number, t1.confirmation,prod.name as operator_name,t1.transaction_flag,
                (CASE WHEN flag = 2 THEN CONCAT(LEFT(prod.name,1),'D') ELSE t1.operator_code END) as operator_code,t1.datetime 
                FROM (SELECT q.id, payment_flag AS flag, q.name, q.amount, q.users_id, q.stv, q.operator_id, q.datetime ,transaction_flag,
                (CASE WHEN payment_flag = 1 THEN topr.product_id ELSE ( CASE WHEN payment_flag = 3 	THEN topr.data_product_id
                    ELSE ( CASE WHEN payment_flag = 4 THEN topr.postpaid ELSE ( CASE WHEN payment_flag = 2 THEN dopr.product_id
                    ELSE '0' END ) END ) END ) END) AS product,topr.opr_code as operator_code,
                CASE WHEN payment_flag =1 OR payment_flag =3 OR payment_flag =4 THEN q.mobile_number ELSE q.subscriber_id END AS number,
                CASE WHEN q.delegate_id =0 THEN 0 ELSE (CASE WHEN dlr.status = 0 THEN 0 ELSE 1 END) END AS delegate_flag, gmn.missed_number, dlr.confirmation
                FROM `quick_pay` AS q
                LEFT JOIN delegate_recharge dlr ON q.delegate_id = dlr.id
                LEFT JOIN global_missed_numbers gmn ON dlr.global_missed_numbers_id = gmn.id
                LEFT JOIN telecom_operators topr ON q.operator_id = topr.id
                LEFT JOIN dth_operators dopr ON q.operator_id = dopr.id
                WHERE q.users_id='" . $userid . "' 
                ORDER BY transaction_flag, q.datetime DESC $limit_cond ) as t1 LEFT JOIN products as prod ON prod.id=t1.product"
                    . " WHERE t1.operator_id!=0 AND t1.product IS NOT NULL AND prod.name IS NOT NULL";
            // ORDER BY 1 DESC $limit_cond) as t1 LEFT JOIN products as prod ON prod.id=t1.product
            $quickpay_detail = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status' => 'success', 'errCode' => 0, 'description' => $quickpay_detail);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }

    /*
     * This function is used to activate and de-activate missed call service on specific quick pay entry for user
     * @return : array
     */

    public static function edit_quickpay($data) {
        try {
            $quickpObj = QuickPay::model()->findByAttributes(array('id' => $data['id']));
            $quickpObj->amount = $data['amount'];
            $quickpObj->modified_time = GeneralComponent::getFormatedDate();
            $successmsg = array('msg' => 'quickpay successfully updated', 'missed_number' => null);
            if (isset($data['delegate_flag'])) {
                $data = self::set_dataforQuickpay($data);
                if ($quickpObj->delegate_id > 0) {
                    $delg_Obj = DelegateRecharge::model()->findByAttributes(array('id' => $quickpObj->delegate_id));
                    $delg_Obj->status = $data['delegate_flag'];
                    $delg_Obj->confirmation = $data['confirmation'];
                    $missed_num = $delg_Obj->global_missed_numbers_id;
                    $delg_Obj->save();
                    if ($data['delegate_flag'] === '1') {
                        $successmsg['msg'] = ($data['delegate_flag'] === '1') ? "Missed call recharge service activated " : "Missed call recharge service deactivated ";
                        $successmsg['missed_number'] = GeneralComponent::get_missedcallNumberById($missed_num);
                    } else {
                        $successmsg['msg'] = ($data['delegate_flag'] === '1') ? "Missed call recharge service activated " : "Missed call recharge service deactivated ";
                    }
                }
                if ($data['delegate_flag'] === '1' && $quickpObj->delegate_id === '0') {
                    $data['operator'] = $data['operator_id'];
                    $create_delegate = self::setDelegateRecharge($data);
                    if ($create_delegate['errCode'] > 0) {
                        throw new Exception($create_delegate['description'], $create_delegate['errCode']);
                    } else {
                        $delegate_detail = DelegateRecharge::model()->findByAttributes(
                                        array('users_id' => $quickpObj->users_id, 'mobile_number' => $data['mobile_number'],
                                                'global_missed_numbers_id' => GeneralComponent::get_globalMissedCallumberId($create_delegate['description'])))->attributes;
                        $quickpObj->delegate_id = $delegate_detail['id'];
                        $successmsg['msg'] = ($data['delegate_flag'] === '1') ? "Missed call recharge service activated " : "Missed call recharge service deactivated ";
                        $successmsg['missed_number'] = $create_delegate['description'];
                    }
                }
            }
            if (!$quickpObj->save()) {
                throw new Exception("error in quick pay object", 227);
            }
            $result = array('status' => 'success', 'errCode' => '0', 'description' => $successmsg);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
    }

    /*
     * This function is used to delete or remove specific entry from user's quick pay list
     * @return : array
     */

    public static function del_quickpay($data) {
        try {
            $transaction = Yii::app()->db->beginTransaction();
            $quickpObj = QuickPay::model()->findByAttributes(array('id' => $data['id']));
            $delg_Obj = DelegateRecharge::model()->findByAttributes(array('id' => $quickpObj->delegate_id));
            if ($delg_Obj) {
                $delg_Obj->delete();
            }
            $quickpObj->delete();
            $transaction->commit();
            $result = array('status' => 'success', 'errCode' => 0, 'description' => 'quickpay deleted successfully');
        } catch (Exception $ex) {
            $transaction->rollback();
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' =>$message);
        }
        return $result;
    }

    /*
     * This function is used to set or format data according to the rrequest for inserting in quickpay list of user
     * @return : array
     */

    public static function set_dataforQuickpay($data) {
        switch ($data['flag']) {
            case 1:
                $data['mobile_number'] = $data['number'];
                break;
            case 2:
                $data['subscriber_id'] = $data['number'];
                break;
            case 3:
                $data['mobile_number'] = $data['number'];
                break;
            case 4:
                $data['mobile_number'] = $data['number'];
                break;
        }
        return $data;
    }

    /*
     * This function is used to check whether the current entry already exists in user's quick pay list or not
     * @return : boolean
     */

    public static function is_AlreadyQuickpay($data) {
        $data['payment_flag'] = $data['flag'];
        $userid = isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
        $quickpObj = QuickPay::model()->findByAttributes(array($data['recharge_column'] => $data[$data['recharge_column']],
                'users_id' => $userid, 'amount' => $data['amount'], 'operator_id' => $data['operator_id'],'payment_flag'=> $data['payment_flag']));
        if ($quickpObj) {
            $quickpObj->datetime = GeneralComponent::getFormatedDate();
            $quickpObj->transaction_flag = 1;
            $quickpObj->save();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * This function is use to update transaction_flag of quick pay whether it was successful or failure
     * @return : int (count of rows affected)
     */

    public static function update_Quickpay_by_param($data, $connection) {
        //file_put_contents("/tmp/chk_Quickpay_by_param", " " . json_encode($data) . "\n", FILE_APPEND | LOCK_EX);
        $userid = isset($data['users_id']) ? $data['users_id'] : Yii::app()->user->id;
        $query = "UPDATE quick_pay SET transaction_flag=" . $data['transaction_flag'] . " WHERE (mobile_number='" . $data['number'] . "' OR subscriber_id='" . $data['number'] . "')
         AND users_id = '" . $userid . "' AND amount='" . $data['amount'] . "' AND operator_id='" . $data['operator_id'] . "'";
        return $connection->createCommand($query)->execute();
    }

    /*
     * This function is use to delete entry from quick pay list of user
     * @return : void
     */

    public static function del_Quickpay_by_param($data) {
        $Qqry = "SELECT * FROM quick_pay WHERE (mobile_number='" . $data['number'] . "' OR subscriber_id='" . $data['number'] . "')
         AND users_id = '" . $data['user_id'] . "' AND amount='" . $data['amount'] . "' AND operator_id='" . $data['operator_id'] . "'";
        //$quickpObj = QuickPay::model()->findByAttributes(array($data['recharge_column']=>$data[$data['recharge_column']],
        //        'users_id'=>Yii::app()->user->id,'amount'=>$data['amount']));
        $quickpObj = QuickPay::model()->findBySql($Qqry);
        if ($quickpObj) {
            $quickpObj->delete();
        }
        return;
    }

    /*
     * This function is use to send notification to user via (sms & email) whenever requested
     * @return : array
     */

    public static function notifyme($data) {
        try {
            $deal_detail_array = TransactionComponent::get_Deal_Transaction_details($data['transaction_id']);
            GeneralComponent::notifier(array('DEAL_DETAILS'), array('txn_id' => $data['transaction_id']), $data['type']);
            $result = array('status' => 'success', 'errCode' => 0, 'description' => $data['type'] . ' sent successfully');
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message);
        }
        return $result;
    }

    /*
     * This function is use to send b2c app link to customer after their request via missedcall
     * @return : array
     */

    public static function sms_on_missedcall($data) {
        $data['delegate_id'] = 0;
        self::log_missed_call_history($data);
        $content = Yii::app()->params['message_template']['LINK_VIA_MISSED_CALL'];
        $download_urls = array('a_link' => 'goo.gl/Xmk1WJ');
        foreach ($download_urls as $var => $val) {
            $content = GeneralComponent::replaceword('<' . strtoupper($var) . '>', $val, $content);
        }
        GeneralComponent::sendSMS($data['msisdn'], urlencode($content));
        $nlog_data = array('mobile' => $data['msisdn'], 'sendtime' => GeneralComponent::getFormatedDate(), 'type' => 'sms', 'msg_content' => addslashes($content), 'status' => 1);
        GeneralComponent::log_notifications($nlog_data);
        return array('status' => 'success', 'errCode' => 0, 'description' => 'successfully sent sms');
    }

    /*
     * This is a offline pay1 txn status checking function which fetch txn status from B2B and update txn table accordingly
     * @return : boolean (if any status) or void
     */

    public static function check_pay1_transaction_status() {
        //file_put_contents("/var/log/apps/onlinetrans.log", "| check_pay1_transaction_status start ".date('Y-m-d h:i:s')."\n", FILE_APPEND | LOCK_EX);
        $connection = Yii::app()->db;
        $qry = "SELECT trans_id FROM `transactions` where trans_category in ('recharge','billing') "
                . " AND response_id = '' and status = 2 and ((UNIX_TIMESTAMP('" . date('Y-m-d H:i:s') . "') - UNIX_TIMESTAMP(trans_datetime))/60) > 3";
        $result = $connection->createCommand($qry)->queryAll();
        foreach ($result as $k => $v) {
            $result[$k] = $v['trans_id'];
        }
        $result = array_chunk($result, 8);
        foreach ($result as $trans_id) {
            $data['trans_id'] = implode(",", $trans_id);
            $pay1_resp_data = VendorComponent::get_pay1_transaction_status($data);
            $res = json_decode($pay1_resp_data, true);
            if (!isset($res['status']) || !(is_array($res['status']))) {
                exit();
            }
            foreach ($res['status'] as $res_id => $res_val) {
                $data['response_id'] = $res['status'][$res_id]['trans_id'];
                if ($res['status'][$res_id]['status'] == "failure") {
                    $failed_description = "Technical glitch. Please try after some time";
                    $failed_data = array('client_req_id' => $res['status'][$res_id]['req_id'],
                            'err_code' => $res['status'][$res_id]['errCode'],
                            'description' => (trim($res['status'][$res_id]['errCode']) == "E015") ? $failed_description : $res['status'][$res_id]['description'],
                            'trans_id' => "",
                            'status' => $res['status'][$res_id]['status'],
                            'vendor_code' => 'Signal7');
                    echo "failer array<hr>";
                    echo json_encode($failed_data);
                    if ((trim($res['status'][$res_id]['errCode']) != "E015")) {
                        $response_data = array('response_id' => $res['status'][$res_id]['trans_id']);
                        TransactionComponent::updateTransaction($connection, $res['status'][$res_id]['req_id'], $response_data);
                    }
                    $up_res = WalletComponent::updateTransaction($failed_data);
                    echo "-- wallet update res --";
                    echo json_encode($up_res);
                } elseif ($res['status'][$res_id]['status'] == "success") {
                    $success_data = array('response_id' => $res['status'][$res_id]['trans_id']);
                    $up_res = TransactionComponent::updateTransaction($connection, $res['status'][$res_id]['req_id'], $success_data);
                    echo "-- transaction update response --";
                    echo json_encode($up_res);
                }
            }
        }
        file_put_contents("/var/log/apps/onlinetrans.log", "| check_pay1_transaction_status END ".date('Y-m-d h:i:s')."\n", FILE_APPEND | LOCK_EX);
        return TRUE;
    }

    /*
     * This function check the validity of an offer id by checking its validity mode
     * @return : boolean
     */

    public static function check_Offer_validity($offerID) {
        $connection = Yii::app()->db;
        $qry = "SELECT total_stock,stock_sold,validity_mode,validity FROM `offers` where id = $offerID";
        $result = $connection->createCommand($qry)->queryAll();

        if ($result[0]['validity_mode'] == 2) { 
            if (strtotime('now') < strtotime($result[0]['validity'])) {
                return true;
            } else {
                return false;
            }
        } elseif ($result[0]['validity_mode'] == 1) {
            if ($result[0]['total_stock'] > $result[0]['stock_sold']) {
                return true;
            } else {
                return false;
            }
        } elseif ($result[0]['validity_mode'] == 3) {
            if (($result[0]['total_stock'] > $result[0]['stock_sold']) && (strtotime('now') < strtotime($result[0]['validity']))) {
                return true;
            } else {
                return false;
            }
        }
    }

    /*
     * This function call solr connector to fetch user records
     * @return : array
     */

    public static function get_response_via_solr($data) {
        $var = new MultiSortComponent($data);
        $limit = 6;
        $column_name = 'frequency';
        if (isset($data['limit']))
            $limit = trim($data['limit']);
        if (isset($data['column_name']))
            $column_name = $data['column_name'];
        $res = $var->get_solrResult($column_name, $limit);
        return $res;
    }

    /*
     * This function fetch user txn from solr and set them in quick pay
     * @return : array
     */

    public static function set_quickpay_via_solr($data = null) {
        $res = self::get_response_via_solr($data);
        $sdata = $res['description'];
        $qp_res = array();
        if (!is_array($sdata)) {
            $sdata = array();
        }
        foreach ($sdata as $k => $v) {
            if (isset(Yii::app()->params['solrMapping'][$sdata[$k]['productID']])) {
                $sdata[$k] = array_merge($sdata[$k], Yii::app()->params['solrMapping'][$sdata[$k]['productID']]);
            } else {
                $sdata[$k]['operator_id'] = 0;
            }
            $sdata[$k]['operator_id'] = isset($sdata[$k]['operator_id']) ? $sdata[$k]['operator_id'] : 0;
            $sdata[$k]['userid'] = $data['userid'];
            if (trim($sdata[$k]['param']) == "") {
                $sdata[$k]['mobile_number'] = $data['mobile'];
                $sdata[$k]['recharge_column'] = "mobile_number";
                $sdata[$k]['flag'] = 1;
            } else {
                $sdata[$k]['subscriber_id'] = $sdata[$k]['param'];
                $sdata[$k]['recharge_column'] = "subscriber_id";
                $sdata[$k]['flag'] = 2;
            }
            if (isset($sdata[$k]['flag'])) {
                $sdata[$k]['payment_flag'] = $sdata[$k]['flag'];
            }
            $qp_res[$sdata[$k][$sdata[$k]['recharge_column']]] = self::is_AlreadyQuickpay($sdata[$k]) ? "already exist" : self::addQuickpay($sdata[$k]);
        }
        return $qp_res;
    }

}
