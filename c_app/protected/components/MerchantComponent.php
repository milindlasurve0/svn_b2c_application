<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MerchantComponent {
    
    public static function getDealerInfo($dealerId) {        
        
        $query = "SELECT us.user_name,us.mobile_number,us.dealer_name  "
                . " FROM users as us "
                . " WHERE us.id=$dealerId";
        $dealsList = Yii::app()->db->createCommand($query)->queryAll();
        return (isset($dealsList) && count($dealsList)>0) ? $dealsList : array();
    }
    
    public static function getfreegiftreport($dealerId) {        
        $today = GeneralComponent::getFormatedDate('Y-m-d');
         $date = new DateTime('- 1 day');
         $date = $date->format('Y-m-d');
        $yday = $date;
        $query = "SELECT (SELECT COUNT( code )
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) WHERE d.dealer_id =$dealerId AND DATE(createdon) = '$today' AND c.status=1 AND d.status=1) as redeemtoday,"
                . "(SELECT COUNT( code )
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) WHERE d.dealer_id =$dealerId AND DATE(createdon) = '$yday' AND c.status=1 AND d.status=1) as redeemyday ,"
                . "(SELECT COUNT( code )
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) WHERE d.dealer_id =$dealerId AND c.status=1  AND d.status=1) as redeemtotal, "
                . "(SELECT COUNT( code )
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) WHERE d.dealer_id =$dealerId AND DATE(confirmation_date) = '$today' AND c.status=2 AND d.status=1) as claimtoday,"
                . "(SELECT COUNT( code )
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) WHERE d.dealer_id =$dealerId AND DATE(confirmation_date) = '$yday' AND c.status=2 AND d.status=1 ) as claimyday ,"
                . "(SELECT COUNT( code )
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) WHERE d.dealer_id =$dealerId AND c.status=2 AND d.status=1) as claimtotal";
        $report = Yii::app()->db->createCommand($query)->queryAll();
        return isset($report) ? $report : array();
    }
    
    public static function getfreegiftSearchReport($data) { 
        $dealerId = $data['dealer_id'];
        $startDate = $data['start_date'];
        $endDate = $data['end_date'];
        $offer = $data['offers'];
        $cond = "1";
        if(!empty($offer)){
            $cond = " of.id= $offer ";
        }
        $query = "SELECT DATE(c.confirmation_date) as date,of.name as offer,sum(case WHEN c.status=1 THEN 1 ELSE 0 END ) as grabbed,sum(case WHEN c.status=2 THEN 1 ELSE 0 END ) as redeem,sum(case WHEN c.status=3 THEN 1 ELSE 0 END ) as expired,count(1) as total
                    FROM coupons AS c
                    INNER JOIN deals AS d ON ( d.id = c.`deal_id` ) 
                    INNER JOIN offers as of ON (of.deal_id=d.id) WHERE d.dealer_id =$dealerId AND d.status=1 AND of.status=1 AND of.sequence=1 AND DATE(c.confirmation_date) BETWEEN '$startDate' AND '$endDate'
                     AND $cond
                    group by 1 order by 1 desc";
        $report = Yii::app()->db->createCommand($query)->queryAll();
        return isset($report) ? $report : array();
    }
    
    public static function getAlloffers($dealerId) { 
        $query = "SELECT of.name as offer_name,of.id as offerid,d.img_url
                FROM offers as of INNER JOIN deals as d ON (of.deal_id=d.id)
                WHERE d.dealer_id = $dealerId AND of.sequence=1 AND of.status=1 AND d.status=1 ";
         $report = Yii::app()->db->createCommand($query)->queryAll();
        return isset($report) ? $report : array();
    }
    
    public static function getAllDealer($role=3){
        $query = "SELECT us.user_name,us.mobile_number,us.dealer_name as user_name,us.id as id "
                . " FROM users as us "
                . " WHERE us.role=$role";
        $dealsList = Yii::app()->db->createCommand($query)->queryAll();
        return (isset($dealsList) && count($dealsList)>0) ? $dealsList : array();
    }
    
}
