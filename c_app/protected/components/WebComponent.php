<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class WebComponent {
    
    public static function setWebSectionDetails($data) {
       // image_url  image_flag  big_text  small_text  img_link   created
        
        $result = array();
        try {
                $info = new Web_Section();
                $info->image_url = $data['img_url'];
                $info->image_flag = $data['select_image_page'];
                $info->image_sequence = $data['select_image_sequence'];
                $info->gift_type = $data['select_gift_types'];
                $info->category_type = $data['select_category_types'];
                $info->big_text = $data['home_page_img_big_text'];
                $info->small_text = $data['home_page_img_small_text'];
                $info->img_link  = $data['home_page_img_link'];
                $info->created = $data['date'];
                if(!$info->save()){
                    $errror = $info->getErrors();
                    throw new Exception("Info creation error :".json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Info updated successfully .','model'=>$info);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
       
    }
    
    
    // page_url  page_tittle  page_meta_description  page_meta_tag  created
    
    public static function setWebMetaTagDetails($data) {
        $result = array();
        try {
                $info = new Web_Metatag_Details();
                $info->page_url = $data['page_url'];
                $info->page_tittle = $data['page_tittle'];
                $info->page_meta_description = $data['page_meta_description'];
                $info->page_meta_tag = $data['page_meta_tag'];
                $info->created = $data['date'];
                if(!$info->save()){
                    $errror = $info->getErrors();
                    throw new Exception("Info creation error :".json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Info updated successfully .','model'=>$info);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
       
    }
    
    
    // job_title  job_desc created
    
    public static function setHRpanelDetails($data) {
        $result = array();
        try {
                $info = new HR_panel();
                $info->job_title = $data['job_tittle'];
                $info->job_desc = $data['job_description'];
                $info->created = $data['date'];
                if(!$info->save()){
                    $errror = $info->getErrors();
                    throw new Exception("Info creation error :".json_encode($errror),422); 
                }
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Candidate Job Details are inserted successfully .','model'=>$info);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
       
    }
    //check IP is internal or not for loging to b2c panel
    public static function isInternalIP(){        
        return in_array(CHttpRequest::getUserHostAddress(), Yii::app()->params['Internal_IP']);
    }
    
    
}//End of WebComponent 