<?php

class TransactionComponent {
    
    public static function RechargeTransaction($connection, $data_rev){
        $connection->createCommand("INSERT INTO recharge_transaction "
                . "(`transaction_id`,`" . $data_rev['recharge_column'] . "`,`amount`,`operator_id`,`recharge_type`,`device_type`,`type`,`profile_id`,`status`,`recharge_flag`,`datetime`) "
                . "values('" . $data_rev['transaction_id'] . "','" . $data_rev[$data_rev['recharge_column']] . "','" . $data_rev['amount'] . "','" . $data_rev['operator_id'] . "','"
                . "" . $data_rev['recharge_type'] . "','" . $data_rev['device_type'] . "','" . $data_rev['type'] . "',"
                . "'" . $data_rev['profile_id'] . "','','" . $data_rev['flag']. "','" . GeneralComponent::getFormatedDate() . "')")->execute();
    }
    
    public static function BillingTransaction($connection, $data_rev){
        $data_rev['additional_detail1'] = isset($data_rev['additional_detail1']) ? $data_rev['additional_detail1'] : "";

        $connection->createCommand("INSERT INTO `billing_transaction` (`transaction_id`, `mobile_number`, `subscriber_id`, `amount`, `service_charge`, `service_tax`, `operator_id`,`additional_detail1`, `payment_flag`,`datetime`)"
        . " VALUES ('" . $data_rev['transaction_id'] . "', '" . $data_rev['mobile_number'] . "', '" . $data_rev['subscriber_id'] . "', '" . $data_rev['base_amount'] . "',"
        . " '" . $data_rev['service_charge'] . "','" . $data_rev['service_tax'] . "','" . $data_rev['operator_id'] . "', "
                . "'" . $data_rev['additional_detail1'] . "', '" . $data_rev['payment_flag']. "', '" . GeneralComponent::getFormatedDate() . "')")->execute();
    }

    public static function DealTransaction($connection, $data_rev){
        $expiryDate = date('Y-m-d',strtotime("+30 days"));
        $result = $connection->createCommand("SELECT id,dealer_id,coupon_type,coupon_prefix FROM deals WHERE id='".$data_rev['deal_id']."'")->queryAll();
        $couponType = $result[0]['coupon_type'];
        $offer = $connection->createCommand("SELECT (CASE WHEN $couponType!=0 THEN validity ELSE '$expiryDate' END) as 'expiry_date' FROM offers WHERE sequence=1 AND deal_id='".$data_rev['deal_id']."'")->queryRow();
        $data_rev['deal_detail']=$result[0];        
        $coupon_code = isset($offer['expiry_date']) ? self::Generate_DealCoupon($connection, $data_rev) : "";
        $data_rev['coupon_id'] = $connection->lastInsertID;
        $data_rev['expiry_date'] = $offer['expiry_date'];
        $column = ""; $colvals ="";
        if(isset($data_rev['ref_trans_id'])){
            $cols_list = array('transaction_id','users_id','deal_id','offer_id','coupon_id','quantity','amount','expiry_date','ref_trans_id');
        
        }else{
            $cols_list = array('transaction_id','users_id','deal_id','offer_id','coupon_id','quantity','amount','expiry_date');
        }
        foreach ($cols_list as $cols){
            $column .= (strlen($column) >0)?",`".$cols."`":"`".$cols."`";
            $colvals .= (strlen($colvals) >0)?",'".$data_rev[$cols]."'":"'".$data_rev[$cols]."'";
        }
        $connection->createCommand("INSERT INTO `deal_transaction` ($column) VALUES($colvals)")->execute();
        return $coupon_code;
    }
    
    public static function GenerateVoucher($connection, $data_rev){
        $column = ""; $colvals ="";
        $voucherDetails = GeneralComponent::getGiftXoxoVoucher($data_rev);
        try{
            $vouchercode = array();
            $v_pin = array();
            if($voucherDetails->message=='Success'){
                 $data_rev['createdon'] = GeneralComponent::getFormatedDate();
                $data_rev['confirmation_date'] = GeneralComponent::getFormatedDate();
                $data_rev['status'] = 1;

                foreach($voucherDetails->result as $each){
                    /* This will give you each result of each voucher*/
                    $data_rev['expiry_date'] = $each->validity_date;
                    $data_rev['amount'] = $each->amount;
                    $data_rev['code'] = $each->code;
                    $vouchercode[]=$data_rev['code'];
                    $data_rev['pin'] = $each->pin;
                    $v_pin[]=$data_rev['pin'];
                    $cols_list = array('users_id','deal_id','offer_id','createdon','confirmation_date','code','status','pin');
                    foreach ($cols_list as $cols){
                        $column .= (strlen($column) >0)?",`".$cols."`":"`".$cols."`";
                        $colvals .= (strlen($colvals) >0)?",'".$data_rev[$cols]."'":"'".$data_rev[$cols]."'";
                    }
                    $connection->createCommand("INSERT INTO `coupons` ($column) VALUES($colvals)")->execute();
                    $cols_list = array();
                    $column = "";
                    $colvals = "";
                    $data_rev['coupon_id'][] = $connection->lastInsertID;
                }
                //store voucher code in coupon table and deal_transaction
                if(isset($data_rev['ref_trans_id'])){
                    $cols_list = array('transaction_id','users_id','deal_id','offer_id','coupon_id','quantity','amount','expiry_date','ref_trans_id');

                }else{
                    $cols_list = array('transaction_id','users_id','deal_id','offer_id','coupon_id','quantity','amount','expiry_date');
                }
                foreach ($cols_list as $cols){
                    $column .= (strlen($column) >0)?",`".$cols."`":"`".$cols."`";
                    $colvals .= (strlen($colvals) >0)?",'".$data_rev[$cols]."'":"'".$data_rev[$cols]."'";
                }
                $query = "INSERT INTO `deal_transaction` (transaction_id,users_id,deal_id,offer_id,coupon_id,quantity,amount,expiry_date,ref_trans_id) ";
                $query .= "VALUES";
                $i=0;
                foreach ($data_rev['coupon_id'] as $row){
                    if($i>0){
                        $query .= ",";
                    }
                    $transaction_id = $data_rev['transaction_id'];
                    $users_id = $data_rev['users_id'];
                    $deal_id = $data_rev['deal_id'];
                    $offer_id = $data_rev['offer_id'];
                    $quantity = 1;//$data_rev['quantity'];
                    $amount = $data_rev['amount'];
                    $expiry_date = $data_rev['expiry_date'];
                    $ref_trans_id = $data_rev['ref_trans_id'];
                    $query .= "($transaction_id,$users_id,$deal_id,$offer_id,$row,$quantity,$amount,'$expiry_date',$ref_trans_id)";
                    $i++;
                }
                $connection->createCommand($query)->execute();
                $data = array("code"=> json_encode($vouchercode),"pin"=>  json_encode($v_pin),"validity_date"=>$data_rev['expiry_date'],"amount"=>$data_rev['amount']);
                return $result = array('status'=>'success','errCode'=>'0','description'=>$data);
            }else{
                return $result = array('status'=>'failure','errCode'=>'272','description'=>$voucherDetails->message);
            }
        }
        catch (Exception $ex) {
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
         }
    }


    public static function LoyaltyTransaction($connection, $data){
        if(isset($data['api_version']) && $data['api_version']==3 && ($data['trans_category']!='deal' || (isset($data['by_voucher']) && !empty($data['by_voucher']) ) ) && $data['trans_category']!='refill'){
            if(isset($data['by_voucher']) && !empty($data['by_voucher'])){
                $data['flag']=6;
            }
                $data['userid'] =isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
                $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
                $servicePoints = "";
                $trans_id = $data['trans_id'];
                if(isset($data['flag']) && isset($Loyalty_points[$data['flag']])){
                    $servicePoints = $Loyalty_points[$data['flag']];
                    $trans_qry = "SELECT trans_id,transaction_amount,users_id FROM transactions WHERE trans_id = '$trans_id'";
                    $transdata = $connection->createCommand($trans_qry)->queryAll();
                    $txn_id = $transdata[0]['trans_id']; //------ transaction id
                    $amount = $transdata[0]['transaction_amount']; //------ transaction amount //$data['amount'];
                   // $wallet_qry = "SELECT * FROM wallets WHERE users_id =" . $transdata[0]['users_id'] . " FOR UPDATE";
                   // $wallet_data = $connection->createCommand($wallet_qry)->queryAll();
                    $query="UPDATE wallets as w 
                               LEFT JOIN 
                               `transactions` as t ON (w.users_id = t.users_id )
                               SET w.loyalty_points = CEIL(w.loyalty_points + ($amount*$servicePoints))
                               , t.loyalty_points = CEIL($amount*$servicePoints)
                                ,t.free_points=1
                               WHERE t.trans_id=$trans_id AND t.free_points != 1";
                    //file_put_contents("/tmp/get_LoyaltyPoints", "|  : deallocation : ".$query."\n", FILE_APPEND | LOCK_EX);
                    try{
                    $connection->createCommand($query)->execute();
                    }
                     catch (Exception $ex) {
                         return 0;
                    }
                    return $amount*$servicePoints;
                }
            return 0;
        }
        return 0;
    }

        public static function Generate_DealCoupon($connection, $data_rev){
        $column = ""; $colvals ="";
        $data_rev['createdon'] = GeneralComponent::getFormatedDate(); 
        $data_rev['confirmation_date'] = GeneralComponent::getFormatedDate(); 
        $data_rev['status'] = 1;
        $code_prefix = trim($data_rev['deal_detail']['coupon_prefix']);
        $couponType = $data_rev['deal_detail']['coupon_type'];
        $dealer_id = $data_rev['deal_detail']['dealer_id'];
        $dealId = isset($data_rev['deal_id']) ? $data_rev['deal_id'] : 0;
        $prefix = ($code_prefix === "NULL")?"":$code_prefix;
        $i = 0;
        while($i<1){
            try{
                $suffix = GeneralComponent::generate_unique_number(8 -(strlen($prefix)),TRUE);
                switch ($couponType){
                    case 0 :
                        $code =  $suffix;
                        break;
                    case 1:
                       $code =  ($prefix).$suffix;//strtoupper($prefix).$suffix
                        break;
                    case 2 :
                        $code =  ($prefix);
                        break;
                    case 3 :
                        $dealerCoupon_Q = "SELECT id,code FROM dealerCoupon WHERE dealer_id=$dealer_id AND deal_id = "."'$dealId'"."  AND is_used=0 FOR UPDATE";
                        $dealerCoupon = $connection->createCommand($dealerCoupon_Q)->queryRow();
                         $code =  "";
                        if(count($dealerCoupon)>0 && isset($dealerCoupon['code'])){
                            $codeId = $dealerCoupon['id'];
                             $code =  $dealerCoupon['code'];
                             $connection->createCommand("UPDATE dealerCoupon SET is_used=1 WHERE id=$codeId AND is_used=0 LIMIT 1")->execute();
                        }
                        else{
                             throw new Exception("coupon not exist for this offer",226);
                        }
                        break;
                }
                $data_rev['code'] = $code;
                $cols_list = array('users_id','deal_id','offer_id','createdon','confirmation_date','code','status');
                foreach ($cols_list as $cols){
                    $column .= (strlen($column) >0)?",`".$cols."`":"`".$cols."`";
                    $colvals .= (strlen($colvals) >0)?",'".$data_rev[$cols]."'":"'".$data_rev[$cols]."'";
                }
                $connection->createCommand("INSERT INTO `coupons` ($column) VALUES($colvals)")->execute();
                $i++;
            } catch (Exception $ex) {
                $i = 0;
            }        
        }
        return $data_rev['code'];
    }

    public static function newTransaction($connection, $data_rev){
        $tablename = "transactions";
        $data_rev['trans_date'] = GeneralComponent::getFormatedDate('Y-m-d');
        $data_rev['ip_address'] = CHttpRequest::getUserHostAddress();
        $data_rev['update_datetime'] = GeneralComponent::getFormatedDate();
        if(!isset($data_rev['trans_datetime'])){
            $data_rev['trans_datetime'] = GeneralComponent::getFormatedDate();
        }
        self::addData($connection, $tablename, $data_rev);        
    }
    
    public static function newRefillTransaction($connection, $data_rev){
        $tablename = "refill_transaction";
        if(!isset($data_rev['datetime'])){
            $data_rev['datetime'] = GeneralComponent::getFormatedDate();
        }
        self::addData($connection, $tablename, $data_rev);
    }
    
    public static function newTempRefillTransaction($connection, $data_rev){
        $tablename = "temp_wallets";
        self::addData($connection, $tablename, $data_rev);
    }

    public static function addData($connection,$tablename, $data_rev){
        $qry_col = implode("`,`",array_keys($data_rev));
        $col_val = implode("','",array_map('mysql_escape_string',array_values($data_rev)));
       // file_put_contents("/tmp/query", "| hash ". "INSERT into $tablename (`".$qry_col."`)  VALUES('".$col_val."')"."\n", FILE_APPEND | LOCK_EX);
        $connection->createCommand("INSERT into $tablename (`".$qry_col."`)  VALUES('".$col_val."')")->execute();
    }
    
    public static function updateTransaction($connection,$trans_id,$data=array()){
        $tablename = "transactions";
        $current_datetime = GeneralComponent::getFormatedDate();
        if(count($data)>0){
            $update_data = "";
            foreach ($data as $col=>$val){
                $update_data .= (strlen($update_data)>0)?", `$col`='$val'":"`$col`='$val'";
            }
            $connection->createCommand("UPDATE $tablename SET $update_data , update_datetime= '".$current_datetime."' WHERE trans_id='$trans_id'")->execute();
        }
    }

    public static function update_payu_Transaction($connection,$trans_id,$data=array()){
        $tablename = "pg_payuIndia";
        if(count($data)>0){
            $update_data = "";
            foreach ($data as $col=>$val){
                $update_data .= (strlen($update_data)>0)?", `$col`='$val'":"`$col`='$val'";
            }
            $connection->createCommand("UPDATE $tablename SET $update_data WHERE txnid='$trans_id'")->execute();
        }
    }
    
    public static function check_trans_by_client_req($data){
        try{
            if(!isset($data['client_req_id']) || !ctype_alnum($data['client_req_id'])){ throw new Exception("Missing parameter",209); }
            $transObj = Transactions::model()->findAllByAttributes(array('response_id'=>$data['client_req_id']));
            if(count($transObj)<1){ throw new Exception("transaction not found",226); }
            $transdata = $transObj[0]->attributes;
            $result = array('status'=>'success','errCode'=>'','description'=>$transdata['trans_id']);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($result);
    }
    
    public static function getTransactionlog_old($type=null,$mode=null,$page=0,$limit=10){
        try{            
            $offset = ($page > 0)?$page*$limit:$page;
            $condition_param = array('users_id'=>Yii::app()->user->id);
            if(!is_null($type)){
                if(!in_array($type,array('0','1'))){
                    throw new Exception("Invalid transaction type",222);
                }
                $condition_param = array_merge($condition_param, array('trans_type'=>$type));                
            }
            if(!is_null($mode)){
                if(!in_array($mode,array('wallet','online'))){
                    throw new Exception("Invalid transaction mode",223);
                }
                $condition_param = array_merge($condition_param, array('transaction_mode'=>$mode));                
            }
            $transObj = Transactions::model()->findAllByAttributes(
                    $condition_param,
                    array('limit'=>$limit,'offset'=>$offset,'order'=>'trans_datetime desc'));
            $transdetail = array();
            foreach ($transObj as $transaction=>$transval){            
                $transdetail[] = $transval->getAttributes(array('trans_category','trans_datetime','transaction_amount','closing_bal'));
            }
            $result = array('status'=>'success','errCode'=>'','description'=>$transdetail);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return json_encode($result);
    }
    
    public static function getTransactionlog($type=null,$mode=null,$page=0,$limit=10){
        return self::getPurchaselog($page,$limit,$mode);
    }
    
    public static function getPurchaselog($page=0,$limit=10,$mode=NULL,$userId=null,$dtFrom=null,$dtTo=null,$additionCond=array()){
        try{
            $ref_col = 'trans_id';
            $refundstatus = "";
            $cond = "";
            if($mode == "wallet" && $mode != null){
                $refundstatus = ",'4'";
                $ref_col = 'ref_trans_id';
            }
            if($mode != NULL){
                $cond .= " AND (txn.transaction_mode = '".strtolower($mode)."' OR (txn.trans_category = 'refill' AND txn.status = '2'))";
            }            
            $dtCondition = "";
            if(($userId == null) || ($userId === "") ){
               $userId =  Yii::app()->user->id; 
            }else{
                $refundstatus = ",'4','5','7'";
                $ref_col = 'ref_trans_id';                
                
                if(isset($additionCond['txnid']) && $additionCond['txnid'] != ""){
                    $dtCondition .= " AND txn.trans_id = ".$additionCond['txnid'];
                }
                if(isset($additionCond['number']) && $additionCond['number'] != ""){
                    $dtCondition .= " AND (bt.subscriber_id = '".$additionCond['number']."' OR bt.mobile_number = '".$additionCond['number']."' "
                            . "OR rt.subscriber_id = '".$additionCond['number']."' OR rt.mobile_number = '".$additionCond['number']."')";
                }
                if(isset($additionCond['pay1_voucher']) && $additionCond['pay1_voucher'] != ""){
                    $dtCondition .= " AND vchr.voucher_code = '".$additionCond['pay1_voucher']."' AND vchr.status=3 ";
                }
                if(isset($additionCond['deal_coupon_no']) && $additionCond['deal_coupon_no'] != ""){
                    $dtCondition .= " AND cpn.code = '".$additionCond['deal_coupon_no']."' ";
                }                
                if($dtTo != null || $dtTo != ""){                    
                    if($dtFrom == null || $dtFrom == ""){
                        $dtFrom = date("Y-m-d");
                    }
                    $dtCondition .= " AND (txn.trans_date >= '$dtFrom' && txn.trans_date <= '$dtTo')";
                }
            }
            $qry = "SELECT t1.trans_id as transaction_id,t1.trans_type, t1.trans_datetime, t1.transaction_amount, t1.trans_category,t1.status, CONCAT(UCASE(LEFT(t1.transaction_mode, 1)), SUBSTRING(t1.transaction_mode, 2)) as transaction_mode,t1.number,t1.recharge_flag,
                t1.closing_bal,prod.name as operator_name,d.name as deal_name,ofr.name as offer_name,t1.payment_mode,t1.base_amount,t1.ip_address,t1.mihpayid,t1.response_id,t1.product as product_id,
                (CASE WHEN t1.status = 4    
                THEN 0 
                ELSE (CASE WHEN prod.service_id is not NULL
                        THEN prod.service_id
                        ELSE (CASE WHEN t1.trans_category='refill' 
                                THEN 98 
                                ELSE (CASE WHEN t1.trans_category='deal' 
                                            THEN 99 ELSE 0 
                                        END) 
                                END)
                        END)
                END) as service_id
                FROM (
                SELECT txn.trans_id, txn.trans_datetime, txn.transaction_amount,txn.status,txn.transaction_mode,txn.closing_bal,txn.recharge_flag,
                dt.deal_id,dt.offer_id, txn.trans_type,bt.amount as 'base_amount',txn.ip_address,pg1.mihpayid,txn.response_id,
                (CASE  WHEN txn.status = '4' THEN concat(txn.trans_category,' (refund)') ELSE txn.trans_category END) as trans_category,
                (CASE 
                WHEN trans_category='recharge' 
                THEN 
                    (CASE 
                    WHEN rt.recharge_flag=2 
                    THEN rt.subscriber_id  
                    ELSE rt.mobile_number 
                    END) 
                ELSE 
                    (CASE 
                    WHEN trans_category='billing' 
                    THEN 
                        (CASE 
                        WHEN bt.payment_flag=2 
                        THEN bt.subscriber_id 
                        ELSE bt.mobile_number 
                        END) 
                    ELSE 
                        (CASE
                        WHEN trans_category='refill'
                        THEN u.mobile_number
                        ELSE 
                            (CASE
                            WHEN trans_category='deal'
                            THEN 'N.A' 
                            ELSE 'N.A'
                            END) 	 
                        END) 
                    END)
                END) as number,
                (CASE 
                WHEN trans_category='recharge' 
                THEN 
                    (CASE
                    WHEN rt.recharge_flag = 1
                    THEN topr.product_id
                    ELSE
                        (CASE
                        WHEN rt.recharge_flag = 2
                        THEN dopr.product_id
                        ELSE
                            (CASE
                            WHEN rt.recharge_flag = 3
                            THEN topr.data_product_id
                            ELSE 0
                            END)
                        END)
                    END)
                ELSE 
                    (CASE
                    WHEN trans_category='billing' and payment_flag = 1
                    THEN topr1.postpaid
                    ELSE 0
                    END)
                END) as product, 
                (CASE 
                    WHEN pg1.status = 'failure' THEN 
                    (CASE 
                        WHEN pg1.error ='' THEN pg1.field9
                        ELSE pg1.error_Message
                    END)
                    ELSE
                    (CASE 
                        WHEN pg1.mode = 'CC' THEN concat('Credit Card (',pg1.cardnum,')') ELSE
                        (CASE 
                            WHEN pg1.mode = 'DC' THEN concat('Debit Card (',pg1.cardnum,')') ELSE
                            (CASE 
                                WHEN pg1.mode = 'NB' THEN concat('Net Banking ( ',pg1.PG_TYPE,' : ',pg1.bank_ref_num,' )') ELSE  
                                (CASE 
                                    WHEN (pg1.mode = 'WALLET' AND pg1.PG_TYPE = 'PAISA') THEN concat('PAYU ( ',pg1.mode,' )') ELSE  pg1.mode
                                END)
                            END)
                        END)
                    END)
                END) as payment_mode 
                FROM `transactions` as txn 
                    LEFT JOIN billing_transaction as bt ON txn.ref_trans_id=bt.transaction_id 
                    LEFT JOIN recharge_transaction as rt ON txn.ref_trans_id = rt.transaction_id
                    LEFT JOIN deal_transaction as dt ON txn.trans_id = dt.transaction_id
                    LEFT JOIN users as u ON txn.users_id = u.id
                    LEFT JOIN telecom_operators as topr ON rt.operator_id=topr.id 
                    LEFT JOIN dth_operators as dopr ON rt.operator_id=dopr.id
                    LEFT JOIN telecom_operators as topr1 ON bt.operator_id=topr1.id 
                    LEFT JOIN pg_payuIndia as pg1 ON txn.ref_trans_id = pg1.txnid
                    LEFT JOIN voucher as vchr ON txn.ref_trans_id=vchr.txnid 
                    LEFT JOIN `coupons` as cpn ON cpn.id = dt.coupon_id  
                        WHERE txn.status IN ('0','1','2','3' ".$refundstatus.") AND
                        txn.users_id=".$userId." ".$cond." ".$dtCondition." AND txn.transaction_amount > 0 AND 	txn.transaction_mode!='loyaltypoints' AND txn.trans_category NOT IN ('signup','pay1shop')
                        ORDER BY 1 desc
                        LIMIT ".($page*$limit).",". $limit."
                        ) as t1 
                LEFT JOIN products as prod ON prod.id=t1.product  
                LEFT JOIN deals as d ON d.id=t1.deal_id
                LEFT JOIN offers as ofr ON ofr.id=t1.offer_id
                LEFT JOIN services as serv ON serv.id=prod.service_id
                ORDER BY 1 DESC";
            //file_put_contents("/tmp/getPurchaselog", "| noti-array ".$qry."\n", FILE_APPEND | LOCK_EX);
            $data = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$data);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    public static function get_Deal_Transaction_details($trans_id){
        $qry = "SELECT dl.id, dl.transaction_id,dl.users_id,dl.deal_id,dl.offer_id,dl.quantity,dl.amount,d.name as deal_name,ofr.name as offer_name,
                ofr.discount,txn.trans_datetime,txn.transaction_mode,ofr.short_desc,ofr.long_desc,ofr.validity as expiry,d.dealer_id,
                ofr.actual_price,ofr.offer_price,ofr.discount,ofr.total_stock,ofr.stock_sold,cpn.code,
                (CASE WHEN cpn.status = 1 THEN 'UNUSED' ELSE (CASE WHEN cpn.status=2 THEN 'USED' ELSE (CASE WHEN cpn.status=3 THEN 'EXPIRED' ELSE 'UNUSED' END) END) END) as coupon_status,
                (CASE WHEN length(d.img_url) > 0 THEN concat('".Yii::app()->params['GLOBALS']['LIVE_BASE_URL']."',d.img_url) ELSE  d.img_url END) as img_url
                     FROM `deal_transaction` as dl 
                     LEFT JOIN deals d ON d.id = dl.deal_id 
                     LEFT JOIN offers ofr ON ofr.id = dl.offer_id 
                     LEFT JOIN transactions txn ON txn.trans_id = dl.transaction_id
                     LEFT JOIN coupons cpn ON cpn.id = dl.coupon_id
                         WHERE dl.transaction_id='".$trans_id."'";
        
        try{
            $data = Yii::app()->db->createCommand($qry)->queryAll();
        }
        catch (Exception $ex) {
            return null;
        }
        return isset($data[0])?$data[0]:null;
        
    }
    
    
    //$searchBy => "txn_id" , "user_id"
    public static function getComplaintDetails($id,$searchBy="txn_id"){
        try{
            $qry = "Select complains.id,complains.users_id,complains.transaction_id, complains.complain_msg, complains.response_txt,complains.created,complains.status,
                        CASE complains.status
                        WHEN 1 THEN 'mobile'
                        WHEN 2 THEN 'dth'
                        WHEN 3 THEN 'datacard'
                        WHEN 4 THEN 'postpaid'
                        ELSE recharge_transaction.recharge_flag
                        END as status_name
                    from complains";
            $data = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$data);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    //get all comments related to user_id or txn_id
    public static function getComments($id,$searchBy="txn_id"){
        try{
            if($searchBy=="user_id"){
                $condition = "comments.users_id = $id";
            }else if($searchBy=="txn_id"){
                $condition = "comments.ref_code = $id";
            }
            
            $qry = "SELECT comments.id,   comments.users_id,        comments.ref_code, comments.flag,        comments.comments,        comments.mobile,        comments.created 
                    FROM comments
                    WHERE $condition ";
            $data = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$data);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    //$searchBy => "txn_id" , "mobile_no" ,"dth_no"
    //$txnType  => "recharge", "entertainment", "transfer", "bus", "bus"
    public static function getFullTxnDetails($id,$txnType,$searchBy="txn_id",$dtFr=null,$dtTo=null){
        
        if( $txnType == "recharge"){
            $txnTypeColumns = "recharge_transaction.recharge_id,recharge_transaction.transaction_id,recharge_transaction.mobile_number,recharge_transaction.subscriber_id,recharge_transaction.amount,recharge_transaction.datetime,recharge_transaction.operator_id,recharge_transaction.circle_id,recharge_transaction.recharge_type,
                               (
                                    CASE recharge_transaction.recharge_flag
                                    WHEN 1 THEN 'mobile'
                                    WHEN 2 THEN 'dth'
                                    WHEN 3 THEN 'datacard'
                                    WHEN 4 THEN 'postpaid'
                                    ELSE recharge_transaction.recharge_flag
                                    END
                               ) as recharge_type_name,
                               recharge_transaction.uuid,recharge_transaction.device_type,recharge_transaction.type,recharge_transaction.profile_id,recharge_transaction.status,recharge_transaction.recharge_flag,";
        }else if($txnType == "entertainment"){
            $txnTypeColumns = "";
        }else if($txnType == "transfer"){
            $txnTypeColumns = "transfer_transaction.id,transfer_transaction.transaction_id,transfer_transaction.datetime,transfer_transaction.beneficiary_type,transfer_transaction.beneficiary_userid,transfer_transaction.beneficiary_account_no,transfer_transaction.bank_detail,transfer_transaction.response_detail,transfer_transaction.device_type,transfer_transaction.type,transfer_transaction.profile_id,";
        }else if($txnType == "bus"){
            $txnTypeColumns = "";
        }else if($txnType == "bus"){
            $txnTypeColumns = "refill_transaction.id,refill_transaction.transaction_id,refill_transaction.datetime,refill_transaction.retailer_name,refill_transaction.retailer_mobile,refill_transaction.retailer_id,refill_transaction.bank_detail,refill_transaction.response_detail,refill_transaction.account_number,refill_transaction.additional_detail1,refill_transaction.additional_detail2,";
        }else if("txn_id"==$searchBy){
            $result = array('status'=>'failure','errCode'=>"0",'description'=>"Invalid Input ( Transaction Type ).");
            return $result;
        }
        
        if($searchBy == "mobile_no"){
             $where = $txnType."_transaction.mobile_number = $id";
        }else if($searchBy == "dth_no"){
             $where = $txnType."_transaction.subscriber_id = $id";
        }else if($searchBy == "txn_id"){
             $where = $txnType."_transaction.$txnType"."_id = $id";
        }else{
             //$result = array('status'=>'failure','errCode'=>"0",'description'=>"Invalid Input ( Search Type ).");
             //return $result;
            $where = "transactions.id"." = $id";
        }
        
        $dtWhere = "";
        if(empty ($dtFr) || empty ($dtTo)){
            $dtWhere = ' AND transactions.trans_date >= $dtFr AND transactions.trans_date <= $dtTo';
        }
        try{
            $qry = "SELECT
                        $txnTypeColumns
                        
                        transactions.trans_id,transactions.trans_datetime,transactions.trans_category,transactions.users_id,transactions.track_id,transactions.transaction_mode ,
                        (
                            CASE transactions.trans_type
                            WHEN 0 THEN 'debit'
                            WHEN 1 THEN 'credit'
                            ELSE transactions.trans_type
                            END
                        ) as trans_type_name,
                        transactions.opening_bal,transactions.transaction_amount,transactions.closing_bal,transactions.response_id,transactions.response,transactions.updated_response,
                        (
            0=received,1=inprocess,2=complete,3=failed,4=refunded,5=pulledback,6=refilled
                            CASE transactions.status
                            WHEN 0 THEN 'received'
                            WHEN 1 THEN 'inprocess'
                            WHEN 2 THEN 'complete'
                            WHEN 3 THEN 'failed'
                            WHEN 4 THEN 'refunded'
                            WHEN 5 THEN 'pulledback'
                            WHEN 6 THEN 'refilled'
                            ELSE transactions.status
                            END
                        ) as status_name,                        
                        
                        users.user_name,users.mobile_number,users.password,users.salt,users.created_datetime,users.role,
                        
                        user_role.role_name,
            
                        user_profile.id as 'user_profile_id',user_profile.name,user_profile.mobile,user_profile.email,user_profile.gender,user_profile.date_of_birth,user_profile.user_id,user_profile.gcm_reg_id,user_profile.uuid,user_profile.longitude,user_profile.latitude,user_profile.location_src,user_profile.manufacturer,user_profile.os_info,user_profile.device_type,user_profile.created as 'user_profile_created',
                        
                        wallets.wallet_id as 'wallet_user_id',wallets.account_balance
            
                    FROM $txnType"."_transaction
                    LEFT JOIN transactions ON recharge_transaction.transaction_id = transactions.trans_id
                    LEFT JOIN users ON transactions.users_id = users.id
                    LEFT JOIN user_role ON user_role.id = users.role
                    LEFT JOIN `user_profile` ON `users`.`id` = `user_profile`.`user_id`
                    LEFT JOIN wallets ON wallets.users_id = users.id
                    WHERE
                    $where 
                    $dtWhere";
            
             
            $data = Yii::app()->db->createCommand($qry)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$data);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    public static function loyaltyPointslog($data){
        $userid = Yii::app()->user->id;
        $user  = Yii::app()->user->username;
        $next = isset($data['next']) ? $data['next']: 0;
        $connection = Yii::app()->db;

        $start = 0;
        $end =  isset($data['displayLimit']) ? $data['displayLimit']: 10;
        $limitcond = "";
       if(!empty($next) && $next!=-1){
            $start = ($next * 10);
        }

        $limitcond =  "LIMIT $start,$end ";

        if($next==-1){
             $limitcond = "";
        }
       $query = "SELECT prod1.trans_id,prod1.trans_category,(CASE WHEN prod.name IS NULL THEN '' ELSE prod.name END) as operator, prod1.Name, prod1.rechargeType, prod1.transaction_amount, ABS(prod1.coins) as coins, prod1.trans_datetime, prod1.msg,prod1.redeem_status
	FROM	
	(SELECT trans_category,(CASE WHEN trans_category =  'recharge'
		        THEN (CASE WHEN rt.mobile_number!=0
		            THEN rt.mobile_number
		            ELSE rt.subscriber_id
		            END ) 
                        WHEN t.trans_category =  'pay1shop'
                        THEN $user
		        WHEN t.trans_category =  'deal'
		        THEN (SELECT name FROM offers WHERE id= (SELECT offer_id FROM deal_transaction WHERE transaction_id = t.trans_id OR transaction_id = t.ref_trans_id))
		        WHEN t.trans_category =  'billing'
		        THEN bt.mobile_number
		        ELSE ''
		        END ) AS Name,
		        (CASE WHEN trans_category =  'recharge' THEN 
		            (CASE 
		                   WHEN rt.recharge_flag=1 THEN 'Mobile Recharge'
		                   WHEN rt.recharge_flag=2 THEN 'DTH Recharge'
		                   WHEN rt.recharge_flag=3 THEN 'Datacard Recharge'
		                   WHEN rt.recharge_flag=4 THEN 'Postpaid Recharge'
		            END ) 
		                WHEN trans_category =  'billing' THEN 'Bill payment'
		                WHEN  trans_category =  'deal' THEN 'Deal'
                                WHEN  trans_category =  'pay1shop' THEN 'pay1shop'
		                ELSE ''
                            END ) as rechargeType,transaction_amount,loyalty_points as coins,trans_datetime,
		        (CASE WHEN t.trans_category='signup' THEN 'Sign up Bonus' 
		              WHEN t.status =4 THEN 'Reversed due to recharge failure' ELSE '' END ) as msg,
		        (CASE WHEN t.trans_category =  'deal' AND transaction_mode='loyaltypoints' AND t.status=2 THEN '1' 
		              WHEN t.trans_category =  'deal' AND t.status=4 THEN '2'
                              WHEN t.trans_category =  'deal' AND transaction_mode='wallet' AND t.status=2 THEN '3'
		              WHEN (t.trans_category =  'recharge'  OR t.trans_category =  'billing' OR t.trans_category =  'pay1shop') AND (t.status=1 OR t.status=2 OR t.status=3) THEN '3'
		              WHEN  (t.trans_category =  'recharge'  OR t.trans_category =  'billing' OR t.trans_category =  'pay1shop')  AND (t.status>2) THEN '4'
		               WHEN t.trans_category =  'signup' THEN '5'
		              END ) as redeem_status,
			(CASE WHEN trans_category='recharge' THEN
				(CASE WHEN (rt.recharge_flag=1 OR rt.recharge_flag=3)
				THEN 
				topr.product_id
				ELSE 
				dopr.product_id END) 
		WHEN trans_category='billing' THEN 
		topr1.product_id ELSE '' END) as product,t.trans_id
		FROM transactions as t LEFT JOIN recharge_transaction as rt ON t.ref_trans_id = rt.transaction_id
		LEFT JOIN billing_transaction as bt ON t.ref_trans_id = bt.transaction_id
		LEFT JOIN deal_transaction as dt ON t.ref_trans_id = dt.transaction_id
		LEFT JOIN users as u ON t.users_id = u.id
		LEFT JOIN telecom_operators as topr ON rt.operator_id=topr.id 
		LEFT JOIN dth_operators as dopr ON rt.operator_id=dopr.id
		LEFT JOIN telecom_operators as topr1 ON bt.operator_id=topr1.id 
		WHERE t.users_id=$userid AND t.trans_category !=  'refill' AND t.status IN (1,2,3,4) AND t.loyalty_points!=0
		AND t.transaction_amount>0) as prod1
            LEFT JOIN products as prod ON prod.id=prod1.product "
               . " order by prod1.trans_datetime desc ";
        try{
            $result= $connection->createCommand($query)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$result);
        }
        catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    public static function resetLoyaltyPoints ($data){
        TransactionComponent::updateTransaction($connection, $data['transaction_id'], array('status' => 3,'free_points'=>-1));
        $query="UPDATE wallets SET loyalty_points = loyalty_points-".$LoyaltyData['coins']." WHERE users_id = ".$data['userid'];
        $connection->createCommand($query)->execute();
        if(Yii::app()->cache->hexists('Loyalty_'.$data['transaction_id'],$data['transaction_id']."_1")===true){
            Yii::app()->cache->hdel('Loyalty_'.$data['transaction_id'], $data['transaction_id']."_1");
        }
    }
    public static function  setLoyaltyAmount($data){
        if(isset($data['api_version']) && $data['api_version']==3){
            $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
            if(isset($data['flag']) && isset($Loyalty_points[$data['flag']])){
                $servicePoints = $Loyalty_points[$data['flag']];
                $coins =$servicePoints*$data['amount'];
                $Coindata = array('coins'=>$coins,'userid'=>$data['userid'],'transaction_id'=>$data['transaction_id'],'api_version'=>$data['api_version']);
                 Yii::app()->cache->hset('Loyalty_'.$data['transaction_id'],$data['transaction_id']."_1", json_encode($Coindata));
            }
        }
    }
    
    public static function transaction_details_web($data){
        $connection = Yii::app()->db;
        $txn_id = (float)$data['id'];
        $query = "SELECT trans_category,update_datetime,transaction_mode,transaction_amount FROM `transactions` WHERE trans_id= $txn_id";
        try{ 
            $result =  $connection->createCommand($query)->queryAll();
            $result = array('status'=>'success','errCode'=>'0','description'=>$result);
        } catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$message);
        }
        return $result;
    }
    
    public function set_TransactionInprocess($data){
        //file_put_contents("/tmp/Transaction", " asffas |".  json_encode($data)." \n", FILE_APPEND | LOCK_EX);
        try{
            if(isset($data['trans_id']) && !empty($data['trans_id'])){
                $trans_id = $data['trans_id'];
                $connection = Yii::app()->db;
                $query = "SELECT * FROM  `transactions` WHERE  `ref_trans_id` =$trans_id ";
                //file_put_contents("/tmp/Transaction", " query |".  $query." \n", FILE_APPEND | LOCK_EX);
                $result= $connection->createCommand($query)->queryAll();
                //file_put_contents("/tmp/Transaction", " query |". json_encode($result)." count ".  count($result)." \n", FILE_APPEND | LOCK_EX);
                if(count($result)==0){
                    return array('status'=>'failed','errCode'=>'0','description'=>"There is no transaction with this id");
                }
                else if(count($result)>1){
                    return array('status'=>'failed','errCode'=>'0','description'=>"Please contact admin");
                }else if($result[0]['status']==1){
                    return array('status'=>'failed','errCode'=>'0','description'=>"This transaction is in process state");
                }
                else if($result[0]['status']==2){
                    return array('status'=>'failed','errCode'=>'0','description'=>"This transaction is already complted");
                }
                else if($result[0]['status']==3){
                    $query= "UPDATE  `b2c_app1`.`transactions` SET  `status` =  '1' WHERE  `transactions`.`trans_id` =$trans_id";
                    $connection->createCommand($query)->execute();
                    return array('status'=>'success','errCode'=>'0','description'=>"Record updated successfully");
                }

            }
        
        }
        catch(Exception $ex){
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            return $result = array('status'=>'failed','errCode'=>0,'description'=>$message);
        }
        
    }
}