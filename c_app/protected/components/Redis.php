<?php

Class Redis
{
	var $default_server_config = array(
        "scheme" => "tcp",
        "host" => "127.0.0.1",
        "port" => 6300,
        "password"=>'$avail$p@y!'
 	);
	
	public static function connect()
	{
        Yii::setPathOfAlias('Predis',Yii::getPathOfAlias('application.vendors.Predis'));
		require_once('Autoloader.php');
		Predis\Autoloader::register();
		return Predis\Client($this->default_server_config);	
	}
		
}