<?php

class WalletComponent {

    public $dbconn;
    public $connection;

    public static function getWalletDetail($id = null) {
        $userid = ($id == null) ? Yii::app()->user->id : $id;
        $data = array();
        try{
            $result = Yii::app()->db->createCommand("SELECT * FROM wallets WHERE users_id ='" . $userid . "'")->queryAll();
            if($result){
                $data = $result[0];
            }
        }
        catch(Exception $ex){
            $data = array();
        }
        return $data;
    }
    
    public static function getTrans_WalletDetail($data = null) {
        try{
            $userid = empty($data['id']) ? Yii::app()->user->id : $data['id'];
            if(isset($data['voucher'])){
                return self::Getuserpoints($data);
            }
            $Loyalty_coin = 0;
            $status = 0;
            $deal = array();
            $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
            $result = Yii::app()->db->createCommand("SELECT (CASE WHEN status=2 THEN 'success' ELSE 'fail' END) as 'status',transaction_amount,trans_category,loyalty_points,free_points FROM transactions WHERE ref_trans_id  ='" . $data['txnid'] . "' order by trans_id desc LIMIT 0,1")->queryAll();
            if(count($result)>0 && $result[0]['free_points']==1){
                $Loyalty_coin = $result[0]['loyalty_points'];
                $status = $result[0]['status'];
            }

            $result1 = Yii::app()->db->createCommand("SELECT *,$Loyalty_coin as 'points' FROM wallets WHERE users_id ='" . $userid . "'")->queryAll();
                if(count($result1) >0){
                     $data = $result1[0];
                   return array('status'=>'success','errCode'=>'0','description'=>$data);
                }
                else{
                    return array('status'=>'error','errCode'=>'0','description'=>'No User Details');
                }
        } catch(Exception $ex){
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            return array('status'=>'error','errCode'=>'0','description'=>$message);
        }
    }

    public static function PaymentTranction($data_rev = null) {
        $data_rev = is_null($data_rev) ? $_REQUEST : $data_rev;
        $data_rev['stv'] = isset($data_rev['stv']) ? $data_rev['stv'] : 0;
        $data_rev['operator_id'] = isset($data_rev['operator_id']) ? $data_rev['operator_id'] : $data_rev['operator'];
        $data_rev['profile_id'] = isset($data_rev['profile_id']) ? $data_rev['profile_id'] : uniqid();
        $data_rev['recharge_type'] = isset($data_rev['recharge_type']) ? $data_rev['recharge_type'] : 0;
        $data_rev['device_type'] = isset($data_rev['device_type']) ? $data_rev['device_type'] : (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] :"") ;
        $data_rev['type'] = isset($data_rev['type']) ? $data_rev['type'] : "";
        $data_rev['mobile_number'] = isset($data_rev['mobile_number']) ? $data_rev['mobile_number'] : "";
        $data_rev['subscriber_id'] = isset($data_rev['subscriber_id']) ? $data_rev['subscriber_id'] : "";
        $recharge_flag = isset($data_rev['flag']) ? $data_rev['flag'] : "";
        if(empty($data_rev['mobile_number']) && empty($data_rev['subscriber_id'])){
          return  $result = json_encode(array('status' => 'failure', 'errCode' =>314, 'description' => 'missing either mobile number or subscriber id'));
        }
        
        if(isset($data_rev['amount']) && (empty($data_rev['amount']) || $data_rev['amount']<0)){
          return  $result = json_encode(array('status' => 'failure', 'errCode' =>314, 'description' => 'Please enter valid amount'));
        }
        
        switch ($recharge_flag) {
            case 1:
                $data_rev['product'] = "MOBILE";
                $data_rev['recharge_column'] = "mobile_number";
                $data_rev['number'] = $data_rev['mobile_number'];
                break;
            case 2:
                $data_rev['product'] = "DTH";
                $data_rev['recharge_column'] = "subscriber_id";
                $data_rev['number'] = $data_rev['subscriber_id'];
                $v_result = self::dthValidation($data_rev);
                if($v_result['status']=="failure"){
                    return json_encode($v_result);
                }
                break;
            case 3:
                $data_rev['product'] = "DATA";
                $data_rev['recharge_column'] = "mobile_number";
                $data_rev['number'] = $data_rev['mobile_number'];
                break;
            case 4:
                $data_rev['product'] = "POSTPAID";
                $data_rev['recharge_column'] = "mobile_number";
                $data_rev['number'] = $data_rev['mobile_number'];
                $data_rev['s_amount'] = $data_rev['service_charge'];
                break;
            case 5:
                $data_rev['product'] = "BILLING";
                $data_rev['recharge_column'] = "mobile_number";
                $data_rev['number'] = $data_rev['mobile_number'];
                $data_rev['s_amount'] = $data_rev['service_charge'];
                break;
        }
        //$add_to_quick_pay = ServiceComponent::addQuickpay($data_rev);
        //$add_to_quick_pay = ServiceComponent::is_AlreadyQuickpay($data_rev)?"":ServiceComponent::addQuickpay($data_rev);
        $data_rev['quick_pay_flag'] = TRUE;
        $connection = Yii::app()->db;
        $user_id = isset($data_rev['userid']) ? $data_rev['userid'] : Yii::app()->user->id;
       
        Yii::app()->user->id = $user_id;
        if(UsersComponent::isBlackListed($user_id)){
            $fraud_response = array('status'=>'failure','errCode'=>900,'description'=>'Your PAY1 account is blocked. Please call on 022 67242266 to re-activate it.');
            return json_encode($fraud_response);
        }
        if(strtolower($data_rev['paymentopt'])==="online"){
            $data_rev['userid'] = Yii::app()->user->id;
            $data_rev['users_id'] = $data_rev['userid'];
            $data_rev['trans_category'] = isset($data_rev['trans_category'])?$data_rev['trans_category']:self::getTransactionCategory($data_rev);
            return json_encode(OnlineComponent::OnlinePaymentProcessor($data_rev));
        }else{
            $data_rev['paymentopt'] = 'wallet';
            return self::Processpayments($data_rev);
        }       
    }

    public static function getTransactionCategory($data_rev) {
        if (isset($data_rev['recharge'])) {
            return "recharge";
        } elseif (isset($data_rev['billpayment'])) {
            return "billing";
        } else {
            return "";
        }
    }
    
    public static function setUserforloyalty($user_id){
        $hash = "wall_day".$user_id;
        $params1 = yii::app()->cache->hexists($hash,1);
        $today =intval((strtotime("tomorrow 00:00")-strtotime("now")));
        if(isset($params1) && ($params1===false)){
            $time = intval(($today-strtotime("now")));
            Yii::app()->cache->hset('wall_day'.$user_id,1, 1);
            Yii::app()->cache->expire($user_id,$time);//expire after midnight of day.
            return true;
        }
        return false;
    }
    
    public static function Processpayments($data_rev) {
        $user_id = isset($data_rev['userid']) ? $data_rev['userid'] : Yii::app()->user->id ;
        Yii::app()->user->id = $user_id;
        if(!isset(Yii::app()->user->username) || empty(Yii::app()->user->username)){
            $details = Yii::app()->db->createCommand("SELECT user_name FROM users where id=".$user_id)->queryAll();
            $data_rev['user_name'] = $details[0]['user_name'];
        }else{
            $data_rev['user_name'] = Yii::app()->user->username;
        }
       // file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step2 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
        $data_rev['userid'] = $user_id;
        $data_rev['users_id'] = $data_rev['userid'];
        $data_rev['trans_category'] = isset($data_rev['trans_category'])?$data_rev['trans_category']:self::getTransactionCategory($data_rev);
        //check whether user is black-listed or not
        if(UsersComponent::isBlackListed($user_id)){
            $fraud_response = array('status'=>'failure','errCode'=>900,'description'=>'Your Account is suspended due to detection of fraudualent activities. Please contact customer care');
            return json_encode($fraud_response);
        }
         //check daily wallet transaction count (for recharge and billing)
        $result = Yii::app()->db->createCommand("SELECT count(trans_id) as cts ,sum(transaction_amount) as total_amt FROM transactions WHERE users_id ='" . Yii::app()->user->id . "' AND status = 2 AND transaction_mode = 'wallet' AND trans_category='recharge' AND trans_date='".date('Y-m-d')."'")->queryAll();
        $DailyTxnLimit = Yii::app()->params['GLOBALS']['_DAILY_TRANSACTION_LIMIT'];
        $DailyBillingTxnLimit = Yii::app()->params['GLOBALS']['_DAILY_BILLINGTRANSACTION_LIMIT'];
        $MonthlyRechageLimit = Yii::app()->params['GLOBALS']['_MONTHLY_RECHARGE_LIMIT'];
        $MonthlyBillingLimit = Yii::app()->params['GLOBALS']['_MONTHLY_BILLING_LIMIT'];
        $transcategoryArr = array('recharge','billing');
        if(!isset($data_rev['returnparam'])){
            $DailyTxnLimit = $DailyTxnLimit-1;
//            $MonthlyRechageLimit = $MonthlyRechageLimit-1;
//            $MonthlyBillingLimit = $MonthlyBillingLimit-1;
        }
        if(!empty($result[0]) && isset($result[0]['cts']) && ($result[0]['cts'] >= $DailyTxnLimit) && $data_rev['trans_category']=='recharge'){
                $exceed_response = array('status'=>'failure','errCode'=>245,'description'=>'Oops! seems you have crossed the limit. Please come back tomorrow.');
        	//$exceed_response = array('status'=>'failure','errCode'=>245,'description'=>'You have exceeded your daily transaction limit of '.$DailyTxnLimit.' transactions per day.');
            return json_encode($exceed_response);
        }
        $result = Yii::app()->db->createCommand("SELECT count(trans_id) as cts ,sum(transaction_amount) as total_amt FROM transactions WHERE users_id ='" . Yii::app()->user->id . "' AND status = 2 AND transaction_mode = 'wallet' AND trans_category ='billing' AND trans_date='".date('Y-m-d')."'")->queryAll();
        if(!empty($result[0]) && isset($result[0]['cts']) && ($result[0]['cts'] >= $DailyBillingTxnLimit) && $data_rev['trans_category']=='billing'){
        	$exceed_response = array('status'=>'failure','errCode'=>245,'description'=>'Oops! seems you have crossed the limit. Please come back tomorrow.');
            return json_encode($exceed_response);
        }
        //check monthly transaction count for prepaid,dth and datacard
        $startDate =  GeneralComponent::getMonthStartDate();
        if(isset($data_rev['trans_category']) && $data_rev['trans_category']=='recharge'){
            
            $result = Yii::app()->db->createCommand("SELECT count(trans_id) as total_recharge  FROM transactions WHERE users_id ='" . Yii::app()->user->id . "' AND status = 2 AND transaction_mode = 'wallet' AND trans_category ='recharge' AND (trans_date>='".$startDate."' AND trans_date<='".date('Y-m-d')."')")->queryAll();
            if(!empty($result[0]) && ($result[0]['total_recharge'] >= $MonthlyRechageLimit)){
        	$exceed_response = array('status'=>'failure','errCode'=>245,'description'=>'Sorry! you have exceeded the limit. Please try later.');
                return json_encode($exceed_response);
            }
            
        }elseif (isset($data_rev['trans_category']) && $data_rev['trans_category']=='billing') {
            
            $result = Yii::app()->db->createCommand("SELECT count(trans_id) as total_recharge  FROM transactions WHERE users_id ='" . Yii::app()->user->id . "' AND status = 2 AND transaction_mode = 'wallet' AND trans_category ='billing' AND (trans_date>='".$startDate."' AND trans_date<='".date('Y-m-d')."')")->queryAll();
            if(!empty($result[0]) && ($result[0]['total_recharge'] >= $MonthlyBillingLimit)){
        	$exceed_response = array('status'=>'failure','errCode'=>245,'description'=>'Sorry! you have exceeded the limit. Please try later.');
                return json_encode($exceed_response);
            }
        }
        
       //check daily wallet transaction amount (for recharge and billing)
        $DailyAmtLimit = Yii::app()->params['GLOBALS']['_DAILY_AMOUNT_LIMIT'];
        if(!empty($result[0]) && isset($result[0]['total_amt']) && ($result[0]['total_amt'] >= $DailyAmtLimit)){
            $exceed_response = array('status'=>'failure','errCode'=>245,'description'=>'You have exceeded your daily transaction amount of Rs. '.$DailyAmtLimit.' per day.');
            return json_encode($exceed_response);
        }
        $data_rev['amount'] = isset($data_rev['amount']) ? $data_rev['amount'] : 0;
        if($data_rev['amount']<0){
            $wrongAmount = array('status'=>'failure','errCode'=>245,'description'=>'Please give valid amount.');
            return json_encode($wrongAmount);
        }
            Yii::app()->session['exception'] = "";
           
        try {
            //check payment within 2 min if yes then send in-process message
            if(in_array($data_rev['trans_category'],$transcategoryArr) && !isset($data_rev['returnparam'])){
                $hash = $data_rev['amount'].'_'.$user_id.'_'.$data_rev['number'];
                if(isset($data_rev['operator_id']) && !empty($data_rev['operator_id'])){
                    $hash = $data_rev['amount'].'_'.$user_id.'_'.$data_rev['number'].'_'.$data_rev['operator_id'];
                }
                if(isset($data_rev['flag']) && !empty($data_rev['flag'])){
                    $hash = $data_rev['amount'].'_'.$user_id.'_'.$data_rev['number'].'_'.$data_rev['operator_id'].'_'.$data_rev['flag'];
                } 
                $params1 = yii::app()->cache->hexists($hash,1);
                if(isset($params1) && ($params1===true)){ //user get only 1 free bee in 24 hours
                   // throw new Exception("Your recharge of Rs. ".$data_rev['amount']." for ".$data_rev['number']." is in process", 261);
                    $exceed_trnsaction = array('status'=>'failure','errCode'=>261,'description'=>"Your recharge of Rs. ".$data_rev['amount']." for ".$data_rev['number']." is in process. Please try again after 2 min.");
                    return json_encode($exceed_trnsaction);
                }
                
            }
            //self::partialpayment($data_rev);
            $byvoucher=1;
            $connection = Yii::app()->db;
            $data_rev['actual_price'] = 0;
            //for partial payment
            $functions = array('recharge','billpayment','purchase_deal');
            if(strtolower($data_rev['actiontype'])=='purchase_deal'){
                if(isset($data_rev['quantity']) && (empty($data_rev['quantity']) || $data_rev['quantity']<0) ){
                    $result = array('status' => 'failure', 'errCode' =>'001', 'description' => 'Please placed valid quantity.');
                    return json_encode($result);
                }
                $q="SELECT name,by_voucher,status FROM deals WHERE id =".$data_rev['deal_id'];
                $dealData = $connection->createCommand($q)->queryRow(); //deal details
                $userDeal =  $connection->createCommand("SELECT users_id FROM deal_transaction WHERE users_id=$user_id AND deal_id=".$data_rev['deal_id'])->queryAll();
                $query  = "SELECT min_amount, offer_price,total_stock, stock_sold,max_limit_peruser,actual_price FROM offers WHERE id=".$data_rev['offer_id']." AND sequence=1 " ;
                $offerResult =  Yii::app()->db->createCommand($query)->queryRow();//offer details
                $data_rev['offer_price'] = $offerResult['offer_price'];
                if($dealData['by_voucher']==0){
                    $byvoucher = 0;
                    $data_rev['by_voucher'] = 0;
                    
                }else{
                    $data_rev['by_voucher'] = $dealData['by_voucher'];
                    //Check if the User is Genuine or InGenuine
                    $IsUserGenuineQuery  = "SELECT is_genuine FROM `user_profile` WHERE `user_id` = $user_id";
                    file_put_contents("/tmp/Processpayments".date('Ymd').".log", "| step1 ".$IsUserGenuineQuery, FILE_APPEND | LOCK_EX);
                    $IsUserGenuine =  Yii::app()->db->createCommand($IsUserGenuineQuery)->queryRow();
                    //If User is no-Genuine then dont allow to grab this gift
                    if($IsUserGenuine['is_genuine']==0){
                       // $data_rev['offer_price'] = $offerResult['offer_price'];
                        $result = array('status' => 'failure', 'errCode' =>'001', 'description' => 'Oo oh! The product has been sold out. Please try later.');
                        return json_encode($result);
                    }
                    
                  //  file_put_contents("/tmp/Processpayments".date('Ymd').".log", "| step1 ".$IsUserGenuine['is_genuine']." offer price ". $data_rev['offer_price']."|".$offerResult['offer_price']. "|".$offerResult['actual_price'], FILE_APPEND | LOCK_EX);
                    
                }
                $data_rev['amount'] = $offerResult['min_amount'];
                $data_rev['actual_price'] = $offerResult['actual_price'];
              
//                $DailyVoucherCountquery  = "SELECT COUNT( * ) as total_voucher FROM  `coupons` WHERE users_id=$user_id AND DATE( createdon ) =  '".date('Y-m-d')."'";
//                $DailyVoucherCount =  Yii::app()->db->createCommand($DailyVoucherCountquery)->queryRow();//Total number of voucher in a day
                
                //check daily e-voucher limit 
//                $DailyEVoucherLimit = Yii::app()->params['GLOBALS']['_DAILY_E-VOUCHER_LIMIT'];
//                if($DailyVoucherCount['total_voucher'] > $DailyEVoucherLimit){
//                    $result = array('status' => 'failure', 'errCode' =>'001', 'description' => 'Exceeding E-Voucher user Quantity.');
//                    return json_encode($result);
//                }
                //check whther user can get more than 1 offer or not
                if(count($userDeal) >= $offerResult['max_limit_peruser'] && $offerResult['max_limit_peruser']!=-1){
                    $result = array('status' => 'failure', 'errCode' =>'501', 'description' => 'Exceeding per user quantity.');
                    return json_encode($result);
                }
                
                 $requiredCoins = $data_rev['amount'] * $data_rev['quantity']; //amount=coins
                 
                  //check stock available or not
                $quantityAvailable = $offerResult['total_stock']-($offerResult['stock_sold']+$data_rev['quantity']);
                if($quantityAvailable<0  && $offerResult['total_stock']!=-1){
                    $result = array('status' => 'failure', 'errCode' =>'252', 'description' => 'This item is out of stock');
                    return json_encode($result);
                }
                //if deal is not open (closed/expired) 
                if($dealData['status']!=1){
                    $result = array('status' => 'failure', 'errCode' =>'252', 'description' => 'This item is out of stock');
                    return json_encode($result);
                }
            }
            $requiredAmount = $data_rev['amount'];
            if(!isset($data_rev['returnparam']) && isset($data_rev['actiontype']) && in_array(strtolower($data_rev['actiontype']),$functions) && $byvoucher ){
                $balance = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $data_rev['userid'])->queryAll();
                if(isset($data_rev['api_version']) && $data_rev['api_version']==3 && $data_rev['trans_category']=='deal'){
                     $requiredAmount = $data_rev['offer_price'] * $data_rev['quantity'];
                    if($balance[0]['loyalty_points'] < ($requiredCoins) && !empty($requiredCoins) ){
                          $result = array('status' => 'failure', 'errCode' =>'501', 'description' => 'minimum '.(($requiredCoins)-$balance[0]['loyalty_points']).' loyalty coins required ');
                          return json_encode($result);
                    }
                    
                }
                if($data_rev['paymentopt']!=='online'){
                   // file_put_contents("/tmp/user_cashback".date('Ymd').".log", "| step1 ".  "SELECT * FROM wallets WHERE users_id =" . $data_rev['userid']."\n", FILE_APPEND | LOCK_EX);
                    $requiredAmount = $balance[0]['account_balance'] - $requiredAmount;   
                }
                if(($requiredAmount<0 || $data_rev['paymentopt']=='online') && isset($data_rev['partial'])){
                       $data_rev['paymentopt']='wallet';
                       $requiredAmount = abs($requiredAmount);
                       $fun = array('function'=>'Processpayments');
                       $data_rev = array_merge($data_rev,$fun);
                       $data = array('trans_category' => 'refill', 'paymentopt' => 'online', 'amount'=>$requiredAmount, 'returnparam'=>$data_rev);
                       if(isset($data_rev['return_url'])){
                          $data['return_url']=   urldecode($data_rev['return_url']);
                          $data['returnparam']['return_url'] = $data['return_url'];
                          if(strtolower($data_rev['actiontype'])=='purchase_deal'){
                            $data['returnparam']['category'] = 'deal';
                            $data['operator'] = $dealData['name'];
                          }else{
                              $data['returnparam']['category'] = '';
                          }
                          $data['request_via']= $data_rev['request_via'];
                          $data['returnparam']['partial']=1;
                          // array_push($data,array('return_url'=>,'request_via'=>$data_rev['request_via']));
                       }
                       $res = json_encode(WalletComponent::refillwallet_Online($data));   
                       return $res;
                }

            }
           // $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();            
            
            $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $data_rev['userid'] . " FOR UPDATE");
            $walletdetail = $walletdata->queryAll();
            $walletObj = $walletdetail[0];
            
            $recharge_flag = 0;
            if(isset($data_rev['msisdn']) && isset($data_rev['origincode'])){
            	$recharge_flag = 2;
            }
            else if(isset($data_rev['recharge_flag']) && $data_rev['recharge_flag']==1){
            	$recharge_flag = 1;
            }
            if(Yii::app()->params['GLOBALS']['MAX_WALLET_BALANCE_ALLOWED'] < $walletObj['account_balance']){
               GeneralComponent::sendMail("tadka@mindsarray.com,nandan@mindsarray.com,sunilc@mindsarray.com,dharmesh@mindsarray.com", "Alert: User have wallet balance more than max limit", json_encode($data_rev));
            }
          
            $balance_flag = false;
            if(isset($data_rev['api_version']) && $data_rev['api_version']==3 && $data_rev['trans_category']=='deal'){
                $data_rev['voucherName']=$dealData['name'];
                $data_rev['account_balance']  = $walletObj['loyalty_points'];
                //by_vaucher = 1 for e-vaucher
                if(!empty($data_rev['by_voucher'])){
                   $data_rev['account_balance'] =  $walletObj['account_balance'];
                   $data_rev['voucher']=true;
                   $data_rev['points'] = $walletObj['loyalty_points']  - $requiredCoins;
                   $data_rev['new_bal'] = $walletObj['account_balance'] - ($data_rev['offer_price']*$data_rev['quantity']);
                   $data_rev['transaction_mode'] = 'wallet';
                   if(($data_rev['new_bal']>=0) && ($data_rev['points'] >=0) && $data_rev['offer_price']>0 && $walletObj['account_balance']>0){
                       $balance_flag = true;   
                    }
                }else{
                   $data_rev['new_bal'] = $walletObj['loyalty_points']  - $requiredCoins;
                   $data_rev['points']  = $data_rev['new_bal'];     
                   $data_rev['transaction_mode'] = 'loyaltypoints';
                   if($data_rev['new_bal']>=0){
                      $balance_flag = true;   
                    }
                }
            }else{
                $data_rev['account_balance'] =  $walletObj['account_balance'];
                $data_rev['new_bal'] = $walletObj['account_balance'] - $data_rev['amount'];
                $data_rev['transaction_mode'] = 'wallet';
                if($data_rev['new_bal']>=0){
                   $balance_flag = true;
                }
            }
            if($balance_flag || $data_rev['paymentopt']==='online') {
                if((isset($data_rev['by_voucher']) && $data_rev['by_voucher']!= 0)){
                    $data = array('opening_bal'=>$data_rev['account_balance'],'transaction_amount'=>($data_rev['offer_price']*$data_rev['quantity']),'closing_bal'=>$data_rev['new_bal'],
                            'trans_category'=>$data_rev['trans_category'],'users_id'=>$data_rev['userid'],'trans_type'=>'0','transaction_mode'=> 'wallet',
                            'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>1,'trans_datetime'=>GeneralComponent::getFormatedDate(),'recharge_flag'=>$recharge_flag);
                    
                    TransactionComponent::newTransaction($connection, $data);
                    $data_rev['transaction_id'] = $connection->lastInsertID;
                    $data_rev['trans_id']       = $data_rev['transaction_id'];
                    TransactionComponent::updateTransaction($connection,$data_rev['transaction_id'],array('ref_trans_id'=>$data_rev['transaction_id']));

                    $data_L = array('opening_bal'=>$walletObj['loyalty_points'],'transaction_amount'=>($data_rev['amount']*$data_rev['quantity']),'closing_bal'=>$data_rev['points'],
                            'trans_category'=>$data_rev['trans_category'],'users_id'=>$data_rev['userid'],'trans_type'=>'0','transaction_mode'=> 'loyaltypoints',
                            'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>1,'trans_datetime'=>GeneralComponent::getFormatedDate(),'recharge_flag'=>$recharge_flag);
                    TransactionComponent::newTransaction($connection, $data_L);
                    $data_rev['transaction_id_L'] = $connection->lastInsertID;
                    $data_rev['trans_id_L']       = $data_rev['transaction_id_L'];
                    TransactionComponent::updateTransaction($connection,$data_rev['transaction_id_L'],array('ref_trans_id'=>$data_rev['transaction_id']));
                }else {  
                    $data = array('opening_bal'=>$data_rev['account_balance'],'transaction_amount'=>$data_rev['amount'],'closing_bal'=>$data_rev['new_bal'],
                            'trans_category'=>$data_rev['trans_category'],'users_id'=>$data_rev['userid'],'trans_type'=>'0','transaction_mode'=> $data_rev['transaction_mode'],
                            'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>1,'trans_datetime'=>GeneralComponent::getFormatedDate(),'recharge_flag'=>$recharge_flag);

                    TransactionComponent::newTransaction($connection, $data);
                    $data_rev['transaction_id'] = $connection->lastInsertID;
                    $data_rev['trans_id']       = $data_rev['transaction_id'];
                    $data_rev['respns_trans_id'] = isset($data_rev['responce_transaction_id']) ? $data_rev['responce_transaction_id'] : $connection->lastInsertID;
                    TransactionComponent::updateTransaction($connection,$data_rev['transaction_id'],array('ref_trans_id'=>$data_rev['transaction_id']));
                }
                switch ($data_rev['trans_category']){
                case "recharge":
                   // file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step3 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    $response_detail = self::recharge_by_wallet($connection, $data_rev);
                  //  file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step4 ".json_encode($response_detail)."\n", FILE_APPEND | LOCK_EX);
                    $notify_success_var = array();
                    if(!(isset($data_rev['msisdn']) && isset($data_rev['origincode']))){
                        if(isset($data_rev['api_version']) && $data_rev['api_version']==3){
                            $notify_success_var = array("RECHARGE_SUCCESS_NEW");
                        }else{
                            $notify_success_var = array("RECHARGE_SUCCESS");
                        }
                    }
                    $data['operator_name'] = GeneralComponent::getProductDetails($data_rev['product'],GeneralComponent::get_plancode($data_rev['operator_id'],$data_rev['product']),TRUE);
                    break;
                case "billing":
                    
                    $response_detail = self::billpayment_by_wallet($connection, $data_rev);
                    if(isset($data_rev['api_version']) && $data_rev['api_version']==3){
                        $notify_success_var = array("BILLPAYMENT_SUCCESS_NEW");
                    }else{
                        $notify_success_var = array("BILLPAYMENT_SUCCESS");
                    }
                    $data['operator_name'] = GeneralComponent::getProductDetails($data_rev['product'],GeneralComponent::get_plancode($data_rev['operator_id'],$data_rev['product']),TRUE);
                    break;
                case "deal":
                    //$data_rev['freebee'] = true;//set freebee for deal
                    //file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step8 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    $response_detail = self::dealpayment_by_wallet($connection, $data_rev);
                  // file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step9 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    if(isset($data_rev['freebie'])){
                        $notify_success_var = array("TEST");
                    }else{
                        if(isset($data_rev['api_version']) && $data_rev['api_version']==3){
                            $notify_success_var = array("DEAL_SUCCESS","DEAL_DETAILS");
                            //$notify_success_var = array("DEAL_SUCCESS_NEW","DEAL_DETAILS_NEW");
                        }else{
                           $notify_success_var = array("DEAL_SUCCESS","DEAL_DETAILS");
                        }
                    }
                    $data['operator_name'] = '';
                    break;
                }
                 
                TransactionComponent::LoyaltyTransaction($connection,$data_rev);//set for loyalty points
                $response_detail_array = json_decode($response_detail, true);
                 file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step1 ".json_encode($response_detail_array)."\n", FILE_APPEND | LOCK_EX);

                if ($response_detail_array['status'] === 'failure'){
                    throw new Exception($response_detail_array['description'], 501);
                }
                $response_id = isset($response_detail_array['ref_code']) ? $response_detail_array['ref_code'] : "";
                $connection->createCommand("UPDATE transactions SET response_id='" . $response_id . "',status='2',"
                        . "response='" . addslashes($response_detail) . "' WHERE trans_id='" . $data_rev['transaction_id'] . "'")->execute();
                $result = array('status' => 'success', 'errCode' => '0', 'description' => $data_rev['transaction_id']);
               // file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step1 ".json_encode($response_detail_array)."\n", FILE_APPEND | LOCK_EX);
                
                $transaction->commit();
                if(in_array($data_rev['trans_category'],$transcategoryArr) ){
                $hash = $data_rev['amount'].'_'.$user_id.'_'.$data_rev['number'];
                if(isset($data_rev['operator_id']) && !empty($data_rev['operator_id'])){
                    $hash = $data_rev['amount'].'_'.$user_id.'_'.$data_rev['number'].'_'.$data_rev['operator_id'];
                }
                if(isset($data_rev['flag']) && !empty($data_rev['flag'])){
                    $hash = $data_rev['amount'].'_'.$user_id.'_'.$data_rev['number'].'_'.$data_rev['operator_id'].'_'.$data_rev['flag'];
                }      
                $params1 = yii::app()->cache->hexists($hash,1);
                if(isset($params1) && ($params1===true)){ //user get only 1 free bee in 24 hours
                   // throw new Exception("Your recharge of Rs. ".$data_rev['amount']." for ".$data_rev['number']." is in process", 261);
                    //$exceed_trnsaction = array('status'=>'failure','errCode'=>261,'description'=>"Your recharge of Rs. ".$data_rev['amount']." for ".$data_rev['number']." is in process");
                    //return json_encode($exceed_trnsaction);
                }
                else{
                    Yii::app()->cache->hset($hash,1,1);
                    Yii::app()->cache->expire($hash,120);//expire after 120 seconds.
                }
            }
                $servicePoints  = 0;
                $loyalty_balnce = 0;
                $present_wallet_detail = self::getWalletDetail();
                if(isset($data_rev['api_version']) && $data_rev['api_version'] == 3){
                     $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
                    if($data_rev['trans_category']!='deal'){
                       $servicePoints    = $data_rev['amount']*$Loyalty_points[$data_rev['flag']];
                    }
                    $loyalty_balnce      = $present_wallet_detail['loyalty_points'];
                }
                if(isset($data_rev['quick_pay_flag']) && $data_rev['quick_pay_flag'] == TRUE){
                    file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step1 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    $add_to_quick_pay    = ServiceComponent::is_AlreadyQuickpay($data_rev)?"":ServiceComponent::addQuickpay($data_rev);
                } 
                $expiry = isset($response_detail_array['EXPIRY']) ? $response_detail_array['EXPIRY'] : 0;
                $pin = isset($response_detail_array['description']['pin']) ? $response_detail_array['description']['pin'] : "";
                if(is_array($response_detail_array['description']) && isset($response_detail_array['description']['deal_coupon_code'])){ 
                   $result['description'] = array('transaction_id'=>$data_rev['transaction_id'],'deal_coupon_code'=>$response_detail_array['description']['deal_coupon_code'],'closing_balance'=>$present_wallet_detail['account_balance'],'loyalty_points'=>$servicePoints,'loyalty_balance'=>$loyalty_balnce,'expiry'=>$expiry,'pin'=>$pin);
                }else{
                   $result['description'] = array('transaction_id'=>$data_rev['transaction_id'],'closing_balance'=>$present_wallet_detail['account_balance'],'operator_name'=>$data['operator_name'],'loyalty_points'=>$servicePoints,'loyalty_balance'=>$loyalty_balnce,'expiry'=>$expiry,'pin'=>$pin);
                }
               // file_put_contents("/tmp/usercashbag".date('Ymd').".log", "| step8 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                if(isset($data_rev['cb_code']) && !empty($data_rev['cb_code'])){
                    //give cashback
                    $cashback = array('code'=>$data_rev['cb_code'],'transaction_id'=>$data_rev['transaction_id'],'recharge_amount'=>$data_rev['amount']);
                    VoucherComponent::user_cashback($cashback);
                }
               // file_put_contents("/tmp/usercashbag".date('Ymd').".log", "| step9 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                $result['description']['online_form']=false;
                $notify_data = array_merge($data_rev,array('amount'=>$data_rev['amount'],'user_id'=>$data_rev['userid'],'operator'=>$data['operator_name'],
                        'balance'=>$present_wallet_detail['account_balance'],'txn_id'=>$data_rev['transaction_id'],'loyalty_points'=>$servicePoints,'loyalty_balance'=>$loyalty_balnce));
                GeneralComponent::notifier($notify_success_var,$notify_data);
               //  file_put_contents("/tmp/usercashbag".date('Ymd').".log", "| step10 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                //-----------------
            }else{
                if(isset($data_rev['api_version']) && $data_rev['api_version']==3 && $data_rev['trans_category']=='deal' && isset($data_rev['amount'])){
                    $amount  = $data_rev['amount'];
                    $content = Yii::app()->params['notification_template']['INSUFFICIENT_COINS'];
                    $content = GeneralComponent::replaceword('<'.'AMOUNT'.'>', ($amount* $data_rev['quantity']), $content);
                    throw new Exception($content, 211);
                }else{
                  throw new Exception("Insufficent balance", 211);  
                }
                
            }
        } catch (Exception $e) {
            if(isset($transaction)){
                $transaction->rollback();
            }
            if($e->getCode()=='272'){
                $connection = Yii::app()->db;
                $dealStatus = $connection->createCommand("SELECT status FROM deals WHERE id = ".$data_rev['deal_id'])->queryRow();
                if(isset($dealStatus['status']) && $dealStatus['status']==1){
                    $user = $data_rev['user_name'];
                    $currentTime = date('Y-m-d h:i:s');

                    $subject = "Out of Stock | E-gift Voucher";
                    $message = "Hello \r\n E-gift Voucher of ".$data_rev['voucherName']." of ".$data_rev['offer_price']." is out of stock.\r\n Request generated by $user";
                    $message .=" \r\n Please contact to Giftxoxo immediately to allocate the gift voucher.";
                    $sender = 'support@pay1.in';
                    $to = 'evoucher@pay1.in';


                    $currentTime = date('Y-m-d h:i:s');
                    $connection->createCommand("UPDATE deals SET status=2 where id = ".$data_rev['deal_id'])->execute();
                    $connection->createCommand("UPDATE offers SET updated='$currentTime' where id = ".$data_rev['offer_id'])->execute();
                  // file_put_contents("/var/log/apps/dealpaymentbywallet".date('Ymd').".log", "|  "."UPDATE deals SET status=2 where id = ".$data_rev['deal_id']."\n", FILE_APPEND | LOCK_EX);
                    GeneralComponent::sendMail($to, $subject, $message,$sender);
                }
            }
            Yii::app()->session['exception'] = $e->getMessage();
            $result = array('status' => 'failure', 'errCode' => $e->getCode(), 'description' => $e->getMessage());
            if($e->getCode()=='0'){
                $result = array('status' => 'failure', 'errCode' => $e->getCode(), 'description' => 'There is error in transaction. Please contact admin');
            }
            
        }
        // file_put_contents("/tmp/usercashbag".date('Ymd').".log", "| step11 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
        return json_encode($result);
    }

    private static function recharge_by_wallet($connection, $data_rev) {
        try{
            TransactionComponent::RechargeTransaction($connection, $data_rev);
            $username = "";
            if(!isset(Yii::app()->user->username)){
                $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id: 0;
                $query = "SELECT mobile_number FROM users WHERE id =".$user_id;
                $result = Yii::app()->db->createCommand($query)->queryAll();
                if(count($result)>0){
                    $username = $result[0]['mobile_number'];
                }
            }else{
                $username = Yii::app()->user->username;
            }


            $connection->createCommand("UPDATE wallets set account_balance = account_balance - " . $data_rev['amount'] . " WHERE users_id='" . $data_rev['userid'] . "'")->execute();

            $params = array('number' => $data_rev[$data_rev['recharge_column']], 'product' => $data_rev['product'], 'amount' => $data_rev['amount'],
                    'trans_id' => $data_rev['transaction_id'], 'operator' => GeneralComponent::get_plancode($data_rev['operator_id'], $data_rev['product']), 'stv' => $data_rev['stv'],'mobile'=>$username);

            $response_detail = VendorComponent::callPay1($params);
            Yii::app()->session['apiResponse'] = $response_detail;
            return $response_detail;
        }
        catch(Exception $ex){
            return array();
        }
    }

    private static function billpayment_by_wallet($connection, $data_rev) {
        TransactionComponent::BillingTransaction($connection, $data_rev);
        try{
            $connection->createCommand("UPDATE wallets set account_balance = account_balance - " . $data_rev['amount'] . " WHERE users_id='" . $data_rev['userid'] . "'")->execute();
        }catch(Exception $ex){
            
        }
        
        $response_detail = self::callBillingVendor($data_rev);
        Yii::app()->session['apiResponse'] = $response_detail;
        return $response_detail;
    }

    private static function callBillingVendor($data_rev) {
        $billtype = $data_rev['payment_flag'];
        $operator = GeneralComponent::get_plancode($data_rev['operator_id'], $data_rev['product']);
        if($operator == 0){
            throw new Exception("Invalid Operator",243);
        }
        $mobile="";
        
        if(!isset(Yii::app()->user->username) || empty(Yii::app()->user->username)){
            $details = Yii::app()->db->createCommand("SELECT user_name FROM users where id=".$data_rev['userid'])->queryAll();
            
            $mobile = $details[0]['user_name'];
        }else{
            $mobile = Yii::app()->user->username;
        }
        switch ($billtype) {
            case 1:
                $params = array('number' => $data_rev[$data_rev['recharge_column']], 'product' => $data_rev['product'],
                        'amount' => $data_rev['base_amount'], 'trans_id' => $data_rev['transaction_id'],
                        'operator' => $operator, 'stv' => $data_rev['stv'],'mobile'=>$mobile);
                $response_detail = VendorComponent::callPay1($params);
                break;
            case 2:
                break;
            default:
                throw new Exception("No bill type specified",218);
                break;
        }
        return $response_detail;
    }
   
    public static function dealpayment_by_wallet($connection, $data_rev){
        // check for free bee
        $allow_freebie = false;
        $IsFreebie     = false;
       try{
            $Offer_validity = true;
            if(!isset($data_rev['by_voucher']) || (isset($data_rev['by_voucher']) && empty($data_rev['by_voucher']))){
              $Offer_validity = ServiceComponent::check_Offer_validity($data_rev['offer_id']);  
            }
            if($Offer_validity===false){
                throw new Exception("This item is out of stock", 252);
            }
            if(isset($data_rev['freebie'])){
                file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step10 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                 if((isset($data_rev['ref_id']) && GeneralComponent::validate_transaction($data_rev['ref_id']))){
                     throw new Exception("Free gift with this transaction is already grabbed", 270);
                }
                 file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step11 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
            if(isset($data_rev['transaction_id_L'])){
               $data_rev['ref_trans_id'] = $data_rev['trans_id'];  
            }else{
               $data_rev['ref_trans_id'] = (isset($data_rev['ref_id']) && GeneralComponent::validate_transaction($data_rev['ref_id'])) ? $data_rev['ref_id'] : GeneralComponent::get_freebie_ref_id($data_rev);
            }
             file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step12 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                //check for loyalty points redeem
                //check freebie already used
               // $allow_freebie = DealsComponent::allow_freebie($connection,$data_rev);
                unset($data_rev['freebie']);
                if($allow_freebie===true){
                    throw new Exception("You have already grabbed it. Try out the other free gifts.", 253);
                }
                $IsFreebie = true;                
            }
            if(isset($data_rev['freebie'])){       
                //set
               $data_rev['user_id'] = $data_rev['users_id'];
               $responce = GeneralComponent::set_FreeBie($connection,$data_rev);
               return $responce;
            }
            else{
                
                 file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step13 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                
                $data_rev['users_id'] = $data_rev['userid'];
                if(isset($data_rev['by_voucher']) && !empty($data_rev['by_voucher'])){
                    $voucher = TransactionComponent::GenerateVoucher($connection,$data_rev);
                     file_put_contents("/var/log/apps/dealpaymentbywallet".date('Ymd').".log", "|  ".  json_encode($voucher)."\n", FILE_APPEND | LOCK_EX);
                    if($voucher['status']=='success'){
                        $data_rev['coupon_code'] = json_decode($voucher['description']['code'],true);
                        $data_rev['pin'] = json_decode($voucher['description']['pin'],true);
                        $data_rev['pin'] = $data_rev['pin'][0];
                        file_put_contents("/var/log/apps/dealpaymentbywallet".date('Ymd').".log", "|  ".  json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    }else{
                        file_put_contents("/var/log/apps/dealpaymentbywallet".date('Ymd').".log", "|  ".  json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                          throw new Exception("Oops !! Something went wrong, your money is safe & credited to the wallet. Try again later.", 272);
                    }
                }else{
                    for($i=0;$i<$data_rev['quantity'];$i++){
                        $coupon[] = TransactionComponent::DealTransaction($connection, $data_rev);
                    }
                    if(empty($coupon)){
                         throw new Exception("Oops !! Something went wrong",272);
                    }
                    $data_rev['coupon_code'] = $coupon;
                }
                if($data_rev['quantity']==1){
                            $data_rev['coupon_code'] = $data_rev['coupon_code'][0];
                 }
                if(trim($data_rev['paymentopt'])==='online'){
                    $result = OnlineComponent::OnlinePaymentProcessor($data_rev);
                    echo GeneralComponent::get_formated_response(json_encode($result),$data_rev);
                    exit();
                }else{
                     file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step14 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    $offerID = $data_rev['offer_id'];
                    $qry     = "SELECT total_stock,stock_sold FROM `offers` where id = $offerID";
                    $result  = $connection->createCommand($qry)->queryAll();
                    if($result[0]['total_stock']>=($result[0]['stock_sold']+$data_rev['quantity']) || $result[0]['total_stock']==-1){
                        if(isset($data_rev['api_version']) && $data_rev['api_version']==3 && $data_rev['trans_category']=='deal'){
                            if(isset($data_rev['transaction_id_L']) && ($data_rev['transaction_id_L'] != 0)){
//                                $connection->createCommand("UPDATE wallets set loyalty_points = loyalty_points - " . $data_rev['gift_coin'] . " , account_balance = account_balance - " . $data_rev['total_amount'] . " WHERE users_id='" . $data_rev['userid'] . "'")->execute();
                                $connection->createCommand("UPDATE wallets set loyalty_points = loyalty_points - " . ($data_rev['amount']*$data_rev['quantity']) . " , account_balance = account_balance - " . ($data_rev['offer_price']*$data_rev['quantity']). " WHERE users_id='" . $data_rev['userid'] . "' AND loyalty_points>=0")->execute();
                                if(isset($data_rev['responce_transaction_id'])){
                                    $array =  array('status'=>2,'free_points'=>0,'loyalty_points'=>0,'ref_trans_id'=>$data_rev['responce_transaction_id']);
                                }else{
                                    $array =  array('status'=>2,'free_points'=>0,'loyalty_points'=>0);
                                }
                                $array_L =  array('status'=>2,'free_points'=>1,'loyalty_points'=>$data_rev['amount']*$data_rev['quantity']);
                            }else{
                                $connection->createCommand("UPDATE wallets set loyalty_points = loyalty_points - " .($data_rev['amount']*$data_rev['quantity']) . " WHERE users_id='" . $data_rev['userid'] . "' AND loyalty_points>=0")->execute();
                                $array =  array('status'=>2,'free_points'=>1,'loyalty_points'=>$data_rev['amount']);
                            } 
                            
                        }else{
                            if($data_rev['trans_category']=='deal'){
                                $data_rev['amount']=0;
                            }
                            $connection->createCommand("UPDATE wallets set account_balance = account_balance - " . $data_rev['amount'] . " WHERE users_id='" . $data_rev['userid'] . "'")->execute();
                            $array =  array('status'=>2);
                        }
                    $connection->createCommand("UPDATE offers set stock_sold = stock_sold + " . $data_rev['quantity'] . " WHERE id='" . $offerID . "'")->execute();
                    if(isset($data_rev['transaction_id_L']) && isset($data_rev['transaction_id'])){
                      TransactionComponent::updateTransaction($connection, $data_rev['transaction_id'], $array);    
                      TransactionComponent::updateTransaction($connection, $data_rev['transaction_id_L'], $array_L);
                    }else{
                      TransactionComponent::updateTransaction($connection, $data_rev['transaction_id'], $array);                    
                    }
                     file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step15 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
                    $pin = isset($data_rev['pin']) ? $data_rev['pin'] : "";
                    $result = array('status'=>'success','errCode'=>0,'description'=>
                                array('message'=>'deal transaction completed successfully','deal_coupon_code'=>$data_rev['coupon_code'],'pin'=>$pin));
                    }
                    else{
                        throw new Exception("This item is out of stock", 252);
                         }
                         
                    }            
                }
                $n_data = array();
           if($IsFreebie===true){
                file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step16 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
              // $deal_data = DealsComponent::getDealOffersList($data_rev['deal_id'],$data_rev['offer_id']);
               $deal_data = DealsComponent::getDealName($data_rev['deal_id']);
               $data_rev['fb_name']   = isset($deal_data[0])?$deal_data[0]['name']:"";
               $deal_transaction_data = DealsComponent::getDealTransaction_expiry($data_rev['transaction_id']);    
               $type = array();
               if($result['status']=='success'){
                   if(isset($data_rev['api_version']) && $data_rev['api_version']==3 && $data_rev['trans_category']=='deal'){
                       $type = array("FREEBIE_SUCCESS_NEW");
                   }else{
                       $type = array("FREEBIE_SUCCESS");
                   }
               }
               else if($result['status']=='failure'){
                    $type = array("FREEBIE_FAILURE");
               }
               $user_id = Yii::app()->user->id;
               $walletData = UsersComponent::getUserLoyatyBalance($user_id);
               $loyaltyPoints =0;
               if(isset($walletData['loyalty_points'])){
                   $loyaltyPoints = $walletData['loyalty_points'];
               }
                file_put_contents("/var/log/apps/process_transaction_after_pg".date('Ymd').".log", "| step17 ".json_encode($data_rev)."\n", FILE_APPEND | LOCK_EX);
              $n_data = array('CODE'=>(isset($data_rev['coupon_code'])?$data_rev['coupon_code']:""),'FB_NAME'=> $data_rev['fb_name'],'EXPIRY'=>$deal_transaction_data['expiry_date'],'user_id'=>$user_id,'loyalty_balance'=>$loyaltyPoints);
              GeneralComponent::notifier($type, $n_data);
           }
          } catch (Exception $ex) {
                $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
            }
           
            return json_encode(array_merge($result,$n_data));

    }
    
    public static function refillwallet($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
            
        try {
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            if(!isset($data['mobile_number']) || empty($data['mobile_number']) )
               // throw new Exception($message, 580);
            $data['userid'] = Yii::app()->user->getId();
            //$data['userid'] = (!isset($data['userid']) || is_null($data['userid']) || $data['userid'] == 0)? (UsersComponent::getIdByName($data['mobile_number'])):$data['userid'];
            if(empty($data['userid']) && isset($data['mobile_number']) && !empty($data['mobile_number'])){
                $data['userid'] = UsersComponent::getIdByName($data['mobile_number']);
            }else if(empty($data['userid']) && (empty($data['mobile_number']))){
                $message = Yii::app()->params['message_template']['EXCEPTION'];
                throw new Exception($message, 580);
            }
            $data['trans_category'] = 'refill';
            $data['transaction_mode'] = isset($data['transaction_mode'])?$data['transaction_mode']:"wallet";   
            if(!isset($data['vcode']) && !isset($data['retailer_id'])){
                throw new Exception('UNAUTHORIZE ACCESS','201');
            }
            $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $data['userid'] . " FOR UPDATE");
            $walletdetail = $walletdata->queryAll();
            $walletObj = isset($walletdetail[0]) ? $walletdetail[0] : null;
            $data['current_bal'] = isset($walletObj['account_balance']) ? $walletObj['account_balance'] : 0;
            $data['new_bal'] = $data['current_bal'] + $data['amount'];
            if(Yii::app()->params['GLOBALS']['MAX_WALLET_BALANCE_ALLOWED'] < $data['new_bal']){
                throw new Exception("Wallet Amount Exceeding Max Allowed Amount",229);
            }
            if(isset($data['trans_id'])){
                $trans_check = $connection->createCommand("SELECT * FROM transactions WHERE response_id='".$data['trans_id']."'")->queryAll();
                if($trans_check){
                    throw new Exception("Duplicate update Request",217);
                }
            }
            $recharge_flag = isset($data['recharge_flag'])?$data['recharge_flag']:0;
            $data['recharge_flag'] = $recharge_flag;
            $rdata = array('opening_bal'=>$data['current_bal'],'transaction_amount'=>$data['amount'],'closing_bal'=>$data['new_bal'],'recharge_flag'=>$recharge_flag,
                   'trans_category'=>$data['trans_category'],'users_id'=>$data['userid'],'trans_type'=>1,'transaction_mode'=>$data['transaction_mode'],
                   'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>2,'response_id'=>GeneralComponent::set_defaultBlank($data,'trans_id'),
                   'trans_datetime'=>GeneralComponent::getFormatedDate());
            TransactionComponent::newTransaction($connection, $rdata);                    
            $data['transaction_id'] = $connection->lastInsertID;
            TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['transaction_id']));
            $data['balance'] = $data['new_bal'];             
            $userFlag = FALSE;
            if (count($walletdetail) > 0) {
                self::refill_user_wallet($connection, $data);
                $userFlag = TRUE;
            } else {
               $data = self::newUser_refill_wallet($connection, $data);
//                self::refill_temp_wallet($connection, $data);
            }
            if(isset($data['vcode']) && trim($data['vcode'])!=""){
                $vdata['txnid'] = $data['transaction_id'];
                $vdata['status'] = 3;
                VoucherComponent::update_voucher_status($connection, $data['vcode'], $vdata);
            }
            $transaction->commit();
            $result = array('status' => 'success', 'errCode' => '0', 'description' => $data['transaction_id']);
            $present_wallet_detail = self::getWalletDetail($data['userid']);
            $closing_bal = isset($present_wallet_detail['account_balance'])?$present_wallet_detail['account_balance']:0;
            if($data['transaction_mode'] == 'voucher'){
               GeneralComponent::notifier(array('WALLET_REFILLED_VOUCHER'),array('amount'=>$data['amount'],'balance'=>$closing_bal,
                        'user_id'=>$data['userid'],'txn_id'=>$data['transaction_id']));
            } 
            if(isset($data['retailer_id']) && $data['retailer_id'] != ""){
                if($userFlag){ 
                    GeneralComponent::notifier(array('WALLET_REFILLED_RETAILER'),$data);
                }else{ 
                    GeneralComponent::notifier(array('WALLET_REFILLED_RETAILER_NONUSER'),$data);
                }
            }
            $result['description'] = array('transaction_id'=>$result['description'],'closing_balance'=>$closing_bal);
        } catch (Exception $e) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $e->getCode(), 'description' => $e->getMessage());
        }        
        return $result;
    }
    
    public static function refillwallet_Online($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        $data['userid'] = Yii::app()->user->id;
        $data['trans_category'] = 'refill';
        $data['trans_mode']='online';
        
        try {
            $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $data['userid'] . " FOR UPDATE");
            $walletdetail = $walletdata->queryAll();
            $walletObj = isset($walletdetail[0]) ? $walletdetail[0] : null;
            $data['current_bal'] = isset($walletObj['account_balance']) ? $walletObj['account_balance'] : 0;
            $data['new_bal'] = $data['current_bal'] + $data['amount'];
            if(Yii::app()->params['GLOBALS']['MAX_WALLET_BALANCE_ALLOWED'] < $data['new_bal']){
                throw new Exception("You are exceeding the maximum allowed amount of Rs.5000",229);
            }
            $rdata = array('opening_bal'=>$data['current_bal'],'transaction_amount'=>$data['amount'],'closing_bal'=>$data['current_bal'],
                        'trans_category'=>$data['trans_category'],'users_id'=>$data['userid'],'trans_type'=>1,'transaction_mode'=>$data['trans_mode'],
                        'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>1,'trans_datetime'=>GeneralComponent::getFormatedDate());
            TransactionComponent::newTransaction($connection, $rdata);                    
            $data['transaction_id'] = $connection->lastInsertID;
            TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['transaction_id']));
            $transaction->commit();
            $result = OnlineComponent::OnlinePaymentProcessor($data);
            //$result = array('status' => 'success', 'errCode' => 0, 'description' => $resultdata);
        } catch (Exception $e) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $e->getCode(), 'description' => $e->getMessage());
        }
        return $result;
     
    }
    
    public static function refill_user_wallet($connection, $data) {
        //lat long add
        $connection->createCommand("UPDATE wallets SET account_balance = account_balance + " . $data['amount'] . " "
                . "WHERE users_id='" . $data['userid'] . "'")->execute();       
        $rdata = array('transaction_id'=>$data['transaction_id'],'datetime'=>GeneralComponent::getFormatedDate(),
                'retailer_name'=>GeneralComponent::set_defaultBlank($data,'retailer_shop'),'retailer_mobile'=>GeneralComponent::set_defaultBlank($data,'retailer_mobile'),
                'retailer_id'=>GeneralComponent::set_defaultBlank($data,'retailer_id'),'bank_detail'=>GeneralComponent::set_defaultBlank($data,'bank_detail'),
                'account_number'=>GeneralComponent::set_defaultBlank($data,'account_number'),
                'lat'=>((isset($data['latitude']) && strlen(trim($data['latitude']))>0) ? $data['latitude'] : 0),
                'lng'=>((isset($data['longitude']) && strlen(trim($data['longitude']))>0) ? $data['longitude'] : 0));
        TransactionComponent::newRefillTransaction($connection, $rdata);
    }
    
    private static function refill_temp_wallet($connection, $data) {
        $rdata = array('trans_id'=>$data['transaction_id'],'mobile'=>$data['mobile_number'],'amount'=>$data['amount'],
                'retailer_id'=>GeneralComponent::set_defaultBlank($data,'retailer_id'),
                'retailer_shop'=>GeneralComponent::set_defaultBlank($data,'retailer_shop'),
                'retailer_mobile'=>GeneralComponent::set_defaultBlank($data,'retailer_mobile'),
                'created'=>  GeneralComponent::getFormatedDate(),
                'lat'=>((isset($data['latitude']) && strlen(trim($data['latitude']))>0) ? $data['latitude'] : 0),
                'lng'=>((isset($data['longitude']) && strlen(trim($data['longitude']))>0) ? $data['longitude'] : 0));
        TransactionComponent::newTempRefillTransaction($connection, $rdata);
        TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['transaction_id']));
    }

    public static function newUser_refill_wallet($connection,$data){        
        $datetime = GeneralComponent::getFormatedDate();
        $connection->createCommand("INSERT INTO users (user_name,mobile_number,password,salt,created_datetime) VALUES('" . $data['mobile_number'] . "',"
                        . "'" . $data['mobile_number'] . "','','','" . GeneralComponent::getFormatedDate() . "')")->execute();
        $data['userid'] = $connection->lastInsertID;
        TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('users_id'=>$data['userid']));
        $connection->createCommand("INSERT INTO wallets (users_id,account_balance, loyalty_points, created_datetime) "
                . "VALUES('" . $data['userid'] . "','" . $data['amount'] . "',100,'$datetime')")->execute();
        $ldata = array('opening_bal'=>$data['current_bal'],'transaction_amount'=>100,'closing_bal'=>100,'recharge_flag'=>$data['recharge_flag'],
                    'trans_category'=>'pay1_refill','users_id'=>$data['userid'],'trans_type'=>1,'transaction_mode'=>'loyalty',
                    'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>2,'response_id'=>GeneralComponent::set_defaultBlank($data,'trans_id'),
                    'trans_datetime'=>GeneralComponent::getFormatedDate(),'free_points'=>1,'loyalty_points'=>100);
        TransactionComponent::newTransaction($connection, $ldata);                    
        $data['transaction_id'] = $connection->lastInsertID;
        TransactionComponent::updateTransaction($connection,$data['transaction_id'],array('ref_trans_id'=>$data['transaction_id']));
        UsersComponent::callcreateProfile($connection, $data);
        return $data;
    }

    
    public static function redeem_temp_wallet($connection,$data){        
        self::refill_user_wallet($connection, $data);
        self::pullback_from_temp_wallet($connection, $data);
    }

    public static function updateTransaction($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $loyaty_trans_category = array('recharge','billing');
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        try {
            if(!isset($data['trans_id']) || !isset($data['status']) || !isset($data['client_req_id']) || !ctype_alnum($data['client_req_id'])){
                $message = Yii::app()->params['message_template']['VALID_PARAM'];
                throw new Exception($message, 580);
            }
            
            $api_version = 0;
            
            $data['status'] = ($data['status'] === 'success') ? 2 : 3;
            $updated_response = json_encode($data);
            // get user and transaction detail
            $response_cond = strlen($data['trans_id']) > 0?"response_id='".$data['trans_id']."'":"";
            $response_cond .= strlen($data['client_req_id']) > 0?(strlen($response_cond)>0?" AND trans_id ='".$data['client_req_id']."'":" trans_id ='".$data['client_req_id']."'"):"";
            $cond = strlen($response_cond)>0?" WHERE ".$response_cond:"";
            $trans_qry ="SELECT * FROM transactions ".$cond." FOR UPDATE";
            $transdata = $connection->createCommand($trans_qry)->queryAll();
            $transObj = $transdata[0];
            if ($transObj['status'] >= 3) {
                throw new Exception("Duplicate update Request",217) ;
            } else {
                $loyaltyFlag = false;
                if(in_array($transObj['trans_category'],$loyaty_trans_category)){
                    $loyaltyFlag = true;
                }
                $tqry = "UPDATE transactions SET status ='".$data['status']."', updated_response='".addslashes($updated_response)."' $cond";                
                $transactData = $connection->createCommand($tqry)->execute();
                if ($data['status'] == 3) {
                    $userProfile_Q = "SELECT api_version FROM user_profile WHERE user_id = ".$transObj['users_id'];
                    $userAPi = $connection->createCommand($userProfile_Q)->queryAll();
                    
                    if(count($userAPi)>0 && isset($userAPi[0]['api_version']) && $userAPi[0]['api_version']=3){
                        $api_version = $userAPi[0]['api_version'];
                    }
                    //get users current balance                                
                    $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id='" . $transObj['users_id'] . "' FOR UPDATE")->queryAll();
                    $walletObj = $walletdata[0];
                    //make entry in transaction table
                    $new_bal = ($walletObj['account_balance'] + $transObj['transaction_amount']);
                    if($loyaltyFlag){
                         $rdata = array('opening_bal'=>$walletObj['account_balance'],'transaction_amount'=>$transObj['transaction_amount'],'closing_bal'=>$new_bal,
                        'trans_category'=>$transObj['trans_category'],'users_id'=>$transObj['users_id'],'trans_type'=>'1','transaction_mode'=>'wallet','trans_datetime'=>GeneralComponent::getFormatedDate(),
                        'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>4,'response_id'=>$data['trans_id'],'ref_trans_id'=>$transObj['trans_id'],'loyalty_points'=> -$transObj['loyalty_points'],'free_points'=> 0);
                     TransactionComponent::newTransaction($connection, $rdata);
                    //refund amount to wallet
                    $connection->createCommand("UPDATE wallets SET account_balance = account_balance + " . $transObj['transaction_amount'] . ",loyalty_points= loyalty_points - ".$transObj['loyalty_points']." WHERE users_id='" . $transObj['users_id'] . "'")->execute();
                    //remove free bie which get ref_transid and delete respctive record from deal_transaction and transaction table
                    }else{
                         $rdata = array('opening_bal'=>$walletObj['account_balance'],'transaction_amount'=>$transObj['transaction_amount'],'closing_bal'=>$new_bal,
                        'trans_category'=>$transObj['trans_category'],'users_id'=>$transObj['users_id'],'trans_type'=>'1','transaction_mode'=>'wallet','trans_datetime'=>GeneralComponent::getFormatedDate(),
                        'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>4,'response_id'=>$data['trans_id'],'ref_trans_id'=>$transObj['trans_id']);
                          TransactionComponent::newTransaction($connection, $rdata);
                        //refund amount to wallet
                        $connection->createCommand("UPDATE wallets SET account_balance = account_balance + " . $transObj['transaction_amount'] ." WHERE users_id='" . $transObj['users_id'] . "'")->execute();
                        //remove free bie which get ref_transid and delete respctive record from deal_transaction and transaction table
                    }
                    $freebie = DealsComponent::delete_freebie($connection,$transObj);
                    if($freebie===true){
                        $type = array("FREEBIE_FAILURE");
                        $user_id = $transObj['users_id'];
                        $n_data = array('user_id'=>$user_id);
                        GeneralComponent::notifier($type, $n_data);
                    } 
                    $data['description'] = isset($data['description'])?$data['description']:"";
                    $operatordata = TelecomComponent::getOperatorByTransaction($transObj['trans_id'], $transObj['trans_category']);                    
                    $operator_id = $operatordata['operator_id'];
                    $product_name = $operatordata['product'];
                    $data['number'] = $operatordata['number'];
                    $loyaltyPoints = $walletObj['loyalty_points']-$transObj['loyalty_points'];
                    $data['operator_name'] = GeneralComponent::getProductDetails($product_name,GeneralComponent::get_plancode($operator_id,$product_name),TRUE);
                    switch ($transObj['trans_category']){
                        case "refill":
                            GeneralComponent::notifier(array('WALLET_TOPUP_REVERSED','FAILED_TRANSACTION'),array('amount'=>$transObj['transaction_amount'],
                                    'txn_id'=>$transObj['trans_id'],'closing'=>$new_bal,'user_id'=>$transObj['users_id'],'errCode'=>$data['err_code'],
                                    'errMsg'=>$data['description'],'operator'=>$data['operator_name'],'balance'=>$new_bal,'number'=>$data['number']));
                        break;
                        case "recharge":
                            $amt = $transObj['transaction_amount'];
                           $arr = array('RECHARGE_FAILURE','FAILED_TRANSACTION');
                            if(isset($api_version) && $api_version==3){
                                 $arr = array('RECHARGE_FAILURE_NEW','FAILED_TRANSACTION_NEW');
                            }
                            GeneralComponent::notifier($arr,array('amount'=>$transObj['transaction_amount'],
                                    'txn_id'=>$transObj['trans_id'],'closing'=>$new_bal,'user_id'=>$transObj['users_id'],'errCode'=>$data['err_code'],
                                    'errMsg'=>$data['description'],'operator'=>$data['operator_name'],'balance'=>$new_bal,'number'=>$data['number'],'loyalty_balance'=>$loyaltyPoints));
                        break;
                        case "billing":
                            $amt = 0;
                            $arr = array('BILLPAYMENT_FAILURE','FAILED_TRANSACTION');
                            if(isset($api_version) && $api_version==3){
                                $arr = array('BILLPAYMENT_FAILURE_NEW','FAILED_TRANSACTION_NEW');
                            }
                            GeneralComponent::notifier($arr,array('amount'=>$transObj['transaction_amount'],
                                    'txn_id'=>$transObj['trans_id'],'closing'=>$new_bal,'user_id'=>$transObj['users_id'],'errCode'=>$data['err_code'],
                                    'errMsg'=>$data['description'],'operator'=>$data['operator_name'],'balance'=>$new_bal,'number'=>$data['number'],'loyalty_balance'=>$loyaltyPoints));
                        break;
                    }
                    
                    //set transaction_flag=0 (failure) in quickPay
                    $data['transaction_flag']=2;
                    $data['operator_id']=$operatordata['operator_id'];
                    $data['amount']=isset($amt)? $amt : 0;
                    $data['users_id']=$transObj['users_id'];
                    ServiceComponent::update_Quickpay_by_param($data,$connection);
                    
                    $cashbackData = array('user_id'=>$transObj['users_id'],'txn_id'=>$transObj['trans_id']);
                    VoucherComponent::revert_cashback($connection,$cashbackData);
                }
                if(isset($amt)){
                    $qdata = array('number'=>$data['number'],'operator_id'=>$operatordata['operator_id'],'user_id'=>$transObj['users_id'],'amount'=>$amt);
                    //$delQuickpay = ServiceComponent::del_Quickpay_by_param($qdata);                
                }
                $transaction->commit();                
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Transaction updated successfully');
                unset($transObj);
            }
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return json_encode($result);
    }

    public static function pullbackTransaction($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        try {
            $message = Yii::app()->params['message_template']['VALID_PARAM'];
            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            $trans_cond = "";
            if(!isset($data['trans_id']) || !is_numeric($data['trans_id']) || !isset($data['client_req_id']) || !ctype_alnum($data['client_req_id']))
                 throw new Exception($message, 580);
            if($data['trans_id'] !=""){
                $trans_cond .= " AND response_id='" . $data['trans_id'] . "' ";
            }
            if($data['client_req_id'] !=""){
                $trans_cond .= " AND trans_id='" . $data['client_req_id'] . "' ";
            }
            
            //$transdata = $connection->createCommand("SELECT * FROM transactions WHERE 1=1 ".$trans_cond." FOR UPDATE")->queryAll();
            $transdata = $connection->createCommand("SELECT * FROM transactions WHERE (response_id='" . $data['trans_id'] . "' "
                                . "OR trans_id='" . $data['client_req_id'] . "') AND response_id != '' FOR UPDATE")->queryAll();
            if(count($transdata)<1){
                throw new Exception("No such transaction", 505);
            }
            $transObj = $transdata[0];
            $data['trans_id'] = $transObj['response_id'];
            $data['client_req_id'] = $transObj['trans_id'];
            
            $transactData = $connection->createCommand("SELECT * FROM transactions WHERE ref_trans_id='" . $data['client_req_id'] . "' "
                            . "AND status='5'")->queryAll();
            if (count($transactData) < 1) {
                //get users current balance                                
                $walletdata = $connection->createCommand("SELECT account_balance FROM wallets WHERE users_id='" . $transObj['users_id'] . "'")->queryAll();
                $walletObj = isset($walletdata[0]) ? $walletdata[0] : "";
                $data['account_balance'] = isset($walletObj['account_balance']) ? $walletObj['account_balance'] : 0;

                if (count($walletdata) > 0) {
                    //check if user wallet have sufficient balance
                    if ($data['account_balance'] < $transObj['transaction_amount']) {
                        throw new Exception("Insufficient Balance", 211);
                    }
                    //deduct amount from wallet
                    $connection->createCommand("UPDATE wallets SET account_balance = account_balance - " . $transObj['transaction_amount'] . " "
                            . "WHERE users_id='" . $transObj['users_id'] . "'")->execute();
                } else {
                    $tmp_wallet = $connection->createCommand("SELECT amount as account_balance FROM temp_wallets WHERE trans_id ='".$data['client_req_id']."'")->queryAll();
                    $data['account_balance'] = $tmp_wallet[0]['account_balance'];
                    self::pullback_from_temp_wallet($connection,$data);
                    $connection->createCommand("DELETE FROM temp_wallets WHERE trans_id ='".$data['client_req_id']."'")->execute();
                }
                $data['new_balance'] = $data['account_balance'] - $transObj['transaction_amount'];
                //make entry in transaction table                    
                $rdata = array('opening_bal'=>$data['account_balance'],'transaction_amount'=>$transObj['transaction_amount'],'closing_bal'=>$data['new_balance'],
                    'trans_category'=>$transObj['trans_category'],'users_id'=>$transObj['users_id'],'trans_type'=>$transObj['trans_type'],'transaction_mode'=>'wallet',
                    'track_id'=>Yii::app()->session['Transaction_trackid'],'status'=>'5','response_id'=>$data['trans_id'],'ref_trans_id'=>$transObj['trans_id'],'trans_datetime'=>GeneralComponent::getFormatedDate());
                TransactionComponent::newTransaction($connection, $rdata);                    
                $transaction->commit();
                $result = array('status' => 'success', 'errCode' => '0', 'description' => 'Transaction updated successfully');                
                //if(!isset($data['system'])){
                    GeneralComponent::notifier(array('WALLET_TOPUP_REVERSED'),array('amount'=>$transObj['transaction_amount'],
                                    'txn_id'=>$transObj['trans_id'],'closing'=>$data['new_balance'],'user_id'=>$transObj['users_id'],'errCode'=>1001,
                                    'balance'=>$data['new_balance']));
                //}
            } else {
                throw new Exception("Duplicate Request", 217);
            }
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return json_encode($result);
    }

    private static function pullback_from_temp_wallet($connection,$data){
        $data['trans_id'] = isset($data['transaction_id'])?$data['transaction_id']:$data['trans_id'];
        $connection->createCommand("DELETE FROM temp_wallets WHERE trans_id ='".$data['trans_id']."'")->execute();
    }
    
    public static function getTempWalletDetail($mobile){
        return Yii::app()->db->createCommand("SELECT * FROM temp_wallets WHERE mobile ='".$mobile."'")->queryAll();
    }
    
    public static function checkWalletBal($id=null){
        $id = is_null($id)?Yii::app()->user->id:$id;
        $result = Yii::app()->db->createCommand("SELECT account_balance as 'balance' FROM wallets where users_id='".$id."'")->queryAll();
        return $result;
    }
    
    public static function dthValidation($data){
        $len = strlen($data['subscriber_id']);
        $params['subId'] = $data['subscriber_id'];
        if($data['operator_id'] == 1 && ($len != 10 || substr($params['subId'],0,2) != "30")){//Airtel DTH
            return array('status'=>'failure','errCode'=>242,'description'=>'Wrong subscriber id');
        }
        else if($data['operator_id'] == 2 && ($len != 12 || substr($params['subId'],0,2) != "20")){//Big TV
            return array('status'=>'failure','errCode'=>242,'description'=>'Wrong subscriber id');
        }
        else if($data['operator_id'] == 3 && ($len != 11)){//Dish TV
            return array('status'=>'failure','errCode'=>242,'description'=>'Wrong subscriber id');
        }
        else if($data['operator_id'] == 4 && ($len != 11 || !in_array(substr($params['subId'],0,1),array(1,4)))){//Sun TV
            return array('status'=>'failure','errCode'=>242,'description'=>'Wrong subscriber id');
        }
        else if($data['operator_id'] == 5 && ($len != 10 || (substr($params['subId'],0,2) != "10" && substr($params['subId'],0,2) != "11"))){//Tata Sky
            return array('status'=>'failure','errCode'=>242,'description'=>'Wrong subscriber id');
        }
        return array('status'=>'success','errCode'=>0,'description'=>'');
    }
    
    public static function refill_loyalty_wallet($data){
      $loyalty_points = Yii::app()->params['GLOBALS']['_New_User_LoyaltyPoints'];
        $userId = isset($data['userid']) ? $data['userid'] : Yii::app()->user->getId();
        Yii::app()->db->createCommand("UPDATE user_profile SET api_version=3 WHERE user_id=$userId")->execute();
        $query=" UPDATE wallets SET loyalty_points=loyalty_points+".$loyalty_points." WHERE users_id=".$userId;
        Yii::app()->db->createCommand($query)->execute();
    }
    
    public static function Getuserpoints($data){
        $userid = empty($data['id']) ? Yii::app()->user->id : $data['id'];
         $Loyalty_coin = 0;
        $status = 0;
        $deal = array();
        $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
        $result = Yii::app()->db->createCommand("SELECT (CASE WHEN status=2 THEN 'success' ELSE 'fail' END) as 'status',transaction_amount,trans_category,loyalty_points,free_points FROM transactions where transaction_mode!='loyaltypoints' ORDER BY `trans_id` DESC  LIMIT 0,1 ")->queryAll();
        if(count($result)>0 && $result[0]['free_points']==1){
            $Loyalty_coin = $result[0]['loyalty_points'];
            $status = $result[0]['status'];
        }
        if(isset($data['voucher'])){
            $deal = Yii::app()->db->createCommand("SELECT dt.expiry_date, c.code,c.pin,t.loyalty_points
                    FROM  `deal_transaction` AS dt
                    INNER JOIN coupons AS c ON ( c.id = dt.coupon_id ) 
                    INNER JOIN transactions as t ON (t.trans_id =dt.transaction_id)
                    WHERE  t.ref_trans_id =". $data['txnid']." order by t.trans_id DESC")->queryRow();
        }
        
        $result1 = Yii::app()->db->createCommand("SELECT *,$Loyalty_coin as 'points' FROM wallets WHERE users_id ='" . $userid . "'")->queryAll();
            if(count($result1) >0){
                 $data = $result1[0];
                if(isset($deal['code'])){
                    $data['code'] = $deal['code'];
                    $data['pin'] = isset($deal['pin']) ? $deal['pin'] : "" ;
                    $data['expiry_date'] = $deal['expiry_date'];
                    $data['status'] = $result[0]['status'];
                    $data['loyalty_points'] = $deal['loyalty_points'];
                }
               return array('status'=>'success','errCode'=>'0','description'=>$data);
            }
            else{
                return array('status'=>'error','errCode'=>'0','description'=>'No User Details');
            }
    }
    
    public static function getTrans_loyaltyDetail($data){
        $userid = empty($data['id']) ? Yii::app()->user->id : $data['id'];
        if(isset($data['voucher'])){
            return self::Getuserpoints($data);
        }
        $txnid = $data['txnid'];
        $transType = Yii::app()->db->createCommand("SELECT trans_category FROM  `transactions`  where trans_id =$txnid")->queryRow();
        $data['type'] = $transType['trans_category'];
        
        $cond=" trans_id  = $txnid " ;
        if(isset($data['type']) && $data['type']=='refill'){
            $cond=" trans_id  >$txnid  AND users_id=$userid ";
        }
        $Loyalty_coin = 0;
        $status = 0;
        $deal = array();
        $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
        $result = Yii::app()->db->createCommand("SELECT (CASE WHEN status=2 THEN 'success' ELSE 'fail' END) as 'status',transaction_amount,trans_category,loyalty_points,free_points FROM transactions WHERE $cond  LIMIT 0,1")->queryAll();
        if(count($result)>0 && $result[0]['free_points']==1){
            $Loyalty_coin = $result[0]['loyalty_points'];
            $status = $result[0]['status'];
        }
        
        $result1 = Yii::app()->db->createCommand("SELECT *,$Loyalty_coin as 'points' FROM wallets WHERE users_id ='" . $userid . "'")->queryAll();
            if(count($result1) >0){
                 $data = $result1[0];
               return array('status'=>'success','errCode'=>'0','description'=>$data);
            }
            else{
                return array('status'=>'error','errCode'=>'0','description'=>'No User Details');
            }
    }
}

