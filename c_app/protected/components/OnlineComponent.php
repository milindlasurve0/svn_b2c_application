<?php

class OnlineComponent {
    /*
     * This the entry point for online payment. It collect all the data required to process payment online
     * @return : array
     */

    public static function OnlinePaymentProcessor($data_rev) {
        $data_rev['amount'] = isset($data_rev['amount']) ? $data_rev['amount'] : 0;
        try {
            if (isset($data_rev['quick_pay_flag']) && $data_rev['quick_pay_flag'] == TRUE) {
                $add_to_quick_pay = ServiceComponent::is_AlreadyQuickpay($data_rev) ? "" : ServiceComponent::addQuickpay($data_rev);
            }
            $connection = Yii::app()->db;
            $data_rev['userid'] = Yii::app()->user->id;
            $data_rev['trans_category'] = isset($data_rev['trans_category']) ? $data_rev['trans_category'] : WalletComponent::getTransactionCategory($data_rev);
            if (!isset($data_rev['transaction_id'])) {
                $recharge_flag = isset($data_rev['recharge_flag']) ? $data_rev['recharge_flag'] : 0;
                $data = array('opening_bal' => 0, 'transaction_amount' => $data_rev['amount'], 'closing_bal' => '0', 'recharge_flag' => $recharge_flag,
                        'trans_category' => $data_rev['trans_category'], 'users_id' => $data_rev['userid'], 'trans_type' => '0', 'transaction_mode' => $data_rev['paymentopt'],
                        'track_id' => Yii::app()->session['Transaction_trackid'], 'status' => 1);
                TransactionComponent::newTransaction($connection, $data);
                $data_rev['transaction_id'] = $connection->lastInsertID;
                TransactionComponent::updateTransaction($connection, $data_rev['transaction_id'], array('ref_trans_id' => $data_rev['transaction_id']));
            }
            switch ($data_rev['trans_category']) {
                case "recharge":
                    $response_detail = self::recharge($connection, $data_rev);
                    break;
                case "billing":
                    $response_detail = self::billpayment($connection, $data_rev);
                    break;
                case "deal":
                    $response_detail = self::dealpayment($connection, $data_rev);
                    break;
                case "refill":
                    $response_detail = self::walletRefill($connection, $data_rev);
                    break;
            }
            $response_detail['userid'] = $data_rev['userid'];
            $response_detail['mobile'] = Yii::app()->user->username;
            //$response_detail['mobile'] = Yii::app()->user->username;
            if (!isset($response_detail['return_url']) && isset($data_rev['return_url'])) {
                $response_detail['return_url'] = $data_rev['return_url'];
            }
            $response_detail['trans_category'] = $data_rev['trans_category'];
            $response_detail['intime'] = time();
            if(isset($data_rev['api_version']) && $data_rev['api_version']==3 && ($data_rev['trans_category']!='refill' ||  $data_rev['trans_category']!='deal')){
                $response_detail['api_version'] = $data_rev['api_version'];
                $response_detail['flag'] = isset($data_rev['flag']) ? $data_rev['flag'] : "";
             //   self::setLoyaltyAmount($data_rev);
            }
            Yii::app()->cache->hset('transdata', $data_rev['transaction_id'], json_encode($response_detail));
           // file_put_contents("/tmp/transaction", "|".  json_encode($data_rev)." \n", FILE_APPEND | LOCK_EX);
            $pg_form_data = self::generatePayment_form($data_rev);
            $description = array('txnid' => $data_rev['transaction_id'], 'form_content' => $pg_form_data);
            if(isset($data_rev['returnparam']) && !empty($data_rev['returnparam'])){
               $description = array('txnid' => $data_rev['transaction_id'], 'form_content' => $pg_form_data, 'online_form'=>true);
            }
            
                if(isset($data_rev['cb_code']) && !empty($data_rev['cb_code'])){
                    //give cashback
                     $cashback = array('code'=>$data_rev['cb_code'],'transaction_id'=>$data_rev['transaction_id'],'recharge_amount'=>$data_rev['amount']);
                    VoucherComponent::user_cashback($cashback);
                }
                
            $result = array('status' => 'success', 'errCode' => 0, 'description' => $description);
        } catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result;
    }
    
    /*
     * This return array of data require to do online recharge
     * @return : array
     */

    public static function recharge($connection, $data_rev) {
        TransactionComponent::RechargeTransaction($connection, $data_rev);
        $params = array('number' => $data_rev[$data_rev['recharge_column']], 'product' => $data_rev['product'], 'amount' => $data_rev['amount'], 'userid' => $data_rev['userid'], 'trans_category' => $data_rev['trans_category'],
                'trans_id' => $data_rev['transaction_id'], 'operator' => GeneralComponent::get_plancode($data_rev['operator_id'], $data_rev['product']), 'stv' => $data_rev['stv']);
        return $params;
    }

    /*
     * This return array of data require to do online bill payment
     * @return : array
     */

    public static function billpayment($connection, $data_rev) {
        TransactionComponent::BillingTransaction($connection, $data_rev);
        $params = array('number' => $data_rev[$data_rev['recharge_column']], 'product' => $data_rev['product'], 'amount' => $data_rev['base_amount'], 'userid' => $data_rev['userid'], 'trans_category' => $data_rev['trans_category'],
                'trans_id' => $data_rev['transaction_id'], 'operator' => GeneralComponent::get_plancode($data_rev['operator_id'], $data_rev['product']), 'stv' => $data_rev['stv']);
        return $params;
    }

    /*
     * This return array of data require to do online deal purchase
     * @return : array
     */

    public static function dealpayment($connection, $data_rev) {
        TransactionComponent::DealTransaction($connection, $data_rev);

        $params = array('deal_id' => $data_rev['deal_id'], 'offer_id' => $data_rev['offer_id'], 'coupon_id' => $data_rev['coupon_id'],
                'amount' => $data_rev['amount'], 'quantity' => $data_rev['quantity'], 'trans_category' => $data_rev['trans_category'],
                'userid' => $data_rev['userid'], 'new_bal' => $data_rev['new_bal'], 'transaction_id' => $data_rev['transaction_id']);
        return $params;
    }

    /*
     * This return array of data require to do online wallet top-up
     * @return : array
     */

    public static function walletRefill($connection, $data_rev) {
        $params = $data_rev;
        return $params;
    }

    /*
     * This verify the hash received in response of payu
     * @return : boolean
     */

    public static function check_payu_hash($data) {
        $payu_detail = self::payu_getconfig();
        $key = $payu_detail['KEY'];
        $salt = $payu_detail['SALT'];
        $status = GeneralComponent::set_defaultBlank($data, 'status');
        $email = GeneralComponent::set_defaultBlank($data, 'email');
        $firstname = GeneralComponent::set_defaultBlank($data, 'firstname');
        $productinfo = GeneralComponent::set_defaultBlank($data, 'productinfo');
        $amount = GeneralComponent::set_defaultBlank($data, 'amount');
        $txnid = GeneralComponent::set_defaultBlank($data, 'txnid');
        $dstr = $salt . "|" . $status . "|||||||||||" . $email . "|" . $firstname . "|" . $productinfo . "|" . $amount . "|" . $txnid . "|" . $key;
        //$dstr = $salt."|".$data['status']."|||||||||||".$data['email']."|".$data['firstname']."|".$data['productinfo']."|".$data['amount']."|".$data['txnid']."|".$key;        
        $internal_hash = hash("sha512", $dstr);
        if ($internal_hash == $data['hash']) {
            return TRUE;
        }
        return FALSE;
    }

    /*
     * This function is use to add payu txn detail in payu table and pass detail to further redirecting to processing function
     * On the basis of category it calls (process_deal_after_pg, process_refill_after_pg, process_transaction_after_pg)
     */

    public static function update_pg_payu($data, $savedetail = TRUE) {
        $connection = Yii::app()->db;
        if ($savedetail) {
            if (!isset($data['offline']) && !self::check_payu_hash($data)) {
                $t_data = $data;
                $t_data['ip'] = $_SERVER['REMOTE_ADDR'];
                GeneralComponent::sendMail("tadka@mindsarray.com,nandan@mindsarray.com", "wrong hash for this transaction please check it", json_encode($t_data));
                exit();
            }
            unset($data['offline']);
            $data['cardnum'] = isset($data['card_no']) ? $data['card_no'] : (isset($data['cardnum']) ? $data['cardnum'] : "");
            $data['error'] = isset($data['error_code']) ? $data['error_code'] : (isset($data['error']) ? $data['error'] : "");
            unset($data['card_no']);
            unset($data['error_code']);
            $colqry = "SELECT COLUMN_NAME  FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='b2c_app1' AND `TABLE_NAME`='pg_payuIndia'";
            $col_result = $connection->createCommand($colqry)->queryall();
            $col_data = array();
            foreach ($col_result as $k => $v) {
                $col_data[$v['COLUMN_NAME']] = isset($data[$v['COLUMN_NAME']]) ? $data[$v['COLUMN_NAME']] : "";
            }
            $addition_var = array_diff($data, $col_data);
            if (count($addition_var) > 0) {
                GeneralComponent::sendMail("nandan@mindsarray.com", "addition data in response from payu", json_encode($addition_var));
            }
            $tablename = "pg_payuIndia";
            $payuQry = "select txnid,status from " . $tablename . " where txnid='" . $col_data['txnid'] . "'";
            $payu_result = $connection->createCommand($payuQry)->queryall();
            if (count($payu_result) < 1) {
                TransactionComponent::addData($connection, $tablename, $col_data);
            } elseif ($payu_result[0]['status'] === 'pending') {
                unset($col_data['id']);
                TransactionComponent::update_payu_Transaction($connection, $col_data['txnid'], $col_data);
                $transdata = json_decode(yii::app()->cache->hget('transdata', $data['txnid']), TRUE);
                if(isset($transdata['returnparam']) && !empty($transdata['returnparam'])){
                     $hashResult = $transdata;
                     unset($hashResult['returnparam']);
                     if(Yii::app()->cache->hexists('transdata', $data['txnid'])){
                        Yii::app()->cache->hdel('transdata', $data['txnid']);
                        Yii::app()->cache->hset('transdata', $hashResult['transaction_id'], json_encode($hashResult));
                     }
                    // self::callback_processpayment($transdata);
                }
            }
        }   
        if ($data['status'] === "success") {
            $transdata = json_decode(yii::app()->cache->hget('transdata', $data['txnid']), TRUE);
           // file_put_contents("/var/log/apps/txn.log", "| ".Yii::app()->user->getId()." |".  json_encode($transdata)." \n", FILE_APPEND | LOCK_EX);
             $data['category'] = isset($data['returnparam']['category']) ? $data['returnparam']['category'] :"";
            if(empty($transdata) && isset($data['txnid']) && !empty($data['txnid'])){
                self::setRedisTransData($data['txnid']);
            }
            if ($transdata['trans_category'] === 'deal') {
                $result = self::process_deal_after_pg($data);
            } elseif ($transdata['trans_category'] === 'refill') {
                $result = self::process_refill_after_pg($data);
            } else {
                self::process_transaction_after_pg($data);
                $result = json_encode(array('status' => 'success', 'errCode' => 0, 'description' => 'successfull'));
            }
            if(isset($transdata['returnparam']) && !empty($transdata['returnparam'])){
                $hashResult = $transdata;
                unset($hashResult['returnparam']);
                if(Yii::app()->cache->hexists('transdata', $data['txnid'])){
                   Yii::app()->cache->hdel('transdata', $data['txnid']);
                   Yii::app()->cache->hset('transdata', $hashResult['transaction_id'], json_encode($hashResult));
                }
                return self::callback_processpayment($transdata);
            }
            return $result;
        }
        if ($data['status'] === "failure") {
            TransactionComponent::updateTransaction($connection, $data['txnid'], array('status' => 3));
            $transdata = json_decode(yii::app()->cache->hget('transdata', $data['txnid']), TRUE);
            //file_put_contents("/var/log/apps/txn_failure.log", "| ".Yii::app()->user->getId()." |".  json_encode($transdata)." \n", FILE_APPEND | LOCK_EX);
            if(isset($transdata['returnparam']) && !empty($transdata['returnparam'])){
                $hashResult = $transdata;
                unset($hashResult['returnparam']);
                if(Yii::app()->cache->hexists('transdata', $data['txnid'])){
                   Yii::app()->cache->hdel('transdata', $data['txnid']);
                   Yii::app()->cache->hset('transdata', $hashResult['transaction_id'], json_encode($hashResult));
                }
                //return self::callback_processpayment($transdata);
            }
            if(isset($transdata['return_url']) && !empty($transdata['return_url'])){
                 header("location: http://pay1.in/home" );
                exit();
            }

//            if(isset($data['txnid'])){
//               if(Yii::app()->cache->hexists('transdata', $data['txnid'])){
//                    Yii::app()->cache->hdel('transdata', $data['txnid']);
//                }
//            }
        }
    }
    
    public static function callback_processpayment($data){
        $data['return_url'] = isset($data['returnparam']['return_url']) ? $data['returnparam']['return_url'] : 0;
        $data['returnparam'] = array_merge($data['returnparam'],array('returnparam'=>1,'responce_transaction_id'=>$data['transaction_id']));
        $responce =  WalletComponent::Processpayments($data['returnparam']);
       // file_put_contents("/var/log/apps/txn.log", "| step3 responce  "." |".  json_encode($responce)." \n", FILE_APPEND | LOCK_EX);
        if (isset($data['return_url']) && strlen(trim($data['return_url'])) != 0 && !empty($data['return_url'])) {
           // file_put_contents("/var/log/apps/txn.log", "| step4  "." |".  json_encode($data)." \n", FILE_APPEND | LOCK_EX);
              $responce = json_decode($responce,true);
              if($responce['status']=='success' && $data['returnparam']['trans_category']=='deal'){
                   $transdata = array('txnid'=>$data['transaction_id'],'voucher'=>1);
                   $result = WalletComponent::getTrans_WalletDetail($transdata);
                   $expiry_date = $result['description']['expiry_date'];
                   $code = $result['description']['code'];
                   $pin = $result['description']['pin'];
                   $web_url = strstr($data['return_url'],'/giftstore');
                   $date = new DateTime($expiry_date);
                   $expiry_date =  $date->format('Ymd');
                   //$data['return_url'] = substr($data['return_url'],0,strpos($data['return_url'],'/giftdetails'));
                   $url = $data['return_url'] . "/success/?status=" . $responce['status'] . "&expiry_date=" . $expiry_date . "&code=".$code."&pin=".$pin."&web_url=".$web_url;
              }
              else if($responce['status']=='success' && $data['returnparam']['category']==''){
                  $transdata = array('txnid'=>$data['transaction_id']);
                  $result = WalletComponent::getTrans_WalletDetail($transdata);
                  //file_put_contents("/var/log/apps/txn.log", "| step4  "." |".  json_encode($result)." \n", FILE_APPEND | LOCK_EX);
                  $url = "http://".$data['return_url'] . "?status=" . $result['status'] . "&txnid=" . $data['transaction_id'] . "&closing=" . $result['description']['account_balance'];
              }
              else{
                  if(array_key_exists('transaction_id', $data))
                         $url = "http://".$data['return_url'] . "?status=failure&txnid=" . $data['transaction_id'];//$data['return_url'] . "/success/?status=failure"; 
                  else
                   $url = "http://".$data['return_url'] . "?status=failure";//$data['return_url'] . "/success/?status=failure";
              }
              //file_put_contents("/var/log/apps/txn.log", "| step3  "." |".  $url." \n", FILE_APPEND | LOCK_EX);
              $url = str_replace('http//', '', $url);
            header("location:" .$url );
            exit();
        }
        return $responce;
    }
    
    /*
     * It display the status of online transaction from payu
     * @return : array
     */

    public static function payu_id_details($data) {
        $data['service_type'] = isset($data['service_type']) ? $data['service_type'] : 'verify_payment';
        $payu_detail = self::payu_getconfig();
        $var1 = implode('|', (explode(',', $data['txnid'])));
        $service_api_data = array('var1' => $var1, 'command' => $data['service_type'], 'key' => $payu_detail['KEY']);
        $hash = hash("sha512", $payu_detail['KEY'] . "|" . $service_api_data['command'] . "|" . $service_api_data['var1'] . "|" . $payu_detail['SALT']);
        $service_api_data = array_merge($service_api_data, array('hash' => $hash));
        try {
            $result_data = unserialize(GeneralComponent::curl($payu_detail['SERVICE_URL'], $service_api_data, 'post'));
        } catch (Exception $e) {
            $result_data = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage());
        }
        return $result_data;
    }

    /*
     * It will call payu web service on the bases of command that will be in service_type param
     * @input : service_type 
     * @return : array
     */

    public static function call_payu_webservices($data = array(),$flag = TRUE) {
        $connection = Yii::app()->db;
        $data['service_type'] = isset($data['service_type']) ? $data['service_type'] : '';
        $data['payu_detail'] = self::payu_getconfig();
        $data['connection'] = Yii::app()->db;
        switch ($data['service_type']) {
            case 'refund_payu_txn':
                $data['service_type'] = "cancel_refund_transaction";
                $result = self::refund_payu_txn($data,$flag);
                break;
        }
        return $result;
    }

    /*
     * It call payu webservice for refund
     * @mandatory params: txn_id,connection (connection obj),payu_detail(array),service_type
     * @return : array
     */

    public static function refund_payu_txn($data = array(), $timeflag = TRUE) {
        try {
            $txn_id = isset($data['txn_id']) ? $data['txn_id'] : null;
            $logger = isset($data['loggerObj'])?$data['loggerObj']:"";
            $track_id = isset($data['request_track_id'])?$data['request_track_id']:"";
            $qry = "SELECT txn.trans_id,pg.status as payu_status,max(txn.status) as pay1_status,txn.users_id as user_id, pg.pg_type, "
                    . "wlt.account_balance as wallet_balance, txn.transaction_amount as amount, pg.mihpayid, pg.unmappedstatus,txn.trans_datetime "
                    . "FROM `transactions` as txn "
                    . "INNER JOIN pg_payuIndia as pg ON txn.ref_trans_id = pg.txnid "
                    . "INNER JOIN wallets as wlt ON txn.users_id=wlt.users_id "
                    . "WHERE txn.`ref_trans_id` = '$txn_id' AND wlt.account_balance >= wlt.account_balance";
            is_object($logger)?$logger->info(($data['request_track_id']) . "Query : " . $qry):"";
            $connection = $data['connection'];
            $txn_result = $connection->createCommand($qry)->queryRow();
            $txn_result_cnt = trim(implode($txn_result),",");
            if (strlen($txn_result_cnt) >= 1) {
                $additional_cond = (1 == 1);
                if ($timeflag) {
                    $time_gap = strtotime($txn_result['trans_datetime']) - time();
                    if ($time_gap > 60 * 60 * 24) {
                        throw new Exception("Refund is not allowed due to more time gap", 268);
                    }
                    if(self::is_black_users($txn_result)){
                        is_object($logger)?$logger->info(($data['request_track_id']) . "This is a blocked user "):"";
                        throw new Exception("Please contact pay1 support", 264);
                    }                    
                    $additional_cond = ($txn_result['pay1_status'] == 4);
                }
                is_object($logger)?$logger->info(($data['request_track_id']) . "Result from Query : " . json_encode($txn_result)):"";
                if ($txn_result['payu_status'] == "success" && $additional_cond && ($txn_result['wallet_balance'] >= $txn_result['amount'])) {
                    is_object($logger)?$logger->info(($data['request_track_id']) . "Response from payu : " . " checked payu and pay1 status "):"";
                    if (self::allowed_bank_for_refund($txn_result) && strtolower($txn_result['unmappedstatus']) == "captured") {
                        $uniq_id = isset($data['request_track_id']) ? $data['request_track_id']:  GeneralComponent::get_unique_trackId();
                        $service_api_data = array('var1' => $txn_result['mihpayid'], 'var2' => $uniq_id, 'var3' => $txn_result['amount'], 'command' => $data['service_type'], 'key' => $data['payu_detail']['KEY']);
                        is_object($logger)?$logger->info(($data['request_track_id']) . "Service api params : " . json_encode($service_api_data)):"";
                        $hash = hash("sha512", $data['payu_detail']['KEY'] . "|" . $service_api_data['command'] . "|" . $service_api_data['var1'] . "|"  . $data['payu_detail']['SALT']);
                        $service_api_data = array_merge($service_api_data, array('hash' => $hash));
                        $result_data = unserialize(GeneralComponent::curl($data['payu_detail']['SERVICE_URL'], $service_api_data, 'post'));
                        is_object($logger)?$logger->info(($data['request_track_id']) . "Response from payu : " . json_encode($result_data)):"";
                        if ($result_data['status'] == 1) {
                            try {
                                $request_id = isset($result_data['request_id']) ? $result_data['request_id'] : "";
                                $transaction = $connection->beginTransaction();
                                $trans_qry = "SELECT * FROM transactions WHERE trans_id = '$txn_id'";
                                $transdata = $connection->createCommand($trans_qry)->queryAll();
                                $transObj = $transdata[0]; //------ transaction detail
                                $wallet_qry = "SELECT * FROM wallets WHERE users_id =" . $transObj['users_id'] . " FOR UPDATE";
                                $wallet_data = $connection->createCommand($wallet_qry)->queryAll();
                                $walletObj = $wallet_data[0]; //---- wallet detail
                                $new_bal = ($walletObj['account_balance'] - $transObj['transaction_amount']);
                                $rdata = array('opening_bal' => $walletObj['account_balance'], 'transaction_amount' => $transObj['transaction_amount'], 'closing_bal' => $new_bal,
                                        'trans_category' => $transObj['trans_category'], 'users_id' => $transObj['users_id'], 'trans_type' => '0', 'transaction_mode' => 'wallet', 'trans_datetime' => GeneralComponent::getFormatedDate(),
                                        'track_id' => Yii::app()->session['Transaction_trackid'], 'status' => 7, 'response_id' => $request_id, 'response' => json_encode($result_data), 'ref_trans_id' => $txn_id);
                                TransactionComponent::newTransaction($connection, $rdata);
                                $connection->createCommand("UPDATE wallets SET account_balance = account_balance - " . $transObj['transaction_amount'] . " WHERE users_id='" . $transObj['users_id'] . "'")->execute();
                                $transaction->commit();
                                GeneralComponent::sendMail("tadka@mindsarray.com,sunilc@mindsarray.com,nandan@mindsarray.com", "transaction refunded via payu", json_encode($result_data));
                                return array('status' => 'success', 'errorCode' => 0, 'description' => array('message' => $result_data['msg']));
                            } catch (Exception $ex) {
                                $transaction->rollback();
                                throw new Exception("Failed to refund via payment gateway", 266);
                            }
                        } else {
                            throw new Exception($result_data['msg'], 267);
                        }
                    } else {
                        is_object($logger)?$logger->info(($data['request_track_id']) . "Issue with bank list  "):"";
                        throw new Exception("Please contact pay1 support", 264);
                    }
                } else {
                    throw new Exception("Please contact pay1 support", 264);
                }
            } else {
                throw new Exception("No transaction found for refund", 265);
            }
        } catch (Exception $ex) {
            return array('status' => 'failure', 'errorCode' => $ex->getCode(), 'description' => array('message' => $ex->getMessage()));
        }
    }
    
    public static function refund_refill_transaction($data = array()){
       try{
             $txnId = $data['txn_id'];
           // $logger = isset($data['loggerObj'])?$data['loggerObj']:"";
            $connection = Yii::app()->db;
             $query="SELECT txn.trans_category,p.mihpayid,txn.trans_id,txn.users_id,p.amount
                FROM transactions as txn 
                INNER JOIN pg_payuIndia as p ON (txn.ref_trans_id=p.txnid)
                INNER JOIN wallets as w ON (w.users_id = txn.users_id)
                where p.unmappedstatus='captured' AND ((txn.trans_category='refill' AND txn.status=2) OR (txn.trans_category='recharge' AND txn.status=4))
                AND txn.ref_trans_id=$txnId "
               . "ORDER BY txn.trans_id DESC ";
                $result = $connection->createCommand($query)->queryAll();
            if(count($result)>0){
                foreach($result as $row){
                    $userId = $row['users_id'];
                    $amount = $row['amount'];
                    $mihpayid = $row['mihpayid'];
                    $txnId = $row['trans_id'];
                    $trans_category = $row['trans_category'];
                    $transaction = $connection->beginTransaction();
                    $wallet_qry = "SELECT * FROM wallets WHERE users_id =$userId FOR UPDATE";
                    $wallet_data = $connection->createCommand($wallet_qry)->queryAll();
                    if($amount>$wallet_data[0]['account_balance']){
                        //file_put_contents("/tmp/refund_refill_transaction", "| : ".json_encode($row)."\n", FILE_APPEND | LOCK_EX);
                        continue;
                    }
                    $new_bal = ($wallet_data[0]['account_balance'] - $amount);

                    $rdata = array('opening_bal' => $wallet_data[0]['account_balance'], 'transaction_amount' => $amount, 'closing_bal' => $new_bal,
                            'trans_category' => $trans_category, 'users_id' => $userId, 'trans_type' => '0', 'transaction_mode' => 'wallet', 'trans_datetime' => GeneralComponent::getFormatedDate(),
                            'track_id' => "", 'status' => 7, 'response_id' => "", 'response' => '', 'ref_trans_id' => $txnId);
                    TransactionComponent::newTransaction($connection, $rdata);
                    $connection->createCommand("UPDATE wallets SET account_balance = account_balance - $amount WHERE users_id=$userId")->execute();
                    $transaction->commit();
                }
                
                return array('status' => 'success', 'errorCode' => 0, 'description' => array('message' => 'amount pull back successfully'));         
               
            }
       } catch (Exception $ex) {
           $transaction->rollback();
           $message = Yii::app()->params['message_template']['EXCEPTION'];
            return array('status' => 'error', 'errorCode' => $ex->getCode(), 'description' => array('message' =>$message));         
       }
    }
    /*
     * It call payu webservice for refund
     * @mandatory params: txn_id,payu_detail(array),service_type(API)
     * @return : array
     */
    public static function refund_payu_transaction($data = array()){
       try{
            $txnId = $data['txn_id'];
           // $logger = isset($data['loggerObj'])?$data['loggerObj']:"";
            $connection = Yii::app()->db;
             $query="SELECT txn.trans_category,p.mihpayid,txn.trans_id,txn.users_id,p.amount
                FROM transactions as txn 
                INNER JOIN pg_payuIndia as p ON (txn.ref_trans_id=p.txnid)
                INNER JOIN wallets as w ON (w.users_id = txn.users_id)
                where p.unmappedstatus='captured' AND ((txn.trans_category='recharge' AND txn.status=4) OR (txn.trans_category='refill' AND txn.status=2))
                AND txn.ref_trans_id=$txnId AND w.account_balance>=p.amount "
               . "ORDER BY txn.trans_id DESC ";
            $result = $connection->createCommand($query)->queryAll();
        if(count($result)>0){
            $payu_detail = self::payu_getconfig();
            $userId = $result[0]['users_id'];
            $amount = $result[0]['amount'];
            $mihpayid = $result[0]['mihpayid'];
            $txnId = $result[0]['trans_id'];
            $trans_category = $result[0]['trans_category'];
            $command = "cancel_refund_transaction";
            
            $uniq_id = GeneralComponent::get_unique_trackId();
            $service_api_data = array('var1' => $mihpayid, 'var2' => $uniq_id, 'var3' => $amount, 'command' => $command, 'key' => $payu_detail['KEY']);
            // is_object($logger)?$logger->info(($data['request_track_id']) . "Service api params : " . json_encode($service_api_data)):"";
            $hash = hash("sha512", $payu_detail['KEY'] . "|" . $service_api_data['command'] . "|" . $service_api_data['var1'] . "|"  . $payu_detail['SALT']);
            $service_api_data = array_merge($service_api_data, array('hash' => $hash));
                    
            $result_data = unserialize(GeneralComponent::curl($payu_detail['SERVICE_URL'], $service_api_data, 'post'));
             //is_object($logger)?$logger->info($uniq_id . "Response from payu : " . json_encode($result_data)):"";
             if ($result_data['status'] == 1) {
                 try{
                        $transaction = $connection->beginTransaction();
                        $wallet_qry = "SELECT * FROM wallets WHERE users_id =$userId FOR UPDATE";
                       $wallet_data = $connection->createCommand($wallet_qry)->queryAll();
                       $new_bal = ($wallet_data[0]['account_balance'] - $amount);
                       $rdata = array('opening_bal' => $wallet_data[0]['account_balance'], 'transaction_amount' => $amount, 'closing_bal' => $new_bal,
                               'trans_category' => $trans_category, 'users_id' => $userId, 'trans_type' => '0', 'transaction_mode' => 'wallet', 'trans_datetime' => GeneralComponent::getFormatedDate(),
                               'track_id' => "", 'status' => 5, 'response_id' => "", 'response' => json_encode($result_data), 'ref_trans_id' => $txnId);
                       TransactionComponent::newTransaction($connection, $rdata);
                       $connection->createCommand("UPDATE wallets SET account_balance = account_balance - $amount WHERE users_id=$userId")->execute();
                       $transaction->commit();
                       return array('status' => 'success', 'errorCode' => 0, 'description' => array('message' => $result_data['msg']));         
                       
                 }  catch (Exception $ex){
                     $transaction->rollback();
                     throw new Exception("Failed to refund via payment gateway", 266);
                 }
                   }
            else{
                throw new Exception($result_data['msg'], 267);
            }
        }
        else {
                throw new Exception("No transaction found for refund", 265);
            }
            
        } catch (Exception $ex) {
           return array('status' => 'failure', 'errorCode' => $ex->getCode(), 'description' => array('message' => $ex->getMessage()));
       }
    }
    
     /*
     * This function refund pullback amount (by mistake)
     * @mandatory params: txn_id
     * @return : array
     */
    public static function pullback_refund($data){
        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        try{
            if(!isset($data['trans_id']) || empty($data['trans_id']))
                throw new Exception("Please provide transaction id");
            
            $txn_id = $data['trans_id'];
            
             $query="SELECT users_id,transaction_amount,trans_id
                FROM transactions 
                WHERE trans_id = $txn_id AND status=5";
             //refill
            $result = $connection->createCommand($query)->queryRow();
            
            if(isset($result['users_id']) && !empty($result['users_id'])){
                $userId = $result['users_id'];
                    $wallet_qry = "SELECT * FROM wallets WHERE users_id =$userId FOR UPDATE";
                    $wallet_data = $connection->createCommand($wallet_qry)->queryRow();
                    $amount = $result['transaction_amount'];
                    $opening_bal = $wallet_data['account_balance'];
                    $new_bal = ($opening_bal + $amount);
                    $rdata = array('opening_bal' => $opening_bal, 'transaction_amount' => $amount, 'closing_bal' => $new_bal,
                            'trans_category' => 'refund', 'users_id' => $userId, 'trans_type' => '1', 'transaction_mode' => 'wallet', 'trans_datetime' => GeneralComponent::getFormatedDate(),
                            'track_id' => "", 'status' => 4, 'response_id' => "", 'response' => '', 'ref_trans_id' => $txn_id);
                    TransactionComponent::newTransaction($connection, $rdata);
                    $connection->createCommand("UPDATE wallets SET account_balance = account_balance + $amount WHERE users_id=$userId")->execute();
                    $transaction->commit();
                    return array('status' => 'success', 'errorCode' => 0, 'description' => array('message' => 'Pullback reversed successfully'));         
               }
            
        }
        catch(Exception $ex){
            $transaction->rollback();
            return array('status' => 'failure', 'errorCode' => $ex->getCode(), 'description' => array('message' => $ex->getMessage()));
        }
    }


    /*
     * This function will return whether refund to be allowed or not for a particular bank
     * @return boolean
     */

    public static function allowed_bank_for_refund($data = array()) {
        $list_of_allowed_banks = array('SBIPG', 'SBINB');
        $pg_type = isset($data['pg_type']) ? $data['pg_type'] : null;
        return in_array($pg_type, $list_of_allowed_banks);
    }

    /*
     * This will return whether user is blacklisted or not
     * @return boolean
     */

    public static function is_black_users($data = array()) {
        $user_id = isset($data['user_id']) ? $data['user_id'] : null;
        $data_black_count = UsersComponent::BlackListedUsers($user_id);
        return (count($data_black_count) > 0);
    }

    /*
     * This function is to get status of a transaction from payu by calling their web service
     * 
     */

    public static function payu_get_txn_detail($data) {
        $connection = Yii::app()->db;
        $data['service_type'] = isset($data['service_type']) ? $data['service_type'] : '';
        $payu_detail = self::payu_getconfig();
        $connection = Yii::app()->db;
        switch ($data['service_type']) {
            case 'verify_payment':
                if (trim(strtolower($data['txnid'])) == "all") {
                    $txnqry = "SELECT group_concat( trans_id order by rand() ) AS txnid FROM `transactions` WHERE STATUS =1 and ((UNIX_TIMESTAMP('" . date('Y-m-d H:i:s') . "') - UNIX_TIMESTAMP(trans_datetime))/60) > 10";
                    $txn_result = $connection->createCommand($txnqry)->queryRow();
                    $data['txnid'] = $txn_result['txnid'];
                    if ($data['txnid'] == "") {
                        exit();
                    }
                }
                $var1 = implode('|', (explode(',', $data['txnid'])));
                $service_api_data = array('var1' => $var1, 'command' => $data['service_type'], 'key' => $payu_detail['KEY']);
                $hash = hash("sha512", $payu_detail['KEY'] . "|" . $service_api_data['command'] . "|" . $service_api_data['var1'] . "|" . $payu_detail['SALT']);
                $service_api_data = array_merge($service_api_data, array('hash' => $hash));
                $result_data = unserialize(GeneralComponent::curl($payu_detail['SERVICE_URL'], $service_api_data, 'post'));
                if (count($result_data['transaction_details']) > 0) {
                    
                    foreach ($result_data['transaction_details'] as $txn_num => $txndetail) {
                        if ($txndetail['status'] === "Not Found") {
                            $txndetail['offline'] = "";
                            $txndetail['txnid'] = $txn_num;
                            $txndetail['status'] = 'failure';
                            self::update_pg_payu($txndetail, False);
                        } else {
                            /* if($txndetail['status'] === "pending"){
                              $txndetail['status'] = "failure";
                              } */
                            $txndetail['offline'] = "";
                            $txndetail['amount'] = isset($txndetail['amount']) ? $txndetail['amount'] : "";
                            $txndetail['amount'] = ($txndetail['amount'] == "" && isset($txndetail['amt'])) ? $txndetail['amt'] : "";
                            self::update_pg_payu($txndetail);
                        }
                    }
                } else {
                    exit();
                }
                return array('txn_id' => $var1, 'result' => $result_data);
                break;
        }
    }

    /*
     * This function generate all the form element and their values that are required to process txn to online gateway (payu)
     * @return : string
     */

    public static function generatePayment_form($data) {
        $payu_detail = self::payu_getconfig();
        $data2process = self::payu_setdata2process($data);
        $fdata = array('data2process' => $data2process, 'salt' => $payu_detail['SALT']);
        $hash = self::generate_payu_hash($fdata);
        $userdetail = UsersComponent::getuserProfile();
        $form_data = array_merge($data2process, array('phone' => ($userdetail['description']['mobile'] == 0) ? $data['userid'] : $userdetail['description']['mobile']), array('surl' => $payu_detail['SUCCESS_URL']), array('furl' => $payu_detail['FAILURE_URL']), array('curl' => $payu_detail['CANCEL_URL']), array('touturl' => $payu_detail['TIMEOUT_URL']), array('hash' => $hash), array('user_credentials' => $data2process['key'] . ":" . $data['userid']));
        //$pay_page_content = self::generate_pg_payu_form_content($form_data); //for production use only
        file_put_contents("/tmp/generatePayment_form", "| payment ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        
        //if(isset($data['request_via']) && $data['request_via']=='web' ){
        if(isset($data['request_via']) && $data['request_via']=='web'){
            $form_data = array_merge($form_data,array('product'=>isset($data['product']) ? $data['product'] : ""));
            if(isset($data['product']) && isset($data['operator'])){
                $operator = GeneralComponent::get_plancode($data['operator'],$data['product']);
                if(array_key_exists($data['product'], Yii::app()->params['productMapping'])){
                    $operator = Yii::app()->params['productMapping'][$data['product']][$operator]['operator'];
                }
                else{
                    $operator='';
                }
                //file_put_contents("/tmp/generatePayment_form", "| payment ".  json_encode($operator)."\n", FILE_APPEND | LOCK_EX);
                $form_data = array_merge($form_data,array('operator'=>$operator));
            }
            if(isset($data['trans_category']) && $data['trans_category']='refill'  && isset($data['returnparam']['category'])){
                if(!empty($data['returnparam']['category'])){
                    $form_data = array_merge($form_data,array('category'=>$data['returnparam']['category']));
                    if(isset($data['returnparam']['actual_price'])){
                        $form_data = array_merge($form_data,array('actual_price'=>$data['returnparam']['actual_price']));
                    }
                    if(isset($data['returnparam']['offer_price'])){
                        $form_data = array_merge($form_data,array('actual_price'=>$data['returnparam']['offer_price']));
                    }
                    if(isset($data['operator'])){
                        $form_data = array_merge($form_data,array('operator'=>$data['operator']));
                    }
                }
                
            }
            if(array_key_exists('returnparam', $data) ){
                if( array_key_exists('partial', $data['returnparam'])){
                $form_data =  array_merge($form_data,array('partial'=>$data['returnparam']['partial']));
                if(array_key_exists('current_bal',$data))
                    $form_data =  array_merge($form_data,array('wallet_balance'=>$data['current_bal']));
                }
            }
            //file_put_contents("/tmp/transaction", "|test data ".  json_encode($form_data)." \n", FILE_APPEND | LOCK_EX);
            $pay_page_content = self::generate_b2cPayu_form_content($form_data); // for developement server
            
        }
        else{
            $pay_page_content = self::generate_pg_payu_form_content($form_data); //for production use only
        }
        //$pay_page_content = self::generate_pg_payu_form_content($form_data); //for production use only
        return $pay_page_content;
    }
    
    /*
     * This generate card details form 
     * @return : html
     */
    public static function generate_b2cPayu_form_content($data) {
        $user_sess_id = session_id();
        $user_sess_val = session_name();
        $data['user_sess_id'] = $user_sess_id;
        $data['user_sess_val'] = $user_sess_val;
        $payu_detail = self::payu_getconfig();
        file_put_contents("/tmp/callback_processpayment", "| payment step2 ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
        $content = "<html>";
        $content .= '<script> function submitform(){document.getElementById("pg_payu_form").submit();}</script>';
        $content .= "<body onload=submitform()>";
        $content .= "<form id='pg_payu_form' action='" . $payu_detail['PAYMENT_URL'] . "' id='pg_payu_form' method='post'>";
        foreach ($data as $key => $value) {
            $content .= "<input type='hidden' name='$key' value='$value'>";
        }
        $content .= "</form>";
        $content .="</body></html>";
        //file_put_contents("/tmp/transaction", "| payment step2 ".  $content."\n", FILE_APPEND | LOCK_EX);
        return $content;
    }
    
    /*
     * This generate payu hash 
     * @return : string
     */

    public static function generate_payu_hash($data) {
        $str = implode("|", array_values($data['data2process'])) . "|||||||||||" . $data['salt'];
        return hash("sha512", $str);
    }

    /*
     * This returns array of data required to process txn to payment gateway
     * @return : array
     */

    public static function payu_setdata2process($data) {
        $payu_detail = self::payu_getconfig();
        $data2process['key'] = $payu_detail['KEY'];
        //transaction related detail
        $data2process['txnid'] = $data['transaction_id'];
        $data2process['amount'] = $data['amount'];
        $data2process['productinfo'] = $data['trans_category'];
        //customer detail
        $userdetail = UsersComponent::getuserProfile();
        if ($userdetail['errCode'] <= 0) {
            $data2process['firstname'] = $userdetail['description']['name'];
            $data2process['email'] = $userdetail['description']['email'];
        } else {
            $data2process['firstname'] = "";
            $data2process['email'] = "";
        }
        return $data2process;
    }

    /*
     * This returns array of payu configuration
     * @return : array
     */

    public static function payu_getconfig() {
        $payu_detail = Yii::app()->params['vendorApi']['payment_gateway']['payu_india'];
        return $payu_detail;
    }

    /*
     * This returns html content which include form that need to be submit to pass data to payment gateway
     * @return : string
     */

    public static function generate_pg_payu_form_content($data) {
        $payu_detail = self::payu_getconfig();
        $content = "<html>";
        $content .= '<script> function submitform(){document.getElementById("pg_payu_form").submit();}</script>';
        $content .= "<body onload=submitform()>";
        $content .= "<form id='pg_payu_form' action='" . $payu_detail['SUBMIT_URL'] . "' id='pg_payu_form' method='post'>";
        foreach ($data as $key => $value) {
            $content .= "<input type='hidden' name='$key' value='$value'>";
        }
        $content .= "</form>";
        $content .="</body></html>";
        return $content;
    }
    
    public static function getUserId_transaction($transactionId) {
        $query = "SELECT users_id FROM transactions WHERE trans_id = $transactionId";
        $Result = Yii::app()->db->createCommand($query)->queryRow();
        return isset($Result['users_id']) ? $Result['users_id'] : 0;
    }
    /*
     * This function is call to forward txn request to B2B server for recharges & bill-payment
     * @return : json string (app), void (web)
     */

    public static function process_transaction_after_pg($data) {
        $closing = "";
        try {
            $is_fraud = FALSE;
            $connection = Yii::app()->db;
            $params = json_decode(yii::app()->cache->hget('transdata', $data['txnid']), True);
            if (is_object($params)) {
                $result = json_decode(json_encode($params), TRUE);
                $params = $result;
            }
            $transId = $data['txnid'];
            //$transaction = $connection->beginTransaction();
            $userid = isset($params['userid']) ? $params['userid'] : self::getUserId_transaction($transId);
            //file_put_contents("/tmp/process_transaction_after_pg", "| noti-array ".json_encode($params).'  Txn '. $data['txnid']."\n", FILE_APPEND | LOCK_EX);
            if (isset($params['intime']) && (time() - $params['intime']) > 60 * 15) {
                throw new Exception("Late success from payment gateway", 262);
            }
            if ($data['amount'] > 0) {
                $is_fraud = self::check_fraudulant_engine($userid, $data['amount'], $data);
                if ($is_fraud) {
                    throw new Exception("Suspected fraudulant transaction. Please provide your KYC and scan copy of both side of your Debit/Credit card at listen@mindsarray.com", 502);
                }
            }
            //$params['mobile']= isset(Yii::app()->user->username) ? Yii::app()->user->username :"";
            //$transaction->commit();
            $transaction = $connection->beginTransaction();
            $response_detail = VendorComponent::callPay1($params);
            Yii::app()->cache->hdel('transdata', $data['txnid']);
            $response_detail_array = json_decode($response_detail, true);
            $response_id = isset($response_detail_array['ref_code']) ? $response_detail_array['ref_code'] : "";
            if ($response_detail_array['status'] === 'failure') {
                throw new Exception($response_detail_array['description'], 501);
            }
            $connection->createCommand("UPDATE transactions SET response_id='" . $response_id . "',status='2',"
                    . "response='" . addslashes($response_detail) . "' WHERE trans_id='" . $data['txnid'] . "'")->execute();
            $result = array('status' => 'success', 'errCode' => '0', 'description' => $data['txnid']);
            
             if(isset($params['api_version']) && $params['api_version']==3 ){
                  TransactionComponent::LoyaltyTransaction($connection,$params);
             }
            $transaction->commit();
        } catch (Exception $ex) {
            if (!$is_fraud && isset($transaction)) {
                $transaction->rollback();
            }
            if ($is_fraud) {
                $black_listed_cond .= " users_id = '$userid' ";
                //Checking User is Paramanent user or not. If User is ParamentUser don't block it.
                $NotPermanentUnblockUser = self::isNotPermanentUnblockUser($userid);
                //file_put_contents("/tmp/process_transaction_after_pg", "| " . "INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')" . "|" . var_dump($NotPermanentUnblockUser) . " | \n", FILE_APPEND | LOCK_EX);
                $data_black_count = $connection->createCommand("SELECT id FROM black_listed_user WHERE $black_listed_cond")->execute();
                if (($data_black_count < 1) && ($NotPermanentUnblockUser)) {
                    $device_id = "";
                    Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')")->execute();
                   // file_put_contents("/tmp/process_transaction_after_pg", "| " . "INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')" . "|" . var_dump($NotPermanentUnblockUser) . " | \n", FILE_APPEND | LOCK_EX);
                    Yii::app()->db->createCommand("update `user_profile` SET `is_genuine`=0 where `user_id`= $userid")->execute();
                }
            }
            $failed_data = array('client_req_id' => $data['txnid'], 'err_code' => $ex->getCode(), 'description' => $ex->getMessage(),
                    'trans_id' => '', 'status' => 'failure', 'vendor_code' => 'cronscript');
            $reversal = WalletComponent::updateTransaction($failed_data);
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage() . " | " . $reversal);
        }
        if (isset($params['return_url']) && strlen(trim($params['return_url'])) != 0) {
            header("location:http://".$params['return_url']."?status=" . $data['status'] . "&txnid=" . $data['txnid'] . "&closing=" . $closing);
            //header("location:" . $params['return_url'] . "?status=" . $data['status'] . "&txnid=" . $data['txnid'] . "&closing=" . $closing);
            exit();
        } else {
            return json_encode($result);
        }
    }

    /*
     * This function checks whether the txn done by valid user of frauds. In case of fraudulant it block the users
     * @return : boolean
     */

    public static function check_fraudulant_engine($userid, $amount = null, $pg_data = array()) {
        try {
            $user_array = UsersComponent::getuserProfile($userid);
            $data['umobile'] = $user_array['mobile'];
            $blocked_area = array("BR" => 1000, "WB" => 1500);
            $circle_array = json_decode(VendorComponent::get_circle_by_mobile($data), TRUE);
            $area = "";
            $black_listed_cond = "";
            $device_id = $user_array['uuid'];
            $black_listed_cond .= " users_id = '$userid' ";
            $connection = Yii::app()->db;
            $data_black_count = $connection->createCommand("SELECT id FROM black_listed_user WHERE $black_listed_cond")->execute();
            if ($data_black_count > 0) {
                GeneralComponent::sendMail("tadka@mindsarray.com,dharmesh@mindsarray.com,sunilc@mindsarray.com,nandan@mindsarray.com", "Blocked user trying to do transaction", json_encode($pg_data));
                return TRUE;
            }
            if (count($circle_array) > 0) {
                $area = $circle_array[0]['details']['area'];
            }
            
            //Checking User is Paramanent user or not. If User is ParamentUser don't block it.
            $NotPermanentUnblockUser = self::isNotPermanentUnblockUser($userid);
            //file_put_contents("/tmp/check_fraudulant_engine", "| " . "INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')" . "|" . var_dump($NotPermanentUnblockUser) . " | \n", FILE_APPEND | LOCK_EX);
            if ((in_array($area, array_keys($blocked_area)) && $blocked_area[$area] <= $amount) && ($NotPermanentUnblockUser)) {
                $fraud_insert = Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')")->execute();
                
                //file_put_contents("/tmp/chk_bihar_fraud", "| " . "INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')" . "|" . var_dump($fraud_insert) . " | \n", FILE_APPEND | LOCK_EX);
                GeneralComponent::sendMail("tadka@mindsarray.com,dharmesh@mindsarray.com,sunilc@mindsarray.com,nandan@mindsarray.com", "Blocked pay1 user from bihar and jharkhand circle", json_encode($pg_data));
                Yii::app()->db->createCommand("update `user_profile` SET `is_genuine`=0 where `user_id`= $userid")->execute();
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            GeneralComponent::sendMail("nandan@mindsarray.com", "Blocked pay1 user", json_encode($ex->getMessage()) . " " . json_encode($user_array));
            return FALSE;
        }
    }

    /*
     * This function is call to process deal after successful online payment
     * @return : json string (app), void (web)
     */

    public static function process_deal_after_pg($data) {
        $closing = "";
        try {
            $connection = Yii::app()->db;
            $params = json_decode(yii::app()->cache->hget('transdata', $data['txnid']));
            if (is_object($params)) {
                $result = json_decode(json_encode($params), TRUE);
                $params = $result;
            }
            $userid = isset($params['userid']) ? $params['userid'] : Yii::app()->user->id;
            $connection->createCommand("UPDATE wallets set account_balance = account_balance + " . $data['amount'] . " WHERE users_id='" . $userid . "'")->execute();
            $transaction = $connection->beginTransaction();
            $connection->createCommand("UPDATE wallets set account_balance = account_balance - " . $data['amount'] . " WHERE users_id='" . $userid . "'")->execute();
            Yii::app()->cache->hdel('transdata', $data['txnid']);
            TransactionComponent::updateTransaction($connection, $data['txnid'], array('status' => 2));
            $present_wallet_detail = WalletComponent::getWalletDetail($userid);
            if (isset($param['deal_coupon_code'])) {
                $result = array('status' => 'success', 'errCode' => '0', 'description' => array('deal_coupon_code' => $param['deal_coupon_code'], 'transaction_id' => $data['txnid'], 'closing_balance' => $present_wallet_detail['account_balance']));
            } else {
                $result = array('status' => 'success', 'errCode' => '0', 'description' => array('transaction_id' => $data['txnid'], 'closing_balance' => $present_wallet_detail['account_balance']));
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $e->getCode(), 'description' => $e->getMessage());
        }
        if (isset($params['return_url']) && strlen(trim($params['return_url'])) != 0) {
            header("location:" . $params['return_url'] . "?status=" . $data['status'] . "&txnid=" . $data['txnid'] . "&closing=" . $closing);
            exit();
        } else {
            return json_encode($result);
        }
    }

    /*
     * This function is call to refill the users wallet after successful online payment
     * @return : json string (app), void (web)
     */

    public static function process_refill_after_pg($data) {
        $closing = "";
        try {
            $connection = Yii::app()->db;
            $params = json_decode(yii::app()->cache->hget('transdata', $data['txnid']));
            if ($params) {
                if (is_object($params)) {
                    $result = json_decode(json_encode($params), TRUE);
                    $params = $result;
                }
                $userid = isset($params['userid']) ? $params['userid'] : Yii::app()->user->id;
                $transaction = $connection->beginTransaction();
                $walletdata = $connection->createCommand("SELECT * FROM wallets WHERE users_id =" . $userid . " FOR UPDATE");
                $walletdetail = $walletdata->queryAll();
                $walletObj = isset($walletdetail[0]) ? $walletdetail[0] : null;
                $current_bal = isset($walletObj['account_balance']) ? $walletObj['account_balance'] : 0;
                TransactionComponent::updateTransaction($connection, $data['txnid'], array('status' => 2, 'closing_bal' => $current_bal + $data['amount']));
                $connection->createCommand("UPDATE wallets set account_balance = account_balance + " . $data['amount'] . " WHERE users_id='" . $userid . "'")->execute();
                $transaction->commit();
                $cresult = Yii::app()->cache->hdel('transdata', $data['txnid']);
                $user_wallet_detail = WalletComponent::getWalletDetail($userid);
                $prof = UsersComponent::getuserProfile($userid);
                $device_id = $prof['uuid'];
                $data_black = array();
                if (strlen(trim($device_id)) > 1) {
                    $data_black = Yii::app()->db->createCommand("SELECT id FROM black_listed_user WHERE device_id = '$device_id'")->queryall();
                }
                //Checking User is Paramanent user or not. If User is ParamentUser don't block it.
                $NotPermanentUnblockUser = self::isNotPermanentUnblockUser($userid);
           // file_put_contents("/tmp/process_refill_after_pg", "| " .$data['PG_TYPE'] ."|". $user_wallet_detail['account_balance']."|". $NotPermanentUnblockUser ."|". $data_black. "|" . var_dump($NotPermanentUnblockUser) . " | \n", FILE_APPEND | LOCK_EX);

                //code to block a account if a PG is SBI & refill amount >= 4500
                //if(($data['PG_TYPE'] == 'SBIPG' && $data['amount'] >= 4500) || !empty($data_black)){
                if ((($data['PG_TYPE'] == 'SBIPG' && $user_wallet_detail['account_balance'] >= 4000) && ($NotPermanentUnblockUser)) || !empty($data_black)) {
                    Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, $userid,'$device_id')")->execute();
                    //file_put_contents("/tmp/process_refill_after_pg", "| " . "INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')" . "|" . var_dump($NotPermanentUnblockUser) . " | \n", FILE_APPEND | LOCK_EX);
                    Yii::app()->db->createCommand("update `user_profile` SET `is_genuine`=0 where `user_id`= $userid")->execute();
                    GeneralComponent::sendMail("tadka@mindsarray.com,dharmesh@mindsarray.com,sunilc@mindsarray.com", "Blocked pay1 user", json_encode($data));
                }
            }
            $present_wallet_detail = WalletComponent::getWalletDetail($userid);
            $closing = isset($present_wallet_detail['account_balance']) ? $present_wallet_detail['account_balance'] : 0;
            if(!isset($params['returnparam'])){
                GeneralComponent::notifier(array('WALLET_REFILLED_ONLINE'), array('amount' => $data['amount'], 'balance' => $closing, 'user_id' => $userid));
            }
            
            $result = array('status' => 'success', 'errCode' => 0,
                    'description' => array('transaction_id' => isset($data['txnid']) ? $data['txnid'] : 0,
                            'closing_balance' => $closing));
        } catch (Exception $ex) {
            $transaction->rollback();
            $result = array('status' => 'failure', 'errCode' => $e->getCode(), 'description' => $e->getMessage());
        }
        if(isset($params['returnparam']['category']) && !empty($params['returnparam']['category'])){
            return json_encode($result);
        }
        if (isset($params['return_url']) && strlen(trim($params['return_url'])) != 0 && !isset($params['returnparam']['category'])) {
              
              $url = $params['return_url'] . "?status=" . $data['status'] . "&txnid=" . $data['txnid'] . "&closing=" . $closing;
              //file_put_contents("/tmp/update_pg_payu", "| hash 5 ".  ($url)."\n", FILE_APPEND | LOCK_EX);
            header("location:http://" .$url );
            exit();
        } else {
            return json_encode($result);
        }
    }

    /*
     * This function use to add user in block list
     * 
     */

    public static function blockUser($data) {
        $userid = $data['userid'];
        $device_id = $data['device_id'];
        try {
            Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,users_id,device_id) VALUES (NULL, " . $userid . ",'" . $device_id . "')")->execute();
            Yii::app()->db->createCommand("UPDATE user_profile SET is_genuine=0 WHERE user_id = $userid")->execute();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public static function clear_transaction(){
        $query = "select trans_id FROM transactions WHERE status=3 AND transaction_mode='online'";
        $result = Yii::app()->db->createCommand($query)->queryAll();
        if(count($result)>0){
            foreach ($result as $row){
                if(Yii::app()->cache->hexists('transdata', $row['trans_id'])){
                    Yii::app()->cache->hdel('transdata', $row['trans_id']);
                }
            }
        }
            
    }
    
    public static function setRedisTransData($txnId){
        
        $query = "SELECT transaction_amount,users_id,trans_id,trans_category,trans_datetime FROM transactions where trans_id=$txnId AND status =1";
        $txn_details = Yii::app()->db->createCommand($query)->queryRow();
        if(isset($txn_details['trans_id']) && ($txn_details['trans_category']=='billing' || $txn_details['trans_category']=='recharge')){
            $table = $txn_details['trans_category']."_transaction";
            $mobile_query = "SELECT mobile_number FROM $table where transaction_id = $txnId";
            $mobile_details = Yii::app()->db->createCommand($mobile_query)->queryRow();
            if(isset($mobile_details['mobile_number'])){
                $intime = strtotime($txn_details['trans_datetime']);
                $mobile = $mobile_details['mobile_number'];
                $response_detail = array('amount'=>$txn_details['transaction_amount'],'userid'=>$txn_details['users_id'],'transaction_id'=>$txnId,'trans_category'=>$txn_details['trans_category'],'mobile'=>$mobile,'intime'=>$intime);
                Yii::app()->cache->hset('transdata', $txnId, json_encode($response_detail));
            }
        }
         exit();
        // return true;
    }
    
    public static function cancel_payment($data){
        try{
            $addon = date('Y-m-d h:i:s');
            $amount = $data['amount'];
            $trans_id = $data['trans_id'];
            $connection = Yii::app()->db;
            //file_put_contents("/tmp/cancel_payment", "| : ".json_encode($data)."\n", FILE_APPEND | LOCK_EX);
            $connection->createCommand("UPDATE transactions SET status=3 WHERE trans_id =$trans_id")->execute();
            $query = "INSERT INTO `b2c_app1`.`pg_payuIndia` (`txnid`, `amount`, `addedon`,`unmappedstatus`,`status`) VALUES ( '$trans_id',$amount,'$addon','userCancelled','failure');";
            //file_put_contents("/tmp/cancel_payment", "| : ".$query."\n", FILE_APPEND | LOCK_EX);
            $connection->createCommand($query)->execute();
        }
        catch(Exception $ex){
            
        }
    }
    
    public static function isNotPermanentUnblockUser($userID){
        $user_array = Yii::app()->db->createCommand("SELECT * FROM permanent_unblock_user WHERE users_id = '$userID' ")->queryall();  
        if(count($user_array) > 0){
              return 0; 
        }
        return 1;
    }
    
}//End of OnlineComponent
