<?php

class GeneralComponent {

    public static function curl($url, $params = array(),$method = 'get') {
        if($method === 'get'){
            return Yii::app()->curl->get($url,$params);
        }
        if($method === 'post'){
            return Yii::app()->curl->post($url,$params);
        }
    }

    public static function replaceword($oldword, $newword, $stringcontainingword) {
        return str_replace($oldword, $newword, $stringcontainingword);
    }

    public static function get_formated_response($res,$data) {        
        $response_type = isset($data['res_format'])?trim($data['res_format']):"";
        switch($response_type){
            case "jsonp":
                $res = $data['callback']."(".$res.")";
                break;
            default :
                $res = $res;
                break;
        }
        return $res;
    }

    public static function get_plancode($operatorid, $product) {
        switch ($product) {
            case 'MOBILE':
                $product_result = Yii::app()->db->createCommand("SELECT product_id from telecom_operators where id ='$operatorid'")->queryAll();
                return $product_result[0]['product_id'];
                break;
            case 'DTH':
                $product_result = Yii::app()->db->createCommand("SELECT product_id from dth_operators where id ='$operatorid'")->queryAll();
                return $product_result[0]['product_id'];
                break;
            case 'DATA':
                $product_result = Yii::app()->db->createCommand("SELECT data_product_id as product_id from telecom_operators where id ='$operatorid'")->queryAll();
                return $product_result[0]['product_id'];
                break;
            case 'POSTPAID':
                $product_result = Yii::app()->db->createCommand("SELECT postpaid as product_id from telecom_operators where id ='$operatorid'")->queryAll();
                return $product_result[0]['product_id'];
                break;
            case 'BILLING':
                $product_result = Yii::app()->db->createCommand("SELECT id as product_id from telecom_operators where id ='$operatorid'")->queryAll();
                return $product_result[0]['product_id'];
                break;
        }
    }

    public static function getProductDetails($product, $product_id,$name=FALSE) {
            if($name){
                return isset(Yii::app()->params['productMapping'][$product][$product_id]['operator']) ? Yii::app()->params['productMapping'][$product][$product_id]['operator'] : 'unknown';
            }
            return isset(Yii::app()->params['productMapping'][$product][$product_id]) ? Yii::app()->params['productMapping'][$product][$product_id] : 'unknown';
    }

    public static function get_unique_trackId() {
        return "T" . date('YmdHis') . uniqid();
    }

    public static function get_missedcallNumbers() {
        $missedCall_number_result = Yii::app()->db->createCommand("SELECT id, missed_number FROM global_missed_numbers where deleted !=1")->queryAll();
        $missedCall_number_list = array();
        foreach ($missedCall_number_result as $k => $v) {
            $missedCall_number_list[$v['id']] = $v['missed_number'];
        }
        return $missedCall_number_list;
    }

    public static function get_missedcallNumberById($id) {
        $numlist = self::get_missedcallNumbers();
        return $numlist[$id];
    }

    public static function get_globalMissedCallumberId($mobile_number){
        return array_search($mobile_number, self::get_missedcallNumbers());
    }

    public static function set_defaultBlank($dataarray, $var) {
        return isset($dataarray[$var]) ? $dataarray[$var] : "";
    }

    public static function getFormatedDate($format = null) {
        if(empty($format)){
            $format = 'Y-m-d H:i:s';
        }
        return date($format);
    }

    public static function generate_OTP($length = 6,$cstring=null) {
       $characters = is_null($cstring)?'0123456789':(string)$cstring;
       $randomString = '';
       for ($i = 0; $i < $length; $i++) {
           $randomString .= $characters[rand(0, strlen($characters) - 1)];
       }
       return $randomString;
   }

    public static function getEmptyElement($elementarray = array(),$datasource=  array()){
        $emptylist = array();
        foreach($elementarray as $element){
            if(trim($datasource[$element]) === ""){
                array_push($emptylist, $element);
            }
        }
        return $emptylist;
    }
    
    public static function getuserSummary($data){
        //get user detail
        $uObj = Users::model()->findAllByAttributes(array('user_name'=>$data['mobile_number']));
        $u_detail = $uObj[0]->attributes;
        //get user profile detail
        $upObj = UserProfile::model()->findAllByAttributes(array('user_id'=>$u_detail['id']));
        $up_detail = $upObj[0]->attributes; 
        //get user wallet detail
        $uwallet_Obj = Wallets::model()->findAllByAttributes(array('users_id'=>$u_detail['id']));
        $uwallet_detail = $uwallet_Obj[0]->attributes; 
        //print_r($uwallet_detail);echo "<hr>";        
        //get user services detail
        $uservice_Obj = DelegateRecharge::model()->findAllByAttributes(array('users_id'=>$u_detail['id']));
        foreach($uservice_Obj as $key=>$val){
            $uservice_detail[] = $val->attributes;
        }
        //print_r($uservice_detail);echo "<hr>";              
        //get transaction detail
        $userd = array('users_id'=>$u_detail['id']);
        $criteria = new CDbCriteria(array('order'=>'trans_datetime DESC'));
        $utrans_Obj = Transactions::model()->findAllByAttributes(array('users_id'=>$u_detail['id'],'date(trans_datetime) >= "'.$data['intime'].'"'));
        foreach($utrans_Obj as $key=>$val){
            $utrans_detail[] = $val->attributes;
        }
    }
    
    public static function isIPWhitelisted(){        
        return in_array(CHttpRequest::getUserHostAddress(), Yii::app()->params['whitelisted_IP']);
    }
    
    public static function getFAQList($data=null){
        try{
            $data['req']= isset($data['req'])?$data['req']:"";
            $faqObjs = Faq::model()->findAllByAttributes(array('question'=>  strtoupper($data['req'])));
            $resultdata = array();
            foreach($faqObjs as $faqObj){
                if($data['request_via'] === "web"){                    
                    $resultdata[] = preg_replace( "/^\s+|\n|\r|\s+$/", "",GeneralComponent::replaceword('"', '\"', GeneralComponent::replaceword("'", "\'",stripslashes($faqObj->answer))));
                }else{
                    $resultdata[] = $faqObj->attributes;
                }
            }
            
            $result = array('status'=>'success','errCode'=>'0','description'=>$resultdata);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }        
        return $result;
    }
    
    public static function supportRequest($data){
        try{
            $supportObj = new B2cSupport();
            $supportObj->mobile_number=$data["mobile_number"];
            $supportObj->users_id = Yii::app()->user->id;
            $supportObj->message=$data["message"];
            $supportObj->status=0;
            if(!$supportObj->save()){
                throw new Exception("Unable to accept support request",226);
            }
            $user_id = Yii::app()->user->id;
            $prf = UsersComponent::getuserProfile($user_id, 0);
            $sender = (strlen($prf['email'])>1)?$prf['email']:"support@pay1.in";
            $to = "listen@pay1.in";
            $subject = "Pay1 Customer Query";
            $message = $data["message"];
            self::sendMail($to, $subject, $data["mobile_number"]." : ". $message,$sender);
            $result = array('status'=>'success','errCode'=>'0','description'=>$data['message']);
        } catch (Exception $ex) {
            $result = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
        }
        return $result;
    }
    
    public static function getStatesList(){        
       $states = Yii::app()->db->createCommand("SELECT * FROM `locator_state` WHERE toShow = 1")->queryAll();
       return $states;
    }
    
    public static function getCitiesListByStateId($id){        
       $cities = Yii::app()->db->createCommand("SELECT * FROM `locator_city` WHERE state_id = $id AND toShow = 1")->queryAll();
       return $cities;
    }
    
    public static function getAreaByLatLong($lat_lng){//$lat,$long
            $LatLng = urlencode($lat_lng);
//            $data =JSON.parse(httpGet("https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat_lng+"&sensor=true"));
            $geocode = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=".$LatLng."&sensor=true");
//            echo "GEOCODE- ".$geocode;die;
            $data = json_decode($geocode,true);
              
//              echo " ".$data.results[0].formatted_address;
        $areaLine = urlencode($areaLine);
            $geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true");
             $output= json_decode($geocode,true);
             
            //echo "http://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true";
            //echo "<pre>"; print_r($output);echo "</pre>";
            $ret = array();
            $ret['address']             = $output["results"][0]["address_components"][0]["long_name"];
            $ret['area_name']           = "";//$output["results"][0]["address_components"][1]["long_name"];
            $ret['city_name']           = "";//$output["results"][0]["address_components"][2]["long_name"];
            $ret['state_name']          = "";//$output["results"][0]["address_components"][3]["long_name"];
            $ret['country_name']        = "";//$output["results"][0]["address_components"][4]["long_name"];
            $ret['formatted_address']   = $output["results"][0]["formatted_address"];
            $ret['lat']  = $output["results"][0]["geometry"]["location"]["lat"];
            $ret['lng']  = $output["results"][0]["geometry"]["location"]["lng"];
            $ret['geoURL']  = "https://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true";
            
            foreach($output["results"][0]["address_components"] as $arr){
                if($arr["types"][0] == "sublocality"){
                    $ret['area_name']           = $arr["long_name"];
                }else if($arr["types"][0] == "locality"){
                    $ret['city_name']  = $arr["long_name"];
                }else if($arr["types"][0] == "administrative_area_level_1"){
                    $ret['state_name'] = $arr["long_name"];
                }
            	else if($arr["types"][0] == "administrative_area_level_2"){
                    $ret['extra'] = $arr["long_name"];
                }
                else if($arr["types"][0] == "country"){
                    $ret['country_name']  = $arr["long_name"];
                }
            }
            
        	if(!isset($ret['area_name'])){
            	$ret['area_name'] = $ret['city_name'];
            	if(isset($ret['extra']))$ret['city_name'] = $ret['extra'];
            }
            
            //$this->logData('/var/www/html/shops/abc.txt',date('Y-m-d H:i:s')."getLatLongByArea $areaLine::".json_encode($output)."::".json_encode($ret));
        
            return $ret;
        }
    
    
    
    
    public static function getLatLongByArea($areaLine){//$lat,$long
            //Configure::write('debug',2);
            
        $areaLine = urlencode($areaLine);
            $geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true");
             $output= json_decode($geocode,true);
            //echo "http://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true";
            //echo "<pre>"; print_r($output);echo "</pre>";
            $ret = array();
            $ret['address']             = $output["results"][0]["address_components"][0]["long_name"];
            $ret['area_name']           = "";//$output["results"][0]["address_components"][1]["long_name"];
            $ret['city_name']           = "";//$output["results"][0]["address_components"][2]["long_name"];
            $ret['state_name']          = "";//$output["results"][0]["address_components"][3]["long_name"];
            $ret['country_name']        = "";//$output["results"][0]["address_components"][4]["long_name"];
            $ret['formatted_address']   = $output["results"][0]["formatted_address"];
            $ret['lat']  = $output["results"][0]["geometry"]["location"]["lat"];
            $ret['lng']  = $output["results"][0]["geometry"]["location"]["lng"];
            $ret['geoURL']  = "https://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true";
            
            foreach($output["results"][0]["address_components"] as $arr){
                if($arr["types"][0] == "sublocality"){
                    $ret['area_name']           = $arr["long_name"];
                }else if($arr["types"][0] == "locality"){
                    $ret['city_name']  = $arr["long_name"];
                }else if($arr["types"][0] == "administrative_area_level_1"){
                    $ret['state_name'] = $arr["long_name"];
                }
            	else if($arr["types"][0] == "administrative_area_level_2"){
                    $ret['extra'] = $arr["long_name"];
                }
                else if($arr["types"][0] == "country"){
                    $ret['country_name']  = $arr["long_name"];
                }
            }
            
        	if(!isset($ret['area_name'])){
            	$ret['area_name'] = $ret['city_name'];
            	if(isset($ret['extra']))$ret['city_name'] = $ret['extra'];
            }
            
            //$this->logData('/var/www/html/shops/abc.txt',date('Y-m-d H:i:s')."getLatLongByArea $areaLine::".json_encode($output)."::".json_encode($ret));
        
            return $ret;
        }

        public static function getCombineOperators(){
            try{
                $mobileOperator = Yii::app()->db->createCommand("SELECT topr.id, topr.opr_code, topr.product_id as product_id, prod.name, 
                    prod.service_id, '1' AS flag, '1' as delegate, prod.special as stv , prod.min, prod.max, prod.charges_slab,
                    prod.service_charge_amount, prod.service_tax_percent, prod.service_charge_percent, prod.length, prod.prefix
                    FROM  `telecom_operators` AS topr, products AS prod 
                    WHERE topr.product_id = prod.id and prod.active = 1")->queryAll();
                $dataOperator = Yii::app()->db->createCommand("SELECT topr.id, topr.opr_code, topr.data_product_id as product_id,prod.name,
                    prod.service_id, '3' AS flag ,'0' as delegate , prod.special as stv, prod.min, prod.max, prod.charges_slab,
                    prod.service_charge_amount, prod.service_tax_percent, prod.service_charge_percent, prod.length, prod.prefix
                    FROM  `telecom_operators` AS topr, products AS prod
                    WHERE topr.data_product_id = prod.id and prod.active = 1")->queryAll();
                $postpaidOperator = Yii::app()->db->createCommand("SELECT topr.id, topr.opr_code, topr.postpaid as product_id, 
                    prod.name, prod.service_id, '4' AS flag, '0' as delegate, prod.special as stv, prod.min, prod.max, prod.charges_slab,
                    prod.service_charge_amount, prod.service_tax_percent, prod.service_charge_percent, prod.length, prod.prefix
                    FROM  `telecom_operators` AS topr, products AS prod
                    WHERE topr.postpaid = prod.id and prod.active = 1")->queryAll();
                $dthOperator = Yii::app()->db->createCommand("SELECT dth.id, CONCAT(LEFT(prod.name,1),'D') as 'opr_code', dth.product_id, prod.name, prod.service_id,
                    '2' AS flag, '0' as delegate, prod.special as stv, prod.min, prod.max, prod.charges_slab,
                    prod.service_charge_amount, prod.service_tax_percent, prod.service_charge_percent, prod.length, prod.prefix
                    FROM  `dth_operators` AS dth, products AS prod
                    WHERE dth.product_id = prod.id and prod.active = 1")->queryAll();
                $resultarray = array('status'=>'success','errCode'=>'0','description'=>array('mobile'=>$mobileOperator,
                        'data'=>$dataOperator,
                        'postpaid'=>$postpaidOperator,
                        'dth'=>$dthOperator));                
            } catch (Exception $ex) {
                $resultarray = array('status'=>'failure','errCode'=>$ex->getCode(),'description'=>$ex->getMessage());
            }
            return $resultarray;
        }
        
        public static function removeSlashes($data = array()){
            foreach ($data as $key => $value) {
                if(is_array($value)){
                    $data[$key] = self::removeSlashes($value);
                }else{
                    $data[$key]=stripslashes($value);
                    $data[$key]=  str_replace("\r\n", "", $value);
                }                
            }
             return $data;
        }
        
        public static function generate_unique_number($len=14,$num_only=TRUE){
           //$num_only = ($num_only=="")?TRUE:$num_only;    
           $p2 = self::generate_OTP(4,hexdec(uniqid()));
           $p1 = self::generate_OTP(3);
           $p3 = self::generate_OTP(3,time());
           $p4 = ($num_only)?self::generate_OTP(4,hexdec(uniqid())):self::generate_OTP(4,'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
           $num = str_shuffle(self::generate_OTP($len-4,$p1."".$p2."".$p3)."".$p4);
           return $num;
       }
        
        public static function sendNotification($user_ids,$title,$msg,$type,$bal_flag,$save_flag,$img=null,$offer_id=null,$dealName = null,$showImg=null){//$userids is in array format
        	
        	$regis_ids_android = array();
        	$regis_ids_windows = array();
        	$regis_ids_ios = array();
        	
        	$reg_ids = Yii::app()->db->createCommand("SELECT gcm_reg_id,device_type FROM `user_profile` "
                    . "WHERE gcm_reg_id != '' AND user_id in (".implode(",",$user_ids).")")->queryAll();
       		foreach($reg_ids as $key){
                    //file_put_contents("/tmp/sendIOSNotification", "| device id  ".  json_encode($key)."\n FILE_APPEND | LOCK_EX");
       			if($key['device_type'] == 'android'){
       				$regis_ids_android[] = $key['gcm_reg_id'];
       			}
       			else if($key['device_type'] == 'windows'){
       				$regis_ids_windows[] = $key['gcm_reg_id'];
                               
       			}
       			else if(strtoupper($key['device_type']) == 'IOS'){
                            //file_put_contents("/tmp/sendIOSNotification", "| device id  "."TEST"."\n FILE_APPEND | LOCK_EX");
       				$regis_ids_ios[] = $key['gcm_reg_id'];
       			}
       		}
       		
       		if(!empty($regis_ids_android)) return self::sendAndroidNotification($regis_ids_android,$title,$msg,$type,$bal_flag,$save_flag,$img,$offer_id);
       		if(!empty($regis_ids_windows)){ 
                     //file_put_contents("/tmp/checkResponce", "| device id  ".  json_encode($regis_ids_windows)."\n FILE_APPEND | LOCK_EX");
                    return self::sendWindowsNotification($regis_ids_windows,$title,$msg,$type,$bal_flag,$save_flag,$img,$offer_id,$dealName,$showImg);
                }
       		if(!empty($regis_ids_ios)) return self::sendIOSNotification($regis_ids_ios,$title,$msg,$type,$bal_flag,$save_flag,$img);
        }
        
        public static function sendAndroidNotification($regis_ids,$title,$msg,$type,$bal_flag,$save_flag,$img=null,$offer_id=null){
        	$google_urls = Yii::app()->params['vendorApi']['google_notif'];
        	
        	$msgPass = json_encode(array(
                    "type" => $type,
                    "title" => $title,
                    "msg" => $msg,
                    "img" => $img,
                    "bal" => $bal_flag,
                    "save" => $save_flag,
                    "offer_id" => $offer_id
       		));
       		
       		$message = array(
                    "data" => $msgPass
       		);
       		 
        	$fields = array(
            'registration_ids' => $regis_ids,
            'data' => $message,
        	);
            $uniqid = uniqid("NOTI-");
            	$headers = array(
            'Authorization: key=' . $google_urls['GOOGLE_API_KEY'],
            'Content-Type: application/json'
            );
            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $google_urls['GCM_URL']);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3');
            
            // Execute post
            $result = curl_exec($ch);
            //print_r($result);exit();
            $response = array();
            if ($result === FALSE || empty($result)) {
            	$response['status'] = "failure";
            	$response['error_code'] = curl_errno($ch);
            	$response['description'] = curl_error($ch);
            	curl_close($ch);
               // file_put_contents("/tmp/checkResponce", "|  Responce ".  json_encode($response)."\n FILE_APPEND | LOCK_EX");
               return $response;//null;//
            }
            else {
            	$result = json_decode($result,true);
		        if($result['success'] == '0'){
		        	$response['status'] = "failure";
		        }
		        else {
		        	$response['status'] = "success";
		        }
		        $response['error_code'] = "";
		        $response['description'] = $result;
		        curl_close($ch);
                      //  file_put_contents("/tmp/checkResponce", "|  : Final ".  json_encode($response)."\n FILE_APPEND | LOCK_EX");
                       return $response;
            }
            return $response;
        }
        
        public static function sendWindowsNotification($regis_ids,$title,$msg,$type,$bal_flag,$save_flag,$img=null,$offer_id=null,$dealName = null,$showImg=null) {
// Create the toast message
                $paramStr="";
        	//$paramStr = "/NotificationList.xaml?pull=true";
                if(!empty($offer_id)){
                    $paramStr = "/HomePage.xaml?offer_id=$offer_id";//&fromNotification=y
                }
                else{
                     $paramStr = "/HomePage.xaml?msg=";
                }
                $msg = empty($msg) ? $dealName : $msg;
            //$title = "This is test notification line 1.\r\n This is line 2";
                $toastMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
	                    "<wp:Notification xmlns:wp=\"WPNotification\">" .
	                            "<wp:Toast>" .
	                            "<wp:Text1>" .$title . "</wp:Text1>" .
	                            "<wp:Text2>".$msg."</wp:Text2>" .
	                            "<wp:Text3>".$type."</wp:Text3>" .
	                            "<wp:Param>".$paramStr."</wp:Param>" .
	                            "</wp:Toast> " .
	                    "</wp:Notification>";
           // file_put_contents("/tmp/checkResponce", "| Message  ".$toastMessage."\n FILE_APPEND | LOCK_EX");
            
            $tileMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
                "<wp:Notification xmlns:wp=\"WPNotification\">" .
                   //"<wp:Tile Id=\"\" Template=\"IconicTile\">" .
                   "<wp:Tile>" .
                        "<wp:BackgroundImage>/Images/info.png</wp:BackgroundImage>".
                        "<wp:Count>5</wp:Count>" .
                        "<wp:Title>$title</wp:Title>".
                        "<wp:BackBackgroundImage>http://b2c.pay1.in/images/53e9feb209887.jpg</wp:BackBackgroundImage>" .
                        //"<wp:BackBackgroundImage>/Images/info.png</wp:BackBackgroundImage>" .
                        "<wp:BackTitle>" . $title . "</wp:BackTitle>".
                        "<wp:BackContent>" . $title."back -- content" . "</wp:BackContent>".
                    "</wp:Tile>" .
                "</wp:Notification>";
            if(!empty($toastMessage)){
       	foreach($regis_ids as $registatoin_url){
        		// Create request to send
        		$r = curl_init();
        		curl_setopt($r, CURLOPT_URL,$registatoin_url);
        		curl_setopt($r, CURLOPT_RETURNTRANSFER, 1);
        		curl_setopt($r, CURLOPT_POST, true);
        		curl_setopt($r, CURLOPT_HEADER, true);
                        // add headers
        		$httpHeaders=array('Content-type: text/xml; charset=utf-8', 'X-WindowsPhone-Target: toast',
		                        'Accept: application/*', 'X-NotificationClass: 2','Content-Length:'.strlen($toastMessage));
                //$httpHeaders=array('Content-type: text/xml; charset=utf-8', 'X-WindowsPhone-Target: token',
		        //               'Accept: application/*', 'X-NotificationClass: 1','Content-Length:'.strlen($tileMessage));
        		curl_setopt($r, CURLOPT_HTTPHEADER, $httpHeaders);

        		// add message
        		curl_setopt($r, CURLOPT_POSTFIELDS, $toastMessage);
        		// execute request
        		$output = curl_exec($r);
                        //file_put_contents("/tmp/checkResponce", "| output 1".  var_dump($output)."\n FILE_APPEND | LOCK_EX");
        		//file_put_contents("/tmp/checkResponce", "| output 2".  json_encode($output)."\n FILE_APPEND | LOCK_EX");
        		// add message
        		//curl_setopt($r, CURLOPT_POSTFIELDS, $tileMessage);
        		if($output == FALSE){
        			$output = "SUCCESS:".$output;
        		}else{
        			$errno = curl_errno($r);
        			$error =  curl_error($r);
        			$output = $errno.":<".$error.">".$output;
        		}
        		curl_close($r);      
               }
            }
               //return $output;
        }
        
        public static function sendIOSNotification($regis_ids_ios,$title=null,$msg=null,$type=null,$bal_flag=null,$save_flag=null,$img=null){
//            $query = "SELECT gcm_reg_id FROM user_profile where user_id=4";
//            $result = Yii::app()->db->createCommand($query)->queryAll();
            //file_put_contents("/tmp/sendIOSNotification", "| : step1 \n".$regis_ids_ios." ".$title." ".$msg, FILE_APPEND | LOCK_EX);
            //$regis_ids_ios,$title,$msg,$type,$bal_flag,$save_flag,$img
           // $msg = "This is testing message";
            // Put your device token here (without spaces):
            $deviceToken = $regis_ids_ios;//$result[0]['gcm_reg_id'];//'8e688dab2258f1ff6fc2ad50abf4864dd3ad7ba5729467095d'; //'0f744707bebcf74f9b7c25d48e3358945f6aa01da5ddb387462c7eaf61bbad78';

            // Put your private key's passphrase here:
            $passphrase = 'Pay1B2C';

            // Put your alert message here:
            $message = "$msg";
            //$message = '{"action-loc-key": "view","body": "$msg","title" : "$title","launch-image" : "$img"}';


            $ctx = stream_context_create();
            
            //file_put_contents("/tmp/sendIOSNotification", "| : step2 \n", FILE_APPEND | LOCK_EX);
            stream_context_set_option($ctx, 'ssl', 'local_cert', $_SERVER['DOCUMENT_ROOT'].'/protected/vendor/Pay1B2C.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            //file_put_contents("/tmp/sendIOSNotification", "| : step3 \n", FILE_APPEND | LOCK_EX);
           //file_put_contents("/tmp/sendIOSNotification", "| : step2".  json_encode($ctx)." \n", FILE_APPEND | LOCK_EX);
            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
           // file_put_contents("/tmp/sendIOSNotification", "| : step2".  json_encode($fp)." \n", FILE_APPEND | LOCK_EX);
           // stream_socket_enable_crypto($fp, true, STREAM_CRYPTO_METHOD_SSLv23_CLIENT);
            if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);

            echo 'Connected to APNS' . PHP_EOL;

            // Create the payload body
            $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default',
                    );

            // Encode the payload as JSON
            $payload = json_encode($body);
           // file_put_contents("/tmp/sendIOSNotification", "| : step4".  $payload." \n", FILE_APPEND | LOCK_EX);
            // Build the binary notification
            foreach($regis_ids_ios as $registatoin_url){
                // file_put_contents("/tmp/sendIOSNotification", "| : step4".  $registatoin_url." \n", FILE_APPEND | LOCK_EX);
                $msg = chr(0) . pack('n', 32) . pack('H*', $registatoin_url) . pack('n', strlen($payload)) . $payload;
                $result = fwrite($fp, $msg, strlen($msg));
            }
            
            if (!$result)
                    echo 'Message not delivered' . PHP_EOL;
            else
                    echo 'Message successfully delivered' . PHP_EOL;

            // Close the connection to the server
            fclose($fp);
        }


        public static function sendMail($to, $subject, $message,$sender="support@pay1.in", $additional_headers = null, $additional_parameters = null){
            $headers = "From: " . strip_tags($sender) . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            return mail($to, $subject, $message, $headers, $additional_parameters);
        }
        
        public static function sendSMS($msisdn,$message){
            $api_param = array('msisdn'=>$msisdn,'message'=>$message);
            Yii::app()->cache->select(0);
            $flag = Yii::app()->params['GLOBALS']['SMS_VENDOR_FLAG'];
            if(strpos($msisdn,',')!==false){
                $flag=2;
            }
            if($flag == 1){
                Yii::app()->cache->lpush("tatamessages_b2c",$msisdn."|<>|".urldecode($message)."|<>|");
            }else{
                Yii::app()->cache->lpush("247messages_b2c","|<>|".$msisdn."|<>|".urldecode($message)."|<>|payone");
            }
            //Yii::app()->cache->select(Yii::app()->params['GLOBALS']['redis_default_database']);
            //VendorComponent::callsmstadka($api_param);
        }
        
        public static function notifier($type = array(),$data = array(),$mode=null){
            $notifier_array = array('type'=>$type,'data'=>$data);
            if(!is_null($mode)){
                $notifier_array['mode']=$mode;
            }
            Yii::app()->cache->lpush(Yii::app()->params['GLOBALS']['_NOTIFIER_Q'],  json_encode($notifier_array));
        }
        
        public static function offline_notifier($type = array(),$data = array(),$mode=null){
        //public static function notifier($type = array(),$data = array(),$mode=null){ 
            foreach ($type as $key){
                $paramdata = self::get_notification_variable($key, $data);
                $paramdata['download_url'] = Yii::app()->params['GLOBALS']['API_BASE_URL'].'download';
                $paramdata['date'] =  GeneralComponent::getFormatedDate('d/m/Y');
                $paramdata['time'] = GeneralComponent::getFormatedDate('H:i:s');
                if(($mode == null || strtolower($mode) == "notification") && in_array($key, array_keys(Yii::app()->params['notification_template']))){
                    $content = Yii::app()->params['notification_template'][$key];
                    foreach ($paramdata as $var=>$val){
                        $content = GeneralComponent::replaceword('<'.strtoupper($var).'>', $val, $content);
                    }
                    $bal_flag = isset($paramdata['bal_flag'])?$paramdata['bal_flag']:0;
                    $notify_type = isset($data['notify_type'])?$data['notify_type']:'notify';
                    $save_flag = isset($data['save_flag'])?$data['save_flag']:0;
                    $paramdata['user_id'] = isset($paramdata['user_id'])?$paramdata['user_id']:$paramdata['users_id'];
                    $notification = self::sendNotification(array($paramdata['user_id']),'Pay1 Notification',$content,$notify_type,$bal_flag,$save_flag);
                    
                    $nlog_data = array('mobile'=>$paramdata['mobile'],'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>'notification',
                            'msg_content'=>  addslashes($content),'status'=>1);
                    self::log_notifications($nlog_data);
                }
                if(($mode == null || strtolower($mode) == "email") && in_array($key, array_keys(Yii::app()->params['email_template']))){
                    $content = Yii::app()->params['email_template'][$key];
                    foreach ($paramdata as $var=>$val){
                        $content = GeneralComponent::replaceword('<'.strtoupper($var).'>', $val, $content);
                    }
                    $content1 = Yii::app()->params['email_template']['EMAIL_TEMPLATE_HEADER']." ".
                            $content." ".Yii::app()->params['email_template']['EMAIL_TEMPLATE_FOOTER'];
                    $paramdata['subject'] = isset($paramdata['subject'])?$paramdata['subject']:"Pay1 Notification !!";
                    $notification = self::sendMail($paramdata['email'], $paramdata['subject'], $content1);
                    $nlog_data = array('mobile'=>$paramdata['mobile'],'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>'email',
                            'email_id'=>$paramdata['email'],'msg_content'=>  addslashes($content),'status'=>1);
                    self::log_notifications($nlog_data);
                }
                if(($mode == null || strtolower($mode) == "sms") && in_array($key,array_keys(Yii::app()->params['message_template']))){
                    $content = Yii::app()->params['message_template'][$key];
                    foreach ($paramdata as $var=>$val){
                        $content = GeneralComponent::replaceword('<'.strtoupper($var).'>', $val, $content);
                    }
                    $notification = self::sendSMS($paramdata['mobile'], urlencode($content));
                    //$notification = VendorComponent::callsmstadka(array('msisdn'=>$paramdata['mobile'],'message'=>  urlencode($content)));
                    $nlog_data = array('mobile'=>$paramdata['mobile'],'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>'sms',
                            'msg_content'=>  addslashes($content),'status'=>1);
                    self::log_notifications($nlog_data);
                }
            }
        }
        
        public static function log_notifications($data_rev){
            $connection = Yii::app()->db;
            $tablename = "notification_log";
            return TransactionComponent::addData($connection,$tablename, $data_rev);
        }

        public static function get_notification_variable($type,$data=  array()){
            $param = array();
            $data['operator'] = isset($data['operator'])?$data['operator']:"";
            $data['number'] = isset($data['number'])?$data['number']:"";
            switch ($type){                
                case "SUCCESS_REGISTRATION":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('user_id'=>$data['user_id'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email']);
                    break;
                case "WALLET_REFILLED_ONLINE":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'user_id'=>$data['user_id'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email']);
                    break;
                case "WALLET_REFILLED_VOUCHER":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                        'user_id'=>$data['user_id'], 'txn_id'=>$data['txn_id']);
                    break;
                case "WALLET_REFILLED_RETAILER":                    
                    $prf = UsersComponent::getuserProfile(0, $data['transaction_id']);                    
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'shop_name'=>$data['retailer_shop'],'bal_flag'=>1,
                            'mobile'=>$data['mobile_number'],'email'=>$prf['email'],'user_id'=>$data['userid'],
                            'retailer_mobile'=>$data['retailer_mobile'],'txn_id'=>$data['transaction_id']);
                    break;
                case "WALLET_TOPUP_REVERSED":
                    $prf = UsersComponent::getuserProfile(0, $data['txn_id']);
                    $param = array('misscall_no'=>'02267242266','amount'=>$data['amount'],'txn_id'=>$data['txn_id'],'bal_flag'=>1,
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'closing'=>$data['closing'],'user_id'=>$data['user_id']);
                    break;
                case "RECHARGE_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                    'balance'=>$data['balance'],'operator'=>$data['operator'],'operator0'=>$data['operator'],
                            'txn_id'=>$data['txn_id'],'number'=>$data['number']);
                    break;
                case "RECHARGE_SUCCESS_NEW":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                    'balance'=>$data['balance'],'operator'=>$data['operator'],'operator0'=>$data['operator'],
                            'txn_id'=>$data['txn_id'],'number'=>$data['number'],'loyalty_balance'=>$data['loyalty_balance'],'loyalty_points'=>$data['loyalty_points']);
                    break;
                case "RECHARGE_FAILURE":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                            'number'=>$data['number'],'txn_id'=>$data['txn_id'],'balance'=>$data['balance'],'operator'=>$data['operator'],
                            'operator0'=>$data['operator'],'bal_flag'=>1,'errCode'=>$data['errCode'],'errmsg'=>$data['errMsg']);
                    break;
                case "RECHARGE_FAILURE_NEW":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                            'number'=>$data['number'],'txn_id'=>$data['txn_id'],'balance'=>$data['balance'],'operator'=>$data['operator'],
                            'operator0'=>$data['operator'],'bal_flag'=>1,'errCode'=>$data['errCode'],'errmsg'=>$data['errMsg'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                
                case "BILLPAYMENT_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['base_amount'],'user_id'=>$data['user_id'],'balance'=>$data['balance'],'number'=>$data['number'],
                            'operator'=>$data['operator'],'txn_id'=>$data['txn_id'],'operator0'=>$data['operator'],'s_amount'=>$data['s_amount'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'service_charge'=>$data['service_charge']);
                    break;
                case "BILLPAYMENT_SUCCESS_NEW":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['base_amount'],'user_id'=>$data['user_id'],'balance'=>$data['balance'],'number'=>$data['number'],
                            'operator'=>$data['operator'],'txn_id'=>$data['txn_id'],'operator0'=>$data['operator'],'s_amount'=>$data['s_amount'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'service_charge'=>$data['service_charge'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                case "BILLPAYMENT_FAILURE":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                            'number'=>$data['number'],'balance'=>$data['balance'],'operator'=>$data['operator'],'txn_id'=>$data['txn_id'],
                            'operator0'=>$data['operator'],'bal_flag'=>1,'errCode'=>$data['errCode'],'errmsg'=>$data['errMsg']);
                    break;
                case "BILLPAYMENT_FAILURE_NEW":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('amount'=>$data['amount'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],
                            'number'=>$data['number'],'balance'=>$data['balance'],'operator'=>$data['operator'],'txn_id'=>$data['txn_id'],
                            'operator0'=>$data['operator'],'bal_flag'=>1,'errCode'=>$data['errCode'],'errmsg'=>$data['errMsg'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                
                case "DEAL_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['userid'], 0);
                    $deal_detail = TransactionComponent::get_Deal_Transaction_details($data['txn_id']);
                    $dealer_prf = UsersComponent::getuserProfile($deal_detail['dealer_id'], 0);
                    if($dealer_prf == null){
                        $dealer_prf['name'] = "";
                    }
                    $param = array('txn_id'=>$data['transaction_id'],'amount'=>$data['amount'],'amount0'=>$data['amount'],
                            'dealer_name'=>$dealer_prf['name'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'user_id'=>$data['user_id'],'balance'=>$data['balance']);
                    break;
                case "DEAL_DETAILS":
                    $prf = UsersComponent::getuserProfile(0, $data['txn_id']);
                    $deal_detail = TransactionComponent::get_Deal_Transaction_details($data['txn_id']);
                    $dealer_prf = UsersComponent::getuserProfile($deal_detail['dealer_id'], 0);
                    if($dealer_prf == null){
                        $dealer_prf['name'] = "";
                    }
                   $param = array('txn_id'=>$data['txn_id'],'amount'=>$deal_detail['amount'],'deal_name'=>$deal_detail['deal_name'],
                            'user_id'=>$deal_detail['users_id'],'amount0'=>$deal_detail['amount'],'dealer_name'=>$dealer_prf['name'],'offer_name'=>$deal_detail['offer_name'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'vcode'=>$deal_detail['code'],'expiry'=>$deal_detail['expiry']);
                  
                    break;
                case "MISSCALL_SUCCESS_USER":
                    $prf = UsersComponent::getuserProfile(0, $data['txn_id']);
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'user_id'=>$prf['user_id'],'bal_flag'=>1,'umobile'=>$data['mobile'],
                            'operator'=>$data['operator'],'operator0'=>$data['operator'],'txn_id'=>$data['txn_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email']);
                    break;
                case "MISSCALL_SUCCESS_USER_NEW":
                    $prf = UsersComponent::getuserProfile(0, $data['txn_id']);
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'user_id'=>$prf['user_id'],'bal_flag'=>1,'umobile'=>$data['mobile'],
                            'operator'=>$data['operator'],'operator0'=>$data['operator'],'txn_id'=>$data['txn_id'],'mobile'=>$prf['mobile'],'email'=>$prf['email'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                case "MISSCALL_FAILURE_USER_NOBAL":
                    $details = WalletComponent::checkWalletBal($data['user_id']);
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('mobile'=>$data['mobile'],'user_id'=>$data['user_id'],'errmsg'=>$data['errmsg'],'amount'=>$data['amount'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'balance'=>$details[0]['balance']);
                    break;
                case "MISSCALL_FAILURE_USER_NOBAL_NEW":
                    $details = WalletComponent::checkWalletBal($data['user_id']);
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('mobile'=>$data['mobile'],'user_id'=>$data['user_id'],'errmsg'=>$data['errmsg'],'amount'=>$data['amount'],
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'balance'=>$details[0]['balance'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                case "MISSCALL_FAILURE_USER":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('mobile'=>$data['mobile'],'user_id'=>$data['userid'],'errmsg'=>$data['errmsg'],'bal_flag'=>1,
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'amount'=>$data['amount'],);
                    break;
                case "MISSCALL_FAILURE_USER_NEW":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('mobile'=>$data['mobile'],'user_id'=>$data['userid'],'errmsg'=>$data['errmsg'],'bal_flag'=>1,
                            'mobile'=>$prf['mobile'],'email'=>$prf['email'],'amount'=>$data['amount'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                case "CHANGE_PASSWORD_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('mobile'=>$prf['mobile'],'email'=>$prf['email'],'user_id'=>$data['user_id']);
                    break;
                case "COMPLAINT_MISSCALL_SUCCESS":
                    $prf = UsersComponent::getuserProfile(0, $data['txn_id']);
                    $param = array('txn_id'=>$data['txn_id'],'comp_no'=>'','mobile'=>$prf['mobile'],'email'=>$prf['email'],'user_id'=>$prf['user_id']);
                    break;
                case "COMPLAINT_MISSCALL_FAILURE":
                    $param = array('errmsg'=>$data['errmsg'],'mobile'=>$data['msisdn']);
                    break;
                case "COMPLAINT_RESOLVED_SUCCESS":
                    $param = array('txn_id'=>'','mobile'=>'');
                    break;
                case "COMPLAINT_RESOLVED_FAILURE":
                    $param = array('txn_id'=>'','comp_no'=>'','mobile'=>'');
                    break;
                case "CHANGE_EMAIL_ID_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['user_id'],0);
                    $param = array('email'=>$prf['email'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile']);
                    break;
                case "CHANGE_PROFILE_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['user_id'],0);
                    $param = array('email'=>$prf['email'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile']);
                    break;
                case "CHANGE_PROFILE_SUCCESS_FB":
                    $prf = UsersComponent::getuserProfile($data['user_id'],0);
                    $param = array('email'=>$prf['email'],'user_id'=>$data['user_id'],'mobile'=>$prf['mobile']);
                    break;
                case "FORGOT_PASSWORD":
                    $param = array('passwd'=>$data['password'],'email'=>$data['email'],'subject'=>'Forgot Password','mobile'=>$data['mobile']);
                    break;
                case "FAILED_TRANSACTION":                    
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);                    
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'operator'=>$data['operator'],'bal_flag'=>1,
                            'errCode'=>$data['errCode'],'user_id'=>$data['user_id'],'errmsg'=>$data['errMsg'],'mobile'=>$prf['mobile'],'number'=>$data['number']);
                    break;
                case "FAILED_TRANSACTION_NEW":                    
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);                    
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'operator'=>$data['operator'],'bal_flag'=>1,
                            'errCode'=>$data['errCode'],'user_id'=>$data['user_id'],'errmsg'=>$data['errMsg'],'mobile'=>$prf['mobile'],'number'=>$data['number'],'loyalty_balance'=>$data['loyalty_balance']);
                    break;
                
                case "WALLET_REFILLED_RETAILER_NONUSER":                    
                    $param = array('amount'=>$data['amount'],'balance'=>$data['balance'],'app_url'=>'','web_url'=>'','mobile'=>$data['mobile_number']);
                    break;
                case "MISSCALL_FAILURE_NON_USER_NOBAL":
                    $param = array('mobile'=>$data['mobile'],'errmsg'=>$data['errmsg']);
                    break;
                case "MISSCALL_FAILURE_NON_USER":
                    $param = array('mobile'=>$data['mobile'],'errmsg'=>$data['errmsg']);
                    break;
                case "CHANGE_PASSWORD_SUCCESS":
                    break;
                case "FREEBIE_FAILURE":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array('user_id'=>$prf['user_id'],'mobile'=>$prf['mobile']);
                    break;
                case "B2C_BY_RETAILER_RECHARGE_SUCC":
                    $param = $data;
                    break;
                case "B2C_BY_RETAILER_RECHARGE_FAIL":
                    $param = $data;
                    break;
                case "NEW_MISS_CALL_USER_MSG":
                    $param = $data;
                    break;
                case "NON_B2B_N_B2C_USER":
                    $param = $data;
                    break;
                case "B2C_EXISTING_USER":
                    $param = $data;
                    break;
                case "APP_EXISTING_USER":
                    $param = $data;
                    break;
                case "FREEBIE_SUCCESS":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array_merge($data,array('user_id'=>$prf['user_id'],'mobile'=>$prf['mobile']));
                    break;
                case "FREEBIE_SUCCESS_NEW":
                    $prf = UsersComponent::getuserProfile($data['user_id'], 0);
                    $param = array_merge($data,array('user_id'=>$prf['user_id'],'mobile'=>$prf['mobile'],'loyalty_balance'=>$data['loyalty_balance']));
                    break;
           
            }
            return $param;
        }

        public static function get_mobileNumbers($emailId=null,$additionCond=array()){
            $Condition='';
            if(isset($additionCond['deviceId']) && $additionCond['deviceId'] != ""){
                    $Condition .= " AND  uuid = '".$additionCond['deviceId']."'";
            }
            if(isset($emailId) && $emailId != ""){
                    $Condition .= " AND email ='".$emailId."'";
            }
            $qry = "SELECT mobile from user_profile where 1=1 ".$Condition;
            try{
                $mobile_numbers = Yii::app()->db->createCommand($qry)->queryAll();
                $result = array("status" => "success","description" => "","data" => $mobile_numbers);            
            } catch (Exception $ex) {
                $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage(), "data" => array());
            }
            return $result;
        }
        //set transaction id for loyalty points
        public static function set_LoyaltyPoints($connection,$data){
            $data['userid'] =isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
            if(isset($data['transaction_id'])){
                 $data['time'] = time();
                Yii::app()->cache->zadd('Loyalty_Points',time(),json_encode($data));
            }
            return true;
        }
        //give loyalty points to user for respective transaction id after 5 min of transaction
        //loyalty points depends on service Table ex: for 1rs mobile recharge 1 loyalty point
         public static function get_LoyaltyPoints($req_data = null){
                $connection = Yii::app()->db;
                $time = isset($req_data['time']) && !empty($req_data['time']) ? $req_data['time'] : 5;
                $timelimit = (60*$time); //give loyalty points after 5 min
                $Loyalty_points =  Yii::app()->params['GLOBALS']['_Loyalty_Points'];
                $servicePoints = "";
                $amount = "";
                $trans_id = "";
             $freecoins = yii::app()->cache->zrangebyscore('Loyalty_Points',$timelimit,'+inf');
             foreach($freecoins as $val){
                 $record = json_decode($val,true);
                 $trans_id = $record['transaction_id'];
                 if(time()-$record['time']>=$timelimit  && isset($record['flag'])){
                 $amount = $record['amount'];
                 $servicePoints = $Loyalty_points[$record['flag']];
                 $transaction = $connection->beginTransaction();
                 $trans_qry = "SELECT trans_id,users_id FROM transactions WHERE trans_id = '$trans_id'";
                 $transdata = $connection->createCommand($trans_qry)->queryAll();
                 $txn_id = $transdata[0]['trans_id']; //------ transaction id
                 $wallet_qry = "SELECT * FROM wallets WHERE users_id =" . $transdata[0]['users_id'] . " FOR UPDATE";
                 $wallet_data = $connection->createCommand($wallet_qry)->queryAll();
                 $query="UPDATE wallets as w 
                            LEFT JOIN 
                            `transactions` as t ON (w.users_id = t.users_id )
                            SET w.loyalty_points = w.loyalty_points + ($amount*$servicePoints)
                            , t.loyalty_points = CEIL($amount*$servicePoints)
                            ,t.free_points = 1
                            WHERE t.trans_id=$trans_id AND t.free_points != 1 ";
                 //yii::app()->cache->zrem('Loyalty_Points',$val);
                    if($connection->createCommand($query)->execute()){
                       yii::app()->cache->zrem('Loyalty_Points',$val);
                    }
                    $transaction->commit();
                 }
                 else{
                     $trans_qry = "SELECT trans_id,users_id,status FROM transactions WHERE trans_id = $trans_id ";
                     $transdata = $connection->createCommand($trans_qry)->queryAll();
                     if(isset($transdata[0]['status']) && $transdata[0]['status']==3){
                         yii::app()->cache->zrem('Loyalty_Points',$val);
                     }
                 }
             }
         }
        //check whether freebee set for user or not
        public static function set_FreeBie($connection,$data){
            $userid =isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
            $params1 = yii::app()->cache->hexists('fb24HR_'.$userid,1);
            if(isset($params1) && ($params1===true)){ //user get only 1 free bee in 24 hours
                $result = array('status'=>'failure','errCode'=>246,'description'=>'Freebie can be used only once a day');
            }
            else{
                   //check number of freebee used is less than set maxlimitperuser in offer
                   $isFreeBeeUsed = false;
                   $offerID = $data['offer_id'];
                   $dealID = $data['deal_id'];
                   $offeTab = $connection->createCommand("SELECT max_limit_peruser FROM `offers` where id=$offerID")->queryAll();
                   $totalfreebee=$connection->createCommand("SELECT id FROM `deal_transaction` "
                            . "where users_id=$userid and deal_id=$dealID and offer_id=$offerID")->queryAll();
                   if($offeTab[0]['max_limit_peruser']==-1){
                           $isFreeBeeUsed=true;
                   }
                   elseif ($offeTab[0]['max_limit_peruser'] >= count($totalfreebee)) {
                           $isFreeBeeUsed=true;
                   }
                   else{
                           $isFreeBeeUsed=false;
                   }
                   if($isFreeBeeUsed){
                           //Yii::app()->cache->hset('fb24HR_'.$userid,1,1);             
                           //Yii::app()->cache->expire('fb24HR_'.$userid,3600*24);//for 24 hrs   
                           Yii::app()->cache->zadd('fb5minQ',time(),json_encode($data));        
                           $result = array('status'=>'success','errCode'=>0,'description'=>array('message'=>'Freebie added successfully'));
                   }
                   else{
                         $result = array('status'=>'failure','errCode'=>247,'description'=>'User already use maximum allocated freebie');
                   }
             }
             return json_encode($result);
        }
        
        //get freebee which already set
        public static function get_FreeBie($req_data = null){
            $responce=array();
            $connection = Yii::app()->db;
            $timelimit = time()-60; //for 1 min
            $freebee = array();
            $freebee = yii::app()->cache->zrangebyscore('fb5minQ','-inf',$timelimit);
            if(count($freebee) > 0){
                foreach($freebee as $val){
                   //add in transaction
                   $data = json_decode($val,true);
                   $transaction_id = $data['transaction_id'];
                   //$chkfb  = self::check_freebie_transaction($data['transaction_id']);
                   $data['ref_id'] = self::get_freebie_ref_id($data);
                   $chkfb  = self::check_freebie_transaction($data['ref_id']);
                   if($chkfb === true ){
                        unset($data['freebie']);
                        $data['paymentopt'] = "wallet";
                        $response = WalletComponent::dealpayment_by_wallet($connection, $data);
                        $fb_response_status = json_decode($response,TRUE);
                        if($fb_response_status['status'] == "success"){
                            $notify_data['deal_id'] = isset($data['deal_id'])?$data['deal_id']:0; 
                            $notify_data['offer_id'] = isset($data['offer_id'])?$data['offer_id']:0;       
                            //$deal_data = DealsComponent::getDealOffersList($data['deal_id'],$data['offer_id']);
                            $deal_data = DealsComponent::getDealName($data['deal_id']);
                            $deal_transaction_data = DealsComponent::getDealTransaction_expiry($transaction_id);
                            $notify_data['fb_name'] = isset($deal_data[0])?$deal_data[0]['name']:"";
                            $notify_data['expiry'] = isset($deal_data[0])?$deal_data[0]['validity']:"";
                            $notify_data['expiry'] = explode(" ", $notify_data['expiry']);
                            $type = array("FREEBIE_SUCCESS");
                            $n_data = array('code'=>(isset($fb_response_status['deal_coupon_code'])?$fb_response_status['deal_coupon_code']:""),'fb_name'=>$notify_data['fb_name'],'expiry'=>$deal_transaction_data['expiry_date']);
                            self::notifier($type, $n_data);
                        }
                   }else{
                       //yii::app()->cache->del('fb24HR_'.$data['userid']);
                       //$data['notify_type'] = "freebie";
                       $data['save_flag'] = 3;
                       $type = array("FREEBIE_FAILURE");
                       self::notifier($type, $data);
                   } 
                }
                yii::app()->cache->zremrangebyscore('fb5minQ','-inf',$timelimit);
                return $responce;
            }
            else{
                $responce = array('status'=>'failure','errCode'=>248,'description'=>'Freebie not found');
                return json_encode($responce);
            }
        }
        
        public static function get_freebie_ref_id($data){
            $userid = $data['userid'];
            $qry    = "SELECT max(trans_id) as ref_id from transactions where trans_id  <= '".$data['transaction_id']."' and users_id='".$userid."' and (trans_category != 'deal' OR (trans_category='deal' AND transaction_mode='wallet'))";
            $result = Yii::app()->db->createCommand($qry)->queryColumn();
            if(count($result) > 0 && count($result[0]) > 0){
                return $result[0];
            }
            return "0";
        }
        
        public static function check_freebie_transaction($transaction_id){
            $connection = Yii::app()->db;
            $qry        = "SELECT trans_id FROM `transactions` where trans_id = $transaction_id and status=2";
            $result     = $connection->createCommand($qry)->queryAll();
            if(count($result) > 0){
                return true;
            }
            else{
                return false;
            }
        }

        //check whether freebee used by user or not
        public static function isFreeBeeUsed($data){
            $user_id  = $data['user_id'];
            $deal_id  = $data['deal_id'];
            $offer_id = $data['offer_id'];
            
            $query    = "SELECT id
                         FROM `deal_transaction`
                         WHERE `users_id` = $user_id
                         AND `deal_id` = $deal_id
                         AND `offer_id` = $offer_id ";
            //check whether user used the deal with same offerid
            $dealsList = Yii::app()->db->createCommand($query)->execute();
            $isUsed =  (!empty($dealsList)) ?true :false;
            return $isUsed;
      }
      
      //get daily transaction
      public static function daily_transaction($data=null){
          $connection  = Yii::app()->db;
          $currentdate = (isset($data['date']) && !empty($data['date'])) ? $data['date'] : date("Y-m-d");
          //get daily blocked amount
          $blocked_query = "SELECT SUM( `account_balance` ) as total_blocked_amt
                            FROM `wallets`
                            WHERE users_id
                            IN (
                            SELECT DISTINCT(users_id)
                            FROM `black_listed_user`
                            ) AND DATE( `created_datetime`) <= '".$currentdate."'";
          //get daily wallet balance
          $wallet_bal_qry = "SELECT SUM( `account_balance` ) as total_wallet_bal
                             FROM `wallets` where DATE( `created_datetime` ) <= '".$currentdate."'";
          //get daily total amount transfer by retailer
          $retailer_topup_qry = "SELECT sum( `transaction_amount` ) as total_retailer_topup
                                    FROM `transactions`
                                    WHERE `trans_category` = 'refill'
                                    AND `transaction_mode` = 'wallet'
                                    AND `trans_type` =1 AND `status`<=2
                                    AND DATE( `trans_datetime`) = '".$currentdate."'";
          //get daily total pg success transfer
          $total_pg_qry = "SELECT sum( `transaction_amount` ) as total_pg
                            FROM `transactions`
                            WHERE `transaction_mode` = 'online'
                             AND `status`=2
                             AND DATE( `trans_datetime`) = '".$currentdate."'";
          
          $total_pg_pending_qry = "SELECT sum( `transaction_amount` ) as total_pg
                            FROM `transactions`
                            WHERE `transaction_mode` = 'online'
                             AND `status`=1
                             AND DATE( `trans_datetime`) = '".$currentdate."'";
          $walletRefillviaRefund = "SELECT sum(t1.transaction_amount) as amount FROM `transactions` as t1 "
                  . " inner join transactions as t2 ON (t2.ref_trans_id = t1.trans_id AND t2.trans_id != t1.trans_id) "
                  . " WHERE t1.`transaction_mode`='online' and t1.`status`=3 AND t1.trans_date = '".$currentdate."' "
                  . "AND t2.transaction_mode = 'wallet' AND t2.status = 4 AND t2.trans_date ='".$currentdate."' ";
          
          $OlderwalletRefillviaRefund = "SELECT sum(t1.transaction_amount) as amount FROM `transactions` as t1 "
                  . " inner join transactions  as t2 ON (t1.ref_trans_id = t2.trans_id) WHERE t2.`status`=3  AND "
                  . "t1.trans_date = '".$currentdate."' AND t1.status = 4  AND t2.trans_date < t1.trans_date";
          
          $blocked_result = $connection->createCommand($blocked_query)->queryRow();
          $wallet_bal_result = $connection->createCommand($wallet_bal_qry)->queryRow();
          $retailer_topup_result = $connection->createCommand($retailer_topup_qry)->queryRow();
          $total_pg_result = $connection->createCommand($total_pg_qry)->queryRow();
          $total_pg_pending_result = $connection->createCommand($total_pg_pending_qry)->queryRow();
          $walletRefillviaRefund_result = $connection->createCommand($walletRefillviaRefund)->queryRow();
          $OlderwalletRefillviaRefund_result = $connection->createCommand($OlderwalletRefillviaRefund)->queryRow();
          
          $blockedAmt = empty($blocked_result['total_blocked_amt']) ? 0 : $blocked_result['total_blocked_amt'];
          $total_wallet_bal = empty($wallet_bal_result['total_wallet_bal']) ? 0 :$wallet_bal_result['total_wallet_bal'];
          $total_retailer_topup = empty($retailer_topup_result['total_retailer_topup']) ? 0 : $retailer_topup_result['total_retailer_topup'];
          $total_online_success = empty($total_pg_result['total_pg']) ? 0 : $total_pg_result['total_pg'];
          $total_online_pending = empty($total_pg_pending_result['total_pg']) ? 0 : $total_pg_pending_result['total_pg'];
          $walletRefund = empty($walletRefillviaRefund_result['amount']) ? 0 : $walletRefillviaRefund_result['amount'];
          $OlderwalletRefund = empty($OlderwalletRefillviaRefund_result['amount']) ? 0 : $OlderwalletRefillviaRefund_result['amount'];
          
          $total_pg = $total_online_success+$total_online_pending;
          
          $query = "INSERT INTO daily_transaction (`transaction_date`,`total_blocked_amt`,`total_retailer_topup`,`total_pg_txn`,`total_wallet_balnce`)
                                       values ('".$currentdate."','".$blockedAmt."','".$total_retailer_topup."','".$total_pg."','".$total_wallet_bal."')";
          if(isset($data['url']) && $data['url']==1){
              $result = array('transaction_date'=>$currentdate,
                                "total_blocked_amt"=>$blockedAmt,
                                "retailer_topup"=>$total_retailer_topup,
                                'online_successfull_txn'=>$total_online_success,
                                'online_pending_txn'=>$total_online_pending,
                                'total_wallet_balnce'=>$total_wallet_bal,
                                'wallet_refill_via_refund_sameDay'=>$walletRefund,
                                'wallet_refill_via_refund_OlderDay'=>$OlderwalletRefund);
              if(count($result)>0){
                  return json_encode($result);
              }else{
                  return json_encode(array('Empty data'));
              }
          }
          try{ 
            if(!$connection->createCommand($query)->execute()){
              $sender = "support@pay1.in";
               $to = "swapnil@mindsarray.com,nandan@mindsarray.com";
              $subject = "daily transaction query gets failed";
              $message ="Please check daily_transaction api";
              self::sendMail($to, $subject, $message,$sender);
            }
            else{ 
                $sender = "support@pay1.in";
                $to = "tadka@mindsarray.com,accounts@mindsarray.com";
                $subject = "daily transaction details";
                $message ="Todays transaction details are as follows, <br> total blocked amount (till now) = Rs ". self::format_amount($blockedAmt)." <br> total wallet balance (till now) = Rs ".self::format_amount($total_wallet_bal)." <br> total amount transfer by retailer in wallet ($currentdate) = Rs ".self::format_amount($total_retailer_topup)." <br> total online transaction ($currentdate) = Rs ".self::format_amount($total_pg)." successfull(Rs ".self::format_amount($total_online_success).")  pending(Rs ".self::format_amount($total_online_pending).")"
                        . " <br> Wallet refill via refund on same day Rs ". self::format_amount($walletRefund). " <br> Wallet refill via refund of other day Rs ". self::format_amount($OlderwalletRefund);
                self::sendMail($to, $subject, $message,$sender);
            }
          }
          catch(Exception $ex){
            $sender = "support@pay1.in";
            $to = "swapnil@mindsarray.com,nandan@mindsarray.com";
            $subject = "exception occure in daily transaction query ";
            $message = json_encode($ex->getTraceAsString());
             self::sendMail($to, $subject, $message,$sender);
          }
          
      }
      
      //search freegift for view
       public static function searchFreeGift($data,$flag=false){
          $msg = $data['code'];
          $userid = Yii::app()->user->id;
          $connection = Yii::app()->db;
         //authorization of sender part1
          $user_query = "SELECT mobile_number
                         FROM users where
                         id ='".$userid."'";
          
         $user_result = $connection->createCommand($user_query)->queryAll();
         if(strlen($user_result[0]['mobile_number'])==13 || strlen($user_result[0]['mobile_number'])==11){
            $sender = substr($user_result[0]['mobile_number'], 0,strlen($user_result[0]['mobile_number'])-1);
         }
         else{
             $sender = $user_result[0]['mobile_number'];
         }
         $data['sender']=$sender;
         $data['message']="red ".$msg;
         
         return self::redeem_Freebie($data,$flag);
       }
      
      //redim free bie by merchant
      public static function redeem_Freebie($data,$searchFreeGift = false){
          //mobile number 10 digits
          //coupon code 6 digits
          $currentDateTime = self::getFormatedDate();
          $currentDate = self::getFormatedDate('Y-m-d');
          $sender = isset($data['sender']) ? trim($data['sender']) : 1;
          $msg = isset($data['message']) ? trim(urldecode($data['message'])) : 1;
          $Offer_id = (isset($data['Offer_id']) && !empty($data['Offer_id'])) ? " AND off.id=".$data['Offer_id'] : "";
          if(!isset($data['sender']) || strlen($sender)<9){
              $result = array('status' => 'failure', 'errCode' => '254', 'description' => "Please provide proper retailer number");
                $content ="Please provide proper retailer number";
                self::sendSMS($sender, $content); 
              return $result;
          }
            $userdata=1;
            if(strpos($msg," ")==3){
                $userdata = explode(" ",$msg);
                $userdata = strtolower(trim($userdata[1]));
            }
            $connection = Yii::app()->db;
            //authorization of sender part1
            $auth_query = "SELECT count(1) AS cnd,dealer_id
                          FROM users AS u
                          INNER JOIN deals AS d ON u.id = d.dealer_id
                          AND ( u.mobile_number ='".$sender."0')";
            $auth_result = $connection->createCommand($auth_query)->queryAll(); 

            if($auth_result[0]['cnd']==0){
                $content ="Please send sms from your registered mobile number";
                $result = array('status' => 'failure', 'errCode' => '260', 'description' => $content);
                self::sendSMS($sender, $content); 
                return $result;
            }
            
          if(!isset($data['message']) || strpos($msg," ")!=3){
              $content ="Please provide message in proper format. Eg, RED usernumber  OR RED code";
              $result = array('status' => 'failure', 'errCode' => '255', 'description' => $content);
              
              self::sendSMS($sender, $content); 
              return $result;
          }
          if(strlen($userdata)!=10 && strlen($userdata)!=8 ){
              $content ="Please provide message in proper format. Eg, RED usernumber  OR RED code";
              $result = array('status' => 'failure', 'errCode' => '255', 'description' => $content);
              self::sendSMS($sender, $content); 
              return $result;
            }
            //get couponid
             $query = "SELECT c.id as coupon, c.code,c.confirmation_date as redeemDate,dt.expiry_date as exDate,d.img_url, u.user_name, c.status as status, DATE(dt.expiry_date) as ex_date,u.mobile_number,off.name
                        FROM deal_transaction as dt 
                        LEFT JOIN users as u
                        on (dt.users_id = u.id)
                        LEFT JOIN coupons as c
                        on c.id= dt.coupon_id
                        LEFT JOIN offers as off
                        ON off.id = c.offer_id
                        LEFT JOIN deals as d
                        on d.id=dt.deal_id
                        where (u.mobile_number ='".$userdata."' or  LOWER(c.code)='".$userdata."' ) 
                        AND c.users_id = dt.users_id
                        AND d.dealer_id=".$auth_result[0]['dealer_id'].
                        " $Offer_id "." group by dt.deal_id,dt.offer_id
                        order by dt.id asc"; //and dt.deal_id in (".$auth_result[0]['dealid'].") 
            $result = $connection->createCommand($query)->queryAll();
            if($searchFreeGift===true){
                return $result;
            }
            /*
             *  else if($result[0]['ex_date']>$currentDate){
                  $query = "update coupons SET status =3 where id=".$result[0]['coupon'];
                  $connection->createCommand($query)->execute();
                  return "coupon expired already";
              }
             */
            //check status
            if(!empty($result)){
                $code = $result[0]['code'];
                $mobile_number = isset($result[0]['mobile_number']) ? substr_replace($result[0]['mobile_number'],'*******',0,-4) : "";
              if(count($result)>1){
                  $flag=false;
                  foreach($result as $row){
                      if($row['status']<2){
                          $flag=true;
                      }
                  }
                  if($flag===false){
                      $content ="All coupons belonging to this user $mobile_number are already redeemed.";
                      $msg = array('status' => 'failure', 'errCode' => '256', 'description' => $content);
                        $content = urlencode($content);
                        self::sendSMS($sender, $content);
                        return $msg;
                  }
                  
                  $content ="Please send one of the below mentioned code to redeem";
                   $i=1;
                  foreach($result as $row){
                      if($row['status']<2){
                        $content .="\r\n".$i." ".$row['name']." '".$row['code']."'";
                        $i++;       
                      }
                  }
                  $content .= "\r\n"."Send sms RED Code to 9223178889 or 9004350350";
                  $msg = array('status' => 'failure', 'errCode' => '256', 'description' => $content);
                  $content = urlencode($content);
                  self::sendSMS($sender, $content);
                  return $msg;
                  
              }
              else if($result[0]['status']>=2){
                 $content ="Coupon no. $code belonging to this user $mobile_number already redeemed";
                  $result = array('status' => 'failure', 'errCode' => '257', 'description' => $content);
                  self::sendSMS($sender, $content);
                  return $result;
              }
               else if($result[0]['ex_date']<$currentDate){
                  $query = "update coupons SET status =3,confirmation_date='$currentDateTime' where id=".$result[0]['coupon'];
                  $connection->createCommand($query)->execute();
                  $content = "Coupon no. $code belonging to this user $mobile_number already expired";
                  $result = array('status' => 'failure', 'errCode' => '263', 'description' => $content);
                  self::sendSMS($sender, $content);
                  return $result;
              }
              else{
                  $query = "update coupons SET status =2,confirmation_date='$currentDateTime' where id=".$result[0]['coupon'];
                  $connection->createCommand($query)->execute();
                  $content ="Coupon no. $code successfully redeemed for user $mobile_number";
                  $result = array('status' => 'success', 'errCode' => '258', 'description' => $content);
                  self::sendSMS($sender, $content);
                  return $result;
              }
            }
            else{
                if(strlen($userdata)==10){
                    $content = "There is no free gift available for this user $userdata";
                }else{
                    $content = "There is no free gift available for this code $userdata";
                }
                
                $result = array('status' => 'failure', 'errCode' => '259', 'description' => $content);
                 self::sendSMS($sender, $content);
                return $result;
            }
      }
      
      public static function getCoupons($data){
          $connection = Yii::app()->db;
          $dealerId = Yii::app()->user->id;
          if(isset($data['custMobNum']) && !empty($data['custMobNum'])){
              $query = "SELECT  c.id,c.code ,of.name,of.id as offer_id,d.id as deal_id,d.img_url,dt.expiry_date as exDate,c.confirmation_date as redeemDate,c.status FROM coupons as c "
                  . " INNER JOIN users as u ON (u.id=c.users_id) "
                  . " INNER JOIN offers as of ON (of.id=c.offer_id) "
                  . " INNER JOIN deal_transaction as dt ON (dt.coupon_id = c.id) "
                  . " INNER JOIN deals as d ON (d.id=c.deal_id) "
                  . " WHERE of.status =1 AND d.status=1 AND u.user_name=".$data['custMobNum']." AND d.dealer_id = $dealerId " ;
               $result = $connection->createCommand($query)->queryAll();
          }
          elseif(isset($data['voucher']) && !empty($data['voucher'])){
              $code = $data['voucher'];
              $query = "SELECT  c.id,c.status,c.code,of.name,of.id as offer_id,d.id as deal_id,d.img_url,dt.expiry_date as exDate,c.confirmation_date as redeemDate,c.status FROM coupons as c "
                  . " INNER JOIN offers as of ON (of.id=c.offer_id) "
                  . " INNER JOIN deal_transaction as dt ON (dt.coupon_id = c.id) "
                  . " INNER JOIN deals as d ON (d.id=c.deal_id) "
                  . " WHERE of.status =1 AND d.status=1 AND c.code='$code'"." AND d.dealer_id = $dealerId ";
            $result = $connection->createCommand($query)->queryAll();
          }
         if(count($result)>0){
             return $result;
         }
         return array();
      }
      
      public static function redeem_coupons($data){
          try{
            $couponId = $data['code'];
            $offerId = $data['offerId'];
            $dealId = $data['dealId'];
            $connection = Yii::app()->db;
            $dealer_Q = "SELECT u.mobile_number FROM deals d INNER JOIN users as u "
                    . " ON (d.dealer_id=u.id) WHERE d.id=$dealId";
            $dealer = $connection->createCommand($dealer_Q)->queryRow();
            
            $query = "SELECT u.user_name,u.mobile_number,c.status,DATE(dt.expiry_date) as expiry_date,c.code,c.id"
                    . " FROM coupons as c "
                    . " INNER JOIN deal_transaction as dt ON (dt.coupon_id=c.id) "
                    . "INNER JOIN users as u ON (u.id = c.users_id) "
                    . "WHERE c.id=$couponId AND dt.offer_id=$offerId AND dt.deal_id=$dealId";
            $result = $connection->createCommand($query)->queryRow();
            $sender = $dealer['mobile_number'];
            if(isset($result['code']) && !empty($result['code'])){
                return self::redeem_message($connection,$result,$sender);
            }else{
                //This coupon code is invalid
                  $content ="Invalid coupon code";
               return  $result = array('status' => 'failure', 'errCode' => '269', 'description' => $content);
            }
          }
          catch(Exception $ex){
             return $result = array('status'=>'failure','errCode'=>'249','description'=>'no data found');
          }
          
      }
      
      public static function redeem_message($connection,$result,$sender){
              $mobile_number = isset($result['user_name']) ? substr_replace($result['user_name'],'*******',0,-4) : "";
              $code = $result['code'];
              $currentDateTime = self::getFormatedDate();
              $couponId = $result['id'];
              $currentDate = self::getFormatedDate('Y-m-d');
              if($result['expiry_date']<$currentDate){
                  $query = "update coupons SET status =3,confirmation_date='$currentDateTime' where id=".$couponId;
                  $connection->createCommand($query)->execute();
                  $content = "Coupon no. $code belonging to this user $mobile_number already expired";
                  $result = array('status' => 'failure', 'errCode' => '263', 'description' => $content);
                  self::sendSMS($sender, $content);
                  return $result;
              }
              else if($result['status']>=2){
                 $content ="Coupon no. $code belonging to this user $mobile_number already redeemed";
                  $result = array('status' => 'failure', 'errCode' => '257', 'description' => $content);
                  self::sendSMS($sender, $content);
                  return $result;
              }
               else{
                  $query = "update coupons SET status =2,confirmation_date='$currentDateTime' where id=".$couponId;
                  $connection->createCommand($query)->execute();
                  $content ="Coupon no. $code successfully redeemed for user $mobile_number";
                  $result = array('status' => 'success', 'errCode' => '258', 'description' => $content);
                  self::sendSMS($sender, $content);
                  return $result;
              }
      }
      
      public static function redeem_FreebieByApp($data){
          try{
                $currentDate = self::getFormatedDate('Y-m-d');
                $currentDateTime = self::getFormatedDate();
                $connection = Yii::app()->db;
                $coupon  = isset($data['coupon']) ? $data['coupon']:1;
                $userMobile = isset($data['mobile']) ? $data['mobile'] : 1;
                $dealid = isset($data['dealid']) ? $data['dealid'] : 1;
                $userid = isset($data['userid']) ? $data['userid'] : Yii::app()->user->id;
                //verify merchant
                $password = $data['password'];

                $qry = "SELECT u.mobile_number, u.password
                          FROM users AS u
                          INNER JOIN deals AS d ON ( u.id = d.dealer_id )
                          WHERE u.password = MD5( '$password' ) AND d.id=$dealid
                          LIMIT 0 , 1";
                $merchant_result = $connection->createCommand($qry)->queryAll();

                if(count($merchant_result)>0){
                    if(strlen($merchant_result[0]['mobile_number'])==13 || strlen($merchant_result[0]['mobile_number'])==11){
                      $sender = substr($merchant_result[0]['mobile_number'], 0,strlen($merchant_result[0]['mobile_number'])-1);
                   }
                   else{
                       $sender = $merchant_result[0]['mobile_number'];
                   }
                    //redeem free gift
                    $query = "SELECT c.id, c.code,u.user_name, c.status as status, DATE(dt.expiry_date) as expiry_date,u.mobile_number,off.name
                              FROM deal_transaction as dt 
                              LEFT JOIN users as u
                              on (dt.users_id = u.id)
                              LEFT JOIN coupons as c
                              on c.id= dt.coupon_id
                              LEFT JOIN offers as off
                              ON off.id = c.offer_id
                              LEFT JOIN deals as d
                              on d.id=dt.deal_id
                              WHERE (u.id =$userid AND c.code='$coupon' AND dt.deal_id=$dealid) LIMIT 0 , 1";
                    $result = $connection->createCommand($query)->queryRow();
                    if(count($result)>0){
                        return self::redeem_message($connection,$result,$sender);
                    }
                }
                else{
                    $content = "please enter valid password";
                      $result = array('status' => 'failure', 'errCode' => '263', 'description' => $content);
                      //self::sendSMS($sender, $content);
                      return $result;
                }
                //please enter valid password
          }
          catch(Exception $ex){
             return $result = array('status'=>'failure','errCode'=>'249','description'=>'no data found');
          }
      }
      
      public static function format_amount($amount){
            setlocale(LC_MONETARY, 'en_IN'); 
            $amount = money_format('%!i', $amount);
            return $amount;
      }
      
      public static function announce_freeGift($data){
          $connection = Yii::app()->db;
          $query = "INSERT INTO FreeGift_announcer (`image`,`details`,`users_id`,`created_datetime`,`updated_datetime`)"
                  . "VALUES ('".$data['image']."','".$data['details']."','".$data['users_id']."','".$data['created_datetime']."','".$data['updated_datetime']."')";
         //exit; 
         if(!$connection->createCommand($query)->execute()){
              return FALSE;
          }  
          return TRUE;
      }
  
      public static function get_errorMsg_list(){
          $result = array();
          $result['BILLPAYMENT_FAILURE']=htmlspecialchars(Yii::app()->params['message_template']['BILLPAYMENT_FAILURE']);
          $result['RECHARGE_FAILURE']=htmlspecialchars(Yii::app()->params['message_template']['RECHARGE_FAILURE']);
          $result['WALLET_REFILLED_RETAILER']=htmlspecialchars(Yii::app()->params['message_template']['WALLET_REFILLED_RETAILER']);
          return $result;
      }
      
      public static function validate_transaction($ref_id){
          $ref_id = (isset($ref_id) && !empty($ref_id)) ? $ref_id : 1;
          $connection = Yii::app()->db;
          $userid = Yii::app()->user->id;
          $query = "SELECT `transaction_id`
                    FROM `deal_transaction`
                    WHERE `users_id` =$userid AND ref_trans_id=$ref_id";
          $result = $connection->createCommand($query)->queryAll();
          if(count($result) >0){
              return true;
          }
          return FALSE;
      }
      public static function send_message($data,$message,$notificationMsg=null){
          $mobile = $data['mobile'];
          $query="SELECT id,user_name FROM users where mobile_number=$mobile";
          $result = Yii::app()->db->createCommand($query)->queryAll();
          $type = '';
          $content = $message;
          if(count($result)>0){
               //file_put_contents("/tmp/getUsersforannoucement11", "| : test ".json_encode($data)."\n", FILE_APPEND | LOCK_EX);
              $id = $result[0]['id'];
              $type = 'notification';
              foreach ($data as $var=>$val){
                    $notificationMsg = GeneralComponent::replaceword('<'.strtoupper($var).'>', $val, $notificationMsg);
              }
              $notification = self::sendNotification(array($id),'Pay1 Notification',$notificationMsg,'notify',1,0);
                //file_put_contents("/tmp/getUsersforannoucement11", "| : test ".$notification."\n", FILE_APPEND | LOCK_EX);
              if(empty($notification)){
                  foreach ($data as $var=>$val){
                    $content = GeneralComponent::replaceword('<'.strtoupper($var).'>', $val, $content);
                  }
              
                  $api_param = array('msisdn' => $mobile, 'message' => $content);
                VendorComponent::callsmstadka($api_param);
              // self::sendSMS($mobile,$content);
                  $type = 'sms';
                  $nlog_data = array('mobile'=>$data['mobile'],'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>$type,
                    'msg_content'=>  addslashes($content),'status'=>1);
                 // file_put_contents("/tmp/getUsersforannoucement11", "| : ".json_encode($nlog_data)."\n", FILE_APPEND | LOCK_EX);
            self::log_notifications($nlog_data);
            return true;
              }
             // file_put_contents("/tmp/getUsersforannoucement12", "| : ".json_encode($notification)."\n", FILE_APPEND | LOCK_EX);
             // $responce = json_decode($notification,true);
              if(is_array($notification)){
                  $responce = $notification;
              }else{
                  $responce = json_decode($notification,true);
              }
              
              if($responce['status']!='success'){
                  $api_param = array('msisdn' => $mobile, 'message' => $content);
                   VendorComponent::callsmstadka($api_param);
                  //self::sendSMS($mobile,$content);
                  $type = 'sms';
              }
                    
            $nlog_data = array('mobile'=>$data['mobile'],'sendtime'=>  GeneralComponent::getFormatedDate(),'type'=>$type,
                    'msg_content'=>  addslashes($content),'status'=>1);
            self::log_notifications($nlog_data);
          }
      }
      
      public static function nongenuineUser($month=null,$year=null){
          $month = is_null($month)?date('m'):$month;
          $year  = is_null($year)?date('Y'):$year;
          $chunk_val = 100;
          $connection = Yii::app()->db;
          $transaction = $connection->beginTransaction();
          try{
                $query = "SELECT group_concat(users) as users FROM ("
                        . " SELECT transactions.users_id as users "
                        . "FROM `transactions` left join users "
                        . "ON (users.id = users_id) WHERE month(trans_date) = '$month' AND year(trans_date) = '$year' "
                        . "AND trans_category = 'recharge' AND `transactions`.status = 2 "
                        . "group by transactions.users_id having (!(sum(transaction_amount) <= 10000 AND count(trans_date) <= 15 AND count(trans_id) <=20))) as t";
                $result = $connection->createCommand($query)->queryRow();
                if(isset($result['users']) && !empty($result['users'])){
                  $res = explode(',',$result['users']);
                  $chunkArray = array_chunk($res,$chunk_val);
                  foreach ($chunkArray as $arr) {
                   $result =  trim(implode(",", $arr),",");
                   $connection->createCommand("update `user_profile` SET `is_genuine`=0 where `is_genuine`=1 AND `user_id` in (".$result.")")->execute();  
                   }  
                  }
                  $transaction->commit();
          }
           catch (Exception $ex){
               $transaction->rollback();
                $sender = "swapnil@mindsarray.com";
                $to = "swapnil@mindsarray.com";
                $subject = "exception occure in nongenuineUser query ";
                $message = json_encode($ex->getTraceAsString());
                GeneralComponent::sendMail($to, $subject, $message,$sender);
           }
          }
          
          public static function getGiftXoxoVoucher($data = null){
           //   $client = new nusoap_client("http://www.giftxoxo.com/index.php?route=account/perkvoucherordertest&wsdl", 'wsdl');// for dev server
              $client = new nusoap_client("http://www.giftxoxo.com/index.php?route=account/egv&wsdl", 'wsdl');
                $err = $client->getError();
                if ($err) {

                        echo '<p><b>Error: ' . $err . '</b></p>';

                }
                $voucher_name =$data['voucherName'];// "Flipkart E-Gift Voucher"; //Voucher Name Want to buy

                $amount = $data['actual_price'];//"5"; //Price of voucher

                $quantity = $data['quantity'];//"10"; //Quantity Want to buy

                $username = Yii::app()->params['GLOBALS']['xoxo_voucher_user'];//Pay1.in

                $password = Yii::app()->params['GLOBALS']['xoxo_voucher_password']; //Password Given From Giftxoxo 3jhg3iug5u34

                $server_details = $_SERVER['SERVER_NAME'];
                //$key = "982hkhk23";
                $key = "pay".(microtime(true)*10000).substr(uniqID(),-2);
                $args = array('vname' => $voucher_name, 'vamount' => $amount, 'vquantity' => $quantity,'username' => $username,'password' => $password,'server'=>$server_details,'key'=>$key);
                file_put_contents("/var/log/apps/step". date('Ymd') . ".log", "| Service api params 9".  json_encode($args)."\n", FILE_APPEND | LOCK_EX);
               // $return = $client->call('ControllerAccountPerkvoucherordertest.pollServer', $args); // for dev server
                $return = $client->call('ControllerAccountEgv.pollServer', $args); //for production server
                $data = json_decode($return);
                file_put_contents("/var/log/apps/step". date('Ymd') . ".log", "| Service api params 10".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
                return $data;
          }
          
          public function sendSlowLogData() {
              try{
                $connection = Yii::app()->db;
                $data = $connection->createCommand("SELECT DISTINCT sql_text,user_host,start_time,query_time
                    FROM mysql.slow_log
                    WHERE DATE_FORMAT(start_time,  '%Y-%m-%d' ) =  '".date('Y-m-d')."'
                    AND db='b2c_app1' ORDER BY query_time DESC LIMIT 20")->queryAll();
                $subject = "Slow query";
                $body = "";
                file_put_contents("/var/log/apps/sendSlowLogData". date('Ymd') . ".log", "| output   ".  json_encode($data)."\n", FILE_APPEND | LOCK_EX);
                if (count($data) > 0) {
                    foreach ($data as $val) {
                        file_put_contents("/var/log/apps/sendSlowLogData". date('Ymd') . ".log", "| error  ".  json_encode($val['sql_text'])."\n", FILE_APPEND | LOCK_EX);
                    $body .="Query:&nbsp;&nbsp;&nbsp;" . $val['sql_text'] . "<br/></br/>Host Machine:&nbsp;&nbsp;&nbsp;" . $val['user_host'] . "<br/><br/>Start time:&nbsp;&nbsp;&nbsp;" . $val['start_time'] . "<br/><br/>Execution time:&nbsp;&nbsp;&nbsp;" . $val['query_time'] . "<br/><br/>";
                    }
                    $to= 'swapnil@mindsarray.com';
                    $sender = 'swapnil@mindsarray.com';
                    self::sendMail($to, $subject, $body);
                }
          }
            catch (Exception $ex){
                file_put_contents("/var/log/apps/sendSlowLogData". date('Ymd') . ".log", "| error  ".  json_encode($ex)."\n", FILE_APPEND | LOCK_EX);
            }
        }
        
        public function bank_list(){
            $connection = Yii::app()->db;
            $query = "SELECT bank,code FROM banklist ORDER BY order_flag ";
            return $connection->createCommand($query)->queryAll();
        }
        
        public function genuineUser(){
            try{
                    
                    $startdate  = date('Y-m-d',mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")));
                    $endate = date('Y-m-d');
                    $connection = Yii::app()->db;
                    $transaction = $connection->beginTransaction();  
                    $query = "SELECT up.user_id as userID,up.is_genuine, up.mobile as mobile, up.created as created, up.updated as updated, SUM( transaction_amount ) AS recharge_amt, COUNT( 1 ) AS frequency
                        FROM user_profile AS up
                        INNER JOIN transactions AS txn ON ( txn.users_id = up.user_id) 
                        WHERE txn.status =2
                        AND txn.trans_category
                        IN (
                        'recharge',  'billing'
                        )
                        AND (
                        DATE( txn.trans_date ) 
                        BETWEEN '$startdate' AND '$endate' 
                        )
                        GROUP BY 1 , 2, 3, 4 ";
                    $result =  $connection->createCommand($query)->queryAll();
                    $connection->createCommand()->truncateTable('genuine_user');
                    
                    $query_g = Yii::app()->db->createCommand("SELECT users_id as userID, COUNT( 1 ) AS total_gifts, SUM( 
                    CASE WHEN STATUS =2
                    THEN 1 
                    ELSE 0 
                    END ) AS total_redeem
                    FROM coupons
                    GROUP BY 1 ")->queryAll();
                    
                    $list = ReportingComponent::getMerge($result,$query_g);

                    $insert = "INSERT INTO genuine_user (userID,is_genuine,mobile,created,updated,recharge_amt,frequency,total_gifts,total_redeem) values ";
                    foreach($list as $row){
                        $userId = $row['userID'];
                        $is_genuine = $row['is_genuine'];
                        $mobile = $row['mobile'];
                        $created = $row['created'];
                        $updated = $row['updated'];
                        $recharge_amt = $row['recharge_amt'];
                        $frequency = $row['frequency'];
                        $total_gifts = $row['total_gifts'];
                        $total_redeem = $row['total_redeem'];
                        $insert .= " ($userId,$is_genuine,'$mobile','$created','$updated',$recharge_amt,$frequency,$total_gifts,$total_redeem) , ";
                    }
                    $insert = rtrim($insert,' ,');
                    file_put_contents("/var/log/apps/genuineUser". date('Ymd') . ".log", "| error  ".  json_encode($insert)."\n", FILE_APPEND | LOCK_EX);
                    $connection->createCommand($insert)->execute();
                    $transaction->commit();
                }
               catch (Exception $ex){
                    $transaction->rollback();
                    file_put_contents("/var/log/apps/genuineUser". date('Ymd') . ".log", "| error  ".  json_encode($insert)."\n", FILE_APPEND | LOCK_EX);
                }
            
        }
        
        public static function send_coupon_code($data){
            try{
                $mobile = $data['mobile'];
                $txn_id = $data['txn_id'];
                $result = VoucherComponent::get_coupondetails($data);
                $deal = $result['description'][0]['deal'];
                $coupon = $result['description'][0]['code'];
                $expiry = $result['description'][0]['expiry_date'];
                $pin = $result['description'][0]['pin'];
                $content = "Your vocher code for gift $deal is $coupon ";
                if(!empty($pin)){
                    $content .= "and Pin is $pin";
                }
                 //file_put_contents("/tmp/sms". date('Ymd') . ".log", "| error  ". $mobile." data ". urlencode($content)."\n", FILE_APPEND | LOCK_EX);
                self::sendSMS($mobile, ($content));
                $result = array('status'=>'success','errCode'=>0,'description'=>"SMS send successfully");
            }
            catch(Exception $ex){
                $result = array('status'=>'failure','errCode'=>230,'description'=>$ex->getMessage());
            }
            return $result;
        }
        
        public static function getMonthStartDate(){
            $d = date('Y-m');
            $date=date_create("$d-01");
            return date_format($date,"Y-m-d");
        }
    }
