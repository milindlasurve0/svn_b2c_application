<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportingComponent
 *
 * @author nandan
 */
class ReportingComponent {
    //put your code here    
    public static function get_newUsers($from,$to){
        $start_date = $from." 00:00:00";
        $end_date = $to." 23:59:59";
        $qry = "SELECT count(1) as total_users from users "
                . " WHERE created_datetime >='$start_date' AND created_datetime <='$end_date' AND salt != '0' and salt != ''";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        return $data['0']['total_users'];
    }
    
    public static function get_totalUsers(){
        //$qry = "select TABLE_ROWS as total_users from information_schema.tables where TABLE_SCHEMA='b2c_app1' AND TABLE_NAME='users'";
        $qry = "select count(1) as total_users from users where salt != '0' and salt != '' ";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        return $data['0']['total_users'];
    }
    
    public static function get_repeatedUsers_count($data){
        $data['sd'] = $data['start_date'];
        $data['ed'] = $data['end_date'];
        $qry = self::get_repeated_users_qry($data);
        return Yii::app()->db->createCommand($qry)->execute();
    }
    
    public static function get_trans_category(){
        return array('recharge','billing','deal','refill');
    }

    public static function get_totalTransdetails($from,$to){
        $start_date = $from;
        $end_date = $to;
        $category_arr = self::get_trans_category();
        $mode_arr = array('online','wallet','voucher');
        $cond = " trans_date <= '$end_date' AND trans_date >= '$start_date' AND status=2 ";
        $qry = "SELECT trans_category as category,transaction_mode as mode, sum(1) as total_transaction,sum(transaction_amount) as total_amount FROM `transactions` WHERE $cond group by 1,2";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $new_data = array();
        foreach ($data as $k=>$v){
                $new_data[$v['category']][$v['mode']] = array('total_transaction'=>$v['total_transaction'],'total_amount'=>$v['total_amount']);
        }        
        $category_diff = array_diff($category_arr, array_keys($new_data));
        if(count($category_diff) > 0){
            foreach($category_diff as $cv){
                $new_data[$cv]['wallet']['total_transaction']=0;
                $new_data[$cv]['online']['total_amount']=0;
                $new_data[$cv]['online']['total_transaction']=0;
                $new_data[$cv]['wallet']['total_amount']=0;
            }
        }
        foreach($new_data as $nk=>$nv){
            $mode_diff = array_diff($mode_arr, array_keys($new_data[$nk]));
            if(count($mode_diff) > 0){
                foreach ($mode_diff as $mv){
                    $new_data[$nk][$mv]['total_transaction']=0;
                    $new_data[$nk][$mv]['total_amount']=0;
                }
            }
        }        
        $mode_diff = array_diff($mode_arr, array_keys($new_data));
        return $new_data;
    }
    
    public static function format_report_data($new_data_detail ,$dataArray){
        $data_type = array('count'=>'transaction','amount'=>'amount');
        $category_arr = self::get_trans_category();
        foreach ($category_arr as $categry){
            foreach($data_type as $k=>$v){
                $dataArray[$categry.'_total_'.$k] = $new_data_detail[$categry]['online']['total_'.$v] + $new_data_detail[$categry]['wallet']['total_'.$v] + $new_data_detail[$categry]['voucher']['total_'.$v];
                $dataArray[$categry.'_wallet_'.$k] = $new_data_detail[$categry]['wallet']['total_'.$v];
                $dataArray[$categry.'_online_'.$k] = $new_data_detail[$categry]['online']['total_'.$v];
                $dataArray[$categry.'_voucher_'.$k] = $new_data_detail[$categry]['voucher']['total_'.$v];
            }
        }
        return $dataArray;
    }
    
    public static function get_detailed_report($data){        
        $type_arr = explode("_",$data['type']);
        $data['req_type'] = $type_arr['1'];
        $data['mode'] = $type_arr['0'];
        $connection = Yii::app()->db;
        switch($data['req_type']){
            case "user":
                $qry = "SELECT u.created_datetime as intime, up.name,up.mobile,up.email,up.gender,up.date_of_birth,up.longitude,up.latitude,up.manufacturer,up.os_info,up.device_type FROM `users` as u left join `user_profile` as up on u.id=up.user_id "
                        . "WHERE u.created_datetime >='".$data['sd']." 00:00:00' AND u.created_datetime <='".$data['ed']." 23:59:59'";
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "NEW USER DETAIL";
                break;
            case "billing":
                $qry = self::get_trans_operatorwise_qry($data);
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "OPERATOR WISE BILLING TRANSACTION DETAIL";
                break;
            case "recharge":
                $qry = self::get_trans_operatorwise_qry($data);
                //file_put_contents("/tmp/get_detailed_report", "| noti-array ".$qry."\n", FILE_APPEND | LOCK_EX);
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "OPERATOR WISE RECHARGE TRANSACTION DETAIL";
                break;
            case "refill":
                $qry = self::get_refill_detail_qry($data);
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "TOP-UP/REFILL TRANSACTION DETAIL";
                break;
            case "repeated":
                $qry = self::get_repeated_users_qry($data);
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "REPEATED USERS DETAILS";
                break;
            case "deal":
                $qry = self::get_wallet_deal_qry($data);
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "OFFER WISE DEAL TRANSACTION DETAILS";
                break;
            case "directTransaction":
                $qry = self::get_Direct_Transaction($data);
                //file_put_contents("/tmp/process_transaction_after_pg", "| noti-array ".$qry."\n", FILE_APPEND | LOCK_EX);
                $data_res['data'] = $connection->createCommand($qry)->queryAll();
                $data_res['count'] = $connection->createCommand($qry)->execute();
                $data_res['title'] = "DIRECT TRANSACTION DETAILS";
                break;
        }
        return $data_res;
    }
    
    public static function get_trans_operatorwise_qry($data){
        $mode_cond = "";
        $type_cond = "";
        if(in_array($data['mode'],array('wallet','online'))){
            $mode_cond = " AND txn.transaction_mode='".$data['mode']."' ";
        }
        $trans_type_cond = " AND txn.trans_category='".$data['req_type']."' ";
        
        if($data['req_type'] == "billing"){
            $case_qry = "WHEN trans_category='billing' and payment_flag = 1
                        THEN topr1.postpaid
                        ELSE 0";            
        }elseif ($data['req_type'] == "recharge") {
            $case_qry = "WHEN trans_category='recharge' 
                            THEN 
                                (CASE
                                WHEN rt.recharge_flag = 1
                                THEN topr.product_id
                                ELSE
                                    (CASE
                                    WHEN rt.recharge_flag = 2
                                    THEN dopr.product_id
                                    ELSE
                                        (CASE
                                        WHEN rt.recharge_flag = 3
                                        THEN topr.data_product_id
                                        ELSE 0
                                        END)
                                    END)
                                END)
                            ELSE 0";
        }else{
            echo "Invalid Request";
            exit();
        }        
        $qry = "SELECT prod.name, t1.cnt, t1.total
                FROM (SELECT
                (CASE 
                $case_qry
                END) as product ,count(1) as cnt, sum(txn.transaction_amount) as total
                FROM `transactions` as txn 
                 LEFT JOIN `billing_transaction` as bt ON txn.ref_trans_id=bt.transaction_id 
                 LEFT JOIN `recharge_transaction` as rt ON txn.ref_trans_id = rt.transaction_id
                 LEFT JOIN `telecom_operators` as topr ON rt.operator_id=topr.id 
                 LEFT JOIN `dth_operators` as dopr ON rt.operator_id=dopr.id
                 LEFT JOIN `telecom_operators` as topr1 ON bt.operator_id=topr1.id 
                 WHERE txn.trans_date >='".$data['sd']."' AND txn.trans_date <='".$data['ed']."' "
                . "AND txn.status=2 $mode_cond $trans_type_cond 
                    GROUP BY 1
                ) as t1 
                LEFT JOIN products as prod ON prod.id=t1.product";
        return $qry;
    }
    
    public static function get_refill_detail_qry($data){
        $mode_cond = " AND txn.transaction_mode='".$data['mode']."' ";
        if($data['mode'] == "total"){
            $mode_cond = "";
        }                
        $qry = "SELECT txn.trans_id, txn.trans_datetime, u.mobile_number,u.created_datetime, txn.transaction_amount, 
                (CASE 
                    WHEN transaction_mode='wallet' 
                    THEN rft.retailer_mobile 
                    ELSE (CASE 
                        WHEN transaction_mode='online'
                        THEN pg.mihpayid 
                        ELSE 0
                        END)
                 END) as ref_num 
                 FROM transactions as txn
                    LEFT JOIN refill_transaction as rft ON txn.trans_id = rft.transaction_id
                    LEFT JOIN pg_payuIndia as pg ON txn.trans_id = pg.txnid
                    LEFT JOIN users as u ON txn.users_id = u.id 
                    WHERE txn.trans_date >='".$data['sd']."' AND txn.trans_date <='".$data['ed']."' 
                        AND txn.status = '2' AND txn.trans_category='".$data['req_type']."' ".$mode_cond;
        return $qry;
    }
    
    public static function get_deal_detail_qry($data){
        $mode_cond = " AND txn.transaction_mode='".$data['mode']."' ";
        if($data['mode'] == "total"){
            $mode_cond = "";
        }
        $qry = "SELECT txn.trans_id, txn.trans_datetime, u.mobile_number, txn.transaction_amount, 
                 d.name, dt.deal_id,dt.offer_id,concat('XXXXXX',RIGHT(cpn.code,4)) as couponcode
                 FROM transactions as txn
                    LEFT JOIN deal_transaction as dt ON txn.trans_id = dt.transaction_id
                    LEFT JOIN pg_payuIndia as pg ON txn.trans_id = pg.txnid
                    LEFT JOIN users as u ON txn.users_id = u.id 
                    LEFT JOIN voucher as vchr ON txn.ref_trans_id=vchr.txnid 
                    LEFT JOIN `coupons` as cpn ON cpn.id = dt.coupon_id  
                    LEFT JOIN deals as d ON d.id = dt.deal_id
                    WHERE txn.trans_date >='".$data['sd']."' AND txn.trans_date <='".$data['ed']."' 
                        AND txn.status = '2' AND txn.trans_category='".$data['req_type']."' ".$mode_cond;
        return $qry;
    }
    
    public static function get_wallet_deal_qry($data){
        $mode_cond = " AND txn.transaction_mode='".$data['mode']."' ";
        if($data['mode'] == "total"){
            $mode_cond = "";
        }
        $qry = "SELECT dt.deal_id as deal_id,d.name as deal,dt.offer_id as offer_id,off.name as offer, count( 1 ) AS cnt,off.total_stock
                FROM deal_transaction AS dt
                INNER JOIN transactions AS txn ON txn.trans_id = dt.transaction_id
                INNER JOIN deals as d ON d.id = dt.deal_id
                LEFT JOIN offers AS off ON off.id = dt.offer_id
                WHERE off.deal_id = dt.deal_id AND txn.trans_date >='".$data['sd']."' AND txn.trans_date <='".$data['ed']."' AND txn.status=2 ".$mode_cond." GROUP BY 1";
            
        return $qry;
    }


    public static function get_repeated_users_qry($data){
        $qry = "SELECT distinct(txn.users_id),txn.trans_date,date(u.created_datetime) as reg_date, u.mobile_number
                    FROM transactions as txn 
                    LEFT JOIN users as u ON txn.users_id = u.id 
                    WHERE txn.trans_date >='".$data['sd']."' AND txn.trans_date <='".$data['ed']."' 
                          AND u.created_datetime < txn.trans_date ";
        return $qry;
    }

    public static function get_user_details($data) {
        $connection = Yii::app()->db;
        $deal_id = $data['deal_id'];
        $startDate = $data['start'];
        $endDate = $data['end'];
        
        $qry = "SELECT user_name as User,d.name as deal,off.name as offer, trans_datetime as GrabedAt,dl.expiry_date
                FROM deal_transaction AS dl
                LEFT JOIN deals as d ON (d.id = dl.deal_id)
                LEFT JOIN offers as off ON (off.id = dl.offer_id)
                LEFT JOIN users AS u ON dl.users_id = u.id
                LEFT JOIN transactions AS txn ON dl.transaction_id = txn.trans_id"
                . " where dl.deal_id =$deal_id and txn.trans_date >=DATE('".$startDate."') AND txn.trans_date <='".$endDate."'";//and dl.offer_id =$offer_id 
        $data_res = array();
        $data_res['data'] = $connection->createCommand($qry)->queryAll();
        $data_res['count'] = $connection->createCommand($qry)->execute();
        $data_res['title'] = "USERS DETAILS";
        return $data_res;
    }
    
    public static function get_AllOperator($data=null) {
        $condition="";
        if($data['mainType']=='billing'){
            $condition="AND service_id!=1 AND service_id!=5";
        }
        $connection = Yii::app()->db;
        $query="SELECT id, service_id, (
                CASE WHEN service_id =5
                THEN CONCAT( name, ' DATA CARD ')
                ELSE name
                END
                ) AS name
                FROM products
                WHERE service_id !=3 $condition";
        return $connection->createCommand($query)->queryAll();
    }
    public static function get_RechargeDetails($data=null) {
        if(isset($data['type']) && !empty($data['type'])){
             $condition = "1";
            if($data['type']=='wallet'){
                $condition = " 	transaction_mode='wallet' ";
            }
            elseif ($data['type']=='online') {
                $condition = " transaction_mode='online' ";
            }
            $column='mobile_number';
            switch($data['type']){
                case 'Mobile':
                    $recharge_flag=1;
                    break;
                case 'DTH':
                    $column='subscriber_id';
                    $recharge_flag=2;
                    break;
                case 'DataCard':
                    $recharge_flag=3;
                    break;
                case 'Postpaid':
                    $recharge_flag=4;
                    break;
            }
            $connection = Yii::app()->db;
            
            $start_date = $data['startDate'];
            $end_date = $data['EndDate'];
            $operator_id = $data['operator_id'];
            $recharge_flag = $data['flag'];
            $query = " SELECT $column as number,trans_datetime,transaction_mode,transaction_amount"
                    . " FROM recharge_transaction as rt INNER JOIN transactions as txn on rt.transaction_id=txn.trans_id"
                    . " WHERE txn.trans_date >='$start_date' AND txn.trans_date <='$end_date' AND txn.status=2 AND txn.trans_category='recharge' AND operator_id=$operator_id AND $condition";
            $result = $connection->createCommand($query)->queryAll();
            $data_res['data'] = $result;
            if(count($result)>0){
            $data_res['count'] = $connection->createCommand($query)->execute();
            $data_res['title'] = "OPERATOR WISE RECHARGE TRANSACTION DETAIL";
            return $data_res;
            }
             else{
                return false;
            }
            
        }
    }
    public static function get_Direct_Transaction($data=null) {
        $start_date = $data['sd'];
        $end_date = $data['ed'];
        $connection = Yii::app()->db;
//        $query = "SELECT trans_id,trans_datetime,transaction_amount as amount,phone as mobile,mihpayid as ref_id,
//                   (CASE WHEN txn.status=2 THEN 'COMPLETE' WHEN txn.status=3 THEN 'FAILED' END) as status
//                  FROM `pg_payuIndia` as pg
//                  LEFT JOIN transactions as txn ON pg.`txnid`=txn.trans_id
//                  WHERE txn.status in (2,3) AND DATE(txn.trans_datetime) BETWEEN '$start_date' AND  '$end_date'";
        $query = " SELECT t1.trans_id,t1.trans_datetime,t1.amount,t1.number as mobile,t1.ref_id,
                   t1.status,(CASE WHEN ISNULL(prod.name)  THEN t1.trans_category ELSE prod.name END ) as operator_name,(CASE WHEN  (SELECT COUNT(users_id) FROM transactions where users_id=t1.users_id AND transaction_mode IN ('online','wallet') AND status IN (2,3))>1 THEN 'olduser' ELSE ' newuser' END) as 'User (old/new)',
                   t1.id as 'userId'
		FROM (SELECT  u.id,txn.users_id, txn.trans_id,txn.trans_datetime,txn.transaction_amount as amount,txn.trans_category,
                   (CASE WHEN txn.status=2 THEN 'COMPLETE' WHEN txn.status=3 THEN 'FAILED' END) as status,mihpayid as ref_id,txn.status as txn_status,
		(CASE WHEN trans_category='recharge' 
				THEN 
				    (CASE 
				    WHEN rt.recharge_flag=2 
				    THEN rt.subscriber_id  
				    ELSE rt.mobile_number 
				    END) 
				ELSE 
				    (CASE 
				    WHEN trans_category='billing' 
				    THEN 
				        (CASE 
				        WHEN bt.payment_flag=2 
				        THEN bt.subscriber_id 
				        ELSE bt.mobile_number 
				        END) 
				    ELSE 
				        (CASE
				        WHEN trans_category='refill'
				        THEN u.mobile_number
				        ELSE 
				            'NA'	 
				        END) 
				    END)
				END) as number,
		(CASE 
                WHEN trans_category='recharge' 
                THEN 
                    (CASE
                    WHEN rt.recharge_flag = 1
                    THEN topr.product_id
                    ELSE
                        (CASE
                        WHEN rt.recharge_flag = 2
                        THEN dopr.product_id
                        ELSE
                            (CASE
                            WHEN rt.recharge_flag = 3
                            THEN topr.data_product_id
                            ELSE 0
                            END)
                        END)
                    END)
                ELSE 
                    (CASE
                    WHEN trans_category='billing' and payment_flag = 1
                    THEN topr1.postpaid
                    ELSE 0
                    END)
                END) as product
 	FROM pg_payuIndia as pg1 
			LEFT JOIN `transactions`  as txn ON txn.trans_id = pg1.txnid
                    LEFT JOIN billing_transaction as bt ON txn.trans_id=bt.transaction_id 
                    LEFT JOIN recharge_transaction as rt ON txn.trans_id = rt.transaction_id
                    LEFT JOIN users as u ON txn.users_id = u.id
                    LEFT JOIN telecom_operators as topr ON rt.operator_id=topr.id 
                    LEFT JOIN dth_operators as dopr ON rt.operator_id=dopr.id
                    LEFT JOIN telecom_operators as topr1 ON bt.operator_id=topr1.id     
               	    WHERE  txn.status in (2,3)  AND DATE(txn.trans_datetime) BETWEEN '$start_date' AND  '$end_date'
			) as t1 
                LEFT JOIN products as prod ON prod.id=t1.product ";
        return $query;
    }
    public static function get_Direct_Transaction_Count($data=null) {
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $connection = Yii::app()->db;
        $query = "SELECT count(trans_id) as count,sum(transaction_amount) as amount 
                  FROM `pg_payuIndia` as pg
                  LEFT JOIN transactions as txn ON pg.`txnid`=txn.trans_id
                  WHERE DATE(txn.trans_datetime) BETWEEN '$start_date' AND  '$end_date' AND  txn.status in (2,3)";
        $result = $connection->createCommand($query)->queryAll();
        if($result>0){
            $data_res['count'] = $result[0]['count'];
            $data_res['amount'] = $result[0]['amount'];
            $data_res['title'] = "DIRECT TRANSACTION DETAIL";
        }
        else{
            $data_res['count'] = 0;
        }
        return $data_res;
    }

    public static function get_dealermapcount(){
        //SELECT count(DISTINCT d.id ) AS cnt
        $qry = "SELECT count( d.id ) AS cnt
                FROM deals AS d
                LEFT JOIN offers AS of ON ( d.id = of.deal_id )
                LEFT JOIN deal_locations AS dl ON ( of.deal_id = dl.deal_id )
                WHERE of.status =1 AND d.status=1 AND dl.lat is not null AND dl.lat!=0 AND dl.lat!=''";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $count = 0;
        if(count($data)>0){
            $count = $data[0]['cnt'];
        }
        return $count;
    }
    public static function get_dealermapdetails($deal_id=null){
        $condition  = (isset($deal_id) && !empty($deal_id)) ? " AND d.id=$deal_id " : "";//CONCAT('<h5>', of.name, '</h5>', dl.address )
        $qry = "SELECT dl.lat as latitude,dl.lng as longitude,of.name AS address
                FROM deals AS d
                LEFT JOIN offers AS of ON ( d.id = of.deal_id )
                LEFT JOIN deal_locations AS dl ON ( of.deal_id = dl.deal_id )
                WHERE of.status=1 AND d.status=1 AND dl.lat is not null AND dl.lat!=0 AND dl.lat!='' $condition";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        return $data;
    }
    
    public static function get_usermapcount(){
        $qry = "SELECT count(latitude) as cnt"
                . " FROM user_profile as u WHERE u.latitude IS NOT NULL
                AND u.latitude !=0 AND u.latitude !=''";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $count = 0;
        if(count($data)>0){
            $count = $data[0]['cnt'];
        }
        return $count;
    }
    
    public static function get_usermapdetails(){
        $qry = "SELECT latitude,longitude,mobile as address "
                . " FROM user_profile as u WHERE u.latitude IS NOT NULL
                AND u.latitude !=0 AND u.latitude !=''";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
       
        return $data;
    }
    
    public static function get_retailermapcount(){
        $url = 'http://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getNearByRetailer&limit=-1';
        $result = GeneralComponent::curl($url);
        $result = substr($result,1,-2);
        $result = json_decode($result,true);
        return count($result[0]);
    }
    public static function get_retailermapdetails(){
        $url = 'http://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getNearByRetailer&limit=-1';
        $result = GeneralComponent::curl($url);
        $result = substr($result,1,-2);
        $result = json_decode($result,true);
        $query = "SELECT rt.retailer_id as id, count( * ) AS count
                    FROM refill_transaction AS rt
                    LEFT JOIN `temp_wallets` AS tw ON ( rt.retailer_id = tw.retailer_id )
                    WHERE rt.retailer_id IS NOT NULL AND rt.retailer_id!=0 AND rt.retailer_id!=''
                    GROUP BY 1
                    order by 1";
        //file_put_contents("/tmp/get_retailermapdetails", "| noti-array ".$query."\n", FILE_APPEND | LOCK_EX);
        $data = array();
        $data = Yii::app()->db->createCommand($query)->queryAll();
        $result = $result[0];
        $data_array = array();
        if(count($data)>0){
            $i=0;            
            foreach($result as $row){
                $count = self::multiSearch(&$data,$row['t']['user_id']);
                $data_array[$i] = $row['t'];
                $data_array[$i]['count']=$count;
               $i++;
            }
        }
        return $result;
    }
    
    public static function multiSearch($arr,$val){
        $count = 0;
        $i=0;
        foreach($arr as $row){
            if($row['id']==$val){
                $count = $row['count'];
                unset($arr[$i]);
                break;
            }
            $i++;
        }
        return $count;
    }
    
    
    public static function getTotalMsgUsage($start_date, $end_date){    
        $a = 0 ;
        $b = 0 ;
        for($i = 6 ; $i >= 0 ; $i--){
          $days[$a++] =  date('Y-m-d', strtotime($end_date .' -'.$i.'day'));
        }
        for($i = 0 ; $i < $a  ; $i++){
          $reqUrl[$b++] = 'http://www.smstadka.com/users/b2cMarketingMsg/'.$days[$i];
        }
        try{ 
            for($j = 0 ; $j < $i ; $j++){
             $ch[$j] = curl_init();
             curl_setopt($ch[$j], CURLOPT_URL, $reqUrl[$j]);
             curl_setopt($ch[$j], CURLOPT_RETURNTRANSFER, 1); 
             $resp[$j] = curl_exec($ch[$j]);                   //Send the request & save response to $resp
             curl_close($ch[$j]);                              //Close request to clear up some resources
            }      
        }catch(Exception $ex){      
           echo $ex->getMessage();
        }
        for($i = 0 ; $i < $j ; $i++){
            $result[$i] = json_decode($resp[$i],true);
        }
        return $result;
    }        
                
    public static function getTotalAppInstall($start_date, $end_date){    
         $first_date = date ("Y-m-d", strtotime("-6 day", strtotime($end_date))); //before 7 days
        try{
         $query = "SELECT date(up.created) AS Create_Date, 
             COUNT(CASE WHEN (up.user_type =2 OR up.user_type =3) THEN 1 END ) AS App_mrk,
             COUNT(CASE WHEN up.user_type =0 THEN 1 END ) AS App_Store
             FROM transactions AS t INNER JOIN user_profile AS up ON ( up.user_id = t.users_id ) 
             INNER JOIN users AS u ON ( u.id = t.users_id )
             WHERE t.trans_category =  'signup' AND t.status =2 AND u.password IS NOT NULL AND 
             date(up.created) BETWEEN '$first_date' AND '$end_date' GROUP BY date(up.created)";
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $ex->getMessage(), "data" => array());
        }
        return $result;
    }
    
    public static function getTotalMissedCall($start_date, $end_date){    
         $first_date = date ("Y-m-d", strtotime("-6 day", strtotime($end_date))); //before 7 days
        try{
         $query = "SELECT date(up.created) AS Create_Date, COUNT(CASE WHEN up.user_type = 3 THEN 1 END) AS via_Poster,
             COUNT(CASE WHEN up.user_type = 2 THEN 1 END) AS via_Sms
             FROM  `user_profile` AS up WHERE (date(up.created) BETWEEN '$first_date' AND '$end_date')"
             . " GROUP BY date(up.created)";
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    
    public static function getTotalSmsMissCallUsers($start_date, $end_date){    
        try{
         $query = "SELECT COUNT( CASE WHEN up.created <  '$start_date' THEN 1 END ) AS sms_old_user,"
                 . "COUNT( CASE WHEN up.created BETWEEN  '$start_date' AND DATE_ADD(  '$end_date', INTERVAL 1 DAY ) THEN 1 END )"
                 . "AS sms_new_user FROM  `user_profile` AS up WHERE up.user_type = 2";
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getTotalPosterMissCallUsers($start_date, $end_date){    
        try{
         $query = "SELECT COUNT( CASE WHEN up.created <  '$start_date' THEN 1 END ) AS poster_old_user,"
                 . "COUNT( CASE WHEN up.created BETWEEN  '$start_date' AND DATE_ADD(  '$end_date', INTERVAL 1 DAY ) THEN 1 END )"
                 . "AS poster_new_user FROM  `user_profile` AS up WHERE up.user_type = 3";
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getTotalSignupUsers($start_date, $end_date){    
        try{
         $query = "SELECT COUNT(CASE WHEN up.user_type =2 THEN 1 END ) AS sms_signup_user, "
                 . "COUNT(CASE WHEN up.user_type =3 THEN 1 END ) AS poster_signup_user, "
                 . "COUNT(CASE WHEN up.user_type =0 OR up.user_type =1 THEN 1 END ) AS other_signup_user "
                 . "FROM transactions AS t INNER JOIN user_profile AS up ON ( up.user_id = t.users_id ) "
                 . "INNER JOIN users AS u ON ( u.id = t.users_id ) WHERE t.trans_category =  'signup' "
                 . "AND t.status =2 AND u.password IS NOT NULL AND up.created BETWEEN  '$start_date' AND "
                 . "DATE_ADD(  '$end_date', INTERVAL 1 DAY )";
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getTotalLoyaltyPointUsers($start_date, $end_date){    
        try{
         $query = "SELECT count(CASE WHEN DATE(created) between '$start_date' and '$end_date' THEN 1 END) AS via_miss_call,
                count(CASE WHEN DATE(created) not between '$start_date' and '$end_date' THEN 1 END) AS via_recharge
                FROM `transactions` inner join user_profile ON (user_id = users_id) where loyalty_points > 0 AND 
                trans_date between '$start_date' and '$end_date' AND user_type in (2,3) AND trans_category = 'pay1shop' and transactions.status = 2"; 
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getLoyaltyPointAppUsers($start_date, $end_date){    
        try{
         $query = "SELECT  count(CASE WHEN (DATE(up.created) between '$start_date' and '$end_date')  THEN 1 END ) AS appUser_via_miss_call,
                count(CASE WHEN (DATE(up.created) not between '$start_date' and '$end_date') THEN 1 END ) AS appUser_via_recharge
                FROM transactions as t
                INNER JOIN user_profile as up ON (up.user_id = t.users_id) 
                INNER JOIN users as u ON ( u.id = up.user_id) WHERE  up.user_type 
                IN ( 2, 3 ) AND u.password is not null AND t.trans_category =  'pay1shop'
                AND t.trans_date >=  '$start_date' and t.trans_date <=  '$end_date' AND t.loyalty_points >0 and t.status = 2"; 
         $list= Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $list);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getMissCallUserGift($start_date, $end_date){
        $connection = Yii::app()->db;
        $query="SELECT COUNT( 1 ) AS total_gifts, SUM( 
                CASE WHEN c.STATUS =2
                     THEN 1 
                     ELSE 0 
                     END ) AS total_redeem
                FROM coupons as c
                left join user_profile as up on (up.user_id = c.users_id)
                WHERE date(up.created) >= '$start_date' and date(up.updated) <= '$end_date' and up.user_type in (2,3)";
        $result = $connection->createCommand($query)->queryAll();
        return isset($result) ? $result : array();
    }
    
        public static function getGenuineUser(){    
        try{
        /*$query_r = Yii::app()->db->createCommand("SELECT up.user_id as userID, up.mobile as mobile, up.created as created, up.updated as updated, SUM( transaction_amount ) AS recharge_amt, COUNT( 1 ) AS frequency
                   FROM user_profile AS up
                   LEFT JOIN transactions AS txn ON ( txn.users_id = up.user_id
                   AND up.is_genuine =1 ) 
                   WHERE txn.status =2
                   AND txn.trans_category
                   IN (
                   'recharge',  'billing'
                   )
                   AND txn.trans_date BETWEEN DATE_SUB(CURDATE(), INTERVAL 60 DAY) AND CURDATE()
                   GROUP BY 1 , 2, 3")->queryAll();
       
        $query_g = Yii::app()->db->createCommand("SELECT users_id as userID, COUNT( 1 ) AS total_gifts, SUM( 
                    CASE WHEN STATUS =2
                    THEN 1 
                    ELSE 0 
                    END ) AS total_redeem
                    FROM coupons
                    GROUP BY 1 ")->queryAll();*/
            
            $query = "SELECT `userID`, `is_genuine`, `mobile`, `created`, `updated`, `recharge_amt`, `frequency`, `total_gifts`, `total_redeem` FROM `genuine_user` where is_genuine =1";
            $user_details = Yii::app()->db->createCommand($query)->queryAll();
         
         //$list = self::getMerge($query_r,$query_g);
         $result = array("status" => "success","description" => "","data" => $user_details);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getNonGenuineUser(){    
        try{
       /* $query_r = Yii::app()->db->createCommand("SELECT up.user_id as userID, up.mobile as mobile, up.created as created, up.updated as updated, SUM( transaction_amount ) AS recharge_amt, COUNT( 1 ) AS frequency
                   FROM user_profile AS up
                   LEFT JOIN transactions AS txn ON ( txn.users_id = up.user_id
                   AND up.is_genuine =0 ) 
                   WHERE txn.status =2
                   AND txn.trans_category
                   IN (
                   'recharge',  'billing'
                   )
                   AND txn.trans_date BETWEEN DATE_SUB(CURDATE(), INTERVAL 60 DAY) AND CURDATE()
                   GROUP BY 1 , 2, 3")->queryAll();
         
        $query_g = Yii::app()->db->createCommand("SELECT users_id as userID, COUNT( 1 ) AS total_gifts, SUM( 
                    CASE WHEN STATUS =2
                    THEN 1 
                    ELSE 0 
                    END ) AS total_redeem
                    FROM coupons
                    GROUP BY 1")->queryAll();
         */
         //$list = self::getMerge($query_r,$query_g);
            $query = "SELECT `userID`, `is_genuine`, `mobile`, `created`, `updated`, `recharge_amt`, `frequency`, `total_gifts`, `total_redeem` FROM `genuine_user` where is_genuine =0";
            $user_details = Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $user_details);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    
    public static function getAllUser(){    
        try{
          /* $startdate  = date('Y-m-d',mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")));
            $endate = date('Y-m-d');
         $query_r = Yii::app()->db->createCommand("SELECT up.user_id as userID, up.mobile as mobile, up.created as created, up.updated as updated, SUM( transaction_amount ) AS recharge_amt, COUNT( 1 ) AS frequency
                    FROM user_profile AS up
                    INNER JOIN transactions AS txn ON ( txn.users_id = up.user_id) 
                    WHERE txn.status =2
                    AND txn.trans_category
                    IN (
                    'recharge',  'billing'
                    )
                    AND (
                    DATE( txn.trans_date ) 
                    BETWEEN '$startdate' AND '$endate' 
                    )
                    GROUP BY 1 , 2, 3, 4 ")->queryAll();
         
         $query_g = Yii::app()->db->createCommand("SELECT users_id as userID, COUNT( 1 ) AS total_gifts, SUM( 
                    CASE WHEN STATUS =2
                    THEN 1 
                    ELSE 0 
                    END ) AS total_redeem
                    FROM coupons
                    GROUP BY 1 ")->queryAll();
         
         $list = self::getMerge($query_r,$query_g);*/
            $query = "SELECT `userID`, `is_genuine`, `mobile`, `created`, `updated`, `recharge_amt`, `frequency`, `total_gifts`, `total_redeem` FROM `genuine_user`";
            $user_details = Yii::app()->db->createCommand($query)->queryAll();
         $result = array("status" => "success","description" => "","data" => $user_details);
        }catch (Exception $ex) {
            $message = Yii::app()->params['message_template']['EXCEPTION'];
            $result = array('status' => 'failure', 'errCode' => $ex->getCode(), 'description' => $message, "data" => array());
        }
        return $result;
    }
    public static function get_OnlineRechargeDetails($data=null) {
        if(isset($data['name']) && isset($data['name'])){
             $condition = "1";
            if ($data['type']=='online') {
               // $condition = " transaction_mode='online' AND rt.recharge_flag=$recharge_flag AND rt.operator_id=$operator_id";
            }
            
             $operator = $data['name'];
            $connection = Yii::app()->db;
            $start_date = $data['start'];
            $end_date = $data['end'];
            $query="SELECT prod.name, t1.transaction_amount,t1.recharge_number,t1.user_number
                FROM (SELECT
                (CASE 
                WHEN trans_category='recharge' 
                            THEN 
                                (CASE
                                WHEN rt.recharge_flag = 1
                                THEN topr.product_id
                                ELSE
                                    (CASE
                                    WHEN rt.recharge_flag = 2
                                    THEN dopr.product_id
                                    ELSE
                                        (CASE
                                        WHEN rt.recharge_flag = 3
                                        THEN topr.data_product_id
                                        ELSE 0
                                        END)
                                    END)
                                END)
                            ELSE 0
                END) as product ,txn.transaction_amount,(CASE WHEN rt.subscriber_id=0 THEN rt.mobile_number ELSE rt.subscriber_id END ) as recharge_number,us.mobile_number as user_number
                FROM `transactions` as txn 
                 LEFT JOIN `billing_transaction` as bt ON txn.ref_trans_id=bt.transaction_id 
                 LEFT JOIN `recharge_transaction` as rt ON txn.ref_trans_id = rt.transaction_id
                 LEFT JOIN `telecom_operators` as topr ON rt.operator_id=topr.id 
                 LEFT JOIN `dth_operators` as dopr ON rt.operator_id=dopr.id
                 LEFT JOIN `telecom_operators` as topr1 ON bt.operator_id=topr1.id 
                 LEFT JOIN `users` as us ON us.id=txn.users_id 
                 WHERE txn.trans_date >='$start_date' AND txn.trans_date <='$end_date'
                AND txn.status=2 AND txn.transaction_mode='online'  AND txn.trans_category='recharge'  
                ) as t1 
                LEFT JOIN products as prod ON prod.id=t1.product
                WHERE prod.name='$operator'";
            
            //file_put_contents("/tmp/get_OnlineRechargeDetails", "| noti-array ".$query."\n", FILE_APPEND | LOCK_EX);
             $result = $connection->createCommand($query)->queryAll();
            $data_res['data'] = $result;
            if(count($result)>0){
            $data_res['count'] = $connection->createCommand($query)->execute();
            $data_res['title'] = "OPERATOR WISE RECHARGE TRANSACTION DETAIL";
            return $data_res;
            }
             else{
                return false;
            }
            
        }
    }
    public static function getMerge($query_r,$query_g){    
             $merg = array();
            foreach($query_r as $r){
                $i=0;
                foreach($query_g as $g){
                    if($r['userID'] == $g['userID']){
                     $merg[$r['userID']] = array_merge($r,$g);
                     unset($query_g[$i]);
                     break;
                    }else{
                     $gift = array("total_gifts" => 0, "total_redeem" => 0);
                     $merg[$r['userID']] = array_merge($r,$gift);
                    }
                    $i++;
                }   
            }
        return $merg;
    }
    
    public static function getUserDirectTxn($data){
        $start_date = $data['sd'];
        $end_date = $data['ed'];
        $user = $data['user'];
        $connection = Yii::app()->db;
        $query = " SELECT t1.trans_id,t1.trans_datetime,t1.amount,t1.ref_id,t1.number,(CASE WHEN ISNULL(prod.name)  THEN t1.trans_category ELSE prod.name END ) as operator_name,t1.status
		FROM (SELECT  u.id,txn.users_id, txn.trans_id,txn.trans_datetime,txn.transaction_amount as amount,txn.trans_category,
                   (CASE WHEN txn.status=2 THEN 'COMPLETE' WHEN txn.status=3 THEN 'FAILED' END) as status,mihpayid as ref_id,txn.status as txn_status,
		(CASE WHEN trans_category='recharge' 
				THEN 
				    (CASE 
				    WHEN rt.recharge_flag=2 
				    THEN rt.subscriber_id  
				    ELSE rt.mobile_number 
				    END) 
				ELSE 
				    (CASE 
				    WHEN trans_category='billing' 
				    THEN 
				        (CASE 
				        WHEN bt.payment_flag=2 
				        THEN bt.subscriber_id 
				        ELSE bt.mobile_number 
				        END) 
				    ELSE 
				        (CASE
				        WHEN trans_category='refill'
				        THEN u.mobile_number
				        ELSE 
				            'NA'	 
				        END) 
				    END)
				END) as number,
		(CASE 
                WHEN trans_category='recharge' 
                THEN 
                    (CASE
                    WHEN rt.recharge_flag = 1
                    THEN topr.product_id
                    ELSE
                        (CASE
                        WHEN rt.recharge_flag = 2
                        THEN dopr.product_id
                        ELSE
                            (CASE
                            WHEN rt.recharge_flag = 3
                            THEN topr.data_product_id
                            ELSE 0
                            END)
                        END)
                    END)
                ELSE 
                    (CASE
                    WHEN trans_category='billing' and payment_flag = 1
                    THEN topr1.postpaid
                    ELSE 0
                    END)
                END) as product
 	FROM pg_payuIndia as pg1 
			LEFT JOIN `transactions`  as txn ON txn.trans_id = pg1.txnid
                    LEFT JOIN billing_transaction as bt ON txn.trans_id=bt.transaction_id 
                    LEFT JOIN recharge_transaction as rt ON txn.trans_id = rt.transaction_id
                    LEFT JOIN users as u ON txn.users_id = u.id
                    LEFT JOIN telecom_operators as topr ON rt.operator_id=topr.id 
                    LEFT JOIN dth_operators as dopr ON rt.operator_id=dopr.id
                    LEFT JOIN telecom_operators as topr1 ON bt.operator_id=topr1.id     
               	    WHERE  txn.status in (2,3) AND u.id= $user
			) as t1 
                LEFT JOIN products as prod ON prod.id=t1.product  ";
                $data_res['data'] = $connection->createCommand($query)->queryAll();
                $data_res['count'] = $connection->createCommand($query)->execute();
                $data_res['title'] = "DIRECT TRANSACTION DETAILS";
                return $data_res;
    }
    
    public static function getB2cAmount($date){
                //$date = new DateTime($date);
                //$date = $date->format('Y-m-d');
                $connection = Yii::app()->db;
                 $query = "SELECT sum(account_balance) as amount FROM wallets_$date";
                $result = $connection->createCommand($query)->queryAll();
                return $result[0]['amount'];
    }

}
