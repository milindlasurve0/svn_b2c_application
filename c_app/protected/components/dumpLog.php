<?php

Class dumpLog
{	
	
    public function __construct($loggername,$loggerfilename) {
        try {
            //Yii::import('application.vendor.logger.main.php.*'); 
            //require 'Logger.php';
            $this->logger = Logger::getLogger($loggername);
            Logger::configure(array(
                'rootLogger' => array(
                    'appenders' => array('default'),
                ),
                'appenders' => array(
                    'default' => array(
                        'class' => 'LoggerAppenderFile',
                        'layout' => array(
                            'class' => 'LoggerLayoutPattern'
                        ),
                        'params' => array(
                            'file' => '/var/log/apps/b2c_' . $loggerfilename . '_' . date('Ymd') . '.log',
                            'append' => true
                        )
                    )
                )
            ));
        } catch (Exception $e) {
            echo "Logger problem";
            echo $e->getMessage();
            $this->logger = false;
        }
        return $this->logger;
    }
}