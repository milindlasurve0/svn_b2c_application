<?php
/**
 * ApiController class file
 * @author Joachim Werner <joachim.werner@diggin-data.de>  
 */
/**
 * ApiController 
 * 
 * @uses Controller
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @author 
 * @see http://www.gen-x-design.com/archives/making-restful-requests-in-php/
 * @license (tbd)
 */

class DealsController extends Controller
{
    // {{{ *** Members ***
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    Const APPLICATION_ID = 'ASCCPE';

    private $format = 'json';
    private $actionData = array();
    public $layout = '//layouts/empty';
    // }}} 
    // {{{ filters
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array();
    }
    /*protected function beforeAction($action) {
        parent::beforeAction($action);
        echo "BEFORE";
        echo $this->action->id;
    }*/
    
    private function _checkAuth()
    {
        //echo "CHECKAUTH";
        // Check if user is admin or dealer
        if(!( UsersComponent::isAdmin())) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
    }
    
    public function actionIndex(){
        echo "INDEX";exit;
    }
    // }}} 
    // {{{ actionView
    /* Shows a single item
     * 
     * @access public
     * @return void
     */
    public function actionEditOffer($id)
    {
        //  echo "ACTION";exit;
        $this->_checkAuth();
        //  Check id
        //  if(empty ($id))
        //  echo "Wrong offer id.";//$this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
        
        $model = Offers::model()->findByPk($id);
        $dealDetails = Deal::model()->findByPk($model->deal_id);
        if(is_null($model)) {
            echo "Wrong offer id.";//$this->_sendResponse(404, 'No Offer found with id '.$id);
        } else {
            //echo "Panel Recharge page";
            $this->actionData['model'] = $model;
            if(!empty($dealDetails->by_voucher)){
                $this->actionData['by_vaucher'] = $dealDetails->by_voucher;
            }
            //$categoriesList = DealsComponent::getDealCategoriesList();
            //$offersList = DealsComponent::getDealOffersList($id);
            //$dealersList = UsersComponent::getUsersListByType(Yii::app()->params['GLOBALS']['_ROLE_DEALER']);

            //$this->actionData['dealStatus'] = Yii::app()->params['GLOBALS']['_DEAL_STATUS'];
            $this->actionData['offerValidityModes'] = Yii::app()->params['GLOBALS']['_OFFER_VALIDITY_MODES'];
            $this->actionData['offerStatus'] = Yii::app()->params['GLOBALS']['_DEAL_OFFER_STATUS'];


            //$this->actionData['categoriesList'] = $categoriesList;
            //$this->actionData['offersList'] = $offersList;
            //$this->actionData['dealersList'] = $dealersList;

            $this->render('offer_edit',$this->actionData);
        }
    }
    
    /*
     * This function is used to edit Offer Info using OfferDeatils.php in offer_details table
     * @return : render edit_offer_info  or wrong Offer Info message (based on method)
    */
    public function actionEditOfferInfo($id){        
        $this->_checkAuth();
        $model = OfferDetails::model()->findByPk($id);
        if(is_null($model)) {
            echo "<p>Wrong Offer Info ID.</p>";
        } else {
            $this->actionData['model'] = $model;
            $this->render('edit_offer_info',$this->actionData);
        }
        
    }
   /*End of Edit Offer Info Function */    
    
    public function actionEditInfo($id)
    {   
        //  echo "ACTION";exit;
        $this->_checkAuth();
        $model = DealInfo::model()->findByPk($id);
        if(is_null($model)) {
            echo "<p>Wrong info id.</p>";
        } else {
            $this->actionData['model'] = $model;
            $this->actionData['infoStatus'] = Yii::app()->params['GLOBALS']['_DEAL_INFO_STATUS'];
            $this->render('edit_info',$this->actionData);
        }
    }
    
    public function actionEditLocation($id)
    {
        //  echo "ACTION";exit;
        $this->_checkAuth();
        
        //$model = Locations::model()->findByPk($id);
        $info = DealsComponent::getDealLocationsList('',$id);
        if(is_null($info)) {
            echo "Wrong location id.";//$this->_sendResponse(404, 'No Offer found with id '.$id);
        } else {
            //echo "Panel Recharge page";
            $statesList = GeneralComponent::getStatesList();
            $this->actionData['info'] = $info;
            $this->actionData['id'] = $info[0]['id'];
            $this->actionData['locationStatus'] = Yii::app()->params['GLOBALS']['_DEAL_LOCATION_STATUS'];
            $this->actionData['statesList'] = $statesList;

            $this->render('edit_location',$this->actionData);
        }
    }
    
    public function actionEditMap($id)
    {
        $this->_checkAuth();
        $info = DealsComponent::getDealLocationMapList($id);
        if(is_null($info)) {
            echo "Wrong location id.";//$this->_sendResponse(404, 'No Offer found with id '.$id);
        } 
        else{
            $this->actionData['info'] = $info;
            $this->actionData['id'] = $info[0]['id'];
            $this->actionData['deal_id'] = $info[0]['deal_id'];
            $this->actionData['lat'] = $info[0]['lat'];
            $this->actionData['lng'] = $info[0]['lng'];
            
            $this->actionData['$lat_lng'] = $this->actionData['lat'].','.$this->actionData['lng'];
            $this->render('edit_map',  $this->actionData);
           
        }
    }
}


/* vim:set ai sw=4 sts=4 et fdm=marker fdc=4: */
?>
