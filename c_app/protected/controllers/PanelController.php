<?php

class PanelController extends Controller {
    
        /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2_2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {        
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_2';
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array( 'create','test','payu'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('wallet','recharge','services','index','billpayment','complain','create_voucher','user_reports','genuine_non_genuine_users','miss_call_users','detail_reports','getmobileDetails','blockUser','unblockUser','payu','freegift','redeem_Freebie','redeem_Coupon','rechargeDetail_reports','map_reports','getDealMap','UploadExtraIncentiveList','getrechargeDetails','getuserdirecttxn','settransaction'),
                'users' => array('@'),
                'deniedCallback'=>ApiResponse::unAuthorizedCall()
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete','search'),
                'users' => UsersComponent::getAdminusers(),
            ),
            array('allow', // allow report user to perform 'search' and 'user_report' actions
                'actions' => array('search','user_reports'),
                'users' => UsersComponent::getReportusers(),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){
        if(UsersComponent::isAdmin(Yii::app()->user->username)){
            $this->actionadmin();
        }
        else if(UsersComponent::isDealer(Yii::app()->user->username)){
            $this->actionDealer();
        }
        else if(UsersComponent::isReport(Yii::app()->user->username)){
            $this->actionSearch();
//            $this->actionUser_reports();
        }
        else{
            $this->actionRecharge();            
        }
    }
    
    public function actionDealer(){
        $this->layout = '//layouts/column1_1';
        $dataArray = array();
        $this->actionFreegift();
    }
    
    public function actionRecharge(){
        $ops = array('1'=>'airtel','2'=>'vodafone','3'=>'aircel');
        $this->render('recharge',array('operator'=>$ops));
    }
    
    public function actionWallet(){        
        //echo "Panel Recharge page";        
        $this->render('wallet');
    }
    
    public function actionServices(){        
        //echo "Panel Recharge page";
        $this->render('services');
    }
    
    public function actionadmin(){        
        //echo "Panel Recharge page";
        $this->render('admin');
    }
    
    public function actionBillpayment(){
        $this->render('billpayment');
    }
    
    public function actionCreate_voucher(){
        $result = array();
        
        if(isset($_REQUEST['vreport_submit'])){
            $result = VoucherComponent::voucher_report($_REQUEST);
        }
        if(isset($_REQUEST['submit'])){            
            $result = VoucherComponent::generate_VCODE($_REQUEST);
            if($result['errCode']==0){
                $msg = "Vouchers generated successfully";
                Yii::app()->user->setFlash('success', "Voucher detail ID : ".$result['description']['voucher_detail_id']."<br/>".$msg);
            }else{
                Yii::app()->user->setFlash('error', "<b stype='color:red'>".$result['description']."</b>");
            }
        }
        if(empty($_REQUEST)){
            $_REQUEST['vreport_submit'] = true;
        }
        $this->render('create_voucher',$result);
    }

    public function actionTest(){
        $this->renderPartial('test');
        //$this->render('test',array());
    }
    
    public function actionSearch($dtFrom=null,$dtTo=null){        
        try{
             $userData = array();               
             $dataArray = array();
             $dataArray['display'] = FALSE;
             $dataArray['details'] = FALSE;
             $dataArray['mobNoDisplay'] = FALSE;
             $dataArray['errmsg'] = "";
             $dataArray['start_date'] = "";
             $dataArray['end_date'] = "";
             $dataArray['wallet_user_name'] = "";
             $dataArray['sub_no'] = "";
             $dataArray['pay1_voucher_no'] = "";
             $dataArray['deal_coupon_no'] = "";
             $condarray = array();
                //UsersComponent::getFullUserDetailsById(28);
                //if(Yii::app()->request->isPostRequest){
               if((isset($_REQUEST['search_submit']) && $_REQUEST['search_submit'] === "Search") ||!empty($_REQUEST['wallet_user_name'])){
                    $searchTypeUserDetails = "";
                    $inputUserDetails      = "";            
                    $dataArray['display'] = False;
                    //"byUserId" ,"byUserMobile" ,"byUserName" , "byWalletId"
                    if(!empty($_REQUEST['wallet_user_name'])){
                        $searchTypeUserDetails = "byUserName";
                        $inputUserDetails = $_REQUEST['wallet_user_name'];  
                        $dataArray['wallet_user_name'] = trim($_REQUEST['wallet_user_name']);
                    }else if(!empty($_REQUEST['txn_id'])){
                        $condarray['txnid'] = trim($_REQUEST['txn_id']);
                        $searchTypeUserDetails = "bytxnId";
                        $inputUserDetails = $_REQUEST['txn_id'];
                        $dataArray['txn_id'] = trim($_REQUEST['txn_id']);
                    }else if(!empty($_REQUEST['mob_no'])){
                        $condarray['number'] = trim($_REQUEST['mob_no']);
                        $searchTypeUserDetails = "byUserMobile";
                        $inputUserDetails = trim($_REQUEST['mob_no']);
                        $dataArray['mob_no'] = trim($_REQUEST['mob_no']);
                    }else if(!empty($_REQUEST['sub_no'])){
                        $condarray['number'] = trim($_REQUEST['sub_no']);
                        $searchTypeUserDetails = "byUserSubscription";
                        $inputUserDetails = $_REQUEST['sub_no'];
                         $dataArray['sub_no'] = trim($_REQUEST['sub_no']);
                    }else if(!empty($_REQUEST['pay1_voucher_no'])){
                        $condarray['pay1_voucher'] = trim($_REQUEST['pay1_voucher_no']);
                        $searchTypeUserDetails = "byPay1Voucher_no";
                        $inputUserDetails = $_REQUEST['pay1_voucher_no'];
                        $dataArray['pay1_voucher_no'] = trim($_REQUEST['pay1_voucher_no']);
                    }else if(!empty($_REQUEST['deal_coupon_no'])){
                        $condarray['deal_coupon_no'] = trim($_REQUEST['deal_coupon_no']);
                        $searchTypeUserDetails = "byDealCoupon_no";
                        $inputUserDetails = $_REQUEST['deal_coupon_no'];
                         $dataArray['deal_coupon_no'] = trim($_REQUEST['deal_coupon_no']);
                    }
                    if(!empty($_REQUEST['start_date'])){
                        $dtFrom = trim($_REQUEST['start_date']);
                         $dataArray['start_date'] =trim($_REQUEST['start_date']);
                    }
                    if(!empty($_REQUEST['end_date'])){
                        $dtTo = trim($_REQUEST['end_date']);
                        $dataArray['end_date'] = trim($_REQUEST['end_date']);
                    }
                    //print_r($_REQUEST);exit();
                    $userData = UsersComponent::getFullUserDetails($inputUserDetails, $searchTypeUserDetails);//by wallet user name
                    if(count($userData) < 1 || count($userData['data']) < 1){
                        throw new Exception('Not a registered User');
                    }
                    $userTxnData = TransactionComponent::getPurchaselog(0,100,null,$userData["data"][0]["id"],$dtFrom,$dtTo,$condarray );
                    $complaintsData = TransactionComponent::getComplaintDetails($userData["data"][0]["id"], "user_id",$dtFrom,$dtTo);
                    $commentsData = TransactionComponent::getComments($userData["data"][0]["id"], "user_id",$dtFrom,$dtTo);
                    $dataArray['display']=TRUE;
                    $dataArray['detail']=TRUE;
                }
                if(isset($_REQUEST['mobSearch_submit']) && $_REQUEST['mobSearch_submit'] === "Search"){
                    $searchTypeUserDetails = "";
                    $emailId = trim($_REQUEST['emailId']);
                     if(!empty($_REQUEST['deviceId'])){
                        $condarray['deviceId'] = trim($_REQUEST['deviceId']);
                    }
                     $mobNumData = GeneralComponent::get_mobileNumbers($emailId,$condarray);
                     $dataArray['mobNoDisplay']=TRUE;
                }
                if(!empty($userData['status']) && $userData['status'] == "success"){
                    $dataArray["userData"] = $userData["data"];
                }else{
                    $dataArray["userData"] = array();
                }
                if(!empty($userTxnData['status']) && $userTxnData['status'] == "success"){
                    $dataArray["userTxnData"] = $userTxnData["description"];
                }else{
                    $dataArray["userTxnData"] = array();
                }

                if(!empty($complaintsData['status']) && $complaintsData['status'] == "success"){
                    $dataArray["complaintsData"] = $complaintsData["description"];
                }else{
                    $dataArray["complaintsData"] = array();
                } 
                 if(!empty($mobNumData['status']) && $mobNumData['status'] == "success"){
                    $dataArray["mobNumData"] = $mobNumData["data"];
                }else{
                    $dataArray["mobNumData"] = array();
                }
        } catch (Exception $ex) {
            $dataArray['errmsg'] = $ex->getMessage();
        }
        $this->render("search",$dataArray);
    }
    
    public function actionUser_reports(){
        $todayDate = date('Y-m-d');
        $data = $_REQUEST;
        $dataArray = array('total_new_users'=>'','detail'=>'');
        $dataArray['requestdata'] = $data;
        //--- count
        $dataArray['recharge_total_count'] = $dataArray['recharge_wallet_count'] = $dataArray['recharge_online_count'] = 0;
        $dataArray['billing_total_count'] = $dataArray['billing_wallet_count'] = $dataArray['billing_online_count'] = 0;
        $dataArray['refill_total_count'] = $dataArray['refill_wallet_count'] = $dataArray['refill_online_count'] = $dataArray['refill_voucher_count']= 0;
        //--- amount
        $dataArray['recharge_total_amount'] = $dataArray['recharge_wallet_amount'] = $dataArray['recharge_online_amount'] = 0;
        $dataArray['billing_total_amount'] = $dataArray['billing_wallet_amount'] = $dataArray['billing_online_amount'] = 0;
        $dataArray['refill_total_amount'] = $dataArray['refill_wallet_amount'] = $dataArray['refill_online_amount'] = $dataArray['refill_voucher_amount'] = 0;
        //--- deals
        $dataArray['deal_total_count'] = $dataArray['deal_wallet_count'] = $dataArray['deal_online_count'] = 0;
        $dataArray['deal_total_amount'] = $dataArray['deal_wallet_amount'] = $dataArray['deal_online_amount'] = 0;
        $dataArray['repeated_users_count'] =0;
        //--- date
        $data['start_date'] = isset($data['start_date'])?$data['start_date']:$todayDate;
        $data['end_date'] = isset($data['end_date'])?$data['end_date']:$todayDate;
        
        //if(isset($data['search']) && $data['search']=='search'){            
        $dataArray['total_new_users'] = ReportingComponent::get_newUsers($data['start_date'], $data['end_date']);
        $new_data_detail = ReportingComponent::get_totalTransdetails($data['start_date'], $data['end_date']);
        $dataArray = ReportingComponent::format_report_data($new_data_detail, $dataArray);
       // echo $dataArray['deal_total_count']; die;
        $dataArray['repeated_users_count'] = ReportingComponent::get_repeatedUsers_count($data);
        $Direct_Transaction = ReportingComponent::get_Direct_Transaction_Count($data);
        $dataArray['Direct_Transaction'] = $Direct_Transaction['count'];
        $dataArray['Direct_Transaction_amount'] = empty($Direct_Transaction['amount']) ? 0 : $Direct_Transaction['amount'];
        $dataArray['dealerMapCount'] = ReportingComponent::get_dealermapcount();
        $dataArray['userMapCount'] = ReportingComponent::get_usermapcount();
        $dataArray['retailerMapCount'] = ReportingComponent::get_retailermapcount();
       // }        
        $dataArray['start_date'] = $data['start_date'];
        $dataArray['end_date'] = $data['end_date'];
        $this->render("user_report",$dataArray);
    }
    
    
    public function actionGenuine_Non_Genuine_Users(){
        try{     
            $todayDate = date('Y-m-d');
            $dataArray['todayDate'] = $todayDate;
            
            $userType = 1;
            if($_POST && isset($_POST['user_type'])){
              $data = $_POST;
              $userType = $data['user_type'];
             
            }
              if($userType === 1){
                  $Auser  =  ReportingComponent::getAllUser();
                $dataArray['Auser'] = $Auser['data'];
              }elseif($userType == 2){
                $Guser  =  ReportingComponent::getGenuineUser();
                $dataArray['Auser']  = $Guser['data'];
              }else{
                $NGuser =  ReportingComponent::getNonGenuineUser();
                $dataArray['Auser']  = $NGuser['data'];
              }
           $dataArray['user_type'] = $userType;
        }catch (Exception $ex) {
            $dataArray['errmsg'] = $ex->getMessage();
        }    
        $this->render("genuine_non_genuine_user", $dataArray);    
    }
    
    public function actionMiss_Call_Users(){
        try{
            $data = $_REQUEST;
            $todayDate = date('Y-m-d');
            $start_date = isset($data['start_date'])?$data['start_date']:$todayDate;
            $end_date   = isset($data['end_date'])?$data['end_date']:$todayDate;
            $dataArray['start_date'] = $start_date;  //Today
            $dataArray['end_date']   = $end_date;
            $dataArray['errmsg']     = "";

            $TotalMsgUsage         = ReportingComponent::getTotalMsgUsage($start_date, $end_date);
            $TotalAppInstall       = ReportingComponent::getTotalAppInstall($start_date, $end_date);
            $TotalMissedCall       = ReportingComponent::getTotalMissedCall($start_date, $end_date);
            
            $SmsMissCallusers      = ReportingComponent::getTotalSmsMissCallUsers($start_date, $end_date);
            $PosterMissCallusers   = ReportingComponent::getTotalPosterMissCallUsers($start_date, $end_date);
            $SignupUsers           = ReportingComponent::getTotalSignupUsers($start_date, $end_date);
            $LoyaltyPointUsers     = ReportingComponent::getTotalLoyaltyPointUsers($start_date, $end_date);
            $LoyaltyPointAppUsers  = ReportingComponent::getLoyaltyPointAppUsers($start_date, $end_date);
            
            $dataArray['TotalMsgUsage']       = $TotalMsgUsage;
            $dataArray['TotalAppInstall']     = $TotalAppInstall['data'];
            $dataArray['TotalMissedCall']     = $TotalMissedCall['data'];
           
            $dataArray['SmsMissCallusers']    = $SmsMissCallusers['data'];
            $dataArray['PosterMissCallusers'] = $PosterMissCallusers['data'];
            $dataArray['SignupUsers']         = $SignupUsers['data'];
            $dataArray['LoyaltyPointUsers']   = $LoyaltyPointUsers['data'];
            $dataArray['LoyaltyPointAppUsers']= $LoyaltyPointAppUsers['data'];
            
            $result =  ReportingComponent::getMissCallUserGift($start_date, $end_date);
            $dataArray['total_gift_grep']     = $result[0]['total_gifts'];
            $dataArray['total_gift_redeem']   = $result[0]['total_redeem'];
           
        }catch (Exception $ex) {
            $dataArray['errmsg'] = $ex->getMessage();
        }    
        $this->render("miss_call_user",$dataArray);    
    }
    public function actionTransaction($txnId,$dtFrom=null,$dtTo=null){
        //UsersComponent::getFullUserDetailsById(28);
        $userData = array();
        if(!empty($txnId)){
            
        }else{
            if( empty ($dtFrom) || empty($dtTo) ){
                $dtFrom = $dtTo = date("Y-m-d");
            }
        }
        
        if(Yii::app()->request->isPostRequest){
            
            $userTxnData = TransactionComponent::getPurchaselog(1,100,null,$userData["data"][0]["id"] );
            $userData = UsersComponent::getFullUserDetails($inputUserDetails, $searchTypeUserDetails);//by wallet user name
            $complaintsData = TransactionComponent::getComplaintDetails($userData["data"][0]["id"], "user_id");
            $commentsData = TransactionComponent::getComments($userData["data"][0]["id"], "user_id");
            
        }
               
        $dataArray = array();
        if(!empty($userData['status']) && $userData['status'] == "success"){
            $dataArray["userData"] = $userData["data"];
        }else{
            $dataArray["userData"] = array();
        }
        if(!empty($userTxnData['status']) && $userTxnData['status'] == "success"){
            $dataArray["userTxnData"] = $userTxnData["description"];
        }else{
            $dataArray["userTxnData"] = array();
        }
        
        if(!empty($complaintsData['status']) && $complaintsData['status'] == "success"){
            $dataArray["complaintsData"] = $complaintsData["description"];
        }else{
            $dataArray["complaintsData"] = array();
        }
        
        //$dataArray["userData"] = $userData;
        $this->render("search",$dataArray);
    }
    //recharge_id 	transaction_id 	mobile_number 	subscriber_id 	amount 	datetime 	operator_id 	circle_id 	
    //recharge_type ( 1=stv )	uuid 	device_type 	type 	profile_id 	status 	recharge_flag ( 1=mobile,2=dth,3=datacard,4=postpaid )
    
    public function actionGet_new_users(){
            print_r($_REQUEST);
    }
    
    public function actionDetail_reports(){
        $data = $_REQUEST;
        
        $req_param = array('type','sd','ed');
        foreach($req_param as $param){
            if(!isset($data[$param])){
                echo "missing parameter value";exit();
            }elseif($data[$param]==""){
                echo $param." cannot be blank";exit(); 
            }
        }
        $type = explode('_',$data['type']);
        $subtype = $type[0];
        $mainType = $type[1];
        
        $res = ReportingComponent::get_detailed_report($data);
        
        $i=0; 
        $startDate = $data['sd'];
        $endDate = $data['ed'];
        $dealData = array();
        foreach($res['data'] as $k=>$v){
            foreach($v as $key=>$val){
               if($key == "trans_id"){
                    $res['data'][$k][$key] = "<a href='/index.php/panel/search?search_submit=Search&txn_id=$val&category=refill&mode=wallet&type=detail' target='_blank'>".$val."</a>";
                }
                if($key == "cnt" && $data['type']=='online_recharge'){
                    $name = $res['data'][$i]['name'];
                    $res['data'][$k][$key] = "<a href='/index.php/panel/getrechargeDetails?name=$name&start=$startDate&end=$endDate' target='_blank'>".$val."</a>";
                }
                else if($key == "cnt" && ($data['type']=='wallet_deal' || $data['type']=='online_deal' || $data['type']=='total_deal')){
                    $deal_id = $res['data'][$i]['deal_id'];
                    $offer_id = $res['data'][$i]['offer_id'];
                   
                    $res['data'][$k][$key] = "<a href='/index.php/panel/getmobileDetails?deal_id=$deal_id&offer_id=$offer_id&start=$startDate&end=$endDate' target='_blank'>".$val."</a>";
                    
                }else if($key == "userId" && ($data['type']=='total_directTransaction')){
                        $user = $res['data'][$i]['userId'];
                        $res['data'][$k][$key] = "<a href='/index.php/panel/getuserdirecttxn?user=$user&sd=$startDate&ed=$endDate' target='_blank'>".$val."</a>";
                } 
               
            }
            if($data['type']=='wallet_deal' || $data['type']=='online_deal' || $data['type']=='total_deal'){
                $dealData[$k]['deal'] = $res['data'][$k]['deal'];
                $dealData[$k]['offer'] = $res['data'][$k]['offer'];
                $dealData[$k]['users grab'] = $res['data'][$k]['cnt'];
                $dealData[$k]['total stock'] = $res['data'][$k]['total_stock'];
               // $dealData[$k]['deal'] = $res['data'][$k][''];
            }
            
            $i++;
        }
         if($data['type']=='wallet_deal' || $data['type']=='online_deal' || $data['type']=='total_deal'){
              $dataArray['data_col']=  count($dealData) > 0?array_keys($dealData[0]):array();
                $dataArray['detail_data']=$dealData;
         }else{
             //$dealData = $res['data'];
            $dataArray['data_col']=  count($res['data']) > 0?array_keys($res['data'][0]):array();
            $dataArray['detail_data']=$res['data'];
         }
        $dataArray['data_count'] = $res['count'];
        $dataArray['data_title'] = $res['title'];
        $this->render("detail_reports",$dataArray);
    }    
   
    public function actionMap_reports(){
        $data = $_REQUEST;
        $type = $data['type'];
        if($type=='dealer'){
            $dataArray['detail_data']= ReportingComponent::get_dealermapdetails();
        }
        else if($type=='user'){
            $dataArray['detail_data']= ReportingComponent::get_usermapdetails();
        }
        else if($type=='retailer'){
            $dataArray['detail_data']= ReportingComponent::get_retailermapdetails();
        }
        
        $this->render("map_report",$dataArray);
    }
     public function actionRechargeDetail_reports(){
         $data = $_REQUEST;
            $RechargeDetails =  ReportingComponent::get_RechargeDetails($data);
            if($RechargeDetails){
                $dataArray['data_col']=  count($RechargeDetails['data']) > 0 ? array_keys($RechargeDetails['data'][0]):array();
                $dataArray['detail_data']=$RechargeDetails['data'];
                $dataArray['data_count'] = $RechargeDetails['count'];
                $dataArray['data_title'] = $RechargeDetails['title'];
                $dataArray['RechargeDetails'] = true;
           }
           else{
                $dataArray['message'] = "No record found";
           }
        $this->render("detail_reports",$dataArray);
        
     }
    public function actionGetmobileDetails(){
        $data = $_REQUEST;
         $req_param = array('deal_id');
        foreach($req_param as $param){
            if(!isset($data[$param])){
                echo "missing parameter value";exit();
            }elseif($data[$param]==""){
                echo $param." cannot be blank";exit(); 
            }
        }
        $connection = Yii::app()->db;
        $deal_id = $data['deal_id'];
        if(!isset($data['start']) || (isset($data['start']) && empty($data['start']))){
            $deal = $connection->createCommand("SELECT created FROM  `deals` WHERE id=$deal_id")->queryRow();
            $data['start']  = $deal['created'];
        }
        if(!isset($data['end']) || (isset($data['end']) && empty($data['end']))){
            $data['end'] = date('Y-m-d');
        }
         $res = ReportingComponent::get_user_details($data);
        $dataArray['data_col']=  count($res['data']) > 0?array_keys($res['data'][0]):array();
        $i=0; 
        $startDate = $data['start'];
        $endDate = $data['end'];
        foreach($res['data'] as $k=>$v){
            foreach($v as $key=>$val){
                if($key == "User"){
                    $mob_no = $res['data'][$k][$key];
                    $res['data'][$k][$key] = "<a href='/index.php/panel/search?wallet_user_name=$mob_no&deal_id=$deal_id&start=$startDate&end=$endDate' target='_blank'>".$val."</a>";
                }
            }
            $i++;
        } 
        $dataArray['detail_data']= $res['data'];
        $dataArray['data_count'] = $res['count'];
        $dataArray['data_title'] = $res['title'];
        $this->render("detail_reports",$dataArray);
    }
    public function delete_col(&$array, $offset) {
        return array_walk($array, function (&$v) use ($offset) {
             array_splice($v, $offset, 1);
        });
    }

    public function actionBlockUser(){
         $data = $_REQUEST;
        $responce= UsersComponent::blockUser($data);
        //echo "<center><b>User blocked successfully.</b></center>";
//        if($responce){
//            $msg = "User blocked successfully";
//        }else{
//            $msg = "Unable to block user";
//        }
            return $responce;
    }
    
    public function actionUnblockUser(){
         $data = $_REQUEST;
        $responce= UsersComponent::unblockUser($data);
        if($responce){
            $userid = $data['userid'];
            $userData = Yii::app()->db->createCommand("SELECT user_name FROM users where id=$userid")->queryRow();
            $date = date('Y-m-d h:i:s');
            $user = isset($userData['user_name']) ? $userData['user_name'] : "";
            $subject = "User unblocked now";
            $message = "user $user unblocked on $date";
            $sender = 'support@pay1.in';
            $to = 'swapnil@mindsarray.com,sunilc@mindsarray.com';
            GeneralComponent::sendMail($to, $subject, $message,$sender);
        }
         
        //echo "<center><b>User blocked successfully.</b></center>";
//        if($responce){
//            $msg = "User unblocked successfully";
//        }else{
//            $msg = "Unable to unblock user";
//        }
            return $responce;
    }
    
    public function actionPayu() {
         $this->layout = '//layouts/empty';
         $dataArray = $_REQUEST;
        // $session_val = $dataArray['user_sess_val'];
        // $session_id = $dataArray['user_sess_id'];
        // $_SESSION[$session_val] = $session_id;
        /* $payu_detail = OnlineComponent::payu_getconfig();
         $banklist = GeneralComponent::bank_list();
         $dataArray['key'] = "gtKFFx" ;
         $dataArray['txnid'] = "100018350" ;
         $dataArray['amount'] = "53" ;
         $dataArray['productinfo'] = "recharge" ;
         $dataArray['firstname'] = "swapnilT" ;
         $dataArray['email'] = "umeshrathod01@gmail.com" ;
         $dataArray['phone'] = "9975629244" ;
         $dataArray['surl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/success" ;
         $dataArray['furl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/failure" ;
         $dataArray['curl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel" ;
         $dataArray['touturl'] = "http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout" ;
         $dataArray['hash'] = "ab9a62676cb21f67de6a5b38511c1dc360f9fd87a40aa2c10dd189a520b7b225fcf8a9d156d8f56e1aa1f4b47c40a3c51ef41eae0277c722a1c8920d76b3e266" ;
         $dataArray['user_credentials'] = "gtKFFx:99" ;
         $dataArray['user_sess_id'] = "6a6a4ggdjqe5jlg3brgi0vofb3" ;
         $dataArray['user_sess_val'] = "PHPSESSID" ;
         $dataArray['banklist'] = $banklist;*/
         /* $key = 'gtKFFx';
        $txnid = '100017765';
        $amount = '10';
        $productinfo = 'recharge';
        $firstname = 'swapnil';
        $email = 'swapnil@mindsarray.com';
        $salt = 'eCwWELxi';
        $phone = '9975629244';
        $surl = 'http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/success';
        $furl = 'http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/failure';
        $curl = 'http://b2c.pay1.loc/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel';
        $hash =hash('sha512',"$key|$txnid|$amount|$productinfo|$firstname|$email|||||||||||$salt");
       
        
        $dataArray['key']= $key;
        $dataArray['txnid'] = $txnid;
        $dataArray['amount'] = (float)$amount;
        $dataArray['firstname'] = $firstname;
        $dataArray['productinfo'] = $productinfo;
        $dataArray['email'] = $email;
        $dataArray['salt'] = $salt;
        $dataArray['phone'] = $phone;
        $dataArray['surl'] = $surl;
        $dataArray['furl'] = $furl;
        $dataArray['curl'] = $curl;
        $dataArray['hash'] = $hash; 
        $dataArray['userId'] = '1'; */
        $dataArray['B2C_SUBMIT_URL'] = 'https://test.payu.in/_payment'; 
        $dataArray['UserCardDetails'] = DealsComponent::getuser_card(array('var1'=>$dataArray['user_credentials'],'command'=>'get_user_cards')); //"gtKFFx:919876543210";
        $this->render("payu",$dataArray);
    }
    public function actionFreegift(){
        $dataArray = array();
        $Reqdata = $_REQUEST;
        if(isset($_REQUEST['search_freegift']) && $_REQUEST['search_freegift'] === "Search"){
            $msg = '';
            if(isset($_REQUEST['voucher']) && !empty($_REQUEST['voucher'])){
                $Reqdata['code'] = $_REQUEST['voucher'];
            }
            else if(isset($_REQUEST['custMobNum']) && !empty($_REQUEST['custMobNum'])){
                $Reqdata['code'] = $_REQUEST['custMobNum'];
            }
            if(!empty($Reqdata['code'])){
                $data = GeneralComponent::searchFreeGift($Reqdata,true);
                if(isset($data[0]['coupon'])){
                    $dataArray['display']=TRUE;
                    $dataArray['freegift_data']= $data;
                }
            }
           
        }         
         
         $this->render("FreeGift",$dataArray);
     }
     public function actionRedeem_Freebie() {
         $Reqdata = $_REQUEST;
         if(isset($_REQUEST['custMobNum']) && !empty($_REQUEST['custMobNum'])){
             $Reqdata['code'] = $_REQUEST['custMobNum'];
         }
         else if(isset($_REQUEST['voucher']) && !empty($_REQUEST['voucher'])){
             $Reqdata['code'] = $_REQUEST['voucher'];
         }
         $data = GeneralComponent::searchFreeGift($Reqdata);
         echo $data['description']."_".$data['status'];
     }
     
     public function actionRedeem_Coupon() {
         $Reqdata = $_REQUEST;
          $data = GeneralComponent::redeem_coupons($Reqdata);
           echo $data['description']."_".$data['status'];
     }
     
     public function actionGetDealMap(){
         $deal_id = $_REQUEST['id'];
         $_REQUEST['type']='dealer';
         $dataArray['detail_data']= ReportingComponent::get_dealermapdetails($deal_id);
         $this->render("map_report",$dataArray);
     }
     
    public function actionUploadExtraIncentiveList(){
        $msg = "";
        $data = $_POST;
        $distruber_id = array();
        if(isset($_POST["Submit"])) {
            try{
              $filename = $_FILES['file1']['tmp_name'];
              $handle = fopen($filename, "r");                       //Open .csv file for reading
              $i=0;
              while(!feof($handle)){
                $distruber_id[++$i]= fgetcsv($handle); 
              }
              $b=0;
              for($j=1,$a=0 ; $j<$i ; $j++){
               $res[$b++] = MultiSortComponent::Distributor_Extra_Incentive($data,$distruber_id[$j][$a]);
              }
            Yii::import('application.vendor.PHPExcel',true);    
            $objPHPExcel= new PHPExcel();
            
            $styleArray = array(
              'font'  => array(
              'bold'  => true,
              'color' => array('rgb' => 'FF0000'),
              'size'  => 15,
              'name'  => 'Verdana'
            ));
             
             $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Some text');
             $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
             $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
             $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
            
            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A1", 'Airtel')
                        ->setCellValue("B1", 'Idea')
                        ->setCellValue("C1", 'Bill payment');
            $i = 2;
            foreach ($res as $fields) { 
             $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", $fields[0])
                        ->setCellValue("B$i", $fields[1])
                        ->setCellValue("C$i", $fields[2]);
             $i++;
            }
                        
             // Rename worksheet
             $objPHPExcel->getActiveSheet()->setTitle('Simple');
             // Set active sheet index to the first sheet, so Excel opens this as the first sheet
             $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
             $objWriter->save('/tmp/dis_incentive.xls');
             chmod("/tmp/dis_incentive.xls",0755);
            }catch(Exception $ex){
            $msg = $ex->getMessage();
            Yii::app()->user->setFlash('error', $msg);
            }
        }
        $this->render('admin');
    }
    
     public function actionGetrechargeDetails(){
        $data = $_REQUEST;
        $data['type']='online';
        $res = ReportingComponent::get_OnlineRechargeDetails($data);
        $dataArray['data_col']=  count($res['data']) > 0?array_keys($res['data'][0]):array();
        $i=0; 
        $startDate = $data['start'];
        $endDate = $data['end'];
        
        $dataArray['detail_data']= $res['data'];
        $dataArray['data_count'] = $res['count'];
        $dataArray['data_title'] = $res['title'];
        $this->render("detail_reports",$dataArray);
    }
    
    
    public function actionGetuserdirecttxn(){
        $data = $_REQUEST;
        $res = ReportingComponent::getUserDirectTxn($data);
        $dataArray['data_col']=  count($res['data']) > 0?array_keys($res['data'][0]):array();
        
        $dataArray['detail_data']= $res['data'];
        $dataArray['data_count'] = $res['count'];
        $dataArray['data_title'] = $res['title'];
        $this->render("detail_reports",$dataArray);
    }
    
    public function actionSettransaction(){
        file_put_contents("/tmp/Transaction", " |".  json_encode($_REQUEST)." \n", FILE_APPEND | LOCK_EX);
       if(isset($_REQUEST['failedTransaction']) && $_REQUEST['failedTransaction'] === "submit"){
        $data = $_REQUEST;
        $obj = new TransactionComponent();
        $res = $obj->set_TransactionInprocess($data);
        $responce = $res['description'];
        Yii::app()->user->setFlash('Transaction',$responce);
            $this->render('setTransaction');
       }else{
            $this->render('setTransaction');
       }
    }
    
     
}
