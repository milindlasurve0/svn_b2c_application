<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_1';
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array( 'create','adduser'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'testcall', 'test','index', 'view'),
                'users' => array('@'),
                'deniedCallback'=>ApiResponse::unAuthorizedCall()
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                //'users' => array('admin'),
                'users' => array('9029289711'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        try {
            //$writelog = new dumpLog("signup", "user-controller");
            //$logger = $writelog->logger;
            $model = new Users;
            $apiflag = FALSE;
            if (isset($_REQUEST['api']) && $_REQUEST['api'] == 'true') {
                $_POST = array('Users' => $_REQUEST);
                $apiflag = TRUE;
            }
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Users'])) {
                $model->setAttribute('user_name', $_POST['Users']['mobile_number']);
                $model->attributes = $_POST['Users'];
                try {
                    if ($model->save()) {
                        $wallet_model = new Wallets();
                        $wallet_model->setAttribute('users_id', $model->getAttribute('id'));
                        try {
                            if ($wallet_model->save()) {
                                Yii::log("new user and wallet created successfully", CLogger::LEVEL_INFO, "signup");
                                if ($apiflag) {
                                    $responseData = array(
                                        'status' => 'SUCCESS',
                                        'response_code' => 200,
                                        'response_message' => 'User registered successfully'
                                    );
                                    ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                                } else {
                                    $this->redirect(array('view', 'id' => $model->id));
                                }
                            } else {
                                    $this->loadModel($id)->delete();
                                $code = 2021;
                                $msg = "Wallet creation failed";
                                $responseData = array(
                                    'status' => 'FAILED',
                                    'response_code' => "400",
                                    'response_message' => array('error_code' => $code, 'error_message' => $msg)
                                );
                                ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                            }
                        } catch (Exception $ex) {
                            //print $ex->getMessage();
                            $msg = $ex->getMessage();
                            $model->addError('Exception', $msg);
                            if ($apiflag) {
                                $code = 2022;
                                $responseData = array(
                                    'status' => 'FAILED',
                                    'response_code' => "400",
                                    'response_message' => array('error_code' => $code, 'error_message' => $msg)
                                );
                                ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                            }
                            //$logger->debug("Exception : ".$ex->getTraceAsString());
                        }
                    } else {
                        if ($apiflag) {
                            $err_desc = array();
                            foreach ($model->getErrors() as $err_code => $err_array) {
                                list($code, $msg) = explode(':', $err_array[0]);
                                array_push($err_desc, array('error_code' => trim($code), 'error_message' => trim($msg)));
                            }
                            $responseData = array(
                                'status' => 'FAILED',
                                'response_code' => "400",
                                'response_message' => $err_desc
                            );
                            unset($err_desc);
                            ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                        }
                    }
                } catch (Exception $ex) {
                    $msg = $ex->getMessage();
                    $model->addError('Exception', $msg);
                    if ($apiflag) {
                        $code = 2022;
                        $responseData = array(
                            'status' => 'FAILED',
                            'response_code' => "400",
                            'response_message' => array('error_code' => $code, 'error_message' => $msg)
                        );
                        ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                    }
                    //$logger->debug("Exception : ".$ex->getTraceAsString());
                }
            }
            $this->render('create', array(
                'model' => $model,
            ));
        } catch (Exception $ex) {
            //print_r($ex->getMessage());
            $msg = $ex->getMessage();
            $model->addError('Exception', $msg);
            if ($apiflag) {
                $code = 2022;
                $responseData = array(
                    'status' => 'FAILED',
                    'response_code' => "400",
                    'response_message' => array('error_code' => $code, 'error_message' => $msg)
                );
                ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
            }
            //$logger->debug("Exception : ".$ex->getTraceAsString());
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Users');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionTestcall() {
        echo json_encode($_REQUEST);

        $dataProvider = Users::model()->findAll();
        if (is_null($dataProvider)) {
            $message = array(sprintf('No items where found for model <b>%s</b>', $dataProvider));
        } else {
            $message = $dataProvider;
        }
        $dataProvider = array('status' => 'SUCCESS', 'message' => $message);
        ApiResponse::sendResponse(200, CJSON::encode($dataProvider));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Users the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionTest() {
        echo "111";exit();
        $userdetail = Users::model()->findByPk(Yii::app()->user->id);
        $responseData = array(
            'status' => 'SUCCESS API call',
            'response_code' => 200,
            'response_message' => $userdetail->getAttributes()
        );
        ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
    }

    
    public function actionAdduser() {
        $model = new Users;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        UsersComponent::test($model);
    }
}
