<?php

class WebController extends Controller {
   
        /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2_2';
    private $actionData = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $this->layout = (UsersComponent::isAdmin()) ? '//layouts/column2_2' : '//layouts/column2_3';
        if ( !( UsersComponent::isDealer() || UsersComponent::isAdmin() )){
            $this->redirect(array('panel/index'));exit;
        }
          
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('web_section', 'web_details' , 'web_meta_tags','HR_panel'),
                'users' => array('*'),
            )
            
        );
    }

    
    public function actionWeb_Section(){
       $result = array();
       $this->render('websection');
    }
    
    public function actionWeb_Details(){
        $data = $_REQUEST;
        if($_FILES['img_file_upload']['size'] > 0 && $_FILES['img_file_upload']['size']< 355555 && $_FILES['img_file_upload']['name'] != "" ){
            $new_file_name="";
            $size = number_format((float)$_FILES['img_file_upload']['size'] /1024,2,'.','');
            $tmp = explode(".", $_FILES["img_file_upload"]["name"]);
            $ext = end($tmp);
        
            $new_file_name = Yii::app()->params['GLOBALS']['_WEB_IMG_DIR'].'web_img'.$tmp[0].'.'.$ext;
                if(move_uploaded_file($_FILES["img_file_upload"]["tmp_name"], $new_file_name)){
                    $data['img_url'] = $new_file_name;
                }
        }
        $date = GeneralComponent::getFormatedDate();
        $data['date'] = $date;
        WebComponent::setWebSectionDetails($data);
        $this->render('websection');  
    }

    public function actionWeb_Meta_Tags(){
        if(!empty($_REQUEST))
        {
            $data = $_REQUEST;    
            $date = GeneralComponent::getFormatedDate();
            $data['date'] = $date;
            WebComponent::setWebMetaTagDetails($data);
        }
       $this->render('webMetaTags');
    }
    
    public function actionHR_panel(){
       if(!empty($_REQUEST))
        {
            $data = $_REQUEST;    
            $date = GeneralComponent::getFormatedDate();
            $data['date'] = $date;
            WebComponent::setHRpanelDetails($data);
        }
        $result = array();
        $this->render('HRpanel');
    }
    
    
    
}//End of the WebController Class