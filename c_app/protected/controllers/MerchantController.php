<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MerchantController extends Controller {
    
      public $layout = '//layouts/merchant_layout';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {        
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/merchant_layout' : '//layouts/merchant_layout';
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array( 'create','test'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('info','redeem','freeGiftReport','dealerLogin','logout','dealer'),
                'users' => array('@'),
                'deniedCallback'=>ApiResponse::unAuthorizedCall()
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete','search'),
                'users' => UsersComponent::getAdminusers(),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){
        if(UsersComponent::isAdmin(Yii::app()->user->username)){
            $this->actionadmin();
        }
        else if(UsersComponent::isDealer(Yii::app()->user->username)){
            $this->actionDealer();
        }
    }
    
    public function actionadmin(){        
        //echo "Panel Recharge page";
        $this->render('admin');
    }
   
    public function actionDealer(){
        $dataArray = array();
        $this->actionInfo();
    }
    
    public function actionInfo(){  
       // $this->layout = '//layouts/blank';
         $this->layout = '//layouts/merchant_layout';
       // $this->actionDealerLogin();
        $dealerId = Yii::app()->user->id;
        
        $dataArray["dealerData"] = array();
        $dataArray["dealerData"] = MerchantComponent::getDealerInfo($dealerId);
        if(count($dataArray["dealerData"])>0){
            $dataArray["offerData"] = MerchantComponent::getAlloffers($dealerId);
            $dataArray["reportData"] = MerchantComponent::getfreegiftreport($dealerId);
            $this->render('dealerinfo',$dataArray);
        }
        
    }
    
    public function actionRedeem(){
        $dataArray = array();
        $dataArray['freegift_data'] = array();
        $Reqdata = $_REQUEST;
        $dealerId = Yii::app()->user->id;        
        $dataArray["dealerData"] = array();
        $dataArray['display']=false;
        $dataArray["dealerData"] = MerchantComponent::getDealerInfo($dealerId);
        if(isset($_REQUEST['search_freegift']) && $_REQUEST['search_freegift'] === "Search"){
            $dataArray['display']=true;
            $msg = '';
            $data = GeneralComponent::getCoupons($Reqdata);
                //if(isset($data[0]['voucher'])){
                    $dataArray['freegift_data']= $data;
               // }
//            if(isset($_REQUEST['voucher']) && !empty($_REQUEST['voucher'])){
//                $Reqdata['code'] = $_REQUEST['voucher'];
//            }
//            else if(isset($_REQUEST['custMobNum']) && !empty($_REQUEST['custMobNum'])){
//                $Reqdata['code'] = $_REQUEST['custMobNum'];
//            }
//            if(!empty($Reqdata)){
//                $dataArray['display']=TRUE;
//                $data = GeneralComponent::searchFreeGift($Reqdata,true);
//                if(isset($data[0]['coupon'])){
//                    $dataArray['freegift_data']= $data;
//                }
//            }
           
        }         
        $this->render('redeemFreegift',$dataArray);
    }
    
    public function actionFreeGiftReport(){
        $dealerId = Yii::app()->user->id;
        $dataArray["dealerData"] = array();
        $dataArray["dealerData"] =  MerchantComponent::getDealerInfo($dealerId);
        $dataArray["offers"] = MerchantComponent::getAlloffers($dealerId);
        if(isset($_REQUEST['start_date']) && isset($_REQUEST['end_date'])){
            $data = array();
            $data = $_REQUEST;
            $data['dealer_id'] = $dealerId;
            //$dataArray["dealerData"] = array();
            $dataArray["reportData"] = MerchantComponent::getfreegiftSearchReport($data);
        }
        $this->render('freeGiftReport',$dataArray);
    }
    
    public function actionLogout() {
        $apiflag = FALSE;
        if (isset($_REQUEST['api']) && $_REQUEST['api'] == 'true') {
            $apiflag = TRUE;
        }

        Yii::app()->user->logout();
        if (!$apiflag) {
            //$this->redirect(Yii::app()->homeUrl);
            $this->redirect('/site/dealerlogin');
           // $this->actionDealerLogin();
        } else {
            $responseData = array(
                'status' => 'SUCCESS',
                'response_code' => 200,
                'response_message' => 'Logged out successfully'
            );
            ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
        }
    }
}

