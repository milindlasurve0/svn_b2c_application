<?php

class SiteController extends Controller {

    public $layout = '//layouts/column1_1';
    /**
     * Declares class-based actions.
     */
    
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        if(strpos($_SERVER['HTTP_HOST'],'bp.pay1')===0){
             $this->actionDealerlogin();exit;
        }
        $model = new LoginForm;
        $dumlog = new dumpLog('auth', 'login');
        $write_log = $dumlog->logger;
        //$write_log->info("test log");
        $apiflag = FALSE;
                
        if (isset($_POST['api']) && $_POST['api'] == 'true') {
            if(isset($_POST['apiname'])){ unset($_POST['apiname']);}        
            if(isset($_POST['api'])){ unset($_POST['api']);}
            $_POST = array('LoginForm' => $_POST, 'yt0' => 'Login');
            $apiflag = TRUE;
        }
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        //var_dump($apiflag);//exit();
        // collect user input data
        if (isset($_POST['LoginForm']) && WebComponent::isInternalIP()) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if ($apiflag) {
                    $model1 = Users::model()->findByAttributes(array('user_name' => $model->username));
                    $responseData = array(
                        'status' => 'SUCCESS',
                        'response_code' => 200,
                        'response_message' => $model1->getAttributes()
                    );
                    ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                } else {
                    echo "<pre>";
                    print_r(Yii::app()->user);
                    $this->redirect(array('panel/index'));exit;
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            } else {
                if ($apiflag) {
                    var_dump($apiflag);
                    $pass_err = $model->getErrors('password');
                    $user_err = $model->getErrors('username');
                    $resdata = array();
                    
                    if (count($pass_err) > 0) {
                        $resdata[] = array('error_code' => 102, 'error_message' => $pass_err[0]);
                    }
                    if (count($user_err) > 0) {
                        $resdata[] = array('error_code' => 101, 'error_message' => $user_err[0]);
                    }
                    $responseData = array(
                        'status' => 'FAILED',
                        'response_code' => "400",
                        'response_message' => $resdata
                    );
                    ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                }
            }
        }
        // display the login form
        if (!$apiflag) {
            $this->render('login', array('model' => $model));
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $apiflag = FALSE;
        if (isset($_REQUEST['api']) && $_REQUEST['api'] == 'true') {
            $apiflag = TRUE;
        }

        Yii::app()->user->logout();
        if (!$apiflag) {
            $this->redirect(Yii::app()->homeUrl);
        } else {
            $responseData = array(
                'status' => 'SUCCESS',
                'response_code' => 200,
                'response_message' => 'Logged out successfully'
            );
            ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
        }
    }
    
    public function actionDealerlogin() {
        $this->layout = '//layouts/blank';
        $model = new LoginForm;
        $dumlog = new dumpLog('auth', 'login');
        $write_log = $dumlog->logger;
        $error = "";
         // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        //var_dump($apiflag);//exit();
        // collect user input data
       //die;
        
        if (isset($_POST) && !empty($_POST)) {
            $_POST['username'] = $_POST['username']."0";
            $model->attributes = $_POST;
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login() ) {
                if(UsersComponent::isDealer(Yii::app()->user->username)){
                    echo "<pre>";
                    print_r(Yii::app()->user);
                    $this->redirect(array('merchant/dealer'));exit;
                    //$this->redirect(Yii::app()->user->returnUrl);
               }
                else{
                    $error = 'Incorrect username or password.';
                }
            }
            else{
                 $error = 'Incorrect username or password.';
            }
            
        }
            // display the login form
            $this->render('dealerlogin', array('model' => $model,'error'=>$error));
   }
}
