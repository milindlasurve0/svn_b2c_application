<?php

class RechargesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
            $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_2';
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('mobile','Dtd','delegate','schedule'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','wallet','online'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionindex(){
            $rec_data = $_REQUEST;
            return (strtolower(trim($rec_data['payment']))=='wallet')?$this->actionWallet():$this->actionOnline();                
        }

        /** Custom UI */
        public function actionMobile(){
            $rec_data = $_REQUEST;
            $res = Yii::app()->user->username;
            $this->render('mobile',array('received_data'=>$rec_data,'datares'=>$res));
        }
        
        public function actionDth(){
            $this->render('dth');
        }
        
        public function actionDelegate(){
            $this->render('delegate');
        }
        
        public function actionSchedule(){
            $this->render('schedule');
        }
}
