<?php

class ComplainsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_2';
        return array(
                array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions' => array('create', 'update'),
                        'users' => array('@'),
                ),
                array('deny', // deny all users
                        'users' => array('*'),
                ),
        );
    }

    public function actionCreate() {
        $data = $_REQUEST;
        $resdata = isset($data['complain'])?ServiceComponent::manageComplain():"";
        $this->render('create',array('resdata'=>$resdata));
    }
    
    public function actionUpdate() {
        $rec_data = $_REQUEST;
        $this->render('update');
    }
    
}
