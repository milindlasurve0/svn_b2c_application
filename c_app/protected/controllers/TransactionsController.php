<?php

class TransactionsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $logger;
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_2';
        $dumlog = new dumpLog('Transactions', 'transactions');
        $this->logger = $dumlog->logger;
        return array(
                array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('test', 'view','online_response'),
                        'users' => array('*'),
                ),
                array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions' => array('create', 'update', 'wallet', 'online', 'payment', 'process_payment','service'),
                        'users' => array('@'),
                ),
                array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => array('admin', 'delete'),
                        'users' => array('admin'),
                ),
                array('deny', // deny all users
                        'users' => array('*'),
                ),
        );
    }

    public function actionindex() {
        $rec_data = $_REQUEST;
        return (strtolower(trim($rec_data['payment'])) == 'wallet') ? $this->actionWallet() : $this->actionOnline();
    }

    /** Custom UI */
    public function actionWallet() {
        $rec_data = $_REQUEST;
        $res = Yii::app()->user->username;
        $this->render('wallet', array('received_data' => $rec_data, 'datares' => $res));
    }

    public function actionOnline() {
        $this->render('online');
    }

    public function actionPayment() {
        $data = $_REQUEST;
        $wdetail = count(WalletComponent::getWalletDetail()) > 0 ? WalletComponent::getWalletDetail() : "";
        $res = "";
        Yii::app()->session['Transaction_trackid'] = GeneralComponent::get_unique_trackId();
        $data['trackid']= Yii::app()->session['Transaction_trackid'];
        if(isset($data['billpayment'])){
            $account_detail = ServiceComponent::calculateServiceCost($data['amount']);
            $data['amount'] = $account_detail['amount'];
            $data += $account_detail;
        }
        if(isset($data['subscriber_id'])){
            $data['service']['label'] = 'Subscriber Id';
            $data['service']['value'] = $data['subscriber_id'];
        }
        if(isset($data['mobile_number'])){
            $data['service']['label'] = 'Mobile Number';
            $data['service']['value'] = $data['mobile_number'];
        }
        if(isset($data['flag']) && $data['flag']==3){
            $data['service']['label'] = 'Data Card Number';
            $data['service']['value'] = $data['mobile_number'];
        }
        $this->logger->info(Yii::app()->session['Transaction_trackid']." : New Transaction started");
        $this->render('payment', array('form_data' => $data, 'detail' => $res, 'wallet' => $wdetail, 'service_cost' => $res));
    }

    public function actionProcess_payment() {
        $data = $_REQUEST;
        $this->logger->info(Yii::app()->session['Transaction_trackid']." : transaction in process payment");
        $res = "";
        if (isset($data['payment'])) {
            switch($data['paymentopt']){
            case 'wallet':
                $res = $this->actionCallProcessor($data);
                break;
            case 'online':
                $res = $this->actionOnlineProcessor($data);
                break;            
            }
            $res = json_decode($res);
            Yii::app()->user->setState('payment_res', $res);
            $this->redirect(array('view'));
        }
    }
    
                //$res = $this->actionCallProcessor($data);
                
    public function actionCallProcessor($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        if(isset($data['recharge'])){
            $t_result = WalletComponent::PaymentTranction();
        }elseif(isset($data['billpayment'])){
            $t_result = WalletComponent::PaymentTranction();
        }
        $trans_result = json_decode($t_result, true);
        $res = (!($trans_result['status'] === 'success')) ? "Error in Processing please try again" : "Your Request has been accepted. Transaction ID : " . $trans_result['description']['transaction_id'];
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . $trans_result_final);
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }
    
    public function actionOnlineProcessor($data=NULL){        
        $res = WalletComponent::PaymentTranction($data);
        //$res = OnlineComponent::payment($data);
        print_r($res);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " Transaction result : " . json_encode($res));
        if($res['status']==="success"){
            echo "success";
        }        
        return $res;
    }

    public function actionView() {
        $res = Yii::app()->user->getState('payment_res');
        //$this->render('view', array('payment_res' => json_encode($res)));        
        print_r($res);exit();
        $this->render('view', array('payment_res' => $res['description']['form_content']));
    }

    public function actionTest() {
        $params = array('number' => '7738832731', 'amount' => 110, 'trans_id' => uniqid(), 'operator' => 2, 'stv' => 0);
        //VendorComponent::callPay1($params);
        print_r($params);
    }
    
    public function actionService() {
        $data = $_REQUEST;        
        $result = json_decode(ServiceComponent::setDelegateRecharge($data),TRUE);
        $msg = $result['description'];
        $this->render('service',array('msg'=>$msg));
    }
    
    public function actionOnline_response($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->layout = '//layouts/column1_1' ;
        $this->render('online',array('data'=>$data));   
    }
    
    
}
