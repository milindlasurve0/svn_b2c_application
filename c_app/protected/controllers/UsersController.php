<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_1';
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array( 'create','adduser','login','logout','signup','manage_password'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'testcall', 'test','index', 'view'),
                'users' => array('@'),
                'deniedCallback'=>ApiResponse::unAuthorizedCall()
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => UsersComponent::getAdminusers(),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Users');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Users the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLogin() {
        if (isset($_POST['login']) && strlen(trim($_REQUEST['username'])) > 0 && strlen(trim($_REQUEST['password'])) > 0) {
            $login_model = new LoginForm();
            $login_model->setAttributes(array('username' => $_POST['username'], 'password' => $_POST['password']));
            if ($login_model->validate() && $login_model->login()) {
                $this->redirect(array('panel/index'));
            }
            echo "<script>alert('hh')</script>";
            Yii::app()->session['err1']='test';
            print_r(Yii::app()->session['err']);
            $this->redirect(Yii::app()->user->returnUrl);
        } else {
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $apiflag = FALSE;
        if (isset($_REQUEST['api']) && $_REQUEST['api'] == 'true') {
            $apiflag = TRUE;
        }

        Yii::app()->user->logout();
        if (!$apiflag) {
            $this->redirect('https://b2c.pay1.in');
            //$this->redirect(Yii::app()->homeUrl);
        } else {
            $responseData = array(
                'status' => 'SUCCESS',
                'response_code' => 200,
                'response_message' => 'Logged out successfully'
            );
            ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
        }
    }
    
    public function actionSignup(){
        $data = $_REQUEST;
        if(isset($data['action']) && $data['action']=="next"){
            $data1 = UsersComponent::createUser($data);
            $result = json_decode($data1,TRUE);
            if($result['status']==="success"){
                Yii::app()->user->setState('mobile_number', $data['mobile_number']);
                $this->redirect(array('users/manage_password'));
            }else{
                $data = $result;
                $data['error'] = $result['description'];
                $this->render('signup',array('data'=>$data));                
            }
        }else{
            $this->render('signup',array('data'=>$data));
        }
    }
    
    public function actionManage_password($data=NULL){
        $data = !is_null($data)?$data:$_REQUEST;
        if(isset($data['action']) && $data['action']=="finish"){
            $data = UsersComponent::updatePassword($data);
            $result = json_decode($data,TRUE);
            $data = $result;
            if($result['status']==="success"){
                $data['error'] = "click here to login";
            }else{
                $data['error'] = $result['description'];
            }
            $this->render('password',array('data'=>$data));
        }else{
            $this->render('password',array('data'=>$data));
        }
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        try {
            //$writelog = new dumpLog("signup", "user-controller");
            //$logger = $writelog->logger;
            $model = new Users;
            $apiflag = FALSE;
            if (isset($_REQUEST['api']) && $_REQUEST['api'] == 'true') {
                $_POST = array('Users' => $_REQUEST);
                $apiflag = TRUE;
            }
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Users'])) {
                $model->setAttribute('user_name', $_POST['Users']['mobile_number']);
                $model->attributes = $_POST['Users'];
                try {
                    if ($model->save()) {
                        $wallet_model = new Wallets();
                        $wallet_model->setAttribute('users_id', $model->getAttribute('id'));
                        try {
                            if ($wallet_model->save()) {
                                Yii::log("new user and wallet created successfully", CLogger::LEVEL_INFO, "signup");
                                if ($apiflag) {
                                    $responseData = array(
                                        'status' => 'SUCCESS',
                                        'response_code' => 200,
                                        'response_message' => 'User registered successfully'
                                    );
                                    ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                                } else {
                                    $this->redirect(array('view', 'id' => $model->id));
                                }
                            } else {
                                    $this->loadModel($id)->delete();
                                $code = 2021;
                                $msg = "Wallet creation failed";
                                $responseData = array(
                                    'status' => 'FAILED',
                                    'response_code' => "400",
                                    'response_message' => array('error_code' => $code, 'error_message' => $msg)
                                );
                                ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                            }
                        } catch (Exception $ex) {
                            //print $ex->getMessage();
                            $msg = $ex->getMessage();
                            $model->addError('Exception', $msg);
                            if ($apiflag) {
                                $code = 2022;
                                $responseData = array(
                                    'status' => 'FAILED',
                                    'response_code' => "400",
                                    'response_message' => array('error_code' => $code, 'error_message' => $msg)
                                );
                                ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                            }
                            //$logger->debug("Exception : ".$ex->getTraceAsString());
                        }
                    } else {
                        if ($apiflag) {
                            $err_desc = array();
                            foreach ($model->getErrors() as $err_code => $err_array) {
                                list($code, $msg) = explode(':', $err_array[0]);
                                array_push($err_desc, array('error_code' => trim($code), 'error_message' => trim($msg)));
                            }
                            $responseData = array(
                                'status' => 'FAILED',
                                'response_code' => "400",
                                'response_message' => $err_desc
                            );
                            unset($err_desc);
                            ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                        }
                    }
                } catch (Exception $ex) {
                    $msg = $ex->getMessage();
                    $model->addError('Exception', $msg);
                    if ($apiflag) {
                        $code = 2022;
                        $responseData = array(
                            'status' => 'FAILED',
                            'response_code' => "400",
                            'response_message' => array('error_code' => $code, 'error_message' => $msg)
                        );
                        ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
                    }
                    //$logger->debug("Exception : ".$ex->getTraceAsString());
                }
            }
            $this->render('create', array(
                'model' => $model,
            ));
        } catch (Exception $ex) {
            //print_r($ex->getMessage());
            $msg = $ex->getMessage();
            $model->addError('Exception', $msg);
            if ($apiflag) {
                $code = 2022;
                $responseData = array(
                    'status' => 'FAILED',
                    'response_code' => "400",
                    'response_message' => array('error_code' => $code, 'error_message' => $msg)
                );
                ApiResponse::sendResponse(200, ApiResponse::getObjectEncoded('response', $responseData));
            }
            //$logger->debug("Exception : ".$ex->getTraceAsString());
        }
    }
    
    public function test(){
    	GeneralComponent::sendNotification(array(9),"Trial Nofication","Testing Testing","notification",1,1);
    }
}
