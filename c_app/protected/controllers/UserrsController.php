<?php

class UserrsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        $this->layout = (Yii::app()->user->isGuest) ? '//layouts/column1_1' : '//layouts/column2_3';
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','upload'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin','7738832731'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $model=new Userrs;
            $model->created_datetime = date("Y-m-d H:i:s");
            $model->modified_datetime = date("Y-m-d H:i:s");
            $model->salt = 0;
            if(UsersComponent::isAdmin(Yii::app()->user->username)){
                $model->role = $model->role;
            }if(UsersComponent::isReport(Yii::app()->user->username)){
                $model->role = $model->role;
            }else{
                $model->role = 3;
            }

            $profile = new UserProfile;
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            if(isset($_POST['Userrs']))
             {
                 //$model->attributes=$_POST['Userrs'];
                 if($_POST['Userrs']['role']==3){
                    // $model->user_name = trim($_POST['Userrs']['mobile_number']);//substr($_POST['Userrs']['mobile_number'],0,  strlen($_POST['Userrs']['mobile_number'])-1);
                      $model->dealer_name = trim($_POST['Userrs']['user_name']);
                 }
                 $model->user_name = trim($_POST['Userrs']['mobile_number']);
                 $model->password = $_POST['Userrs']['password'];
                 $model->status = $_POST['Userrs']['status'];
                 $model->mobile_number = $_POST['Userrs']['mobile_number'];
                 $model->role = $_POST['Userrs']['role'];

                 $model->modified_datetime = date("Y-m-d H:i:s");
                 $model->password = md5($model->password);
                 try{
                    if($model->save()){
                        $id = $model->id;
                        $profile->attributes = $_POST['UserProfile'];
                        $profile->user_id = $id;
                        $profile->save();
                    }
                    $this->redirect(array('view','id'=>$model->id));
                 }
                 catch(Exception $ex){
                     echo "duplicate entry";die;
                 }
                 
            }

            $this->render('create',array(
                    'model'=>$model,'pmodel'=>$profile,
            ));
     }
        
        /**
         * Create dealer by uploading xl or xls file
         * this dealer have mobile number as null and also make entry in dealvoucher_api table
         */
     
         public function actionUpload(){
           $dataArray = array();
           if(isset($_POST["Submit"])){
            $data = $_POST;
            $filename = $_FILES['file1']['name'];
            $file = $_FILES["file1"]["tmp_name"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $created_datetime = date("Y-m-d H:i:s");
            $users = "";$user_name="";$created_datetime="";
    
            try{
                if($ext == 'csv'){                                   //Checking if the file type is .csv
                $handle = fopen($file, "r");                       //Open .csv file for reading
                $query = "INSERT INTO users (user_name,salt,dealer_name,created_datetime,modified_datetime,role,status) VALUES "; //Insert query for dealerCoupon table
                $i=0;
                while(!feof($handle)){
                  $coupon = fgetcsv($handle);                       //getting content from .csv file

                    if(!empty($coupon[0])){
                       $user_name= $coupon[0];
                       $users .= "'$user_name' ,";
                        if($i!=0){
                                $query.= ",";
                            }
                    $query.= "('$user_name','0','$user_name','$created_datetime','$created_datetime',3,1) ";
                     $i++;
                    }
                 } 
                }
            
             $query = trim($query,',');
                $connection = Yii::app()->db;
                if($connection->createCommand($query)->execute()){
                    $users =  trim($users,",");
                    $query2 = "SELECT id FROM `users` where user_name in ($users)";
                    $result2 = $connection->createCommand($query2)->queryAll();
                    $query3 = "INSERT INTO dealvoucher_api (dealer ,api ) VALUES ";
                    foreach($result2 as $row){
                        $dealer = $row['id'];
                        $query3.= "($dealer,'Giftxoxo') ,";
                    }
                    $query3 = trim($query3,',');  
                    if($connection->createCommand($query3)->execute()){
                        $dataArray['msg']  = "data uploaded successfully";
                    }
                }
           }
           catch(Exception $ex){
               var_dump($ex);
           }
           }
           
             $this->render("upload",$dataArray);
         }
         
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $profile = UserProfile::model()->findByAttributes(array('user_id'=>$id));
        if(!$profile){            
            $profile = new UserProfile;
        }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
        if(isset($_POST['Userrs']))
        {
                    
            if($_POST['Userrs']['role']==3){
//                if(strlen($_POST['Userrs']['mobile_number'])>10){
//                     $model->user_name = substr($_POST['Userrs']['mobile_number'],0,  strlen($_POST['Userrs']['mobile_number'])-1);
//                }
//                else{
//                     $model->user_name = $_POST['Userrs']['mobile_number'];
//                }
                $model->user_name = $_POST['Userrs']['mobile_number'];
            }
            else{
                $model->user_name = $_POST['Userrs']['mobile_number'];
            }
            $model->dealer_name = trim($_POST['Userrs']['dealer_name']);
            $model->password = $_POST['Userrs']['password'];
            $model->status = $_POST['Userrs']['status'];
            $model->mobile_number = $_POST['Userrs']['mobile_number'];
            $model->role = $_POST['Userrs']['role'];

            $model->modified_datetime = date("Y-m-d H:i:s");

            $model->password = strlen(trim($model->password))>0?md5($model->password):"";
                //if(isset($_POST['Userrs']['role']) && $_POST['Userrs']['role']==3){
                   // $model->dealer_name = $_POST['Userrs']['user_name'];
                //}
            if($model->save())
            $profile->attributes = $_POST['UserProfile'];
            $profile->user_id = $id;
            $profile->save();

            $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
                'model'=>$model,'pmodel'=>$profile,
        ));
 }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Userrs');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Userrs('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Userrs']))
			$model->attributes=$_GET['Userrs'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Userrs the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Userrs::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Userrs $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='userrs-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
