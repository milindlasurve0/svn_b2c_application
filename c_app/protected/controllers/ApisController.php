<?php

class ApisController extends Controller {

    var $userid;
    var $amount;
    var $connection;
    var $mobile_number;
    
    public function actionInfo() {
        $this->render('info');
        $this->mobile_number = isset($_REQUEST['mobile_number']) ? $_REQUEST['mobile_number'] : Yii::app()->getUsername;
        $userObj = Users::model()->findByAttributes(array('user_name' => $this->mobile_number));
        $walletObj = Wallets::model()->findByAttributes(array('users_id' => $userObj->getAttribute('id')));
    }

    protected function actionRecharge(){
                
        if(is_null($this->userid)){
            ApiResponse::unAuthorizedCall();
        }        
        $recharge_column = "";
        if(isset($_REQUEST['flag'])){ 
            if($_REQUEST['flag']==='1' ){
                $recharge_column = "mobile_number";
            }
            if($_REQUEST['flag']==='2' ){
                $recharge_column = "subscriber_id";
            }
        }
       
        $this->connection = Yii::app()->db;
        $transaction = $this->connection->beginTransaction();

        $this->mobile_number = isset($_REQUEST['mobile_number']) ? $_REQUEST['mobile_number'] : "";
        $this->amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : 0;
        $trans_category = $_REQUEST['actiontype'];
        $walletObj = Wallets::model()->findBySql("SELECT * FROM wallets WHERE users_id =" . $this->userid . " FOR UPDATE");
        
        if ($walletObj->getAttribute('account_balance') >= $this->amount) {            
            $new_bal = $walletObj->getAttribute('account_balance') - $this->amount;                  
            try {                
                
                $this->connection->createCommand("INSERT into transactions (opening_bal,transaction_amount,closing_bal,trans_category,users_id,trans_type,status) "
                    . "  VALUES('" . $walletObj->getAttribute('account_balance') . "','" . $this->amount . "','" . $new_bal . "','".$trans_category."','" . $this->userid . "',0,1)")->execute();
                
                $this->connection->createCommand("INSERT INTO recharge_transaction "
                . "(transaction_id,$recharge_column,amount,operator_id,recharge_type,device_type,type,profile_id,status,recharge_flag) "
                . "values('".$this->connection->lastInsertID."','".$_REQUEST[$recharge_column]."','" . $this->amount . "','".$_REQUEST['operator_id']."','"
                    . "".$_REQUEST['recharge_type']."','".$_REQUEST['device_type']."','".$_REQUEST['type']."','".$_REQUEST['profile_id']."','','".$_REQUEST['flag']."')")->execute();
                
                $this->connection->createCommand("UPDATE wallets set account_balance ='$new_bal' WHERE users_id='" . $this->userid . "'")->execute();
                
                
                $transaction->commit();
                ApiResponse::gen_response('success', '200', 'transaction successful');
            } catch (Exception $e) { // an exception is raised if a query fails
                $transaction->rollback();
                                
                $this->connection->createCommand("INSERT into transactions (opening_bal,transaction_amount,closing_bal,trans_category,users_id,trans_type,status) "
                    . "  VALUES('" . $walletObj->getAttribute('account_balance') . "','" . $this->amount . "',"
                    . "'" . $walletObj->getAttribute('account_balance') . "','".$trans_category."','" . $this->userid . "',0,3)")->execute();
        
                ApiResponse::gen_response('failed', '2025', 'recharge transfer transaction failed');
            }
        } else {
            ApiResponse::gen_response('failed', '2024', 'Insufficient balance for this transaction');
        }        
    }
    
    protected function actionMoneytransfer(){
        
        if(is_null($this->userid)){
            ApiResponse::unAuthorizedCall();
        }
        $this->connection = Yii::app()->db;
        //$userid = Yii::app()->user->getId();
        
        $transaction = $this->connection->beginTransaction();
                
        $beneficiary_column = "";
        $beneficiary_column_vals = "";
         
        $this->mobile_number = isset($_REQUEST['mobile_number']) ? $_REQUEST['mobile_number'] : "";
        $this->amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : 0;
        $trans_category = $_REQUEST['actiontype'];
        
        $bflag = 0;
        if(isset($_REQUEST['flag'])){ 
            if($_REQUEST['flag']==='1' ){
                $beneficiary_userObj = Users::model()->findBySql("SELECT * FROM users WHERE user_name = " . $this->mobile_number . " FOR UPDATE");
                
                $beneficiary_user_walletObj = Wallets::model()->findBySql("SELECT * FROM wallets WHERE users_id =" . $beneficiary_userObj->getAttribute('id') . " FOR UPDATE");
                $beneficiary_user_new_bal = $beneficiary_user_walletObj->getAttribute('account_balance') + $this->amount;
                $beneficiary_column = ",beneficiary_userid";
                $beneficiary_column_vals = ",'".$beneficiary_userObj->getAttribute('user_name')."'";
                $bflag = 1;
            }
            if($_REQUEST['flag']==='2' ){
                $beneficiary_column = ",beneficiary_account_no,bank_detail,response_detail";
                $bflag = 2;
            }
        }
       
        $walletObj = Wallets::model()->findBySql("SELECT * FROM wallets WHERE users_id =" . $this->userid . " FOR UPDATE");
        
        if ($walletObj->getAttribute('account_balance') >= $this->amount) {
            
            $new_bal = $walletObj->getAttribute('account_balance') - $this->amount;
                  
            try {                
                
                $this->connection->createCommand("INSERT into transactions (opening_bal,transaction_amount,closing_bal,trans_category,users_id,trans_type,status) "
                    . "  VALUES('" . $walletObj->getAttribute('account_balance') . "','" . $this->amount . "','" . $new_bal . "',"
                    . "'".$trans_category."','" . $this->userid . "',0,1)")->execute();
                
                if($bflag === 1){
                    $this->connection->createCommand("UPDATE wallets set account_balance ='".$beneficiary_user_new_bal."' "
                            . "WHERE users_id='" . $beneficiary_userObj->getAttribute('id') . "'")->execute();                
                }
                
                if($bflag === 2){
                    // *** need to implement online recharge
                    //
                    $response_detail = "ok";
                    $beneficiary_column_vals = ",'".$_REQUEST['account_number']."','".$_REQUEST['bank_detail']."','".$response_detail."'";
                }
                
                $this->connection->createCommand("INSERT INTO transfer_transaction "
                    . "(transaction_id".$beneficiary_column.") "
                    . "values('".$this->connection->lastInsertID."'".$beneficiary_column_vals.")")->execute();
                
                
                $this->connection->createCommand("UPDATE wallets set account_balance ='$new_bal' WHERE users_id='" . $this->userid . "'")->execute();                
                
                $transaction->commit();
                ApiResponse::gen_response('success', '200', 'transaction successful');
            } catch (Exception $e) { // an exception is raised if a query fails
                $transaction->rollback();
                                
                $this->connection->createCommand("INSERT into transactions (opening_bal,transaction_amount,closing_bal,trans_category,users_id,trans_type,status) "
                    . "  VALUES('" . $walletObj->getAttribute('account_balance') . "','" . $this->amount . "','" . $walletObj->getAttribute('account_balance') . "',"
                    . "'".$trans_category."','" . $this->userid . "',0,3)")->execute();
                
                ApiResponse::gen_response('success', '2026', 'wallet transfer transaction failed');
                /*echo "<pre>";
                print_r($e->getTraceAsString());
                echo "</pre>";
                print "transaction not successful";
                 */
            }
        } else {
            ApiResponse::gen_response('failed', '2024', 'Insufficient balance for this transaction');
        }        
    }
    
    protected function actionEntertainment(){
        print_r($_REQUEST);  
        exit();
    }
    
    protected function actionDelegate_recharge(){
        $origincode = $_REQUEST['origincode'];
        $msisdn = $_REQUEST['msisdn'];
        
        $delegateObj = DelegateRecharge::model()->findBySql("SELECT * FROM `delegate_recharge` AS dr, global_missed_numbers AS gmn "
                . "WHERE dr.`global_missed_numbers_id`=gmn.id AND dr.`mobile_number`='".$msisdn."' "
                . "AND gmn.missed_number='".$origincode."' AND gmn.status='0' AND gmn.deleted='0'");
              
       echo "<pre>"; 
       $addition_detail = array('device_type'=>'missed_call','type'=>'flexi','profile_id'=>'');
       $_REQUEST['flag'] = ($delegateObj->getAttribute('recharge_flag'));
       $_REQUEST = array_merge($_REQUEST,$delegateObj->getAttributes(),$addition_detail);
       $_REQUEST['amount']= $_REQUEST['recharge_amount'];
       unset($_REQUEST['id'],$_REQUEST['status'],$_REQUEST['recharge_amount']);
       
       $this->userid = $_REQUEST['users_id'];
       //print_r($_REQUEST);exit();
       $this->actionRecharge();
       
    }
    
    protected function pay4app(){
        
        
    }
    
    protected function getapplist(){
        
        
    }
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
    
  
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Wallets'])) {
            $model->attributes = $_POST['Wallets'];
            if ($model->save())
            //$this->redirect(array('view', 'id' => $model->id));
                return true;
        }
        return false;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Users the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Wallets::model()->findByAttributes(array('users_id' => $id));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    public function actionAction(){        
        //$clienip = Yii::app()->request->getUserHostAddress();
        //if(is_null(Yii::app()->user->getId()) || !in_array($clienip,$valid_iplist)){
        $bypassAction = array('delegate_recharge','get_operator');
        if($_REQUEST['api']==='true' && !in_array($_REQUEST['actiontype'],$bypassAction)){
            $this->actionApiauth();
        }else if(is_null(Yii::app()->user->getId()) && !in_array($_REQUEST['actiontype'],$bypassAction)){
            ApiResponse::unAuthorizedCall();
        }
        
        $this->userid = Yii::app()->user->getId();
        $action = "action".ucfirst(trim($_REQUEST['actiontype']));
        $this->$action();
        //Yii::app()->user->logout();
    }
    
    public function actionApiauth(){        
        //echo "<pre>";
        $username = isset($_REQUEST['username'])?$_REQUEST['username']:ApiResponse::gen_response('failed', '1106', 'Missing username');
        $signature = isset($_REQUEST['signature'])?$_REQUEST['signature']:ApiResponse::gen_response('failed', '1112', 'Missing signature');
        
        $users = Users::model()->findByAttributes(array('user_name' => $username));
        if ($users === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }else{
            $nonce = isset($_REQUEST['time'])?$_REQUEST['time']:ApiResponse::gen_response('failed', '1117', 'Missing timestamp');
            if(!preg_match('/^[0-9]+$/', $nonce)){
                ApiResponse::gen_response('failed', '1118', 'Invalid timestamp');
            }
            if($nonce > (time()+300) || $nonce < (time()-300)){
                ApiResponse::gen_response('failed', '1114', 'timestamp out of range');
            }
            $salt = $users->getAttribute('salt');
            $v_signature = base64_encode(sha1($username.$nonce,$salt));
            if($v_signature === $signature){
                $auth = new UserIdentity($username,$users->getAttribute('password'));
                if(!$auth->authenticate()){
                    ApiResponse::gen_response('failed', '1116', 'Invalid password');
                }
                Yii::app()->user->login($auth,0);
                return true;
            }else{
                ApiResponse::gen_response('failed', '1113', 'Invalid Signature');
            }
        }
    }

    
    public function actionGet_operator(){
        $number = $_REQUEST['mobile'];
        echo json_encode(TelecomComponent::getOpbynumber($number));
        //return;
    }
}
