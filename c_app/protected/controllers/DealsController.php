<?php
class DealsController extends Controller {
   
        /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2_2';
    private $actionData = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $this->layout = (UsersComponent::isAdmin()) ? '//layouts/column2_2' : '//layouts/column2_3';
        //$this->layout = (UsersComponent::isAdmin()) ? '//layouts/column1_1' : '//layouts/column2_2';
        if ( !( UsersComponent::isDealer() || UsersComponent::isAdmin() )){
            $this->redirect(array('panel/index'));exit;
        }
          
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array( 'create','test','index','preview','announcer','get_Offers_By_DealsId','show_stockDeal','downloadPdf','addDealType','addCoupons'),
                'users' => array('*'),
            )/*,
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('wallet','recharge','services','index','billpayment','complain'),
                'users' => array('@'),
                'deniedCallback'=>ApiResponse::unAuthorizedCall()
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => UsersComponent::getAdminusers(),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),*/
        );
    }

    public function actionAddImage(){
        $data = $_REQUEST;
        if($_FILES['image_file']['size'] > 0  && $_FILES['image_file']['name'] != "" && $_FILES['image_file']['size']< 51200){ //&& $_FILES['image_file']['size']< 51200
            $new_file_name="";
            $size = number_format((float)$_FILES['image_file']['size'] /1024,2,'.','');
            $tmp = explode(".", $_FILES["image_file"]["name"]);
            $ext = end($tmp);
        
            $ID = $data['image_deal_id'];
            
//          $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR']. $new_file_name . uniqid().".".$ext;
            $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'Shop_'.$ID.uniqid('_IMG_').".".$ext;
            //Yii::app()->params['GLOBALS']['_IMG_DIR'].'Deal_'.$ID.$new_file_name . uniqid('_IMG_').".".$ext
            
            $gallery_data = array('deal_id'=>$data['image_deal_id'],'image_url'=>$new_file_name,'image_info'=>$data['image_desc'],
                    'uploaded_date'=>  GeneralComponent::getFormatedDate());
            if(DealsComponent::add_in_image_gallery($gallery_data)){
//                $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['image_deal_id']));
//                $offer->updated = GeneralComponent::getFormatedDate();
//                $offer->save();
                if(move_uploaded_file($_FILES["image_file"]["tmp_name"], $new_file_name)){
                    $data['img_url'] = $new_file_name;
                }
            }
            $this->redirect("editDetails/".$data['image_deal_id']);
        }
        else{
            $this->redirect("editDetails/".$data['image_deal_id']);
        }
    }

    public function actionAddLogoImage(){
        $data = $_REQUEST;
        if($_FILES['logo_image_file']['size'] > 0 && $_FILES['logo_image_file']['size']< 51200 && $_FILES['logo_image_file']['name'] != "" ){
            $new_file_name="";
            $size = number_format((float)$_FILES['logo_image_file']['size'] /1024,2,'.','');
            $tmp = explode(".", $_FILES["logo_image_file"]["name"]);
            $ext = end($tmp);
        
            $ID = $data['logo_image_deal_id'];
            $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'Logo_'.$ID.".".$ext;
            $gallery_data = array('deal_id'=>$data['logo_image_deal_id'],'image_url'=>$new_file_name,'image_info'=>$data['logo_image_desc'],
                    'uploaded_date'=>  GeneralComponent::getFormatedDate());

            if(DealsComponent::add_in_image_section($gallery_data)){    
//                $offer = Offers::model()->findByAttributes(array('deal_id'=>$data['logo_image_deal_id']));
//                $offer->updated = GeneralComponent::getFormatedDate();
//                $offer->save();
                if(move_uploaded_file($_FILES["logo_image_file"]["tmp_name"], $new_file_name)){
                    $data['img_url'] = $new_file_name;
                }
            }
             $this->redirect("editDetails/".$data['logo_image_deal_id']);
        }
        else{
            $this->redirect("editDetails/".$data['logo_image_deal_id']);
        }
    }
    /*
     * This function is used to Add Temporary Test Image for croping and later used by add_image_test  
     * @return : redirect back to  editDetails page
    */
    public function actionAddTestImage(){
        $data = $_REQUEST;
        if($_FILES['test_image_file']['size'] > 0 && $_FILES['test_image_file']['size']< 51200 && $_FILES['test_image_file']['name'] != "" ){
            $new_file_name="";
            $size = number_format((float)$_FILES['test_image_file']['size'] /1024,2,'.','');
            $tmp = explode(".", $_FILES["test_image_file"]["name"]);
            $ext = end($tmp);
        
            $ID = $data['test_image_deal_id'];
            
            $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'Deal_'.$ID.$new_file_name . uniqid('_TEMP_IMG_').".".$ext;
            
            $gallery_data = array('deal_id'=>$data['test_image_deal_id'],'image_url'=>$new_file_name,'image_info'=>$data['test_image_desc'],
                         'uploaded_date'=>  GeneralComponent::getFormatedDate());
            
            if(DealsComponent::add_in_image_test($gallery_data)){           
                if(move_uploaded_file($_FILES["test_image_file"]["tmp_name"], $new_file_name)){
                    $data['img_url'] = $new_file_name;
                }
            }
              $this->redirect("cropImage/".$data['test_image_deal_id']);
        }
        else{
            $this->redirect("cropImage/".$data['test_image_deal_id']);
        }
    }
    /*End of Add Test image function used for Temporary Image used for croping */   
    
    
    /*
     * This function is used as action  to Remove Cropped Image using remove_from_image_test function from deal_image_test Table 
     * @return : redirect back to  editDetails page
    */
    public function actionAddCropImage(){
        $data = $_REQUEST;
    //the minimum of xlength and ylength to crop.
        $ini_x_size = $data['x'];
        $ini_y_size = $data['y'];
    //the minimum of width and height to crop.            
        $w  = $data['w'];
        $h =  $data['h'];      
    
        $org_name = $data['orginal_image_url'];
        $info = getimagesize($org_name);
            $mime = $info['mime'];
            switch ($mime) {
                case 'image/jpeg':
                        $image_create_func = 'imagecreatefromjpeg';
                        break;

                case 'image/png':
                        $image_create_func = 'imagecreatefrompng';
                        break;

                case 'image/gif':
                        $image_create_func = 'imagecreatefromgif';
                        break;

                default: 
                        throw Exception('Unknown image type.');
            }

        $myImage = $image_create_func($org_name);
        list($width, $height) = getimagesize($org_name);
        $scale = 0.5;
        
        //Create the zoom_out and cropped image
        $myImageZoom=imagecreatetruecolor($width*$scale,$height*$scale);
        $b=imagecopyresampled($myImageZoom,$myImage,0,0,0,0,$width*$scale,$height*$scale,$width,$height);
        
        $myImageCrop =  imagecreatetruecolor($w,$h);
        $b=imagecopyresampled($myImageCrop,$myImage,0,0,$ini_x_size,$ini_y_size,$w,$h,$w,$h);	

        
        $new_file_name="";
        $tmp = explode(".", $org_name);
        $ext = end($tmp);
        $ID = $data['crop_image_deal_id'];
        $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'L_Deal_'.$ID.".".$ext;
    
    // Save the crop images created
        imagejpeg( $myImageCrop,$new_file_name);
        
        $gallery_data = array('deal_id'=>$data['crop_image_deal_id'],'L_img_url'=>$new_file_name,'image_info'=>$data['orginal_image_desc'],
                    'uploaded_date'=>  GeneralComponent::getFormatedDate());
        
        //  Remove Original Image using remove_from_image_test from deal_image_test Table          
        DealsComponent::remove_from_image_test($data['crop_image_id'],$data['crop_image_deal_id']);
        if(DealsComponent::add_crop_in_deal($gallery_data)){
            $data['orginal_image_url'] = $new_file_name;
        }
        $this->redirect("editDetails/".$data['crop_image_deal_id']);
    }
    /*End of Add crop image function */   
    
    public function actionRemove_image($id=null,$deal_id = null){
        DealsComponent::remove_from_image_gallery($id,$deal_id);
        $this->redirect("/deals/editDetails/".$deal_id);
    }
    
    
    /*
     * This function is used as action  to Remove Logo Image using remove_logo_image function from deal_image_section Table 
     * @return : redirect back to  editDetails page
    */
    public function actionRemove_logo_image($id=null,$deal_id = null){
        DealsComponent::remove_from_image_section($id,$deal_id);
        $this->redirect("/deals/editDetails/".$deal_id);
    }
    /*End of remove logo image function */
    
    /*
     * This function is used as action  to Remove Cropped Image using remove_from_image_test function from deal_image_test Table 
     * @return : redirect back to  editDetails page
    */
    public function actionRemove_crop_image($id=null,$deal_id = null){
        DealsComponent::remove_from_image_test($id,$deal_id);
        $this->redirect("/deals/editDetails/".$deal_id);
    }
    /*End of remove crop image function */
    
    public function actionIndex(){                
            $this->actionList();     
    }
    
    public function actionList($dealerId=null){     
        if( UsersComponent::isAdmin() ){
            $dealerId = empty($dealerId) ? "" : $dealerId ;
        }else if(UsersComponent::isDealer()){
            $dealerId = Yii::app()->user->id ;
        }
        
        $this->actionData['dealerId'] = $dealerId;
        $dealsList = DealsComponent::getDealsList2($dealerId,'','Open');
        $categoriesList = DealsComponent::getDealCategoriesList();
        $dealersList = MerchantComponent::getAllDealer(Yii::app()->params['GLOBALS']['_ROLE_DEALER']);
        $this->actionData['dealStatus'] = Yii::app()->params['GLOBALS']['_DEAL_STATUS'];
        $this->actionData['dealsList'] = $dealsList;
        $this->actionData['dealsTypes'] = DealsComponent::getDealTypes();
        $this->actionData['categoriesList'] = $categoriesList;
        $this->actionData['dealersList'] = $dealersList;
        $this->render('list',$this->actionData);
    }
    
    public function actionCreate(){        
        $data = array();
        if($_FILES['main_image_url']['size'] > 0 && $_FILES['main_image_url']['name'] != "" && $_FILES['main_image_url']['size']< 51200){
            $new_file_name="";
            $size = number_format((float)$_FILES['main_image_url']['size'] /1024,2,'.','');
            $tmp = explode(".", $_FILES["main_image_url"]["name"]);
            $ext = end($tmp);
            $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR']. $new_file_name . uniqid().".".$ext;
            if(move_uploaded_file($_FILES["main_image_url"]["tmp_name"], $new_file_name)){
                $data['img_url'] = $new_file_name;
            }
        }
        if(!empty($_POST["name"])){
         $data["name"] = $_POST["name"];       
        // $data["by_voucher"] = 0;
        }
        $data["category_id"] = $_POST["category_id"];
        $data["dealer_id"] = $_POST["dealer_id"];
        $data["status"] = $_POST["status"];
        $result = DealsComponent::createDeal($data);
        if($result['status']=='success'){
            Yii::app()->user->setFlash('success', "Deal created !");
        }else{
            Yii::app()->user->setFlash('error', "Deal creation failed !");
        }
        $this->redirect("list");     
    }
    
    public function actionEdit(){
       $data = $_REQUEST;
       $date = GeneralComponent::getFormatedDate();
       $model = Deal::model()->findByPk($_POST["id"]);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        
//        if($_FILES['main_image_url']['size'] > 51200){
//             Yii::app()->user->setFlash('error', "Too big image to upload.Please upload image having size less than 30kb.");
//             $this->redirect("editDetails/".$_POST["id"]);
//        }
         $data['img_url'] = '';
         $targetFile = '';
        if(isset($_FILES['main_image_url']) && $_FILES['main_image_url']['size'] > 0 && $_FILES['main_image_url']['name'] != "" && $_FILES['main_image_url']['size']< 51200){
            //update updated Column in offer 
            $new_file_name="";
            $size = number_format((float)$_FILES['main_image_url']['size'] /1024,2,'.','');
            $tmp = explode(".", $_FILES["main_image_url"]["name"]);
            $ext = end($tmp);
           // $uni_ID = $new_file_name . uniqid().".".$ext;//For unique ID
            
        /*For Small Image Name*/
            $large_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'L_'.'Deal_'.$data['id'].".".$ext;  //Yii::app()->params['GLOBALS']['_IMG_DIR'].'L_'.'Deal_'.$data['id'].$uni_ID
        /*For Original Image Name*/
            $new_file_name = Yii::app()->params['GLOBALS']['_IMG_DIR'].'Deal_'.$data['id'].".".$ext;//Yii::app()->params['GLOBALS']['_IMG_DIR'].'Deal_'.$data['id'].'_'.$uni_ID
           if(move_uploaded_file($_FILES["main_image_url"]["tmp_name"], $large_file_name)){
                $data['L_img_url'] = $large_file_name;
            }
            
        /*Start Resizing the Original Image Code*/    
            $url = $data['L_img_url'];
            $originalFile = Yii::app()->params['GLOBALS']['LIVE_BASE_URL'].$url;
            $newWidth = Yii::app()->params['GLOBALS']['NEW_WIDTH'];
            $targetFile =  $new_file_name ;
            $info = getimagesize($originalFile);
            $mime = $info['mime'];
            switch ($mime) {
                case 'image/jpeg':
                        $image_create_func = 'imagecreatefromjpeg';
                        $image_save_func = 'imagejpeg';
                        $new_image_ext = 'jpg';
                        break;

                case 'image/png':
                        $image_create_func = 'imagecreatefrompng';
                        $image_save_func = 'imagepng';
                        $new_image_ext = 'png';
                        break;

                case 'image/gif':
                        $image_create_func = 'imagecreatefromgif';
                        $image_save_func = 'imagegif';
                        $new_image_ext = 'gif';
                        break;

                default: 
                        throw Exception('Unknown image type.');
            }

            $img = $image_create_func($originalFile);
            list($width, $height) = getimagesize($originalFile);
            /* find the “desired height” of this thumbnail, relative to the desired width  */
            $newHeight = floor(($height / $width) * $newWidth);
            /* create a new, “virtual” image */
            $tmp = imagecreatetruecolor($newWidth, $newHeight);
             /* copy source image at a resized size */
            $t = imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
            if (file_exists($targetFile)) {
                    unlink($targetFile);
            }
             /* create the physical thumbnail image to its destination */
            $t1 = imagejpeg($tmp, $targetFile,75);
            $tt = $image_save_func($tmp, "$targetFile");
            list($width, $height, $type, $attr) = getimagesize($targetFile);
            /*End Resizing the Original Image Code*/
            $model->img_url = $targetFile;
            $model->L_img_url = $data['L_img_url'];
        }elseif(isset($_FILES['main_image_url']) && $_FILES['main_image_url']['size']> 51200){
            Yii::app()->user->setFlash('error', "Please upload image with size less han 50 kb.!");
        }
        
        $data['by_voucher']=0;
        $query = "SELECT dealer from dealvoucher_api as d LEFT JOIN users as u ON (u.id=d.dealer) WHERE u.role=3 AND d.dealer= ".$_POST["dealer_id"];
        $dealerForVoucher = Yii::app()->db->createCommand($query)->queryRow();
        if(isset($dealerForVoucher['dealer']) && !empty($dealerForVoucher['dealer'])){
            $data['by_voucher']=1;
        }
        $model->by_voucher = $data['by_voucher'];
        $model->name = $_POST["name"];
        $model->type_id = $_POST["type_id"];
        $model->category_id = $_POST["category_id"];
        $model->dealer_id = $_POST["dealer_id"];
        $model->status  = $_POST["status"];
        $model->coupon_type = $_POST["coupon_type"];
        $model->coupon_prefix = $_POST["coupon_prefix"];
        $model->company_url = $_POST["company_url_name"];
        $model->deal_url = $_POST["deal_url"];
        
        if(isset($_POST["reason"])){
            $model->reason = $_POST["reason"];
        }
        if($model->save()){
            if(isset($_POST["Emails"]) && !empty($_POST["Emails"])){
                $status='';
                $deal_reason_list = DealsComponent::get_reason_list();
                $reson = (isset($_POST["reason"]) && !empty($_POST["reason"])) ? $deal_reason_list[$_POST["reason"]-1]['reason'] : "";
                $sender = "support@pay1.in";
                $to = $_POST["Emails"];
                if($_POST["status"]==1){
                    //added
                    $status = " Open ";
                    $subject="Freebie/Offer Active";
                   $message = $_POST["dealer_name"]." offering ".$_POST["name"]." is active now.";
                }
                else if($_POST["status"]==2){
                    //closed
                    $subject="Freebie/Offer Closed";
                    $status = " Close ";
                    $message = $_POST["dealer_name"]." offering ".$_POST["name"]." has been closed due to $reson on $date.";
                }
                if(!empty($status)){
                    GeneralComponent::sendMail($to, $subject, $message,$sender);
                }
                
                
            }
            $offer = Offers::model()->findByAttributes(array('deal_id'=>$_POST["id"]));
             if(!empty($offer)){
                $offer->updated = $date;
                $offer->save();
             }
            Yii::app()->user->setFlash('success', "Deal updated !");
        }else{
            Yii::app()->user->setFlash('error', "Deal updation failed !");
        }
        $this->redirect("editDetails/".$_POST["id"]);
    }    
    
    public function actionEditDetails($id){         
        $this->actionData['id'] = $id;
        $model = Deal::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        $this->actionData['model'] = $model;
        
        $couponTypeList = DealsComponent::getDealCouponTypesList();  //getting system generated coupon type which value = 0 
        $couponType     = DealsComponent::getDealCouponTypes($id);  //getting system generated coupon type which value = 0 
        $giftVoucherDeal= DealsComponent::getDealGiftVoucher($id);
        $typesList      = DealsComponent::getDealTypesList();
        $categoriesList = DealsComponent::getDealCategoriesList();
        $offersList     = DealsComponent::getDealOffersList($id);
        $offerInfoList  = DealsComponent::getDealOffersInfoList($id);// getting Offer Info from offer_details Table
        $locationList   = DealsComponent::getDealLocationsList($id);
        $mapList        = DealsComponent::getDealMapList($id);
        $dealLocationMapList = DealsComponent::getDealLocationMapList($id);
        $infosList      = DealsComponent::getDealInfosList($id);
        $dealersList    = MerchantComponent::getAllDealer(Yii::app()->params['GLOBALS']['_ROLE_DEALER']);
        $deal_image_list= DealsComponent::get_gallery_list($id);
        $deal_logo_list = DealsComponent::get_logo_list($id);// getting deatils of logo image from DealsImageSection
        $deal_test_list = DealsComponent::get_test_list($id);// getting deatils of test image from DealsImageTest
        $deal_reason_list = DealsComponent::get_reason_list();
        $statesList     = GeneralComponent::getStatesList($id);
        $this->actionData['dealStatus']         = Yii::app()->params['GLOBALS']['_DEAL_STATUS'];
        $this->actionData['offerValidityModes'] = Yii::app()->params['GLOBALS']['_OFFER_VALIDITY_MODES'];
        $this->actionData['offerStatus']        = Yii::app()->params['GLOBALS']['_DEAL_OFFER_STATUS'];
        $this->actionData['locationStatus']     = Yii::app()->params['GLOBALS']['_DEAL_LOCATION_STATUS'];
        $this->actionData['infoStatus']         = Yii::app()->params['GLOBALS']['_DEAL_INFO_STATUS'];
        $this->actionData['couponTypeList']    = $couponTypeList; //getting system generated coupon type using getDealCouponTypes()        
        $this->actionData['couponType']         = $couponType; //getting system generated coupon type using getDealCouponTypes()
        $this->actionData['giftVoucherDeal']    = $giftVoucherDeal;
        $this->actionData['typesList']          = $typesList;
        $this->actionData['categoriesList']     = $categoriesList;
        $this->actionData['offersList']         = $offersList;
        $this->actionData['offerInfoList']      = $offerInfoList; // getting Offer Info using getDealOffersInfoList()
        $this->actionData['locationList']       = $locationList;
        $this->actionData['mapList']            = $mapList ;
        $this->actionData['dealLocationMapList'] = $dealLocationMapList;        
        $this->actionData['infosList']          = $infosList;
        $this->actionData['reasons']            = $deal_reason_list;
        $this->actionData['deal_image_list']    = $deal_image_list;
        $this->actionData['deal_logo_list']     = $deal_logo_list;// getting deatils of logo image from DealsImageSection
        $this->actionData['deal_test_list']     = $deal_test_list;// getting deatils of tmp image from DealsImageTest
        $this->actionData['dealersList']        = $dealersList;
        $this->actionData['statesList']         = $statesList;//getting state List array using getStatesList()
        //updated Column in offer
        
        $this->render('editDetails',$this->actionData);
    }
    
    public function actionCropImage($id){         
        $this->actionData['id'] = $id;
        $model = Deal::model()->findByPk($id);
        if($model===null)
			throw new CHttpException(404,'The requested Crop Image page does not exist.');
        $this->actionData['model'] = $model;
        $deal_test_list = DealsComponent::get_test_list($id);// getting deatils of test image from DealsImageTest
        $this->actionData['deal_test_list'] = $deal_test_list;// getting deatils of logo image from DealsImageTest
        //updated Column in offer
        // $offer = Offers::model()->findByAttributes(array('deal_id'=>$id));
//        if(!empty($offer)){
//           $offer->updated = date("Y-m-d H:i:s");
//           $offer->save();
//        }
        $this->render('cropImage',$this->actionData);
    }
    
    public function actionCreateOffer(){        
        /*
        [offer_deal_id] => 6
        [offer_name] => name
        [offer_offer_price] => O P
        [offer_actual_price] => A P
        [offer_total_stock] => T S
        [offer_validity] => Val
        [offer_validity_mode] => Val M
        [offer_status] => S
        [offer_category] => Cat
        [offer_short_desc] => S Desc
        [offer_long_desc] => L DESC
         */
        $data = $_POST;
        $result = DealsComponent::createOffer($data);
        $this->redirect("editDetails/".$data["offer_deal_id"]."#offer"); 
        //$this->render('services');
    }
    
    /*
     * This function is used to create Offer Info using createOfferInfo()for the Offer Deatils table 
     * @return : display result on editDeatils (based on method)
     */
    public function actionCreateOfferInfo(){        
        $data = $_POST;
        $result = DealsComponent::createOfferInfo($data);
        $this->redirect("editDetails/".$data["offer_info_deal_id"]."#offer_info"); 
    }
    /*End of Create Offer Info*/
    
    public function actionCreateInfo(){        
        $data = $_POST;
        $result = DealsComponent::createInfo($data);
        $this->redirect("editDetails/".$data["info_deal_id"]."#deals"); 
        //$this->render('services');
    }
    
    public function actionCreateLocation(){        
        $data = $_POST;        
        $addDetails = GeneralComponent::getLatLongByArea($data['location_address'].(empty($data['location_city_name'])? "" : " , ".$data['location_city_name']));
        if(isset($data['location_lat']) && empty($data['location_lat'])){
            $data['location_lat'] = isset($addDetails['lat']) ? $addDetails['lat'] : 0 ;
        }
        if(isset($data['location_lng']) && empty($data['location_lng'])){
            $data['location_lng'] = isset($addDetails['lng']) ? $addDetails['lng'] : 0 ;
        }
        $result = DealsComponent::createLocation($data);
        
        if($result["status"] == 'failure' ){
            Yii::app()->user->setFlash('error', $result["description"]/*"Offer update failed !"*/);
        }
        $this->redirect("editDetails/".$data["location_deal_id"]."#locations"); 
        //$this->render('services');
    }
    
    /*  Start of SetMap
     * This function is used for ADDING NEW LOCATION by using DealsComponent functions
     * @redirect : Display on SUCCESS or 'error' description on failure 
     */
    public function actionSetMap(){        
        $data = $_POST;
        if(!empty($data['map_deal_location_id'])){
           $deal_location_data = DealsComponent::getDealLocationMapList($data['map_deal_location_id']);
           $result = DealsComponent::updateDealLocationMapList($data,$deal_location_data);
        }else{
            $result = DealsComponent::createMapLocation($data);
        }
        if($result["status"] == 'failure' ){
            Yii::app()->user->setFlash('error', $result["description"]/*"Map update failed !"*/);
        }
        $this->redirect("editDetails/".$data["map_deal_id"]."#SetMap"); 
    }
    /*End of SetMap*/
 
    public function actionEditLocation(){        
        $data = $_POST;  
        $addDetails = GeneralComponent::getLatLongByArea($data['edit_location_address'].(empty($data['edit_location_city_name'])? "" : " , ".$data['edit_location_city_name']));
        if(isset($data['edit_location_lat']) && empty($data['edit_location_lat'])){
            $data['edit_location_lat'] = $addDetails['lat'] ;
        }
        if(isset($data['edit_location_lng']) && empty($data['edit_location_lng'])){
            $data['edit_location_lng'] = $addDetails['lng'];
        }
        $result = DealsComponent::editLocation($data);    
        
        if($result["status"] == 'failure' ){
            Yii::app()->user->setFlash('error', $result["description"]/*"Offer update failed !"*/);
        }
        $this->redirect("editDetails/".$data["edit_location_deal_id"]."#locations"); 
        //$this->render('services');
    }
    
//    public function actionEditMap(){        
//        $data = $_POST;
//        $addDetails = GeneralComponent::getLatLongByArea($data['edit_location_address'].(empty($data['edit_location_city_name'])? "" : " , ".$data['edit_location_city_name']));
//        if(isset($data['edit_location_lat']) && empty($data['edit_location_lat'])){
//            $data['edit_location_lat'] = $addDetails['lat'] ;
//        }
//        if(isset($data['edit_location_lng']) && empty($data['edit_location_lng'])){
//            $data['edit_location_lng'] = $addDetails['lng'];
//        }
//        $result = DealsComponent::editMap($data);    
//        
//        if($result["status"] == 'failure' ){
//            Yii::app()->user->setFlash('error', $result["description"]/*"Offer update failed !"*/);
//        }
//        $this->redirect("editDetails/".$data["edit_map_deal_id"]."#map"); 
//        //$this->render('services');
//    }
    
    public function actionEditOffer(){        
        $data = $_POST;
        $result = DealsComponent::editOffer($data);
        if($result["status"] == 'failure' ){
            Yii::app()->user->setFlash('error', $result["description"]/*"Offer update failed !"*/);
        }
        $this->redirect("editDetails/".$data["offer_deal_id"]."#offer"); 
        //$this->render('services');
    }
    
    /*
     * This function is used to edit Offer Info  Details in offer_details table 
     * @return : success or error description (based on method)
    */
    public function actionEditOfferInfo(){        
        $data = $_POST;
        $result = DealsComponent::editOfferInfo($data);
        if($result["status"] == 'failure' ){
            Yii::app()->user->setFlash('error', $result["description"]/*"Offer Info update failed !"*/);
        }
        $this->redirect("editDetails/".$data["offer_info_deal_id"]."#offer"); 
    }
    /*End of Edit Offer Info */    
    public function actionEditInfo(){        
        $data = $_POST;
        $result = DealsComponent::editInfo($data);
        if($result["status"] == 'failure' ){
            Yii::app()->user->setFlash('error', $result["description"]/*"Offer update failed !"*/);
        }
        $this->redirect("editDetails/".$data["edit_info_deal_id"]."#deals"); 
        //$this->render('services');
    }    
    
    public function actionadmin(){        
        //echo "Panel Recharge page";
        $this->render('admin');
    }
    
    public function actionBillpayment(){
        $this->render('billpayment');
    }
    
    public function actionTest(){
        $u = new UserProfile();
        $u->name='abc123';
        if($u->save()){
            echo "new profile";
        }else{
            print_r($u->errors);
        }
        exit();
        $this->render('test');
    }    
    //show deals preview in deals controller
    public function actionPreview($id){
        $this->layout = "//layouts/blank" ;
        $this->actionData['dealsList'] = DealsComponent::getPreview($id);
        $this->actionData['context'] = DealsComponent::getDealContext($id);
        $this->actionData['gallery'] = DealsComponent::get_gallery_list($id);//Getting gallery image by given deal_ID
        $this->actionData['offerDetails'] = DealsComponent::getOfferdetails($id);
        $this->render('preview',$this->actionData);
    }
    
    public function actionAnnouncer() {
        $users_id =Yii::app()->user->id;
        //$model=new FreeGift_announcer;
        $responce = '';
        $dataArray = array();
        $dataArray['message'] = '';
        $data = $_REQUEST;        
        $dataArray['deals'] = DealsComponent::getAllDeals();
        if(isset($_REQUEST) && (!empty($_REQUEST))){
            $responce = DealsComponent::getUsersforannoucement($data);
            if($responce){
                $dataArray['message'] = $responce;
            }
            else{
                $dataArray['message'] = 'Something goes wrong while sending announcement';
            }
        }
        //$dataArray['success'] =$responce;
        $this->render("FreeGift_announcer", $dataArray);
     }
     
     public function Get_Offers_By_DealsId(){
         $deals_id = $_REQUEST['deals_id'];
         return json_encode(DealsComponent::getDealOffersList($deals_id));
     }
     
     public function actionShow_stockDeal(){
         $type = $_REQUEST['type'];
         if($type=='open'){
             $showCond = 'Open';
         }
         else if($type =='stockout'){
             $showCond = 'Expire';
         }
         if( UsersComponent::isAdmin() ){
            $dealerId = empty($dealerId) ? "" : $dealerId ;
        }else if(UsersComponent::isDealer()){
            $dealerId = Yii::app()->user->id ;
        }
        
        $this->actionData['dealerId'] = $dealerId;
        $dealsList = DealsComponent::getDealsList2($dealerId,'',$showCond);
        $categoriesList = DealsComponent::getDealCategoriesList();
        $dealersList = MerchantComponent::getAllDealer(Yii::app()->params['GLOBALS']['_ROLE_DEALER']);
        $this->actionData['dealStatus'] = Yii::app()->params['GLOBALS']['_DEAL_STATUS'];
        $this->actionData['dealsList'] = $dealsList;
        $this->actionData['categoriesList'] = $categoriesList;
        $this->actionData['dealersList'] = $dealersList;
        $this->actionData['dealsTypes'] = DealsComponent::getDealTypesList();
        $this->render('list',$this->actionData);
     }
     public static function actionAddDealType(){
         $data = $_REQUEST;
         $result = DealsComponent::updateDealType($data);
         echo $result;
     }

     // ob_start('My_OB');
     public function actionDownloadPdf()
    {   
        $id=$_REQUEST['id'];
        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');
        $this->layout = "//layouts/blank" ;
        $this->actionData['dealsList'] = DealsComponent::getPreview($id);
        $this->actionData['context'] = DealsComponent::getDealContext($id);
        $this->actionData['gallery'] = DealsComponent::get_gallery_list($id);//Getting gallery image by given deal_ID
        $this->actionData['offerDetails'] = DealsComponent::getOfferdetails($id);
       
        # render (full page)
        $mPDF1->WriteHTML($this->render('preview',$this->actionData,true));
        # Load a stylesheet
        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        //$mPDF1->WriteHTML($stylesheet, 1);
 
        # renderPartial (only 'view' of current controller)
        //$mPDF1->WriteHTML($this->renderPartial('index', array(), true));
 
        # Renders image
        //$mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));
 
        # Outputs ready PDF
        //Para D to save file
        $file = $mPDF1->Output('deal-preview.pdf','D');
        exit;
       //$file = $mPDF1->Output('/file.pdf', 'F');

    // It will be called downloaded.pdf
   // header("Content-Disposition:attachment;filename='download.pdf'");

    // The PDF source is in original.pdf
    //readfile($file);
       // exit;
    }
    /**
	 * Create actionAddCoupons function
	 *
	 * @access	public
	 * @param	$_POST["id"] , $_POST["dealer"] ,file name with .xls , xlsx or .csv extension
         * @descrp	insert file content into dealerCoupon table & update total_stock in offers table based on coupon code
	 * @return	redirect to editDetails page
	 * @throws	Exception
    */
    public function actionAddCoupons(){
        $msg = "";
        $data = $_POST;
        if(isset($_POST["Submit"])) {
         try{
             $connection = Yii::app()->db;
             $transaction = $connection->beginTransaction();
            $dealid = $_POST["id"];
            $dealer_id = $_POST["dealer"];
            $filename = $_FILES['file1']['name'];
            $file = $_FILES["file1"]["tmp_name"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if($ext == 'csv'){                                   //Checking if the file type is .csv
              $handle = fopen($file, "r");                       //Open .csv file for reading
              $query = "INSERT INTO dealerCoupon (dealer_id,code,deal_id) VALUES "; //Insert query for dealerCoupon table
              $i=0;
              while(!feof($handle)){
                $coupon = fgetcsv($handle);                       //getting content from .csv file

                if(!empty($coupon[0])){
                    if($i!=0){
                            $query.= ",";
                        }
                $query.= "($dealer_id,'$coupon[0]','$dealid') ";
                 $i++;
                }
              
              }
             fclose($handle);                                     //close .csv file after reading    
            }elseif($ext == 'xlsx'){                              //Checking if the file type is .xlsx
             Yii::import('application.vendor.PHPExcel',true);
             $objReader     = PHPExcel_IOFactory::createReader('Excel2007');
             $objPHPExcel   = $objReader->load($file);            //$file is your filepath and filename
             $objWorksheet  = $objPHPExcel->getActiveSheet();
             $highestRow    = $objWorksheet->getHighestRow();     // e.g. 10
             $highestColumn = $objWorksheet->getHighestColumn();  // e.g 'F'
             $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
             $query = "INSERT INTO dealerCoupon (dealer_id,code,deal_id) VALUES "; //Insert query for dealerCoupon table
             for ($row = 1; $row <= $highestRow; ++$row) {
               for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                 $coupon_code = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                 $query.= "($dealer_id,'$coupon_code','$dealid') ";
                 $query.= ",";
                 break;  
               }
             }
             $query = trim($query,',');
            }elseif($ext == 'xls'){              //Checking if the file type is .xls  
              Yii::import('ext.phpexcelreader.JPhpExcelReader');  //Import phpexcelreader to read .xls or .xlsx file
              $data=new JPhpExcelReader($file);
              $data->dump(true,true);
              $data = $data->sheets[0]['cells'];
              $query = "INSERT INTO dealerCoupon (dealer_id,code,deal_id) VALUES "; //Insert query for dealerCoupon table
              foreach ($data as $dd){
                foreach ($dd as $key => $d){
                   $query.= "($dealer_id,'$d','$dealid') ";
                   $query.= ",";
                   break;                                         //break is used to read only 1 column 
                }
              }
              $query = trim($query,',');                          
            }else{                                                //throws Exception for invalid file
                 throw new Exception("Invalid File Uploaded, Please upload file only with .csv, .xls or .xlsx extension"); 
            }
            if(Yii::app()->db->createCommand($query)->execute()){
                $totalCoupons = Yii::app()->db->createCommand("SELECT COUNT(dealer_id) as CNT FROM `dealerCoupon` "
                        . "WHERE `deal_id` = $dealid AND `dealer_id` = $dealer_id ")->queryROW();
                $countCoupon =  $totalCoupons['CNT'];
                $currentDate = GeneralComponent::getFormatedDate();
                $updateStock = Yii::app()->db->createCommand("UPDATE offers SET `total_stock` = $countCoupon ,updated='$currentDate', `validity_mode` = 3"
                        . " WHERE `deal_id` = $dealid")->execute();
                $msg = "Inserted successfully";
                Yii::app()->user->setFlash('success', $msg);
                $transaction->commit();
            }else{
                throw new Exception("Error ocured while uploading file"); 
            }
          }catch(Exception $ex){
              $transaction->rollback();
            $msg = $ex->getMessage();
            Yii::app()->user->setFlash('error', $msg);
          }
        }
        $this->redirect("editDetails/".$dealid."#dealer_coupon"); 
    }/* End of function AddCoupons()**/
    
}

