<?php

class Api_newController extends Controller {

    var $userid;
    var $amount;
    var $connection;
    var $mobile_number;
    var $logger;
    var $track_id;
  
    public function actionInfo() {
        $this->render('info');
        $this->mobile_number = isset($_REQUEST['mobile_number']) ? $_REQUEST['mobile_number'] : Yii::app()->getUsername;
        $userObj = Users::model()->findByAttributes(array('user_name' => $this->mobile_number));
        $walletObj = Wallets::model()->findByAttributes(array('users_id' => $userObj->getAttribute('id')));
    }
    
    public function filterInput($data){
        $result= array();
        foreach ($data as $k=>$v){
            if(is_array($v))
               $result[$k]= $this->filterInput($v);
            else
               $result[$k]= addslashes($v);
        }
        
        return $result;
    }
    
    public function actionAction() {
        $date = date('Y-m-d');
        $_REQUEST = array_merge($_GET,$_POST);
        $_REQUEST = $this->filterInput($_REQUEST);
        //  error_reporting(E_ALL ^ E_NOTICE);   
        if (!isset($_REQUEST['actiontype']))
            throw new CHttpException(404, 'The specified post cannot be found.');
        $data = $_REQUEST;
        $actionType = $_REQUEST['actiontype'];
        $apiFlag = $_REQUEST['api'];
        $data['request_via'] = 'app';
        if (isset($data['res_format'])) {
            $data['request_via'] = 'web';
        }
        $res = "";
        $dumlog = new dumpLog('ApiTransactions', 'API_transaction');
        $this->logger = $dumlog->logger;
        Yii::app()->session['Transaction_trackid'] = GeneralComponent::get_unique_trackId();
        $this->track_id = Yii::app()->session['Transaction_trackid'];
        $this->logger->info($this->track_id . " IP: " . CHttpRequest::getUserHostAddress() . " inside function with param : " . json_encode($data));
        if($actionType == "purchase_history"){
        $header = '';
            foreach ($this->actionGetallheaders() as $name => $value) {
                $header .= "$name: $value\n";
            } 
            $date = date('Ymd');
            file_put_contents("/var/log/apps/header_$date.log", "| ".Yii::app()->user->getId()." |".  json_encode($header)." \n", FILE_APPEND | LOCK_EX);
        }
        $whitelisted_actions = array('pullback', 'check_transaction', 'pay1_status_update', 'clear_in_process_via_b2b','Popb2bTxnSucc','create_daily_wallet_copy','pullback_refund','getB2cAmount' );
        $bypassAction = array_merge($whitelisted_actions, array('refillwallet', 'delegate_call', 'create_user','register_user', 'resolve_complain', 'update_password', 'signin', 'signout', 'clear_in_process', 'test_solrResult',
                'get_deallist', 'get_deals', 'get_deal_details','get_deal_WebImg', 'get_similar_WebDeal', 'get_deal_offers', 'get_faq', 'complain_call', 'call_offline_notifier', 'get_freeBie','Get_LoyaltyPoints',
                 'get_all_operators', 'Is_Free_Bee_Used', 'Get_all_deal_data', 'payu_update', 'forgotpwd', 'download', 'download_by_missedcall', 'Daily_transaction',
                'Redeem_Freebie', 'Redeem_FreebieByApp', 'Notify_offer_expire_validity', 'Notify_offer_expire_stock', 'Expire_offer_stock', 'Expire_offer','NongenuineUser',
                'Notify_freeGift_expire', 'FreeGift_expired', 'GetupdatedDeal', 'Downloadapp','GetAllofferTypes','GetAllOfferCategory','getDealOffersList','Get_OffAvgReview',
                'GetAllGifts_WEB','Create_MissCall_user','GetGiftsByType_WEB','get_coupondetails','Send_coupon_code','check_usercashback','GetMyGifts_WEB', 'GetMyLikes_WEB' , 'GiftCoinDetails_WEB',
                'GetAllCategory_WEB','SearchGift_WEB','GetGiftDetails_WEB','GetGiftInfo_WEB','Announcement','Getuser_card','RenameImg','Popb2bTxnFail','sendSlowLogData',
                'refund_refill_transaction','GenuineUser','set_cashback','get_userinfo','transaction_details_web','setOfferReview','refresh_captcha','unblockuser_cron','failedLoginByAPP','killIdleConnections'));
        if (in_array($actionType, $whitelisted_actions) && !GeneralComponent::isIPWhitelisted()) {
            $this->logger->info($this->track_id . " UnAuthorizes access (ip whitelisted api) ");
            echo $res = json_encode(array('status' => 'failure', 'errCode' => '201', 'description' => 'UNAUTHORIZE ACCESS'));
            return;
        }
        if ($apiFlag === 'true' && is_null(Yii::app()->user->getId()) && !in_array($actionType, $bypassAction)) {
            $res = $this->actionApiauth();
        } else if (is_null(Yii::app()->user->getId()) && !in_array($actionType, $bypassAction)) {
            $this->logger->info($this->track_id . " UnAuthorizes access (bypass api) ");
            echo $res = json_encode(array('status' => 'failure', 'errCode' => '201', 'description' => 'UNAUTHORIZE ACCESS'));
            return;
        } else {
            $action = "action" . ucfirst(trim($actionType));
            $this->logger->info($this->track_id . " calling : " . $action . " with data : " . json_encode($_REQUEST));
            $res = $this->$action($data);
        }
        if (isset($data['app_type'])) {
            if (base64_decode($data['app_type']) == "webapps") {
                header('Access-Control-Allow-Origin: *');
                header('Access-Control-Allow-Methods: POST');
                header('Access-Control-Max-Age: 1000');
            }
        }
        //$response_type = isse($data['res_format'])?trim($data['res_format']):"";
        if (isset($data['mobile_number']) ) {
            $header = '';
            foreach ($this->actionGetallheaders() as $name => $value) {
                $header .= "$name: $value\n";
            }
            if (isset($data['mobile_number']) && $data['mobile_number']=='9076716912' ) {
                $date = date('Ymd');
                file_put_contents("/var/log/apps/userheader_$date.log", "| ".Yii::app()->user->getId()." |".  json_encode($header)." \n", FILE_APPEND | LOCK_EX);
            }
            $this->logger->info($this->track_id . " debug : " . json_encode($header) . "");
        }
        $res = GeneralComponent::get_formated_response($res, $data);
        echo $res;
        $this->logger->info($this->track_id . " memory - usage : " . number_format(memory_get_usage()) . "");
        unset($res, $data);
        $this->logger->info($this->track_id . " memory - usage after unset : " . number_format(memory_get_usage()) . "");
    }

    public function actionApiauth() {
        /* $user = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
          $signature = isset($_REQUEST['signature']) ? $_REQUEST['signature'] : "";
          $auth = new UserIdentity($user, $signature);
          if (!$auth->authenticate()) {
          echo "003";
          //$this->logger->info($this->track_id . " UnAuthorizes access ");
          //ApiResponse::unAuthorizedCall();
          echo json_encode(array('status'=>'failure','errCode'=>'201','description'=>'UNAUTHORIZE ACCESS'));
          exit();
          }
          $this->userid = Yii::app()->user->getId();
         */
        $this->logger->info($this->track_id . " UnAuthorizes access");
        $res = json_encode(array('status' => 'failure', 'errCode' => '201', 'description' => 'UNAUTHORIZE ACCESS'));
        return $res;
        //exit();
    }

    public function actionRefillwallet($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function ");
        $this->logger->info($this->track_id . " refillwallet from wallet component " . $_SERVER['REMOTE_ADDR']);
        $normalFlag = TRUE;
        if (isset($data['vcode']) && trim($data['vcode']) != "") {
            $rdata = VoucherComponent::getVoucher_detail($data);
            if ($rdata['errCode'] != 0) {
                $result = array('status' => 'failure', 'errCode' => $rdata['errCode'], 'description' => $rdata['description']);
                $res = json_encode($result);
                $normalFlag = FALSE;
            } else {
                $data['amount'] = $rdata['description']['amount'];
                $data['transaction_mode'] = 'voucher';
            }
        } elseif (!GeneralComponent::isIPWhitelisted()) {
            $normalFlag = FALSE;
            $res = json_encode(array('status' => 'failure', 'errCode' => '201', 'description' => 'UNAUTHORIZE ACCESS'));
            Yii::app()->session['exception'] = 'UNAUTHORIZE ACCESS';
        }
        if ($normalFlag == TRUE) {
            $res = json_encode(WalletComponent::refillwallet($data));
        }
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : api result : " . $res);
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . json_encode($trans_result_final));
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }

    public function actionRecharge($data = null) {
        $date = date('Ymd');
        $data = is_null($data) ? $_REQUEST : $data;
            
        $this->logger->info($this->track_id . " inside function");
        //$res = WalletComponent::rechargeTranction();
        $updateUserlat_lng = UsersComponent::updatedUserlat_lng($data);
        $res = WalletComponent::PaymentTranction($data);
//        echo $res;
        
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : api result : " . $res);
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . json_encode($trans_result_final));
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        $trans_result['deal'] = $this->actionDeal_of_day();
        if ($trans_result['status'] == 'success') {
            $data['synk_flag'] = 1;
            
            $trans_result['dealDetails']['Alldeals'] = array();
            $trans_result['dealDetails']['claimedGifts'] = '';
            $trans_result['dealDetails']['offer_details'] = array();
            $trans_result['dealDetails']['ExpiredOffers'] = '';
            $trans_result['dealDetails']['displayLimit'] = '20';
            
//            $trans_result['dealDetails'] = json_decode($this->actionGetupdatedDeal($data), true);
//            $trans_result['dealDetails'] = $trans_result['dealDetails']['description'];
        }
        $header = '';
        foreach ($this->actionGetallheaders() as $name => $value) {
            $header .= "$name: $value\n";
        }
       
        return json_encode($trans_result);
    }

    public function actionGetupdatedDeal($data) {
        $data = is_null($data) ? $_REQUEST : $data;

        $this->logger->info($this->track_id . " inside function");
        $res['offer_details'] = "";
        $res['Alldeals'] = "";
        $res['claimedGifts'] = "";
        if (isset($data['updatedTime'])) {
            $data['displayLimit'] = 20;
            if (isset($data['latitude']) && isset($data['longitude'])) {
                if (empty($data['longitude'])) {
                    $profileObj = new UserProfile();
                    if (isset($data['user_mobile'])) {
                        $profileObj = $profileObj->findByAttributes(array('mobile' => $data['user_mobile']));
                    } else {
                        $profileObj = $profileObj->findByAttributes(array('user_id' => Yii::app()->user->id));
                    }
                    $data['longitude'] = $profileObj['longitude'];
                    $data['latitude'] = $profileObj['latitude'];
                }
            } else if (isset(Yii::app()->user->id)) {
                $profileObj = new UserProfile();
                $profileObj = $profileObj->findByAttributes(array('user_id' => Yii::app()->user->id));
                $data['longitude'] = $profileObj['longitude'];
                $data['latitude'] = $profileObj['latitude'];
            }
            if(isset($data['api_version']) && ($data['api_version']==3)){
                $res['offer_details'] = array();
            }else{
                $res['offer_details'] = DealsComponent::getupdatedDeal($data);
            }
            if (isset($data['synk_flag']) && $data['synk_flag'] == 1) {
                $data['offer_details'] = $res['offer_details'];
            }
            $res['Alldeals'] = DealsComponent::getAllDealdetails($data);
            if(isset($data['api_version']) && ($data['api_version']==3)){
                $res['claimedGifts'] = '';
            }else{
                $res['claimedGifts'] = DealsComponent::getClaimedFreegift($data);
            }
            

            $res['ExpiredOffers'] = DealsComponent::getExpiredFreegift($data);
            $res['displayLimit'] = $data['displayLimit'];
        }
        $result = array('status' => 'success', 'errCode' => 0, 'description' => $res);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($result);
    }

    public function actionBillpayment($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $updateUserlat_lng = UsersComponent::updatedUserlat_lng($data);
        //$res = WalletComponent::rechargeTranction();
        unset($data['recharge']);
        $res = WalletComponent::PaymentTranction($data);
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $res;
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . $trans_result_final);
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        $trans_result['deal'] = $this->actionDeal_of_day();
        if ($trans_result['status'] == 'success') {
            $data['synk_flag'] = 1;
            $trans_result['dealDetails']['Alldeals'] = array();
            $trans_result['dealDetails']['claimedGifts'] = '';
            $trans_result['dealDetails']['offer_details'] = array();
            $trans_result['dealDetails']['ExpiredOffers'] = '';
            $trans_result['dealDetails']['displayLimit'] = '20';
            
//            $trans_result['dealDetails'] = json_decode($this->actionGetupdatedDeal($data), true);
//            $trans_result['dealDetails'] = $trans_result['dealDetails']['description'];
        }
        return json_encode($trans_result);
    }

    public function actionPay1_status_update($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = WalletComponent::updateTransaction();
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . $trans_result_final);
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }

    public function actionPullback($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = WalletComponent::pullbackTransaction();
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . $trans_result_final);
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }

    public function actionDelegate_call($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = ServiceComponent::delegateRecharge();
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . json_encode($trans_result_final));
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }

    public function actionComplain_call($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $data['request_track_id'] = $this->track_id;
        $data['loggerObj'] = $this->logger;
        $res = ServiceComponent::manage_missedcall_complain($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionSetDelegate($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::setDelegateRecharge());
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . $trans_result_final);
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }

    public function actionConfirm_delegate_call($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = ServiceComponent::confirmdelegateRecharge();
        $trans_result = json_decode($res, true);
        $trans_result_final = (!($trans_result['status'] === 'success')) ? Yii::app()->session['exception'] : $trans_result['description'];
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Called : " . Yii::app()->session['apiCalled']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : Api Response : " . Yii::app()->session['apiResponse']);
        $this->logger->info(Yii::app()->session['Transaction_trackid'] . " : transaction result : " . $trans_result_final);
        unset(Yii::app()->session['apiCalled'], Yii::app()->session['apiResponse']);
        return $res;
    }

    /**
	 * Create action Create_MissCall_user()
	 *
	 * @access	public
	 * @param	array $data (user mobile_numer and miss_call_number)
         * @descp       check & validate miss call mobile nuber and flag. Call createUser() if flag exist & mobile number 
         *              is valid 10 digit number starting with only [7,8,9]. 
         * @return	status success/failure message on the basis of function process
	 */
        //SMS_Miss_Call_Number       02267242255    
        //Poster_Miss_Call_Number    02267242211   
    public function actionCreate_MissCall_user($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $validate_mobile_number="/^[789][0-9]{9}$/";
        if((isset($data['mobile_number'])&&preg_match($validate_mobile_number, $data['mobile_number']))&&(isset($data['miss_call_num']))){
         $data['miss_call'] = TRUE;
         
         $data['flag'] = 1;
         if($data['miss_call_num']=='02267242211'){
             $data['user_type'] = 3;                        //Poster_Miss_Call_Number User
         }else{
             $data['user_type'] = 2;                        //SMS_Miss_Call_Number User
         }
         $data['origincode'] = $data['miss_call_num'];
         $data['msisdn']     = $data['mobile_number'];
         $data['delegate_id'] = 0;
         ServiceComponent::log_missed_call_history($data);   //for maintaing Miss_Call_history table
         $this->logger->info($this->track_id . " inside function");
         $res =  json_encode(UsersComponent::createUser($data));
        }else{
         $result = array('status' => 'failure', 'errCode' => 271 , 'description' => 'Invalid parameters, Please pass a valid 10 digit Mobile & Miss call number  as mandatory parameter ');
         $res = json_encode($result);
        }
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }//End of function Create_MissCall_user()	
    
    public function actionCreate_user($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $normalFlag = TRUE;
        if (isset($data['vcode']) && trim($data['vcode']) != "") {
//            $rdata = VoucherComponent::redeem_voucher($data);
            $rdata = VoucherComponent::getVoucher_detail($data);
            if ($rdata['errCode'] != 0) {
                $result = array('status' => 'failure', 'errCode' => $rdata['errCode'], 'description' => $rdata['description']);
                $res = json_encode($result);
                $normalFlag = FALSE;
            } else {
                $data['amount'] = $rdata['description']['amount'];
                $data['transaction_mode'] = 'voucher';
                WalletComponent::refillwallet($data);
            }
        }
        if ($normalFlag == TRUE) {
            $res = json_encode(UsersComponent::createUser($data));
        }
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionRegister_user($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $normalFlag = TRUE;
        if (isset($data['vcode']) && trim($data['vcode']) != "") {
//            $rdata = VoucherComponent::redeem_voucher($data);
            $rdata = VoucherComponent::getVoucher_detail($data);
            if ($rdata['errCode'] != 0) {
                $result = array('status' => 'failure', 'errCode' => $rdata['errCode'], 'description' => $rdata['description']);
                $res = json_encode($result);
                $normalFlag = FALSE;
            } else {
                $data['amount'] = $rdata['description']['amount'];
                $data['transaction_mode'] = 'voucher';
                WalletComponent::refillwallet($data);
            }
        }
        if ($normalFlag == TRUE) {
            $res = json_encode(UsersComponent::create_Newuser($data));
        }
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionReport_complain($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = ServiceComponent::manageComplain($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionCheck_transaction($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = TransactionComponent::check_trans_by_client_req($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionResolve_complain($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        if (!in_array('resolve', $data)) {
            $data['resolve'] = '';
        }
        $res = ServiceComponent::manageComplain($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionUpdate_password($data = null) {
        $date = date('Ymd');
        
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = UsersComponent::updatePassword($data);
        $this->logger->info($this->track_id . " return : " . $res);
        file_put_contents("/var/log/apps/SetOfferReview_$date.log", "|  ".  json_encode($data)." responce ".  json_encode($res)." \n", FILE_APPEND | LOCK_EX);
        return $res;
    }

    public function actionSignin($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = UsersComponent::login($data, FALSE);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionSignout($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = UsersComponent::logout($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionUpdate_profile($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = UsersComponent::edituserProfile($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionPurchase_history($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $page = isset($data['page']) ? $data['page'] : 0;
        $limit = isset($data['limit']) ? $data['limit'] : 10;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(TransactionComponent::getPurchaselog($page, $limit));
        $this->logger->info($this->track_id . " return : " . $res);
        $header = '';
        foreach ($this->actionGetallheaders() as $name => $value) {
            $header .= "$name: $value\n";
        }
        $this->logger->info($this->track_id . " debug : " . json_encode($header) . "");
        return $res;
    }

    public function actionWallet_history($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $page = isset($data['page']) ? $data['page'] : 0;
        $limit = isset($data['limit']) ? $data['limit'] : 10;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(TransactionComponent::getTransactionlog(null, 'wallet', $page, $limit));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionCities_By_StateId($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $state_id = $data["state_id"];
        //$page = isset($data['page'])?$data['page']:0;
        //$limit = isset($data['limit'])?$data['limit']:10;
        //$this->logger->info($this->track_id . " inside function");
        //$res = TransactionComponent::getTransactionlog('1','wallet',$page,$limit);
        //$this->logger->info($this->track_id . " return : " . $res);
        $cities = GeneralComponent::getCitiesListByStateId($state_id);
        $cities = json_encode($cities);
        return $cities;
    }

    public function actionGet_faq($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = stripcslashes(json_encode(GeneralComponent::getFAQList($data)));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionDownload_by_missedcall($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::sms_on_missedcall($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionSupport_req($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(GeneralComponent::supportRequest($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_deallist($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getDeals($data));
        $this->logger->info($this->track_id . " return : " . $res);
        if (!isset($data['dod'])) {
            return $res; //--echo
        }
        return $res;
    }

    public function actionGet_all_operators($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $res = json_encode(GeneralComponent::getCombineOperators());
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionAdd_quickpay($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::addQuickpay($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_quickpaylist($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::get_quickpaylist());
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionEdit_quickpay($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::edit_quickpay($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_deals($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getDeals($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_deal_details($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        if(isset($data['api_version']) && $data['api_version']==3){
            $res = json_encode(DealsComponent::offerDetails($data));
        }else{
            $res = json_encode(DealsComponent::getDetailedDeal($data));
        }
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    public function actionGet_deal_WebImg($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getDealWebImg($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    public function actionGet_similar_WebDeal($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getSimilarWebDeal($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_deal_offers($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getDealOffers($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionDel_quickpay($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::del_quickpay($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionPayu_update($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $response_data = $_POST;
        
        if(isset($response_data['page']) && $response_data['page']=='web.pay1.loc'){
            $response_data['page']='success';
            isset($response_data['page']) ? $response_data['page']='success' : '';
            if(isset($data['home']))
                unset($data['home']);
        }
         $this->logger->info($this->track_id . " inside function");
        if (isset($response_data['page'])) {
            if ($response_data['page'] != "success") {
                //$data['status'] = "failure";
                $response_data['status'] = "failure";
            }
            if ($response_data['page'] == "success" && $response_data['field9']=='Cancelled by user') {
                $response_data['page']='pending';
                $response_data['status']='pending';
            }
        }
        $res ='';
       
        //if(isset($response_data['hash'])){
        $result = OnlineComponent::update_pg_payu($response_data);
        //if(isset($result['returnparam'])){
         //   return $result;
        //}
        $res = json_encode($result);
        //$res = json_encode(OnlineComponent::update_pg_payu($response_data));

       // }
        $this->logger->info($this->track_id . " return : " . $res);
        if (!isset($data['api'])) {
            $this->redirect(array('transactions/online_response', 'txnid' => $data['txnid'], 'page' => $data['page'], 'status' => $data['status']));
        }
        
        //echo $res;
    }

    public function actionPurchase_deal($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = WalletComponent::Processpayments($data);
        //$res = json_encode(WalletComponent::Processpayments($data));
        $this->logger->info($this->track_id . "   " . $res);
        $res_array = json_decode($res, true);
        $res_array['deal'] = $this->actionDeal_of_day();
        $res = json_encode($res_array);
        return $res;
    }

    public function actionCheck_bal($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $result = array('status' => 'success', 'errCode' => 0, 'description' => WalletComponent::getWalletDetail());
        $result['deal'] = $this->actionDeal_of_day();
        $res = json_encode($result);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionOnline_walletrefill($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $data = array_merge($data, array('trans_category' => 'refill', 'paymentopt' => 'online'));
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(WalletComponent::refillwallet_Online($data));
        //$res = WalletComponent::refillwallet_Online($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
        //print_r($res);        
    }

    public function actionGet_deal_coupon($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(VoucherComponent::get_deal_couponCode($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionCheck_voucher($data = NULL) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(VoucherComponent::getVoucher_detail($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionRedeem_voucher($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(VoucherComponent::redeem_voucher($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_my_deal($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::get_my_deals($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionNotifyme($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::notifyme($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionForgotpwd($data = null) {
        $date = date('Ymd');
        file_put_contents("/var/log/apps/SetOfferReview_$date.log", "|  ".  json_encode($data)." \n", FILE_APPEND | LOCK_EX);
        
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(UsersComponent::forgotPassword($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionDownload($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        echo "Download will be available soon";
        header('Location: https://play.google.com/store/apps/details?id=com.pay1&hl=en');
        exit;
    }

    public function actionDeal_of_day($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $deal_req_param = array('latitude' => 1, 'longitude' => 1, 'dod' => TRUE);
        $_REQUEST = array_merge($_REQUEST, $deal_req_param);
        $data = json_decode($this->actionGet_deallist(), true);
        return $data['description'];
    }

    public function actionClear_in_process($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(OnlineComponent::payu_get_txn_detail($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionGet_all_deal_data($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getAllDealData($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionIs_Free_Bee_Used($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(GeneralComponent::isFreeBeeUsed($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionClear_in_process_via_b2b($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(ServiceComponent::check_pay1_transaction_status());
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    public function actionPopb2bTxnFail($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(UsersComponent::popb2bTxnFail($data));
        $this->logger->info($this->track_id . " return : " . $res);
        //return $res;
    }
    
    public function actionPopb2bTxnSucc($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(UsersComponent::popb2bTxnSucc($data));
        $this->logger->info($this->track_id . " return : " . $res);
        //return $res;
    }

    public function actionCall_offline_notifier($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $type = isset($data['type']) ? $data['type'] : array();
        $data = isset($data['data']) ? $data['data'] : array();
        $mode = isset($data['mode']) ? $data['mode'] : null;
        $res = json_encode(GeneralComponent::offline_notifier($type, $data, $mode));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionView_templates($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        switch (strtolower($data['type'])) {
            case 'email':
                break;
            case 'sms':
                break;
            case 'notification':
                break;
        }
    }

    public function actionGet_freeBie($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = GeneralComponent::get_FreeBie($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    public function actionGet_solrResult($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $var = new MultiSortComponent($data);

        $limit = 3;
        $column_name = 'frequency';
        if (isset($data['limit']))
            $limit = trim($data['limit']);
        if (isset($data['column_name']))
            $column_name = $data['column_name'];
        $res = json_encode($var->get_solrResult($column_name, $limit));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionPayu_Details($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = (OnlineComponent::payu_id_details($data));
        $this->logger->info($this->track_id . " return : " . $res);
        echo "<pre>";
        print_r($res);
    }

    public function actionDaily_transaction($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = (GeneralComponent::daily_transaction($data));
        $this->logger->info($this->track_id . " return : " . $res);
        if(isset($data['url'])){
            return $res;
        }
    }

    public function actionRedeem_Freebie($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(GeneralComponent::redeem_Freebie($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionRedeem_FreebieByApp($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(GeneralComponent::redeem_FreebieByApp($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }

    public function actionNotify_offer_expire_validity($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::notify_offer_expire_validity());
        $this->logger->info($this->track_id . " return : " . $res);
    }

    public function actionNotify_offer_expire_stock($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::notify_offer_expire_stock());
        $this->logger->info($this->track_id . " return : " . $res);
    }

    public function actionExpire_offer_stock($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::expire_offer_stock());
        $this->logger->info($this->track_id . " return : " . $res);
    }

    public function actionExpire_offer($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::expire_offer());
        $this->logger->info($this->track_id . " return : " . $res);
    }

    public function actionNotify_freeGift_expire($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::notify_freeGift_expire()); //before 2 day expiry
        $this->logger->info($this->track_id . " return : " . $res);
    }

    public function actionFreeGift_expired($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::freeGift_expired()); //current day expiry
        $this->logger->info($this->track_id . " return : " . $res);
    }

    public function actionGetDealOffersList($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res[0] = DealsComponent::getDealOffersList($data['deals_id'], '', true);
        $res[1] = DealsComponent::get_gallery_list($data['deals_id'], '', true);
        ;
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }

    public function actionDownloadapp($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $agent = $_SERVER['HTTP_USER_AGENT'];
        if (stripos($agent, "Android")) {
            $this->redirect('https://play.google.com/store/apps/details?id=com.pay1');
        } else if (stripos($agent, "Windows Phone 8")) {
            $this->redirect('https://www.windowsphone.com/s?appid=074d22c8-ad55-4933-9cf0-61fa2e8d5e8f');
        } else if (stripos($agent, "Windows Phone")) {
            $this->redirect('https://www.windowsphone.com/s?appid=074d22c8-ad55-4933-9cf0-61fa2e8d5e8f');
        } else {
            //$this->redirect('http://test.pay1.in');
            echo "<h1 style='font-size:25px;padding-top:30px'>Sorry there is no app for your mobile</h1>";
        }
        // $res = json_encode(ServiceComponent::downloadapp());
    }
    
    public function actionGetAllOfferCategory($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getAllOfferCategory($data); //current day expiry
        $this->logger->info($this->track_id . " return : " . $res);
        $result = array('status' => 'success', 'errCode' => '0', 'description' => json_decode($res,true));
        return json_encode($result);
    }
    
    public function actionGetAllofferTypes($data = null) {
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getAllofferTypes($data); //current day expiry
        $this->logger->info($this->track_id . " return : " . $res);
        $result = array('status' => 'success', 'errCode' => '0', 'description' => json_decode($res,true));
        return json_encode($result);
    }
    
    public function actionGet_LoyaltyPoints($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = GeneralComponent::get_LoyaltyPoints($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    public function actionMyLikes($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $date = date('Ymd');
        file_put_contents("/var/log/apps/myLikes_$date.log", "|  ".  json_encode($data)." \n", FILE_APPEND | LOCK_EX);
        
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::myLikes($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionSetUserFeedback($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = UsersComponent::setUserFeedback($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionSetOfferReview($data = null) {
        $date = date('Ymd');
        file_put_contents("/var/log/apps/SetOfferReview_$date.log", "|  ".  json_encode($data)." \n", FILE_APPEND | LOCK_EX);
        $this->logger->info($this->track_id . " inside function");
        $data = is_null($data) ? $_REQUEST : $data;
        
        $res = DealsComponent::setOfferReview($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
     public function actionLoyaltyPointsHistory($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = TransactionComponent::loyaltyPointslog($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionGetTrans_WalletDetail($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $id = isset($data['txnid']) ? $data['txnid']: 0;
        $this->logger->info($this->track_id . " inside function");
        $res = WalletComponent::getTrans_WalletDetail($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionGetTrans_loyaltyDetail($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $id = isset($data['txnid']) ? $data['txnid']: 0;
        $this->logger->info($this->track_id . " inside function");
        $res = WalletComponent::getTrans_loyaltyDetail($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionGet_OffAvgReview($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $id = isset($data['id']) ? $data['id']: 0;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::get_OffAvgReview($id);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    //get all gifts (types/categories)
    public function actionGetAllGifts_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getAllGifts_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    //get deal by type (gift/category) and typeid
     public function actionGetGiftsByType_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getGiftsByType_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
     //get mygift for web
     public function actionGetMyGifts_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getMyGifts_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
     //get mylikes for web
     public function actionGetMyLikes_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getMyLikes_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    //get deal on gift coins by type (gift/category) and typeid
     public function actionGiftCoinDetails_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getGiftCoinDetails_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionGetAllCategory_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getAllCategory_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    //Offer Details for WEB
    public function actionGetGiftDetails_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getGiftDetails_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
     //give id and name of particular type eg.deal type or category
    public function actionGetGiftInfo_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getGiftInfo_WEB($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionMyLikesCount($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::myLikesCount($data);
        $bal = WalletComponent::getWalletDetail();
        $wallet['wallet_balance'] = $bal['account_balance'];
        $wallet['loyalty_points'] = $bal['loyalty_points'];
        $user_data = array_merge($res,$wallet);
        $res = array('status'=>'success','errocode'=>0,'description'=>$user_data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionAnnouncement($data){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::getUsersforannoucement($data);
        $this->logger->info($this->track_id . " return : " . $res);
    }
    
    public function actionGetBase_64Code($data) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = UsersComponent::getImageUrlFromBase64Code($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    public function actionGet_Payu_Token($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getPayuToken($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    /**
         * Create action SetRedisAll_Users()
         *
         * @access	public
         * @param	array $data
         * @descp       Call setRedisAllUsers() which is used for set all the users from Users table
         * @return	status success/failure message on the basis of function process
    */
    public function actionSetRedisAll_Users($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(UsersComponent::setRedisAllUsers());
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }//End of function SetRedisAll_Users()	

    /**
         * Create action Coupon_code_CSV()
         *
         * @access	public
         * @param	array $data
         * @descp       Call Coupon_code_CSV() which is create given number of coupon code and
         *              write into .csv file 
         * @return	status success/failure message on the basis of function process
    */
    public function actionCoupon_code_CSV($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
       $res = json_encode(DealsComponent::Coupon_code_CSV($data));
       $this->logger->info($this->track_id . " return : " . $res);
       return $res;
    }//End of function Coupon_code_CSV()	
    
    /**
         * Create action Distributor_Extra_Incentive()
         *
         * @access	public
         * @param	array $data
         * @descp       Call Distributor_Extra_Incentive()of MultiSortComponent to hit solar api on basis of product_id and
         * @return	return total sum on success/failure message on the basis of function process
    */
    public function actionDistributor_Extra_Incentive($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(MultiSortComponent::Distributor_Extra_Incentive($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }//End of function Distributor_Extra_Incentive()    
    
    public function actionGetuser_card($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::getuser_card($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    public function actionRenameImg($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(DealsComponent::renameImg());
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    public function actionNongenuineUser($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $month = isset($data['month']) ? $data['month']: '';
        $year = isset($data['year']) ? $data['year']: '';
        $res = json_encode(GeneralComponent::nongenuineUser($month,$year));
        $this->logger->info($this->track_id . " return : " . $res);
    }
   
    public function actionGetVoucher($data = null){
        //$this->logger->info($this->track_id . " inside function");
        $res = GeneralComponent::getGiftXoxoVoucher($data);
       // $this->logger->info($this->track_id . " return : " . $res);
       //var_dump($res->result[0]->code);
        return $res->result[0]->code;
        //print_r($res);
    }
    
    public function actionRefund_refill_transaction($data = null){
        $this->logger->info($this->track_id . " inside function");
        $res = OnlineComponent::refund_refill_transaction($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
   
    public function actionsendSlowLogData(){
        $obj = new GeneralComponent();
        return $obj->sendSlowLogData();
    }
    
    public function actionCreate_daily_wallet_copy(){
        $cur_date = date('Ymd');
        $newTable = "wallets_".$cur_date;
        $createTable = Yii::app()->db->createCommand("CREATE TABLE $newTable like wallets")->execute();    
        $copyTable = Yii::app()->db->createCommand("INSERT $newTable SELECT * FROM wallets")->execute();    
    }
    
    public function actionGenuineUser(){
        $this->logger->info($this->track_id . " inside function");
        $res = GeneralComponent::genuineUser();
        $this->logger->info($this->track_id . " return : " . $res);
        return json_encode($res);
    }
    
    //for searching gifts by brand name and area name
    public function actionSearchGift_WEB($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = DealsComponent::giftsearch($data);
        $this->logger->info($this->track_id . " return : " . json_encode($res));
        return json_encode($res);
    }
    
    public function actionCancel_payment($data = null){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = OnlineComponent::cancel_payment($data);
        $this->logger->info($this->track_id . " return : " . $res);
        return true;
        //return json_encode($res);
    }
    
    //get coupon details 
    public function actionGet_coupondetails($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(VoucherComponent::get_coupondetails($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    //send voucher code through sms
    public function actionSend_coupon_code($data){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(GeneralComponent::send_coupon_code($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    //check voucher code validity in recharge section
    public function actionCheck_usercashback($data){
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(VoucherComponent::check_usercashback($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
    
    //give cashback to user
    public function actionSet_cashback(){
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(VoucherComponent::cashback());
        $this->logger->info($this->track_id . " return : " . $res);
    }
    
    //give cashback to user
    public function actionGet_userinfo(){
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(UsersComponent::getuserinfo());
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }
   public  function  actionTransaction_details_web($data){
       $data = is_null($data) ? $_REQUEST : $data;
       $this->logger->info($this->track_id . " inside function");
        $res = json_encode(TransactionComponent::transaction_details_web($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
   }
   /*
    Created By :  Swapnil Tiwhane
    * Input : username
    * Output : captcha with Image name
    *     */
   public function  actionRefresh_captcha($data){
    $data = is_null($data) ? $_REQUEST : $data;
    $this->logger->info($this->track_id . " inside function");
    $res = json_encode(UsersComponent::createCaptcha($data));
    $this->logger->info($this->track_id . " return : " . $res);
    return $res;
   }
    /**
         * Create action Distributor_Extra_Incentive()
         *
         * @access	public
         * @param	array $data  username mandatory
         * @descp       when user exide max failed login attemp this fun get called to block user for 1 hr
         * @return	return success or fail
    */
    public function actionFailedLoginByAPP($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        $this->logger->info($this->track_id . " inside function");
        $res = json_encode(UsersComponent::failedLoginByAPP($data));
        $this->logger->info($this->track_id . " return : " . $res);
        return $res;
    }//End of function Distributor_Extra_Incentive()   
    
    public static function actionUnblockuser_cron(){
        UsersComponent::unblockuser_cron();
    }

    public function actionPullback_refund($data = null){
        $this->logger->info($this->track_id . " inside function");
        $res = OnlineComponent::pullback_refund($data);
        $this->logger->info($this->track_id . " return : " . json_encode($res));
        return json_encode($res);
    }
    
    public function actionGetallheaders(){
        if (!function_exists('getallheaders')) {            
                $headers = '';
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
                return $headers;
            }else{
                return getallheaders();
            }        
    }
    
    public function actionKillIdleConnections($slave=0) {
        $result = Yii::app()->db->createCommand("SHOW FULL PROCESSLIST")->queryAll();
        $sub = "";
        $time = 50;
        if(count($result)>1)
        foreach ($result as $key=>$res) {
            $process_id = $res['Id'];
            $db = $res['db'];
            //&& date('H') > 8 && date('H') < 23 
            if ($res['Time'] > $time && $db='b2c_app1') {
               
               Yii::app()->db->createCommand("KILL $process_id")->execute();
            }
         }
    }
    
   public function actionGetB2cAmount($data = null){

                if(isset($data['date'])  ){
                   $date = (int) $data['date'];
                   $d = DateTime::createFromFormat('Ymd', $date);
                        if($d && $d->format('Ymd') == $date){
                            $res = ReportingComponent::getB2cAmount($data['date']);
                            return $res;
                        }else{
                                return "Plese enter valid date format  YYYYMMDD";
                        }
                 }
    }


    public function actionTest($data = null) {
        $data = is_null($data) ? $_REQUEST : $data;
        //echo $content = Yii::app()->params['message_template']['B2C_BY_RETAILER_RECHARGE_FAIL'];die;
//         return $fn = UsersComponent::setRedisCreateUser();
//         return $fn = UsersComponent::getRedisCreateUser(8115093740);
//         return $fn = DealsComponent::getupdatedDeal();
         //return $fn = UsersComponent::rpushRedisUser();    //SUCCESS POP
      //  return $fn = OnlineComponent::setRedisTransData($data['txnid']);
         
//         return $fn = UsersComponent::rpopFailureRedisUser();    //FAILURE POP
// return $fn = OnlineComponent::setRedisTransData();
         //return $fn = DealsComponent::getGiftsByType_WEB($data);
        //$fn = DealsComponent::getAllDealData_old($data);
        //$offer = Offers::model()->findByAttributes(array('deal_id'=>54));
        //var_dump(empty($offer));
        //$fn = GeneralComponent::sendSMS($data['msisdn'],$data['message']);
        //echo "<pre>";
        //print_r($data);exit();
        //$fn = UsersComponent::createUser($data);
        //$fn = WalletComponent::getTempWalletDetail($data['mobile']);
        //$fn = ServiceComponent::getDelegateMissed($data['mobile_number']);
        //$fn = ServiceComponent::manageComplain($data);
        //$fn = GeneralComponent::generate_OTP(4);
        //$fn = UsersComponent::sendOTP($data);
        //$fn = (UsersComponent::verifyOTP($data)) ? "T" : "F";        
        //$fn = GeneralComponent::getuserSummary($data);
        //$fn = TransactionComponent::check_trans_by_client_req($data);
        //$fn = QuickPay::model()->getAttributes();
        //$fn = Yii::app()->db->createCommand("SELECT * FROM wallets WHERE users_id ='" . 1 . "'")->queryAll();
        //$fn = GeneralComponent::getProductDetails('DATA', 8);
        //$fn = TransactionComponent::getTransactionlog('1','wallet');
        //$fn = UsersComponent::edituserProfile($data);
        //$fn =Yii::app()->user->getId();
        //var_dump($fn);
        //$fn = UsersComponent::is_incomplete_signup($data['mobile']);
        // $fn = UsersComponent::getFullUserDetails($data['wallet_user_name']);
        //$fn = TransactionComponent::getPurchaselog();
        //$fn = GeneralComponent::getFAQList();
        //$fn = DealsComponent::getAllDealData($data);
        //$fn = GeneralComponent::getCombineOperators();
        //$fn = GeneralComponent::get_mobileNumbers($data['emailId'],$data['deviceId']);
        //$fn = DealsComponent::getDeals($data);
        //$fn = GeneralComponent::getCombineOperators();
        //$fn = GeneralComponent::isFreeBeeUsed($data);
        //$fn = GeneralComponent::set_FreeBee($data);
        //$fn = GeneralComponent::getFreeBee();
        //$fn = WalletComponent::call_dealpayment_by_wallet($data);
        //$fn = ServiceComponent::addQuickpay($data);
        //$fn = ServiceComponent::setDelegateRecharge($data);
        //$fn = ServiceComponent::get_quickpaylist();
        //$fn = ServiceComponent::edit_quickpay($data);
        //$fn = DealsComponent::getDetailedDeal($data);
        //$fn = ServiceComponent::del_quickpay($data);
        //$fn = WalletComponent::checkWalletBal();
        //$fn = Yii::app()->params['vendorApi']['pay1'];
        //$fn = Yii::app()->params['vendorApi']['payment_gateway']['payu_india']['SUCCESS_URL'];
        //$fn = yii::app()->cache->hdel('transdata',$data['txnid']);
        //$fn = GeneralComponent::generate_unique_number($data['len'],$data['onlynum']);
        //$fn = OnlineComponent::process_refill_after_pg($data);
        //$fn = VoucherComponent::generate_VCODE($data);
        //$fn = VoucherComponent::getVoucher_detail($data);
        //$fn = json_encode(GeneralComponent::getFAQList($data));
        //$r = '{"number":"9004387418","product":"POSTPAID","amount":"20","trans_id":"19837","operator":"42","stv":0,"userid":"50","trans_category":"billing"}';
        //$r = json_decode($r,TRUE);
        //$fn = VendorComponent::callPay1($r);       
        //$user_id = $data['user_id'];
        //$fn =GeneralComponent::sendIOSNotification('29d25cdc7eaec2c471ca78f4eb046903be3bbe9253045be28684903551cd5b79','title','Testing message');
        //$fn = GeneralComponent::sendNotification(array(1),"Trial Nofication","This is test message three four five","notification",1,0,'http://b2c.pay1.in/images/53bbfe961d542.png');
        //$data['img'] = isset($data['img'])?'http://b2c.pay1.in/images/53bbfe961d542.png':null;
        //$fn = GeneralComponent::sendNotification(array(50),$data['title'],$data['msg'],$data['type'],$data['bal_flag'],$data['save_flag'],$data['img']);
        //$fn = Users::model()->findByAttributes(array('id'=>Yii::app()->user->id))->attributes;
        //$fn = GeneralComponent::notifier(array('FORGOT_PASSWORD'),array('password'=>"12345",'email'=>"nandan@mindsarray.com"));
        //$fn = UsersComponent::getuserProfile($data['user'],$data['txn']);
        //$fn = TransactionComponent::get_Deal_Transaction_details($data['transaction_id']);
        //$fn = $this->actionNotifyme();
        //$fn = TelecomComponent::getOperatorByTransaction($data['trans_id'],$data['category']);
        //$connection = Yii::app()->db;
        //$fn = OnlineComponent::payu_get_txn_detail($data);
        //$tablename = "pg_payuIndia";
        //$payuQry = "select txnid from ".$tablename." where txnid='".$data['txnid']."'";
        //$fn = $connection->createCommand($payuQry)->queryall();
        //$fn = $this->actionDeal_of_day();
        //$fn = GeneralComponent::sendMail("nr15627@gmail.com", "Blocked pay1 user", json_encode($data));
        //$fn = Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,user_id) VALUES (NULL, $userid)")->execute();
        //$fn = GeneralComponent::sendNotification(array(85),"Trial Nofication","This is test message three","notification",1,1,'http://b2c.pay1.in/images/53bbfe961d542.png');
        // $fn = GeneralComponent::sendNotification(array($data['id']),"Trial Nofication","This is test message three","notification",1,1,'http://b2c.pay1.in/images/53bbfe961d542.png','15');
        //$fn = GeneralComponent::sendNotification(array($data['id']),"Trial Nofication","This is test message three","notification",1,1,'http://b2c.pay1.in/images/53bbfe961d542.png',$data['offer_id']);
        //$fn = ServiceComponent::get_quickpaylist($data['id']);
        //$userid = 0;
        //$fn = VendorComponent::get_pay1_transaction_status($data['trans_id']);
        //$fn = VendorComponent::get_pay1_transaction_status($data);
        //$fn = ServiceComponent::check_pay1_transaction_status();
        //$fn = OnlineComponent::process_transaction_after_pg($data);
        //$fn = ServiceComponent::check_pay1_transaction_status();   
        //$fn = WalletComponent::getWalletDetail(1);
        //Yii::app()->db->createCommand("INSERT INTO black_listed_user (id,users_id) VALUES (NULL, $userid)")->execute();
        //$fn = GeneralComponent::sendMail("ashish@mindarray.com", "Blocked pay1 user", json_encode($data));
        //$fn = GeneralComponent::sendMail("nandan@mindsarray", "Blocked pay1 user", "hello tests");
        //$fn = json_encode(UsersComponent::getuserProfile('1'));
        //$fn = ReportingComponent::get_user_details($data);
        //$fn = GeneralComponent::redeem_Freebie($data);
        //$fn = DealsComponent::notify_freeGift_expire();
        //$fn = ReportingComponent::get_detailed_report($data);
        //$fn = DealsComponent::getAllDealdetails($data);
        // $fn = ReportingComponent::get_retailermapcount();
        // $fn = iconv("UTF-8", "ISO-8859-1//TRANSLIT",DealsComponent::getDetailedDeal($data));
        //   var_dump($fn);
        //$fn = GeneralComponent::get_register_details();
        return json_encode($fn);
        //return $fn;
        //print_r($fn);
    }

}
