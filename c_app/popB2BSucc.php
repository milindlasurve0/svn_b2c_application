<?php
     /*
 * pop up succesfull transaction from b2b
 * 
 */
require_once(dirname(__FILE__).'/protected/vendor/Predis/Autoloader.php'); 
$config = require(dirname(__FILE__) . '/protected/config/cache_config.php');
$config = $config['servers'];
global $domain_url;
$urlSucc = $domain_url."index.php/api_new/action/api/true/actiontype/Popb2bTxnSucc/?";
    
try{
    Predis\Autoloader::register();
    $redisObj = new Predis\Client($config);
} catch (Exception $ex) {
    print_r($ex->getTraceAsString());
}

function curl_post_async($url, $params=array()){
    $post_params = array();
    foreach ($params as $key => $val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);
    $parts=parse_url($url);
    $fp = fsockopen($parts['host'],isset($parts['port'])?$parts['port']:80,$errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)){
        $out.= $post_string;
    }
    fwrite($fp, $out);
    fclose($fp);
}


    
    /*
 * Executing the script forever
 * 
 */
while(1){
    $data  = array();
    $myarray =  $redisObj->rpop("B2B_shop_success_txn_Q");   
    
    $data = json_decode($myarray,true);
    var_dump($data);
    if(is_null($data)){
        echo "\n".date('Y-m-d H:i:s')." going to sleep.\n";
        sleep(5);
        continue;
    }else{
       curl_post_async($urlSucc,$data);
    }
}