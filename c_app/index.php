<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',false);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii); 
//Yii::createWebApplication($config)->run();

$app = Yii::createWebApplication($config);
spl_autoload_unregister(array('YiiBase','autoload')); 
Yii::import('application.vendors.*');
require_once(dirname(__FILE__).'/protected/vendor/log4php/Logger.php'); // require registers Logger autoload
require_once(dirname(__FILE__).'/protected/vendor/lib/nusoap.php'); // require registers Logger autoload
Yii::registerAutoloader(array('LoggerAutoloader', 'autoload'), true);
spl_autoload_register(array('YiiBase','autoload'));

$app->run();
